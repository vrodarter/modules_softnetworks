package com.dl.openbravo.magento;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.utility.Image;
import org.openbravo.model.common.enterprise.Organization;

import com.dl.openbravo.magento.client.soap.v1.model.region.Region;
import com.dl.openbravo.magento.client.soap.v1.service.RemoteServiceFactory;
import com.dl.openbravo.magento.client.soap.v1.service.ServiceException;
import com.dl.openbravo.magento.client.soap.v1.soap.MagentoSoapClient;
import com.dl.openbravo.magento.client.soap.v1.soap.SoapConfig;
import com.dl.openbravo.magento.data.DLMGTOStore;
import com.dl.openbravo.magento.mapping.AttributeMapper;
import com.dl.openbravo.magento.mapping.AttributeSetMapper;
import com.dl.openbravo.magento.mapping.BPGroupMapper;
import com.dl.openbravo.magento.mapping.CustomerMapper;
import com.dl.openbravo.magento.mapping.ProductCategoryMapper;
import com.dl.openbravo.magento.mapping.ProductMapper;
import com.dl.openbravo.magento.mapping.SalesOrderLineMapper;
import com.dl.openbravo.magento.mapping.SalesOrderMapper;
import com.dl.openbravo.magento.service.MagentoAddressService;
import com.dl.openbravo.magento.service.MagentoAttributeService;
import com.dl.openbravo.magento.service.MagentoAttributeSetService;
import com.dl.openbravo.magento.service.MagentoBPGroupService;
import com.dl.openbravo.magento.service.MagentoCustomerService;
import com.dl.openbravo.magento.service.MagentoInvoiceService;
import com.dl.openbravo.magento.service.MagentoProductCategoryService;
import com.dl.openbravo.magento.service.MagentoProductService;
import com.dl.openbravo.magento.service.MagentoSalesOrderService;
import com.dl.openbravo.magento.service.MagentoShippingService;

public class MagentoContext implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

  private static final String STR_FIND_OB_LOOKING = "Looking for : ";
  private static final String STR_FIND_OB_NOT_FOUND = " Not Found ";

  private String magentoUrl;
  private String magentoUser;
  private String magentoApiKey;

  private DLMGTOStore store;

  private MagentoSoapClient soapClient;

  private SoapConfig soapConfig;

  private RemoteServiceFactory remoteServiceFactory;

  private Map<String, Map<String, Region>> regionCache = new HashMap<String, Map<String, Region>>();

  private final StringBuilder debugStringBuilder = new StringBuilder();
  private final StringBuilder debugFindStringBuilder = new StringBuilder();
  private final StringBuilder debugErrortringBuilder = new StringBuilder();
  private final Date debugDate = new Date();

  private ProductMapper productMapper;
  private MagentoProductService productService;

  private AttributeMapper attributeMapper;
  private MagentoAttributeService attributeService;

  private AttributeSetMapper attributeSetMapper;
  private MagentoAttributeSetService attributeSetService;

  private BPGroupMapper bpGroupMapper;
  private MagentoBPGroupService bpGroupService;

  private CustomerMapper customerMapper;
  private MagentoCustomerService customerService;

  private ProductCategoryMapper productCategoryMapper;
  private MagentoProductCategoryService productCategoryService;

  private MagentoAddressService addressService;

  private SalesOrderMapper salesOrderMapper;
  private SalesOrderLineMapper salesOrderLineMapper;
  private MagentoSalesOrderService salesOrderService;

  private MagentoShippingService magentoShippingService;
  private MagentoInvoiceService magentoInvoiceService;

  public MagentoContext(DLMGTOStore store) {
    this.store = store;
    this.magentoUrl = store.getURL();
    this.magentoUser = store.getAPIUser();
    this.magentoApiKey = store.getAPIKey();
  }

  public MagentoSoapClient getSoapClient() {
    if (soapClient == null) {
      debug("Point to    :" + getMagentoUrl());
      soapClient = MagentoSoapClient.getInstance(getSoapConfig());
    }
    return soapClient;
  }

  public SoapConfig getSoapConfig() {
    if (soapConfig == null)
      soapConfig = new SoapConfig(getMagentoUser(), getMagentoApiKey(), getMagentoUrl());
    return soapConfig;
  }

  public RemoteServiceFactory getRemoteServiceFactory() {
    if (remoteServiceFactory == null)
      remoteServiceFactory = new RemoteServiceFactory((MagentoSoapClient) getSoapClient());
    return remoteServiceFactory;
  }

  public String getMagentoUrl() {
    return magentoUrl;
  }

  public String getMagentoUser() {
    return magentoUser;
  }

  public String getMagentoApiKey() {
    return magentoApiKey;
  }

  public DLMGTOStore getStore() {
    if (!OBDal.getInstance().getSession().contains(store)) {
      store = OBDal.getInstance().get(DLMGTOStore.class, store.getId());
    }
    return store;
  }

  public ProductMapper getProductMapper() {
    if (productMapper == null)
      productMapper = new ProductMapper(getStore());
    return productMapper;
  }

  public MagentoProductService getProductService() {
    if (productService == null)
      productService = new MagentoProductService(this);
    return productService;
  }

  public AttributeMapper getAttributeMapper() {
    if (attributeMapper == null)
      attributeMapper = new AttributeMapper(getStore());
    return attributeMapper;
  }

  public MagentoAttributeService getAttributeService() {
    if (attributeService == null)
      attributeService = new MagentoAttributeService(this);
    return attributeService;
  }

  public AttributeSetMapper getAttributeSetMapper() {
    if (attributeSetMapper == null)
      attributeSetMapper = new AttributeSetMapper(getStore());
    return attributeSetMapper;
  }

  public MagentoAttributeSetService getAttributeSetService() {
    if (attributeSetService == null)
      attributeSetService = new MagentoAttributeSetService(this);
    return attributeSetService;
  }

  public BPGroupMapper getBpGroupMapper() {
    if (bpGroupMapper == null)
      bpGroupMapper = new BPGroupMapper(getStore());
    return bpGroupMapper;
  }

  public MagentoBPGroupService getBpGroupService() {
    if (bpGroupService == null)
      bpGroupService = new MagentoBPGroupService(this);
    return bpGroupService;
  }

  public CustomerMapper getCustomerMapper() {
    if (customerMapper == null)
      customerMapper = new CustomerMapper(getStore());
    return customerMapper;
  }

  public MagentoCustomerService getCustomerService() {
    if (customerService == null)
      customerService = new MagentoCustomerService(this);
    return customerService;
  }

  public ProductCategoryMapper getProductCategoryMapper() {
    if (productCategoryMapper == null)
      productCategoryMapper = new ProductCategoryMapper(getStore());
    return productCategoryMapper;
  }

  public MagentoProductCategoryService getProductCategoryService() {
    if (productCategoryService == null)
      productCategoryService = new MagentoProductCategoryService(this);
    return productCategoryService;
  }

  public MagentoAddressService getAddressService() {
    if (addressService == null)
      addressService = new MagentoAddressService(this);
    return addressService;
  }

  public SalesOrderMapper getSalesOrderMapper() {
    if (salesOrderMapper == null)
      salesOrderMapper = new SalesOrderMapper(getStore());
    return salesOrderMapper;
  }

  public SalesOrderLineMapper getSalesOrderLineMapper() {
    if (salesOrderLineMapper == null)
      salesOrderLineMapper = new SalesOrderLineMapper(getStore());
    return salesOrderLineMapper;
  }

  public MagentoSalesOrderService getSalesOrderService() {
    if (salesOrderService == null)
      salesOrderService = new MagentoSalesOrderService(this);
    return salesOrderService;
  }

  public MagentoShippingService getShippingService() {
    if (magentoShippingService == null)
      magentoShippingService = new MagentoShippingService(this);
    return magentoShippingService;
  }

  public MagentoInvoiceService getInvoiceService() {
    if (magentoInvoiceService == null)
      magentoInvoiceService = new MagentoInvoiceService(this);
    return magentoInvoiceService;
  }

  public void debug(String string) {
    debugDate.setTime(System.currentTimeMillis());
    debugStringBuilder.setLength(0);
    debugStringBuilder.append("[").append(sdf.format(debugDate)).append("] - MGTO - ")
        .append(string);
    System.out.println(debugStringBuilder);
  }

  public void debug(Throwable throwable) {
    debugErrortringBuilder.setLength(0);
    if (throwable != null) {
      if (throwable.getCause() != null)
        debug(throwable.getCause());
      debug(debugErrortringBuilder.append("Error:").append(throwable.getClass().getName())
          .append(" : ").append(throwable.getMessage()).toString());
    }
  }

  public <T extends BaseOBObject> T getNewOBInstance(Class<T> clazz, Organization org) {
    T obj = OBProvider.getInstance().get(clazz);

    if (obj instanceof ClientEnabled) {
      ((ClientEnabled) obj).setClient(getStore().getClient());
    }
    if (obj instanceof OrganizationEnabled) {
      // ((OrganizationEnabled) obj).setOrganization(getStore().getOrganization());
      ((OrganizationEnabled) obj).setOrganization(org);
    }
    if (obj instanceof ActiveEnabled) {
      ((ActiveEnabled) obj).setActive(true);
    }
    if (obj instanceof Traceable) {
      ((Traceable) obj).setCreatedBy(OBContext.getOBContext().getUser());
      ((Traceable) obj).setCreationDate(new Date());
      ((Traceable) obj).setUpdatedBy(OBContext.getOBContext().getUser());
      ((Traceable) obj).setUpdated(new Date());
    }
    return obj;
  }

  public <T extends BaseOBObject> T findOBObject(Class<T> clazz, Criterion criterion) {
    OBCriteria<T> criteria = OBDal.getInstance().createCriteria(clazz);
    criteria.add(criterion);

    try {
      return criteria.list().get(0);
    } catch (Exception e) {
      debugFindStringBuilder.setLength(0);
      debugFindStringBuilder.append(clazz.getName()).append(STR_FIND_OB_NOT_FOUND);
      debug(debugFindStringBuilder.toString());
      return null;
    }
  }

  public <T extends BaseOBObject> T findOBObject(Class<T> clazz, List<Criterion> criterions) {
    OBCriteria<T> criteria = OBDal.getInstance().createCriteria(clazz);
    for (Criterion criterion : criterions) {
      criteria.add(criterion);
    }
    try {
      return criteria.list().get(0);
    } catch (Exception e) {
      debugFindStringBuilder.setLength(0);
      debugFindStringBuilder.append(clazz.getName()).append(STR_FIND_OB_NOT_FOUND);
      debug(debugFindStringBuilder.toString());
      return null;
    }
  }

  public <T extends BaseOBObject> T findOBObject(Class<T> clazz, Map<String, Object> props) {
    debugFindStringBuilder.setLength(0);
    debugFindStringBuilder.append(STR_FIND_OB_LOOKING).append(clazz.getName());
    debug(debugFindStringBuilder.toString());
    List<Criterion> criterions = new ArrayList<Criterion>();
    for (Entry<String, Object> property : props.entrySet()) {
      debugFindStringBuilder.setLength(0);
      debugFindStringBuilder.append(property.getKey()).append(" : ").append(property.getValue());
      debug(debugFindStringBuilder.toString());
      criterions.add(Restrictions.eq(property.getKey(), property.getValue()));
    }
    return findOBObject(clazz, criterions);
  }

  public <T extends BaseOBObject> T findOBObject(Class<T> clazz, String property, Object value) {
    debugFindStringBuilder.setLength(0);
    debugFindStringBuilder.append(STR_FIND_OB_LOOKING).append(clazz.getName());
    debug(debugFindStringBuilder.toString());
    debugFindStringBuilder.setLength(0);
    debugFindStringBuilder.append(property).append(" : ").append(value);
    debug(debugFindStringBuilder.toString());
    List<Criterion> criterions = new ArrayList<Criterion>();
    criterions.add(Restrictions.eq(property, value));
    return findOBObject(clazz, criterions);
  }

  public Image getImageFromUrl(String strUrl, String fileName) {
    try {

      debug("Getting image from " + strUrl);

      URL url = new URL(strUrl);
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      String contentType = connection.getContentType();
      InputStream is = connection.getInputStream();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      byte[] b = new byte[2048];
      int length;

      while ((length = is.read(b)) != -1) {
        baos.write(b, 0, length);
      }

      is.close();
      baos.close();
      ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());
      BufferedImage rImage = ImageIO.read(bis);
      Image productImage = getNewOBInstance(Image.class, getStore().getOrganization());
      productImage.setName(fileName);
      productImage.setBindaryData(baos.toByteArray());
      productImage.setWidth(new Long(rImage.getWidth()));
      productImage.setHeight(new Long(rImage.getHeight()));
      productImage.setMimetype(contentType);
      return productImage;

    } catch (Exception e) {
      debug("Getting image from " + strUrl);
      debug(e);
      return null;
    }

  }

  public Region getMagentoRegion(String countryCode, String regionName) {
    debug("looking for region " + countryCode + " - " + regionName);
    if (regionCache.get(countryCode) == null) {
      Map<String, Region> regionMap = new HashMap<String, Region>();
      try {
        for (Region region : getRemoteServiceFactory().getRegionRemoteService().list(countryCode))
          regionMap.put(region.getName(), region);
      } catch (ServiceException e) {
        e.printStackTrace();
      }
      regionCache.put(countryCode, regionMap);
    }
    return regionCache.get(countryCode).get(regionName);
  }
}
