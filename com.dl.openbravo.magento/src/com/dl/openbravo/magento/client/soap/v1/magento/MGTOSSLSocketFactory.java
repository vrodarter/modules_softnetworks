package com.dl.openbravo.magento.client.soap.v1.magento;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.HttpClientError;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.SecureProtocolSocketFactory;

/**
 * 52 *
 * <p>
 * 53 * EasySSLProtocolSocketFactory can be used to creats SSL {@link Socket}s 54 * that accept
 * self-signed certificates. 55 *
 * </p>
 * 56 *
 * <p>
 * 57 * This socket factory SHOULD NOT be used for productive systems 58 * due to security reasons,
 * unless it is a concious decision and 59 * you are perfectly aware of security implications of
 * accepting 60 * self-signed certificates 61 *
 * </p>
 * 62 * 63 *
 * <p>
 * 64 * Example of using custom protocol socket factory for a specific host: 65 *
 * 
 * <pre>
66       *     Protocol easyhttps = new Protocol("https", new EasySSLProtocolSocketFactory(), 443);
67       *
68       *     URI uri = new URI("https://localhost/", true);
69       *     // use relative url only
70       *     GetMethod httpget = new GetMethod(uri.getPathQuery());
71       *     HostConfiguration hc = new HostConfiguration();
72       *     hc.setHost(uri.getHost(), uri.getPort(), easyhttps);
73       *     HttpClient client = new HttpClient();
74       *     client.executeMethod(hc, httpget);
75       *
 * </pre>
 * 
 * 76 *
 * </p>
 * 77 *
 * <p>
 * 78 * Example of using custom protocol socket factory per default instead of the standard one: 79
 * *
 * 
 * <pre>
80       *     Protocol easyhttps = new Protocol("https", new EasySSLProtocolSocketFactory(), 443);
81       *     Protocol.registerProtocol("https", easyhttps);
82       *
83       *     HttpClient client = new HttpClient();
84       *     GetMethod httpget = new GetMethod("https://localhost/");
85       *     client.executeMethod(httpget);
86       *
 * </pre>
 * 
 * 87 *
 * </p>
 * 88 * 89 * @author <a href="mailto:oleg -at- ural.ru">Oleg Kalnichevski</a> 90 * 91 *
 * <p>
 * 92 * DISCLAIMER: HttpClient developers DO NOT actively support this component. 93 * The component
 * is provided as a reference material, which may be inappropriate 94 * for use without additional
 * customization. 95 *
 * </p>
 * 96
 */

public class MGTOSSLSocketFactory implements SecureProtocolSocketFactory {

  public static final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
    @Override
    public X509Certificate[] getAcceptedIssuers() {
      return null;
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType)
        throws CertificateException {
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType)
        throws CertificateException {

    }
  } };

  /** Log object for this class. */

  private SSLContext sslcontext = null;

  /**
   * 106 * Constructor for EasySSLProtocolSocketFactory. 107
   */
  public MGTOSSLSocketFactory() {
    super();
  }

  private static SSLContext createEasySSLContext() {
    try {
      SSLContext context = SSLContext.getInstance("SSL");
      context.init(null, trustAllCerts, null);
      return context;
    } catch (Exception e) {
      throw new HttpClientError(e.toString());
    }
  }

  private SSLContext getSSLContext() {
    if (this.sslcontext == null) {
      this.sslcontext = createEasySSLContext();
    }
    return this.sslcontext;
  }

  /**
   * 134 * @see
   * SecureProtocolSocketFactory#createSocket(java.lang.String,int,java.net.InetAddress,int) 135
   */
  public Socket createSocket(String host, int port, InetAddress clientHost, int clientPort)
      throws IOException, UnknownHostException {

    return getSSLContext().getSocketFactory().createSocket(host, port, clientHost, clientPort);
  }

  /**
   * 152 * Attempts to get a new socket connection to the given host within the given time limit.
   * 153 *
   * <p>
   * 154 * To circumvent the limitations of older JREs that do not support connect timeout a 155 *
   * controller thread is executed. The controller thread attempts to create a new socket 156 *
   * within the given limit of time. If socket constructor does not return until the 157 * timeout
   * expires, the controller terminates and throws an {@link ConnectTimeoutException} 158 *
   * </p>
   * 159 * 160 * @param host the host name/IP 161 * @param port the port on the host 162 * @param
   * clientHost the local host name/IP to bind the socket to 163 * @param clientPort the port on the
   * local machine 164 * @param params {@link HttpConnectionParams Http connection parameters} 165 *
   * 166 * @return Socket a new socket 167 * 168 * @throws IOException if an I/O error occurs while
   * creating the socket 169 * @throws UnknownHostException if the IP address of the host cannot be
   * 170 * determined 171
   */
  public Socket createSocket(final String host, final int port, final InetAddress localAddress,
      final int localPort, final HttpConnectionParams params)
      throws IOException, UnknownHostException, ConnectTimeoutException {
    if (params == null) {
      throw new IllegalArgumentException("Parameters may not be null");
    }
    int timeout = params.getConnectionTimeout();
    SocketFactory socketfactory = getSSLContext().getSocketFactory();
    if (timeout == 0) {
      return socketfactory.createSocket(host, port, localAddress, localPort);
    } else {
      Socket socket = socketfactory.createSocket();
      SocketAddress localaddr = new InetSocketAddress(localAddress, localPort);
      SocketAddress remoteaddr = new InetSocketAddress(host, port);
      socket.bind(localaddr);
      socket.connect(remoteaddr, timeout);
      return socket;
    }
  }

  /**
   * 197 * @see SecureProtocolSocketFactory#createSocket(java.lang.String,int) 198
   */
  public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
    return getSSLContext().getSocketFactory().createSocket(host, port);
  }

  /**
   * 208 * @see
   * SecureProtocolSocketFactory#createSocket(java.net.Socket,java.lang.String,int,boolean) 209
   */
  public Socket createSocket(Socket socket, String host, int port, boolean autoClose)
      throws IOException, UnknownHostException {
    return getSSLContext().getSocketFactory().createSocket(socket, host, port, autoClose);
  }

  public boolean equals(Object obj) {
    return ((obj != null) && obj.getClass().equals(MGTOSSLSocketFactory.class));
  }

  public int hashCode() {
    return MGTOSSLSocketFactory.class.hashCode();
  }

}
