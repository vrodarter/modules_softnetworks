/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.model.product;

public class ConfigurableDataException extends Exception {

	private static final long serialVersionUID = -8601203755742890161L;

	public ConfigurableDataException() {
		super();
	}

	public ConfigurableDataException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ConfigurableDataException(String arg0) {
		super(arg0);
	}

	public ConfigurableDataException(Throwable arg0) {
		super(arg0);
	}

}
