/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.service;

import java.io.Serializable;

import com.dl.openbravo.magento.client.soap.v1.model.BaseMagentoModel;
import com.dl.openbravo.magento.client.soap.v1.soap.MagentoSoapClient;

@SuppressWarnings("rawtypes")
public interface GeneralService<T extends BaseMagentoModel> extends Serializable {

	public abstract MagentoSoapClient getSoapClient();

	public abstract void setDebug(Boolean b);
}
