/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.service;

import java.util.HashMap;
import java.util.Map;

import com.dl.openbravo.magento.client.soap.v1.model.BaseMagentoModel;
import com.dl.openbravo.magento.client.soap.v1.soap.MagentoSoapClient;

@SuppressWarnings("rawtypes")
public abstract class GeneralServiceImpl<T extends BaseMagentoModel> implements GeneralService<T> {

  private static final long serialVersionUID = -7262909756654576277L;

  protected Boolean debug = true;

  protected MagentoSoapClient soapClient;

  protected Map<Object, T> _cache = new HashMap<Object, T>();

  public GeneralServiceImpl(MagentoSoapClient soapClient) {
    super();
    this.soapClient = soapClient;
  }

  @Override
  public MagentoSoapClient getSoapClient() {
    return soapClient;
  }

  /**
   * disable or enable debug informations
   *
   * @param Boolean
   * @return
   */
  public void setDebug(Boolean b) {
    this.debug = b;
  }

}
