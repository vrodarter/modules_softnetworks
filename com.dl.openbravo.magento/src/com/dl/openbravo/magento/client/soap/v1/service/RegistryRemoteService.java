/**
 * 
 */
package com.dl.openbravo.magento.client.soap.v1.service;

import com.dl.openbravo.magento.client.soap.v1.soap.MagentoSoapClient;

/**
 * Responsible for creating Remote Service instances.
 * @author ceefour
 */
public interface RegistryRemoteService {

	void setSoapClient(MagentoSoapClient instance);

}
