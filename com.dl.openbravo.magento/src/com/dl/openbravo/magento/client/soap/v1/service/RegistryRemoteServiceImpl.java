package com.dl.openbravo.magento.client.soap.v1.service;

import com.dl.openbravo.magento.client.soap.v1.soap.MagentoSoapClient;

/**
 * @author ceefour
 *
 */
public class RegistryRemoteServiceImpl implements RegistryRemoteService {

	private MagentoSoapClient soapClient;

	public RegistryRemoteServiceImpl(MagentoSoapClient soapClient) {
		super();
		this.soapClient = soapClient;
	}

	/**
	 * @return the soapClient
	 */
	public MagentoSoapClient getSoapClient() {
		return soapClient;
	}

	/**
	 * @param soapClient the soapClient to set
	 */
	public void setSoapClient(MagentoSoapClient soapClient) {
		this.soapClient = soapClient;
	}
	
}
