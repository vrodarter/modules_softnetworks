/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.service.category;

import java.util.List;

import com.dl.openbravo.magento.client.soap.v1.model.category.CategoryAttribute;
import com.dl.openbravo.magento.client.soap.v1.service.GeneralService;
import com.dl.openbravo.magento.client.soap.v1.service.ServiceException;

public interface CategoryAttributeRemoteService extends GeneralService<CategoryAttribute> {

	/**
	 * List all categories attributes from a specific store view
	 * @param storeView
	 * @return List<CategoryAttribute>
	 * @throws ServiceException
	 */
	public abstract List<CategoryAttribute> listAll(String storeView) throws ServiceException;



}
