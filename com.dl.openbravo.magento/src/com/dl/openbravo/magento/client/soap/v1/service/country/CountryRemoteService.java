/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.service.country;

import java.util.List;

import com.dl.openbravo.magento.client.soap.v1.model.country.Country;
import com.dl.openbravo.magento.client.soap.v1.service.GeneralService;
import com.dl.openbravo.magento.client.soap.v1.service.ServiceException;

public interface CountryRemoteService extends GeneralService<Country> {

	/**
	 * @return list of all countries
	 * @throws ServiceException
	 */
	public abstract List<Country> list() throws ServiceException;

	/**
	 * @return get country by name
	 * @throws ServiceException
	 */
	public abstract Country getCountryByName(String countryName) throws ServiceException;

	/**
	 * @return get country id by name
	 * @throws ServiceException
	 */
	public abstract String getCountryIdByName(String countryName) throws ServiceException;
}
