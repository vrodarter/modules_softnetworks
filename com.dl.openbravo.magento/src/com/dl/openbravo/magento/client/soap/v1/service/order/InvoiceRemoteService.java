/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.service.order;

import java.util.List;

import com.dl.openbravo.magento.client.soap.v1.model.order.Invoice;
import com.dl.openbravo.magento.client.soap.v1.service.GeneralService;
import com.dl.openbravo.magento.client.soap.v1.service.ServiceException;

public interface InvoiceRemoteService extends GeneralService<Invoice> {

    void save(Invoice shipment, String comment, Boolean email,
              Boolean includeComment) throws ServiceException;

    void addComment(Invoice invoice, String comment, Boolean email,
                    Boolean includeComment) throws ServiceException;

    Invoice getById(Integer id) throws ServiceException;

    List<Invoice> list(String filter) throws ServiceException;

    void capture(Invoice invoice)
            throws ServiceException;

    void voidInvoice(Invoice invoice)
                    throws ServiceException;

    void cancelInvoice(Invoice invoice)
                            throws ServiceException;
}
