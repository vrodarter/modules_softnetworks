/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.service.order;

import java.util.List;
import java.util.Map;

import com.dl.openbravo.magento.client.soap.v1.model.order.Filter;
import com.dl.openbravo.magento.client.soap.v1.model.order.Shipment;
import com.dl.openbravo.magento.client.soap.v1.model.order.ShipmentTrack;
import com.dl.openbravo.magento.client.soap.v1.service.GeneralService;
import com.dl.openbravo.magento.client.soap.v1.service.ServiceException;

public interface ShipmentRemoteService extends GeneralService<Shipment> {

	public abstract List<Shipment> list(Filter filter) throws ServiceException;

	public abstract Shipment getById(Integer id) throws ServiceException;

	public abstract void save(Shipment shipment, String comment, Boolean email, Boolean includeComment) throws ServiceException;

	public abstract void addComment(Shipment shipment, String comment, Boolean email, Boolean includeComment) throws ServiceException;

	public abstract void addTrack(Shipment shipment, ShipmentTrack track) throws ServiceException;

	public abstract void removeTrack(Shipment shipment, ShipmentTrack track) throws ServiceException;

	public abstract Map<String, String> getCarriers(Integer orderId) throws ServiceException;

	public abstract void sendInfo(com.dl.openbravo.magento.client.soap.v1.model.order.Shipment shipment, String comment) throws ServiceException;

}
