/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.service.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.axis2.AxisFault;

import com.dl.openbravo.magento.client.soap.v1.magento.ResourcePath;
import com.dl.openbravo.magento.client.soap.v1.model.product.Product;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttribute;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttributeSet;
import com.dl.openbravo.magento.client.soap.v1.service.GeneralServiceImpl;
import com.dl.openbravo.magento.client.soap.v1.service.ServiceException;
import com.dl.openbravo.magento.client.soap.v1.soap.MagentoSoapClient;

public class ProductAttributeRemoteServiceImpl extends GeneralServiceImpl<ProductAttribute>
    implements ProductAttributeRemoteService {

  private static final long serialVersionUID = -1575503087022556608L;

  public ProductAttributeRemoteServiceImpl(MagentoSoapClient soapClient) {
    super(soapClient);
  }

  /**
   * Create a object product attribute from the attributes map
   *
   * @param attributes
   * @return ProductAttribute
   */
  private ProductAttribute buildProductAttribute(Map<String, Object> attributes) {
    ProductAttribute pa = new ProductAttribute();

    for (Map.Entry<String, Object> attr : attributes.entrySet())
      pa.set(attr.getKey(), attr.getValue());

    return pa;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.google.code.magja.service.product.ProductAttributeRemoteService#delete
   * (java.lang.String)
   */
  @Override
  public void delete(String attributeName) throws ServiceException {

    try {
      if (!(Boolean) soapClient.callSingle(ResourcePath.ProductAttributeDelete, attributeName))
        throw new ServiceException("Error deleting product attribute.");
    } catch (AxisFault e) {
      if (debug)
        e.printStackTrace();
      throw new ServiceException(e.getMessage());
    }

  }

  /*
   * (non-Javadoc)
   *
   * @see com.google.code.magja.service.product.ProductAttributeRemoteService# getOptions
   * (com.google.code.magja.model.product.ProductAttribute)
   */
  @Override
  public void getOptions(ProductAttribute productAttribute) throws ServiceException {

    if (productAttribute.getId() == null && productAttribute.getCode() == null)
      throw new ServiceException("The id or the code of the attribute must have some value.");

    List<Map<String, Object>> options;
    try {
      options = (List<Map<String, Object>>) soapClient
          .callSingle(ResourcePath.ProductAttributeOptions, (productAttribute.getId() != null
              ? productAttribute.getId() : productAttribute.getCode()));
    } catch (AxisFault e) {
      if (debug)
        e.printStackTrace();
      throw new ServiceException(e.getMessage());
    }

    if (options != null) {
      for (Map<String, Object> option : options) {
        if (!"".equals((String) option.get("label"))) {
          if (productAttribute.getOptions() == null)
            productAttribute.setOptions(new HashMap<Object, Object>());
          productAttribute.getOptions().put(option.get("value"), (String) option.get("label"));
        }
      }
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see com.google.code.magja.service.product.ProductAttributeRemoteService#
   * listAllProductAttributeSet()
   */
  @Override
  public List<ProductAttributeSet> listAllProductAttributeSet() throws ServiceException {

    List<ProductAttributeSet> resultList = new ArrayList<ProductAttributeSet>();

    List<Map<String, Object>> attSetList;
    try {
      attSetList = (List<Map<String, Object>>) soapClient
          .callSingle(ResourcePath.ProductAttributeSetList, "");
    } catch (AxisFault e) {
      if (debug)
        e.printStackTrace();
      throw new ServiceException(e.getMessage());
    }

    if (attSetList == null)
      return resultList;

    for (Map<String, Object> att : attSetList) {
      ProductAttributeSet set = new ProductAttributeSet();
      for (Map.Entry<String, Object> attribute : att.entrySet())
        set.set(attribute.getKey(), attribute.getValue());
      resultList.add(set);
    }

    return resultList;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.google.code.magja.service.product.ProductAttributeRemoteService# listByAttributeSet
   * (com.google.code.magja.model.product.ProductAttributeSet)
   */
  @Override
  public List<ProductAttribute> listByAttributeSet(Integer setId) throws ServiceException {

    List<ProductAttribute> results = new ArrayList<ProductAttribute>();

    List<Map<String, Object>> prd_attributes = null;
    try {
      prd_attributes = soapClient.callSingle(ResourcePath.ProductAttributeList, setId);
    } catch (AxisFault e) {
      if (debug)
        e.printStackTrace();
      throw new ServiceException(e.getMessage());
    }

    if (prd_attributes == null)
      return results;

    for (Map<String, Object> att : prd_attributes) {
      ProductAttribute prd_attribute = new ProductAttribute();
      for (Map.Entry<String, Object> attribute : att.entrySet()) {
        if (!attribute.getKey().equals("scope"))
          prd_attribute.set(attribute.getKey(), attribute.getValue());
      }

      prd_attribute.setScope(ProductAttribute.Scope.getByName((String) att.get("scope")));

      results.add(prd_attribute);
    }

    return results;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.google.code.magja.service.product.ProductAttributeRemoteService# listByAttributeSet
   * (com.google.code.magja.model.product.ProductAttributeSet)
   */
  @Override
  public List<ProductAttribute> listByAttributeSet(ProductAttributeSet set)
      throws ServiceException {
    return listByAttributeSet(set.getId());
  }

  /*
   * (non-Javadoc)
   *
   * @see com.google.code.magja.service.product.ProductAttributeRemoteService# listAllAttributes()
   */
  private List<ProductAttribute> allProductAttributes;

  public List<ProductAttribute> listAllAttributes() throws ServiceException {
    if (allProductAttributes != null && !allProductAttributes.isEmpty())
      return allProductAttributes;
    allProductAttributes = new ArrayList<ProductAttribute>();

    try {
      List<ProductAttributeSet> allProductAttributeSets = listAllProductAttributeSet();
      for (ProductAttributeSet productAttributeSet : allProductAttributeSets) {
        List<ProductAttribute> listProductAttributes = listByAttributeSet(productAttributeSet);
        for (ProductAttribute productAttribute : listProductAttributes) {
          allProductAttributes.add(productAttribute);
        }
      }
    } catch (ServiceException e) {
      if (debug)
        e.printStackTrace();
      throw new ServiceException(e.getMessage());
    }

    return allProductAttributes;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.google.code.magja.service.product.ProductAttributeRemoteService#create(
   * com.google.code.magja.model.product.ProductAttribute)
   */
  @Override
  public void save(ProductAttribute productAttribute) throws ServiceException {
    /*
     * if (productAttribute.getId() != null || exists(productAttribute.getCode())) throw new
     * ServiceException(productAttribute.getCode() +
     * " exists already. Not allowed to update product attributes yet");
     */
    Integer id = null;
    try {
      id = Integer.parseInt("" + soapClient.callArgs(ResourcePath.ProductAttributeCreate,
          productAttribute.serializeToApi()));
      for (ProductAttributeSet pas : listAllProductAttributeSet()) {
        try {
          soapClient.callArgs(ResourcePath.ProductAttributeSetAttributeAdd,
              new Object[] { id, pas.getId(), });
        } catch (Exception e) {
        }
      }
      return;
    } catch (AxisFault e) {
      /*
       * if (debug) e.printStackTrace();
       */
      /* throw new ServiceException(e.getMessage()); */
      return;
    }

    /*
     * if (id == null) throw new ServiceException("Error on create product attribute");
     * 
     * productAttribute.setId(id);
     * 
     * // if has options, include this too if (productAttribute.getOptions() != null) { if
     * (!productAttribute.getOptions().isEmpty()) { String[] options = new
     * String[productAttribute.getOptions().size()]; int i = 0; for (Map.Entry<Object, Object>
     * option : productAttribute.getOptions().entrySet()) options[i++] =
     * option.getValue().toString();
     * 
     * // List<Object> params = new LinkedList<Object>(); // params.add(productAttribute.getId());
     * // params.add(options);
     * 
     * try { if (!(Boolean) soapClient.callArgs(ResourcePath.ProductAttributeAddOptions, new
     * Object[] { productAttribute.getId(), options })) throw new ServiceException(
     * "The product attribute was saved, but had error " + "on create the options for that"); }
     * catch (AxisFault e) { if (debug) e.printStackTrace(); throw new
     * ServiceException(e.getMessage()); } } }
     */
  }

  /*
   * (non-Javadoc)
   *
   * @seecom.google.code.magja.service.product.ProductAttributeRemoteService# addOptions(
   * com.google.code.magja.model.product.ProductAttribute , Map<Integer, String>)
   */
  @Override
  public void saveOptions(ProductAttribute productAttribute,
      Map<Object, Object> productAttributeOptions) throws ServiceException {
    // if has options, include this too
    if (productAttributeOptions != null) {
      if (!productAttributeOptions.isEmpty()) {
        String[] options = new String[productAttributeOptions.size()];
        int i = 0;
        for (Map.Entry<Object, Object> option : productAttributeOptions.entrySet())
          options[i++] = option.getValue().toString();

        // List<Object> params = new LinkedList<Object>();
        // params.add(productAttribute.getId());
        // params.add(options);

        try {
          if (!(Boolean) soapClient.callArgs(ResourcePath.ProductAttributeAddOptions,
              new Object[] { productAttribute.getId(), options }))
            throw new ServiceException("The product attribute was saved, but had error "
                + "on create the options for that");
        } catch (AxisFault e) {
          if (debug)
            e.printStackTrace();
          throw new ServiceException(e.getMessage());
        }
      }
    }
  }

  @Override
  public Integer AddOptions(String code, String option) throws ServiceException {
    // if has options, include this too

    // List<Object> params = new LinkedList<Object>();
    // params.add(code);
    // params.add(option);

    try {
      LinkedList list = (LinkedList) soapClient.callArgs(ResourcePath.ProductAttributeAdd,
          new Object[] { code, option });
      if (list.size() > 0) {
        Map id = (Map) list.get(0);
        if (id != null) {
          return new Integer((String) id.get("id"));
        }
      }
    } catch (AxisFault e) {
      if (debug)
        e.printStackTrace();
      throw new ServiceException(e.getMessage());
    }

    return null;
  }

  /*
   * (non-Javadoc)
   *
   * @seecom.google.code.magja.service.product.ProductAttributeRemoteService# exists(String)
   */
  public boolean exists(String attributeName) throws ServiceException {
    List<ProductAttribute> allProductAttributes = listAllAttributes();

    for (ProductAttribute productAttribute : allProductAttributes) {
      if (productAttribute.getCode().equals(attributeName)) {
        return true;
      }
    }

    return false;
  }

  /*
   * (non-Javadoc)
   *
   * @seecom.google.code.magja.service.product.ProductAttributeRemoteService# getByCode(String)
   */
  public ProductAttribute getByCode(String code) throws ServiceException {

    if (_cache.get(code) != null)
      return _cache.get(code);

    Map<String, Object> remote_result = null;
    try {
      remote_result = (Map<String, Object>) soapClient.callSingle(ResourcePath.ProductAttributeInfo,
          code);

    } catch (AxisFault e) {
      if (debug)
        e.printStackTrace();
      throw new ServiceException(e.getMessage());
    }

    if (remote_result == null)
      return null;
    else {
      ProductAttribute p = buildProductAttribute(remote_result);
      _cache.put(code, p);
      return p;
    }
  }

  /*
   * (non-Javadoc)
   *
   * @seecom.google.code.magja.service.product.ProductAttributeRemoteService# getById(Integer)
   */
  public ProductAttribute getById(Integer id) throws ServiceException {

    if (_cache.get(id) != null)
      return _cache.get(id);

    Map<String, Object> remote_result = null;
    try {
      remote_result = (Map<String, Object>) soapClient.callSingle(ResourcePath.ProductAttributeInfo,
          id);

    } catch (AxisFault e) {
      if (debug)
        e.printStackTrace();
      throw new ServiceException(e.getMessage());
    }

    if (remote_result == null)
      return null;
    else {
      ProductAttribute p = buildProductAttribute(remote_result);
      _cache.put(id, p);
      return p;
    }
  }

  /*
   * (non-Javadoc)
   *
   * @seecom.google.code.magja.service.product.ProductAttributeRemoteService#
   * addOption(ProductAttribute, String)
   */
  public void addOption(ProductAttribute productAttribute, Object option) throws ServiceException {
    // create ProductAttribute option
    Map<Object, Object> productAttributeOption = new HashMap<Object, Object>();
    productAttributeOption.put(0, option);

    // add options to ProductAttribute
    productAttribute.setOptions(productAttributeOption);

    // create ProductAttribute
    saveOptions(productAttribute, productAttributeOption);
  }
}
