/**
 * @author andre
 *
 */
package com.dl.openbravo.magento.client.soap.v1.service.region;

import java.util.List;

import com.dl.openbravo.magento.client.soap.v1.model.region.Region;
import com.dl.openbravo.magento.client.soap.v1.service.GeneralService;
import com.dl.openbravo.magento.client.soap.v1.service.ServiceException;

public interface RegionRemoteService extends GeneralService<Region> {

	/**
	 * @param countryId
	 * @return list of all regions of the specified country id
	 * @throws ServiceException
	 */
	public abstract List<Region> list(String countryId) throws ServiceException;

}
