package com.dl.openbravo.magento.mapping;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.dl.openbravo.magento.client.soap.v1.model.BaseMagentoModel;
import com.dl.openbravo.magento.data.DLMGTOStore;

public abstract class AbstractMapper {

  private DLMGTOStore store;

  public AbstractMapper(DLMGTOStore store) {
    super();
    this.store = store;
  }

  public abstract List<ItemMap> getMagentoToOBMap();

  public abstract List<ItemMap> getOBToMagentoMap();

  public void mapMagentoToOB(Object mgtoObject, Object obObject) {
    mapProperties(getMagentoToOBMap(), mgtoObject, obObject);
  }

  public void mapOBToMagento(Object obObject, Object mgtoObject) {
    mapProperties(getOBToMagentoMap(), obObject, mgtoObject);
  }

  @SuppressWarnings("rawtypes")
  public void mapProperties(List<ItemMap> map, Object src, Object target) {
    Object value = null;
    if (src instanceof BaseMagentoModel) {
      mapInternalMagetoObject((BaseMagentoModel) src, target);
    }
    if (target instanceof BaseMagentoModel) {
      mapInternalOBObject(src, (BaseMagentoModel) target);
    }
    for (ItemMap propMap : map) {
      try {
        if (propMap.getTargetType() == null || propMap.getSrcType() == null
            || propMap.getSrcName() == null || propMap.getSrcName().trim().isEmpty()
            || propMap.getTargetName() == null || propMap.getTargetName().trim().isEmpty())
          continue;

        System.out.println("Mapping: " + propMap.getSrcName() + " -> " + propMap.getTargetName());

        try {
          try {
            value = BeanUtils.getProperty(src, propMap.getSrcName());
          } catch (Exception e) {
            if (src instanceof BaseMagentoModel)
              value = ((BaseMagentoModel) src).getAllProperties().get(propMap.getSrcName());
          }
        } catch (Exception e) {
          System.out.println("Error getting: " + propMap.getSrcName());
        }

        System.out.println("Src Value: " + value);
        try {
          if (value == null && src instanceof BaseMagentoModel) {
            value = ((BaseMagentoModel) src).getAllProperties().get(propMap.getSrcName());
            if (value == null)
              for (Object key : ((BaseMagentoModel) src).getAllProperties().keySet()) {
                String srcPropKey = key.toString().replaceAll("_", "").toUpperCase();
                String propKey = propMap.getSrcName().replaceAll("_", "").toUpperCase();
                if (srcPropKey.equals(propKey))
                  value = ((BaseMagentoModel) src).getAllProperties().get(key);
              }
          }
        } catch (Exception e1) {
        }

        if (value != null) {
          value = value.toString();
          if (propMap.getTargetType().equals(BigDecimal.class))
            value = new BigDecimal(value.toString());
          else if (propMap.getTargetType().equals(BigInteger.class))
            value = new BigInteger(value.toString());
          else if (propMap.getTargetType().equals(Long.class))
            value = Long.valueOf(value.toString());
          else if (propMap.getTargetType().equals(Boolean.class))
            value = Boolean.valueOf(value.toString());
          else if (propMap.getTargetType().equals(String.class)) {
            if (value.toString().length() > propMap.getMaxLenth())
              value = value.toString().substring(0, (propMap.getMaxLenth() - 1));
            else
              value = value.toString();
          } else if (propMap.getTargetType().equals(Date.class)) {
            value = new SimpleDateFormat(propMap.getFormat()).parse(value.toString());
          }
        }
        if (value != null) {
          if (target instanceof BaseMagentoModel) {

            ((BaseMagentoModel) target).set(propMap.getTargetName(), value);

          } else
            BeanUtils.setProperty(target, propMap.getTargetName(), value);

        }

      } catch (Exception e) {
        System.out
            .println("Error mapping " + propMap.getSrcName() + " to " + propMap.getTargetName()
                + " (" + value + ") : " + e.getClass() + " - " + e.getMessage());
        e.printStackTrace();
      }
    }
  }

  @SuppressWarnings("rawtypes")
  private void mapInternalMagetoObject(BaseMagentoModel src, Object target) {

    try {
      for (Object obj : src.getAllProperties().keySet()) {
        String key = obj.toString().replaceAll("_", "").toUpperCase();
        for (Method setter : target.getClass().getMethods()) {
          if (!setter.getName().toUpperCase().startsWith("SET"))
            continue;
          
          if (setter.getName().toUpperCase().endsWith("DLMGTO" + key)) {
            Object[] val = new Object[1];
            Class<?> type = setter.getParameterTypes()[0];
            Object value = src.getAllProperties().get(obj);
            if (type.equals(BigDecimal.class))
              value = new BigDecimal(value.toString());
            else if (type.equals(BigInteger.class))
              value = new BigInteger(value.toString());
            else if (type.equals(Long.class))
              value = Long.valueOf(value.toString());
            else if (type.equals(Boolean.class))
              value = Boolean.valueOf(value.toString());
            else if (type.equals(String.class)) {
              if (value.toString().length() > 512)
                value = value.toString().substring(0, (512 - 1));
              else
                value = value.toString();
            } // else if (type.equals(Date.class)) {
              // value = new SimpleDateFormat(propMap.getFormat()).parse(value.toString());
            // }
            val[0] = value;
            try {
             if(key!=null && key.toUpperCase().endsWith("STATUS") && (val[0] == null || (!val[0].toString().trim().equals("1") && !val[0].toString().trim().equals("2") )))
val[0]="1";    
              setter.invoke(target, val);
            } catch (Exception ex) {
            }
          }

        }

      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

  }

  @SuppressWarnings({ "rawtypes" })
  private void mapInternalOBObject(Object src, BaseMagentoModel target) {

    try {

      for (Method getter : src.getClass().getMethods()) {
        if ((getter.getName().toUpperCase().startsWith("GETDLMGTO")
            || getter.getName().toUpperCase().startsWith("ISDLMGTO"))
            && (!getter.getName().toUpperCase().endsWith("LIST")
                && !getter.getName().toUpperCase().endsWith("STORE"))) {

          String propName = null;
          if (getter.getName().toUpperCase().startsWith("GETDLMGTO"))
            propName = getter.getName().substring(9).replaceAll("([a-z])([A-Z]+)", "$1_$2")
                .toLowerCase().replaceAll("_list", "").replaceAll("_idlist", "");
          else
            propName = getter.getName().substring(8).replaceAll("([a-z])([A-Z]+)", "$1_$2")
                .toLowerCase().replaceAll("_list", "").replaceAll("_idlist", "");
          try {
            Object val = getter.invoke(src, (Object[]) null);
            if (val != null && !val.toString().equals("[]") && !val.toString().isEmpty()) {
              target.set(propName, val);
            }

          } catch (Exception e) {
          }
        }
      }
    } catch (Exception e) {
    }

  }

  public DLMGTOStore getStore() {
    return store;
  }

}
