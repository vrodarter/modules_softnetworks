package com.dl.openbravo.magento.mapping;

import java.util.ArrayList;
import java.util.List;

import com.dl.openbravo.magento.data.DLMGTOAttributeMap;
import com.dl.openbravo.magento.data.DLMGTOStore;

public class AttributeMapper extends AbstractMapper {

  private List<ItemMap> magentoToOBMap;

  private List<ItemMap> oBToMagentoMap;

  public AttributeMapper(DLMGTOStore store) {
    super(store);
  }

  @Override
  public List<ItemMap> getMagentoToOBMap() {
    if (magentoToOBMap == null) {
      magentoToOBMap = new ArrayList<ItemMap>();
      for (DLMGTOAttributeMap m : getStore().getDlmgtoAttributeMapList()) {
        magentoToOBMap.add(new ItemMap(m.getMgname(), m.getObname(),
            m.getMaxSize() != null ? m.getMaxSize().intValue() : 60, m.getMgtoClassName(),
            m.getClassName()));
      }
    }
    return magentoToOBMap;
  }

  @Override
  public List<ItemMap> getOBToMagentoMap() {
    if (oBToMagentoMap == null) {
      oBToMagentoMap = new ArrayList<ItemMap>();
      for (DLMGTOAttributeMap m : getStore().getDlmgtoAttributeMapList())
        oBToMagentoMap.add(new ItemMap(m.getObname(), m.getMgname(),
            m.getMgtoMaxSize() != null ? m.getMaxSize().intValue() : 60, m.getClassName(),
            m.getMgtoClassName()));
    }
    return oBToMagentoMap;
  }

}
