package com.dl.openbravo.magento.mapping;

import java.util.ArrayList;
import java.util.List;

import com.dl.openbravo.magento.data.DLMGTOBPGroupMap;
import com.dl.openbravo.magento.data.DLMGTOStore;

public class BPGroupMapper extends AbstractMapper {

  private List<ItemMap> magentoToOBMap;

  private List<ItemMap> oBToMagentoMap;

  public BPGroupMapper(DLMGTOStore store) {
    super(store);
  }

  @Override
  public List<ItemMap> getMagentoToOBMap() {
    if (magentoToOBMap == null) {
      magentoToOBMap = new ArrayList<ItemMap>();
      for (DLMGTOBPGroupMap m : getStore().getDlmgtoBpGroupMapList()) {
        magentoToOBMap.add(new ItemMap(m.getMgname(), m.getObname(),
            m.getMaxSize() != null ? m.getMaxSize().intValue() : 60, m.getMgtoClassName(),
            m.getClassName()));
      }
    }
    return magentoToOBMap;
  }

  @Override
  public List<ItemMap> getOBToMagentoMap() {
    if (oBToMagentoMap == null) {
      oBToMagentoMap = new ArrayList<ItemMap>();
      for (DLMGTOBPGroupMap m : getStore().getDlmgtoBpGroupMapList())
        oBToMagentoMap.add(new ItemMap(m.getObname(), m.getMgname(),
            m.getMgtoMaxSize() != null ? m.getMaxSize().intValue() : 60, m.getClassName(),
            m.getMgtoClassName()));
    }
    return oBToMagentoMap;
  }

}
