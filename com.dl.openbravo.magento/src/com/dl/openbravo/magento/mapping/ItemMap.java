package com.dl.openbravo.magento.mapping;

import java.io.Serializable;

public class ItemMap implements Serializable {

  private static final long serialVersionUID = 1L;

  private String srcName;

  private String targetName;

  private int maxLenth;

  private String format;

  private Class<?> srcType;

  private Class<?> targetType;

  private String srcTypeName;

  private String targetTypeName;

  public ItemMap(String srcName, String targetName, int maxLenth, String srcTypeName,
      String targetTypeName) {
    super();
    this.srcName = srcName;
    this.targetName = targetName;
    this.maxLenth = maxLenth;
    this.srcTypeName = srcTypeName;
    this.targetTypeName = targetTypeName;
  }

  public String getSrcName() {
    return srcName;
  }

  public String getTargetName() {
    return targetName;
  }

  public int getMaxLenth() {
    return maxLenth;
  }

  public String getFormat() {
    return format;
  }

  public Class<?> getSrcType() {
    if (srcType == null) {
      try {
        srcType = Class.forName(srcTypeName);
      } catch (Exception e) {
      }
    }
    return srcType;
  }

  public Class<?> getTargetType() {
    if (targetType == null) {
      try {
        targetType = Class.forName(targetTypeName);
      } catch (Exception e) {
      }
    }
    return targetType;
  }

}
