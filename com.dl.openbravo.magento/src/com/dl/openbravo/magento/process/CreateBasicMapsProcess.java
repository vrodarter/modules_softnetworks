package com.dl.openbravo.magento.process;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.plm.Attribute;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import com.dl.openbravo.magento.client.soap.v1.model.customer.Customer;
import com.dl.openbravo.magento.client.soap.v1.model.customer.CustomerGroup;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttribute;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttributeSet;
import com.dl.openbravo.magento.data.DLMGTOAttributeMap;
import com.dl.openbravo.magento.data.DLMGTOAttributeSetMap;
import com.dl.openbravo.magento.data.DLMGTOBPGroupMap;
import com.dl.openbravo.magento.data.DLMGTOBPartnerMap;
import com.dl.openbravo.magento.data.DLMGTOCategoryMap;
import com.dl.openbravo.magento.data.DLMGTOProductMap;
import com.dl.openbravo.magento.data.DLMGTOStore;

public class CreateBasicMapsProcess extends DalBaseProcess {

  private static final String STORE_ID_PARAM = "Dlmgto_Store_ID";

  private DLMGTOStore store;

  private List<String> avoidNameList;
  private List<Class<?>> allowClassList;

  @Override
  public void doExecute(ProcessBundle bundle) throws Exception {

    final OBError msg = new OBError();
    msg.setTitle("Create Basic Maps");
    String storeId = (String) bundle.getParams().get(STORE_ID_PARAM);
    store = OBDal.getInstance().get(DLMGTOStore.class, storeId);

    try {

      if (store.getDlmgtoAttributeMapList() == null || store.getDlmgtoAttributeMapList().isEmpty())
        createAttributeMap();

      // if(store.getDlmgtoAttributesetinsMapList()!=null &&
      // !store.getDlmgtoAttributesetinsMapList().isEmpty())
      // throw new Exception("Attribute Set Instance Map is not empty.");

      if (store.getDlmgtoAttributesetMapList() == null
          || store.getDlmgtoAttributesetMapList().isEmpty())
        createAttributesetMap();

      if (store.getDlmgtoBpartnerMapList() == null || store.getDlmgtoBpartnerMapList().isEmpty())
        createBPartnerMap();

      if (store.getDlmgtoProductMapList() == null || store.getDlmgtoProductMapList().isEmpty())
        createProductMap();

      if (store.getDlmgtoCategoryMapList() == null || store.getDlmgtoCategoryMapList().isEmpty())
        createCategoryMap();

      if (store.getDlmgtoPaymentmethodMapList() == null
          || store.getDlmgtoPaymentmethodMapList().isEmpty())
        createPaymentmethodMap();

      if (store.getDlmgtoBpGroupMapList() == null || store.getDlmgtoBpGroupMapList().isEmpty())
        createBPGroupMap();

      OBDal.getInstance().commitAndClose();

      msg.setType("Success");
      msg.setMessage("Success");

    } catch (Exception e) {
      e.printStackTrace(System.out);
      msg.setType("Error");
      msg.setMessage(e.getMessage());
    }
    bundle.setResult(msg);
  }

  private void createAttributeMap() throws Exception {
    for (PropertyDescriptor mgtoPd : PropertyUtils.getPropertyDescriptors(ProductAttribute.class)) {
      if (mgtoPd.getName().toUpperCase().endsWith("ID")
          || getAvoidNameList().contains(mgtoPd.getName().toUpperCase())
          || !getAllowClassList().contains(mgtoPd.getPropertyType()))
        continue;
      DLMGTOAttributeMap map = OBProvider.getInstance().get(DLMGTOAttributeMap.class);
      map.setClient(store.getClient());
      map.setOrganization(store.getOrganization());
      map.setCreatedBy(OBContext.getOBContext().getUser());
      map.setUpdatedBy(OBContext.getOBContext().getUser());
      map.setCreationDate(new Date());
      map.setUpdated(new Date());
      map.setMgname(mgtoPd.getName());
      map.setActive(true);
      map.setObname("");
      map.setDlmgtoStore(store);
      map.setMaxSize(60L);
      map.setMgtoMaxSize(60L);
      map.setMgtoClassName(mgtoPd.getPropertyType().getName());
      for (PropertyDescriptor obPd : PropertyUtils.getPropertyDescriptors(Attribute.class)) {
        if (obPd.getName().toLowerCase().equals(mgtoPd.getName().toLowerCase())) {
          map.setObname(obPd.getName());
          map.setClassName(obPd.getPropertyType().getName());
        }
      }
      OBDal.getInstance().save(map);
    }
  }

  private void createAttributesetMap() throws Exception {
    for (PropertyDescriptor mgtoPd : PropertyUtils
        .getPropertyDescriptors(ProductAttributeSet.class)) {
      if (mgtoPd.getName().toUpperCase().endsWith("ID")
          || getAvoidNameList().contains(mgtoPd.getName().toUpperCase())
          || !getAllowClassList().contains(mgtoPd.getPropertyType()))
        continue;
      DLMGTOAttributeSetMap map = OBProvider.getInstance().get(DLMGTOAttributeSetMap.class);
      map.setClient(store.getClient());
      map.setOrganization(store.getOrganization());
      map.setCreatedBy(OBContext.getOBContext().getUser());
      map.setUpdatedBy(OBContext.getOBContext().getUser());
      map.setCreationDate(new Date());
      map.setUpdated(new Date());
      map.setMgname(mgtoPd.getName());
      map.setActive(true);
      map.setObname("");
      map.setDlmgtoStore(store);
      map.setMaxSize(60L);
      map.setMgtoMaxSize(60L);
      map.setMgtoClassName(mgtoPd.getPropertyType().getName());
      for (PropertyDescriptor obPd : PropertyUtils.getPropertyDescriptors(AttributeSet.class)) {
        if (obPd.getName().toLowerCase().equals(mgtoPd.getName().toLowerCase())) {
          map.setObname(obPd.getName());
          map.setClassName(obPd.getPropertyType().getName());
        }
      }
      OBDal.getInstance().save(map);
    }
  }

  private void createBPartnerMap() throws Exception {
    for (PropertyDescriptor mgtoPd : PropertyUtils.getPropertyDescriptors(Customer.class)) {
      if (mgtoPd.getName().toUpperCase().endsWith("ID")
          || getAvoidNameList().contains(mgtoPd.getName().toUpperCase())
          || !getAllowClassList().contains(mgtoPd.getPropertyType()))
        continue;
      DLMGTOBPartnerMap map = OBProvider.getInstance().get(DLMGTOBPartnerMap.class);
      map.setClient(store.getClient());
      map.setOrganization(store.getOrganization());
      map.setCreatedBy(OBContext.getOBContext().getUser());
      map.setUpdatedBy(OBContext.getOBContext().getUser());
      map.setCreationDate(new Date());
      map.setUpdated(new Date());
      map.setMgname(mgtoPd.getName());
      map.setActive(true);
      map.setObname("");
      map.setDlmgtoStore(store);
      map.setMaxSize(60L);
      map.setMgtoMaxSize(60L);
      map.setMgtoClassName(mgtoPd.getPropertyType().getName());
      for (PropertyDescriptor obPd : PropertyUtils.getPropertyDescriptors(BusinessPartner.class)) {
        if (obPd.getName().toLowerCase().equals(mgtoPd.getName().toLowerCase())) {
          map.setObname(obPd.getName());
          map.setClassName(obPd.getPropertyType().getName());
        }
      }
      OBDal.getInstance().save(map);
    }
  }

  private void createBPGroupMap() throws Exception {
    for (PropertyDescriptor mgtoPd : PropertyUtils.getPropertyDescriptors(CustomerGroup.class)) {
      if (mgtoPd.getName().toUpperCase().endsWith("ID")
          || getAvoidNameList().contains(mgtoPd.getName().toUpperCase())
          || !getAllowClassList().contains(mgtoPd.getPropertyType()))
        continue;
      DLMGTOBPGroupMap map = OBProvider.getInstance().get(DLMGTOBPGroupMap.class);
      map.setClient(store.getClient());
      map.setOrganization(store.getOrganization());
      map.setCreatedBy(OBContext.getOBContext().getUser());
      map.setUpdatedBy(OBContext.getOBContext().getUser());
      map.setCreationDate(new Date());
      map.setUpdated(new Date());
      map.setMgname(mgtoPd.getName());
      map.setActive(true);
      map.setObname("");
      map.setDlmgtoStore(store);
      map.setMaxSize(60L);
      map.setMgtoMaxSize(60L);
      map.setMgtoClassName(mgtoPd.getPropertyType().getName());
      for (PropertyDescriptor obPd : PropertyUtils.getPropertyDescriptors(Category.class)) {
        if (obPd.getName().toLowerCase().equals(mgtoPd.getName().toLowerCase())) {
          map.setObname(obPd.getName());
          map.setClassName(obPd.getPropertyType().getName());
        }
      }
      OBDal.getInstance().save(map);
    }
  }

  private void createProductMap() throws Exception {
    for (PropertyDescriptor mgtoPd : PropertyUtils.getPropertyDescriptors(
        com.dl.openbravo.magento.client.soap.v1.model.product.Product.class)) {
      if (mgtoPd.getName().toUpperCase().endsWith("ID")
          || getAvoidNameList().contains(mgtoPd.getName().toUpperCase())
          || !getAllowClassList().contains(mgtoPd.getPropertyType()))
        continue;
      DLMGTOProductMap map = OBProvider.getInstance().get(DLMGTOProductMap.class);
      map.setClient(store.getClient());
      map.setOrganization(store.getOrganization());
      map.setCreatedBy(OBContext.getOBContext().getUser());
      map.setUpdatedBy(OBContext.getOBContext().getUser());
      map.setCreationDate(new Date());
      map.setUpdated(new Date());
      map.setMgname(mgtoPd.getName());
      map.setActive(true);
      map.setObname("");
      map.setDlmgtoStore(store);
      map.setMaxSize(60L);
      map.setMgtoMaxSize(60L);
      map.setMgtoClassName(mgtoPd.getPropertyType().getName());
      for (PropertyDescriptor obPd : PropertyUtils.getPropertyDescriptors(Product.class)) {
        if (obPd.getName().toLowerCase().equals(mgtoPd.getName().toLowerCase())) {
          map.setObname(obPd.getName());
          map.setClassName(obPd.getPropertyType().getName());
        }
      }
      OBDal.getInstance().save(map);
    }
  }

  private void createCategoryMap() throws Exception {
    for (PropertyDescriptor mgtoPd : PropertyUtils.getPropertyDescriptors(
        com.dl.openbravo.magento.client.soap.v1.model.category.Category.class)) {
      if (mgtoPd.getName().toUpperCase().endsWith("ID")
          || getAvoidNameList().contains(mgtoPd.getName().toUpperCase())
          || !getAllowClassList().contains(mgtoPd.getPropertyType()))
        continue;
      DLMGTOCategoryMap map = OBProvider.getInstance().get(DLMGTOCategoryMap.class);
      map.setClient(store.getClient());
      map.setOrganization(store.getOrganization());
      map.setCreatedBy(OBContext.getOBContext().getUser());
      map.setUpdatedBy(OBContext.getOBContext().getUser());
      map.setCreationDate(new Date());
      map.setUpdated(new Date());
      map.setMgname(mgtoPd.getName());
      map.setActive(true);
      map.setObname("");
      map.setDlmgtoStore(store);
      map.setMaxSize(60L);
      map.setMgtoMaxSize(60L);
      map.setMgtoClassName(mgtoPd.getPropertyType().getName());
      for (PropertyDescriptor obPd : PropertyUtils.getPropertyDescriptors(ProductCategory.class)) {
        if (obPd.getName().toLowerCase().equals(mgtoPd.getName().toLowerCase())) {
          map.setObname(obPd.getName());
          map.setClassName(obPd.getPropertyType().getName());
        }
      }
      OBDal.getInstance().save(map);
    }
  }

  private void createPaymentmethodMap() throws Exception {

  }

  public List<String> getAvoidNameList() {
    if (avoidNameList == null) {
      avoidNameList = new ArrayList<String>();
      avoidNameList.add("ID");
      avoidNameList.add("ALLPROPERTIES");
      avoidNameList.add("CLASS");
    }
    return avoidNameList;
  }

  public List<Class<?>> getAllowClassList() {
    if (allowClassList == null) {
      allowClassList = new ArrayList<Class<?>>();
      allowClassList.add(String.class);
      allowClassList.add(Date.class);
      allowClassList.add(Integer.class);
      allowClassList.add(Number.class);
      allowClassList.add(Double.class);
      allowClassList.add(Boolean.class);
    }
    return allowClassList;
  }

}
