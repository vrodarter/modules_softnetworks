package com.dl.openbravo.magento.process;

import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.data.DLMGTOStore;

public class PingProcess extends DalBaseProcess {

  private static final String STORE_ID_PARAM = "Dlmgto_Store_ID";

  @Override
  public void doExecute(ProcessBundle bundle) throws Exception {

    final OBError msg = new OBError();
    msg.setTitle("Ping");
    String storeId = (String) bundle.getParams().get(STORE_ID_PARAM);
    DLMGTOStore store = OBDal.getInstance().get(DLMGTOStore.class, storeId);

    try {

      MagentoContext context = new MagentoContext(store);
      context.getRemoteServiceFactory().getCategoryRemoteService().getDefaultParent().getName();
      msg.setType("Success");
      msg.setMessage("Success");
    } catch (Exception e) {
      msg.setType("Error");
      msg.setMessage(e.getMessage());
    }
    bundle.setResult(msg);
  }

}
