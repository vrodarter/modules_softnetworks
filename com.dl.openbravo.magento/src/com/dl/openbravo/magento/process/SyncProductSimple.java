package com.dl.openbravo.magento.process;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;
import org.openbravo.scheduling.ProcessBundle;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.data.DLMGTOStore;

public class SyncProductSimple extends SyncSimple {

  @Override
  public String getMProcessName() {
    return "SYNC PRODUCT SIMPLE ";
  }

  @Override
  public void sync(ProcessBundle bundle) throws Exception {

    for (String pkey : bundle.getParams().keySet()) {
      if (pkey.toUpperCase().contains("M_PRODUCT_ID")) {
        Product obProduct = OBDal.getInstance().get(Product.class, bundle.getParams().get(pkey));
        if (obProduct == null || obProduct.getDlmgtoStore() == null)
          throw new Exception("No magento product found");
        DLMGTOStore store = obProduct.getDlmgtoStore();
        debug(STR_CONNECTING + store.getName(), true);
        MagentoContext context = new MagentoContext(store);
        setCurrentMagentoContext(context);
        debug(STR_CONNECTED + store.getName(), true);
        syncMagentoProduct(obProduct);
        return;
      }
    }
    throw new Exception("Invalid parameters");
  }

  public void syncMagentoProduct(Product obProduct) {
    try {
      com.dl.openbravo.magento.client.soap.v1.model.product.Product product = getCurrentMagentoContext()
          .getProductService().getMagentoObjectFromOB(obProduct);

      obProduct.setDlmgtoReference(product.getId().toString());

      getCurrentMagentoContext().getProductService().saveProductInOB(obProduct);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug("ERROR While sync product " + obProduct.getSearchKey(), true);
      debug(e);
    }
  }
}
