package com.dl.openbravo.magento.process.sync;

import java.util.List;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.AttributeSet;

import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttributeSet;

public class SyncAttributeSets extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC ATTRIBUTE SETS";

  private int count = 0;
  private int index = 1;

  @Override
  public void sync() throws Exception {
    debug("Reading magento attribute sets......", false);
    List<ProductAttributeSet> attributeSets = getCurrentMagentoContext().getRemoteServiceFactory()
        .getProductAttributeRemoteService().listAllProductAttributeSet();
    count = attributeSets.size();
    debug("Count  " + count, false);
    for (ProductAttributeSet mgtoAttributeSet : attributeSets) {
      debug("Sync AttributeSet ( " + (index++) + " of " + count + " ) - Magento Ref = "
          + mgtoAttributeSet.getId(), false);
      syncOBAttributeSet(mgtoAttributeSet);
    }
  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncOBAttributeSet(ProductAttributeSet mgtoAttributeSet) {
    try {
      AttributeSet attributeSet = getCurrentMagentoContext().getAttributeSetService()
          .getOBObjectFromMagento(mgtoAttributeSet);
      getCurrentMagentoContext().getAttributeSetService().saveAttributeSetInOB(attributeSet);
      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug(e);
    }
  }
}
