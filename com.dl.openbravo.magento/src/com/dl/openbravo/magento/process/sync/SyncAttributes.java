package com.dl.openbravo.magento.process.sync;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Attribute;

import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttribute;

public class SyncAttributes extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC ATTRIBUTES";

  private int count = 0;
  private int index = 1;

  @Override
  public void sync() throws Exception {
    debug("Reading magento attributes ......", false);
    List<ProductAttribute> attributeList = getCurrentMagentoContext().getRemoteServiceFactory()
        .getProductAttributeRemoteService().listAllAttributes();
    Set<ProductAttribute> attributes = new HashSet<ProductAttribute>();
    attributes.addAll(attributeList);

    count = attributes.size();
    debug("Count  " + count, false);

    for (ProductAttribute mgtoAttribute : attributes) {
      debug("Sync Attribute ( " + (index++) + " of " + count + " ) - Magento Ref = "
          + mgtoAttribute.getId(), false);
      syncOBAttribute(mgtoAttribute);
    }

  }

  // private void syncTo() throws Exception {
  // localAttributeIds = new HashSet<String>();
  // debug("Reading openbravo attributes ......", false);
  // OBCriteria<Attribute> attributeCriteria = OBDal.getInstance().createCriteria(Attribute.class);
  // attributeCriteria.add(Restrictions.and(
  // Restrictions.eq(Attribute.PROPERTY_ORGANIZATION,
  // getCurrentMagentoContext().getStore().getOrganization()),
  // Restrictions.or(Restrictions.isNull(Attribute.PROPERTY_DLMGTOREFERENCE),
  // Restrictions.isEmpty(Attribute.PROPERTY_DLMGTOREFERENCE))));
  // for (Attribute attr : attributeCriteria.list())
  // localAttributeIds.add(attr.getId());
  // OBDal.getInstance().commitAndClose();
  // count = localAttributeIds.size();
  // debug("Count " + count, false);
  //
  // for (String attributeId : localAttributeIds) {
  // Attribute attribute = OBDal.getInstance().get(Attribute.class, attributeId);
  // debug("Sync Attribute ( " + (index++) + " of " + count + " ) - Name = " + attribute.getName(),
  // false);
  // syncMagentoAttribute(attribute);
  // }
  // }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncOBAttribute(ProductAttribute mgtoAttribute) {
    try {
      Attribute attribute = getCurrentMagentoContext().getAttributeService()
          .getOBObjectFromMagento(mgtoAttribute
      // getCurrentMagentoContext().getRemoteServiceFactory()
      // .getProductAttributeRemoteService().getById(mgtoAttribute.getId())
      );

      getCurrentMagentoContext().getAttributeService().saveAttributeInOB(attribute);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug(e);
    }
  }

}
