package com.dl.openbravo.magento.process.sync;

import java.util.List;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.Category;

import com.dl.openbravo.magento.client.soap.v1.model.customer.CustomerGroup;

public class SyncBPGroups extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC BP GROUPS";

  private int count = 0;
  private int index = 1;

  @Override
  public void sync() throws Exception {
    SyncMode syncMode = SyncMode
        .valueOf(getCurrentMagentoContext().getStore().getCustomerSyncMode());
    switch (syncMode) {
    case MAGENTO_OB:
      syncFrom();
      syncTo();
      break;
    case MGTO_TO_OB:
      syncFrom();
      break;
    case OB_MAGENTO:
      syncTo();
      syncFrom();
      break;
    case OB_TO_MGTO:
      syncTo();
      break;
    }
  }

  public void syncFrom() throws Exception {

    debug("Reading magento customer groups......", false);
    List<CustomerGroup> customerGroups = getCurrentMagentoContext().getRemoteServiceFactory()
        .getCustomerRemoteService().listGroups();
    count = customerGroups.size();
    debug("Count  " + count, false);
    for (CustomerGroup customerGroup : customerGroups) {
      debug("Sync BPGroup ( " + (index++) + " of " + count + " ) - Magento Ref = "
          + customerGroup.getId(), false);
      syncOBBPGroup(customerGroup);
    }

  }

  public void syncTo() throws Exception {

  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncOBBPGroup(CustomerGroup customerGroup) {
    try {
      Category category = getCurrentMagentoContext().getBpGroupService()
          .getOBObjectFromMagento(customerGroup);

      getCurrentMagentoContext().getBpGroupService().saveBPGroupInOB(category);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug(e);
    }
  }

}
