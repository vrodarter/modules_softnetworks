package com.dl.openbravo.magento.process.sync;

import java.util.List;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;

import com.dl.openbravo.magento.client.soap.v1.model.customer.Customer;

public class SyncCustomers extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC CUSTOMERS";

  private int count = 0;
  private int index = 1;

  @Override
  public void sync() throws Exception {
    SyncMode syncMode = SyncMode
        .valueOf(getCurrentMagentoContext().getStore().getCustomerSyncMode());
    switch (syncMode) {
    case MAGENTO_OB:
      syncFrom();
      syncTo();
      break;
    case MGTO_TO_OB:
      syncFrom();
      break;
    case OB_MAGENTO:
      syncTo();
      syncFrom();
      break;
    case OB_TO_MGTO:
      syncTo();
      break;
    }
  }

  public void syncFrom() throws Exception {

    debug("Reading magento customers......", false);
    List<Customer> customers = getCurrentMagentoContext().getRemoteServiceFactory()
        .getCustomerRemoteService().list();
    count = customers.size();
    debug("Count  " + count, false);
    for (Customer mgtoCustomer : customers) {
      debug("Sync Customer ( " + (index++) + " of " + count + " ) - Magento Ref = "
          + mgtoCustomer.getId(), false);
      syncOBBPartner(mgtoCustomer);
    }

  }

  public void syncTo() throws Exception {

  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncOBBPartner(Customer mgtoCustomer) {
    try {

      BusinessPartner customer = getCurrentMagentoContext().getCustomerService()
          .getOBObjectFromMagento(mgtoCustomer);

      getCurrentMagentoContext().getCustomerService().saveCustomerInOB(customer);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug("ERROR While sync customer " + mgtoCustomer.getId(), true);
      debug(e);
    }
  }
}
