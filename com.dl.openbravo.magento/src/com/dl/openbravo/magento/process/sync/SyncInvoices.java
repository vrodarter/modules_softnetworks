package com.dl.openbravo.magento.process.sync;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;

public class SyncInvoices extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC SHIPMENTS";

  private Set<String> localInvoicesIds;

  private int count = 0;
  private int index = 1;

  @Override
  public void sync() throws Exception {
    syncTo();
  }

  public void syncTo() throws Exception {
    localInvoicesIds = new HashSet<String>();
    debug("Reading openbravo invoices ......", false);
    OBCriteria<Invoice> productCriteria = OBDal.getInstance().createCriteria(Invoice.class);
    productCriteria.add(Restrictions.isNull(Invoice.PROPERTY_DLMGTOSTORE));
    for (Invoice prod : productCriteria.list()) {
      if (prod.getSalesOrder() != null && prod.getSalesOrder().getDlmgtoStore() != null
          && prod.isPaymentComplete())
        localInvoicesIds.add(prod.getId());
    }

    OBDal.getInstance().commitAndClose();

    count = localInvoicesIds.size();
    debug("Count invoices " + count, false);

    for (String packingId : localInvoicesIds) {
      Invoice invoice = OBDal.getInstance().get(Invoice.class, packingId);
      debug("Sync simple invoice ( " + (index++) + " of " + count + " )", false);
      syncMagentoInvoice(invoice);
    }

  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncMagentoInvoice(Invoice obInvoice) {
    try {

      com.dl.openbravo.magento.client.soap.v1.model.order.Invoice mgtoInvoice = getCurrentMagentoContext()
          .getInvoiceService().getMagentoObjectFromOB(obInvoice);

      obInvoice.setDlmgtoReference(mgtoInvoice.getId().toString());
      obInvoice.setDlmgtoStore(getCurrentMagentoContext().getStore());

      getCurrentMagentoContext().getInvoiceService().saveInvoiceInOB(obInvoice);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug("ERROR While sync packing " + obInvoice.getId(), true);
      debug(e);
    }
  }

}
