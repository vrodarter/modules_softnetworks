package com.dl.openbravo.magento.process.sync;

public enum SyncMode {
  MGTO_TO_OB, OB_TO_MGTO, MAGENTO_OB, OB_MAGENTO
}
