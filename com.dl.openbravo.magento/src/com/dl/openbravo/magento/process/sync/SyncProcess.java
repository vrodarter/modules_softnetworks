package com.dl.openbravo.magento.process.sync;

import java.sql.BatchUpdateException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.data.DLMGTOStore;

public abstract class SyncProcess extends DalBaseProcess {

  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

  private static final String STR_SUCCESS = "Success";
  private static final String STR_ERROR = "Error";
  private static final String STR_ERROR_1 = "ERROR : ";
  private static final String STR_CONNECTING = "Connecting to store";
  private static final String STR_CONNECTED = "Connected to store";
  private static final String STR_BO = "[";
  private static final String STR_BC = "]";
  private static final String STR_SCORE = " - ";
  private static final String STR_COLON = " : ";

  private static boolean SYNC_RUNNIG = false;

  private final StringBuilder logSb = new StringBuilder();

  private final StringBuilder logErrorSb = new StringBuilder();

  private final Date logDate = new Date();

  private ProcessLogger logger;

  private MagentoContext currentMagentoContext;

  private ProcessBundle bundle;

  @Override
  protected void doExecute(ProcessBundle pbundle) throws Exception {
    if (SYNC_RUNNIG)
      throw new Exception("Another sync process is running");
    SYNC_RUNNIG = true;
    final OBError msg = new OBError();
    msg.setTitle(getMProcessName());
    this.logger = pbundle.getLogger();
    this.bundle = pbundle;
    OBContext.setAdminMode();
    try {
      for (DLMGTOStore store : OBDal.getInstance().createCriteria(DLMGTOStore.class).list()) {
        debug(STR_CONNECTING + store.getName(), true);
        MagentoContext context = new MagentoContext(store);
        setCurrentMagentoContext(context);
        debug(STR_CONNECTED + store.getName(), true);
        sync();
      }
      debug(STR_SUCCESS, true);
      msg.setType(STR_SUCCESS);
      msg.setMessage(STR_SUCCESS);
      SYNC_RUNNIG = false;
    } catch (Exception e) {
      e.printStackTrace();
      debug(e);
      msg.setType(STR_ERROR);
      msg.setMessage(e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
      SYNC_RUNNIG = false;
    }
    bundle.setResult(msg);
  }

  public abstract String getMProcessName();

  public abstract void sync() throws Exception;

  public void debug(String string, boolean obLogger) {
    logSb.setLength(0);
    logDate.setTime(System.currentTimeMillis());
    logSb.append(STR_BO).append(sdf.format(logDate)).append(STR_BC).append(STR_SCORE)
        .append(getMProcessName()).append(STR_SCORE).append(string);
    System.out.println(logSb);
    if (obLogger)
      logger.logln(logSb.toString());
  }

  public void debug(Throwable throwable) {
    logErrorSb.setLength(0);
    if (throwable != null) {
      if (throwable.getCause() != null)
        debug(throwable.getCause());
      debug(logErrorSb.append(STR_ERROR_1).append(throwable.getClass().getName()).append(STR_COLON)
          .append(throwable.getMessage()).toString(), true);
    }
    if (throwable instanceof BatchUpdateException)
      debug(((BatchUpdateException) throwable).getNextException());

  }

  public ProcessLogger getLogger() {
    return logger;
  }

  public void setLogger(ProcessLogger logger) {
    this.logger = logger;
  }

  public MagentoContext getCurrentMagentoContext() {
    return currentMagentoContext;
  }

  public void setCurrentMagentoContext(MagentoContext currentMagentoContext) {
    this.currentMagentoContext = currentMagentoContext;
  }
}
