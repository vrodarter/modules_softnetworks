package com.dl.openbravo.magento.process.sync;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.model.common.plm.ProductCategory;

import com.dl.openbravo.magento.client.soap.v1.model.category.Category;

public class SyncProductCategories extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC PRODUCT CATEGORIES";

  private List<String> localProductCategoryIds;

  @Override
  public void sync() throws Exception {
    SyncMode syncMode = SyncMode
        .valueOf(getCurrentMagentoContext().getStore().getProductSyncMode());
    switch (syncMode) {
    case MAGENTO_OB:
      syncFrom();
      syncTo();
      break;
    case MGTO_TO_OB:
      syncFrom();
      break;
    case OB_MAGENTO:
      syncTo();
      syncFrom();
      break;
    case OB_TO_MGTO:
      syncTo();
      break;
    }
  }

  public void syncFrom() throws Exception {

    debug("Reading category tree...", false);
    Category parent = getCurrentMagentoContext().getRemoteServiceFactory()
        .getCategoryRemoteService().getDefaultParent();

    parent = getCurrentMagentoContext().getRemoteServiceFactory().getCategoryRemoteService()
        .getTree(parent.getId());

    if (parent != null && parent.getChildren() != null)
      syncOBCategory(parent, null);
  }

  public void syncTo() throws Exception {

    debug("Reading openbravo product categories ......", false);

    Criterion rootCategoryFilter = Restrictions.and(
        Restrictions.eq(ProductCategory.PROPERTY_ORGANIZATION,
            getCurrentMagentoContext().getStore().getOrganization()),
        Restrictions.eq(ProductCategory.PROPERTY_DLMGTOREFERENCE, Long.valueOf(
            getCurrentMagentoContext().getSoapConfig().getDefaultRootCategoryId().toString())));

    ProductCategory root = getCurrentMagentoContext().findOBObject(ProductCategory.class,
        rootCategoryFilter);

    if (root == null)
      throw new Exception("No root categrory found");

    localProductCategoryIds = getCurrentMagentoContext().getProductCategoryService()
        .readObCategoryIdTree(root, null, true, false);

    OBDal.getInstance().commitAndClose();

    for (String categoryId : localProductCategoryIds) {
      ProductCategory category = OBDal.getInstance().get(ProductCategory.class, categoryId);
      debug("Sync product categroy - Name = " + category.getName(), false);
      syncMagentoCategory(category);
    }

  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncOBCategory(Category category, TreeNode parent) {

    debug("Sync OBCategory - Magento Ref = " + category.getId() + " " + category.getName(), false);
    ProductCategory productCategory = null;
    try {

      productCategory = getCurrentMagentoContext().getProductCategoryService()
          .getOBObjectFromMagento(category);
      OBDal.getInstance().save(productCategory);
      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug(e);
      return;
    }
    TreeNode currentNode = getCurrentMagentoContext().findOBObject(TreeNode.class,
        TreeNode.PROPERTY_NODE, productCategory.getId());
    if (parent != null) {
      currentNode.setReportSet(parent.getNode());
      try {
        OBDal.getInstance().save(currentNode);
        OBDal.getInstance().flush();
        OBDal.getInstance().commitAndClose();
      } catch (Exception e) {
        OBDal.getInstance().rollbackAndClose();
        e.printStackTrace();
        debug(e);
      }
    }
    if (category.getChildren() != null && category.getChildren().size() > 0)
      for (Category child : category.getChildren())
        syncOBCategory(child, currentNode);
  }

  public void syncMagentoCategory(ProductCategory obCategory) {
    try {
      Category category = getCurrentMagentoContext().getProductCategoryService()
          .getMagentoObjectFromOB(obCategory);
      obCategory.setDlmgtoReference(category.getId().toString());
      OBDal.getInstance().save(obCategory);
      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug(e);
    }
  }

}
