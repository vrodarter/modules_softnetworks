package com.dl.openbravo.magento.process.sync;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;

public class SyncProducts extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC PRODUCTS";

  private int count = 0;
  private int index = 1;

  private Set<String> localSimpleProductIds = new HashSet<String>();
  private Set<String> localConfigurableProductIds = new HashSet<String>();

  @Override
  public void sync() throws Exception {
    SyncMode syncMode = SyncMode
        .valueOf(getCurrentMagentoContext().getStore().getProductSyncMode());
    switch (syncMode) {
    case MAGENTO_OB:
      syncFrom();
      syncTo();
      break;
    case MGTO_TO_OB:
      syncFrom();
      break;
    case OB_MAGENTO:
      syncTo();
      syncFrom();
      break;
    case OB_TO_MGTO:
      syncTo();
      break;
    }
  }

  public void syncFrom() throws Exception {

    debug("Reading Magento products ...", false);

    List<com.dl.openbravo.magento.client.soap.v1.model.product.Product> mgtoProducts = getCurrentMagentoContext()
        .getRemoteServiceFactory().getProductRemoteService().listAllNoDep();

    count = mgtoProducts.size();
    debug("Count  " + mgtoProducts.size(), false);

    for (com.dl.openbravo.magento.client.soap.v1.model.product.Product mgtoProduct : mgtoProducts) {
      debug("Sync Product ( " + (index++) + " of " + count + " ) - Magento Ref = "
          + mgtoProduct.getId(), false);
      syncOBProduct(mgtoProduct);
    }

  }

  public void syncTo() throws Exception {
    localSimpleProductIds = new HashSet<String>();
    debug("Reading openbravo products ......", false);
    OBCriteria<Product> productCriteria = OBDal.getInstance().createCriteria(Product.class);
    productCriteria
        .add(Restrictions.eq(Product.PROPERTY_DLMGTOSTORE, getCurrentMagentoContext().getStore()));
    for (Product prod : productCriteria.list())
      if ((getCurrentMagentoContext().getStore().getDISProduct() == null
          || !getCurrentMagentoContext().getStore().getDISProduct().getId().equals(prod.getId()))
          && (getCurrentMagentoContext().getStore().getSAHProduct() == null
              || !getCurrentMagentoContext().getStore().getSAHProduct().getId()
                  .equals(prod.getId()))) {
        if (prod.getDlmgtoProductConfigList() == null
            || prod.getDlmgtoProductConfigList().isEmpty())
          localSimpleProductIds.add(prod.getId());
        else
          localConfigurableProductIds.add(prod.getId());
      }

    OBDal.getInstance().commitAndClose();

    count = localSimpleProductIds.size();
    debug("Count Simple " + count, false);

    for (String productId : localSimpleProductIds) {
      Product product = OBDal.getInstance().get(Product.class, productId);
      debug("Sync simple product ( " + (index++) + " of " + count + " ) - Name = "
          + product.getName(), false);
      syncMagentoProduct(product);
    }

    count = localConfigurableProductIds.size();
    index = 1;
    debug("Count Config. " + count, false);

    for (String productId : localConfigurableProductIds) {
      Product product = OBDal.getInstance().get(Product.class, productId);
      debug("Sync configurable product ( " + (index++) + " of " + count + " ) - Name = "
          + product.getName(), false);
      syncMagentoProduct(product);
    }
  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncOBProduct(
      com.dl.openbravo.magento.client.soap.v1.model.product.Product _product) {
    try {
     // com.dl.openbravo.magento.client.soap.v1.model.product.Product mgtoProduct = getCurrentMagentoContext()
     //     .getRemoteServiceFactory().getProductRemoteService().getById(_product.getId());

      Product product = getCurrentMagentoContext().getProductService()
          .getOBObjectFromMagento(_product);

      getCurrentMagentoContext().getProductService().saveProductInOB(product);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      e.printStackTrace(System.out);
      debug("ERROR While sync product " + _product.getId(), true);
      debug(e);
    }
  }

  public void syncMagentoProduct(Product obProduct) {
    try {
      com.dl.openbravo.magento.client.soap.v1.model.product.Product product = getCurrentMagentoContext()
          .getProductService().getMagentoObjectFromOB(obProduct);

      obProduct.setDlmgtoReference(product.getId().toString());

      getCurrentMagentoContext().getProductService().saveProductInOB(obProduct);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug("ERROR While sync product " + obProduct.getSearchKey(), true);
      debug(e);
    }
  }

}
