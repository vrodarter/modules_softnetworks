package com.dl.openbravo.magento.process.sync;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;

public class SyncProductsStock extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC PRODUCTS";

  private int count = 0;
  private int index = 1;

  private Set<String> localProductIds = new HashSet<String>();

  @Override
  public void sync() throws Exception {
    syncTo();
  }

  public void syncTo() throws Exception {
    localProductIds = new HashSet<String>();
    debug("Reading openbravo products ......", false);
    OBCriteria<Product> productCriteria = OBDal.getInstance().createCriteria(Product.class);
    productCriteria
        .add(Restrictions.and(
            Restrictions.gt(Product.PROPERTY_CREATIONDATE,
                getCurrentMagentoContext().getStore().getUpdateFrom()),
            Restrictions.and(
                Restrictions.eq(Product.PROPERTY_DLMGTOSTORE,
                    getCurrentMagentoContext().getStore()),
                Restrictions.isNotNull(Product.PROPERTY_DLMGTOREFERENCE))));
    for (Product prod : productCriteria.list())
      if ((getCurrentMagentoContext().getStore().getDISProduct() == null
          || !getCurrentMagentoContext().getStore().getDISProduct().getId().equals(prod.getId()))
          && (getCurrentMagentoContext().getStore().getSAHProduct() == null
              || !getCurrentMagentoContext().getStore().getSAHProduct().getId()
                  .equals(prod.getId())))
        localProductIds.add(prod.getId());
    OBDal.getInstance().commitAndClose();
    count = localProductIds.size();
    debug("Count " + count, false);

    for (String productId : localProductIds) {
      Product product = OBDal.getInstance().get(Product.class, productId);
      debug("Sync product ( " + (index++) + " of " + count + " ) - Name = " + product.getName(),
          false);
      syncMagentoProductStock(product);
    }
  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncMagentoProductStock(Product obProduct) {
    try {

      getCurrentMagentoContext().getProductService().updateStock(obProduct);
      getCurrentMagentoContext().getProductService().saveProductInOB(obProduct);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug("ERROR While sync product " + obProduct.getSearchKey(), true);
      debug(e);
    }
  }
}
