package com.dl.openbravo.magento.process.sync;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

import com.dl.openbravo.magento.client.soap.v1.model.order.Filter;
import com.dl.openbravo.magento.client.soap.v1.model.order.FilterItem;

public class SyncSalesOrders extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC SALES ORDERS";

  private int count = 0;
  private int index = 1;

  @Override
  public void sync() throws Exception {

    debug("Reading orders......", false);

    FilterItem orderFilterItem = new FilterItem("status", "like", "pending%");
    FilterItem storeFilterItem = new FilterItem("store_name", "like",
        "%" + getCurrentMagentoContext().getStore().getName());
    List<FilterItem> orderFilterItems = new ArrayList<FilterItem>();
    orderFilterItems.add(orderFilterItem);
    orderFilterItems.add(storeFilterItem);
    Filter orderFilter = new Filter();
    orderFilter.setItems(orderFilterItems);

    List<com.dl.openbravo.magento.client.soap.v1.model.order.Order> orders = getCurrentMagentoContext()
        .getRemoteServiceFactory().getOrderRemoteService().list(orderFilter);

    count = orders.size();
    debug("Count  " + count, false);

    for (com.dl.openbravo.magento.client.soap.v1.model.order.Order mgtoOrder : orders) {
      debug("Sync Order ( " + (index++) + " of " + count + " - Magento Ref = "
          + mgtoOrder.getOrderNumber(), false);
      syncOBSalesOrder(mgtoOrder);
    }

  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncOBSalesOrder(com.dl.openbravo.magento.client.soap.v1.model.order.Order _order) {

    try {
      com.dl.openbravo.magento.client.soap.v1.model.order.Order mgtoOrder = getCurrentMagentoContext()
          .getRemoteServiceFactory().getOrderRemoteService().getById(_order.getOrderNumber());

      Order salesOrder = getCurrentMagentoContext().getSalesOrderService()
          .getOBObjectFromMagento(mgtoOrder);

      getCurrentMagentoContext().getSalesOrderService().saveSalesOrderInOB(salesOrder);

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();

      if (getCurrentMagentoContext().getStore().isOrderComplete()) {
        getCurrentMagentoContext().getSalesOrderService().completeOrder(salesOrder);
        OBDal.getInstance().flush();
        OBDal.getInstance().commitAndClose();
      }

      getCurrentMagentoContext().getSalesOrderService().setOrderToProcessing(
          mgtoOrder.getOrderNumber(), "OB DOC NUMBER:" + salesOrder.getDocumentNo());

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug("ERROR While sync order " + _order.getOrderNumber(), true);
      debug(e);
      try {
        getCurrentMagentoContext().getSalesOrderService().setOrderToHold(_order.getOrderNumber(),
            e.getMessage());
      } catch (Exception e1) {
        debug("ERROR While holding order " + _order.getOrderNumber(), true);
        debug(e);
      }
    }
  }
}
