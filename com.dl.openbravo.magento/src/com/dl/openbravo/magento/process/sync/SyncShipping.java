package com.dl.openbravo.magento.process.sync;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.packing.PackingBox;

import com.dl.openbravo.magento.client.soap.v1.model.order.Shipment;

public class SyncShipping extends SyncProcess {

  private static final String M_PROCESS_NAME = "SYNC SHIPMENTS";

  private Set<String> localShipmentsIds;

  private int count = 0;
  private int index = 1;

  @Override
  public void sync() throws Exception {
    syncTo();
  }

  public void syncTo() throws Exception {
    localShipmentsIds = new HashSet<String>();
    debug("Reading openbravo packings ......", false);
    OBCriteria<Packing> productCriteria = OBDal.getInstance().createCriteria(Packing.class);
    productCriteria.add(Restrictions.isNull(Packing.PROPERTY_DLMGTOSTORE));
    for (Packing prod : productCriteria.list()) {
      if (!prod.getOBWPACKBoxList().isEmpty())
        localShipmentsIds.add(prod.getId());
    }

    OBDal.getInstance().commitAndClose();

    count = localShipmentsIds.size();
    debug("Count packing " + count, false);

    for (String packingId : localShipmentsIds) {
      Packing packing = OBDal.getInstance().get(Packing.class, packingId);
      debug("Sync simple packing ( " + (index++) + " of " + count + " )", false);
      syncMagentoShipment(packing);
    }

  }

  @Override
  public String getMProcessName() {
    return M_PROCESS_NAME;
  }

  public void syncMagentoShipment(Packing obPacking) {
    try {
      for (PackingBox box : obPacking.getOBWPACKBoxList()) {
        if (box.getGoodsShipment() == null || box.getGoodsShipment().getSalesOrder() == null
            || box.getGoodsShipment().getSalesOrder().getDlmgtoReference() == null
            || box.getTrackingNo() == null || box.getTrackingNo().isEmpty())
          continue;

        Shipment shipment = getCurrentMagentoContext().getShippingService()
            .getMagentoObjectFromOB(box);

        obPacking.setDlmgtoReference(shipment.getId().toString());
        obPacking.setDlmgtoStore(getCurrentMagentoContext().getStore());

        getCurrentMagentoContext().getShippingService().savePackingInOB(obPacking);

      }
      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug("ERROR While sync packing " + obPacking.getId(), true);
      debug(e);
    }
  }

}
