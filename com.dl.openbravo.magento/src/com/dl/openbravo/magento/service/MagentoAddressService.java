package com.dl.openbravo.magento.service;

import java.util.ArrayList;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.geography.City;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.geography.Region;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.address.Address;
import com.dl.openbravo.magento.client.soap.v1.model.customer.CustomerAddress;

@SuppressWarnings("rawtypes")
public class MagentoAddressService extends MagentoService<Location, Address> {

  public MagentoAddressService(MagentoContext magentoContext) {
    super(magentoContext, Location.class, Address.class);
  }

  @Override
  public Location getOBObjectFromMagento(Address magentoAddress) throws Exception {
    return null;
  }

  public Location getAddressFromMagento(BusinessPartner customer, Address<?> magentoAddress)
      throws Exception {
    Location location = null;
    for (Location l : customer.getBusinessPartnerLocationList())
      if (l.getDlmgtoReference() != null && magentoAddress.getId() != null
          && l.getDlmgtoReference().equals(Long.valueOf(magentoAddress.getId()).toString()))
        location = l;

    if (location == null) {
      location = getMagentoContext().getNewOBInstance(Location.class,
          getMagentoContext().getStore().getCustomerOrg());
      location.setDlmgtoReference(Long.valueOf(magentoAddress.getId()).toString());
      location.setDlmgtoStore(getMagentoContext().getStore());
      location.setBusinessPartner(customer);
      customer.getBusinessPartnerLocationList().add(location);
    }

    String locationName = magentoAddress.getCompany();

    if (locationName == null || locationName.trim().isEmpty())
      locationName = magentoAddress.getFirstName();

    if (locationName == null || locationName.trim().isEmpty())
      locationName = customer.getName();
    else
      locationName += (magentoAddress.getLastName() != null) ? (" " + magentoAddress.getLastName())
          : "";

    locationName = locationName.trim();

    location.setName(locationName);
    location.setPhone(magentoAddress.getTelephone());
    location.setFax(magentoAddress.getFax());
    location.setLocationAddress(getGeoLocationFromMagento(magentoAddress));
    if (magentoAddress instanceof CustomerAddress) {
      location.setShipToAddress(((CustomerAddress) magentoAddress).getDefaultShipping());
      location.setInvoiceToAddress(((CustomerAddress) magentoAddress).getDefaultBilling());
    }

    return location;
  }

  public User getContactFromMagento(BusinessPartner customer, Address<?> magentoAddress)
      throws Exception {
    User contact = null;
    for (User u : customer.getADUserList())
      if (u.getDlmgtoReference() != null
          && u.getDlmgtoReference().equals(Long.valueOf(magentoAddress.getId())))
        contact = u;
    if (contact == null) {
      contact = getMagentoContext().getNewOBInstance(User.class,
          getMagentoContext().getStore().getCustomerOrg());
      contact.setDlmgtoReference(Long.valueOf(magentoAddress.getId()).toString());
      contact.setDlmgtoStore(getMagentoContext().getStore());
      contact.setBusinessPartner(customer);
      customer.getADUserList().add(contact);
    }
    contact.setName(customer.getName());
    contact.setFirstName(magentoAddress.getFirstName());
    contact.setLastName(magentoAddress.getLastName());
    contact.setPhone(magentoAddress.getTelephone());
    contact.setFax(magentoAddress.getFax());
    return contact;
  }

  public org.openbravo.model.common.geography.Location getGeoLocationFromMagento(
      Address<?> magentoAddress) throws Exception {
    org.openbravo.model.common.geography.Location geoLocation = getMagentoContext()
        .getNewOBInstance(org.openbravo.model.common.geography.Location.class,
            getMagentoContext().getStore().getCustomerOrg());

    Country country = getMagentoContext().findOBObject(Country.class,
        Country.PROPERTY_ISOCOUNTRYCODE, magentoAddress.getCountryCode());

    if (country == null)
      throw new Exception("Invalid country code " + magentoAddress.getCountryCode());

    if (magentoAddress.getRegion() == null)
      throw new Exception("Invalid region");

    if (magentoAddress.getCity() == null)
      throw new Exception("Invalid city");

    geoLocation.setCountry(country);
    geoLocation.setAddressLine1(magentoAddress.getStreet());
    geoLocation.setPostalCode(magentoAddress.getPostCode());
    geoLocation.setRegionName(magentoAddress.getRegion());
    geoLocation.setCityName(magentoAddress.getCity());

    if (!getMagentoContext().getStore().isRegionCityNames()) {
      Region region = null;
      com.dl.openbravo.magento.client.soap.v1.model.region.Region magentoRegion = getMagentoContext()
          .getMagentoRegion(magentoAddress.getCountryCode(), magentoAddress.getRegion());
      if (magentoRegion == null)
        throw new Exception("Invalid region " + magentoAddress.getRegion());
      for (Region r : country.getRegionList())
        if (r.getName().equalsIgnoreCase(magentoRegion.getCode()))
          region = r;
      if (region == null) {
        region = getMagentoContext().getNewOBInstance(Region.class,
            getMagentoContext().getStore().getCustomerOrg());
        region.setSearchKey(magentoRegion.getCode());
        region.setSearchKey(magentoRegion.getName());
        region.setDescription(magentoAddress.getRegion());
        region.setCountry(country);
        region.setCityList(new ArrayList<City>());
      }

      City city = null;

      for (City c : region.getCityList())
        if (c.getName().equalsIgnoreCase(magentoAddress.getCity()))
          city = c;

      if (city == null) {
        city = getMagentoContext().getNewOBInstance(City.class,
            getMagentoContext().getStore().getCustomerOrg());
        city.setCountry(country);
        city.setRegion(region);
        if (magentoAddress.getCity().length() > 60)
          city.setName(magentoAddress.getCity().substring(0, 59));
        else
          city.setName(magentoAddress.getCity());
      }
      geoLocation.setRegion(region);
      geoLocation.setCity(city);

    }
    return geoLocation;
  }

  public User saveContactInOB(User user) throws Exception {
    OBDal.getInstance().save(user);
    return user;
  }

  public Location saveLocationInOB(Location location) throws Exception {
    if (location.getLocationAddress() != null) {
      saveGeoLocationInOB(location.getLocationAddress());
    }
    OBDal.getInstance().save(location);
    return location;
  }

  public org.openbravo.model.common.geography.Location saveGeoLocationInOB(
      org.openbravo.model.common.geography.Location location) throws Exception {

    if (location.getRegion() != null)
      OBDal.getInstance().save(location.getRegion());
    if (location.getCity() != null)
      OBDal.getInstance().save(location.getCity());

    OBDal.getInstance().save(location);

    return location;
  }

}
