package com.dl.openbravo.magento.service;

import java.util.ArrayList;
import java.util.Map.Entry;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Attribute;
import org.openbravo.model.common.plm.AttributeValue;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttribute;

public class MagentoAttributeService extends MagentoService<Attribute, ProductAttribute> {

  public MagentoAttributeService(MagentoContext magentoContext) {
    super(magentoContext, Attribute.class, ProductAttribute.class);
  }

  @Override
  public Attribute getOBObjectFromMagento(ProductAttribute mgtoAttribute) throws Exception {
    Attribute attribute = getMagentoContext().findOBObject(Attribute.class,
        Attribute.PROPERTY_DLMGTOREFERENCE,
        Long.valueOf(mgtoAttribute.getId().longValue()).toString());

    if (attribute == null) {
      debug(STR_CREATE);
      attribute = getMagentoContext().getNewOBInstance(Attribute.class,
          getMagentoContext().getStore().getProductOrg());
    } else {
      debug(STR_UPDATE);
    }

    attribute.setDlmgtoReference(Long.valueOf(mgtoAttribute.getId()).toString());
    attribute.setDlmgtoStore(getMagentoContext().getStore());
    attribute.setName(mgtoAttribute.getCode());
    attribute.setMandatory(false);
    getMagentoContext().getAttributeMapper().mapMagentoToOB(mgtoAttribute, attribute);

    if (attribute.getAttributeValueList() == null)
      attribute.setAttributeValueList(new ArrayList<AttributeValue>());

    try {
      getMagentoContext().getRemoteServiceFactory().getProductAttributeRemoteService()
          .getOptions(mgtoAttribute);
      ;
    } catch (Exception e) {
      debug("Error reading options:" + e.getMessage());
    }

    if (mgtoAttribute.getOptions() != null)
      for (Entry<Object, Object> option : mgtoAttribute.getOptions().entrySet()) {
        AttributeValue attributeValue = null;

        String searchKey = (option.getValue().toString().length() > 40)
            ? option.getValue().toString().substring(0, 39) : option.getValue().toString();
        debug("reading option " + option.getValue());
        for (AttributeValue av : attribute.getAttributeValueList())
          if (av.getSearchKey().equalsIgnoreCase(searchKey))
            attributeValue = av;

        if (attributeValue == null) {
          attributeValue = getMagentoContext().getNewOBInstance(AttributeValue.class,
              getMagentoContext().getStore().getProductOrg());
          attribute.getAttributeValueList().add(attributeValue);
          attributeValue.setAttribute(attribute);
        }

        if (option.getValue().toString().length() > 60)
          attributeValue.setName(option.getValue().toString().substring(0, 59));
        else
          attributeValue.setName(option.getValue().toString());
        attributeValue.setDescription(option.getValue().toString());
        attributeValue.setSearchKey(searchKey);
      }

    attribute.setList(mgtoAttribute.getOptions() != null && mgtoAttribute.getOptions().size() > 0);
    return attribute;
  }

  public Attribute saveAttributeInOB(Attribute attribute) throws Exception {

    OBDal.getInstance().save(attribute);

    for (AttributeValue attributeValue : attribute.getAttributeValueList())
      OBDal.getInstance().save(attributeValue);

    return attribute;
  }

}
