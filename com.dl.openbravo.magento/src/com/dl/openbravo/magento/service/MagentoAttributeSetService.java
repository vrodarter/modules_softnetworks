package com.dl.openbravo.magento.service;

import java.util.ArrayList;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Attribute;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.AttributeUse;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttribute;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttributeSet;

public class MagentoAttributeSetService extends MagentoService<AttributeSet, ProductAttributeSet> {

  public MagentoAttributeSetService(MagentoContext magentoContext) {
    super(magentoContext, AttributeSet.class, ProductAttributeSet.class);
  }

  @Override
  public AttributeSet getOBObjectFromMagento(ProductAttributeSet mgtoAttributeSet)
      throws Exception {

    Attribute sn = getMagentoContext().findOBObject(Attribute.class, Attribute.PROPERTY_NAME,
        "MGTOSN");
    if (sn == null) {
      sn = getMagentoContext().getNewOBInstance(Attribute.class,
          getMagentoContext().getStore().getProductOrg());
      sn.setName("MGTOSN");
      sn.setDescription("Serial Number");

    }

    AttributeSet attributeSet = getMagentoContext().findOBObject(AttributeSet.class,
        AttributeSet.PROPERTY_DLMGTOREFERENCE,
        Long.valueOf(mgtoAttributeSet.getId().longValue()).toString());

    if (attributeSet == null) {
      debug(STR_CREATE);
      attributeSet = getMagentoContext().getNewOBInstance(AttributeSet.class,
          getMagentoContext().getStore().getProductOrg());
      attributeSet.setSerialNo(true);
      attributeSet.setAttributeUseList(new ArrayList<AttributeUse>());

      AttributeUse attributeUse = getMagentoContext().getNewOBInstance(AttributeUse.class,
          getMagentoContext().getStore().getProductOrg());
      attributeSet.getAttributeUseList().add(attributeUse);
      attributeUse.setSequenceNumber(0l);
      attributeUse.setAttribute(sn);
      attributeUse.setAttributeSet(attributeSet);
    } else {
      debug(STR_UPDATE);
    }

    attributeSet.setDlmgtoReference(Long.valueOf(mgtoAttributeSet.getId()).toString());
    attributeSet.setDlmgtoStore(getMagentoContext().getStore());
    attributeSet.setName(mgtoAttributeSet.getName());
    getMagentoContext().getAttributeSetMapper().mapMagentoToOB(mgtoAttributeSet, attributeSet);

    attributeSet.setRequireAtLeastOneValue(false);

    long seq = attributeSet.getAttributeUseList().size() * 10l + 10l;

    for (ProductAttribute pa : getMagentoContext().getRemoteServiceFactory()
        .getProductAttributeRemoteService().listByAttributeSet(mgtoAttributeSet)) {
      Attribute attribute = getMagentoContext().findOBObject(Attribute.class,
          Attribute.PROPERTY_DLMGTOREFERENCE, Long.valueOf(pa.getId().toString()).toString());
      if (attribute == null && getMagentoContext().getStore().isForced()) {
        attribute = getMagentoContext().getAttributeService().getOBObjectFromMagento(pa);
      }
      if (attribute == null) {
        throw new Exception("Invalid attribute " + pa.getId().toString());
      }
      AttributeUse attributeUse = null;
      for (AttributeUse au : attributeSet.getAttributeUseList())
        if (au.getAttribute() != null && au.getAttribute().getId() != null
            && attribute.getId() != null && au.getAttribute().getId().equals(attribute.getId()))
          attributeUse = au;

      if (attributeUse == null) {
        attributeUse = getMagentoContext().getNewOBInstance(AttributeUse.class,
            getMagentoContext().getStore().getProductOrg());
        attributeSet.getAttributeUseList().add(attributeUse);
        attributeUse.setSequenceNumber(seq);
        seq += 10l;
      }

      attributeUse.setAttribute(attribute);
      attributeUse.setAttributeSet(attributeSet);
    }
    return attributeSet;
  }

  public AttributeSet saveAttributeSetInOB(AttributeSet attributeSet) throws Exception {

    for (AttributeUse attributeUse : attributeSet.getAttributeUseList()) {
      getMagentoContext().getAttributeService().saveAttributeInOB(attributeUse.getAttribute());
    }

    OBDal.getInstance().save(attributeSet);

    for (AttributeUse attributeUse : attributeSet.getAttributeUseList()) {
      OBDal.getInstance().save(attributeUse);
    }
    // https://svn.apache.org/repos/asf/ofbiz/tags/REL-4.0/applications/product/src/org/ofbiz/shipment/thirdparty/ups/UpsServices.java
    return attributeSet;
  }

}
