package com.dl.openbravo.magento.service;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.Category;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.customer.CustomerGroup;

public class MagentoBPGroupService extends MagentoService<Category, CustomerGroup> {

  public MagentoBPGroupService(MagentoContext magentoContext) {
    super(magentoContext, Category.class, CustomerGroup.class);
  }

  @Override
  public Category getOBObjectFromMagento(CustomerGroup customerGroup) throws Exception {

    Category category = getMagentoContext().findOBObject(Category.class,
        Category.PROPERTY_DLMGTOREFERENCE,
        Long.valueOf(customerGroup.getId().longValue()).toString());

    if (category == null) {
      debug(STR_CREATE);
      category = getMagentoContext().getNewOBInstance(Category.class,
          getMagentoContext().getStore().getCustomerOrg());
    } else {
      debug(STR_UPDATE);
    }

    category.setDlmgtoReference(Long.valueOf(customerGroup.getId()).toString());
    category.setDlmgtoStore(getMagentoContext().getStore());
    category.setName(customerGroup.getGroupCode());
    category.setSearchKey(customerGroup.getGroupCode());

    getMagentoContext().getBpGroupMapper().mapMagentoToOB(customerGroup, category);

    return category;
  }

  public Category saveBPGroupInOB(Category catgeory) throws Exception {

    try{
    OBDal.getInstance().save(catgeory);
    }catch(Exception e){e.printStackTrace();}
    return catgeory;
  }

}
