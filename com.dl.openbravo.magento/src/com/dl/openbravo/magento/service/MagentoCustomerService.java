package com.dl.openbravo.magento.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.businesspartner.Location;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.customer.Customer;
import com.dl.openbravo.magento.client.soap.v1.model.customer.CustomerAddress;

public class MagentoCustomerService extends MagentoService<BusinessPartner, Customer> {

  private static final String ERROR_NO_BP_CATEGORY = "Category cannot be null";

  private Map<Integer, Category> bpCategoryCache = new HashMap<Integer, Category>();

  public MagentoCustomerService(MagentoContext magentoContext) {
    super(magentoContext, BusinessPartner.class, Customer.class);
  }

  @Override
  public BusinessPartner getOBObjectFromMagento(Customer mgtoCustomer) throws Exception {

    Map<String, Object> filter = new HashMap<String, Object>();
    filter.put(BusinessPartner.PROPERTY_DLMGTOREFERENCE,
        Long.valueOf(mgtoCustomer.getId().longValue()).toString());
    filter.put(BusinessPartner.PROPERTY_ORGANIZATION,
        getMagentoContext().getStore().getCustomerOrg());

    BusinessPartner customer = getMagentoContext().findOBObject(BusinessPartner.class, filter);

    if (customer == null) {
      debug(STR_CREATE);
      customer = getMagentoContext().getNewOBInstance(BusinessPartner.class,
          getMagentoContext().getStore().getCustomerOrg());
      customer.setADUserList(new ArrayList<User>());
      customer.setBusinessPartnerLocationList(new ArrayList<Location>());
    } else {
      debug(STR_UPDATE);
    }

    customer.setDlmgtoReference(Long.valueOf(mgtoCustomer.getId()).toString());
    customer.setDlmgtoStore(getMagentoContext().getStore());
    customer.setCustomer(true);
    StringBuilder name = new StringBuilder(mgtoCustomer.getFirstName());
    if (mgtoCustomer.getMiddleName() != null && mgtoCustomer.getMiddleName().trim().length() > 0)
      name.append(" ").append(mgtoCustomer.getMiddleName());
    if (mgtoCustomer.getLastName() != null && mgtoCustomer.getLastName().trim().length() > 0)
      name.append(" ").append(mgtoCustomer.getLastName());
    String searchKey = mgtoCustomer.getEmail();

    if (searchKey == null || searchKey.trim().length() == 0)
      searchKey = name.toString();

    if (name.length() <= 60)
      customer.setName(name.toString());
    else
      customer.setName(name.substring(0, 59));

    if (searchKey.length() <= 40)
      customer.setSearchKey(searchKey);
    else
      customer.setSearchKey(searchKey.substring(0, 39));

    if (mgtoCustomer.getGroupId() != null)
      customer.setBusinessPartnerCategory(getCategory(Integer.valueOf(mgtoCustomer.getGroupId())));

    if (customer.getBusinessPartnerCategory() == null)
      throw new Exception(ERROR_NO_BP_CATEGORY);

    customer.setPaymentTerms(getMagentoContext().getStore().getPaymentterm());
    customer.setPaymentMethod(getMagentoContext().getStore().getFINPaymentmethod());
    customer.setCurrency(getMagentoContext().getStore().getCurrency());
    customer.setPriceList(getMagentoContext().getStore().getPricelist());
    // customer.setTaxCategory(getMagentoContext().getStore().getBpTaxcategory());
    customer.setInvoiceTerms("I");
    customer.setDeliveryMethod("S");
    customer.setDeliveryTerms("A");

    getMagentoContext().getCustomerMapper().mapMagentoToOB(mgtoCustomer, customer);

    List<CustomerAddress> customerAddresses = getMagentoContext().getRemoteServiceFactory()
        .getCustomerAddressRemoteService().list(mgtoCustomer.getId());

    if (customerAddresses != null)
      for (CustomerAddress customerAddress : customerAddresses) {
        User contact = getMagentoContext().getAddressService().getContactFromMagento(customer,
            customerAddress);
        contact.setEmail(mgtoCustomer.getEmail());
        getMagentoContext().getAddressService().getAddressFromMagento(customer, customerAddress);
      }

    return customer;
  }

  public Category getCategory(Integer id) {
    if (bpCategoryCache.get(id) == null) {
      bpCategoryCache.put(id, getMagentoContext().findOBObject(Category.class,
          Category.PROPERTY_DLMGTOREFERENCE, Long.valueOf(id).toString()));
    }
    return bpCategoryCache.get(id);
  }

  public BusinessPartner saveCustomerInOB(BusinessPartner customer) throws Exception {

    getMagentoContext().getBpGroupService().saveBPGroupInOB(customer.getBusinessPartnerCategory());
    OBDal.getInstance().save(customer);

    for (User contact : customer.getADUserList())
      getMagentoContext().getAddressService().saveContactInOB(contact);

    for (Location location : customer.getBusinessPartnerLocationList())
      getMagentoContext().getAddressService().saveLocationInOB(location);

    return customer;
  }

}
