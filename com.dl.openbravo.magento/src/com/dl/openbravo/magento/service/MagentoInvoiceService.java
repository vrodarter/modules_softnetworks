package com.dl.openbravo.magento.service;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.order.InvoiceItem;

public class MagentoInvoiceService
    extends MagentoService<Invoice, com.dl.openbravo.magento.client.soap.v1.model.order.Invoice> {

  public MagentoInvoiceService(MagentoContext magentoContext) {
    super(magentoContext, Invoice.class,
        com.dl.openbravo.magento.client.soap.v1.model.order.Invoice.class);
  }

  @Override
  public Invoice getOBObjectFromMagento(
      com.dl.openbravo.magento.client.soap.v1.model.order.Invoice magentoObject) throws Exception {
    return null;
  }

  public com.dl.openbravo.magento.client.soap.v1.model.order.Invoice getMagentoObjectFromOB(
      Invoice invoice) throws Exception {

    com.dl.openbravo.magento.client.soap.v1.model.order.Invoice mgtoInvoice = new com.dl.openbravo.magento.client.soap.v1.model.order.Invoice();
    mgtoInvoice.setOrderNumber(invoice.getSalesOrder().getDlmgtoReference());
    List<InvoiceItem> items = new ArrayList<InvoiceItem>();

    mgtoInvoice.setItems(items);
    getMagentoContext().getRemoteServiceFactory().getInvoiceRemoteService().save(mgtoInvoice, "",
        false, false);

    return mgtoInvoice;
  }

  public Invoice saveInvoiceInOB(Invoice invoice) throws Exception {
    OBDal.getInstance().save(invoice);
    return invoice;
  }

}
