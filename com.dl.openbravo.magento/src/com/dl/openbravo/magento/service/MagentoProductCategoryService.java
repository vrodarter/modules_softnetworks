package com.dl.openbravo.magento.service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.model.common.plm.ProductCategory;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.category.Category;

public class MagentoProductCategoryService extends MagentoService<ProductCategory, Category> {

  private StringBuilder refSb = new StringBuilder();
  private StringBuilder skSb = new StringBuilder();

  public MagentoProductCategoryService(MagentoContext magentoContext) {
    super(magentoContext, ProductCategory.class, Category.class);
  }

  @Override
  public ProductCategory getOBObjectFromMagento(Category category) throws Exception {

    refSb.setLength(0);
    skSb.setLength(0);

    ProductCategory productCategory = getMagentoContext().findOBObject(ProductCategory.class,
        ProductCategory.PROPERTY_DLMGTOREFERENCE,
        Long.valueOf(category.getId().longValue()).toString());

    if (productCategory == null) {
      debug(STR_CREATE);
      productCategory = getMagentoContext().getNewOBInstance(ProductCategory.class,
          getMagentoContext().getStore().getProductOrg());
    } else {
      debug(STR_UPDATE);
    }

    skSb.append(category.getId()).append(" - ").append(category.getName());
    productCategory.setDlmgtoReference(Long.valueOf(category.getId()).toString());
    productCategory.setDlmgtoStore(getMagentoContext().getStore());
    productCategory.setName(category.getName());
    if (skSb.length() > 40)
      productCategory.setSearchKey(skSb.substring(0, 39));
    else
      productCategory.setSearchKey(skSb.toString());
    productCategory.setPlannedMargin(BigDecimal.ZERO);
    productCategory
        .setSummaryLevel(category.getChildren() != null && category.getChildren().size() > 0);

    return productCategory;
  }

  public Category getMagentoObjectFromOB(ProductCategory obCategory) throws Exception {
    Category category = null;
    Integer parentId = null;
    if (obCategory.getDlmgtoReference() != null)
      category = getMagentoContext().getRemoteServiceFactory().getCategoryRemoteService()
          .getByIdClean(Long.valueOf(obCategory.getDlmgtoReference()).intValue());

    if (category == null) {
      category = new Category();
      category.setActive(true);
      category.setIncludeInMenu(false);
      category.setAvailableSortBy("position");
      category.setDefaultSortBy("position");
      TreeNode parentNode = getMagentoContext().findOBObject(TreeNode.class, TreeNode.PROPERTY_NODE,
          obCategory.getId());
      if (parentNode != null) {
        ProductCategory parentCategory = OBDal.getInstance().get(ProductCategory.class,
            parentNode.getReportSet());
        if (parentCategory != null) {
          parentId = Long.valueOf(parentCategory.getDlmgtoReference()).intValue();
        }
      }
    }

    category.setDescription(obCategory.getDescription());
    category.setName(obCategory.getName());

    if (category.getId() == null)
      getMagentoContext().getRemoteServiceFactory().getCategoryRemoteService().create(parentId,
          category);
    else
      getMagentoContext().getRemoteServiceFactory().getCategoryRemoteService().save(category);

    return category;
  }

  public List<String> readObCategoryIdTree(ProductCategory root, List<String> list,
      boolean obFilter, boolean magentoFilter) {
    List<String> localList = list;
    if (localList == null)
      localList = new LinkedList<String>();

    if ((obFilter && root.getDlmgtoReference() == null)
        || (magentoFilter && root.getDlmgtoReference() != null) || (!obFilter && !magentoFilter))
      localList.add(root.getId());

    OBCriteria<TreeNode> childCriteria = OBDal.getInstance().createCriteria(TreeNode.class);
    childCriteria.add(Restrictions.eq(TreeNode.PROPERTY_REPORTSET, root.getId()));

    for (TreeNode childNode : childCriteria.list())
      readObCategoryIdTree(OBDal.getInstance().get(ProductCategory.class, childNode.getNode()),
          localList, obFilter, magentoFilter);

    return localList;
  }

}