package com.dl.openbravo.magento.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.ApprovedVendor;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.db.CallStoredProcedure;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.category.Category;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttribute;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttribute.Scope;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductAttributeSet;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductMedia;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductMedia.Type;
import com.dl.openbravo.magento.client.soap.v1.model.product.ProductType;
import com.dl.openbravo.magento.client.soap.v1.model.product.Visibility;
import com.dl.openbravo.magento.data.DLMGTOAttributeConfig;
import com.dl.openbravo.magento.data.DLMGTOProductCategory;
import com.dl.openbravo.magento.data.DLMGTOProductConfig;
import com.dl.openbravo.magento.data.DLMGTOProductImage;
import com.dl.openbravo.magento.data.DLMGTOStockWarehouse;
import com.softnetworks.magento.custom.data.SNMTStoreProduct;

public class MagentoProductService
    extends MagentoService<Product, com.dl.openbravo.magento.client.soap.v1.model.product.Product> {

  private static final String ERROR_NO_PRODUCT_CATEGORY = "Product Category cannot be null";

  private Date currentDate = Calendar.getInstance().getTime();
  private PriceList salesPriceList;
  private PriceListVersion salesPriceListVersion;
  private PriceList purchasePriceList;
  private PriceListVersion purchasePriceListVersion;

  public MagentoProductService(MagentoContext magentoContext) {
    super(magentoContext, Product.class,
        com.dl.openbravo.magento.client.soap.v1.model.product.Product.class);
  }

  @Override
  public Product getOBObjectFromMagento(
      com.dl.openbravo.magento.client.soap.v1.model.product.Product _product) throws Exception {

    Product product = getMagentoContext().findOBObject(Product.class,
        Product.PROPERTY_DLMGTOREFERENCE, Long.valueOf(_product.getId().longValue()).toString());

    if (product == null) {
      debug(STR_CREATE);
      product = getMagentoContext().getNewOBInstance(Product.class,
          getMagentoContext().getStore().getProductOrg());
      product.setApprovedVendorList(new ArrayList<ApprovedVendor>());
    } else { 
     debug(STR_UPDATE);
     return product;
    }
    
     com.dl.openbravo.magento.client.soap.v1.model.product.Product mgtoProduct = getMagentoContext()
          .getRemoteServiceFactory().getProductRemoteService().getById(_product.getId());

    product.setDlmgtoReference(Long.valueOf(mgtoProduct.getId()).toString());
    product.setDlmgtoStore(getMagentoContext().getStore());
    if (mgtoProduct.getName().length() <= 60)
      product.setName(mgtoProduct.getName());
    else
      product.setName(mgtoProduct.getName().substring(0, 59));
    product.setSearchKey(mgtoProduct.getSku());
    product.setSKU(mgtoProduct.getSku());
    if (mgtoProduct.getType() != null) {
      product.setStocked(mgtoProduct.getType().equals(ProductType.BUNDLE)
          || mgtoProduct.getType().equals(ProductType.GROUPED)
          || mgtoProduct.getType().equals(ProductType.SIMPLE)
      // || mgtoProduct.getType().equals(ProductType.CONFIGURABLE )
      );
    }

    if (product.isStocked()) {
      if (mgtoProduct.getAllProperties().get("condition") != null
          && getMagentoContext().getStore().getConditionField() != null
          && mgtoProduct.getAllProperties().get("condition").toString()
              .equalsIgnoreCase(getMagentoContext().getStore().getConditionField())) {
        product.setStocked(false);
        product.setPurchase(false);
        product.setProductType("S");
      }
    }

    product.setUOM(getMagentoContext().getStore().getUOM());
    product.setTaxCategory(getMagentoContext().getStore().getTaxCategory());

    if (mgtoProduct.getCategories() != null && mgtoProduct.getCategories().size() > 0)
      product.setProductCategory(
          getProductCategory(Integer.valueOf(mgtoProduct.getCategories().get(0).getId())));

    if (product.getProductCategory() == null)
      throw new Exception(ERROR_NO_PRODUCT_CATEGORY);

    if (mgtoProduct.getAttributeSet() != null)
      product.setAttributeSet(getAttributeSet(mgtoProduct.getAttributeSet().getId()));

    if (mgtoProduct.getPrice() != null) {
      ProductPrice productPrice = null;
      if (product.getPricingProductPriceList() == null)
        product.setPricingProductPriceList(new ArrayList<ProductPrice>());

      for (ProductPrice pp : product.getPricingProductPriceList())
        if (pp.getPriceListVersion().getId().equals(getSalesPriceListVersion().getId()))
          productPrice = pp;

      if (productPrice == null) {
        productPrice = getMagentoContext().getNewOBInstance(ProductPrice.class,
            getMagentoContext().getStore().getProductOrg());
        productPrice.setProduct(product);
        productPrice.setPriceListVersion(getSalesPriceListVersion());
        product.getPricingProductPriceList().add(productPrice);
      }

      productPrice.setListPrice(new BigDecimal(
          mgtoProduct.getMsrp() != null ? mgtoProduct.getMsrp() : mgtoProduct.getPrice()));
      productPrice.setStandardPrice(new BigDecimal(mgtoProduct.getPrice()));
      productPrice.setCost(new BigDecimal(
          mgtoProduct.getCost() != null ? mgtoProduct.getCost() : mgtoProduct.getPrice()));
      productPrice.setPriceLimit(new BigDecimal(mgtoProduct.getPrice()));

    } else
      debug("WARNING: Product Price is null");

    if (mgtoProduct.getCost() != null) {
      ProductPrice productPrice = null;
      if (product.getPricingProductPriceList() == null)
        product.setPricingProductPriceList(new ArrayList<ProductPrice>());

      for (ProductPrice pp : product.getPricingProductPriceList())
        if (pp.getPriceListVersion().getId().equals(getPurchasePriceListVersion().getId()))
          productPrice = pp;

      if (productPrice == null) {
        productPrice = getMagentoContext().getNewOBInstance(ProductPrice.class,
            getMagentoContext().getStore().getProductOrg());
        productPrice.setProduct(product);
        productPrice.setPriceListVersion(getPurchasePriceListVersion());
        product.getPricingProductPriceList().add(productPrice);
      }

      productPrice.setListPrice(new BigDecimal(mgtoProduct.getCost()));
      productPrice.setStandardPrice(new BigDecimal(mgtoProduct.getCost()));
      productPrice.setCost(new BigDecimal(mgtoProduct.getCost()));
      productPrice.setPriceLimit(new BigDecimal(mgtoProduct.getCost()));

    } else
      debug("WARNING: Product Cost is null");

    if (getMagentoContext().getStore().isSyncProductImages())
      if (mgtoProduct.getMedias() != null && mgtoProduct.getMedias().size() > 0) {
        for (ProductMedia pm : mgtoProduct.getMedias())
          if (pm.getTypes() != null && pm.getTypes().contains(Type.IMAGE) && pm.getUrl() != null) {
            product.setImage(getMagentoContext().getImageFromUrl(pm.getUrl(), pm.getFile()));
            break;
          }

        if (product.getDlmgtoProductImageList() == null)
          product.setDlmgtoProductImageList(new ArrayList<DLMGTOProductImage>());

        for (ProductMedia pm : mgtoProduct.getMedias()) {
          DLMGTOProductImage mProductImage = null;
          for (DLMGTOProductImage mpi : product.getDlmgtoProductImageList())
            if (mpi.getName() != null && mpi.getName().equals(pm.getFile().toString()))
              mProductImage = mpi;
          if (mProductImage == null) {
            mProductImage = getMagentoContext().getNewOBInstance(DLMGTOProductImage.class,
                getMagentoContext().getStore().getProductOrg());
            mProductImage.setProduct(product);
            mProductImage.setName(pm.getFile().toString());
            product.getDlmgtoProductImageList().add(mProductImage);
          }
          mProductImage.setImage(getMagentoContext().getImageFromUrl(pm.getUrl(), pm.getFile()));
          if (pm.getTypes() != null) {
            mProductImage.setImage(pm.getTypes().contains(Type.IMAGE));
            mProductImage.setThumbnail(pm.getTypes().contains(Type.THUMBNAIL));
            mProductImage.setSmallimage(pm.getTypes().contains(Type.SMALL_IMAGE));
          }

        }

      }

    if (product.getDlmgtoProductCategoryList() == null)
      product.setDlmgtoProductCategoryList(new ArrayList<DLMGTOProductCategory>());

    if (mgtoProduct.getCategories() != null)
      for (Category cid : mgtoProduct.getCategories()) {
        try {
          ProductCategory productCategory = getProductCategory(Integer.valueOf(cid.getId()));
          if (productCategory != null) {
            DLMGTOProductCategory mProductCategory = null;
            for (DLMGTOProductCategory mpc : product.getDlmgtoProductCategoryList())
              if (mpc.getProductCategory().getId().equals(productCategory.getId()))
                mProductCategory = mpc;

            if (mProductCategory == null) {
              mProductCategory = getMagentoContext().getNewOBInstance(DLMGTOProductCategory.class,
                  getMagentoContext().getStore().getProductOrg());
              mProductCategory.setProduct(product);
              mProductCategory.setProductCategory(productCategory);
              product.getDlmgtoProductCategoryList().add(mProductCategory);
            }
          }

        } catch (Exception e) {
        }
      }

    if (getMagentoContext().getStore().getVendorField() != null && mgtoProduct.getAllProperties()
        .get(getMagentoContext().getStore().getVendorField()) != null) {
      String vendorName = mgtoProduct.getAllProperties()
          .get(getMagentoContext().getStore().getVendorField()).toString();
      ApprovedVendor av = null;
      if (!product.getApprovedVendorList().isEmpty())
        av = product.getApprovedVendorList().get(0);
      if (av == null) {
        av = getMagentoContext().getNewOBInstance(ApprovedVendor.class,
            getMagentoContext().getStore().getProductOrg());
        av.setProduct(product);
        product.getApprovedVendorList().add(av);
      }

      BusinessPartner vendor = av.getBusinessPartner();
      if (vendor == null) {
        vendor = getMagentoContext().findOBObject(BusinessPartner.class,
            BusinessPartner.PROPERTY_SEARCHKEY, "VENDOR-" + vendorName);
        if (vendor == null)
          vendor = getMagentoContext().getNewOBInstance(BusinessPartner.class,
              getMagentoContext().getStore().getProductOrg());
        av.setBusinessPartner(vendor);
      }

      org.openbravo.model.common.businesspartner.Category vendorCategory = vendor
          .getBusinessPartnerCategory();

      if (vendorCategory == null) {
        vendorCategory = getMagentoContext().findOBObject(
            org.openbravo.model.common.businesspartner.Category.class,
            org.openbravo.model.common.businesspartner.Category.PROPERTY_SEARCHKEY, "MGTO VENDOR");
        if (vendorCategory == null) {
          vendorCategory = getMagentoContext().getNewOBInstance(
              org.openbravo.model.common.businesspartner.Category.class,
              getMagentoContext().getStore().getProductOrg());
          vendorCategory.setName("MGTO VENDOR");
          vendorCategory.setSearchKey("MGTO VENDOR");
        }

      }

      vendor.setBusinessPartnerCategory(vendorCategory);
      vendor.setName(vendorName);
      vendor.setSearchKey("VENDOR-" + vendorName);
      vendor.setVendor(true);
      vendor.setPaymentTerms(getMagentoContext().getStore().getPaymentterm());
      vendor.setPaymentMethod(getMagentoContext().getStore().getFINPaymentmethod());
      vendor.setCurrency(getMagentoContext().getStore().getCurrency());
      vendor.setPriceList(getMagentoContext().getStore().getPricelist());
      vendor.setTaxCategory(getMagentoContext().getStore().getBpTaxcategory());
      vendor.setInvoiceTerms("I");
      vendor.setDeliveryMethod("S");
      vendor.setDeliveryTerms("A");
      vendor.setPurchasePricelist(getPurchasePriceList());
      av.setBusinessPartner(vendor);
    }

    getMagentoContext().getProductMapper().mapMagentoToOB(mgtoProduct, product);

    return product;
  }

  public ProductCategory getProductCategory(Integer magentoId) {
    return getMagentoContext().findOBObject(ProductCategory.class,
        ProductCategory.PROPERTY_DLMGTOREFERENCE, Long.valueOf(magentoId).toString());
  }

  public AttributeSet getAttributeSet(Integer magentoId) {
    return getMagentoContext().findOBObject(AttributeSet.class,
        AttributeSet.PROPERTY_DLMGTOREFERENCE, Long.valueOf(magentoId).toString());
  }

  public PriceList getSalesPriceList() {
    if (salesPriceList == null)
      salesPriceList = getMagentoContext().getStore().getPricelist();
    return salesPriceList;
  }

  public PriceListVersion getSalesPriceListVersion() {
    if (salesPriceListVersion == null) {
      for (PriceListVersion plv : getSalesPriceList().getPricingPriceListVersionList())
        if (plv.isActive() && plv.getValidFromDate() != null
            && plv.getValidFromDate().before(currentDate))
          salesPriceListVersion = plv;
      if (salesPriceListVersion == null)
        salesPriceListVersion = getSalesPriceList().getPricingPriceListVersionList().get(0);
    }
    return salesPriceListVersion;
  }

  public PriceList getPurchasePriceList() {
    if (purchasePriceList == null)
      purchasePriceList = getMagentoContext().getStore().getMPricelist();
    return purchasePriceList;
  }

  public PriceListVersion getPurchasePriceListVersion() {
    if (purchasePriceListVersion == null) {
      for (PriceListVersion plv : getPurchasePriceList().getPricingPriceListVersionList())
        if (plv.isActive() && plv.getValidFromDate() != null
            && plv.getValidFromDate().before(currentDate))
          purchasePriceListVersion = plv;
      if (purchasePriceListVersion == null)
        purchasePriceListVersion = getPurchasePriceList().getPricingPriceListVersionList().get(0);
    }
    return purchasePriceListVersion;
  }

  public Product saveProductInOB(Product product) throws Exception {
    if (product.getProductCategory() != null)
      OBDal.getInstance().save(product.getProductCategory());

    if (product.getImage() != null)
      OBDal.getInstance().save(product.getImage());

    for (ApprovedVendor av : product.getApprovedVendorList()) {
      if (av.getBusinessPartner() != null) {
        if (av.getBusinessPartner().getBusinessPartnerCategory() != null)
          OBDal.getInstance().save(av.getBusinessPartner().getBusinessPartnerCategory());
        OBDal.getInstance().save(av.getBusinessPartner());
      }
    }

    OBDal.getInstance().save(product);

    for (DLMGTOProductCategory mpc : product.getDlmgtoProductCategoryList())
      OBDal.getInstance().save(mpc);

    for (ProductPrice pp : product.getPricingProductPriceList())
      OBDal.getInstance().save(pp);

    for (ApprovedVendor av : product.getApprovedVendorList()) {

      OBDal.getInstance().save(av);
    }

    for (DLMGTOProductImage pi : product.getDlmgtoProductImageList()) {
      if (pi.getImage() != null) {
        OBDal.getInstance().save(pi.getImage());
        OBDal.getInstance().save(pi);
      }
    }
    return product;
  }

  public com.dl.openbravo.magento.client.soap.v1.model.product.Product getMagentoObjectFromOB(
      Product obProduct) throws Exception {

    if ((obProduct.getProductCategory() == null
        || obProduct.getProductCategory().getDlmgtoReference() == null)
        && !getMagentoContext().getStore().isForced())
      throw new Exception("Product category is not in magento");

    if (obProduct.getAttributeSet() == null
        || obProduct.getAttributeSet().getDlmgtoReference() == null)
      throw new Exception("AttributeSet is not in magento");

    com.dl.openbravo.magento.client.soap.v1.model.product.Product product = null;
    if (obProduct.getDlmgtoReference() != null)
      product = getMagentoContext().getRemoteServiceFactory().getProductRemoteService()
          .getById(Long.valueOf(obProduct.getDlmgtoReference()).intValue());
    if (product == null) {
      product = new com.dl.openbravo.magento.client.soap.v1.model.product.Product();
      product.setName(obProduct.getName());
    }
    getMagentoContext().getProductMapper().mapOBToMagento(obProduct, product);

    if (product.getName() == null || product.getName().trim().length() == 0)
      product.setName(obProduct.getName());

    if (product.getSku() == null || product.getSku().trim().length() == 0)
      if (obProduct.getSKU() == null)
        product.setSku(obProduct.getSearchKey());
      else
        product.setSku(obProduct.getSKU());

    product.setInStock(obProduct.isStocked());

    if (product.getDescription() == null || product.getDescription().trim().length() == 0)
      product.setDescription(obProduct.getDescription());

    if (obProduct.getWeight() != null)
      product.setWeight(obProduct.getWeight().doubleValue());
    else
      product.setWeight(0d);

    try {
      if (product.getVisibility() == null)
        product.setVisibility(Visibility.CATALOG_SEARCH);
    } catch (Exception ex) {
      product.setVisibility(Visibility.CATALOG_SEARCH);
    }

    product.setEnabled(true);

    ProductAttributeSet pas = new ProductAttributeSet();
    pas.setId(Long.valueOf(obProduct.getAttributeSet().getDlmgtoReference()).intValue());
    product.setAttributeSet(pas);

    for (ProductPrice pp : obProduct.getPricingProductPriceList()) {
      if (pp.getPriceListVersion().getPriceList().getId()
          .equals(getMagentoContext().getStore().getPricelist().getId()))
        product.setPrice(pp.getListPrice().doubleValue());
      if (pp.getPriceListVersion().getPriceList().getId()
          .equals(getMagentoContext().getStore().getMPricelist().getId()))
        product.setCost(pp.getListPrice().doubleValue());
    }
    Set<Long> productCategies = new HashSet<Long>();

    if (obProduct.getProductCategory().getDlmgtoReference() != null)
      productCategies.add(Long.valueOf(obProduct.getProductCategory().getDlmgtoReference()));
    else if (getMagentoContext().getStore().isForced()) {
      debug("The category " + obProduct.getProductCategory().getName() + " will be in magento");
      Category category = getMagentoContext().getProductCategoryService()
          .getMagentoObjectFromOB(obProduct.getProductCategory());
      obProduct.getProductCategory().setDlmgtoReference(category.getId().toString());
      productCategies.add(category.getId().longValue());
    } else
      throw new Exception(
          "The category " + obProduct.getProductCategory().getName() + " in not in magento");

    for (DLMGTOProductCategory pc : obProduct.getDlmgtoProductCategoryList()) {
      if (pc.getProductCategory().getDlmgtoReference() != null)
        productCategies.add(Long.valueOf(pc.getProductCategory().getDlmgtoReference()));
      else if (getMagentoContext().getStore().isForced()) {
        debug("The category " + pc.getProductCategory().getName() + " will be in magento");
        Category category = getMagentoContext().getProductCategoryService()
            .getMagentoObjectFromOB(pc.getProductCategory());
        pc.getProductCategory().setDlmgtoReference(category.getId().toString());
        productCategies.add(category.getId().longValue());
      } else
        throw new Exception(
            "The category " + pc.getProductCategory().getName() + " in not in magento");
    }

    List<Category> categories = new ArrayList<Category>();

    for (Long catId : productCategies) {
      Category cat = new Category();
      cat.setId(catId.intValue());
      categories.add(cat);
    }

    product.setCategories(categories);

    List<String> listSku = new ArrayList<String>();
    List<String> listAttributes = new ArrayList<String>();
    if (obProduct.getDlmgtoProductConfigList() != null
        && !obProduct.getDlmgtoProductConfigList().isEmpty()) {
      product.setType(ProductType.CONFIGURABLE);
      obProduct.setDlmgtoType("configurable");

      for (DLMGTOProductConfig pc : obProduct.getDlmgtoProductConfigList()) {
        if (pc.getDlmgtoProduct().getDlmgtoSku() != null)
          listSku.add(pc.getDlmgtoProduct().getDlmgtoSku());
        else if (pc.getDlmgtoProduct().getSKU() != null)
          listSku.add(pc.getDlmgtoProduct().getSKU());
        else
          listSku.add(pc.getDlmgtoProduct().getSearchKey());
      }

      for (DLMGTOAttributeConfig ac : obProduct.getDlmgtoAttributeConfigList()) {
        listAttributes.add(ac.getAttribute().getDlmgtoReference());
      }
      product.set("associated_skus", listSku.toArray(new String[listSku.size()]));
      product.set("configurable_attributes",
          listAttributes.toArray(new String[listAttributes.size()]));
    }

    // product.set("condition", obProduct.getUPCEAN());

    if (product.getAllProperties() != null && obProduct.getSnmtStoreProductList() != null
        && !obProduct.getSnmtStoreProductList().isEmpty()) {
      for (SNMTStoreProduct snmtStoreProduct : obProduct.getSnmtStoreProductList()) {
        String name = snmtStoreProduct.getSnmtStore().getName().toLowerCase().replaceAll(" ", "_")
            .replaceAll("-", "_");
        name += "_sku_listing";
        try {

          product.set(name, snmtStoreProduct.getProductNumber());
          // if (getMagentoContext().getRemoteServiceFactory().getProductAttributeRemoteService()
          // .exists(name)) {
          ProductAttribute pa = new ProductAttribute();
          pa.setCode(name);
          getMagentoContext().getRemoteServiceFactory().getProductAttributeRemoteService().save(pa);
          // }
        } catch (Exception e) {
          debug("****************** Cant create sku listing " + name + " \n " + e.getMessage());
          e.printStackTrace();
        }
      }
    }

    if (product.getId() == null)
      getMagentoContext().getRemoteServiceFactory().getProductRemoteService().add(product);
    else
      getMagentoContext().getRemoteServiceFactory().getProductRemoteService().update(product, null);

    return product;
  }

  public void updateStock(Product obProduct) throws Exception {
    com.dl.openbravo.magento.client.soap.v1.model.product.Product product = null;
    if (obProduct.getDlmgtoReference() != null) {
      product = new com.dl.openbravo.magento.client.soap.v1.model.product.Product();
      product.setId(Long.valueOf(obProduct.getDlmgtoReference()).intValue());
    } else if (getMagentoContext().getStore().isForced()) {
      debug("The product " + obProduct.getName() + " will be in magento");
      product = getMagentoObjectFromOB(obProduct);
      obProduct.setDlmgtoReference(product.getId().toString());
    } else
      throw new Exception("The product " + obProduct.getName() + " in not in magento");

    BigDecimal qtyAvailable = getQtyAvailable(obProduct);

    if (qtyAvailable == null)
      throw new Exception("Error getting de available qty");

    product.setQty(qtyAvailable.doubleValue());
    product.setInStock(qtyAvailable.doubleValue() > 0);
    product.setManageStock(true);
    product.setUseConfigManageStock(false);

    getMagentoContext().getRemoteServiceFactory().getProductRemoteService()
        .updateInventory(product);

  }

  public BigDecimal getQtyAvailable(Product paramProduct) {

    // localArrayList.add(getMagentoContext().getStore().getWarehouse().getId());
    if (getMagentoContext().getStore().getDlmgtoStockWarehouseList() == null
        || getMagentoContext().getStore().getDlmgtoStockWarehouseList().isEmpty())
      return getQtyAvailable(paramProduct, getMagentoContext().getStore().getWarehouse());

    double total = 0d;

    for (DLMGTOStockWarehouse sw : getMagentoContext().getStore().getDlmgtoStockWarehouseList()) {
      try {
        total += getQtyAvailable(paramProduct, sw.getWarehouse()).doubleValue();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    return new BigDecimal(total);

  }

  public BigDecimal getQtyAvailable(Product paramProduct, Warehouse warehouse) {
    List<Object> localArrayList = new ArrayList<Object>();
    localArrayList.add(paramProduct.getId());
    localArrayList.add(warehouse.getId());
    return (BigDecimal) CallStoredProcedure.getInstance().call("m_bom_qty_available",
        localArrayList, null, false, true);
  }
}
