package com.dl.openbravo.magento.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.service.db.CallStoredProcedure;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.magento.ResourcePath;
import com.dl.openbravo.magento.client.soap.v1.model.order.OrderItem;

public class MagentoSalesOrderService
    extends MagentoService<Order, com.dl.openbravo.magento.client.soap.v1.model.order.Order> {

  public enum StockMode {
    WARN, THROW, RESERVATION
  }

  private static final String QTY_STOCK_FUNCTION_AVAILABLE = "m_bom_qty_available";
  private static final String QTY_STOCK_FUNCTION_ORDERED = "m_bom_qty_ordered";
  private static final String QTY_STOCK_FUNCTION_ONHAND = "m_bom_qty_onhand";
  private static final String QTY_STOCK_FUNCTION_RESERVERD = "m_bom_qty_reserved";

  private StockMode stockMode;

  public MagentoSalesOrderService(MagentoContext magentoContext) {
    super(magentoContext, Order.class,
        com.dl.openbravo.magento.client.soap.v1.model.order.Order.class);
    stockMode = StockMode.valueOf(magentoContext.getStore().getOrderStockMode());
  }

  @Override
  public Order getOBObjectFromMagento(
      com.dl.openbravo.magento.client.soap.v1.model.order.Order magentoOrder) throws Exception {
    Order salesOrder = getMagentoContext().findOBObject(Order.class, Order.PROPERTY_DLMGTOREFERENCE,
        magentoOrder.getOrderNumber());
    if (salesOrder == null) {
      debug(STR_CREATE);
      salesOrder = getMagentoContext().getNewOBInstance(Order.class,
          getMagentoContext().getStore().getOrderOrg());
      salesOrder.setDlmgtoReference(magentoOrder.getOrderNumber());
      salesOrder.setDlmgtoStore(getMagentoContext().getStore());
      salesOrder.setOrderReference(magentoOrder.getOrderNumber());
      salesOrder.setSalesTransaction(true);
      salesOrder.setDocumentNo(getNextDocumentNumber());
      salesOrder.setOrderLineList(new ArrayList<OrderLine>());
    } else {
      debug(STR_UPDATE);
    }

    BusinessPartner customer = getMagentoContext().getCustomerService()
        .getOBObjectFromMagento(magentoOrder.getCustomer());

    salesOrder.setBusinessPartner(customer);
    salesOrder.setDeliveryLocation(getMagentoContext().getAddressService()
        .getAddressFromMagento(customer, magentoOrder.getShippingAddress()));
    salesOrder.setInvoiceAddress(getMagentoContext().getAddressService()
        .getAddressFromMagento(customer, magentoOrder.getBillingAddress()));
    salesOrder.setPartnerAddress(salesOrder.getInvoiceAddress());

    salesOrder.setWarehouse(getMagentoContext().getStore().getWarehouse());
    salesOrder.setTransactionDocument(getMagentoContext().getStore().getDoctype());
    salesOrder.setDocumentType(salesOrder.getTransactionDocument());
    salesOrder.setPaymentTerms(getMagentoContext().getStore().getPaymentterm());
    salesOrder.setPaymentMethod(getMagentoContext().getStore().getFINPaymentmethod());
    salesOrder.setCurrency(getMagentoContext().getStore().getCurrency());
    salesOrder.setPriceList(getMagentoContext().getStore().getPricelist());
    salesOrder.setInvoiceTerms("I");
    salesOrder.setDeliveryMethod("S");
    salesOrder.setDeliveryTerms("A");
    salesOrder.setAccountingDate(new Date());
    salesOrder.setScheduledDeliveryDate(new Date());

    getMagentoContext().getSalesOrderMapper().mapMagentoToOB(magentoOrder, salesOrder);

    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++sales channel:"+magentoOrder.getAllProperties().get("sales_channel"));
    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++market:"+magentoOrder.getAllProperties().get("market_name"));

    if(magentoOrder.getAllProperties().get("sales_channel")!=null)
      salesOrder.setDlmgtoChannel(magentoOrder.getAllProperties().get("sales_channel").toString());

    if(magentoOrder.getAllProperties().get("market_name")!=null)
      salesOrder.setDlmgtoChannelMarket(magentoOrder.getAllProperties().get("market_name").toString());



    try {
      String strDate = magentoOrder.getAllProperties().get("created_at").toString();
      strDate = strDate.substring(0, strDate.indexOf(" "));
      salesOrder.setOrderDate(new SimpleDateFormat("yyyy-MM-dd").parse(strDate.trim()));
    } catch (Exception e) {
      salesOrder.setOrderDate(new Date());
    }

    TaxRate taxRate = getTaxRate(salesOrder, magentoOrder);

    List<OrderItem> orderItems = magentoOrder.getItems();
    for (OrderItem orderItem : orderItems)
      getOrderLineFromMagento(salesOrder, orderItem, taxRate);

    createShippingAngHandling(salesOrder, magentoOrder);
    createCreditOrGiftCardDiscount(salesOrder, magentoOrder);

    return salesOrder;
  }

  public OrderLine getOrderLineFromMagento(Order salesOrder, OrderItem magentoOrderItem,
      TaxRate taxRate) throws Exception {

    OrderLine salesOrderLine = null;
    long lineNo = salesOrder.getOrderLineList().size() * 10L;

    for (OrderLine ol : salesOrder.getOrderLineList())
      if (ol.getDlmgtoReference() != null && magentoOrderItem.getId() != null
          && ol.getDlmgtoReference().equals(Long.valueOf(magentoOrderItem.getId())))
        salesOrderLine = ol;

    if (salesOrderLine == null) {
      salesOrderLine = getMagentoContext().getNewOBInstance(OrderLine.class,
          getMagentoContext().getStore().getOrderOrg());
      salesOrderLine.setDlmgtoReference(Long.valueOf(magentoOrderItem.getId()).toString());
      salesOrderLine.setDlmgtoStore(getMagentoContext().getStore());
      salesOrder.getOrderLineList().add(salesOrderLine);
      lineNo += 10;
    } else {
      lineNo = salesOrderLine.getLineNo();
    }

    Product product = getMagentoContext().findOBObject(Product.class,
        Product.PROPERTY_DLMGTOREFERENCE, Long.valueOf(magentoOrderItem.getProductId()).toString());

    if (product == null) {
      if (getMagentoContext().getStore().isForced())
        product = getMagentoContext().getProductService()
            .getOBObjectFromMagento(getMagentoContext().getRemoteServiceFactory()
                .getProductRemoteService().getById(magentoOrderItem.getProductId()));
      else
        throw new Exception("Invalid Product id " + magentoOrderItem.getProductId());
    }

    BigDecimal qtyOrdered = new BigDecimal(
        magentoOrderItem.getQtyOrdered() != null ? magentoOrderItem.getQtyOrdered() : 0d);
    BigDecimal price = new BigDecimal(
        magentoOrderItem.getPrice() != null ? magentoOrderItem.getPrice() : 0d);
    BigDecimal discAmount = new BigDecimal(
        magentoOrderItem.getAllProperties().get("base_discount_amount") != null
            ? magentoOrderItem.getAllProperties().get("base_discount_amount").toString() : "");
    // BigDecimal cost = new BigDecimal(
    // magentoOrderItem.getBaseCost() != null ? magentoOrderItem.getBaseCost() : 0d);

    salesOrderLine.setSalesOrder(salesOrder);
    salesOrderLine.setOrderDate(salesOrder.getOrderDate());
    salesOrderLine.setProduct(product);
    salesOrderLine.setBusinessPartner(salesOrder.getBusinessPartner());
    salesOrderLine.setDescription(salesOrder.getDescription());

    salesOrderLine.setLineNo(lineNo);
    salesOrderLine.setWarehouse(salesOrder.getWarehouse());
    salesOrderLine.setCurrency(salesOrder.getCurrency());
    salesOrderLine.setUOM(product.getUOM());
    salesOrderLine.setOrderedQuantity(qtyOrdered);
    salesOrderLine.setDeliveredQuantity(BigDecimal.ZERO);
    salesOrderLine.setInvoicedQuantity(BigDecimal.ZERO);
    salesOrderLine.setListPrice(price.setScale(2, RoundingMode.HALF_UP));
    salesOrderLine.setUnitPrice(price.setScale(2, RoundingMode.HALF_UP));

    try {
      if (discAmount != null) {
        double origNetUnitPrice = salesOrderLine.getUnitPrice().doubleValue();
        double netPrice = new BigDecimal(salesOrderLine.getUnitPrice().doubleValue()
            * salesOrderLine.getOrderedQuantity().doubleValue()).setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
        double discount = discAmount.doubleValue();
        double disPercentage = 100d * discount / (netPrice);
        double unitDiscount = new BigDecimal(
            discount / salesOrderLine.getOrderedQuantity().doubleValue()).doubleValue();
        salesOrderLine.setUnitPrice(new BigDecimal(origNetUnitPrice - (unitDiscount)));
        salesOrderLine.setDiscount(new BigDecimal(disPercentage).setScale(2, RoundingMode.HALF_UP));
      }
    } catch (Exception e) {
    }

    salesOrderLine
        .setStandardPrice(salesOrderLine.getListPrice().setScale(2, RoundingMode.HALF_UP));
    salesOrderLine.setPriceLimit(salesOrderLine.getListPrice().setScale(2, RoundingMode.HALF_UP));
    salesOrderLine.setFreightAmount(BigDecimal.ZERO);
    salesOrderLine.setLineNetAmount(price.setScale(2, RoundingMode.HALF_UP));

    if (product.isStocked()) {
      debug("Verify stock ....");
      debug("****  Qty Ordered   : " + qtyOrdered);
      BigDecimal qtyAvailable = getQtyAvailable(salesOrder.getWarehouse(), product);
      debug("****  Qty Available : " + qtyAvailable);
      BigDecimal qtyOnHand = getQtyOnHand(salesOrder.getWarehouse(), product);
      debug("****  Qty On Hand   : " + qtyOnHand);
      BigDecimal qtyReserved = getQtyReserved(salesOrder.getWarehouse(), product);
      debug("****  Qty Reserved  : " + qtyReserved);

      if (qtyAvailable.doubleValue() < qtyOrdered.doubleValue()) {
        switch (stockMode) {
        case RESERVATION:
          debug("WANING Product " + product.getName()
              + " not in stock , setting Create Reservation to CRP");
          salesOrderLine.setCreateReservation("CRP");
          break;
        case THROW:
          throw new Exception("Product " + product.getName() + " not in stock ...");
        case WARN:
          debug("WANING Product " + product.getName() + " not in stock ...");
          break;
        }
      }
    }
    if (taxRate == null)
      throw new Exception("Tax Rate is null");
    debug("Setting tax ..." + taxRate);
    salesOrderLine.setTax(taxRate);
    getMagentoContext().getSalesOrderLineMapper().mapMagentoToOB(magentoOrderItem, salesOrderLine);
    return salesOrderLine;
  }

  private void createShippingAngHandling(Order salesOrder,
      com.dl.openbravo.magento.client.soap.v1.model.order.Order magentoOrder) throws Exception {
    if (magentoOrder.getShippingAmount() == null
        || magentoOrder.getShippingAmount().doubleValue() == 0)
      return;
    if (getMagentoContext().getStore().getSAHProduct() == null)
      throw new Exception("SAH Product is missing...");

    OrderLine sahLine = null;

    for (OrderLine ol : salesOrder.getOrderLineList())
      if (ol.getProduct().getId() != null
          && getMagentoContext().getStore().getSAHProduct().getId().equals(ol.getProduct().getId()))
        sahLine = ol;

    if (sahLine == null) {
      sahLine = getMagentoContext().getNewOBInstance(OrderLine.class,
          getMagentoContext().getStore().getOrderOrg());
      sahLine.setSalesOrder(salesOrder);
      sahLine.setLineNo((salesOrder.getOrderLineList().size() * 10L) + 10L);
      salesOrder.getOrderLineList().add(sahLine);
    }
    sahLine.setProduct(getMagentoContext().getStore().getSAHProduct());
    sahLine.setTax(getMagentoContext().getStore().getSalesTax());

    sahLine.setOrderDate(salesOrder.getOrderDate());
    sahLine.setWarehouse(salesOrder.getWarehouse());
    sahLine.setCurrency(salesOrder.getCurrency());
    sahLine.setUOM(getMagentoContext().getStore().getSAHProduct().getUOM());
    sahLine.setOrderedQuantity(new BigDecimal(1d));
    sahLine.setDeliveredQuantity(new BigDecimal(0));
    sahLine.setInvoicedQuantity(new BigDecimal(0));

    BigDecimal amount = new BigDecimal(magentoOrder.getShippingAmount());

    sahLine.setListPrice(amount);
    sahLine.setUnitPrice(amount);
    sahLine.setStandardPrice(amount);
    sahLine.setPriceLimit(amount);

  }

  private void createCreditOrGiftCardDiscount(Order salesOrder,
      com.dl.openbravo.magento.client.soap.v1.model.order.Order magentoOrder) throws Exception {

    double subtotal = 0;
    try {
      subtotal = magentoOrder.getSubtotal().doubleValue();
    } catch (Exception e) {
      subtotal = 0;
    }
    double shipping = 0;
    try {
      shipping = magentoOrder.getShippingAmount().doubleValue();
    } catch (Exception e) {
      shipping = 0;
    }
    double discount = 0;
    try {
      discount = Double
          .parseDouble(magentoOrder.getAllProperties().get("base_discount_amount").toString());
    } catch (Exception e) {
      discount = 0;
    }
    double tax = 0;
    try {
      tax = magentoOrder.getTaxAmount().doubleValue();
    } catch (Exception e) {
      tax = 0;
    }
    double giftcard = 0;
    try {
      giftcard = Double
          .parseDouble(magentoOrder.getAllProperties().get("gift_cards_amount").toString());
    } catch (Exception e) {
      giftcard = 0;
    }
    double credit = 0;
    try {
      credit = Double
          .parseDouble(magentoOrder.getAllProperties().get("customer_balance_amount").toString());
    } catch (Exception e) {
      credit = 0;
    }

    double total = new BigDecimal(subtotal + shipping + discount + tax)
        .setScale(2, RoundingMode.HALF_UP).doubleValue();
    double discountPercentage = Math.abs((giftcard - credit) * 100 / total);

    if (discountPercentage == 0)
      return;

    debug("subtotal:" + subtotal);
    debug("shipping:" + shipping);
    debug("discount:" + discount);
    debug("tax:" + tax);
    debug("total:" + (total));
    debug("giftcard:" + giftcard);
    debug("credit:" + credit);
    debug("discountPercentage:" + discountPercentage);

    if (getMagentoContext().getStore().getDISProduct() == null)
      throw new Exception("Discount Product is missing...");

    OrderLine dicountLine = null;

    for (OrderLine ol : salesOrder.getOrderLineList())
      if (ol.getProduct().getId() != null
          && getMagentoContext().getStore().getDISProduct().getId().equals(ol.getProduct().getId()))
        dicountLine = ol;

    if (dicountLine == null) {
      dicountLine = getMagentoContext().getNewOBInstance(OrderLine.class,
          getMagentoContext().getStore().getOrderOrg());
      dicountLine.setSalesOrder(salesOrder);
      dicountLine.setLineNo((salesOrder.getOrderLineList().size() * 10L) + 10L);
      salesOrder.getOrderLineList().add(dicountLine);
    }
    dicountLine.setProduct(getMagentoContext().getStore().getDISProduct());
    dicountLine.setTax(getMagentoContext().getStore().getSalesTax());

    dicountLine.setOrderDate(salesOrder.getOrderDate());
    dicountLine.setWarehouse(salesOrder.getWarehouse());
    dicountLine.setCurrency(salesOrder.getCurrency());
    dicountLine.setUOM(getMagentoContext().getStore().getDISProduct().getUOM());
    dicountLine.setOrderedQuantity(new BigDecimal(1d));
    dicountLine.setDeliveredQuantity(new BigDecimal(0));
    dicountLine.setInvoicedQuantity(new BigDecimal(0));

    double discamount = (credit == 0) ? giftcard : credit;
    if (discamount > 0)
      discamount *= -1;

    BigDecimal amount = new BigDecimal(discamount);

    dicountLine.setListPrice(amount);
    dicountLine.setUnitPrice(amount);
    dicountLine.setStandardPrice(amount);
    dicountLine.setPriceLimit(amount);

  }

  public TaxRate getTaxRate(Order salesOrder,
      com.dl.openbravo.magento.client.soap.v1.model.order.Order magentoOrder) throws Exception {
    if (!getMagentoContext().getStore().isTaxRateAutocreate())
      // || getMagentoContext().getStore().isTaxRateAutocreate())
      return getMagentoContext().getStore().getTax();
    if (getMagentoContext().getStore().isRegionCityNames())
      throw new Exception("Dynamic tax rates can't be used with Region & City Names");
    if (salesOrder.getInvoiceAddress().getLocationAddress().getRegion() == null
        || salesOrder.getInvoiceAddress().getLocationAddress().getRegion().getName() == null)
      throw new Exception("Invalid region for tax rate");
    if (salesOrder.getInvoiceAddress().getLocationAddress().getPostalCode() == null
        || salesOrder.getInvoiceAddress().getLocationAddress().getPostalCode().trim().isEmpty())
      throw new Exception("Invalid postal code for tax rate");

    String taxRateName = "MGTO Tax Rate For "
        + salesOrder.getInvoiceAddress().getLocationAddress().getRegion().getName() + " - "
        + salesOrder.getInvoiceAddress().getLocationAddress().getPostalCode().trim();

    Criterion taxRateCriteria = Restrictions.and(
        Restrictions.eq(TaxRate.PROPERTY_NAME, taxRateName),
        Restrictions.eq(TaxRate.PROPERTY_REGION,
            salesOrder.getInvoiceAddress().getLocationAddress().getRegion()));
    TaxRate taxRate = getMagentoContext().findOBObject(TaxRate.class, taxRateCriteria);

    if (taxRate == null) {
      debug("Tax rate '" + taxRateName + "' not found, creating new one...");
      taxRate = getMagentoContext().getNewOBInstance(TaxRate.class,
          getMagentoContext().getStore().getOrderOrg());
      // taxRate.setOrganization(
      // getMagentoContext().findOBObject(Organization.class, Organization.PROPERTY_NAME, "*"));
      taxRate.setOrganization(getMagentoContext().getStore().getTaxCategory().getOrganization());
      taxRate
          .setCountry(salesOrder.getInvoiceAddress().getLocationAddress().getRegion().getCountry());
      // taxRate.setDestinationCountry(
      // salesOrder.getInvoiceAddress().getLocationAddress().getRegion().getCountry());
      taxRate.setRegion(salesOrder.getInvoiceAddress().getLocationAddress().getRegion());
      // taxRate.setDestinationRegion(salesOrder.getInvoiceAddress().getLocationAddress().getRegion());
      taxRate.setCascade(false);
      taxRate.setSummaryLevel(false);
      taxRate.setSalesPurchaseType("B");
      taxRate.setDocTaxAmount("D");
      taxRate.setBaseAmount("LNA");
      taxRate.setValidFromDate(getMagentoContext().getStore().getUpdateFrom());
      taxRate.setName(taxRateName);
      taxRate.setTaxCategory(getMagentoContext().getStore().getTaxCategory());
      // taxRate.setBusinessPartnerTaxCategory(getMagentoContext().getStore().getBpTaxcategory());
    }

    double rate = 0d;
    double discountAmount = 0d;
    double subtotalAmount = 0d;
    double taxAmount = 0d;

    try {
      taxAmount = magentoOrder.getTaxAmount();
      subtotalAmount = magentoOrder.getSubtotal();
      discountAmount = magentoOrder.getDiscountAmount();
    } catch (Exception e) {
    }

    debug("Calc. new rate ");
    debug("taxAmount : " + taxAmount);
    debug("subtotalAmount : " + subtotalAmount);
    debug("discountAmount : " + discountAmount);

    try {
      rate = taxAmount * 100 / (subtotalAmount + discountAmount);
    } catch (Exception e) {
    }

    debug("Updating tax rate '" + taxRateName + "' to " + rate);
    taxRate.setRate(new BigDecimal(rate).setScale(2, RoundingMode.HALF_UP));

    return taxRate;
  }

  public String getNextDocumentNumber() {
    Sequence localSequence = getMagentoContext().findOBObject(Sequence.class, Sequence.PROPERTY_ID,
        getMagentoContext().getStore().getSequence().getId());
    if (localSequence == null) {
      return null;
    }
    Long localLong = localSequence.getNextAssignedNumber();
    localSequence.setNextAssignedNumber(Long.valueOf(localLong.longValue() + 1L));
    OBDal.getInstance().save(localSequence);

    return ((localSequence.getPrefix() == null) ? "" : localSequence.getPrefix().trim())
        + (Long.valueOf(localLong == null ? 0L : localLong.longValue()))
        + ((localSequence.getSuffix() == null) ? "" : localSequence.getSuffix().trim());
  }

  public BigDecimal getQtyAvailable(Warehouse paramWarehouse, Product paramProduct) {
    List<Object> localArrayList = new ArrayList<Object>();
    localArrayList.add(paramProduct.getId());
    localArrayList.add(paramWarehouse.getId());
    return (BigDecimal) CallStoredProcedure.getInstance().call(QTY_STOCK_FUNCTION_AVAILABLE,
        localArrayList, null, false, true);
  }

  public BigDecimal getQtyOrdered(Warehouse paramWarehouse, Product paramProduct) {
    List<Object> localArrayList = new ArrayList<Object>();
    localArrayList.add(paramProduct.getId());
    localArrayList.add(paramWarehouse.getId());
    return (BigDecimal) CallStoredProcedure.getInstance().call(QTY_STOCK_FUNCTION_ORDERED,
        localArrayList, null, false, true);
  }

  public BigDecimal getQtyOnHand(Warehouse paramWarehouse, Product paramProduct) {
    List<Object> localArrayList = new ArrayList<Object>();
    localArrayList.add(paramProduct.getId());
    localArrayList.add(paramWarehouse.getId());
    return (BigDecimal) CallStoredProcedure.getInstance().call(QTY_STOCK_FUNCTION_ONHAND,
        localArrayList, null, false, true);
  }

  public BigDecimal getQtyReserved(Warehouse paramWarehouse, Product paramProduct) {
    List<Object> localArrayList = new ArrayList<Object>();
    localArrayList.add(paramProduct.getId());
    localArrayList.add(paramWarehouse.getId());
    return (BigDecimal) CallStoredProcedure.getInstance().call(QTY_STOCK_FUNCTION_RESERVERD,
        localArrayList, null, false, true);
  }

  public Order saveSalesOrderInOB(Order salesOrder) throws Exception {

    for (OrderLine orderLine : salesOrder.getOrderLineList()) {
      if (orderLine.getTax() != null)
        OBDal.getInstance().save(orderLine.getTax());
      getMagentoContext().getProductService().saveProductInOB(orderLine.getProduct());
    }
    getMagentoContext().getCustomerService().saveCustomerInOB(salesOrder.getBusinessPartner());

    OBDal.getInstance().save(salesOrder);

    for (OrderLine orderLine : salesOrder.getOrderLineList()) {
      OBDal.getInstance().save(orderLine);
    }

    return salesOrder;
  }

  public void completeOrder(Order order) {
    debug("Completing OB Order " + order.getDocumentNo());
    List<Object> localArrayList = new ArrayList<Object>();
    localArrayList.add(null);
    localArrayList.add(order.getId());
    CallStoredProcedure.getInstance().call("c_order_post1", localArrayList, null, false, false);
  }

  public void addCommentToOrder(String magentoOrderId, String status, String message)
      throws Exception {
    debug("Adding comment to Magento Order " + magentoOrderId);
    List<Object> params = new LinkedList<Object>();
    params.add(magentoOrderId);
    params.add(status);
    params.add(message);
    params.add(new Integer(0));
    getMagentoContext().getSoapClient().call(ResourcePath.SalesOrderAddComment.getPath(), params);

  }

  public void setOrderToProcessing(String magentoOrderId, final String message) throws Exception {
    debug("Setting to 'Processing' Magento Order " + magentoOrderId);
    addCommentToOrder(magentoOrderId, "processing", message);
  }

  public void setOrderToHold(String magentoOrderId, String message) throws Exception {
    debug("Setting to 'Holded' Magento Order " + magentoOrderId);
    addCommentToOrder(magentoOrderId, "holded", message);
  }

}
