package com.dl.openbravo.magento.service;

import org.openbravo.base.structure.BaseOBObject;

import com.dl.openbravo.magento.MagentoContext;

public abstract class MagentoService<OBCLASS extends BaseOBObject, MGTOCLASS> {

  protected static final String STR_LOADING = "Reading ";
  protected static final String STR_UPDATE = "Updating ";
  protected static final String STR_REF = "Sync - Magento Ref = ";
  protected static final String STR_CREATE = "Creating ";

  private Class<OBCLASS> obClass;

  private Class<MGTOCLASS> magentoClass;

  private MagentoContext magentoContext;

  public MagentoService(MagentoContext magentoContext, Class<OBCLASS> obClass,
      Class<MGTOCLASS> magentoClass) {
    super();
    this.magentoContext = magentoContext;
    this.obClass = obClass;
    this.magentoClass = magentoClass;
  }

  public void debug(String string) {
    getMagentoContext().debug(getClass().getSimpleName() + " - " + string);
  }

  public abstract OBCLASS getOBObjectFromMagento(MGTOCLASS magentoObject) throws Exception;

  public Class<OBCLASS> getObClass() {
    return obClass;
  }

  public Class<MGTOCLASS> getMagentoClass() {
    return magentoClass;
  }

  public MagentoContext getMagentoContext() {
    return magentoContext;
  }

}
