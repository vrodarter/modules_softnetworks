package com.dl.openbravo.magento.service;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.dal.service.OBDal;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.packing.PackingBox;

import com.dl.openbravo.magento.MagentoContext;
import com.dl.openbravo.magento.client.soap.v1.model.order.Shipment;
import com.dl.openbravo.magento.client.soap.v1.model.order.ShipmentItem;
import com.dl.openbravo.magento.client.soap.v1.model.order.ShipmentTrack;

public class MagentoShippingService extends MagentoService<Packing, Shipment> {

  public MagentoShippingService(MagentoContext magentoContext) {
    super(magentoContext, Packing.class, Shipment.class);
  }

  @Override
  public Packing getOBObjectFromMagento(Shipment magentoObject) throws Exception {
    return null;
  }

  public Shipment getMagentoObjectFromOB(PackingBox obPackingBox) throws Exception {

    Shipment shipment = new Shipment();
    shipment.setOrderNumber(obPackingBox.getGoodsShipment().getSalesOrder().getDlmgtoReference());
    List<ShipmentItem> items = new ArrayList<ShipmentItem>();

    shipment.setItems(items);
    getMagentoContext().getRemoteServiceFactory().getShipmentRemoteService().save(shipment, "",
        false, false);

    ShipmentTrack track = new ShipmentTrack();
    if (obPackingBox.getTrackingNo().length() <= 14)
      track.setCarrier("fedex");
    else if (obPackingBox.getTrackingNo().length() <= 20)
      track.setCarrier("ups");
    else
      track.setCarrier("usps");
    track.setNumber(obPackingBox.getTrackingNo());
    track.setTitle("SHIPPING");

    getMagentoContext().getRemoteServiceFactory().getShipmentRemoteService().addTrack(shipment,
        track);

    return shipment;
  }

  public Packing savePackingInOB(Packing packing) throws Exception {
    OBDal.getInstance().save(packing);
    return packing;
  }

}
