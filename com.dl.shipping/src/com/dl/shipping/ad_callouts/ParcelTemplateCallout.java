package com.dl.shipping.ad_callouts;

import javax.servlet.ServletException;

import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;

import com.dl.shipping.data.DLSHTemplate;

public class ParcelTemplateCallout extends SimpleCallout {

  private static final long serialVersionUID = 1L;

  @Override
  protected void execute(CalloutInfo info) throws ServletException {

    String templateId = info.getStringParameter("inpemDlshTemplateId", null);

    DLSHTemplate template = OBDal.getInstance().get(DLSHTemplate.class, templateId);

    if (template != null) {
      info.addResult("inpemDlshHeight", template.getParcelHeight());
      info.addResult("inpemDlshWidth", template.getParcelWidth());
      info.addResult("inpemDlshLength", template.getParcelLength());
      if (template.getUom() != null)
        info.addResult("inpemDlshDistUomId", template.getUom().getId());
    }

  }

}
