package com.dl.shipping.process;

import java.sql.BatchUpdateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;

public abstract class BaseProcess extends DalBaseProcess {

  public static final String STR_SUCCESS = "Success";
  public static final String STR_WARN = "Warning";
  public static final String STR_ERROR = "Error";

  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

  private static final String STR_BO = "[";
  private static final String STR_BC = "]";
  private static final String STR_SCORE = " - ";
  private static final String STR_COLON = " : ";
  private static final String STR_FIND_OB_LOOKING = "Looking for : ";
  private static final String STR_FIND_OB_NOT_FOUND = " Not Found ";

  private final StringBuilder debugFindStringBuilder = new StringBuilder();
  private StringBuilder warnSb;
  private final StringBuilder logSb = new StringBuilder();
  private final StringBuilder logErrorSb = new StringBuilder();

  private ProcessBundle bundle;
  private ProcessLogger logger;

  private final Date logDate = new Date();
  private final OBError msg = new OBError();

  @Override
  protected void doExecute(ProcessBundle pbundle) throws Exception {
    setBundle(pbundle);
    setLogger(pbundle.getLogger());
    msg.setType(STR_SUCCESS);
    msg.setMessage(STR_SUCCESS);
    OBContext.setAdminMode();
    try {
      run();
      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
      if (warnSb != null) {
        getMsg().setType(STR_WARN);
        getMsg().setMessage(warnSb.toString());
      }
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      e.printStackTrace();
      debug(e);
      msg.setType(STR_ERROR);
      msg.setMessage(e.getMessage());
    } finally {
      bundle.setResult(msg);
      OBContext.restorePreviousMode();
    }

  }

  public abstract void run() throws Exception;

  public abstract Organization getOBOrganization();

  public abstract Client getOBClient();

  public OBError getMsg() {
    return msg;
  }

  public void appendWarn(String warn) {
    if (warnSb == null)
      warnSb = new StringBuilder("Details:\n");
    warnSb.append(warn).append("\n");
  }

  public void debug(String string, boolean obLogger) {
    logSb.setLength(0);
    logDate.setTime(System.currentTimeMillis());
    logSb.append(STR_BO).append(sdf.format(logDate)).append(STR_BC).append(STR_SCORE)
        .append(STR_SCORE).append(string);
    System.out.println(logSb);
    if (obLogger)
      logger.logln(logSb.toString());
  }

  public void debug(Throwable throwable) {
    logErrorSb.setLength(0);
    if (throwable != null) {
      if (throwable.getCause() != null)
        debug(throwable.getCause());
      debug(logErrorSb.append(STR_ERROR).append(STR_COLON).append(throwable.getClass().getName())
          .append(STR_COLON).append(throwable.getMessage()).toString(), true);
    }
    if (throwable instanceof BatchUpdateException)
      debug(((BatchUpdateException) throwable).getNextException());

  }

  public <T extends BaseOBObject> T getNewOBInstance(Class<T> clazz) {
    T obj = OBProvider.getInstance().get(clazz);

    if (obj instanceof ClientEnabled) {
      ((ClientEnabled) obj).setClient(getOBClient());
    }
    if (obj instanceof OrganizationEnabled) {
      ((OrganizationEnabled) obj).setOrganization(getOBOrganization());
    }
    if (obj instanceof ActiveEnabled) {
      ((ActiveEnabled) obj).setActive(true);
    }
    if (obj instanceof Traceable) {
      ((Traceable) obj).setCreatedBy(OBContext.getOBContext().getUser());
      ((Traceable) obj).setCreationDate(new Date());
      ((Traceable) obj).setUpdatedBy(OBContext.getOBContext().getUser());
      ((Traceable) obj).setUpdated(new Date());
    }
    return obj;
  }

  public <T extends BaseOBObject> T findOBObject(Class<T> clazz, Criterion criterion) {
    OBCriteria<T> criteria = OBDal.getInstance().createCriteria(clazz);
    criteria.add(criterion);
    try {
      return criteria.list().get(0);
    } catch (Exception e) {
      debugFindStringBuilder.setLength(0);
      debugFindStringBuilder.append(clazz.getName()).append(STR_FIND_OB_NOT_FOUND);
      debug(debugFindStringBuilder.toString(), true);
      return null;
    }
  }

  public <T extends BaseOBObject> T findOBObject(Class<T> clazz, List<Criterion> criterions) {
    OBCriteria<T> criteria = OBDal.getInstance().createCriteria(clazz);
    for (Criterion criterion : criterions) {
      criteria.add(criterion);
    }
    try {
      return criteria.list().get(0);
    } catch (Exception e) {
      debugFindStringBuilder.setLength(0);
      debugFindStringBuilder.append(clazz.getName()).append(STR_FIND_OB_NOT_FOUND);
      debug(debugFindStringBuilder.toString(), true);
      return null;
    }
  }

  public <T extends BaseOBObject> T findOBObject(Class<T> clazz, Map<String, Object> props) {
    debugFindStringBuilder.setLength(0);
    debugFindStringBuilder.append(STR_FIND_OB_LOOKING).append(clazz.getName());
    debug(debugFindStringBuilder.toString(), false);
    List<Criterion> criterions = new ArrayList<Criterion>();
    for (Entry<String, Object> property : props.entrySet()) {
      debugFindStringBuilder.setLength(0);
      debugFindStringBuilder.append(property.getKey()).append(" : ").append(property.getValue());
      debug(debugFindStringBuilder.toString(), false);
      criterions.add(Restrictions.eq(property.getKey(), property.getValue()));
    }
    return findOBObject(clazz, criterions);
  }

  public <T extends BaseOBObject> T findOBObject(Class<T> clazz, String property, Object value) {
    debugFindStringBuilder.setLength(0);
    debugFindStringBuilder.append(STR_FIND_OB_LOOKING).append(clazz.getName());
    debug(debugFindStringBuilder.toString(), false);
    debugFindStringBuilder.setLength(0);
    debugFindStringBuilder.append(property).append(" : ").append(value);
    debug(debugFindStringBuilder.toString(), false);
    List<Criterion> criterions = new ArrayList<Criterion>();
    criterions.add(Restrictions.eq(property, value));
    return findOBObject(clazz, criterions);
  }

  public ProcessBundle getBundle() {
    return bundle;
  }

  public ProcessLogger getLogger() {
    return logger;
  }

  public void setBundle(ProcessBundle bundle) {
    this.bundle = bundle;
  }

  public void setLogger(ProcessLogger logger) {
    this.logger = logger;
  }

}
