package com.dl.shipping.process;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.TabAttachments;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Attachment;
import org.openbravo.model.ad.utility.Image;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.warehouse.packing.PackingBoxProduct;

import com.dl.shipping.api.carrier.CarrierClient;
import com.dl.shipping.api.fedex.FedExClient;
import com.dl.shipping.api.model.Activity;
import com.dl.shipping.api.model.Address;
import com.dl.shipping.api.model.Currency;
import com.dl.shipping.api.model.Item;
import com.dl.shipping.api.model.Label;
import com.dl.shipping.api.model.Parcel;
import com.dl.shipping.api.model.ParcelContentsType;
import com.dl.shipping.api.model.Rate;
import com.dl.shipping.api.model.Shipment;
import com.dl.shipping.api.model.SubmissionType;
import com.dl.shipping.api.model.UOM;
import com.dl.shipping.api.ups.UPSClient;
import com.dl.shipping.api.usps.USPSClient;
import com.dl.shipping.data.DLSHCarrierProperty;
import com.dl.shipping.data.DLSHLabel;
import com.dl.shipping.data.DLSHRate;
import com.dl.shipping.data.DLSHShipping;
import com.dl.shipping.data.DLSHTracking;

public abstract class BaseShipmentProcess extends BaseProcess {

  private static final String ADDRESS_VALIDATION_ERROR_FORMAT = "The value \"%s\" in field \"%s\" on %s address is not valid, maybe the correct value is \"%s\"";

  private static final String SHIPPING_ID_PARAM = "Dlsh_Shipping_ID";

  private DLSHShipping obShipping;

  private Shipment shipment;

  private Currency currency;
  private CarrierClient carrierClient;

  @Override
  public void run() throws Exception {

    obShipping = OBDal.getInstance().get(DLSHShipping.class,
        getBundle().getParams().get(SHIPPING_ID_PARAM));
    try {
      currency = Currency.valueOf(getObShipping().getCurrency().getISOCode());
    } catch (Exception e) {
      currency = Currency.USD;
    }
    if (getCarrierClient() == null)
      throw new Exception(
          "No carrier client for " + obShipping.getDlshCarrierConfig().getCarrierType());

    exec();

  }

  public abstract void exec() throws Exception;

  public DLSHShipping getObShipping() {
    return obShipping;
  }

  public Shipment getShipment() {

    if (shipment == null) {
      shipment = new Shipment();
      shipment.setShipFrom(getShipmentFromAddress());
      shipment.setShipper(getShipmentFromAddress());
      shipment.setShipTo(getShipmentToAddress());
      shipment.setSubmissionDate(getObShipping().getShipmentDate());
      shipment.setSubmissionType(SubmissionType.PICKUP);
      shipment.setRate(getSelectedRate());
      shipment.setParcel(getParcel());

      if (getObShipping().getInsuranceAmount() != null)
        shipment.setInsuranceAmount(getObShipping().getInsuranceAmount().doubleValue());
      shipment.setInsuranceAmountCurrency(getCurrency());

      if (getObShipping().getTotalAmount() != null)
        shipment.setTotalAmount(getObShipping().getTotalAmount().doubleValue());
      shipment.setTotalAmountCurrency(getCurrency());

      shipment.setTrackingNumber(getObShipping().getTrackingNumber());
    }
    return shipment;
  }

  public Address getShipmentFromAddress() {
    Address address = new Address();
    
    try {
    
   address.setAddressLine1(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getAddressLine1());
      address.setAddressLine2(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getAddressLine2() == null ? ""
              : getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
                  .getSnmtSalesChannel().getAddressLine2());
      address.setAddressLine3(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getAddressLine3() == null ? ""
              : getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
                  .getSnmtSalesChannel().getAddressLine3());
      address.setCity(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getCity());
      address.setRegionCode(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getRegionCode());
      address.setRegionName(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getRegionName());
      address.setCountryCode(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getCountryCode());
      address.setCountryName(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getCountryName());
      address.setPostalCode(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getPostalCode());
      address.setCompany(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getCompany());
      address.setFirstName(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getFirstName());
      address.setLastName(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getLastName());
      address.setEmail(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getEmail());
      address.setPhoneNumber(getObShipping().getObwpackBox().getGoodsShipment().getSalesOrder()
          .getSnmtSalesChannel().getPhoneNumber()); 

    } catch (Exception e) {

    address.setAddressLine1(getObShipping().getDlshCarrierConfig().getAddressLine1());
    address.setAddressLine2(getObShipping().getDlshCarrierConfig().getAddressLine2() == null ? ""
        : getObShipping().getDlshCarrierConfig().getAddressLine2());
    address.setAddressLine3(getObShipping().getDlshCarrierConfig().getAddressLine3() == null ? ""
        : getObShipping().getDlshCarrierConfig().getAddressLine3());
    address.setCity(getObShipping().getDlshCarrierConfig().getCity());
    address.setRegionCode(getObShipping().getDlshCarrierConfig().getRegionCode());
    address.setRegionName(getObShipping().getDlshCarrierConfig().getRegionName());
    address.setCountryCode(getObShipping().getDlshCarrierConfig().getCountryCode());
    address.setCountryName(getObShipping().getDlshCarrierConfig().getCountryName());
    address.setPostalCode(getObShipping().getDlshCarrierConfig().getPostalCode());
    address.setCompany(getObShipping().getDlshCarrierConfig().getCompany());
    address.setFirstName(getObShipping().getDlshCarrierConfig().getFirstName());
    address.setLastName(getObShipping().getDlshCarrierConfig().getLastName());
    address.setEmail(getObShipping().getDlshCarrierConfig().getEmail());
    address.setPhoneNumber(getObShipping().getDlshCarrierConfig().getPhoneNumber());
    address.setFaxNumber(getObShipping().getDlshCarrierConfig().getFAXNumber());
    address.setResidential(getObShipping().getDlshCarrierConfig().isResidential());
}   
 return address;
  }

  public Address getShipmentToAddress() {
    Address address = new Address();
    address.setAddressLine1(getObShipping().getAddressLine1());
    address.setAddressLine2(
        getObShipping().getAddressLine2() == null ? "" : getObShipping().getAddressLine2());
    address.setAddressLine3(
        getObShipping().getAddressLine3() == null ? "" : getObShipping().getAddressLine3());
    address.setCity(getObShipping().getCity());
    address.setRegionCode(getObShipping().getRegionCode());
    address.setRegionName(getObShipping().getRegionName());
    address.setCountryCode(getObShipping().getCountryCode());
    address.setCountryName(getObShipping().getCountryName());
    address.setPostalCode(getObShipping().getPostalCode());
    address.setCompany(getObShipping().getCompany());
    address.setFirstName(getObShipping().getFirstName());
    address.setLastName(getObShipping().getLastName());
    address.setEmail(getObShipping().getEmail());
    address.setPhoneNumber(getObShipping().getPhoneNumber());
    address.setFaxNumber(getObShipping().getFAXNumber());
    address.setResidential(getObShipping().isResidential());
    return address;
  }

  public Rate getSelectedRate() {
    for (DLSHRate obRate : getObShipping().getDlshRateList())
      if (obRate.isSelected())
        return getRate(obRate);
    return null;
  }

  public Rate getRate(DLSHRate obRate) {
    Rate rate = new Rate();
    rate.setCode(obRate.getServiceCode());
    rate.setName(obRate.getName());
    return rate;
  }

  public Parcel getParcel() {
    Parcel parcel = new Parcel();
    for (PackingBoxProduct product : getObShipping().getObwpackBox().getOBWPACKBoxProductList())
      parcel.getItems().add(getItem(product));

    if (getObShipping().getObwpackBox().getDlshWidth() != null)
      parcel.setWidth(getObShipping().getObwpackBox().getDlshWidth().doubleValue());
    if (getObShipping().getObwpackBox().getDlshHeight() != null)
      parcel.setHeight(getObShipping().getObwpackBox().getDlshHeight().doubleValue());
    if (getObShipping().getObwpackBox().getDlshLength() != null)
      parcel.setLength(getObShipping().getObwpackBox().getDlshLength().doubleValue());

    parcel.setContentsExplanation(getObShipping().getObwpackBox().getDlshContentExp());

    try {
      parcel.setContentsType(
          ParcelContentsType.valueOf(getObShipping().getObwpackBox().getDlshContentType()));
    } catch (Exception e) {
      parcel.setContentsType(ParcelContentsType.MERCHANDISE);
    }

    parcel.setContentValueCurrency(getCurrency());
    parcel.setRectangular(getObShipping().getObwpackBox().isDlshIsrectangular());

    try {
      if (getObShipping().getObwpackBox().getDlshMassUom().getName().equals("Kilogram"))
        parcel.setMassUnit(UOM.KILO_GRAMS);
      else if (getObShipping().getObwpackBox().getDlshMassUom().getName().equals("Ounce"))
        parcel.setMassUnit(UOM.OZ);
      else
        parcel.setMassUnit(UOM.POUNDS);
    } catch (Exception e) {
      parcel.setMassUnit(UOM.POUNDS);
    }

    try {
      if (getObShipping().getObwpackBox().getDlshDistUom().getName().equals("Centimeter"))
        parcel.setDistanceUnit(UOM.CENTIMETERS);
      else
        parcel.setDistanceUnit(UOM.INCHES);
    } catch (Exception e) {
      parcel.setDistanceUnit(UOM.INCHES);
    }

    return parcel;
  }

  public Item getItem(PackingBoxProduct product) {
    Item item = new Item();
    if (product.getQuantity() != null)
      item.setQuantity(product.getQuantity().intValue());
    else
      item.setQuantity(1);
    if (product.getProduct().getWeight() != null)
      item.setWeight(product.getProduct().getWeight().doubleValue() * item.getQuantity());
    else
      item.setWeight(1);

    if (item.getWeight() <= 0 && product.getPackingBox().getWeight() != null)
      item.setWeight(product.getPackingBox().getWeight().doubleValue());

    if (product.getPackingBox().getDlshWeight() != null
        && product.getPackingBox().getDlshWeight().doubleValue() > 0)
      item.setWeight(product.getPackingBox().getDlshWeight().doubleValue());

    try {
      if (product.getProduct().getUOMForWeight().getName().equals("Kilogram"))
        item.setMassUnit(UOM.KILO_GRAMS);
      else if (product.getProduct().getUOMForWeight().getName().equals("Ounce"))
        item.setMassUnit(UOM.OZ);
      else
        item.setMassUnit(UOM.POUNDS);
    } catch (Exception e) {
      item.setMassUnit(UOM.POUNDS);
    }
    item.setDescription(product.getProduct().getName());
    try {
      double value = 1;
      for (ProductPrice pp : product.getProduct().getPricingProductPriceList())
        if (pp.getPriceListVersion().getPriceList().isSalesPriceList())
          value = pp.getListPrice().doubleValue();
      item.setValueAmount(value * item.getQuantity());
    } catch (Exception e) {
      item.setValueAmount(1 * item.getQuantity());
    }
    item.setValueCurrency(getCurrency());
    return item;
  }

  public Currency getCurrency() {
    return currency;
  }

  public CarrierClient getCarrierClient() {
    if (carrierClient == null) {
      if (getObShipping().getDlshCarrierConfig().getCarrierType().equals("UPS"))
        carrierClient = new UPSClient(createClientProperties());
      else if (getObShipping().getDlshCarrierConfig().getCarrierType().equals("USPS"))
        carrierClient = new USPSClient(createClientProperties());
      else if (getObShipping().getDlshCarrierConfig().getCarrierType().equals("FEDEX"))
        carrierClient = new FedExClient(createClientProperties());
    }
    return carrierClient;
  }

  public Properties createClientProperties() {
    Properties properties = new Properties();
    if (getObShipping().getDlshCarrierConfig().getTestUrl() != null)
      properties.put(CarrierClient.TESTING_URL_PROPERTY,
          getObShipping().getDlshCarrierConfig().getTestUrl()); // UPS
    if (getObShipping().getDlshCarrierConfig().getPorductionUrl() != null)
      properties.put(CarrierClient.PRODUCTION_URL_PROPERTY,
          getObShipping().getDlshCarrierConfig().getPorductionUrl()); // UPS
    if (getObShipping().getDlshCarrierConfig().getAuthKey() != null)
      properties.put(CarrierClient.AUTH_KEY_PROPERTY,
          getObShipping().getDlshCarrierConfig().getAuthKey());
    if (getObShipping().getDlshCarrierConfig().getUserName() != null)
      properties.put(CarrierClient.USER_ID_PROPERTY,
          getObShipping().getDlshCarrierConfig().getUserName()); // UPS
    if (getObShipping().getDlshCarrierConfig().getPassword() != null)
      properties.put(CarrierClient.PASSWORD_PROPERTY,
          getObShipping().getDlshCarrierConfig().getPassword());
    if (getObShipping().getDlshCarrierConfig().isDebug() != null)
      properties.put(CarrierClient.DEBUG_PROPERTY,
          "" + getObShipping().getDlshCarrierConfig().isDebug());
    if (getObShipping().getDlshCarrierConfig().isProduction() != null)
      properties.put(CarrierClient.PRODUCTION_MODE,
          "" + getObShipping().getDlshCarrierConfig().isProduction());
    if (getObShipping().getDlshCarrierConfig().getShipperNumber() != null)
      properties.put(CarrierClient.ACCOUNT_PROPERTY,
          getObShipping().getDlshCarrierConfig().getShipperNumber());

    String prefix = "";

    if (getObShipping().getDlshCarrierConfig().getCarrierType().equals("USPS"))
      prefix = "com.dl.shipping.api.carrier.usps.";
    else if (getObShipping().getDlshCarrierConfig().getCarrierType().equals("UPS"))
      prefix = "com.dl.shipping.api.carrier.ups.";
    else if (getObShipping().getDlshCarrierConfig().getCarrierType().equals("FEDEX"))
      prefix = "com.dl.shipping.api.carrier.fedex.";
    else if (getObShipping().getDlshCarrierConfig().getCarrierType().equals("DHL"))
      prefix = "com.dl.shipping.api.carrier.dhl.";

    for (DLSHCarrierProperty prop : getObShipping().getDlshCarrierConfig()
        .getDlshCarrierPropertyList()) {

      properties.put(prefix + prop.getName(), prop.getContent());
    }

    return properties;
  }

  public void validateAddresses() throws Exception {
    if (!getShipment().isInternational())
      compareAddress(getShipmentToAddress(),
          getCarrierClient().validateAddress(getShipmentToAddress()), "Ship To");
    else
      appendWarn("International Address Validadion is not supported.");
    compareAddress(getShipmentFromAddress(),
        getCarrierClient().validateAddress(getShipmentFromAddress()), "Ship From");
  }

  private void compareAddress(Address org, Address val, String addresType) throws Exception {
    compareAddressField(org.getCountryCode(), val.getCountryCode(), addresType, "Country Code",
        false);
    compareAddressField(org.getRegionCode(), val.getRegionCode(), addresType, "Region Code", false);
    compareAddressField(org.getCity(), val.getCity(), addresType, "City", true);
    compareAddressField(org.getPostalCode(), val.getPostalCode(), addresType, "Postal Code", true);
    compareAddressField(org.getAddressLine1(), val.getAddressLine1(), addresType, "Address Line 1",
        true);
  }

  private void compareAddressField(String org, String val, String addresType, String fieldName,
      boolean warn) throws Exception {
    boolean comparation = org.trim().equalsIgnoreCase(val.trim());

    if (!comparation) {
      String errorStr = String.format(ADDRESS_VALIDATION_ERROR_FORMAT, org, fieldName, addresType,
          val);
      if (warn)
        appendWarn(errorStr);
      else
        throw new Exception(errorStr);
    }

  }

  public void processShipment() throws Exception {
    int selectCount = 0;

    if (!getObShipping().getStatus().equals("NEW"))
      throw new Exception("This shipping is not new!");
    for (DLSHRate obRate : getObShipping().getDlshRateList()) {
      if (obRate.isSelected())
        selectCount++;
    }
    if (selectCount == 0)
      throw new Exception("No rate selected!");
    if (selectCount > 1)
      throw new Exception("Too many rates selected!");

    // validateAddresses();

    for (DLSHLabel label : getObShipping().getDlshLabelList())
      OBDal.getInstance().remove(label);

    Shipment localShipment = getCarrierClient().createShipment(getShipment());

    getObShipping().setTrackingNumber(localShipment.getTrackingNumber());
    getObShipping().getObwpackBox().setTrackingNo(localShipment.getTrackingNumber());
    getObShipping().setTotalAmount(new BigDecimal(localShipment.getTotalAmount()));

    for (Label label : localShipment.getLabels())
      processLabel(label);

    getObShipping().setStatus("IN_PROCESS");

    OBDal.getInstance().save(getObShipping().getObwpackBox());
    OBDal.getInstance().save(getObShipping());

  }

  private void processLabel(Label label) throws Exception {
    DLSHLabel obLabel = getNewOBInstance(DLSHLabel.class);

    obLabel.setName(label.getName());
    obLabel.setDescription(label.getDescription());
    obLabel.setType(label.getType());
    obLabel.setFormat(label.getFormat());
    obLabel.setDlshShipping(getObShipping());

    if (label.getFormat().equalsIgnoreCase("PNG") || label.getFormat().equalsIgnoreCase("GIF"))
      obLabel.setImage(getImageFromLabel(label));

    if (obLabel.getImage() != null)
      OBDal.getInstance().save(obLabel.getImage());
    OBDal.getInstance().save(obLabel);

    String path = obLabel.getEntity().getTableId() + "/"
        + TabAttachments.splitPath(obLabel.getId());
    Attachment attachment = getNewOBInstance(Attachment.class);
    attachment.setTable(OBDal.getInstance().get(Table.class, obLabel.getEntity().getTableId()));
    attachment.setName(label.getFileName());
    attachment.setRecord(obLabel.getId());
    attachment.setSequenceNumber(10l);
    attachment.setDataType(label.getFormat());
    attachment.setPath(path);

    OBDal.getInstance().save(attachment);

    path = OBPropertiesProvider.getInstance().getOpenbravoProperties().getProperty("attach.path")
        + "/" + path;
    System.out.println("FILE NAME:" + path);
    File f = new File(path);
    if (!f.exists())
      f.mkdirs();
    FileOutputStream fos = new FileOutputStream(path + "/" + label.getFileName());
    fos.write(label.getBinary());
    fos.close();
  }

  public Image getImageFromLabel(Label label) {
    try {
      ByteArrayInputStream bis = new ByteArrayInputStream(label.getBinary());
      BufferedImage rImage = ImageIO.read(bis);
      Image productImage = getNewOBInstance(Image.class);
      productImage.setName(label.getFileName());
      productImage.setBindaryData(label.getBinary());
      productImage.setWidth(new Long(rImage.getWidth()));
      productImage.setHeight(new Long(rImage.getHeight()));
      productImage.setMimetype("application/" + label.getFormat().toLowerCase());
      return productImage;

    } catch (Exception e) {
      debug(e);
      return null;
    }

  }

  public void updateTracking() throws Exception {

    getCarrierClient().track(getShipment());

    for (DLSHTracking tracking : getObShipping().getDlshTrackingList())
      OBDal.getInstance().remove(tracking);

    getObShipping().getDlshTrackingList().clear();

    for (Activity activity : getShipment().getActivities()) {
      DLSHTracking tracking = getNewOBInstance(DLSHTracking.class);
      tracking.setDlshShipping(getObShipping());
      tracking.setDescription(activity.getActivityDescription());
      OBDal.getInstance().save(tracking);
    }

  }

  public void voidShipment() throws Exception {
    if (!getObShipping().getStatus().equals("IN_PROCESS"))
      throw new Exception("This shipping is not in process!");

    getCarrierClient().voidShipment(getShipment());

    getObShipping().setStatus("CANCELED");
    OBDal.getInstance().save(getObShipping());

  }

  @Override
  public Organization getOBOrganization() {
    return getObShipping().getOrganization();
  }

  @Override
  public Client getOBClient() {
    return getObShipping().getClient();
  }

}
