package com.dl.shipping.process;

import org.openbravo.dal.service.OBDal;

import com.dl.shipping.data.DLSHRate;

public class CompleteProcess extends BaseShipmentProcess {

  @Override
  public void exec() throws Exception {

    if (!getObShipping().getStatus().equals("IN_PROCESS"))
      throw new Exception("This shipping is not in process!");

    for (DLSHRate obRate : getObShipping().getDlshRateList())
      if (!obRate.isSelected())
        OBDal.getInstance().remove(obRate);

    getObShipping().setStatus("COMPLETED");
    OBDal.getInstance().save(getObShipping());

  }

}
