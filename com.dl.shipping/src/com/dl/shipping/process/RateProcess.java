package com.dl.shipping.process;

import java.math.BigDecimal;
import java.util.List;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.currency.Currency;

import com.dl.shipping.api.model.Rate;
import com.dl.shipping.data.DLSHRate;

public class RateProcess extends BaseShipmentProcess {

  @Override
  public void exec() throws Exception {

    for (DLSHRate obRate : getObShipping().getDlshRateList())
      OBDal.getInstance().remove(obRate);

    List<Rate> rates = getCarrierClient().getRates(getShipment());
    for (Rate rate : rates) {
      DLSHRate obRate = getNewOBInstance(DLSHRate.class);
      obRate.setDlshShipping(getObShipping());
      obRate.setServiceCode(rate.getCode());
      obRate.setDuration(rate.getDays());
      obRate.setName(rate.getName());
      obRate.setDescription(rate.getName());
      obRate.setDurationTerms(rate.getDurationTerms());
      obRate.setComments(rate.getMessages());
      obRate.setServiceTerms(rate.getServiceLevelTerms());
      obRate.setAdditionalData(rate.getMetadata().toString());
      obRate.setTotalAmount(new BigDecimal(rate.getTotalAmount()));
      obRate.setTransportAmount(new BigDecimal(rate.getTransportAmount()));
      obRate.setTAXAmount(new BigDecimal(rate.getTaxAmount()));
      obRate.setInsuranceAmount(new BigDecimal(0));
      obRate.setExtrasAmount(new BigDecimal(rate.getExtrasAmount()));
      obRate.setCurrency(findOBObject(Currency.class, Currency.PROPERTY_ISOCODE,
          rate.getTotalAmountCurrency().toString()));
      obRate.setSelected(false);
      OBDal.getInstance().save(obRate);
    }

  }

}
