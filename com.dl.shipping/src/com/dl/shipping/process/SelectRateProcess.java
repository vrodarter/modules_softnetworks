package com.dl.shipping.process;

import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;

import com.dl.shipping.data.DLSHRate;
import com.dl.shipping.data.DLSHShipping;

public class SelectRateProcess extends BaseProcess {

  private static final String RATE_ID_PARAM = "dlsh_rate_id";

  private DLSHRate rate;

  @Override
  public void run() throws Exception {
    for (String p : getBundle().getParams().keySet())
      if (p.toLowerCase().equals(RATE_ID_PARAM))
        rate = OBDal.getInstance().get(DLSHRate.class, getBundle().getParams().get(p));

    if (!getObShipping().getStatus().equals("NEW"))
      throw new Exception("This shipping is not new!");

    for (DLSHRate r : rate.getDlshShipping().getDlshRateList())
      if (!r.getId().equals(rate.getId())) {
        r.setSelected(false);
        OBDal.getInstance().save(r);
      }

    rate.setSelected(true);
    OBDal.getInstance().save(rate);

  }

  @Override
  public Organization getOBOrganization() {
    return getRate().getDlshShipping().getOrganization();
  }

  @Override
  public Client getOBClient() {
    return getRate().getDlshShipping().getClient();
  }

  public DLSHShipping getObShipping() {
    return getRate().getDlshShipping();
  }

  public DLSHRate getRate() {
    return rate;
  }

}
