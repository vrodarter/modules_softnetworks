package com.dl.shipping.process;

public class VoidProcess extends BaseShipmentProcess {

  @Override
  public void exec() throws Exception {
    voidShipment();
  }

}
