package com.dl.shipping.process.ob;

import java.util.Date;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.packing.PackingBox;

import com.dl.shipping.data.DLSHCarrierConfig;
import com.dl.shipping.data.DLSHShipping;
import com.dl.shipping.process.BaseProcess;

public class CreatePackingShipmentProcess extends BaseProcess {

  private static final String PACKING_ID_PARAM = "obwpack_packingh_id";

  private Packing obPacking;

  @Override
  public void run() throws Exception {
    for (String p : getBundle().getParams().keySet())
      if (p.toLowerCase().equals(PACKING_ID_PARAM))
        obPacking = OBDal.getInstance().get(Packing.class, getBundle().getParams().get(p));

    for (PackingBox box : obPacking.getOBWPACKBoxList()) {
      DLSHShipping shipping = getNewOBInstance(DLSHShipping.class);
      shipping.setObwpackBox(box);
      shipping.setStatus("NEW");
      shipping.setCurrency(getOBOrganization().getCurrency());
      shipping.setShipmentDate(new Date());

      OBCriteria<DLSHCarrierConfig> carrierCriteria = OBDal.getInstance()
          .createCriteria(DLSHCarrierConfig.class);
      carrierCriteria.add(Restrictions.eq(DLSHCarrierConfig.PROPERTY_CLIENT, getOBClient()));

      for (DLSHCarrierConfig carrier : carrierCriteria.list())
        if (carrier.isDefault())
          shipping.setDlshCarrierConfig(carrier);

      if (shipping.getDlshCarrierConfig() == null)
        throw new Exception("No default carrier config found!");

      if (obPacking.getBusinessPartner() != null) {
        shipping.setCompany(obPacking.getBusinessPartner().getName());
        if (obPacking.getPartnerAddress() != null) {
          shipping.setPhoneNumber(obPacking.getPartnerAddress().getPhone());
          shipping.setFAXNumber(obPacking.getPartnerAddress().getFax());
          try {
            String[] name = obPacking.getPartnerAddress().getName().split(" ");
            shipping.setFirstName(name[0]);
            String last = "";
            for (int x = 1; x < name.length; x++)
              last += (name[x] + " ");
            shipping.setLastName(last.trim());
          } catch (Exception e) {
            shipping.setFirstName(obPacking.getPartnerAddress().getName());
            shipping.setLastName("");
          }
          if (obPacking.getPartnerAddress().getLocationAddress() != null) {
            shipping.setAddressLine1(
                obPacking.getPartnerAddress().getLocationAddress().getAddressLine1());
            shipping.setAddressLine2(
                obPacking.getPartnerAddress().getLocationAddress().getAddressLine2());
            shipping.setCity(obPacking.getPartnerAddress().getLocationAddress().getCityName());
            shipping
                .setPostalCode(obPacking.getPartnerAddress().getLocationAddress().getPostalCode());
            shipping
                .setRegionName(obPacking.getPartnerAddress().getLocationAddress().getRegionName());

            if (obPacking.getPartnerAddress().getLocationAddress().getCity() != null)
              shipping
                  .setCity(obPacking.getPartnerAddress().getLocationAddress().getCity().getName());
            if (obPacking.getPartnerAddress().getLocationAddress().getRegion() != null) {
              shipping.setRegionCode(
                  obPacking.getPartnerAddress().getLocationAddress().getRegion().getName());
              shipping.setRegionName(
                  obPacking.getPartnerAddress().getLocationAddress().getRegion().getDescription());
            }
            if (obPacking.getPartnerAddress().getLocationAddress().getCountry() != null) {
              shipping.setCountryCode(obPacking.getPartnerAddress().getLocationAddress()
                  .getCountry().getISOCountryCode());
              shipping.setCountryName(
                  obPacking.getPartnerAddress().getLocationAddress().getCountry().getName());
            }

          }
        }
      }
      OBDal.getInstance().save(shipping);
    }

  }

  @Override
  public Organization getOBOrganization() {
    return obPacking.getOrganization();
  }

  @Override
  public Client getOBClient() {
    return OBContext.getOBContext().getCurrentClient();
  }

  public Packing getObPacking() {
    return obPacking;
  }

}
