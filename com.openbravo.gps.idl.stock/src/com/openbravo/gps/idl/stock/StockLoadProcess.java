package com.openbravo.gps.idl.stock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.idl.proc.Parameter;
import org.openbravo.idl.proc.Value;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.geography.Location;
import org.openbravo.model.common.geography.Region;
import org.openbravo.model.common.plm.Attribute;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.model.materialmgmt.transaction.InventoryCount;
import org.openbravo.model.materialmgmt.transaction.InventoryCountLine;
import org.openbravo.module.idljava.proc.IdlServiceJava;
import org.openbravo.scheduling.ProcessBundle;

import com.openbravo.gps.manageattributes.AttributeUtils;

public class StockLoadProcess extends IdlServiceJava {

  private List<InventoryCount> inventoryProcessList = new ArrayList<InventoryCount>();
  private List<InventoryCount> inventoryList = new ArrayList<InventoryCount>();
  private HashMap<String, ArrayList<InventoryCountLine>> inventoryLinesMap = new HashMap<String, ArrayList<InventoryCountLine>>();
  private List<String> lineTypeValues = new ArrayList<String>();
  private String fileLineType = null;
  private String[] messageParams = null;

  // Values that define the Attribute Value field format
  private final String GROUPSTART_MARKER = "\\[";
  private final String GROUPEND_MARKER = "\\]";
  private final String ATTVALUE_SEPARATOR = "|";
  private final String REGEX = "(" + GROUPSTART_MARKER + "\\w+\\" + ATTVALUE_SEPARATOR + "\\w+"
      + GROUPEND_MARKER + ")+?";

  @Override
  protected String getEntityName() {
    return "Warehouse, Stock and Physical Inventories";
  }

  @Override
  public Parameter[] getParameters() {
    this.inventoryList.clear();
    this.inventoryLinesMap.clear();
    this.inventoryProcessList.clear();
    this.lineTypeValues.clear();
    this.fileLineType = null;

    return new Parameter[] { new Parameter("Organization", Parameter.STRING),
        new Parameter("TransactionalOrg", Parameter.STRING),
        new Parameter("Name", Parameter.STRING), new Parameter("Description", Parameter.STRING),
        new Parameter("MovementDate", Parameter.STRING),
        new Parameter("Warehouse", Parameter.STRING),
        new Parameter("WarehouseName", Parameter.STRING),
        new Parameter("WarehouseDescription", Parameter.STRING),
        new Parameter("Warehouse1stLine", Parameter.STRING),
        new Parameter("Warehouse2ndLine", Parameter.STRING),
        new Parameter("WarehousePostalCode", Parameter.STRING),
        new Parameter("WarehouseCity", Parameter.STRING),
        new Parameter("WarehouseRegion", Parameter.STRING),
        new Parameter("WarehouseCountry", Parameter.STRING),
        new Parameter("WarehouseBinSeparator", Parameter.STRING),
        new Parameter("Bin", Parameter.STRING), new Parameter("BinIsDefault", Parameter.STRING),
        new Parameter("BinPriority", Parameter.STRING), new Parameter("BinRowX", Parameter.STRING),
        new Parameter("BinStackY", Parameter.STRING), new Parameter("BinLevelZ", Parameter.STRING),
        new Parameter("Product", Parameter.STRING),
        new Parameter("AttributeValue", Parameter.STRING), new Parameter("Lot", Parameter.STRING),
        new Parameter("SerialNumber", Parameter.STRING),
        new Parameter("ExpirationDate", Parameter.STRING),
        new Parameter("Quantity", Parameter.STRING), new Parameter("Cost", Parameter.STRING),
        new Parameter("LineType", Parameter.STRING), new Parameter("Post", Parameter.STRING) };
  }

  @Override
  protected synchronized Object[] validateProcess(org.openbravo.idl.proc.Validator validator,
      String... values) throws Exception {

    // ,Values,0,Organization
    values[0] = validator.checkNotNull(validator.checkString(values[0], 40, "Organization"),
        "Organization");
    values[0] = validator.checkOrganization(values[0]);

    // ,Values,1,TransactionalOrg
    values[1] = validator.checkNotNull(validator.checkString(values[1], 40, "TransactionalOrg"),
        "TransactionalOrg");
    values[1] = validator.checkTransactionalOrganization(values[1]);

    // ,Values,2,Name
    values[2] = validator.checkNotNull(validator.checkString(values[2], 60, "Name"), "Name");

    // ,Values,3,Description
    values[3] = validator.checkNotNull(validator.checkString(values[3], 255, "Description"),
        "Description");

    // ,Values,4,Movement Date
    if ((values[4] != null) && !values[4].isEmpty()) {
      OBDateUtils.getDate(values[4]);
    }

    // ,Values,5,Warehouse
    values[5] = validator.checkNotNull(validator.checkString(values[5], 40, "Warehouse"),
        "Warehouse");

    // ,Values,6,Warehouse Name
    values[6] = validator.checkNotNull(validator.checkString(values[6], 60, "WarehouseName"),
        "WarehouseName");

    // ,Values,7,Warehouse Description
    values[7] = validator.checkString(values[7], 255, "WarehouseDescription");

    // ,Values,8,Warehouse 1st Line
    validator.checkString(values[8], 60);

    // ,Values,9,Warehouse 2nd Line
    validator.checkString(values[9], 60);

    // ,Values,10,Warehouse Postal Code
    validator.checkString(values[10], 10);

    // ,Values,11,Warehouse City
    validator.checkString(values[11], 60);

    // ,Values,12,Warehouse Region
    validator.checkString(values[12], 60);

    // ,Values,13,Warehouse Country
    values[13] = validator.checkString(values[13], 60, "WarehouseCountry");

    // ,Values,14,Warehouse Bin Separator
    values[14] = validator.checkString(values[14], 1, "WarehouseBinSeparator");

    // ,Values,15,Bin
    values[15] = validator.checkNotNull(validator.checkString(values[15], 40, "Bin"), "Bin");

    // ,Values,16,Bin Is Default
    values[16] = validator.checkNotNull(validator.checkBoolean(values[16], "BinIsDefault"));

    // ,Values,17,Bin Priority
    values[17] = validator.checkNotNull(validator.checkBigDecimal(values[17], "BinPriority"));

    // ,Values,18,Bin Row X
    values[18] = validator.checkString(values[18], 60, "BinRowX");

    // ,Values,19,Bin Stack Y
    values[19] = validator.checkString(values[19], 60, "BinRowY");

    // ,Values,20,Bin Level Z
    values[20] = validator.checkString(values[20], 60, "BinRowZ");

    // ,Values,21,Product
    validator.checkString(values[21], 40);

    // ,Values,22,Attribute Value

    // ,Values,23,Lot
    validator.checkString(values[23], 20);

    // ,Values,24,Serial Number
    validator.checkString(values[24], 20);

    // ,Values,25,Expiration Date
    if ((values[25] != null) && !values[25].isEmpty()) {
      OBDateUtils.getDate(values[25]);
    }

    // ,Values,26,Quantity
    validator.checkBigDecimal(values[26]);

    // ,Values,27,Quantity
    validator.checkBigDecimal(values[27]);

    // ,Values,28,LineType
    values[28] = validator.checkNotNull(validator.checkString(values[28], 1, "LineType"));

    String lineType = values[28];
    // If line type is S or I, product and quantity become mandatory
    if (lineType.equals("S") || lineType.equals("I")) {
      // ,Values,21,Product
      validator.checkNotNull(values[21]);
      // ,Values,26,Quantity
      validator.checkNotNull(values[26]);
    }

    // ,Values,29,Post
    values[29] = validator.checkBoolean(values[29], "Post");

    return values;
  }

  @Override
  protected BaseOBObject internalProcess(Object... values) throws Exception {
    return createEntities((String) values[0], (String) values[1], (String) values[2],
        (String) values[3], (String) values[4], (String) values[5], (String) values[6],
        (String) values[7], (String) values[8], (String) values[9], (String) values[10],
        (String) values[11], (String) values[12], (String) values[13], (String) values[14],
        (String) values[15], (String) values[16], (String) values[17], (String) values[18],
        (String) values[19], (String) values[20], (String) values[21], (String) values[22],
        (String) values[23], (String) values[24], (String) values[25], (String) values[26],
        (String) values[27], (String) values[28], (String) values[29]);
  }

  /**
   * @param Org
   * @param transactionalOrg
   * @param name
   * @param description
   * @param movementdate
   * @param warehouse_searchkey
   * @param warehouse_name
   * @param warehouse_description
   * @param warehouse_1stline
   * @param warehouse_2ndline
   * @param warehouse_postalcode
   * @param warehouse_city
   * @param warehouse_region
   * @param warehouse_country
   * @param warehouse_binseparator
   * @param bin_searchkey
   * @param bin_isdefault
   * @param bin_priority
   * @param bin_rowX
   * @param bin_stackY
   * @param bin_levelZ
   * @param product
   * @param attributevalue
   * @param lot
   * @param serialnumber
   * @param expirationdate
   * @param quantity
   * @param cost
   * @param line_type
   * @param post
   * @return
   * @throws Exception
   *           when there is any error
   */
  public synchronized BaseOBObject createEntities(final String Org, final String transactionalOrg,
      final String name, final String description, final String movementdate,
      final String warehouse_searchkey, final String warehouse_name,
      final String warehouse_description, final String warehouse_1stline,
      final String warehouse_2ndline, final String warehouse_postalcode,
      final String warehouse_city, final String warehouse_region, final String warehouse_country,
      final String warehouse_binseparator, final String bin_searchkey, final String bin_isdefault,
      final String bin_priority, final String bin_rowX, final String bin_stackY,
      final String bin_levelZ, final String product, final String attributevalue, final String lot,
      final String serialnumber, final String expirationdate, final String quantity,
      final String cost, final String line_type, final String post) throws Exception {

    // Supported line types:
    lineTypeValues.add("S"); // Stock
    lineTypeValues.add("W"); // Warehouse
    lineTypeValues.add("I"); // Inventory

    // Check if line type is supported
    if (!lineTypeValues.contains(line_type)) {
      messageParams = new String[] { line_type };
      throw new OBException(OBMessageUtils.getI18NMessage("IDLES_InvalidLineType", messageParams));
    }

    // File can only contain a line type for all its lines
    if (fileLineType == null) {
      fileLineType = line_type;
    } else {
      if (!line_type.equals(fileLineType)) {
        throw new OBException(OBMessageUtils.getI18NMessage("IDLES_NotUniqueLineType",
            messageParams));
      }
    }

    // Check if Attribute Value field has a valid format
    if (attributevalue != null && attributevalue.length() > 0) {
      if (!attributevalue.matches(REGEX)) {
        throw new OBException(OBMessageUtils.getI18NMessage("IDLES_InvalidAttributeValue", null));
      }
    }

    // Search warehouse or create it if it does not exist
    Warehouse warehouse = findDALInstance(false, Warehouse.class, new Value("searchKey",
        warehouse_searchkey));

    // Line type "I" (Inventory) should not create warehouse or bin
    if (warehouse == null && line_type.equals("I")) {
      messageParams = new String[] { warehouse_searchkey };
      throw new OBException(
          OBMessageUtils.getI18NMessage("IDLES_WarehouseNotExists", messageParams));
    }

    if (warehouse == null) {
      warehouse = OBProvider.getInstance().get(Warehouse.class);
      warehouse.setActive(true);
      warehouse.setOrganization(rowOrganization);
      warehouse.setSearchKey(warehouse_searchkey);
      warehouse.setName(warehouse_name);
      warehouse.setDescription(warehouse_description);

      Location location = OBProvider.getInstance().get(Location.class);
      location.setActive(true);
      location.setOrganization(rowOrganization);
      location.setAddressLine1(warehouse_1stline);
      location.setAddressLine2(warehouse_2ndline);
      location.setPostalCode(warehouse_postalcode);
      location.setCityName(warehouse_city);

      // Search Default Country
      Country country = findDALInstance(false, Country.class, new Value("name", warehouse_country));
      location.setCountry(country);
      location.setRegion(findDALInstance(false, Region.class, new Value("country", country),
          new Value("name", warehouse_region)));

      try {
        OBContext.setAdminMode(true);
        OBDal.getInstance().save(location);
        OBDal.getInstance().flush();
        warehouse.setLocationAddress(location);
        warehouse.setStorageBinSeparator(warehouse_binseparator);
        OBDal.getInstance().save(warehouse);
        OBDal.getInstance().flush();
      } catch (Exception e) {
        throw new OBException(e);
      } finally {
        OBContext.restorePreviousMode();
      }
    }

    // Search bin or create it if it does not exist
    Locator locator = findDALInstance(false, Locator.class, new Value(Locator.PROPERTY_WAREHOUSE,
        warehouse), new Value(Locator.PROPERTY_SEARCHKEY, bin_searchkey));

    // Line type "I" (Inventory) should not create warehouse or bin
    if (locator == null && line_type.equals("I")) {
      messageParams = new String[] { bin_searchkey };
      throw new OBException(OBMessageUtils.getI18NMessage("IDLES_LocatorNotExists", messageParams));
    }

    if (locator == null) {
      locator = OBProvider.getInstance().get(Locator.class);
      locator.setActive(true);
      locator.setOrganization(rowOrganization);
      locator.setWarehouse(warehouse);
      locator.setSearchKey(bin_searchkey);
      locator.setDefault(Parameter.BOOLEAN.parse(bin_isdefault));
      locator.setRelativePriority(Parameter.LONG.parse(bin_priority));
      locator.setRowX(bin_rowX);
      locator.setStackY(bin_stackY);
      locator.setLevelZ(bin_levelZ);

      try {
        OBContext.setAdminMode(true);
        OBDal.getInstance().save(locator);
        OBDal.getInstance().flush();
      } finally {
        OBContext.restorePreviousMode();
      }
    }

    // Stock and inventory line types imply creating a physical inventory
    InventoryCount inventory = null;
    if (line_type.equals("S") || line_type.equals("I")) {
      // Product and quantity are compulsory then
      if (product == null || quantity == null) {
        messageParams = new String[] { line_type };
        throw new OBException(
            OBMessageUtils.getI18NMessage("IDLES_ProdAndQtyNeeded", messageParams));
      }

      for (InventoryCount inv : this.inventoryList) {
        if (inv.getName().equals(name)) {
          inventory = inv;
        }
      }

      if (inventory == null) {
        inventory = OBProvider.getInstance().get(InventoryCount.class);
        inventory.setActive(true);
        inventory.setOrganization(rowTransactionalOrg);
        inventory.setName(name);
        inventory.setDescription(description);
        inventory.setMovementDate(OBDateUtils.getDate(movementdate));
        inventory.setWarehouse(warehouse);
        inventory.setPosted("N");
        inventoryList.add(inventory);
      }

      // If an line is marked as has to be posted, post the inventory
      // posted
      if (!this.inventoryProcessList.contains(inventory) && (Parameter.BOOLEAN.parse(post))) {
        this.inventoryProcessList.add(inventory);
      }

      // Physical inventory count line
      InventoryCountLine line = OBProvider.getInstance().get(InventoryCountLine.class);
      line.setActive(true);
      line.setOrganization(rowTransactionalOrg);
      line.setPhysInventory(inventory);
      line.setLineNo(10L);

      Product prodEntity = findDALInstance(true, Product.class, new Value("searchKey", product));
      if (prodEntity == null) {
        messageParams = new String[] { product };
        throw new OBException(OBMessageUtils.getI18NMessage("IDLES_ProdNotExists", messageParams));
      }
      line.setProduct(prodEntity);

      AttributeSetInstance attsetinst = null;
      if (prodEntity.getAttributeSet() != null
          && ((attributevalue != null) || (lot != null) || (serialnumber != null) || (expirationdate != null))) {
        Date expDate = OBDateUtils.getDate((expirationdate));
        attsetinst = AttributeUtils.createAttributeSetInstance(prodEntity, lot, serialnumber,
            expDate, this.parseAttributeValue(attributevalue), rowTransactionalOrg);
        line.setAttributeSetValue(attsetinst);
      }

      line.setQuantityCount(Parameter.BIGDECIMAL.parse(quantity)); // The Quantity Count indicates
                                                                   // the actual inventory count
                                                                   // taken for a product in
                                                                   // inventory
      line.setCost(Parameter.BIGDECIMAL.parse(cost));

      line.setQuantityOrderBook(BigDecimal.ZERO);

      StorageDetail stDetail = this.findStorageDetail(prodEntity, locator, attsetinst);
      if (stDetail != null) {
        line.setBookQuantity(stDetail.getQuantityOnHand()); // The Quantity Book indicates the line
                                                            // count stored in the
                                                            // system for a product in inventory
        line.setOrderQuantity(stDetail.getOnHandOrderQuanity());
        line.setOrderUOM(stDetail.getOrderUOM());
      } else {
        line.setBookQuantity(BigDecimal.ZERO);
      }

      line.setUOM(line.getProduct().getUOM());
      line.setStorageBin(locator);

      ArrayList<InventoryCountLine> invLineList = inventoryLinesMap.get(name);
      if (invLineList == null) {
        invLineList = new ArrayList<InventoryCountLine>();
      }
      invLineList.add(line);
      inventoryLinesMap.put(name, invLineList);
    }
    return inventory;
  }

  /**
   * Parses the Attribute Value field from the CSV and returns a HashMap with the parsed values.
   * 
   * @param attributeValue
   *          Attribute Value field from the CSV file
   * @return Map<Attribute, String> with the parsed values
   */
  private Map<Attribute, String> parseAttributeValue(String attributeValue) {
    Map<Attribute, String> attributeMap = new HashMap<Attribute, String>();
    Pattern pattern = Pattern.compile(REGEX);
    Matcher matcher = pattern.matcher(attributeValue);

    while (matcher.find()) {
      String group = matcher.group().replaceAll(GROUPEND_MARKER, "")
          .replaceAll(GROUPSTART_MARKER, "");
      String name = group.substring(0, group.indexOf(ATTVALUE_SEPARATOR));
      String value = group.substring(group.indexOf(ATTVALUE_SEPARATOR) + 1, group.length());

      Attribute attribute = findDALInstance(false, Attribute.class, new Value(
          Attribute.PROPERTY_NAME, name));
      if (attribute == null) {
        messageParams = new String[] { name };
        throw new OBException(OBMessageUtils.getI18NMessage("IDLES_AttributeNotExists",
            messageParams));
      }
      attributeMap.put(attribute, value);
    }
    return attributeMap;
  }

  /**
   * Finds the Storage Detail for a product, locator and attribute set instance.
   * 
   * @param product
   * @param locator
   * @param attsetinst
   * @return StorageDetail for the Product, Locator and AttributeSetInstance
   */
  private StorageDetail findStorageDetail(Product product, Locator locator,
      AttributeSetInstance attsetinst) {
    AttributeSetInstance auxAttsetinst = attsetinst;
    StorageDetail stDetail = null;

    // Products without attribute set instance are stored with attribute set instance 0
    if (auxAttsetinst == null) {
      auxAttsetinst = OBDal.getInstance().get(AttributeSetInstance.class, "0");
    }

    OBCriteria<StorageDetail> criteriaStDetail = OBDal.getInstance().createCriteria(
        StorageDetail.class);
    criteriaStDetail.add(Restrictions.eq(StorageDetail.PROPERTY_PRODUCT, product));
    criteriaStDetail.add(Restrictions.eq(StorageDetail.PROPERTY_STORAGEBIN, locator));
    criteriaStDetail.add(Restrictions.eq(StorageDetail.PROPERTY_ATTRIBUTESETVALUE, auxAttsetinst));

    List<StorageDetail> list = criteriaStDetail.list();
    if (list.size() == 1) {
      stDetail = list.get(0);
    } else if (list.size() > 1) {
      throw new OBException(OBMessageUtils.getI18NMessage(
          "IDLES_MoreThanOneRecordInStDet",
          new String[] { product.getSearchKey(), locator.getSearchKey(),
              auxAttsetinst.getIdentifier() }));
    }
    return stDetail;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.openbravo.idl.proc.IdlService#postProcess()
   */
  @Override
  protected synchronized void postProcess() throws Exception {
    StringBuilder message = new StringBuilder();
    boolean errorOccurred = false;

    // The file was processed properly, so we can save every inventory and its lines
    try {
      OBContext.setAdminMode(true);
      for (InventoryCount inventory : this.inventoryList) {
        OBDal.getInstance().save(inventory);
        List<InventoryCountLine> invLineList = this.inventoryLinesMap.get(inventory.getName());
        for (InventoryCountLine invLine : invLineList) {
          invLine.setPhysInventory(inventory);
          OBDal.getInstance().save(invLine);
        }
      }
      OBDal.getInstance().flush();
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }

    // Process inventories with field Post = True in the CSV file
    try {
      if (SessionHandler.isSessionHandlerPresent()) {
        SessionHandler.getInstance().commitAndStart();
      }
      for (InventoryCount inventory : this.inventoryProcessList) {
        ProcessBundle pb = new ProcessBundle("107", vars);
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("M_Inventory_ID", inventory.getId());
        pb.setParams(params);
        OBError myMessage = null;
        new org.openbravo.materialmgmt.InventoryCountProcess().execute(pb);
        myMessage = (OBError) pb.getResult();
        if (myMessage.getType().equalsIgnoreCase("Error")) {
          errorOccurred = true;
          messageParams = new String[] { inventory.getIdentifier(), myMessage.getMessage() };
          message.append(
              OBMessageUtils.getI18NMessage("IDLES_InventoryCouldNotBePosted", messageParams))
              .append(System.getProperty("line.separator"));
        }
      }
      if (errorOccurred) {
        throw new OBException(message.toString());
      }
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      throw new OBException(e);
    } finally {
      this.inventoryList.clear();
      this.inventoryLinesMap.clear();
      this.inventoryProcessList.clear();
      this.lineTypeValues.clear();
      this.fileLineType = null;
    }
  }
}
