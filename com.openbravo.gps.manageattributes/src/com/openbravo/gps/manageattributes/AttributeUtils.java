/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2013 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.openbravo.gps.manageattributes;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_actionButton.CreateStandards;
import org.openbravo.erpCommon.utility.AttributeSetInstanceValue;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Attribute;
import org.openbravo.model.common.plm.AttributeInstance;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.AttributeUse;
import org.openbravo.model.common.plm.AttributeValue;
import org.openbravo.model.common.plm.Product;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;
import org.openbravo.utils.Replace;

/**
 * Utilities to manage attributes.
 */
public class AttributeUtils {

  private static final Logger log4j = Logger.getLogger(CreateStandards.class);
  private static String[] messageParams = null;

  /**
   * Returns an AttributeSetInstance well formed
   * 
   * @param product
   * 
   * @param lot
   * 
   * @param serialNumber
   * 
   * @param expDate
   * 
   * @param attributeValues
   * 
   * @param organization
   * 
   * @return AttributeSetInstance
   */
  public static AttributeSetInstance createAttributeSetInstance(Product product, String lot,
      String serialNumber, Date expDate, Map<Attribute, String> attributeValues, Organization org)
      throws ServletException {

    // Product needs have an attribute set
    if (product.getAttributeSet() == null) {
      messageParams = new String[] { product.getIdentifier() };
      throw new OBException(OBMessageUtils.getI18NMessage("OBMATT_ProdDoesNotHaveAttSet",
          messageParams));
    }

    final VariablesSecureApp vars = new VariablesSecureApp(OBContext.getOBContext().getUser()
        .getId(), OBContext.getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
        .getCurrentOrganization().getId());

    final ConnectionProvider conn = new DalConnectionProvider(true);

    AttributeSetInstanceValue attSetInstanceValue = new AttributeSetInstanceValue(lot,
        serialNumber, (expDate != null ? OBDateUtils.formatDate(expDate) : ""), "", "");

    Map<String, String> attributeValuesStrings = reviewAttributes(attributeValues,
        product.getAttributeSet(), null);

    OBError attCreation = null;

    if (org != null) {
      attCreation = attSetInstanceValue.setAttributeInstance(conn, vars, product.getAttributeSet()
          .getId(), "", "", "N", product.getId(), attributeValuesStrings, org);
    } else {
      attCreation = attSetInstanceValue.setAttributeInstance(conn, vars, product.getAttributeSet()
          .getId(), "", "", "N", product.getId(), attributeValuesStrings);
    }

    if (attCreation.getType().equals("Success")) {
      OBDal.getInstance().flush();
      AttributeSetInstance newAttSetinstance = OBDal.getInstance().get(AttributeSetInstance.class,
          attSetInstanceValue.getAttSetInstanceId());
      return newAttSetinstance;
    } else {
      log4j.error("Error creating attribute set instance: " + attCreation.getMessage(),
          new Exception());
      throw new OBException(attCreation.getMessage());
    }

  }

  /**
   * Returns an AttributeSetInstance well formed
   * 
   * @param product
   * 
   * @param lot
   * 
   * @param serialNumber
   * 
   * @param expDate
   * 
   * @param attributeValues
   * 
   * @return AttributeSetInstance
   */
  public static AttributeSetInstance createAttributeSetInstance(Product product, String lot,
      String serialNumber, Date expDate, Map<Attribute, String> attributeValues)
      throws ServletException {
    return createAttributeSetInstance(product, lot, serialNumber, expDate, attributeValues, null);
  }

  /**
   * Changes an AttributeSetInstance
   * 
   * @param product
   * 
   * @param attributeSetInst
   * 
   * @param lot
   * 
   * @param serialNumber
   * 
   * @param expDate
   * 
   * @param attributeValues
   * 
   * @return AttributeSetInstance
   */
  public static AttributeSetInstance modifyAttributeSetInstance(
      AttributeSetInstance attributeSetInst, String lot, String serialNumber, Date expDate,
      Map<Attribute, String> attributeValues) throws OBException {

    final VariablesSecureApp vars = new VariablesSecureApp(OBContext.getOBContext().getUser()
        .getId(), OBContext.getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
        .getCurrentOrganization().getId());

    final ConnectionProvider conn = new DalConnectionProvider(true);

    AttributeSetInstanceValue attSetInstanceValue = new AttributeSetInstanceValue(
        (lot != null ? lot : attributeSetInst.getLotName()), (serialNumber != null ? serialNumber
            : attributeSetInst.getSerialNo()), (expDate != null ? OBDateUtils.formatDate(expDate)
            : ""), "", "");

    Map<String, String> attributeValuesStrings = reviewAttributes(attributeValues,
        attributeSetInst.getAttributeSet(), attributeSetInst);

    OBError attCreation;
    try {
      attCreation = attSetInstanceValue.setAttributeInstance(conn, vars, attributeSetInst
          .getAttributeSet().getId(), attributeSetInst.getId(), "", "Y", null,
          attributeValuesStrings, attributeSetInst.getOrganization());
    } catch (ServletException e) {
      Throwable obex = DbUtility.getUnderlyingSQLException(e);
      throw new OBException(obex);
    }

    if (attCreation.getType().equals("Success")) {
      OBDal.getInstance().flush();
      AttributeSetInstance newAttSetinstance = OBDal.getInstance().get(AttributeSetInstance.class,
          attSetInstanceValue.getAttSetInstanceId());
      return newAttSetinstance;
    } else {
      log4j.error("Error modifying attribute set instance: " + attCreation.getMessage(),
          new Exception());
      throw new OBException(attCreation.getMessage());
    }

  }

  private static Map<String, String> reviewAttributes(Map<Attribute, String> attributeValues,
      AttributeSet attributeset, AttributeSetInstance attSetInstance) {

    HashMap<String, String> attributeValuesStrings = new HashMap<String, String>();

    for (AttributeUse attUse : attributeset.getAttributeUseList()) {
      Attribute att = attUse.getAttribute();
      if (attributeValues.get(att) != null) {
        // In the List
        String strValue = attributeValues.get(att);
        if (att.isList() && !Utility.isUUIDString(strValue)) {
          strValue = convertToUUID(strValue, att);
          if (strValue == null) {
            messageParams = new String[] { attributeValues.get(att) };
            throw new OBException(OBMessageUtils.getI18NMessage("OBMATT_AttSetValueNotExists",
                messageParams));
          }
        }
        attributeValuesStrings.put(replace(att.getName()), strValue);
      } else if (attSetInstance != null) {
        // Not in the list, if is set maintain it
        for (AttributeInstance attInstance : attSetInstance.getAttributeInstanceList()) {
          if (attInstance.getAttribute() == att) {
            if (att.isList()) {
              attributeValuesStrings.put(replace(att.getName()),
                  (attInstance.getAttributeValue() == null ? null : attInstance.getAttributeValue()
                      .getId()));
            } else {
              attributeValuesStrings.put(replace(att.getName()), attInstance.getSearchKey());
            }
          }
        }
      }

    }

    return attributeValuesStrings;
  }

  private static String convertToUUID(String strValue, Attribute attribute) {
    if (strValue.equals("")) {
      return strValue;
    }
    OBCriteria<AttributeValue> valuesCri = OBDal.getInstance().createCriteria(AttributeValue.class);
    valuesCri.add(Restrictions.eq(AttributeValue.PROPERTY_ATTRIBUTE, attribute));
    valuesCri.add(Restrictions.eq(AttributeValue.PROPERTY_SEARCHKEY, strValue));
    AttributeValue value = (AttributeValue) valuesCri.uniqueResult();
    if (value != null) {
      return value.getId();
    } else {
      return null;
    }
  }

  private static String replace(String strIni) {
    // delete characters: " ","&",","
    return Replace.replace(Replace.replace(Replace.replace(
        Replace.replace(Replace.replace(Replace.replace(strIni, "#", ""), " ", ""), "&", ""), ",",
        ""), "(", ""), ")", "");
  }

}