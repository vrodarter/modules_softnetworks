/*
 ************************************************************************************
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.softnetworks.warehouse.mobile.picking;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import java.util.List;
import java.math.BigDecimal;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.mobile.warehouse.WarehouseJSONProcess;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.warehouse.pickinglist.MovementLineHandler;
import org.openbravo.warehouse.pickinglist.actionhandler.RaiseIncidenceHandler;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBCriteria;
import org.hibernate.criterion.Restrictions;

public class ProcessPicking extends WarehouseJSONProcess {
  private static final Logger log = Logger.getLogger(ProcessPicking.class);

  @Override
  protected String getProperty() {
    return WHPickingConstants.PICKING_PROPERTY;
  }

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    long t0 = System.currentTimeMillis();

     System.out.println("****************************************************");//EZL


    OBContext.setAdminMode(true);

    try {
      final String action = jsonsent.getString("action");
      JSONArray items = jsonsent.getJSONArray("items");
      if ("incidence".equals(action)) {
        raiseIncidence(items);
      } else if ("resetincidence".equals(action)) {
        resetIncidence(items);
      } else if ("process".equals(action)) {
        processItems(items);
      }

      OBDal.getInstance().flush();

      long t1 = System.currentTimeMillis();
      log.debug("time to create the document: " + (t1 - t0));

      JSONObject result = new JSONObject();
      result.put("status", 0);
      result.put("message", OBMessageUtils.messageBD("success"));

      long t2 = System.currentTimeMillis();
      log.debug("time to process: " + (t2 - t1));
      log.debug(" TOTAL time: " + (t2 - t0));

      return result;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void raiseIncidence(JSONArray items) throws JSONException {
    for (int i = 0; i < items.length(); i++) {
      JSONObject item = items.getJSONObject(i);
      InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
          item.getString("id"));
      RaiseIncidenceHandler.raiseIncidence(mvmtLine, "");
      OBDal.getInstance().save(mvmtLine);
    }
  }

  private void resetIncidence(JSONArray items) throws JSONException {
    for (int i = 0; i < items.length(); i++) {
      JSONObject item = items.getJSONObject(i);
      InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
          item.getString("id"));
      RaiseIncidenceHandler.resetIncidence(mvmtLine);
      OBDal.getInstance().save(mvmtLine);
    }
  }

  private void processItems(JSONArray items) throws JSONException {
    for (int i = 0; i < items.length(); i++) {
      JSONObject item = items.getJSONObject(i);

      try{

      String serials = item.optString("serialNumber");
      System.out.println("**************** SNS:"+serials);

      if(serials != null){

        InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
          item.getString("id"));
        String[] serialsArray = serials.split(",");

        int limit = mvmtLine.getMovementQuantity().intValue();

        if(serialsArray!=null && serialsArray.length > 1){
          for(int x = 1;x<serialsArray.length && x<limit;x++){
            InternalMovementLine line=copyLine(mvmtLine,serialsArray[x]);
            OBDal.getInstance().save(line);
            MovementLineHandler.processMovementLine(line.getId(), "CF");
          }
          putSerialNumber(mvmtLine,serialsArray[0]);
        }else{
          putSerialNumber(mvmtLine,serials);
        }
        
        OBDal.getInstance().save(mvmtLine);

      }


      }catch(Exception e){
        e.printStackTrace();
      }

      final String strMvmtLineId = item.getString("id");
      try {
        MovementLineHandler.processMovementLine(strMvmtLineId, "CF");
      } catch (PropertyException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  public InternalMovementLine copyLine(InternalMovementLine src,String serialNumber) throws Exception {

    InternalMovementLine mvmtLine = OBProvider.getInstance().get(InternalMovementLine.class);

    mvmtLine.setClient(src.getClient());
    mvmtLine.setOrganization(src.getOrganization());
    mvmtLine.setActive(src.isActive());
    mvmtLine.setCreationDate(src.getCreationDate());
    mvmtLine.setCreatedBy(src.getCreatedBy());
    mvmtLine.setUpdated(src.getUpdated());
    mvmtLine.setUpdatedBy(src.getUpdatedBy());
    mvmtLine.setMovement(src.getMovement()); 
    mvmtLine.setStorageBin(src.getStorageBin());
    mvmtLine.setNewStorageBin(src.getNewStorageBin());
    mvmtLine.setProduct(src.getProduct());
    mvmtLine.setLineNo(src.getLineNo());
    mvmtLine.setDescription(src.getDescription());
    mvmtLine.setOrderUOM(src.getOrderUOM());
    mvmtLine.setUOM(src.getUOM());
    mvmtLine.setStockReservation(src.getStockReservation());
    mvmtLine.setCustgsOrderline(src.getCustgsOrderline());
    mvmtLine.setOBWPLWarehousePickingList(src.getOBWPLWarehousePickingList());
    mvmtLine.setOBWPLGroupPickinglist(src.getOBWPLGroupPickinglist());
    mvmtLine.setOBWPLComplete(src.isOBWPLComplete()); 
    mvmtLine.setOBWPLItemStatus(src.getOBWPLItemStatus());
    mvmtLine.setOBWPLEditItem(src.isOBWPLEditItem());
    mvmtLine.setOBWPLAllowDelete(src.isOBWPLAllowDelete());
    mvmtLine.setOBWPLRaiseIncidence(src.isOBWPLRaiseIncidence());
    mvmtLine.setOBWPLIncidenceReason(src.getOBWPLIncidenceReason());
    mvmtLine.setOBWPLReject(src.isOBWPLReject());
    mvmtLine.setSvftrafMovementline(src.getSvftrafMovementline());
    mvmtLine.setSvfpProcessAssign(src.isSvfpProcessAssign());
    mvmtLine.setSvfpProcessReassign(src.isSvfpProcessReassign());
    mvmtLine.setSvfpAdUser(src.getSvfpAdUser());
    mvmtLine.setSvfpMovementlinestatus(src.getSvfpMovementlinestatus());
    mvmtLine.setSvfpPackingh(src.getSvfpPackingh());

    mvmtLine.setMovementQuantity(new BigDecimal(1));
    src.setMovementQuantity(src.getMovementQuantity().subtract(new BigDecimal(1)));
    //mvmtLine.setOrderQuantity(new BigDecimal(1));
    //src.setOrderQuantity(src.getOrderQuantity().subtract(new BigDecimal(1)));

    putSerialNumber(mvmtLine,serialNumber);

    return mvmtLine;

  }

  public void putSerialNumber(InternalMovementLine mvmtLine,String serialNumber) throws Exception {
    AttributeSetInstance attributeSetInstance = null;

      OBCriteria<AttributeSetInstance> criteriaAttributeSet = OBDal.getInstance().createCriteria(
          AttributeSetInstance.class);
      criteriaAttributeSet.add(Restrictions.eq(AttributeSetInstance.PROPERTY_ID, serialNumber));
      List<AttributeSetInstance> attributeSetList = criteriaAttributeSet.list();
      
      if (attributeSetList.size() > 0) {
        attributeSetInstance = attributeSetList.get(0);
        mvmtLine.setAttributeSetValue(attributeSetInstance);
      }
  }
}

