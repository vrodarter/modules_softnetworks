/*
 ************************************************************************************
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.softnetworks.warehouse.mobile.picking;

public class WHPickingConstants {
  public static final String COMPONENT_TYPE = "OBMWHP_Main";
  public static final String MODULE_JAVAPACKAGE = "com.softnetworks.warehouse.mobile.picking";
  public static final String MODULE_ID = "F28D10650F19432388A5416CC27B01ED";

  public static final String PICKING_PROPERTY = "OBMWHP_Picking";
}
