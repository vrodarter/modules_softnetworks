package com.softnetworks.warehouse.mobile.scan;

import java.util.Iterator;
import java.math.BigDecimal;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.mobile.warehouse.WarehouseJSONProcess;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.mobile.warehouse.barcode.BarcodeScanner;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.model.materialmgmt.onhandquantity.ProductStockView;


public class ScanHandler extends WarehouseJSONProcess {
  private final static Logger log = Logger.getLogger(ScanHandler.class);

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    OBContext.setAdminMode(true);
    try {
      JSONObject response =null;

      String eventName = null;
      String code = jsonsent.getString("code");
      String productId = jsonsent.optString("productId");

      response = getProductByBarcode(code);

      if(response!=null)
      	return response;
      else if(productId!=null){
      	response = getProductBySerial(code,productId);
      }

      if(response!=null)
      	return response;
      
      response = new JSONObject();
      response.put("status", 1);
      JSONObject jsonError = new JSONObject();
      jsonError.put("message",
          OBMessageUtils.getI18NMessage("OBWH_NoItemWithCode", new String[] { code }));
      response.put("error", jsonError);
      log.warn("not found item for code [" + code + "]");

      return response;
    } finally {
      OBContext.restorePreviousMode();
    }
  }


  public JSONObject getProductByBarcode(String code) throws JSONException {
    
    OBCriteria<Product> qProduct = OBDal.getInstance().createCriteria(Product.class);
    qProduct.add(Restrictions.eq(Product.PROPERTY_UPCEAN, code));
    List<Product> products = qProduct.list();
    if (products.size() == 1) {
      Product product = products.get(0);

      JSONObject response = new JSONObject();
      JSONObject jsonProduct = new JSONObject();
      response.put("status", 0);
        jsonProduct.put("id", product.getId());
        jsonProduct.put("name", product.getIdentifier());
        if(product.isSnmwhpIsSerialized())
        	jsonProduct.put("serialized", "y");
        jsonProduct.put("uom.name", product.getUOM().getIdentifier());
        jsonProduct.put("uom.id", product.getUOM().getId());
        jsonProduct.put("hasAttribute", product.getAttributeSet() != null);

        JSONObject data = new JSONObject();
        data.put("product", jsonProduct);
        response.put("data", data);

      	return response;
    } 
    return null;
  }

  public JSONObject getProductBySerial(String serial,String code) throws JSONException {
     
    OBCriteria<Product> qProduct = OBDal.getInstance().createCriteria(Product.class);

    qProduct.add(Restrictions.eq(Product.PROPERTY_ID, code));
    qProduct.add(Restrictions.eq(Product.PROPERTY_SNMWHPISSERIALIZED, true));

    List<Product> products = qProduct.list();
    if (products.size() == 1) {
      Product product = products.get(0);

      if(serial.equals("0000000000")){

        JSONObject response = new JSONObject();
        JSONObject jsonProduct = new JSONObject();
      response.put("status", 0);
        jsonProduct.put("id", product.getId());
        jsonProduct.put("name", product.getIdentifier());
        if(product.isSnmwhpIsSerialized())
        	jsonProduct.put("serialized", "y");
        jsonProduct.put("serialNo", "0000000000");
        jsonProduct.put("uom.name", product.getUOM().getIdentifier());
        jsonProduct.put("uom.id", product.getUOM().getId());
        jsonProduct.put("hasAttribute", product.getAttributeSet() != null);

        JSONObject data = new JSONObject();
        data.put("product", jsonProduct);
        response.put("data", data);
		return response;

      }

    OBCriteria<ProductStockView> qStock = OBDal.getInstance().createCriteria(ProductStockView.class);
    qStock.add(Restrictions.eq(ProductStockView.PROPERTY_PRODUCT, product));
    qStock.add(Restrictions.gt(ProductStockView.PROPERTY_QUANTITYONHAND, BigDecimal.ZERO));

    if (qStock.count()>0) 
      for(ProductStockView ps:qStock.list())
      	if(ps.getAttributeSetValue()!=null && ps.getAttributeSetValue().getSerialNo()!=null && ps.getAttributeSetValue().getSerialNo().equals(serial)){
      		JSONObject response = new JSONObject();
	        JSONObject jsonProduct = new JSONObject();
      response.put("status", 0);
	        jsonProduct.put("id", product.getId());
	        jsonProduct.put("name", product.getIdentifier());
	        if(product.isSnmwhpIsSerialized())
	        	jsonProduct.put("serialized", "y");
	        jsonProduct.put("serialNo", ps.getAttributeSetValue().getId());
	        jsonProduct.put("uom.name", product.getUOM().getIdentifier());
	        jsonProduct.put("uom.id", product.getUOM().getId());
	        jsonProduct.put("hasAttribute", product.getAttributeSet() != null);

	        JSONObject data = new JSONObject();
	        data.put("product", jsonProduct);
	        response.put("data", data);
			return response;
      	}
      
    } 

    return null;
  }

}
