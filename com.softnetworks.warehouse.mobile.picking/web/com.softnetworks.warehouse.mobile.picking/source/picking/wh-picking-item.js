/*
 ************************************************************************************
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global enyo, OBWH*/

enyo.kind({
  name: 'OBWH.Picking.RightToolbar',
  kind: 'OB.UI.MultiColumn.Toolbar',

  showMenu: false,

  buttons: [{
    kind: 'OBWH.Picking.ToolbarButton',
    name: 'scan',
    span: '6'
  }, {
    kind: 'OBWH.Picking.ToolbarButton',
    name: 'edit',
    span: '6'
  }]
});

enyo.kind({
  name: 'OBWH.Picking.ToolbarButton',
  kind: 'OB.UI.ToolbarButtonTab',
  events: {
    onActivateTab: ''
  },
  tap: function () {
    this.doActivateTab({
      tab: this.name
    });
  },
  setTabActive: function (tab) {
    this.parent.parent.addRemoveClass('active', this.name === tab);
    this.setDisabled(tab === this.name);
  },
  initComponents: function () {
    this.inherited(arguments);
    if (this.name === 'scan') {
      this.setContent(OB.I18N.getLabel('OBWH_BtnScan'));
    } else {
      this.setContent(OB.I18N.getLabel('OBWH_BtnEdit'));
    }
  },
  init: function (model) {
    var me = this;
    this.setTabActive(model.get('activeTab'));
    model.on('change:activeTab', function () {
      me.setTabActive(model.get('activeTab'));
    });
  }
});

enyo.kind({
  name: 'OBWH.Picking.ItemView',
  components: [{
    name: 'base',
    style: 'position: relative; display: block; background-color: #7da7d9; background-size: cover; color: white; height: 200px; margin: 5px; padding: 5px',

    components: [{
      classes: 'span8',
      style: 'position: absolute; top: 20px; left: 15px; font-size: 32px;',
      name: 'product'
    }, {
      classes: 'span8',
      style: 'position: absolute; bottom: 20px; left: 15px; font-size: 32px;',
      name: 'quantity'
    }, {
      style: 'position: absolute; top: 20px; right: 15px; font-size: 42px; line-height: 42px; color: black;',
      name: 'pickedQty'
    }, {
      style: 'position: absolute; bottom: 0; right: 0; margin: 5px; padding: 5px;',
      kind: 'OB.UI.Clock'
    }]
  }, {
    kind: 'OBWH.Picking.KeyboardScan',
    name: 'keyboardscan'
  }, {
    kind: 'OBWH.Picking.KeyboardEdit',
    name: 'keyboardedit'
  }],

  setTabActive: function (scanMode) {
    if (scanMode) {
      this.$.keyboardscan.show();
      this.$.keyboardscan.isEnabled = true;
      this.$.keyboardscan.defaultcommand = 'code';
      this.$.keyboardedit.hide();
      this.$.keyboardedit.isEnabled = false;
      this.$.product.hide();
      this.$.quantity.hide();
      this.$.pickedQty.hide();
    } else {
      this.$.keyboardscan.hide();
      this.$.keyboardscan.isEnabled = false;
      this.$.keyboardedit.show();
      this.$.keyboardedit.isEnabled = true;
      this.$.keyboardedit.defaultcommand = 'add';
      this.$.product.show();
      this.$.quantity.show();
      this.$.pickedQty.show();
    }
  },

  init: function (model) {
    var currentItem = model.get('currentItem'),
        me = this;
    model.on('change:currentItem', function (model) {
      var currentItem = model.get('currentItem');
      me.$.product.setContent(currentItem.get('productName'));
      me.$.quantity.setContent(currentItem.get('neededQty') + ' ' + currentItem.get('uom'));
      me.$.pickedQty.setContent(currentItem.get('pickedQty'));
      currentItem.on('pickedQtyChanged', function (currentItem) {
        me.$.pickedQty.setContent(currentItem.get('pickedQty'));
      });
      if (currentItem.get('status') === 'IN') {
        //me.$.base.addStyles('background-color: #c7c700;');
        me.$.quantity.setContent(OB.I18N.getLabel('OBMWHP_Incidence'));
      } else {
        //me.$.base.addStyles('background-color: #7da7d9;');
        me.$.quantity.setContent(currentItem.get('neededQty') + ' ' + currentItem.get('uom'));
      }
      currentItem.on('change:status', function () {
        if (currentItem.get('status') === 'IN') {
          //me.$.base.addStyles('background-color: #c7c700;');
          me.$.quantity.setContent(OB.I18N.getLabel('OBMWHP_Incidence'));
        } else {
          //me.$.base.addStyles('background-color: #7da7d9;');
          me.$.quantity.setContent(currentItem.get('neededQty') + ' ' + currentItem.get('uom'));
        }
      });
    });
    me.setTabActive(model.get('activeTab') === 'scan');
    model.on('change:activeTab', function () {
      me.setTabActive(model.get('activeTab') === 'scan');
    });
    if (!currentItem) {
      return;
    }

    this.$.product.setContent(currentItem.get('productName'));
    this.$.quantity.setContent(currentItem.get('neededQuantity'));
  }
});

enyo.kind({
  name: 'OBWH.Picking.KeyboardScan',
  kind: 'OB.UI.Keyboard',
  sideBarEnabled: false,
  events: {
    onScan: ''
  },
  keyMatcher: /^([0-9]|\.|,| |%|[a-z]|[A-Z])$/,

  initComponents: function () {
    var toolbar, me = this;

    this.addCommand('code', new OBWH.Picking.BarcodeHandler({
      keyboard: this
    }));

    // calling super after setting keyboard properties
    this.inherited(arguments);

    this.addKeypad('OBWH.Picking.MyKeypad');

    toolbar = {
      name: 'toolbarscan',
      buttons: [{
        command: 'code',
        label: OB.I18N.getLabel('OBMOBC_KbCode'),
        classButtonActive: 'btnactive-blue'
      }],
      shown: function () {
        var keyboard = this.owner.owner;
        keyboard.showKeypad('obmwhp-keypad');
        keyboard.defaultcommand = 'code';
      }
    };

    this.addToolbar(toolbar);
    this.showKeypad('obmwhp-keypad');
    this.showToolbar('toolbarscan');

    this.defaultcommand = 'code';
  },
  globalKeypressHandler: function (inSender, inEvent) {
    if (!this.isEnabled) {
      return;
    }
    this.inherited(arguments);
  }
});

enyo.kind({
  name: 'OBWH.Picking.BarcodeHandler',
  kind: 'OB.UI.AbstractBarcodeActionHandler',
  events: {
    onScan: ''
  },
  findProductByBarcode: function (code, callback, keyboard) {
    keyboard.doScan({
      code: code
    });
  }
});

enyo.kind({
  name: 'OBWH.Picking.MyKeypad',
  kind: 'OB.UI.KeypadBasic',
  padName: 'obmwhp-keypad',
  toolbarButtons: [{
    label: ' ',
    command: ''
  }, {
    label: ' ',
    command: ''
  }, {
    label: ' ',
    command: 'Add:'
  }]
});

enyo.kind({
  name: 'OBWH.Picking.KeyboardEdit',
  kind: 'OB.UI.Keyboard',
  sideBarEnabled: true,
  events: {
    onSetQuantity: '',
    onRaiseIncidence: '',
    onResetIncidence: ''
  },
  keyMatcher: /^([0-9]|\.|,)$/,

  buttonsDef: {
    sideBar: {
      qtyI18nLbl: '',
      priceI18nLbl: '',
      discountI18nLbl: ''
    }
  },

  initComponents: function () {
    var toolbar, me = this;

    this.addCommand('add', {
      action: function (keyboard, qty) {
        if (me.showing) {
          var value = OB.I18N.parseNumber(qty);
          if (value || value === 0) {
            me.doSetQuantity({
              quantity: value,
              incremental: false
            });
          }
        }
      }
    });

    this.addCommand('-', {
      stateless: true,
      action: function (keyboard, qty) {
        //remove
        if (me.showing) {
          var value = OB.I18N.parseNumber(qty);
          if (!value) {
            value = Number(1);
          }
          value = value * -1;
          me.doSetQuantity({
            quantity: value,
            incremental: true
          });
        }
      }
    });

    this.addCommand('+', {
      stateless: true,
      action: function (keyboard, qty) {
        //remove
        if (me.showing) {
          var value = OB.I18N.parseNumber(qty);
          if (!value) {
            value = Number(1);
          }
          me.doSetQuantity({
            quantity: value,
            incremental: true
          });
        }
      }
    });

    this.addCommand('incidence', {
      stateless: true,
      action: function (keyboard, qty) {
        me.doRaiseIncidence();
      }
    });

    this.addCommand('undoincidence', {
      stateless: true,
      action: function (keyboard, qty) {
        me.doResetIncidence();
      }
    });

    // calling super after setting keyboard properties
    this.inherited(arguments);

    this.addKeypad('OBWH.Picking.MyKeypadBasic');
    toolbar = {
      name: 'toolbaredit',
      buttons: [{
        command: 'incidence',
        label: OB.I18N.getLabel('OBMWHP_Incidence'),
        classButtonActive: 'btnactive-blue'
      }, {
        command: 'undoincidence',
        label: OB.I18N.getLabel('OBMWHP_UndoIncidence'),
        classButtonActive: 'btnactive-blue'
      }],
      shown: function () {
        var keyboard = this.owner.owner;
        keyboard.showSidepad('sideenabled');
        keyboard.showKeypad('obmwhp-keypadbasic');
        keyboard.defaultcommand = 'add';
      }
    };
    this.addToolbar(toolbar);
    this.showKeypad('obmwhp-keypadbasic');
    this.showToolbar('toolbaredit');

    this.defaultcommand = 'add';
  }

});

enyo.kind({
  name: 'OBWH.Picking.MyKeypadBasic',
  kind: 'OB.UI.KeypadBasic',
  padName: 'obmwhp-keypadbasic',
  toolbarButtons: [{
    label: ' ',
    command: ''
  }, {
    label: ' ',
    command: ''
  }, {
    label: ' ',
    command: ''
  }],
  initComponents: function () {
    this.inherited(arguments);
    this.$.toolbarBtn1.disableButton(this.$.toolbarBtn1, {
      disabled: true
    });
  }
});


