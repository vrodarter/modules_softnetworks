/*
 ************************************************************************************
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global OBWH, enyo, Backbone, _*/

OBWH.Picking = OBWH.Picking || {};

OBWH.Picking.Model = OB.Model.WindowModel.extend({
  models: [],

  processPicking: function () {
    var proc = new OB.DS.Process(OBWH.Picking.Model.processPickingClass),
        me = this,
        readyItems = this.get('picking').get('itemsReady'),
        itemsToProcess = new Backbone.Collection();

    readyItems.forEach(function (item) {
      item.get('dalItems').forEach(function (dalItem) {
        itemsToProcess.add(dalItem);
      });
    });

    proc.exec({
      action: 'process',
      items: JSON.parse(JSON.stringify(itemsToProcess.toJSON()))
    }, function (response, message) {
      if (response && response.exception) {
        OB.UTIL.showError(response.exception.message);
      } else {
        me.get('picking').reset();
        OB.UTIL.showSuccess('success');
      }
      OB.UTIL.showLoading(false);
    }, function () {
      me.get('picking').reset();
      OB.UTIL.showLoading(false);
    });
  },

  raiseIncidence: function () {
    var proc = new OB.DS.Process(OBWH.Picking.Model.processPickingClass),
        me = this,
        currentItem = this.get('currentItem'),
        items = currentItem.get('dalItems');

    proc.exec({
      action: 'incidence',
      items: JSON.parse(JSON.stringify(items.toJSON()))
    }, function (response, message) {
      if (response && response.exception) {
        OB.UTIL.showError(response.exception.message);
      } else {
        currentItem.set('status', 'IN');
        OB.UTIL.showSuccess('success');
      }
      OB.UTIL.showLoading(false);
    }, function () {
      me.get('picking').reset();
      OB.UTIL.showLoading(false);
    });
  },

  resetIncidence: function () {
    var proc = new OB.DS.Process(OBWH.Picking.Model.processPickingClass),
        me = this,
        currentItem = this.get('currentItem'),
        items = currentItem.get('dalItems');

    proc.exec({
      action: 'resetincidence',
      items: JSON.parse(JSON.stringify(items.toJSON()))
    }, function (response, message) {
      if (response && response.exception) {
        OB.UTIL.showError(response.exception.message);
      } else {
        currentItem.set('status', 'PE');
        OB.UTIL.showSuccess('success');
      }
      OB.UTIL.showLoading(false);
    }, function () {
      me.get('picking').reset();
      OB.UTIL.showLoading(false);
    });
  },

  scan: function (code) {
    var me = this,
        picking = this.get('picking'),
        currentItem;


    var proc = new OB.DS.Process('com.softnetworks.warehouse.mobile.scan.ScanHandler');
    proc.exec({
      code: code,
      productId:this.currentProductId
    }, enyo.bind(this, function (response, message) {
      var items;
      if (response.exception) {
        OB.UTIL.showError(response.exception.message);
        return;
      }

      if (response.product) {
        items = picking.get('items');
        currentItem = _.find(items.models, function (item) {
          return item.get('product') === response.product.id && item.get('status') === 'PE';
        });
        if (currentItem) {

          this.currentProductId = response.product.id;
          me.set('currentItem', currentItem);

          if(response.product.serialized == 'y'){
            if(response.product.serialNo){
              currentItem.addSerial(response.product.serialNo);
            }
          }else
            currentItem.setPickedQty(Number(1), true);

        }

        OB.UTIL.showSuccess(OB.I18N.getLabel('OBWH_SacannedProduct', [response.product.name]));
      }
    }), function () {
      window.console.error('error');
    });
  },

  init: function () {
    var pickStatusCrit, picking, items, itemsReady, itemsDone, locators;
    OB.Data.Registry.registerModel('OBWPL_pickinglist');
    OB.Data.Registry.registerModel('MaterialMgmtInternalMovementLine');
    OB.Data.Registry.registerModel('DocumentType');

    items = new OBWH.Picking.Model.Items();
    itemsReady = new OBWH.Picking.Model.Items();
    itemsDone = new OBWH.Picking.Model.Items();
    locators = new OBWH.Picking.Model.Locators();
    picking = new OBWH.Picking.Model.Picking({
      windowModel: this,
      items: items,
      itemsReady: itemsReady,
      itemsDone: itemsDone,
      locators: locators
    });
    this.set('picking', picking);
    this.set('activeTab', 'scan');

    items.on('add remove', function () {
      picking.set('totalItems', items.length);
    });
    itemsReady.on('add remove', function () {
      picking.set('ready', (itemsReady.length !== 0));
    });

    items.on('selected', function (item) {
      this.set('currentItem', item);
      this.set('activeTab', 'edit');
    }, this);

    // Load picking status names
    pickStatusCrit = {
      _where: 'e.reference.id = \'3F698D2435774CFAB5B850C7686E47F4\''
    };
    OB.Data.Registry.registerModel('ADList');
    OB.Dal.find(OB.Model.ADList, pickStatusCrit, enyo.bind(this, function (values) {
      enyo.forEach(values.models, function (model) {
        OBWH.Picking.Model.PickingStatus[model.get('searchKey')] = model.get('name');
      });
    }));

    // Load item status names
    pickStatusCrit = {
      _where: 'e.reference.id = \'87D064043A36446B8303A37C0EC929C3\''
    };
    OB.Dal.find(OB.Model.ADList, pickStatusCrit, enyo.bind(this, function (values) {
      enyo.forEach(values.models, function (model) {
        OBWH.Picking.Model.ItemStatus[model.get('searchKey')] = model.get('name');
      });
    }));
  }
});

OBWH.Picking.Model.Item = Backbone.Model.extend({
  addSerial: function (sn) {
    var snl = this.get('serialNumber');
    if(snl){
      if(sn!='0000000000' && snl.includes(sn))
        return;
      snl=snl+","+sn;
    }
    else
      snl = sn;
    console.log(snl);
    this.set('serialNumber', snl);
    this.get('dalItems').forEach(function (dalItem) {
        dalItem.set('serialNumber', snl);
      });
    this.setPickedQty(Number(1), true);
  },
  setItem: function (dalItem) {
    this.get('dalItems').add(dalItem);
    this.set('neededQty', Number(this.get('neededQty')) + Number(dalItem.get('movementQuantity')));
  },
  setPickedQty: function (pickedQty, incremental) {
    var maxQty = Number(this.get('neededQty'));
    if (incremental) {
      pickedQty = this.get('pickedQty') + Number(pickedQty);
    }
    if (Number(pickedQty) > maxQty) {
      pickedQty = maxQty;
    } else if (Number(pickedQty) < 0) {
      pickedQty = 0;
    }

    this.set('pickedQty', pickedQty);
    if (pickedQty === maxQty) {
      this.set('status', 'RE');
      this.get('picking').get('itemsReady').add(this);
    } else {
      this.set('status', 'PE');
      this.get('picking').get('itemsReady').remove(this);
    }
    this.trigger('pickedQtyChanged', this);
  },
  initialize: function () {
    this.set('pickedQty', Number(0));
    this.set('neededQty', Number(0));
    this.set('dalItems', new Backbone.Collection());
    if (this.get('status') === 'CO' || this.get('status') === 'CF') {
      this.set('done', true);
    }
  }
});

OBWH.Picking.Model.Items = Backbone.Collection.extend({
  model: OBWH.Picking.Model.Item
});

OBWH.Picking.Model.Locator = Backbone.Model.extend({
  initialize: function () {
    this.set('items', new OBWH.Picking.Model.Items());
  }
});

OBWH.Picking.Model.Locators = Backbone.Collection.extend({
  model: OBWH.Picking.Model.Locator
});

OBWH.Picking.Model.Picking = Backbone.Model.extend({
  preference: 'OBMWHP_LimitRecord',
  reset: function (dalpicking) {
    var me = this,
        criteria;
    dalpicking = dalpicking || this.get('picking');
    this.set('picking', dalpicking);
    this.set('picking.id', dalpicking.get('id'));
    this.set('picking.name', dalpicking.get('_identifier'));
    this.set('ready', false);
    this.get('items').reset();
    this.get('itemsReady').reset();
    this.get('itemsDone').reset();
    this.get('locators').reset();
    this.set('totalItems', Number(0));
    this.set('done', Number(0));
    criteria = {
      _where: 'e.id = \'' + dalpicking.get('documentType') + '\''
    };
    OB.Dal.find(OB.Model.DocumentType, criteria, enyo.bind(this, function (values) {
      me.set('isGrouping', values.at(0).get('oBWPLIsGroup'));
      me.loadItems();
    }));
  },
  loadItems: function () {
    var items = this.get('items'),
        criteria = {},
        me = this;
    items.reset();
    if (this.get('isGrouping') === true) {
      criteria._where = 'e.oBWPLGroupPickinglist.id =\'' + this.get('picking.id') + '\'';
    } else {
      criteria._where = 'e.oBWPLWarehousePickingList.id =\'' + this.get('picking.id') + '\'';
    }
    criteria._sortBy = 'storageBin._identifier, product._identifier, attributeSetValue._identifier, oBWPLItemStatus';
    if (OB.MobileApp.model.get('permissions')[this.preference] && !isNaN(OB.MobileApp.model.get('permissions')[this.preference])) {
      criteria._limit = Number(OB.MobileApp.model.get('permissions')[this.preference]);
    }
    OB.Dal.find(OB.Model.MaterialMgmtInternalMovementLine, criteria, enyo.bind(this, function (values) {
      enyo.forEach(values.models, function (model) {
        me.addItem(model);
      });
      me.set('done', this.get('itemsDone').length);
      if (this.get('itemsDone').length === this.get('items').length) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMWHP_DoneTitle'), OB.I18N.getLabel('OBMWHP_DoneText'), [{
          label: OB.I18N.getLabel('OBMOBC_LblOk'),
          action: function () {
            OB.MobileApp.model.navigate('wh');
          }
        }, {
          label: OB.I18N.getLabel('OBMOBC_LblCancel')
        }]);
      }
    }));
  },
  addItem: function (dalItem) {
    var dalItemId, baseItem, locator;

    dalItemId = dalItem.get('product') + '$' + dalItem.get('storageBin') + '$' + dalItem.get('attributeSetValue') + '$' + dalItem.get('oBWPLItemStatus');
    baseItem = this.get('items').get(dalItemId);
    if (!baseItem) {
      baseItem = new OBWH.Picking.Model.Item({
        picking: this,
        product: dalItem.get('product'),
        productName: dalItem.get('product$_identifier'),
        storageBin: dalItem.get('storageBin'),
        storageBinName: dalItem.get('storageBin$_identifier'),
        attributeSetValue: dalItem.get('attributeSetValue'),
        attributeSetValueName: dalItem.get('attributeSetValue$_identifier'),
        uom: dalItem.get('uOM$_identifier'),
        status: dalItem.get('oBWPLItemStatus'),
        id: dalItemId
      });
      locator = this.get('locators').get(dalItem.get('storageBin'));
      if (!locator) {
        locator = new OBWH.Picking.Model.Locator({
          picking: this,
          name: dalItem.get('storageBin$_identifier'),
          id: dalItem.get('storageBin')
        });
        this.get('locators').add(locator);
      }
      locator.get('items').add(baseItem);
      if (baseItem.get('status') === 'CO' || baseItem.get('status') === 'CF') {
        this.get('itemsDone').add(baseItem);
      }

      this.get('items').add(baseItem);
    }

    baseItem.setItem(dalItem);
  }
});

OBWH.Picking.Model.PickingStatus = {};
OBWH.Picking.Model.ItemStatus = {};

//EZL
OBWH.Picking.Model.processPickingClass = 'com.softnetworks.warehouse.mobile.picking.ProcessPicking';
OBWH.Picking.Model.getPickingWhereClause = function (qry) {
  var identifier, criteria = {
    _where: 'e.pickliststatus IN (\'AS\', \'IP\', \'IN\') AND e.userContact.id = \'' + OB.MobileApp.model.usermodel.id + '\''
  };

  if (qry) {
    identifier = OB.Constants.FIELDSEPARATOR + OB.Constants.IDENTIFIER;

    criteria = _.extend(criteria, {
      _OrExpression: true,
      operator: 'or',
      _constructor: 'AdvancedCriteria',
      criteria: [{
        'fieldName': 'documentNo',
        'operator': 'iContains',
        'value': qry
      }]
    });
  }
  criteria._sortBy = 'pickliststatus DESC, documentdate';
  return criteria;
};