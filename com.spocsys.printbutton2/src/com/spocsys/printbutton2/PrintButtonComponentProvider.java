package com.spocsys.printbutton2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.client.kernel.KernelConstants;

@ApplicationScoped
@ComponentProvider.Qualifier(PrintButtonComponentProvider.VIEW_COMPONENT_TYPE)
public class PrintButtonComponentProvider extends BaseComponentProvider{
	public static final String VIEW_COMPONENT_TYPE = "SPPRINT_ViewType";
	public static final String module="/com.spocsys.printbutton";
	//@Override
	/*public List<ComponentResource> getGlobalComponentResources() {
		final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();

	    globalResources.add(createStaticResource( "web"+module+"/js/toolbar-btn-cons-purchaseorder.js", false));
	    globalResources.add(createStaticResource( "web"+module+"/js/toolbar-btn-cons-salesorder.js", false));
	    globalResources.add(createStaticResource( "web"+module+"/js/toolbar-btn-ConsignmentPickupOrder.js", false));
	    globalResources.add(createStaticResource( "web"+module+"/js/toolbar-btn-ConsignmentPickupPOrder.js", false));
	    globalResources.add(createStaticResource( "web"+module+"/js/toolbar-btn-GoodsMovements.js", false));
	    globalResources.add(createStaticResource( "web"+module+"/js/toolbar-btn-cons-purchase-send.js", false));
	    globalResources.add(createStaticResource( "web"+module+"/js/windows-send.js", false));
	    globalResources.add(createStaticResource( "web"+module+"/js/windows-print.js", false));
	    globalResources.add(createStaticResource( "web"+module+"/js/toolbar-btn-ConsignmentRFCPurchaseDelivery.js", false));
	    globalResources.add(createStyleSheetResource(
	            "web/org.openbravo.userinterface.smartclient/openbravo/skins/"
	                + KernelConstants.SKIN_VERSION_PARAMETER
	                + module+"/print-styles.css", false));
	    return globalResources;
	}*/

	@Override
	public List<String> getTestResources() {
		return Collections.emptyList();
	}

	@Override
	public Component getComponent(String componentId, Map<String, Object> parameters) {
		// TODO Auto-generated method stub
		return null;
	}
}
