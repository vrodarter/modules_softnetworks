package com.spocsys.printbutton2.process;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import org.openbravo.erpCommon.utility.Utility;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.DocumentTemplate;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.service.db.DbUtility;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.HttpBaseUtils;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.client.application.report.ReportingUtils;


//import com.qualiantech.consignment.sales.ConsOrder;
import com.spocsys.printbutton2.process.Report;
import com.spocsys.printbutton2.utility.PASUtility;

import net.sf.jasperreports.engine.JasperPrint;

public class PrintConsOrderReport2 extends BaseActionHandler{
	private static final Logger log4j = Logger.getLogger(PrintConsOrderReport2.class);
	
	@Override
	  public void execute() {
		final HttpServletRequest request = RequestContext.get().getRequest();
	    final HttpServletResponse response = RequestContext.get().getResponse();
	    String strJSONlines = request.getParameter("orders");
	    String strTabId = request.getParameter("tabid");
	    Writer writer;
	    System.out.println(strJSONlines);
	    System.out.println(strTabId);
		try {
			OBContext.setAdminMode(true);
			JSONArray lines = new JSONArray(strJSONlines);
			
			Tab tab = OBDal.getInstance().get(Tab.class, strTabId);
		    String strDireccion = HttpBaseUtils.getLocalAddress(request);
		    String strActualUrl = HttpBaseUtils.getLocalHostAddress(request);
		    
		    ReportGenerator repGen = new ReportGenerator(strDireccion, strActualUrl);
    	    ArrayList<Report> linestoprint = repGen.generateReports(lines, tab, "print");
    	    ArrayList<JasperPrint> jrPrintReports = repGen.getJrPrintReports();
    	    print(request, response, linestoprint, jrPrintReports, tab, "print");
    	    OBContext.restorePreviousMode();
		} catch (Exception e) {
			OBContext.restorePreviousMode();
			msgOpenbravo(response,e.getMessage());
			// TODO Auto-generated catch block	        
	        e.printStackTrace();
		}
	}
	
	private void msgOpenbravo(HttpServletResponse response,String msg){
		Writer writer;
		msg = msg==null?"--":msg;
		try {
			writer = response.getWriter();
			response.setContentType("text/html; charset=UTF-8");
			writer.write("<HTML><BODY><script type=\"text/javascript\">");
	        writer.write("top.isc.say('" + msg + "');");
	        writer.write("</SCRIPT></BODY></HTML>");
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void print(HttpServletRequest request, HttpServletResponse response,
		      ArrayList<Report> reports, ArrayList<JasperPrint> jrPrintReports, Tab tab, String strAction)
		      throws Exception {

		    String strReportName = "";

		    if (reports.size() == 1) {
		      strReportName = reports.get(0).getFileName();
		    } else {
		      final String dateStamp = Utility.formatDate(new Date(), "yyyyMMdd-HHmmss");
		      strReportName = PASUtility.getTabName(tab, reports.get(0).getLang()) + "_" + dateStamp
		          + ".pdf";
		    }

		    response.setContentType("application/pdf");
		    response.setHeader("Content-disposition", "attachment" + "; filename=" + strReportName);
		    concatReport(reports, jrPrintReports, response, strAction);
		  }

		  private void concatReport(ArrayList<Report> reports, ArrayList<JasperPrint> jrPrintReports,
		      HttpServletResponse response, String strAction) throws Exception {
		    try {
		      ReportingUtils.concatPDFReport(jrPrintReports, true, response.getOutputStream());
		    } catch (Exception e) {
		      log4j.error("Print Error:", e);
		    } finally {
		      for (Report report : reports) {
		        if (strAction.equals("print")) {
		          File file = new File(report.getCompletePath());
		          if (file.exists() && !file.isDirectory()) {
		            file.delete();
		          }
		        }
		      }
		    }
		  }

	
	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {
		return null;
	}

}
