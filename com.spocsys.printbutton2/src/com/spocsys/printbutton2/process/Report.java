package com.spocsys.printbutton2.process;

import org.openbravo.base.model.Property;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Language;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;

public class Report {

  private String lineId = null;
  private String docNo = null;
  private String refNo = null;
  private String attachDir = null;
  private String attachId = null;
  private String fileName = null;
  private String mailTo = null;
  private User userTo = null;
  private String replyTo = null;
  private User userReply = null;
  private String subject = null;
  private String body = null;
  private EmailServerConfiguration emailconf = null;
  private BaseOBObject obObject = null;
  private BusinessPartner bpartner = null;
  private Property propMailLog = null;
  private Language lang = null;

  public BusinessPartner getBpartner() {
    return bpartner;
  }

  public void setBpartner(BusinessPartner bpartner) {
    this.bpartner = bpartner;
  }

  public String getLineId() {
    return lineId;
  }

  public void setLineId(String lineId) {
    this.lineId = lineId;
  }

  public String getAttachDir() {
    return attachDir;
  }

  public void setAttachDir(String attachDir) {
    this.attachDir = attachDir;
  }

  public String getAttachId() {
    return attachId;
  }

  public void setAttachId(String attachId) {
    this.attachId = attachId;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getMailTo() {
    return mailTo;
  }

  public void setMailTo(String mailTo) {
    this.mailTo = mailTo;
  }

  public String getReplyTo() {
    return replyTo;
  }

  public void setReplyTo(String replyTo) {
    this.replyTo = replyTo;
  }

  public String getDocNo() {
    return docNo;
  }

  public void setDocNo(String docNo) {
    this.docNo = docNo;
  }

  public String getRefNo() {
    return refNo;
  }

  public void setRefNo(String refNo) {
    this.refNo = refNo;
  }

  public BaseOBObject getObObject() {
    return obObject;
  }

  public void setObObject(BaseOBObject obObject) {
    this.obObject = obObject;
  }

  public EmailServerConfiguration getEmailconf() {
    return emailconf;
  }

  public void setEmailconf(EmailServerConfiguration emailconf) {
    this.emailconf = emailconf;
  }

  public String getCompletePath() {
    return this.attachDir + "/" + this.fileName;
  }

  public Organization getOrg() {
    return (Organization) obObject.get("organization");
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public User getUserTo() {
    return userTo;
  }

  public void setUserTo(User userTo) {
    this.userTo = userTo;
  }

  public User getUserReply() {
    return userReply;
  }

  public void setUserReply(User userReply) {
    this.userReply = userReply;
  }

  public Property getPropMailLog() {
    return propMailLog;
  }

  public void setPropMailLog(Property propMailLog) {
    this.propMailLog = propMailLog;
  }

  public Language getLang() {
    return lang;
  }

  public void setLang(Language lang) {
    this.lang = lang;
  }

}
