package com.spocsys.printbutton2.process;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.codehaus.jettison.json.JSONArray;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.ConfigParameters;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.application.report.ReportingUtils;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.ad.system.Language;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.model.ad.utility.Attachment;
import org.openbravo.model.common.enterprise.DocumentTemplate;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.warehouse.pickinglist.PickingList;


//import com.qualiantech.consignment.sales.ConsOrder;

public class ReportGenerator {

  private String strActualUrl = null;
  private String strDirection = null;
  private ArrayList<JasperPrint> jrPrintReports;

  public ReportGenerator() {
    jrPrintReports = new ArrayList<JasperPrint>();
  }

  public ReportGenerator(String direction, String actualUrl) {
    strDirection = direction;
    strActualUrl = actualUrl;
    jrPrintReports = new ArrayList<JasperPrint>();
  }

  public ArrayList<Report> generateReports(JSONArray lines, Tab tab, String strAction)
      throws Exception {
	  //strAction = "print";
    ArrayList<Report> printReports = new ArrayList<Report>();
    if (jrPrintReports.size() > 0) {
      jrPrintReports.clear();
    }
    Entity obEntity = (Entity) ModelProvider.getInstance().getEntityByTableId(
        tab.getTable().getId());
    
    Class<?> tabclass = obEntity.getMappingClass();

    for (int i = 0; i < lines.length(); i++) {
    	
    	DocumentTemplate template = null;
    	//Cons_order table
    	if(tab.getId().equalsIgnoreCase("D0E6FA34974B488CBA199CD2585F61DF") 
    			|| tab.getId().equalsIgnoreCase("D2516E05D4A64BB6AF52166154EBD2FF")
    			|| tab.getId().equalsIgnoreCase("4207F363AB3D444ABA0E80D0C8F27F67")
    			|| tab.getId().equalsIgnoreCase("452C85EC2ED148EAB354FFB5C485FB9F")){
    		//ConsOrder order = OBDal.getInstance().get(ConsOrder.class, lines.getString(i));
    		DocumentType doc_transaction = null;//order.getTransactionDocument();
	    	if(doc_transaction != null){
	    		if(doc_transaction.getDocumentTemplateList().isEmpty())
	    			throw new OBException("Define un template para el tipo de documento "+doc_transaction.getName());
	    		else
	    			template = doc_transaction.getDocumentTemplateList().get(0);
	    	}else{
	    		throw new OBException("Documento nulo,define un documento para la ventana");
	    	}		
    	}
    	//M_Inout table
    	if(tab.getId().equalsIgnoreCase("91921883B7F845A3925FF3556D34F54D")){
    		ShipmentInOut inout = OBDal.getInstance().get(ShipmentInOut.class, lines.getString(i));
    		DocumentType doc_transaction = inout.getDocumentType();
    		if(doc_transaction != null){
	    		if(doc_transaction.getDocumentTemplateList().isEmpty())
	    			throw new OBException("Define un template para el tipo de documento "+doc_transaction.getName());
	    		else
	    			template = doc_transaction.getDocumentTemplateList().get(0);
	    	}else{
	    		throw new OBException("Documento nulo,define un documento para la ventana");
	    	}
    	}
	//Picklist
	if(tab.getId().equalsIgnoreCase("7D68FFCA597C4F84BC385DBCA7A8308C")){
		PickingList pickingList = OBDal.getInstance().get(PickingList.class, lines.getString(i));
		DocumentType doc_transaction = pickingList.getDocumentType();
                if(doc_transaction != null){
                        if(doc_transaction.getDocumentTemplateList().isEmpty())
                                throw new OBException("You must configure a template for the type of document "+doc_transaction.getName());
                        else
                                template = doc_transaction.getDocumentTemplateList().get(0);
                }else{
                        throw new OBException("Null document, configure a document for the window");
                }
        }
    	//Movement
    	if(tab.getId().equalsIgnoreCase("259")){
    		InternalMovement movement = OBDal.getInstance().get(InternalMovement.class, lines.getString(i));
    		final OBCriteria<DocumentType> moveCriteria = OBDal.getInstance().createCriteria(DocumentType.class);
    		moveCriteria.add(Restrictions.eq(DocumentType.PROPERTY_ORGANIZATION, movement.getOrganization()));
    		moveCriteria.add(Restrictions.sqlRestriction("ad_table_id='323' and docbasetype='---'"));
    		if(moveCriteria.list().isEmpty()){
    			final OBCriteria<DocumentType> move1Criteria = OBDal.getInstance().createCriteria(DocumentType.class);
        		move1Criteria.add(Restrictions.sqlRestriction("ad_table_id='323' and docbasetype='---' and ad_org_id='0'"));
        		if(move1Criteria.list().isEmpty()){
        			throw new OBException("Documento nulo,define un documento para el documento" +movement.getDocumentNo()+" usando la Tabla:M_Movement");
        		}
        		DocumentType doc_transaction = move1Criteria.list().get(0);
    	    	if(doc_transaction != null){
    	    		if(doc_transaction.getDocumentTemplateList().isEmpty())
    	    			throw new OBException("Define un template para el tipo de documento "+doc_transaction.getName());
    	    		else
    	    			template = doc_transaction.getDocumentTemplateList().get(0);
    	    	}else{
    	    		throw new OBException("Documento nulo,define un documento para la ventana");
    	    	}
    		}else{
    			if(moveCriteria.list().isEmpty()){
        			throw new OBException("Documento nulo,define un documento para el documento" +movement.getDocumentNo()+" usando la Tabla:M_Movement");
        		}
    			DocumentType doc_transaction = moveCriteria.list().get(0);
    	    	if(doc_transaction != null){
    	    		if(doc_transaction.getDocumentTemplateList().isEmpty())
    	    			throw new OBException("Define un template para el tipo de documento "+doc_transaction.getName());
    	    		else
    	    			template = doc_transaction.getDocumentTemplateList().get(0);
    	    	}else{
    	    		throw new OBException("Documento nulo,define un documento para la ventana");
    	    	}
    		}
    	}
      try {

        Report report = new Report();
        report.setLineId(lines.getString(i));
        BaseOBObject obObjectInstance = (BaseOBObject) OBDal.getInstance().get(tabclass,
            report.getLineId());
        report.setObObject(obObjectInstance);

        // Print and Attach
        if (strAction.equals("print") || strAction.equals("attach") || strAction.equals("send")) {

          final Language lang = (report.getLang() != null ? report.getLang() : OBContext
              .getOBContext().getLanguage());
          
          
          if(template.getTemplateLocation() == null)
        	  throw new OBException("Config path @basedesign@");
          if(template.getTemplateFilename() == null)
        	  throw new OBException("Config Report name *.JRXML");
          if(template.getReportFilename() == null)
        	  throw new OBException("Config File name");
          // Get jrxml path
          ConfigParameters confParam = ConfigParameters.retrieveFrom(RequestContext
              .getServletContext());
          String strBasedesing = getBaseDesignPath(confParam);
          String reportPath = template.getTemplateLocation();
          reportPath = reportPath.replaceAll("(?i)@basedesign@", "");
          if (!reportPath.startsWith("/")) {
            reportPath = "/" + reportPath;
          }
          String subreport =strBasedesing + reportPath +  "/";
          reportPath = strBasedesing + reportPath +  "/" +template.getTemplateFilename();
          System.out.println("report path");
          System.out.println(reportPath);

          
          
          JasperDesign jasperDesign = JRXmlLoader.load(reportPath);
          Object[] parameterList = jasperDesign.getParametersList().toArray();
          
          final Locale locLocale = new Locale(lang.getLanguage().substring(0, 2), lang
                  .getLanguage().substring(3, 5));

          String strReplaceWithFull = confParam.strLocalReplaceWith.replace("@actual_url@",
                  strActualUrl).replace("@actual_url_context@", strDirection);
          
          
          HashMap<String, Object> jasperparameters = new HashMap<String, Object>();
          jasperparameters.put("BASE_DESIGN", strBasedesing);
          jasperparameters.put("BASE_WEB", strReplaceWithFull);
          jasperparameters.put("LANGUAGE", lang.getLanguage());
          jasperparameters.put("LOCALE", locLocale);
          jasperparameters.put("DOCUMENT_ID", report.getLineId());
	  jasperparameters.put("id", report.getLineId());
          // Print Report
          JasperPrint jasperPrint = ReportingUtils.generateJasperPrint(reportPath, jasperparameters, true, new DalConnectionProvider(false), null);
          //JasperReport jasperReport = ReportingUtils.getTranslatedJasperReport(
            //  new DalConnectionProvider(false), reportPath, lang.getLanguage(), strBasedesing);
          // JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
         

          


          //JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, jasperparameters,
            //  OBDal.getInstance().getConnection());

          // SaveFile
          final String dateStamp = Utility.formatDate(new Date(), "yyyyMMdd-HHmmss");
          report.setFileName( template.getReportFilename()+"-"+ dateStamp + ".pdf");
          String strAttachDir = "";
          
          /*
          if (jasperPrint.getPages().size() == 0) {
            throw new OBException(OBMessageUtils.messageBD("OBPAS_Error")
                + OBMessageUtils.messageBD("OBPAS_NoPagesInDocument"));
          }
          */
          jrPrintReports.add(jasperPrint);
          // Attach File
          String strAttachId = "";
          if (strAction.equals("print")) {
            strAttachDir = confParam.strFTPDirectory;
          } else if (strAction.equals("attach") || strAction.equals("send")) {
            // The 103 in the following insert specifies the document type: in
            // this case PDF
        	  String pathdir = tab.getTable().getId() + "-"+ report.getLineId();
        	  strAttachDir = confParam.strFTPDirectory + "/" + tab.getTable().getId() + "-"
                      + report.getLineId();
        	  
        	  
        	  System.out.println("AttachDir:"+strAttachDir);
            Organization org = report.getOrg();
            Attachment attach = OBProvider.getInstance().get(Attachment.class);
            attach.setOrganization(org);
            attach.setTable(tab.getTable());
            attach.setRecord(report.getLineId());
            attach.setDataType("103");
            attach.setText(template.getTemplateFilename());
            attach.setPath(pathdir);
            attach.setName(report.getFileName());
            attach.setSequenceNumber(getSeqNo(tab.getTable(), report.getLineId()));
            OBDal.getInstance().save(attach);
            OBDal.getInstance().flush();

            strAttachId = attach.getId();

            
            File attachDir = new File(strAttachDir);
            if (!attachDir.exists()) {
              attachDir.mkdirs();
            }

          }

          report.setAttachDir(strAttachDir);
          report.setAttachId(strAttachId);
          JasperExportManager.exportReportToPdfFile(jasperPrint, report.getCompletePath());

        }

        printReports.add(report);

      } catch (Exception e) {
        // If errors delete All
        for (Report report : printReports) {

          Attachment attach = OBDal.getInstance().get(Attachment.class, report.getAttachId());
          if (attach != null) {
            OBDal.getInstance().remove(attach);
          }

          File reportFile = new File(report.getCompletePath());
          if (reportFile.exists()) {
            reportFile.delete();
          }

          File directory = new File(report.getAttachDir());
          if (directory.isDirectory() && directory.list().length == 0) {
            directory.delete();
          }

        }
        e.printStackTrace();
      }

    }
    return printReports;
  }

  /*
  private Obpas_printsend_template getTemplate(Entity obEntity, BaseOBObject obObjectInstance,
      String docNo) throws Exception {

    // Get DocType Properties
    Entity docType = ModelProvider.getInstance().getEntity("DocumentType");
    @SuppressWarnings("unchecked")
    List<Property> propertyList = obEntity.getProperties();
    ArrayList<Property> propertyDocTypeList = new ArrayList<Property>();
    for (Property prop : propertyList) {
      if (prop.getTargetEntity() == docType) {
        propertyDocTypeList.add(prop);
      }
    }

    Property DocTypeProp = null;
    // Select One Property DocType
    if (propertyDocTypeList.size() == 1) {
      DocTypeProp = propertyDocTypeList.get(0);
    } else if (propertyDocTypeList.size() > 1) {
      for (Property prop : propertyDocTypeList) {
        Column column = OBDal.getInstance().get(Column.class, prop.getColumnId());
        if (column.getObpasMailopt() != null && column.getObpasMailopt().equals("DT")) {
          DocTypeProp = prop;
        }
      }
      if (DocTypeProp == null) {
        DocTypeProp = propertyDocTypeList.get(0);
      }

    }

    if (DocTypeProp == null) {
      throw new OBException(OBMessageUtils.messageBD("OBPAS_Error") + docNo
          + OBMessageUtils.messageBD("OBPAS_NoDocTypeProp"));
    }

    // Get Template
    DocumentType doctype = (DocumentType) obObjectInstance.getValue(DocTypeProp.getName());
    if (doctype == null) {
      throw new OBException(OBMessageUtils.messageBD("OBPAS_Error") + docNo
          + OBMessageUtils.messageBD("OBPAS_NoDocTypeInDocument"));
    } else if (doctype.getObpasPrintsendTemplateList().isEmpty()) {
      throw new OBException(OBMessageUtils.messageBD("OBPAS_Error") + docNo
          + OBMessageUtils.messageBD("OBPAS_NoTemplateDefined"));
    } else {
      for (Obpas_printsend_template temp : doctype.getObpasPrintsendTemplateList()) {
        if (temp.isActive()) {
          return temp;
        }
      }
      throw new OBException(OBMessageUtils.messageBD("OBPAS_Error") + docNo
          + OBMessageUtils.messageBD("OBPAS_NoActiveTemplate"));
    }

  }
  */

  /**
   * Find and set the printing options defined in the entity.
   */
  
  /*
  private void setPrintOptions(Report report, Entity obEntity, BaseOBObject obObjectInstance)
      throws Exception {
    List<Property> propertyList = obEntity.getProperties();
    ArrayList<Property> propertyDocNo = new ArrayList<Property>();
    Property ReferenceNumberProp = null;
    Property propPrintedDate = null;
    for (Property prop : propertyList) {
      if (prop.getColumnId() != null) {
        Column column = OBDal.getInstance().get(Column.class, prop.getColumnId());
        if (prop.isPrimitive() && column.getObpasMailopt() != null) {
          if (column.getObpasMailopt().equals("DN") && prop.getPrimitiveType() == String.class) {
            propertyDocNo.add(prop);
          } else if (column.getObpasMailopt().equals("RN")
              && prop.getPrimitiveType() == String.class) {
            ReferenceNumberProp = prop;
          } else if (column.getObpasMailopt().equals("PD") && prop.getPrimitiveType() == Date.class) {
            propPrintedDate = prop;
          }
        }
      }
    }

    Property DocNoProp = null;
    // Select One Property DocType
    if (propertyDocNo.size() == 1) {
      DocNoProp = propertyDocNo.get(0);
    } else if (propertyDocNo.size() > 1) {
      throw new OBException(OBMessageUtils.messageBD("OBPAS_NoColumnDocNo"));
    }

    if (DocNoProp == null) {
      for (Property prop : propertyList) {
        String propname = prop.getName().trim().toUpperCase();
        if (propname.contains("DOC") && (propname.contains("NO") || propname.contains("NUM"))) {
          DocNoProp = prop;
          break;
        }
      }
    }

    if (DocNoProp != null && obObjectInstance.getValue(DocNoProp.getName()) != null) {
      report.setDocNo(obObjectInstance.getValue(DocNoProp.getName()).toString());
    } else {
      throw new OBException(OBMessageUtils.messageBD("OBPAS_NoColumnDocNo"));
    }

    if (ReferenceNumberProp != null
        && obObjectInstance.getValue(ReferenceNumberProp.getName()) != null) {
      report.setRefNo(obObjectInstance.getValue(ReferenceNumberProp.getName()).toString());
    }

    // Set printed date
    if (propPrintedDate != null && obObjectInstance.getValue(propPrintedDate.getName()) == null) {
      obObjectInstance.setValue(propPrintedDate.getName(), new Date());
      OBDal.getInstance().save(obObjectInstance);
    }

  }
  */

  private String getBaseDesignPath(ConfigParameters confParam) {

    String strNewAddBase = confParam.strDefaultDesignPath;
    String strFinal = confParam.strBaseDesignPath;
    if (!strFinal.endsWith("/" + strNewAddBase)) {
      strFinal += "/" + strNewAddBase;
    }
    System.out.println("Base designt");
    System.out.println(confParam.prefix + strFinal);
    return confParam.prefix + strFinal;
  }

  private Long getSeqNo(Table table, String strId) {
    OBCriteria<Attachment> attCrit = OBDal.getInstance().createCriteria(Attachment.class);
    attCrit.add(Restrictions.eq(Attachment.PROPERTY_TABLE, table));
    attCrit.add(Restrictions.eq(Attachment.PROPERTY_RECORD, strId));
    attCrit.addOrderBy(Attachment.PROPERTY_SEQUENCENUMBER, false);
    List<Attachment> attList = attCrit.list();
    if (attList.isEmpty()) {
      return new Long(10);
    } else {
      Long seqNo = attList.get(0).getSequenceNumber() + 10;
      return seqNo;
    }
  }

  /*
  private void configureSendOptions(Report report, Entity obEntity, BaseOBObject obObjectInstance,
      Obpas_printsend_template template, boolean onlyOne) throws Exception {

    // Get EmailConf looking by Org
    EmailServerConfiguration emailconf = null;
    OBCriteria<EmailServerConfiguration> emailConfCritOrg = OBDal.getInstance().createCriteria(
        EmailServerConfiguration.class);
    emailConfCritOrg.add(Restrictions.eq(EmailServerConfiguration.PROPERTY_ORGANIZATION,
        report.getOrg()));
    if (emailConfCritOrg.list().isEmpty()) {
      // Get EmailConf looking by Client
      OBCriteria<EmailServerConfiguration> emailConfCritClient = OBDal.getInstance()
          .createCriteria(EmailServerConfiguration.class);
      emailConfCritClient.add(Restrictions.eq(EmailServerConfiguration.PROPERTY_CLIENT, report
          .getOrg().getClient()));
      if (emailConfCritClient.list().isEmpty()) {
        // Get EmailConf looking by System
        OBCriteria<EmailServerConfiguration> emailConfCritSystem = OBDal.getInstance()
            .createCriteria(EmailServerConfiguration.class);
        emailConfCritSystem.add(Restrictions.eq(EmailServerConfiguration.PROPERTY_CLIENT, OBDal
            .getInstance().get(Client.class, "0")));
        if (emailConfCritSystem.list().isEmpty()) {
          throw new OBException(OBMessageUtils.messageBD("OBPAS_Error") + report.getDocNo()
              + OBMessageUtils.messageBD("OBPAS_NoEmailConf"));
        }
      } else {
        emailconf = emailConfCritClient.list().get(0);
      }
    } else {
      emailconf = emailConfCritOrg.list().get(0);
    }

    User mailToUser = null;
    User replyToUser = null;
    BusinessPartner bp = null;
    String strDocStatus = null;

    List<Property> propertyList = obEntity.getProperties();
    Entity userEntity = ModelProvider.getInstance().getEntity("ADUser");
    Entity bpEntity = ModelProvider.getInstance().getEntity("BusinessPartner");
    // Loop Properties to get values

    for (Property prop : propertyList) {
      if (prop.getTargetEntity() == userEntity && !prop.getName().equalsIgnoreCase("updatedBy")
          && !prop.getName().equalsIgnoreCase("createdBy")) {
        Column column = OBDal.getInstance().get(Column.class, prop.getColumnId());
        if (column.getObpasMailopt() != null) {
          if (column.getObpasMailopt().equals("MT")) {
            mailToUser = (User) obObjectInstance.getValue(prop.getName());
          } else if (column.getObpasMailopt().equals("RT")) {
            replyToUser = (User) obObjectInstance.getValue(prop.getName());
          }
        }
      } else if (prop.getTargetEntity() == bpEntity) {
        Column column = OBDal.getInstance().get(Column.class, prop.getColumnId());
        if (column.getObpasMailopt() != null && column.getObpasMailopt().equals("BP")) {
          bp = (BusinessPartner) obObjectInstance.getValue(prop.getName());
          report.setBpartner(bp);
        }
      } else if (prop.isPrimitive() && prop.getPrimitiveType() == String.class) {
        Column column = OBDal.getInstance().get(Column.class, prop.getColumnId());
        if (column.getObpasMailopt() != null && column.getObpasMailopt().equals("ML")) {
          report.setPropMailLog(prop);
        }
        if (column.getObpasMailopt() != null && column.getObpasMailopt().equals("DS")) {
          strDocStatus = (String) obObjectInstance.getValue(prop.getName());
        }
      }
    }

    if (strDocStatus != null && strDocStatus.equals("DR")) {
      throw new OBException(OBMessageUtils.messageBD("OBPAS_Error") + report.getDocNo()
          + OBMessageUtils.messageBD("OBPAS_DocumentDraft"));
    }

    String mailTo = null;
    // Get Mail Contact
    if (hasMail(mailToUser)) {
      mailTo = mailToUser.getEmail();
      report.setUserTo(mailToUser);
    } else if (bp != null) {
      if (bp.getADUserList().size() == 1 && hasMail(bp.getADUserList().get(0))) {
        mailTo = bp.getADUserList().get(0).getEmail();
        report.setUserTo(bp.getADUserList().get(0));
      } else if (bp.getADUserList().size() > 1) {
        for (User user : bp.getADUserList()) {
          if (user.isObpasSenddefault() && hasMail(user)) {
            mailTo = user.getEmail();
            report.setUserTo(user);
          }
        }
      }
    }
    if ((mailTo == null || mailTo.equals("")) && !onlyOne) {
      throw new OBException(OBMessageUtils.messageBD("OBPAS_Error") + report.getDocNo()
          + OBMessageUtils.messageBD("OBPAS_EmptyEmailContact"));
    }

    report.setEmailconf(emailconf);
    report.setMailTo(mailTo);

    // Get Reply
    if (hasMail(replyToUser)) {
      report.setReplyTo(replyToUser.getEmail());
      report.setUserReply(replyToUser);
    } else {
      report.setReplyTo(report.getEmailconf().getSmtpServerSenderAddress());
    }

    // Get Subject and Body
    ObpasPrintsendMaildef mailDef = null;
    Language lang = ((report.getUserTo() != null && report.getUserTo().getDefaultLanguage() != null) ? report
        .getUserTo().getDefaultLanguage() : OBContext.getOBContext().getLanguage());
    report.setLang(lang);
    OBCriteria<ObpasPrintsendMaildef> mailDefcri = OBDal.getInstance().createCriteria(
        ObpasPrintsendMaildef.class);
    mailDefcri
        .add(Restrictions.eq(ObpasPrintsendMaildef.PROPERTY_OBPASPRINTSENDTEMPLATE, template));
    mailDefcri.add(Restrictions.eq(ObpasPrintsendMaildef.PROPERTY_LANGUAGE, lang));
    if (mailDefcri.list().size() == 1) {
      mailDef = mailDefcri.list().get(0);
    } else if (mailDefcri.list().size() > 1) {
      mailDefcri.add(Restrictions.eq(ObpasPrintsendMaildef.PROPERTY_DEFAULT, true));
      mailDef = mailDefcri.list().get(0);
    } else {
      if (!onlyOne) {
        throw new OBException(OBMessageUtils.messageBD("OBPAS_Error") + report.getDocNo()
            + OBMessageUtils.messageBD("OBPAS_ErrorNoEmailTemplate"));
      }
    }

    if (mailDef != null) {
      report.setSubject(mailDef.getSubjectPas());
      report.setBody(mailDef.getBodyPas());
    }

  }
  */

  private boolean hasMail(User user) {
    return (user != null && user.getEmail() != null && !user.getEmail().equals(""));
  }

  public ArrayList<JasperPrint> getJrPrintReports() {
    return jrPrintReports;
  }

}
