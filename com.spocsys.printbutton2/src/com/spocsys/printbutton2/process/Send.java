package com.spocsys.printbutton2.process;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.HttpBaseUtils;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.erpCommon.utility.poc.EmailType;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.interaction.EmailInteraction;
import org.openbravo.service.db.DbUtility;
import org.openbravo.utils.FormatUtilities;


public class Send extends BaseActionHandler {

  private static final Logger log4j = Logger.getLogger(Send.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {

    JSONObject result = new JSONObject();

    try {
      JSONObject data = new JSONObject(content);
      JSONArray lines = data.getJSONArray("lines");
      System.out.println("Email process");
      System.out.println(content);
      final HttpServletRequest request = RequestContext.get().getRequest();
      String strTabId = data.getString("tabid");
      OBContext.setAdminMode(true);
      Tab tab = OBDal.getInstance().get(Tab.class, strTabId);
      JSONObject infoForm = data.getJSONObject("infoForm");

      String strDireccion = HttpBaseUtils.getLocalAddress(request);
      String strActualUrl = HttpBaseUtils.getLocalHostAddress(request);

      ReportGenerator repGen = new ReportGenerator(strDireccion, strActualUrl);
      ArrayList<Report> reportstosend = repGen.generateReports(lines, tab, "send");

      boolean moreThanOne = (reportstosend.size() > 1);

      String strResult = "";

      for (Report report : reportstosend) {
        strResult = strResult + sendDocumentEmail(report, moreThanOne, data, tab, infoForm,null);
      }

      strResult = (strResult.equals("") ? OBMessageUtils.messageBD("OBPAS_MailsOk") : strResult);

      OBContext.restorePreviousMode();

      result.put("result", strResult);
      return result;

    } catch (Exception e) {
      OBContext.restorePreviousMode();
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      log4j.error("Print and Send - Error Sending Mail:", ex);
      try {
        result.put("result", ex.getMessage());
      } catch (JSONException e1) {
        log4j.error("Print and Send - Error Sending Mail:", e1);
      }
      return result;
    }
  }

  private String sendDocumentEmail(Report report, boolean moreThanOne, JSONObject data, Tab tab,
      JSONObject infoForm,File file) throws Exception {
	  
	System.out.println("Send email");
    String errormsg = "";

    EmailServerConfiguration mailConfig = report.getEmailconf();
    String senderAddress = mailConfig.getSmtpServerSenderAddress();
    
    if(mailConfig == null){
    	errormsg = errormsg + "</br>";
    }
    String toEmail = null;
    String replyToEmail = null;
    String emailSubject = null;
    String emailBody = null;

    if (moreThanOne) {
      toEmail = report.getMailTo();
      replyToEmail = report.getReplyTo();
      emailSubject = report.getSubject();
      emailBody = report.getBody();
    } else {
      toEmail = infoForm.getString("mailTo");
      replyToEmail = infoForm.getString("replyTo");
      emailSubject = infoForm.getString("subject");
      emailBody = infoForm.getString("body");
    }

    //emailSubject = PASUtility.replaceText(emailSubject, report);
    //emailBody = PASUtility.replaceText(emailBody, report);

    boolean auth = true;

    if ("N".equals(mailConfig.isSMTPAuthentification())) {
      auth = false;
    }
    String host = mailConfig.getSmtpServer();
    String username = mailConfig.getSmtpServerAccount();
    String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(), false);
    String connSecurity = mailConfig.getSmtpConnectionSecurity();
    int port = mailConfig.getSmtpPort().intValue();

    final String recipientTO = toEmail;
    final String recipientCC = (infoForm.has("cc") ? infoForm.getString("cc") : "");
    final String recipientBCC = (infoForm.has("bcc") ? infoForm.getString("bcc") : "");
    final String replyTo = replyToEmail;
    final String contentType = "text/plain; charset=utf-8";

    if (log4j.isDebugEnabled()) {
      log4j.debug("From: " + senderAddress);
      log4j.debug("Recipient TO (contact email): " + recipientTO);
      log4j.debug("Recipient CC: " + recipientCC);
      log4j.debug("Recipient BCC (user email): " + recipientBCC);
      log4j.debug("Reply-to (sales rep email): " + replyTo);
    }
    
    System.out.println("Email send");

    List<File> attachments = new ArrayList<File>();
    attachments.add(new File(report.getCompletePath()));
    if(file!= null)
    	attachments.add(file);
    boolean mailsended = false;
    
    System.out.println("Email send attach");
    try {
      EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress,
          recipientTO, recipientCC, recipientBCC, replyTo, emailSubject, emailBody, contentType,
          attachments, null, null);
      mailsended = true;
      System.out.println("Email send");	
    } catch (Exception exception) {
      // Revisar
    	System.out.println("error:"+exception.getMessage());
      log4j.error(exception);
      errormsg = errormsg + OBMessageUtils.messageBD("OBPAS_ErrorMail") + report.getDocNo()
          + OBMessageUtils.messageBD("OBPAS_MailError") + " " + exception.getMessage() + "</br>";
    }

    if (mailsended == true) {
      // Store the email message
      final String newEmailId = SequenceIdData.getUUID();
      if (log4j.isDebugEnabled())
        log4j.debug("New email id: " + newEmailId);

      EmailInteraction email = OBProvider.getInstance().get(EmailInteraction.class);
      email.setOrganization(report.getOrg());
      email.setEmailType(EmailType.OUTGOING.getStringValue());
      email.setAddressFrom(replyTo);
      email.setAddressTo(recipientTO);
      email.setAddressCc(recipientCC);
      email.setAddressBcc(recipientBCC);
      email.setDateofemail(new Date());
      email.setSubject(emailSubject);
      email.setBody(emailBody);
      email.setBusinessPartner(null);
      email.setTable(tab.getTable());
      email.setDocument(report.getLineId());
      OBDal.getInstance().save(email);

      // Set Mail Log Info
      if (report.getPropMailLog() != null) {
        String dateTimeFormat = OBPropertiesProvider.getInstance().getOpenbravoProperties()
            .getProperty("dateTimeFormat.java");
        BaseOBObject obObject = report.getObObject();
        String strMailLog = "";
        if (obObject.getValue(report.getPropMailLog().getName()) != null) {
          strMailLog = obObject.getValue(report.getPropMailLog().getName()).toString();
        }
        final String dateStamp = Utility.formatDate(new Date(), dateTimeFormat);
        strMailLog = OBMessageUtils.messageBD("OBPAS_MailSent") + dateStamp + "\n"
            + OBMessageUtils.messageBD("OBPAS_Attachment") + report.getFileName() + "\n"
            + OBMessageUtils.messageBD("OBPAS_MailTo") + recipientTO + "\n---\n" + strMailLog;
        if (strMailLog.length() > 4000) {
          strMailLog = strMailLog.substring(0, 3999);
        }
        obObject.setValue(report.getPropMailLog().getName(), strMailLog);
        OBDal.getInstance().save(obObject);
      }

    }
    System.out.println("terminando envio de mail");
    return errormsg;

  }
}
