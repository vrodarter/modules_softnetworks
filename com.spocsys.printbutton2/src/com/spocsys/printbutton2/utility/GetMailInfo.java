package com.spocsys.printbutton2.utility;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.service.db.DbUtility;

import org.openbravo.model.ad.access.User;

//import com.qualiantech.consignment.sales.ConsOrder;
import com.spocsys.printbutton2.process.Report;
import com.spocsys.printbutton2.process.ReportGenerator;


public class GetMailInfo extends BaseActionHandler {

  private static final Logger log4j = Logger.getLogger(GetMailInfo.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {

    JSONObject result = new JSONObject();

    try {
      JSONObject data = new JSONObject(content);
      JSONArray lines = data.getJSONArray("lines");
      String strTabId = data.getString("tabid");

      OBContext.setAdminMode(true);
      Tab tab = OBDal.getInstance().get(Tab.class, strTabId);

      // Get mail info for one mail
      if (lines.length() != 1) {
        throw new OBException("Error: GetMailInfo only for one line");
      }
      
      Report report = null;
      
      for (int i = 0; i < lines.length(); i++) {
	      if(tab.getId().equalsIgnoreCase("D0E6FA34974B488CBA199CD2585F61DF") 
	  			|| tab.getId().equalsIgnoreCase("D2516E05D4A64BB6AF52166154EBD2FF")
	  			|| tab.getId().equalsIgnoreCase("4207F363AB3D444ABA0E80D0C8F27F67")
	  			|| tab.getId().equalsIgnoreCase("452C85EC2ED148EAB354FFB5C485FB9F")){
	  		//ConsOrder order = OBDal.getInstance().get(ConsOrder.class, lines.getString(i));
	  		//report = getUserData(order);
	      }
      }

      
      OBContext.restorePreviousMode();
      result.put("mailTo", report.getMailTo());
      result.put("replyTo", report.getReplyTo());
      result.put("subject", report.getSubject());
      result.put("body", report.getBody());

    } catch (Exception e) {
      OBContext.restorePreviousMode();
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      log4j.error("Error Sending Mail:", ex);
      try {
        result.put("error", ex.getMessage());
      } catch (JSONException e1) {
        log4j.error("Error Sending Mail:", e1);
      }
    }
    return result;

  }
  
  /*private Report getUserData(ConsOrder order){
	  if(order.getSicrAdUser() != null){
		  Report report = new Report();
		  report.setMailTo(order.getSicrAdUser().getEmail());
		  report.setSubject(order.getDocumentNo());
		  report.setBody("");
		  return report;
	  }else{
		  Report report = new Report();
		  report.setMailTo("");
		  report.setSubject(order.getDocumentNo());
		  report.setBody("");
		  return report;
	  }
	  
  }*/
}
