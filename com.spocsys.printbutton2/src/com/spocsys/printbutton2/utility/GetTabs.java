package com.spocsys.printbutton22.utility;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_actionButton.CreateStandards;
import org.openbravo.model.ad.ui.Tab;

public class GetTabs extends BaseActionHandler {

  private static final Logger log4j = Logger.getLogger(CreateStandards.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {
    JSONArray printTabs = new JSONArray();
    JSONArray sendTabs = new JSONArray();

    OBContext.setAdminMode(true);
    OBCriteria<Tab> TabsCriteria = OBDal.getInstance().createCriteria(Tab.class);
    // Print Tabs
    /*
    String[] pasOptions = { "P", "PS" };
    TabsCriteria.add(Restrictions.in(Tab.PROPERTY_OBPASPRINTANDSENDFUNCTIONS, pasOptions));
    for (Tab tab : TabsCriteria.list()) {
      printTabs.put(tab.getId());
    }
    // Send Tabs
    TabsCriteria.add(Restrictions.eq(Tab.PROPERTY_OBPASPRINTANDSENDFUNCTIONS, "PS"));
    for (Tab tab : TabsCriteria.list()) {
      sendTabs.put(tab.getId());
    }
    */
    OBContext.restorePreviousMode();

    JSONObject result = new JSONObject();
    try {
      result.put("printTabs", printTabs);
      result.put("sendTabs", sendTabs);
    } catch (JSONException e) {
      OBContext.restorePreviousMode();
      log4j.error("GetTabs", e);
    }

    return result;
  }
}
