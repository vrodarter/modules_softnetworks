package com.spocsys.printbutton2.utility;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.system.Language;
import org.openbravo.model.ad.ui.Tab;
import org.openbravo.model.ad.ui.TabTrl;
import org.openbravo.model.ad.ui.Window;
import org.openbravo.model.ad.ui.WindowTrl;

import com.spocsys.printbutton2.process.Report;


public class PASUtility {

  public static String getTabName(Tab tab, Language lang) {
    String tabName = tab.getName();
    if (tabName.equals("Header")) {
      tabName = getTranslatedWindowName(tab, lang);
    } else {
      tabName = getTranslatedTabName(tab, lang);
    }
    tabName = tabName.replaceAll(" ", "_");
    return tabName;
  }

  public static String replaceText(String text, Report report) {
    String replacedText = text;

    replacedText = replacedText.replaceAll("@docNo@", report.getDocNo());
    if (report.getUserTo() != null) {
      replacedText = replacedText.replaceAll("@contactName@", report.getUserTo().getName());
    }
    if (report.getUserReply() != null) {
      replacedText = replacedText.replaceAll("@replyName@", report.getUserReply().getName());
    }
    if (report.getBpartner() != null) {
      replacedText = replacedText.replaceAll("@partnerName@", report.getBpartner().getName());
    }
    if (report.getRefNo() != null) {
      replacedText = replacedText.replaceAll("@referenceNo@", report.getRefNo());
    }

    return replacedText;
  }

  private static String getTranslatedTabName(Tab tab, Language lang) {
    OBCriteria<TabTrl> c = OBDal.getInstance().createCriteria(TabTrl.class);
    c.add(Restrictions.eq(TabTrl.PROPERTY_TAB, tab));
    c.add(Restrictions.eq(TabTrl.PROPERTY_LANGUAGE, lang));
    TabTrl trl = (TabTrl) c.uniqueResult();
    if (trl == null) {
      return tab.getName();
    }
    return trl.getName();
  }

  private static String getTranslatedWindowName(Tab tab, Language lang) {
    Window w = tab.getWindow();
    OBCriteria<WindowTrl> c = OBDal.getInstance().createCriteria(WindowTrl.class);
    c.add(Restrictions.eq(WindowTrl.PROPERTY_WINDOW, w));
    c.add(Restrictions.eq(WindowTrl.PROPERTY_LANGUAGE, lang));
    WindowTrl trl = (WindowTrl) c.uniqueResult();
    if (trl == null) {
      return w.getName();
    }
    return trl.getName();
  }

}
