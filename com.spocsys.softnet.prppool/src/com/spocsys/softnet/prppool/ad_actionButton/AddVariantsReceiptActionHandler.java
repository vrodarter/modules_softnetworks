/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011-2015 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.spocsys.softnet.prppool.ad_actionButton;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletException;
import java.util.Iterator;
import java.lang.Object;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBDao;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.financial.FinancialUtils;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.order.ReturnReason;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCharacteristic;
import org.openbravo.model.common.plm.CharacteristicValue;
import org.openbravo.model.common.plm.ProductCharacteristicConf;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;
import org.openbravo.exception.NoConnectionAvailableException;
import org.hibernate.Criteria;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
//import com.qualiantech.consignment.sales.ConsOrder;
//import com.qualiantech.consignment.sales.ConsOrderLine;

/**
 * 
 * @author Victor Rodarte
 * 
 */
//public class AddVariantsActionHandler extends BaseProcessActionHandler {
public class AddVariantsReceiptActionHandler extends BaseProcessActionHandler {
  private static Logger log = Logger.getLogger(AddVariantsReceiptActionHandler.class);
  private static String SERVICEPRODUCT = "S";
  private Connection connection = null;  

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    JSONObject jsonRequest = null;
    OBContext.setAdminMode();
    try {
      jsonRequest = new JSONObject(content);
      log.debug(jsonRequest);
      // When the focus is NOT in the tab of the button (i.e. any child tab) and the tab does not
      // contain any record, the inpcOrderId parameter contains "null" string. Use C_Order_ID
      // instead because it always contains the id of the selected order.
      // Issue 20585: https://issues.openbravo.com/view.php?id=20585
      //System.out.println(jsonRequest.toString());
      final String strM_InOutLineId = jsonRequest.getString("M_InOutLine_ID");


      ShipmentInOutLine shipmentInOutLine = OBDal.getInstance().get(ShipmentInOutLine.class, strM_InOutLineId);

      if (shipmentInOutLine != null) {
	
	     //addVariants(strProductId);	

        String message = createVariants(jsonRequest);
		JSONObject resultMessage = new JSONObject();
		resultMessage.put("severity", "success");
		resultMessage.put("title", "Process successful");
		resultMessage.put("text", message);
		jsonRequest.put("message", resultMessage);
      }

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      VariablesSecureApp vars = RequestContext.get().getVariablesSecureApp();
      log.error(e.getMessage(), e);

      try {
        jsonRequest = new JSONObject();
        Throwable ex = DbUtility.getUnderlyingSQLException(e);
        String message = OBMessageUtils.translateError(new DalConnectionProvider(), vars,
            vars.getLanguage(), ex.getMessage()).getMessage();
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", message);
        jsonRequest.put("message", errorMessage);

      } catch (Exception e2) {
        log.error(e.getMessage(), e2);
        // do nothing, give up
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return jsonRequest;
  }



  private String createVariants(JSONObject jsonRequest) throws JSONException,
      OBException {
    //System.out.println(jsonRequest.toString());
    JSONObject grid = jsonRequest.getJSONObject("_params").getJSONObject("grid");

    JSONArray selectedLines = grid.getJSONArray("_selection");
    int line = Integer.parseInt(jsonRequest.getString("inpline"));

    //final String strProductId = jsonRequest.getString("M_Product_ID");
    
    //Product product = OBDal.getInstance().get(Product.class, strProductId);
    ShipmentInOutLine shipmentInOutLineOld =  OBDal.getInstance().get(ShipmentInOutLine.class, jsonRequest.getString("M_InOutLine_ID"));

    int iterador = 0;
    int total = Integer.parseInt(jsonRequest.getString("inpmovementqty"));
    for (long i = 0; i < selectedLines.length(); i++) {
      line++;
      //iterador = iterador + Integer.parseInt(selectedLines.getString("qty"));
      //if (iterador > total){
	//log.error("The quantity in variants is greater than quantity in line");
      //}
      JSONObject selectedLine = selectedLines.getJSONObject((int) i);
      iterador = iterador + Integer.parseInt(selectedLine.getString("qty"));
      if (iterador > total){
        log.error("The quantity in variants is greater than quantity in line");
	      throw new OBException("The quantity in variants is greater than quantity in the line "+jsonRequest.getString("inpline") );
      }

      log.debug(selectedLine);


      //String strProductCharacteristicId = null;
      String strProductNewId = null;
      //System.out.println("product.............................................");
      //System.out.println(selectedLine.getString("product"));
      //System.out.println("skcode..............................................");
      //System.out.println(selectedLine.getString("skcode"));
      strProductNewId = getProductNewId(selectedLine.getString("product"),selectedLine.getString("skcode"));
      

      if (strProductNewId == null){
        log.error("Error null Product");
	       throw new OBException("The variant " + selectedLine.getString("skcode")
		    + " is not created ");
      }

      Product productNew = OBDal.getInstance().get(Product.class, strProductNewId);
      ShipmentInOutLine shipmentInOutLineNew = OBProvider.getInstance().get(ShipmentInOutLine.class);
      shipmentInOutLineNew.setClient(shipmentInOutLineOld.getClient());
      shipmentInOutLineNew.setOrganization(shipmentInOutLineOld.getOrganization());
      shipmentInOutLineNew.setCreatedBy(shipmentInOutLineOld.getCreatedBy());
      shipmentInOutLineNew.setUpdatedBy(shipmentInOutLineOld.getUpdatedBy());
      shipmentInOutLineNew.setLineNo(Long.valueOf(line));
      shipmentInOutLineNew.setShipmentReceipt(shipmentInOutLineOld.getShipmentReceipt());
      shipmentInOutLineNew.setStorageBin(shipmentInOutLineOld.getStorageBin());
      shipmentInOutLineNew.setProduct(productNew);
      shipmentInOutLineNew.setUOM(shipmentInOutLineOld.getUOM());
      shipmentInOutLineNew.setMovementQuantity(new BigDecimal(selectedLine.getString("qty")));
      shipmentInOutLineNew.setSsppProductOrg(shipmentInOutLineOld.getSalesOrderLine());
      shipmentInOutLineNew.getShipmentReceipt().setSsppCreatepo(true);
      OBDal.getInstance().save(shipmentInOutLineNew);
      //OBDal.getInstance().remove(shipmentInOutLineOld);
      
      /*
      productCharacteristicConf.setActive(true);
      */

      //OBDal.getInstance().save(productCharacteristicConf);
      OBDal.getInstance().flush();

    }
    //shipmentInOutLineOld.setMovementQuantity(BigDecimal.ZERO);
    //shipmentInOutLineOld.setActive(false);
    //OBDal.getInstance().remove(shipmentInOutLineOld);
    deleteReceipt(shipmentInOutLineOld.getId());
    OBDal.getInstance().flush(); 
    
	return "Variants added";
  }

  public void addVariants(String strProductId) {

	final String strUserId = OBContext.getOBContext().getUser().getId();     
        String strQuery = "select * from sspp_add_variants_fnc(?,?) ";
       
        try {
                connection = OBDal.getInstance().getConnection();
        } catch (Exception e) {
                log.error("No Database Connection Available," + e);
                //throw new ServletException(e);
        }

        CallableStatement procedure = null;

        try {
          procedure = connection.prepareCall(strQuery);
          procedure.setString(1, strProductId);
          procedure.setString(2, strUserId);
          procedure.execute();
        } catch (SQLException e) {
          log.error("Sql Exception sspp_add_variants_fnc" + e);
          //setResult(false);
          //setMessage(ConsignmentUtil.parseMessage(connection, "CONS_FILLUP_MVMTPOSTERR",
          //    vars.getLanguage()));
        } finally {
          try {
            procedure.close();
          } catch (SQLException e) {
            e.printStackTrace();
          }
        }
  }

  private String getProductCharacteristic(String strProductId) {

    try {
                connection = OBDal.getInstance().getConnection();
        } catch (Exception e) {
                log.error("No Database Connection Available," + e);
                //throw new ServletException(e);
        }

    String productCharacteristicId = null;

    String sqlQuery = "";
    sqlQuery += "SELECT M_Product_Ch_id "+
		"FROM M_Product_Ch "+
		"WHERE m_product_id = ? "+
		"  AND m_characteristic_id = '3B672AD4A4EE42DA95DA9D4A430F64F5' ";

    PreparedStatement selectst = null;
    ResultSet result = null;

    try {
      selectst = connection.prepareStatement(sqlQuery);
      selectst.setString(1, strProductId);

      result = selectst.executeQuery();

      if (result.next())
        productCharacteristicId = result.getString(1);

      result.close();
      selectst.close();
    } catch (SQLException e) {
      log.error("SQLException while attempt to get Customer Consignment Locator Id," + e);
    }

    return productCharacteristicId;
  }


  private String getProductNewId(String strProductId,String strSkcode) {

    try {
                connection = OBDal.getInstance().getConnection();
        } catch (Exception e) {
                log.error("No Database Connection Available," + e);
                //throw new ServletException(e);
        }

    String productId = null;

    String sqlQuery = "";
    //sqlQuery += "SELECT m_product_id FROM m_product WHERE value ILIKE '"+
    //"((SELECT value FROM M_Product WHERE generic_product_id = ?)||'-'||?)'";
    sqlQuery += "SELECT m_product_id FROM m_product WHERE value ILIKE "+
	" (SELECT value from m_product where m_product_id = "+
	" (SELECT generic_product_id FROM M_Product WHERE m_product_id = ?))||?||?";

    PreparedStatement selectst = null;
    ResultSet result = null;

    try {
      selectst = connection.prepareStatement(sqlQuery);
      selectst.setString(1, strProductId);
      selectst.setString(2, "-");
      selectst.setString(3, strSkcode);

      result = selectst.executeQuery();

      if (result.next())
        productId = result.getString(1);

      result.close();
      selectst.close();
    } catch (SQLException e) {
      log.error("SQLException while attempt to get Customer Consignment Locator Id," + e);
    }

    return productId;
  }



  public void deleteReceipt(String strM_InOutLineId) {

  final String strUserId = OBContext.getOBContext().getUser().getId();     
        String strQuery = "DELETE FROM m_inoutline WHERE m_inoutline_id = ? ";
       
        try {
                connection = OBDal.getInstance().getConnection();
        } catch (Exception e) {
                log.error("No Database Connection Available," + e);
                //throw new ServletException(e);
        }

        CallableStatement procedure = null;

        try {
          procedure = connection.prepareCall(strQuery);
          procedure.setString(1, strM_InOutLineId);
          procedure.execute();
        } catch (SQLException e) {
          log.error("Sql Exception sspp_add_variants_fnc" + e);
          //setResult(false);
          //setMessage(ConsignmentUtil.parseMessage(connection, "CONS_FILLUP_MVMTPOSTERR",
          //    vars.getLanguage()));
        } finally {
          try {
            procedure.close();
          } catch (SQLException e) {
            e.printStackTrace();
          }
        }
  }



  private String getProductCharacteristicConf(String strCharacteristicValueId,String strProductCharacteristicId) {

    try {
                connection = OBDal.getInstance().getConnection();
        } catch (Exception e) {
                log.error("No Database Connection Available," + e);
                //throw new ServletException(e);
        }

    String productCharacteristicConfId = null;

    String sqlQuery = "";
    sqlQuery += "SELECT m_product_ch_conf_id "+
                "FROM m_product_ch_conf "+
                "WHERE m_ch_value_id = ? "+
                "  AND m_product_ch_id = ? ";

    PreparedStatement selectst = null;
    ResultSet result = null;

    try {
      selectst = connection.prepareStatement(sqlQuery);
      selectst.setString(1, strCharacteristicValueId);
      selectst.setString(2, strProductCharacteristicId);

      result = selectst.executeQuery();

      if (result.next())
        productCharacteristicConfId = result.getString(1);

      result.close();
      selectst.close();
    } catch (SQLException e) {
      log.error("SQLException while attempt to get Customer Consignment Locator Id," + e);
    }

    return productCharacteristicConfId;
  }


  
}


