/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.spocsys.softnetworks.addserno;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

import com.spocsys.softnetworks.addserno.data.CustsupInoutline;
import com.spocsys.softnetworks.addserno.data.CustsupMovementline;
import com.spocsys.softnetworks.desarrollos.td_technicaldata;

/**
 * @author mperez
 * 
 */
public class AddSernoActionHandler extends BaseProcessActionHandler {

  private static final Logger log = Logger.getLogger(AddSernoActionHandler.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    JSONObject jsonRequest = null;
    OBContext.setAdminMode();
    try {
      jsonRequest = new JSONObject(content);
      log.debug(jsonRequest);
      // Get window from it has been executed
      // 184 => Goods Receipt
      // 169 => Goods Shipment
      // 170 => Goods Movement
      String strWindowId = jsonRequest.getString("inpwindowId");
      if (strWindowId.equals("169")) {
        addSerialNumbersGS(jsonRequest);
      } else if (strWindowId.equals("184")) {
        addSerialNumbersGR(jsonRequest);
      } else if (strWindowId.equals("170")) {
        addSerialNumbersGM(jsonRequest);
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return jsonRequest;

  }

  // Goods Movements
  private void addSerialNumbersGM(JSONObject jsonRequest) throws JSONException {
    JSONArray selectedLines = jsonRequest.getJSONArray("_selection");
    JSONObject msg = new JSONObject();
    String strMovementlineId = "";

    // if no lines selected don't do anything.
    if (selectedLines.length() == 0) {
      return;
    }

    for (int i = 0; i < selectedLines.length(); i++) {

      JSONObject selectedId = selectedLines.getJSONObject(i);
      String strId = selectedId.getString("_identifier");

      // Get Technical Data if it is Goods Movements
      String strInTechnicalDataId = "";
      strInTechnicalDataId = selectedId.getString("technicaldata");
      td_technicaldata technicaldata = null;

      if (!strInTechnicalDataId.equals("")) {
        technicaldata = OBDal.getInstance().get(td_technicaldata.class, strInTechnicalDataId);
      }

      // Get the Line of the Temporary Table
      CustsupMovementline custsupMovementline = OBDal.getInstance().get(CustsupMovementline.class,
          strId);
      if (technicaldata != null) {
        // Insert new lines of goods movement
        InternalMovementLine internalMovementLine = OBProvider.getInstance()
            .get(InternalMovementLine.class);
        internalMovementLine.setClient(custsupMovementline.getClient());
        internalMovementLine.setOrganization(custsupMovementline.getOrganization());
        internalMovementLine.setActive(true);
        internalMovementLine.setLineNo(custsupMovementline.getLineNew());
        internalMovementLine.setDescription(custsupMovementline.getDescription());
        internalMovementLine.setMovement(custsupMovementline.getMovement());
        internalMovementLine.setCustgsOrderline(custsupMovementline.getOrderline());
        internalMovementLine.setStorageBin(custsupMovementline.getLocator());
        internalMovementLine.setNewStorageBin(custsupMovementline.getLocatorto());
        internalMovementLine.setProduct(custsupMovementline.getProduct());
        internalMovementLine.setUOM(custsupMovementline.getUom());
        internalMovementLine
            .setMovementQuantity(new BigDecimal(custsupMovementline.getMovementqty()));
        internalMovementLine.setCustgsTd(technicaldata);
        internalMovementLine.setAttributeSetValue(technicaldata.getSerialNumber());
        internalMovementLine.setOrderUOM(custsupMovementline.getProductUom());
        if (custsupMovementline.getQuantityorder() != null) {
          internalMovementLine
              .setOrderQuantity(new BigDecimal(custsupMovementline.getQuantityorder()));
        }
        OBDal.getInstance().save(internalMovementLine);
      }
      // Remove the original shipment line

      if (!strMovementlineId.equals(custsupMovementline.getMovementlineOrig().getId())) {
        strMovementlineId = custsupMovementline.getMovementlineOrig().getId();
        InternalMovementLine movementLineOrig = OBDal.getInstance().get(InternalMovementLine.class,
            custsupMovementline.getMovementlineOrig().getId());
        if (movementLineOrig != null) {
          OBDal.getInstance().remove(movementLineOrig);
        }
      }

      // Delete record from temporary table
      OBDal.getInstance().remove(custsupMovementline);

    }

  }

  // Goods Shipment
  private void addSerialNumbersGS(JSONObject jsonRequest) throws JSONException {
    JSONArray selectedLines = jsonRequest.getJSONArray("_selection");
    JSONObject msg = new JSONObject();
    String strInoutlineId = "";

    // if no lines selected don't do anything.
    if (selectedLines.length() == 0) {
      return;
    }

    for (int i = 0; i < selectedLines.length(); i++) {

      JSONObject selectedId = selectedLines.getJSONObject(i);
      String strId = selectedId.getString("_identifier");

      // Get Technical Data if it is Goods Receipt
      String strInTechnicalDataId = "";
      strInTechnicalDataId = selectedId.getString("technicaldata");
      td_technicaldata technicaldata = null;

      if (!strInTechnicalDataId.equals("")) {
        technicaldata = OBDal.getInstance().get(td_technicaldata.class, strInTechnicalDataId);
      }

      AttributeSetInstance attributeSetInstance = null;

      // Get the Line of the Temporary Table
      CustsupInoutline custsupInoutline = OBDal.getInstance().get(CustsupInoutline.class, strId);
      if (technicaldata != null) {
        // Insert new lines of shipment
        ShipmentInOutLine shipmentInOutLine = OBProvider.getInstance().get(ShipmentInOutLine.class);
        shipmentInOutLine.setClient(custsupInoutline.getClient());
        shipmentInOutLine.setOrganization(custsupInoutline.getOrganization());
        shipmentInOutLine.setActive(true);
        shipmentInOutLine.setLineNo(custsupInoutline.getLineNew());
        shipmentInOutLine.setDescription(custsupInoutline.getDescription());
        shipmentInOutLine.setShipmentReceipt(custsupInoutline.getGoodsShipment());
        shipmentInOutLine.setSalesOrderLine(custsupInoutline.getSalesOrderLine());
        shipmentInOutLine.setStorageBin(custsupInoutline.getStorageBin());
        shipmentInOutLine.setProduct(custsupInoutline.getProduct());
        shipmentInOutLine.setUOM(custsupInoutline.getUOM());
        shipmentInOutLine
            .setMovementQuantity(new BigDecimal(custsupInoutline.getMovementQuantity()));
        shipmentInOutLine.setReinvoice(custsupInoutline.isReinvoice());
        shipmentInOutLine.setCustgsTd(technicaldata);
        shipmentInOutLine.setAttributeSetValue(technicaldata.getSerialNumber());
        shipmentInOutLine.setDescriptionOnly(custsupInoutline.isDescriptionOnly());
        shipmentInOutLine.setOrderUOM(custsupInoutline.getOrderUOM());
        shipmentInOutLine.setConditionGoods(custsupInoutline.getConditionOfTheGoods());
        OBDal.getInstance().save(shipmentInOutLine);
      }
      // Remove the original shipment line

      if (!strInoutlineId.equals(custsupInoutline.getInoutlineOrig().getId())) {
        strInoutlineId = custsupInoutline.getInoutlineOrig().getId();
        ShipmentInOutLine shipmentInOutLineOrig = OBDal.getInstance().get(ShipmentInOutLine.class,
            custsupInoutline.getInoutlineOrig().getId());
        if (shipmentInOutLineOrig != null) {
          OBDal.getInstance().remove(shipmentInOutLineOrig);
        }
      }

      // Delete record from temporary table
      OBDal.getInstance().remove(custsupInoutline);

    }
  }

  // Add Serial Number in Goods Shipment
  private void addSerialNumbersGR(JSONObject jsonRequest) throws JSONException {
    JSONArray selectedLines = jsonRequest.getJSONArray("_selection");
    JSONObject msg = new JSONObject();
    String strInoutlineId = "";

    // if no lines selected don't do anything.
    if (selectedLines.length() == 0) {
      return;
    }

    for (int i = 0; i < selectedLines.length(); i++) {

      JSONObject selectedId = selectedLines.getJSONObject(i);
      String strId = selectedId.getString("_identifier");

      // Get Serial Number
      String strInSerialNo = "";
      strInSerialNo = selectedId.getString("serialNo");
      // Description = #SerialNo
      String strDescription = "#" + strInSerialNo;

      AttributeSetInstance attributeSetInstance = null;

      // Error management => Javascript??
      if (strInSerialNo.equals("null")) {
        msg.put("severity", "error");
        msg.put("text", "Serial Number cannot be empty");
        jsonRequest.put("message", msg);
      }

      // Get the Line of the Temporary Table
      CustsupInoutline custsupInoutline = OBDal.getInstance().get(CustsupInoutline.class, strId);

      // Get Attribute Set for Serial Number
      OBCriteria<AttributeSet> criteriaAttributeSet = OBDal.getInstance()
          .createCriteria(AttributeSet.class);
      criteriaAttributeSet.add(Restrictions.eq("serialNo", true));
      criteriaAttributeSet.add(Restrictions.eq("client", custsupInoutline.getClient()));
      List<AttributeSet> attributeSetList = criteriaAttributeSet.list();
      AttributeSet attributeSet = null;
      if (attributeSetList.size() > 0) {
        attributeSet = attributeSetList.get(0);
      }

      if (attributeSet != null) {
        // Insert Attribute Set Instance
        attributeSetInstance = OBProvider.getInstance().get(AttributeSetInstance.class);
        attributeSetInstance.setOrganization(custsupInoutline.getOrganization());
        attributeSetInstance.setAttributeSet(attributeSet);
        attributeSetInstance.setSerialNo(strInSerialNo);
        attributeSetInstance.setDescription(strDescription);
        attributeSetInstance.setLocked(false);
        OBDal.getInstance().save(attributeSetInstance);
      }

      // Insert new lines of shipment
      ShipmentInOutLine shipmentInOutLine = OBProvider.getInstance().get(ShipmentInOutLine.class);
      shipmentInOutLine.setClient(custsupInoutline.getClient());
      shipmentInOutLine.setOrganization(custsupInoutline.getOrganization());
      shipmentInOutLine.setActive(true);
      shipmentInOutLine.setLineNo(custsupInoutline.getLineNew());
      shipmentInOutLine.setDescription(custsupInoutline.getDescription());
      shipmentInOutLine.setShipmentReceipt(custsupInoutline.getGoodsShipment());
      shipmentInOutLine.setSalesOrderLine(custsupInoutline.getSalesOrderLine());
      shipmentInOutLine.setStorageBin(custsupInoutline.getStorageBin());
      shipmentInOutLine.setProduct(custsupInoutline.getProduct());
      shipmentInOutLine.setUOM(custsupInoutline.getUOM());
      shipmentInOutLine.setMovementQuantity(new BigDecimal(custsupInoutline.getMovementQuantity()));
      shipmentInOutLine.setReinvoice(custsupInoutline.isReinvoice());
      shipmentInOutLine.setAttributeSetValue(attributeSetInstance);
      shipmentInOutLine.setDescriptionOnly(custsupInoutline.isDescriptionOnly());
      shipmentInOutLine.setOrderUOM(custsupInoutline.getOrderUOM());
      shipmentInOutLine.setConditionGoods(custsupInoutline.getConditionOfTheGoods());
      OBDal.getInstance().save(shipmentInOutLine);

      // Remove the original shipment line
      // Delete record from temporary table
      OBDal.getInstance().remove(custsupInoutline);

      if (!strInoutlineId.equals(custsupInoutline.getInoutlineOrig().getId())) {
        strInoutlineId = custsupInoutline.getInoutlineOrig().getId();
        ShipmentInOutLine shipmentInOutLineOrig = OBDal.getInstance().get(ShipmentInOutLine.class,
            custsupInoutline.getInoutlineOrig().getId());
        OrderLine lines = shipmentInOutLine.getSalesOrderLine();

        // RC: Fix to the original process
        lines.setGoodsShipmentLine(null);
        OBDal.getInstance().save(lines);
        OBDal.getInstance().flush();

        if (shipmentInOutLineOrig != null) {
          OBDal.getInstance().remove(shipmentInOutLineOrig);
        }
      }
    }

  }
}
