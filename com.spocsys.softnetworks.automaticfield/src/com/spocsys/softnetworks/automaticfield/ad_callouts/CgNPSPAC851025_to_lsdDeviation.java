package com.spocsys.softnetworks.automaticfield.ad_callouts;

import java.math.BigDecimal;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.softnetworks.desarrollos.td_datasheeta;
import com.spocsys.softnetworks.desarrollos.td_programming;
import com.spocsys.softnetworks.desarrollos.td_qacheck;
import com.spocsys.softnetworks.desarrollos.td_tuningdata;
import com.spocsys.softnetworks.desarrollos.td_technicaldata;
import com.spocsys.softnetworks.desarrollosdos.rr_workorder;
import com.spocsys.softnetworks.desarrollosdos.rr_workorderlines;

public class CgNPSPAC851025_to_lsdDeviation extends SimpleCallout{
	private static final long serialVersionUID = 1L;
	  @Override
	  protected void execute(CalloutInfo info) throws ServletException {
		  // parse input parameters here; the names derive from the column
		  // names of the table prepended by inp and stripped of all
		  // underscore characters; letters following the underscore character
		  // are capitalized; this way a database column named
		  // M_PRODUCT_CATEGORY_ID that is shown on a tab will become
		  // inpmProductCategoryId html field
		  String strid= info.getStringParameter("inptdDatasheetaId", null);
		  System.out.println("ID tab:"+strid);
	      String strCgnpspac815025 = info.getStringParameter("inpcgnpspac815025", null);
	      System.out.println("strCgnpspac815025:" + strCgnpspac815025);
	      String strAnalogtones851025 = info.getStringParameter("inpanalogtones851025", null);
	      System.out.println("strAnalogtones851025:"+strAnalogtones851025);   
	      String strTechnicaldataId = info.getStringParameter("inptdTechnicaldataId", null);
	      System.out.println("strTechnicaldataId:"+strTechnicaldataId); 
	      // inject the result into the response
	      info.addResult("inpcgnpspac815025", getConstructedKey(info.vars, strCgnpspac815025,strTechnicaldataId));
	  }
	  
	  protected String getConstructedKey(VariablesSecureApp vars,String strid,String strTechnicaldataId) {
		  String generatedSearchKey=strid+"_act";
		  System.out.println("Ciclo:"+generatedSearchKey);
		   final OBCriteria<td_programming> programming = OBDal.getInstance().createCriteria(td_programming.class);
		   final td_technicaldata technicaldata = OBDal.getInstance().get(td_technicaldata.class, strTechnicaldataId);		   
		   programming.add(Restrictions.eq(td_programming.PROPERTY_TECHNICALDATA,technicaldata));
		   BigDecimal tmp = new BigDecimal(strid);
		   System.out.println("Cambio num:" + tmp+ " -- " + programming.list().get(0).getProgrammerName());
		   programming.list().get(0).setLsddeviation(tmp);
		   OBDal.getInstance().save(tmp);
		   System.out.println("Programming:"+programming.list().get(0).getLsddeviation());
		  return generatedSearchKey;
		  
	  }

}
