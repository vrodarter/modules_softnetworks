package com.spocsys.softnetworks.automaticfield.ad_callouts;

import java.math.BigDecimal;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.softnetworks.desarrollos.td_datasheeta;
import com.spocsys.softnetworks.desarrollos.td_programming;
import com.spocsys.softnetworks.desarrollos.td_qacheck;
import com.spocsys.softnetworks.desarrollos.td_tuningdata;
import com.spocsys.softnetworks.desarrollos.td_technicaldata;
import com.spocsys.softnetworks.desarrollosdos.rr_workorder;
import com.spocsys.softnetworks.desarrollosdos.rr_workorderlines;

public class Programing_LsdDeviation_to_CgNPSPAC851 extends SimpleCallout{
	private static final long serialVersionUID = 1L;
	  @Override
	  protected void execute(CalloutInfo info) throws ServletException {
	      String strid = info.getStringParameter("inplsddeviation", null);  
	      String strTechnicaldataId = info.getStringParameter("inptdTechnicaldataId", null);
	      info.addResult("inplsddeviation", getConstructedKey(info.vars, strid,strTechnicaldataId));
	  }
	  
	  protected String getConstructedKey(VariablesSecureApp vars,String strid,String strTechnicaldataId) {
		   final OBCriteria<td_tuningdata> tuningdata = OBDal.getInstance().createCriteria(td_tuningdata.class);
		   final td_technicaldata technicaldata = OBDal.getInstance().get(td_technicaldata.class, strTechnicaldataId);		   
		   tuningdata.add(Restrictions.eq(td_tuningdata.PROPERTY_TECHNICALDATA,technicaldata));
		   BigDecimal tmp = new BigDecimal(strid);
		   System.out.println("Cambio num:" + tmp+ " -- " + tuningdata.list().get(0).getIdentifier());
		   tuningdata.list().get(0).setCGNPSPAC851(tmp);
		   OBDal.getInstance().save(tmp);
		   System.out.println("Programming:"+tuningdata.list().get(0).getCGNPSPAC851());
		  return strid;
		  
	  }

}
