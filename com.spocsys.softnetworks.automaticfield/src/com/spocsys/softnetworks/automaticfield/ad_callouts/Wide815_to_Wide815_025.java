package com.spocsys.softnetworks.automaticfield.ad_callouts;

import java.math.BigDecimal;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.softnetworks.desarrollos.td_datasheeta;
import com.spocsys.softnetworks.desarrollos.td_programming;
import com.spocsys.softnetworks.desarrollos.td_qacheck;
import com.spocsys.softnetworks.desarrollos.td_tuningdata;
import com.spocsys.softnetworks.desarrollos.td_technicaldata;
import com.spocsys.softnetworks.desarrollosdos.rr_workorder;
import com.spocsys.softnetworks.desarrollosdos.rr_workorderlines;

public class Wide815_to_Wide815_025 extends SimpleCallout{
	private static final long serialVersionUID = 1L;
	  @Override
	  protected void execute(CalloutInfo info) throws ServletException {
		  String strid= info.getStringParameter("inpcgwide815", null);
		  System.out.println("ID tab:"+strid);
	      String strTechnicaldataId = info.getStringParameter("inptdTechnicaldataId", null);
	      System.out.println("strTechnicaldataId:"+strTechnicaldataId); 
	      info.addResult("inpcgwide815", getConstructedKey(info.vars, strid,strTechnicaldataId));
	  }
	  
	  protected String getConstructedKey(VariablesSecureApp vars,String strid,String strTechnicaldataId) {
		  String generatedSearchKey=strid;
		  System.out.println("Ciclo:"+generatedSearchKey);
		   final OBCriteria<td_datasheeta> datasheeta = OBDal.getInstance().createCriteria(td_datasheeta.class);
		   final td_technicaldata technicaldata = OBDal.getInstance().get(td_technicaldata.class, strTechnicaldataId);		   
		   datasheeta.add(Restrictions.eq(td_programming.PROPERTY_TECHNICALDATA,technicaldata));
		   if(!datasheeta.list().isEmpty()){
		   BigDecimal tmp = new BigDecimal(strid);
		   datasheeta.list().get(0).setCGWIDE815025(tmp);
		   OBDal.getInstance().save(datasheeta.list().get(0));
		   System.out.println("inpcgwide815:"+datasheeta.list().get(0).getCGWIDE815025());
		   }
		  return generatedSearchKey;
		  
	  }

}
