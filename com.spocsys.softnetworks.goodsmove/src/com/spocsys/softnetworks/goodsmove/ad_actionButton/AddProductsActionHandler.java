package com.spocsys.softnetworks.goodsmove.ad_actionButton;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.financial.FinancialUtils;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.model.common.enterprise.Locator;


//import com.qualiantech.consignment.sales.ConsOrderLine;

/**
 * 
 * @author Geovanis Pick Edit de Facturas de Compra que trae lines de la remisión
 * 
 */
public class AddProductsActionHandler extends BaseProcessActionHandler {

	private Logger log4j = Logger
			.getLogger(AddProductsActionHandler.class);

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters,
			String content) {

		OBContext.setAdminMode();
		JSONObject jsonRequest = null;
		try {
			jsonRequest = new JSONObject(content);
			//System.out.println("Json ver que tiene............................");
			//System.out.println(jsonRequest.toString());
		    
			@SuppressWarnings("unused")
			JSONArray selection = new JSONArray(
					jsonRequest
							.getString(ApplicationConstants.SELECTION_PROPERTY));

			final String strMovementId = jsonRequest.getString("M_Movement_ID");
			InternalMovement movement = OBDal.getInstance().get(InternalMovement.class, strMovementId);
			if (movement != null) {
				//List<String> idList = getIDListFromOBObject(invoice
				//		.getInvoiceLineList());
				String message = createMovementLines(jsonRequest);
				//String message = "OK";
				JSONObject resultMessage = new JSONObject();
				resultMessage.put("severity", "success");
				resultMessage.put("title", "Successfully");
				resultMessage.put("text", message);
				jsonRequest.put("message", resultMessage);
			}

		} catch (Exception e) {
			OBDal.getInstance().rollbackAndClose();
			VariablesSecureApp vars = RequestContext.get()
					.getVariablesSecureApp();
			log4j.error(e.getMessage(), e);

			try {
				jsonRequest = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(
						new DalConnectionProvider(), vars, vars.getLanguage(),
						ex.getMessage()).getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", message);
				jsonRequest.put("message", errorMessage);

			} catch (Exception e2) {
				log4j.error(e.getMessage(), e2);
				// do nothing, give up
			}
		} finally {
			OBContext.restorePreviousMode();
		}
		return jsonRequest;
	}

	private String createMovementLines(JSONObject jsonRequest)
			throws JSONException, IOException {
		String message = "";
	    JSONArray selectedLines = jsonRequest.getJSONArray("_selection");
	    final String strMovementId = jsonRequest.getString("M_Movement_ID");
	    InternalMovement movement = OBDal.getInstance().get(InternalMovement.class, strMovementId);
	    //boolean isSOTrx = invoice.isSalesTransaction();
	    // if no lines selected don't do anything.
	    if (selectedLines.length() == 0) {
	          //removeNonSelectedLines(idList, invoice);
	         return "Error. No lines selected";
	    }

	    try {
	    	AddProductsActionHandlerData.deleteLines(new DalConnectionProvider(),movement.getId());
	    } catch (Exception e5){
	    	  new OBException("Could not delete lines"+e5.toString());
	    }
	    int line = 0;
	    for (long i = 0; i < selectedLines.length(); i++) {

	      line = line+10;
	      JSONObject selectedLine = selectedLines.getJSONObject((int) i);
	      log4j.debug(selectedLine);
	      
	      String strLocator = null;
	      BigDecimal qtyonhand = null;
	      String strProduct = null;
	      String strAttributesetinstance = null;
	      String strUom = null;
	      String strLocatorTo = null;
	      String strId = null;
	      
	      strId = selectedLine.getString("id");
	      StorageDetail storageDetail = OBDal.getInstance().get(StorageDetail.class, strId);
	      qtyonhand = new BigDecimal(selectedLine.getString("qtyonhand"));
	      strLocatorTo = selectedLine.getString("locatorto");
	      if(qtyonhand.compareTo(storageDetail.getQuantityOnHand()) > 0){
	    	  throw new OBException("Insufficient stock in product: "+storageDetail.getProduct().getName());
	      }
 
	      InternalMovementLine internalMovementLine = null;
	      internalMovementLine = OBProvider.getInstance().get(InternalMovementLine.class);
	      
	      internalMovementLine.setMovement(movement);
	      internalMovementLine.setLineNo(Long.valueOf(line));
	      internalMovementLine.setOrganization(movement.getOrganization());
	      internalMovementLine.setProduct(storageDetail.getProduct());
	      internalMovementLine.setAttributeSetValue(storageDetail.getAttributeSetValue());
	      internalMovementLine.setUOM(storageDetail.getUOM());
	      internalMovementLine.setStorageBin(storageDetail.getStorageBin());
	      internalMovementLine.setMovementQuantity(qtyonhand);
	      if(strLocatorTo == null || strLocatorTo.equals("") || strLocatorTo.equals("null")){

	    	  internalMovementLine.setNewStorageBin(movement.getSsgmsMLocatorto());

	      }else{
	    	  Locator locatorTo = OBDal.getInstance().get(Locator.class, strLocatorTo);
	    	  internalMovementLine.setNewStorageBin(locatorTo);
	      }

	      OBDal.getInstance().save(internalMovementLine);
	      
	   }

	   OBDal.getInstance().flush();

	   message = "Added Lines ";
	   return message; 
	}

	
}
