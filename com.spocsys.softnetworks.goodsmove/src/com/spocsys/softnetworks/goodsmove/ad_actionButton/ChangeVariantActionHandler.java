package com.spocsys.softnetworks.goodsmove.ad_actionButton;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.financial.FinancialUtils;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.materialmgmt.transaction.InventoryCount;
import org.openbravo.model.materialmgmt.transaction.InventoryCountLine;
import org.openbravo.model.common.plm.CharacteristicValue;


//import com.qualiantech.consignment.sales.ConsOrderLine;

/**
 * 
 * @author Geovanis Pick Edit de Facturas de Compra que trae lines de la remisión
 * 
 */
public class ChangeVariantActionHandler extends BaseProcessActionHandler {

	private Logger log4j = Logger
			.getLogger(AddProductsActionHandler.class);

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters,
			String content) {

		OBContext.setAdminMode();
		JSONObject jsonRequest = null;
		try {
			jsonRequest = new JSONObject(content);
			//System.out.println("Json ver que tiene............................");
			//System.out.println(jsonRequest.toString());
		    
			@SuppressWarnings("unused")
			JSONArray selection = new JSONArray(
					jsonRequest
							.getString(ApplicationConstants.SELECTION_PROPERTY));

			final String strInventoryId = jsonRequest.getString("M_Inventory_ID");
			InventoryCount inventoryCount = OBDal.getInstance().get(InventoryCount.class, strInventoryId);
			if (inventoryCount != null) {
				//List<String> idList = getIDListFromOBObject(invoice
				//		.getInvoiceLineList());
				String message = createInventoryLines(jsonRequest);
				//String message = "OK";
				JSONObject resultMessage = new JSONObject();
				resultMessage.put("severity", "success");
				resultMessage.put("title", "Successfully");
				resultMessage.put("text", message);
				jsonRequest.put("message", resultMessage);
			}

		} catch (Exception e) {
			OBDal.getInstance().rollbackAndClose();
			VariablesSecureApp vars = RequestContext.get()
					.getVariablesSecureApp();
			log4j.error(e.getMessage(), e);

			try {
				jsonRequest = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(
						new DalConnectionProvider(), vars, vars.getLanguage(),
						ex.getMessage()).getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", message);
				jsonRequest.put("message", errorMessage);

			} catch (Exception e2) {
				log4j.error(e.getMessage(), e2);
				// do nothing, give up
			}
		} finally {
			OBContext.restorePreviousMode();
		}
		return jsonRequest;
	}

	private String createInventoryLines(JSONObject jsonRequest)
			throws JSONException, IOException {
		String message = "";
	    JSONArray selectedLines = jsonRequest.getJSONArray("_selection");
	    final String strInventoryId = jsonRequest.getString("M_Inventory_ID");
	    InventoryCount inventoryCount = OBDal.getInstance().get(InventoryCount.class, strInventoryId);
	    //boolean isSOTrx = invoice.isSalesTransaction();
	    // if no lines selected don't do anything.
	    if (selectedLines.length() == 0) {
	          //removeNonSelectedLines(idList, invoice);
	         return "Error. No lines selected";
	    }
	    /*
	    try {
	    	ChangeVariantActionHandlerData.deleteLines(new DalConnectionProvider(),inventoryCount.getId());
	    } catch (Exception e5){
	    	  new OBException("Could not delete lines"+e5.toString());
	    }*/
	    int line = 0;
	    for (long i = 0; i < selectedLines.length(); i++) {

	      line = line+10;
	      JSONObject selectedLine = selectedLines.getJSONObject((int) i);
	      log4j.debug(selectedLine);
	      
	      BigDecimal qtyonhand = null;
	      String strProduct = null;
	      String strAttributesetinstance = null;
	      String strUom = null;
	      String strLocatorTo = null;
	      String strLocator = null;
	      String strId = null;
	      Locator locatorTo = null;
	      
	      strId = selectedLine.getString("id");
	      StorageDetail storageDetail = OBDal.getInstance().get(StorageDetail.class, strId);
	      qtyonhand = new BigDecimal(selectedLine.getString("qtyonhand"));
	      strLocatorTo = selectedLine.getString("locatorto");
	      strLocator = selectedLine.getString("locator");
	      if(strLocatorTo == null || strLocatorTo.equals("") || strLocatorTo.equals("null")){
	      	locatorTo = OBDal.getInstance().get(Locator.class, strLocator);
	      }else{
	    	locatorTo = OBDal.getInstance().get(Locator.class, strLocatorTo);
	      }
	      if (storageDetail.getProduct().getGenericProduct()==null){
	      	throw new OBException("Product without conditions: "+storageDetail.getProduct());
	      }

	      Product productGeneric = OBDal.getInstance().get(Product.class, storageDetail.getProduct().getGenericProduct().getId());
	      CharacteristicValue characteristicValue = OBDal.getInstance()
	      		.get(CharacteristicValue.class, selectedLine.getString("chValue"));
	      
	      if (characteristicValue == null){
	      	throw new OBException("Need new conditions for: "+storageDetail.getProduct().getName());
	      }
	      
	      int isValidProduct = 0;
	      try{
		     isValidProduct = Integer
		        .parseInt(ChangeVariantActionHandlerData
		        	.isValidProduct(new DalConnectionProvider(),
		        		productGeneric.getSearchKey(),
		        		characteristicValue.getCode()));
	      }catch(Exception e){
	      	throw new OBException("Problems with conditions: "+storageDetail.getProduct().getName());
	      }
	      if (isValidProduct == 0){
	      	throw new OBException("Doesn't exist conditions for: "+storageDetail.getProduct().getName());
	      }

	      String strNewProductId = "";
	      try{
		     strNewProductId = ChangeVariantActionHandlerData
		        	.getNewProduct(new DalConnectionProvider(),
		        		productGeneric.getSearchKey(),
		        		characteristicValue.getCode());
	      }catch(Exception e){
	      	throw new OBException("Problems with conditions: "+storageDetail.getProduct().getName());
	      }
	      Product newProduct = OBDal.getInstance().get(Product.class, strNewProductId);

	      String stockNewProdut = "0";
	      try{
		     stockNewProdut = ChangeVariantActionHandlerData
		        	.getStockNewProdut(new DalConnectionProvider(),
		        		newProduct.getId(),
		        		locatorTo.getId(),
		        		storageDetail.getAttributeSetValue()==null?null:storageDetail.getAttributeSetValue().getId(),
		        		storageDetail.getUOM().getId());
	      }catch(Exception e){
	      	throw new OBException("Problems with Storage Detail in: "+storageDetail.getProduct().getName());
	      }
	      System.out.println("stockNewProdut");
	      System.out.println(stockNewProdut);
	      System.out.println("locatorTo.getId()");
	      System.out.println(locatorTo.getId());
	      System.out.println("storageDetail.getAttributeSetValue()==null?null:storageDetail.getAttributeSetValue().getId()");
	      System.out.println(storageDetail.getAttributeSetValue()==null?null:storageDetail.getAttributeSetValue().getId());
	      System.out.println("storageDetail.getUOM().getId()");
	      System.out.println(storageDetail.getUOM().getId());
	      
	      if(qtyonhand.compareTo(storageDetail.getQuantityOnHand()) > 0){
	    	  throw new OBException("Insufficient stock in product: "+storageDetail.getProduct().getName());
	      }
 
	      InventoryCountLine inventoryCountLine = null;
	      inventoryCountLine = OBProvider.getInstance().get(InventoryCountLine.class);
	      
	      inventoryCountLine.setPhysInventory(inventoryCount);
	      inventoryCountLine.setLineNo(Long.valueOf(line));
	      inventoryCountLine.setOrganization(inventoryCount.getOrganization());
	      inventoryCountLine.setProduct(storageDetail.getProduct());
	      inventoryCountLine.setAttributeSetValue(storageDetail.getAttributeSetValue());
	      inventoryCountLine.setUOM(storageDetail.getUOM());
	      inventoryCountLine.setStorageBin(storageDetail.getStorageBin());
	      inventoryCountLine.setQuantityCount(storageDetail.getQuantityOnHand().subtract(qtyonhand));
	      inventoryCountLine.setBookQuantity(storageDetail.getQuantityOnHand());
	      
	      InventoryCountLine inventoryCountLineTo = null;
	      inventoryCountLineTo = OBProvider.getInstance().get(InventoryCountLine.class);
	      
	      inventoryCountLineTo.setPhysInventory(inventoryCount);
	      inventoryCountLineTo.setLineNo(Long.valueOf(line+5));
	      inventoryCountLineTo.setOrganization(inventoryCount.getOrganization());
	      inventoryCountLineTo.setProduct(newProduct);
	      inventoryCountLineTo.setAttributeSetValue(storageDetail.getAttributeSetValue());
	      inventoryCountLineTo.setUOM(storageDetail.getUOM());
	      inventoryCountLineTo.setStorageBin(locatorTo);
	      inventoryCountLineTo.setQuantityCount(qtyonhand.add(new BigDecimal(stockNewProdut)));
	      inventoryCountLineTo.setBookQuantity(new BigDecimal(stockNewProdut));
	      inventoryCountLineTo.setSsgmsQtylabels(Long.parseLong(selectedLine.getString("qtyonhand")));

	      OBDal.getInstance().save(inventoryCountLine);
	      OBDal.getInstance().save(inventoryCountLineTo);
	      
	   }

	   OBDal.getInstance().flush();

	   message = "Added Lines ";
	   return message; 
	}

	
}
