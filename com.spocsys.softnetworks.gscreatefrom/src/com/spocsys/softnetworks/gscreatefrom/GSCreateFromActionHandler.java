/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.spocsys.softnetworks.gscreatefrom;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

import com.spocsys.softnetworks.desarrollos.td_technicaldata;
import com.spocsys.softnetworks.gscreatefrom.data.CustgsCreatefromV;

/**
 * @author mperez
 * 
 */
public class GSCreateFromActionHandler extends BaseProcessActionHandler {

  private static final Logger log = Logger.getLogger(GSCreateFromActionHandler.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    JSONObject jsonRequest = null;
    OBContext.setAdminMode();
    try {
      jsonRequest = new JSONObject(content);
      log.debug(jsonRequest);
      // Get window from it has been executed
      // 170 => Goods Movement
      // 169 => Goods Shipment
      String strWindowId = jsonRequest.getString("inpwindowId");
      if (strWindowId.equals("169")) {
        addGoodShipmentLines(jsonRequest);
      } else if (strWindowId.equals("170")) {
        addGoodMovementLines(jsonRequest);
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return jsonRequest;

  }

  private void addGoodMovementLines(JSONObject jsonRequest) throws JSONException {

    JSONArray selectedLines = jsonRequest.getJSONArray("_selection");
    JSONObject msg = new JSONObject();
    Long lineNo = new Long("0");

    // Get Header Goods Movement
    String strMovementId = jsonRequest.getString("inpmMovementId");
    InternalMovement internalMovement = OBDal.getInstance().get(InternalMovement.class,
        strMovementId);
    

    // if no lines selected don't do anything.
    if (selectedLines.length() == 0) {
      return;
    }

    for (int i = 0; i < selectedLines.length(); i++) {

      JSONObject selectedId = selectedLines.getJSONObject(i);
      String strId = selectedId.getString("_identifier");

      // Get the Line of the View
      CustgsCreatefromV custgsCreatefromV = OBDal.getInstance().get(CustgsCreatefromV.class, strId);

      // Get Sales Order Line
      OrderLine salesOrderLine = OBDal.getInstance().get(OrderLine.class,
          custgsCreatefromV.getSalesOrderLine().getId());

      lineNo = lineNo + 10;

      // Create Goods Movement Line
      InternalMovementLine internalMovementLine = OBProvider.getInstance().get(
          InternalMovementLine.class);
      internalMovementLine.setClient(internalMovement.getClient());
      internalMovementLine.setOrganization(internalMovement.getOrganization());
      internalMovementLine.setActive(true);
      internalMovementLine.setLineNo(lineNo);
      internalMovementLine.setMovement(internalMovement);
      internalMovementLine.setDescription(salesOrderLine.getDescription());
      internalMovementLine.setMovementQuantity(new BigDecimal(custgsCreatefromV.getQuantity()));
      internalMovementLine.setProduct(custgsCreatefromV.getProduct());
      internalMovementLine.setUOM(salesOrderLine.getUOM());
      internalMovementLine.setCustgsOrderline(salesOrderLine);

      // If there is Goods Receipt => M_AttributeSetInstance and Td_technicaldata
      if (!custgsCreatefromV.getGoodsReceiptDocno().equals("")) {
        ShipmentInOutLine goodsReceiptLine = OBDal.getInstance().get(ShipmentInOutLine.class,
            custgsCreatefromV.getId());
        internalMovementLine.setAttributeSetValue(custgsCreatefromV.getAttributeSetValue());

        // Search Technical Data
        OBCriteria<td_technicaldata> criteriaTechnicalData = OBDal.getInstance().createCriteria(
            td_technicaldata.class);
        criteriaTechnicalData.add(Restrictions.eq("product", custgsCreatefromV.getProduct()));
        criteriaTechnicalData.add(Restrictions.eq("serialNumber",
            custgsCreatefromV.getAttributeSetValue()));
        List<td_technicaldata> technicalDataList = criteriaTechnicalData.list();
        if (technicalDataList.size() > 0) {
          internalMovementLine.setCustgsTd(technicalDataList.get(0));
        }
      }

      // Set Storage Bin From and To
      String strLocatorFromId = selectedId.getString("locatorFrom");

      if (strLocatorFromId.equals("")) {
        OBCriteria<Locator> criteriaLocator = OBDal.getInstance().createCriteria(Locator.class);
        criteriaLocator.add(Restrictions.eq("client", custgsCreatefromV.getClient()));
        List<Locator> locatorList = criteriaLocator.list();
        if (locatorList.size() > 0) {
          internalMovementLine.setStorageBin(locatorList.get(0));
        }
      } else {
        Locator locatorFrom = OBDal.getInstance().get(Locator.class, strLocatorFromId);
        internalMovementLine.setStorageBin(locatorFrom);
      }

      String strLocatorToId = selectedId.getString("locatorTo");

      if (strLocatorToId.equals("")) {
        OBCriteria<Locator> criteriaLocator = OBDal.getInstance().createCriteria(Locator.class);
        criteriaLocator.add(Restrictions.eq("client", custgsCreatefromV.getClient()));
        List<Locator> locatorList = criteriaLocator.list();
        if (locatorList.size() > 0) {
        	//Modificado por Jonatan Mora
        	if(internalMovement.getCustgsMLocator() == null)
        		internalMovementLine.setNewStorageBin(locatorList.get(0));
        	else
        		internalMovementLine.setNewStorageBin(internalMovement.getCustgsMLocator());
          //internalMovementLine.setNewStorageBin(locatorList.get(0));
          //Fin de modificacion
        }
      } else {
        Locator locatorFrom = OBDal.getInstance().get(Locator.class, strLocatorToId);
        internalMovementLine.setNewStorageBin(locatorFrom);
      }

      OBDal.getInstance().save(internalMovementLine);
    }

  }

  private void addGoodShipmentLines(JSONObject jsonRequest) throws JSONException {

    JSONArray selectedLines = jsonRequest.getJSONArray("_selection");
    JSONObject msg = new JSONObject();
    Long lineNo = new Long("0");
    Boolean isSOUpdated = false;

    // Get Header Goods Shipment
    String strInoutlineId = jsonRequest.getString("inpmInoutId");
    ShipmentInOut shipmentInOut = OBDal.getInstance().get(ShipmentInOut.class, strInoutlineId);

    // if no lines selected don't do anything.
    if (selectedLines.length() == 0) {
      return;
    }

    for (int i = 0; i < selectedLines.length(); i++) {

      JSONObject selectedId = selectedLines.getJSONObject(i);
      String strId = selectedId.getString("_identifier");

      // Get the Line of the View
      CustgsCreatefromV custgsCreatefromV = OBDal.getInstance().get(CustgsCreatefromV.class, strId);

      // Get Sales Order and Set value
      if (custgsCreatefromV.getSalesOrder() != null && isSOUpdated == false) {
        shipmentInOut.setSalesOrder(custgsCreatefromV.getSalesOrder());
        isSOUpdated = true;
      }

      // Get Sales Order Line
      OrderLine salesOrderLine = OBDal.getInstance().get(OrderLine.class,
          custgsCreatefromV.getSalesOrderLine().getId());

      lineNo = lineNo + 10;

      // Create Goods Shipmnet Line
      ShipmentInOutLine shipmentInOutLine = OBProvider.getInstance().get(ShipmentInOutLine.class);
      shipmentInOutLine.setShipmentReceipt(shipmentInOut);
      shipmentInOutLine.setClient(shipmentInOut.getClient());
      shipmentInOutLine.setOrganization(shipmentInOut.getOrganization());
      shipmentInOutLine.setActive(true);
      shipmentInOutLine.setLineNo(lineNo);
      shipmentInOutLine.setDescription(salesOrderLine.getDescription());
      shipmentInOutLine.setMovementQuantity(new BigDecimal(custgsCreatefromV.getQuantity()));
      shipmentInOutLine.setSalesOrderLine(salesOrderLine);
      shipmentInOutLine.setProduct(custgsCreatefromV.getProduct());
      shipmentInOutLine.setUOM(salesOrderLine.getUOM());

      // If there is Goods Receipt => M_AttributeSetInstance and Td_technicaldata
      if (!custgsCreatefromV.getGoodsReceiptDocno().equals("")) {
        ShipmentInOutLine goodsReceiptLine = OBDal.getInstance().get(ShipmentInOutLine.class,
            custgsCreatefromV.getId());
        shipmentInOutLine.setAttributeSetValue(custgsCreatefromV.getAttributeSetValue());

        // Search Technical Data
        OBCriteria<td_technicaldata> criteriaTechnicalData = OBDal.getInstance().createCriteria(
            td_technicaldata.class);
        criteriaTechnicalData.add(Restrictions.eq("product", custgsCreatefromV.getProduct()));
        criteriaTechnicalData.add(Restrictions.eq("serialNumber",
            custgsCreatefromV.getAttributeSetValue()));
        List<td_technicaldata> technicalDataList = criteriaTechnicalData.list();
        if (technicalDataList.size() > 0) {
          shipmentInOutLine.setCustgsTd(technicalDataList.get(0));
        }
      }
      // Get Storage Bin From
      String strLocatorFromId = selectedId.getString("locatorFrom");

      if (strLocatorFromId.equals("")) {
        OBCriteria<Locator> criteriaLocator = OBDal.getInstance().createCriteria(Locator.class);
        criteriaLocator.add(Restrictions.eq("client", custgsCreatefromV.getClient()));
        List<Locator> locatorList = criteriaLocator.list();
        if (locatorList.size() > 0) {
          shipmentInOutLine.setStorageBin(locatorList.get(0));
        }
      } else {
        Locator locatorFrom = OBDal.getInstance().get(Locator.class, strLocatorFromId);
        shipmentInOutLine.setStorageBin(locatorFrom);
      }

      OBDal.getInstance().save(shipmentInOutLine);
    }
  }
}
