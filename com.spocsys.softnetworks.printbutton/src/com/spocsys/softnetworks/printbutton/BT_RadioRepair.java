/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.spocsys.softnetworks.printbutton;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.utility.DateTimeData;
public class BT_RadioRepair extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strWindow = "1AD3FD01CD6941069031E2AE6720C0A4";
      String strTab = "A714437AD2E6422487E764C6FF410C50";
      String strKey = vars.getGlobalVariable("inprrRadiorepairRequestId", strWindow + "|RR_Radiorepair_Request_ID");
        //strRadioRepairId = vars.getSessionValue("BT_RadioRepair.inprrRadiorepair_Id");
      System.out.println("Key radio repair:"+ strKey);
      if (log4j.isDebugEnabled())
        log4j.debug("strRadioRepair: " + strKey);
      printPagePDF(response, vars, strKey);
   } else
      pageError(response);
  }

  private void printPagePDF(HttpServletResponse response, VariablesSecureApp vars,
      String strRadioRepair) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: pdf");

    if (log4j.isDebugEnabled())
      log4j.debug("printPagePDF strRadioRepairId = " + strRadioRepair);
    System.out.println("RadioRepair ID metodo windows: " + strRadioRepair);
    String strReportName = "@basedesign@/com/spocsys/printbutton/JR_RadioRepair.jrxml";
    String strOutput = "pdf";
    HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("RR_Radiorepair_Request_ID", strRadioRepair);
    response.setHeader("Content-disposition", "inline; filename=JR_RadioRepair.pdf");
    renderJR(vars, response, strReportName, strOutput, parameters, null, null);
  }

  public String getServletInfo() {
    return "Servlet that presents the BT_RadioRepair";
  } // End of getServletInfo() method
}
