package com.spocsys.softnetworks.serviceorder.ad_actionButton;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.pricing.pricelist.PriceList;

import com.spocsys.softnetworks.serviceorder.SSNSO_Partandlabor;
import com.spocsys.softnetworks.serviceorder.SSNSO_Ticket;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;
import com.sun.xml.messaging.saaj.packaging.mime.internet.ParseException;

public class ServiceOrderToQuote {

      private static final Logger log4j = Logger.getLogger(ServiceOrderToQuote.class);

      private ServiceOrder        service;
      private Order               order;
      private boolean             update;

      public ServiceOrderToQuote(String strOrderId, String serviceOrderId) {
            service = (serviceOrderId.equals("")) ? null : OBDal.getInstance().get(ServiceOrder.class, serviceOrderId);
            boolean orderIsNull = ((!strOrderId.equals("undefined")) && (!strOrderId.equals("null"))
                        && (!strOrderId.equals(""))) ? false : true;
            order = (orderIsNull) ? null : OBDal.getInstance().get(Order.class, strOrderId);
            update = (order == null) ? false : true;
      }

      /**
       * This allow you create Service Quote from Service Order
       * 
       * @return
       * @throws OBException
       * @throws JSONException
       */
      public boolean isOrderDraft() {
            if (update) {
                  String docstatus = order.getDocumentStatus();
                  return (docstatus.equals("DR")) ? true : false;
            }
            return true;
      }

      public JSONObject manageGeneration() throws OBException, JSONException {
            String documentNo = "";
            JSONObject myMessage = new JSONObject();
            try {
                  OBContext.setAdminMode();
                  Order neworder = genHeader();
                  List<OrderLine> olineList = genLines(neworder);
                  neworder.setOrderLineList(olineList);
                  if (!update) {
                        service.setSalesOrder(neworder);
                        OBDal.getInstance().save(service);
                  }
                  OBDal.getInstance().save(neworder);
                  OBDal.getInstance().flush();
                  OBDal.getInstance().commitAndClose();
                  documentNo = neworder.getDocumentNo();
            } catch (Exception e1) {
                  OBDal.getInstance().rollbackAndClose();
                  log4j.error(e1.getMessage());
                  throw new OBException(e1.getMessage(), e1);
            } finally {
                  OBContext.restorePreviousMode();
            }
            String add = (!update) ? " <br> Order Quote was created with Document No. " + documentNo
                        : "<br> Order Quote with Document No. " + documentNo + " was updated";
            String message = "<strong>Process completed successfully</strong> " + add;
            myMessage.put("severity", "success");
            myMessage.put("title", "success");
            myMessage.put("text", message);
            return myMessage;
      }

      private Warehouse getWarehouse() {
            OBCriteria<Warehouse> wr_crit = OBDal.getInstance().createCriteria(Warehouse.class);
            wr_crit.add(Restrictions.eq(Warehouse.PROPERTY_ORGANIZATION, service.getOrganization()));
            wr_crit.add(Restrictions.eq(Warehouse.PROPERTY_ACTIVE, true));
            List<Warehouse> wr_list = wr_crit.list();
            if (!wr_list.isEmpty()) {
                  Warehouse result = wr_list.get(0);
                  return result;
            }
            return null;
      }

      private Order genHeader() throws OBException, ParseException {
            Order newOrder = null;
            try {
                  if (update) {
                        newOrder = order;
                  } else {
                        newOrder = OBProvider.getInstance().get(Order.class);
                        // newOrder.setNewOBObject(true);
                  }
                  newOrder.setSalesTransaction(true);
                  newOrder.setOrganization(service.getOrganization());
                  newOrder.setSsnsoServiceorder(service);
                  newOrder.setDocumentStatus("DR");
                  newOrder.setPartnerAddress(service.getShiptoLoc());
                  newOrder.setBusinessPartner(service.getClientid());

                  /*
                   * OBCriteria<Table> tablelist = OBDal.getInstance().createCriteria(Table.class);
                   * tablelist.add(Restrictions.eq(Table.PROPERTY_DBTABLENAME, "C_Order"));
                   * tablelist.setMaxResults(1); Table tableOrder = (Table)
                   * tablelist.uniqueResult();
                   */

                  // Obtaining a document type for service quote
                  OBCriteria<DocumentType> dTlist = OBDal.getInstance().createCriteria(DocumentType.class);
                  // dTlist.add(Restrictions.eq(DocumentType.PROPERTY_TABLE, tableOrder));
                  dTlist.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, true));
                  dTlist.add(Restrictions.eq(DocumentType.PROPERTY_SOSUBTYPE, "SSNSO_OV"));
                  // dTlist.add(Restrictions.eq(DocumentType.PROPERTY_DEFAULT, true));
                  dTlist.setMaxResults(1);
                  DocumentType doctype = (DocumentType) dTlist.uniqueResult();
                  if (doctype == null) {
                        throw new OBException("Document type for Service Quote not founded.");
                  }

                  newOrder.setTransactionDocument(doctype);

                  newOrder.setDocumentType(doctype);
                  newOrder.setOrderDate(new Date());
                  newOrder.setAccountingDate(new Date());
                  Currency currency = service.getClientid().getCurrency();
                  if (currency == null) {
                        throw new OBException(
                                    "Currency of Client".concat(service.getClient().getName().concat(" haven't set")));
                  }
                  Warehouse wr = (service.getOrganization().getSsnsoSowarehouse() != null)
                              ? service.getOrganization().getSsnsoSowarehouse() : getWarehouse();
                  if (wr == null) {
                        throw new OBException("Organization selected "
                                    .concat(service.getOrganization().getName().concat(" haven't Warehouse")));
                  }
                  PaymentTerm paym_t = service.getClientid().getPaymentTerms();
                  if (paym_t == null) {
                        throw new OBException("Client selected ".concat(
                                    service.getClient().getName().concat(" haven't cofigurated Payment terms")));
                  }
                  PriceList pl = service.getClientid().getPriceList();
                  if (pl == null) {
                        throw new OBException("Client selected "
                                    .concat(service.getClient().getName().concat(" haven't cofigurated Price List")));
                  }

                  newOrder.setCurrency(currency);
                  newOrder.setWarehouse(wr);
                  newOrder.setPaymentTerms(paym_t);
                  newOrder.setPriceList(pl);
                  newOrder.setSsnsoDocaction("BK");
                  if (!update) {
                        newOrder.setDocumentNo(FIN_Utility.getDocumentNo(doctype, "C_Order", true));
                  }

            } catch (Exception e1) {
                  throw new OBException(e1.getMessage(), e1);
            }
            OBDal.getInstance().save(newOrder);
            return newOrder;
      }

      private List<OrderLine> genLines(Order neworder) throws OBException {

            List<OrderLine> olList = neworder.getOrderLineList();
            Long lineNo = 0L;

            // Obtaining LineNo
            if (!olList.isEmpty()) {
                  final OBCriteria<OrderLine> linenoCrit = OBDal.getInstance().createCriteria(OrderLine.class);
                  linenoCrit.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, neworder));
                  linenoCrit.setProjection(Projections.max(OrderLine.PROPERTY_LINENO));

                  Object o = linenoCrit.list().get(0);
                  if (o != null) {
                        lineNo = (Long) o;
                  }
            }
            try {
                  List<SSNSO_Ticket> tkList = service.getSSNSOTICKETList();
                  for (SSNSO_Ticket ticked : tkList) {
                        List<SSNSO_Partandlabor> plList = ticked.getSSNSOPARTANDLABORList();
                        for (int i = 0; i < plList.size(); i++) {
                              SSNSO_Partandlabor partandLabor = plList.get(i);
                              OrderLine newOl = OBProvider.getInstance().get(OrderLine.class);
                              lineNo += 10L;
                              newOl.setSalesOrder(neworder);
                              newOl.setOrganization(neworder.getOrganization());
                              newOl.setLineNo(lineNo);
                              newOl.setAttributeSetValue(partandLabor.getAttributeSetValue());
                              newOl.setOrderDate(neworder.getOrderDate());
                              newOl.setWarehouse(neworder.getWarehouse());
                              newOl.setCurrency(neworder.getCurrency());
                              newOl.setProduct(partandLabor.getProduct());
                              newOl.setUOM(partandLabor.getUOM());
                              newOl.setOrderedQuantity(partandLabor.getQuantityordered());
                              newOl.setTax(partandLabor.getTax());
                              newOl.setUnitPrice(partandLabor.getPriceunit());
                              newOl.setGrossListPrice(partandLabor.getGrossListPrice());
                              newOl.setLineNetAmount(partandLabor.getLineNetAmount());
                              newOl.setLineGrossAmount(partandLabor.getLineGrossAmount());
                              newOl.setListPrice(partandLabor.getListPrice());
                              newOl.setDiscount(partandLabor.getDiscount());
                              newOl.setDescription(partandLabor.getDescription());
                              newOl.setProject(partandLabor.getProject());

                              newOl.setWarehouseRule(partandLabor.getWarehouseRule());
                              newOl.setTaxableAmount(partandLabor.getAlternateTaxableAmt());
                              newOl.setCostcenter(partandLabor.getCostCenter());

                              olList.add(newOl);
                              OBDal.getInstance().save(newOl);
                        }
                  }
            } catch (Exception e1) {
                  throw new OBException(e1.getMessage(), e1);
            }

            return olList;
      }
}