package com.spocsys.softnetworks.serviceorder.ad_actionButton;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.application.process.BaseProcessActionHandler;

public class ServiceOrderToQuoteProcess extends BaseProcessActionHandler {

      private static final Logger log = Logger.getLogger(ServiceOrderToQuoteProcess.class);

      protected JSONObject doExecute(Map<String, Object> parameters, String content) {
            try {
                  JSONObject request = new JSONObject(content);
                  JSONObject msg = new JSONObject();
                  String strOrderId = request.getString("inpcOrderId");
                  String strServiceOId = request.getString("inpssnsoServiceorderId");
                  try {
                        ServiceOrderToQuote manage = new ServiceOrderToQuote(strOrderId, strServiceOId);
                        boolean isquoteDraft = manage.isOrderDraft();
                        if (!isquoteDraft) {
                              throw new OBException("Error. Service Quote to update is Completed");
                        }
                        msg = manage.manageGeneration();
                        request.put("message", msg);
                        return request;
                  } catch (Exception e1) {
                        msg.put("severity", "error");
                        msg.put("title", "Error");
                        msg.put("text", e1.getMessage());
                        request.put("message", msg);
                        return request;
                  }
            } catch (Exception e1) {
                  System.out.println("main void error: " + e1.getMessage());
                  log.error(e1.getMessage());
                  return new JSONObject();
            }
      }

}