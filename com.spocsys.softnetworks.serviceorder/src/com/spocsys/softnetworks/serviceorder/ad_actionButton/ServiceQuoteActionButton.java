package com.spocsys.softnetworks.serviceorder.ad_actionButton;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.softnetworks.serviceorder.utils.ActionButtonUtility;

public class ServiceQuoteActionButton extends HttpSecureAppServlet {

      private static final long serialVersionUID = 1L;

      public void init(ServletConfig config) {
            super.init(config);
            boolHist = false;
      }

      public void doPost(HttpServletRequest request, HttpServletResponse response)
                  throws IOException, ServletException {

            VariablesSecureApp vars = new VariablesSecureApp(request);
            String strTab = "";
            String strWindowPath = "";

            Enumeration<String> names = vars.getParameterNames();
            try {
                  if (vars.commandIn("DEFAULT")) {

                        String strCOrderId = vars.getStringParameter("inpcOrderId");
                        String strWindowId = vars.getStringParameter("inpwindowId");
                        strTab = vars.getStringParameter("inpTabId");
                        strWindowPath = Utility.getTabURL(strTab, "R", true);

                        if (strWindowPath.equals("")) {
                              strWindowPath = strDefaultServlet;
                        }

                        if (!this.serviceQuoteHasLines(strCOrderId)) {
                              // document needs lines to be processed
                              OBError msg = new OBError();
                              msg.setMessage("Document has not Lines");
                              msg.setType("Error");
                              msg.setTitle("Error");
                              vars.setMessage(strTab, msg);
                              printPageClosePopUp(response, vars, strWindowPath);

                        } else {
                              String strDocStatus = vars.getStringParameter("inpdocstatus");
                              String strDocAction = vars.getStringParameter("inpemSsnsoDocaction");

                              // Validating ssnsoDocStatus and ssnsoDocAction of Service Quote witch
                              // it
                              // field is Empty
                              if (strDocAction.equals("") || strDocAction.equals("undefined")
                                          || strDocAction.equals("null")) {
                                    strDocAction = "BK";
                              }
                              // Order Table in ad_table
                              String strTableId = "259";
                              printPageHtml(vars, response, strDocAction, strDocStatus, strTableId, strWindowId, strTab,
                                          strCOrderId);
                        }
                  } else if (vars.commandIn("SAVE")) {
                        strTab = vars.getStringParameter("inptabId");
                        strWindowPath = Utility.getTabURL(strTab, "R", true);
                        String strDocStatus = vars.getStringParameter("inpdocstatus");
                        String strDocAction = vars.getStringParameter("inpdocaction");
                        String inpCOrderId = vars.getStringParameter("strCOrderId");
                        OBError message = null;
                        message = this.manageDoucmentActionButton(vars, inpCOrderId, strDocStatus, strDocAction);

                        vars.setMessage(strTab, message);
                        printPageClosePopUp(response, vars, strWindowPath);
                  } else
                        pageErrorPopUp(response);
            } catch (Exception e1) {
                  OBError err = new OBError();
                  err.setType("Error");
                  err.setTitle("Error");
                  err.setMessage(e1.getMessage());
                  vars.setMessage(strTab, err);
                  printPageClosePopUp(response, vars, strWindowPath);
            }
      }

      private void printPageHtml(VariablesSecureApp vars, HttpServletResponse response, String strDocAction,
                  String strDocStatus, String strTableId, String strWindowId, String strTabId, String strCOrderId)
                  throws IOException, ServletException {
            try {
                  FieldProvider[] dataDocAction = ActionButtonUtility.docStatus(this, vars, strDocAction, strDocStatus,
                              strTableId, strWindowId, strTabId);
                  XmlDocument xmlDocument = null;
                  xmlDocument = xmlEngine
                              .readXmlTemplate("com/spocsys/softnetworks/serviceorder/ad_actionButton/DocAction")
                              .createXmlDocument();
                  {
                        OBError myMessage = vars.getMessage("ServiceQuoteActionButton");
                        vars.removeMessage("ServiceQuoteActionButton");
                        if (myMessage != null) {
                              xmlDocument.setParameter("messageType", myMessage.getType());
                              xmlDocument.setParameter("messageTitle", myMessage.getTitle());
                              xmlDocument.setParameter("messageMessage", myMessage.getMessage());
                        }
                  }

                  xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
                  xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
                  xmlDocument.setParameter("theme", vars.getTheme());
                  xmlDocument.setParameter("css", vars.getTheme());
                  xmlDocument.setParameter("window", strWindowId);
                  xmlDocument.setParameter("inpTabId", strTabId);
                  xmlDocument.setParameter("docstatus", strDocStatus);
                  xmlDocument.setParameter("strCOrderId", strCOrderId);

                  xmlDocument.setData("reportdocaction", "liststructure", dataDocAction);
                  response.setContentType("text/html; charset=UTF-8");

                  PrintWriter out = response.getWriter();
                  out.println(xmlDocument.print());
                  out.close();

            } catch (Exception e1) {
                  throw new OBException(e1.getMessage(), e1);
            }

      }

      /**
       * this method allow managing of actionButton Servlet. Taking the action selected by user.
       * order.setDocumentStatus set the status related with action choiced order.setSSnsoDocaction
       * is for change the name of the button showed in Service Quote Window
       * 
       * @param vars
       * @param strOrder
       *              string with id of C_order in question
       * @param StrDocAction
       *              Action regarding for document selected by user in the Pupup
       * @return OBError with successful message
       */
      private OBError manageDoucmentActionButton(VariablesSecureApp vars, String strOrder, String strDocStatus,
                  String StrDocAction) {
            OBError myMessage = new OBError();
            try {
                  OBContext.setAdminMode();
                  Order order = OBDal.getInstance().get(Order.class, strOrder);
                  if (StrDocAction.equals("IP")) {
                        order.setDocumentStatus("IP"); // Under Way
                        order.setSsnsoDocaction("CO"); // Approve
                  }
                  if (StrDocAction.equals("CO")) {
                        order.setDocumentStatus("CO"); // Booked
                        order.setSsnsoDocaction("CL"); // Button Close
                  } else if (StrDocAction.equals("RJ")) {
                        order.setDocumentStatus("SSNSO_RJ"); // Rejected
                        order.setSsnsoDocaction("CL");
                  } else if (StrDocAction.equals("RE")) {
                        order.setDocumentStatus("DR"); // DRAFT
                        order.setSsnsoDocaction("BK"); // Book
                  } else if (StrDocAction.equals("CL")) { // Close
                        String status = (strDocStatus.equals("CO")) ? "CO" : "SSNSO_RJ";
                        order.setDocumentStatus(status);
                        order.setSsnsoDocaction("--"); // Hide Button
                  }
                  OBDal.getInstance().save(order);
                  OBDal.getInstance().flush();
                  OBDal.getInstance().commitAndClose();

            } catch (Exception e1) {
                  throw new OBException(
                              "Error. An Error occourred while setting the action selected " + e1.getMessage(), e1);
            } finally {
                  OBContext.restorePreviousMode();
            }
            myMessage.setMessage(Utility.messageBD(this, "ProcessOK", vars.getLanguage()));
            myMessage.setTitle("Success");
            myMessage.setType("Success");
            return myMessage;

      }

      private boolean serviceQuoteHasLines(String strOrder) {
            OBContext.setAdminMode();
            boolean hasLines = false;
            try {
                  Order order = OBDal.getInstance().get(Order.class, strOrder);
                  hasLines = (!order.getOrderLineList().isEmpty()) ? true : false;
            } catch (Exception e1) {
                  throw new OBException(
                              "An error occurr while processing Service Quote Lines" + e1.getLocalizedMessage(), e1);
            } finally {
                  OBContext.restorePreviousMode();
            }

            return hasLines;
      }

}
