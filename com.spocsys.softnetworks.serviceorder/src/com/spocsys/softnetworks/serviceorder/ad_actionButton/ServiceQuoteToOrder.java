package com.spocsys.softnetworks.serviceorder.ad_actionButton;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;

import com.spocsys.softnetworks.serviceorder.SSNSO_Partandlabor;
import com.spocsys.softnetworks.serviceorder.SSNSO_Ticket;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;
import com.sun.xml.messaging.saaj.packaging.mime.internet.ParseException;

public class ServiceQuoteToOrder {
      private static final Logger log4j = Logger.getLogger(ServiceQuoteToOrder.class);
      private Order               order;
      private ServiceOrder        service;
      private BusinessPartner     tecnician, outsorced;
      private Date                startingDate, dueDate;
      private boolean             update;

      public ServiceQuoteToOrder(String strOrderId, String serviceOrderId, String technicianId, String outsorcedId,
                  Date startingDate, Date dueDate) {

            order = (strOrderId.equals("")) ? null : OBDal.getInstance().get(Order.class, strOrderId);

            boolean serviceIsNull = ((!serviceOrderId.equals("undefined")) && (!serviceOrderId.equals("null"))
                        && (!serviceOrderId.equals(""))) ? false : true;

            service = (serviceIsNull) ? null : OBDal.getInstance().get(ServiceOrder.class, serviceOrderId);
            update = (service == null) ? false : true;
            tecnician = OBDal.getInstance().get(BusinessPartner.class, technicianId);
            outsorced = OBDal.getInstance().get(BusinessPartner.class, outsorcedId);
            this.startingDate = startingDate;
            this.dueDate = dueDate;

      }

      public boolean isServiceOrderDraft() {
            if (update) {
                  String docstatus = service.getDocumentStatus();
                  return (docstatus.equals("DR")) ? true : false;
            }
            return true;
      }

      /**
       * This method allow you create Service Order from Service Quote
       * 
       * @return JSONObject an object with the success message
       * @throws OBException
       * @throws JSONException
       */
      public JSONObject manageGeneration() throws OBException, JSONException {
            String documentNo = "";
            JSONObject myMessage = new JSONObject();
            try {
                  OBContext.setAdminMode();
                  ServiceOrder so = genHeader();
                  documentNo = so.getDocumentno();
                  SSNSO_Ticket tk = genTicket(so);
                  genPartandLabor(tk);
                  updateTikedTotals(tk);
                  OBDal.getInstance().flush();
                  OBDal.getInstance().commitAndClose();
            } catch (Exception e1) {
                  OBDal.getInstance().rollbackAndClose();
                  log4j.error(e1.getMessage());
                  throw new OBException(e1.getMessage(), e1);
            } finally {
                  OBContext.restorePreviousMode();
            }
            String add = (!update) ? " <br> Service Order was created with Document No. " + documentNo
                        : "<br> Service Order with Document No. " + documentNo + " was updated ";
            String message = "<strong>Process completed successfully</strong> " + add;
            myMessage.put("severity", "success");
            myMessage.put("title", "success");
            myMessage.put("text", message);
            return myMessage;
      }

      private ServiceOrder genHeader() throws OBException, ParseException {
            ServiceOrder so = null;
            so = (update) ? service : OBProvider.getInstance().get(ServiceOrder.class);
            try {
                  so.setOrganization(order.getOrganization());
                  so.setSalesOrder(order);
                  int p = Integer.valueOf(order.getPriority());
                  String prior = ((p <= 5)) ? "p1" : "p2";
                  so.setPriority(prior);
                  so.setDocumentStatus("DR");
                  so.setAlertStatus("s1");
                  so.setShiptoLoc(order.getPartnerAddress());
                  so.setClientid(order.getBusinessPartner());
                  DocumentType docType = order.getTransactionDocument();
                  if (docType.getDocumentSequence() == null) {
                        String message = " Can't generate Service Quote because <br> " + "Document Type "
                                    + docType.getName() + " of Service Quote " + order.getDocumentNo()
                                    + " have't Sequence assigned ";
                        throw new OBException(message);

                  } else {
                        if (!update) {
                              so.setDocumentno(FIN_Utility.getDocumentNo(docType, "ssnso_serviceorder", true));
                        }
                  }
                  so.setDocumentType(order.getTransactionDocument());
                  so.setJOBType("RE");
                  if (!update) {
                        order.setSsnsoServiceorder(so);
                        OBDal.getInstance().save(order);
                  }
                  OBDal.getInstance().save(so);
            } catch (Exception e1) {
                  throw new OBException(e1.getMessage(), e1);
            }
            return so;
      }

      private SSNSO_Ticket genTicket(ServiceOrder so) throws OBException {
            long lineNo = 0L;
            ServiceOrder newso = so;

            SSNSO_Ticket tk = OBProvider.getInstance().get(SSNSO_Ticket.class);
            List<SSNSO_Ticket> tickedList = new ArrayList<SSNSO_Ticket>();

            if (update) {
                  tickedList = service.getSSNSOTICKETList();
                  tickedList.clear();
                  // OBDal.getInstance().save(service);
            }
            try {
                  if (!update) {
                        final OBCriteria<SSNSO_Ticket> linenoCrit = OBDal.getInstance()
                                    .createCriteria(SSNSO_Ticket.class);
                        linenoCrit.add(Restrictions.eq(SSNSO_Ticket.PROPERTY_SERVICEORDER, service));
                        linenoCrit.setProjection(Projections.max(SSNSO_Ticket.PROPERTY_TICKETNUMBER));
                        Object o = linenoCrit.list().get(0);
                        if (o != null) {
                              lineNo = (Long) o;
                        }
                  }
                  lineNo += 10L;
                  tk.setTicketNumber(lineNo);
                  tk.setServiceOrder(so);
                  tk.setOrganization(so.getOrganization());
                  tk.setStatus("COM");
                  tk.setTechnician(tecnician);
                  tk.setOutsourced(outsorced);
                  tk.setStartingDate(startingDate);
                  tk.setDueDate(dueDate);
                  OBDal.getInstance().save(tk);
                  // Setting ticket to service order

                  tickedList.add(tk);
                  newso.setSSNSOTICKETList(tickedList);
                  OBDal.getInstance().save(newso);
            } catch (Exception e1) {
                  throw new OBException(e1.getMessage(), e1);
            }
            return tk;
      }

      private void genPartandLabor(SSNSO_Ticket tk) throws OBException {
            SSNSO_Partandlabor propl = OBProvider.getInstance().get(SSNSO_Partandlabor.class);
            SSNSO_Ticket ticket = tk;
            List<SSNSO_Partandlabor> plList = tk.getSSNSOPARTANDLABORList();
            OBContext.setAdminMode();
            try {
                  List<OrderLine> olList = order.getOrderLineList();
                  if (!olList.isEmpty()) {
                        for (int i = 0; i < olList.size(); i++) {
                              OrderLine ol = olList.get(i);
                              propl = OBProvider.getInstance().get(SSNSO_Partandlabor.class);
                              propl.setSsnsoTicket(tk);
                              propl.setOrganization(ol.getOrganization());
                              propl.setLineNo(ol.getLineNo());
                              propl.setProduct(ol.getProduct());
                              propl.setQuantityordered(ol.getOrderedQuantity());
                              propl.setUOM(ol.getUOM());
                              propl.setPriceunit(ol.getUnitPrice());
                              propl.setLineNetAmount(ol.getLineNetAmount());
                              propl.setGrossListPrice(ol.getGrossListPrice());
                              propl.setLineGrossAmount(ol.getLineGrossAmount());
                              propl.setTax(ol.getTax());
                              propl.setListPrice(ol.getListPrice());
                              propl.setDiscount(ol.getDiscount());
                              propl.setDescription(ol.getDescription());
                              propl.setProject(ol.getProject());
                              propl.setWarehouseRule(ol.getWarehouseRule());
                              propl.setDescription(ol.getDescription());
                              propl.setAlternateTaxableAmt(ol.getTaxableAmount());
                              propl.setSalesOrderLine(ol);
                              propl.setCostCenter(ol.getCostcenter());
                              propl.setPurchaseorder(order);
                              OBDal.getInstance().save(propl);
                              plList.add(propl);
                        }
                        ticket.setSSNSOPARTANDLABORList(plList);
                        OBDal.getInstance().save(ticket);
                  }
            } catch (OBException e1) {
                  throw new OBException(e1.getMessage(), e1);
            }
      }

      private void updateTikedTotals(SSNSO_Ticket tk) throws OBException {
            SSNSO_Ticket tkk = OBDal.getInstance().get(SSNSO_Ticket.class, tk.getId());
            try {
                  final List<SSNSO_Partandlabor> plList = tkk.getSSNSOPARTANDLABORList();
                  BigDecimal totalParts = BigDecimal.ZERO;
                  BigDecimal totalLaborts = BigDecimal.ZERO;
                  if (!plList.isEmpty()) {
                        for (int i = 0; i < plList.size(); i++) {
                              SSNSO_Partandlabor pl = plList.get(i);
                              Product product = pl.getProduct();
                              boolean isItem = (product.getProductType().equals("I")) ? true : false;
                              if (isItem) {
                                    BigDecimal lineNetAmt = pl.getLineNetAmount();
                                    totalParts = totalParts.add(lineNetAmt);
                              }
                              boolean isService = (product.getProductType().equals("S")) ? true : false;
                              if (isService) {
                                    BigDecimal lineNetAmt = pl.getLineNetAmount();
                                    totalLaborts = totalLaborts.add(lineNetAmt);
                              }
                        }
                  }
                  tkk.setTotalParts(totalParts);
                  tkk.setTotalLabors(totalLaborts);
                  OBDal.getInstance().save(tkk);
            } catch (OBException e1) {
                  throw new OBException(e1.getMessage(), e1);
            }
      }
}