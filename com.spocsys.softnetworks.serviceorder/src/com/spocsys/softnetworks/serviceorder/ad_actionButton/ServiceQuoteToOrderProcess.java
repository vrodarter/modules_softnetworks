package com.spocsys.softnetworks.serviceorder.ad_actionButton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.application.process.BaseProcessActionHandler;

public class ServiceQuoteToOrderProcess extends BaseProcessActionHandler {

      private static final Logger log          = Logger.getLogger(ServiceOrderToQuoteProcess.class);
      SimpleDateFormat            outputFormat = new SimpleDateFormat("yyyy-MM-dd");

      protected JSONObject doExecute(Map<String, Object> parameters, String content) {
            try {

                  JSONObject request = new JSONObject(content);
                  JSONObject msg = new JSONObject();
                  JSONObject params = request.getJSONObject("_params");
                  String strOrderId = request.getString("C_Order_ID");
                  String strServiceOId = request.getString("inpemSsnsoServiceorderId");
                  String technician = params.getString("inpTechnician");
                  String outsorced = params.getString("inpOutsorced");
                  Date startingDate = null;
                  Date dueDate = null;
                  String stDate = params.getString("inpStartingDate");
                  String dDate = params.getString("inpDueDate");
                  startingDate = outputFormat.parse(stDate);
                  dueDate = outputFormat.parse(dDate);

                  try {
                        ServiceQuoteToOrder manage = new ServiceQuoteToOrder(strOrderId, strServiceOId, technician,
                                    outsorced, startingDate, dueDate);
                        boolean isServiceorderDraft = manage.isServiceOrderDraft();
                        if (!isServiceorderDraft) {
                              throw new OBException("Error. Service Order to update is Completed");
                        }
                        msg = manage.manageGeneration();
                        request.put("message", msg);
                        return request;
                  } catch (Exception e1) {
                        msg.put("severity", "error");
                        msg.put("title", "error");
                        msg.put("text", e1.getMessage());
                        request.put("message", msg);
                        return request;
                  }

            } catch (Exception e1) {
                  System.out.println("main void error: " + e1.getMessage());
                  log.error(e1.getMessage());
                  return new JSONObject();
            }

      }

}