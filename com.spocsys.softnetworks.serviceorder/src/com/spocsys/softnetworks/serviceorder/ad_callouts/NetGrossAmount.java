package com.spocsys.softnetworks.serviceorder.ad_callouts;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.servlet.ServletException;

import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;

public class NetGrossAmount extends SimpleCallout {

  private static final long serialVersionUID = 1L;

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    String lastField = info.getStringParameter("inpLastFieldChanged", null);
    String isGrossPrice = info.getStringParameter("GROSSPRICE", null);

    BigDecimal Qty = info.getBigDecimalParameter("inpquantityordered");
    BigDecimal ListPrice = info.getBigDecimalParameter("inppricelist");
    BigDecimal Priceunit = info.getBigDecimalParameter("inppriceunit");
    BigDecimal Discount = info.getBigDecimalParameter("inpdiscount");
    BigDecimal GrossUnitPrice = info.getBigDecimalParameter("inpgrosspricelist");
    BigDecimal LinNetAmt = BigDecimal.ZERO;
    BigDecimal LinGrossAmt = BigDecimal.ZERO;
    BigDecimal DiscAmt = BigDecimal.ZERO;
    OBContext.setAdminMode();

    try {

      LinNetAmt = (Qty.multiply(Priceunit));
      DiscAmt = Discount.multiply(LinNetAmt).divide(new BigDecimal("100"), 4,
          BigDecimal.ROUND_HALF_EVEN);
      LinNetAmt = LinNetAmt.subtract(DiscAmt);
      LinGrossAmt = (Qty.multiply(GrossUnitPrice));
      DiscAmt = Discount.multiply(LinGrossAmt).divide(new BigDecimal("100"), 4,
          BigDecimal.ROUND_HALF_EVEN);
      LinGrossAmt = LinGrossAmt.subtract(DiscAmt);
      if (isGrossPrice.equals("N")) {
        // Discount
        if (lastField.equals("inpdiscount")) {
          // calcular el pricelist (looking for discount)
          ListPrice = Priceunit.multiply((new BigDecimal(100).subtract(Discount)))
              .divide(new BigDecimal(100), RoundingMode.HALF_UP);
          info.addResult("inppricelist", ListPrice);
        } else {
          // calcular el descuento (looking for pricelist)
          Discount = (Priceunit == null || Priceunit.compareTo(BigDecimal.ZERO) == 0)
              ? BigDecimal.ZERO
              : new BigDecimal(100).subtract((ListPrice.multiply(new BigDecimal(100))
                  .divide(Priceunit, RoundingMode.HALF_UP)));
          info.addResult("inpdiscount", Discount);
        }
      }
      info.addResult("inplinenetamt", LinNetAmt);
      info.addResult("inplineGrossAmount", LinGrossAmt);

    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}