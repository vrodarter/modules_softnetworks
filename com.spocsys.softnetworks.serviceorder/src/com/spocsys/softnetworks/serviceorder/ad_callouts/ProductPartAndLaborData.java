//Sqlc generated V1.O00-1
package com.spocsys.softnetworks.serviceorder.ad_callouts;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.data.FieldProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.database.SessionInfo;
import org.openbravo.service.db.QueryTimeOutUtil;

public class ProductPartAndLaborData implements FieldProvider {
  static Logger log4j = Logger.getLogger(ProductPartAndLaborData.class);
  private String InitRecordNumber = "0";
  public String id;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("id"))
      return id;

    else {
      log4j.debug("Field does not exist: " + fieldName);
      return null;
    }
  }

  public static String getNetFromGross(ConnectionProvider connectionProvider, String sTaxId,
      String sNetAmount, String alternateAmount, String precision, String qty)
      throws ServletException {
    String strSql = "";
    strSql = strSql
        + "        select c_get_net_price_from_gross(?, TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?)) as amount from dual";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
      st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, sTaxId);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, sNetAmount);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, alternateAmount);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, precision);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, qty);

      result = st.executeQuery();
      if (result.next()) {
        strReturn = UtilSql.getValue(result, "amount");
      }
      result.close();
    } catch (SQLException e) {
      log4j.error("SQL error in query: " + strSql + "Exception:" + e);
      throw new ServletException(
          "@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch (Exception ex) {
      log4j.error("Exception in query: " + strSql + "Exception:" + ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch (Exception ignore) {
        ignore.printStackTrace();
      }
    }
    return (strReturn);
  }

  public static String getTaxAmountFromNet(ConnectionProvider connectionProvider, String sTaxId,
      String sNetAmount, String alternateAmount, String precision, String qty)
      throws ServletException {
    String strSql = "";
    strSql = strSql
        + "        select c_get_tax_amt_from_net(?, TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?)) as amount from dual";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
      st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, sTaxId);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, sNetAmount);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, alternateAmount);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, precision);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, qty);

      result = st.executeQuery();
      if (result.next()) {
        strReturn = UtilSql.getValue(result, "amount");
      }
      result.close();
    } catch (SQLException e) {
      log4j.error("SQL error in query: " + strSql + "Exception:" + e);
      throw new ServletException(
          "@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch (Exception ex) {
      log4j.error("Exception in query: " + strSql + "Exception:" + ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch (Exception ignore) {
        ignore.printStackTrace();
      }
    }
    return (strReturn);
  }

}
