/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2013 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.spocsys.softnetworks.serviceorder.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.businessUtility.PAttributeSet;
import org.openbravo.erpCommon.businessUtility.PAttributeSetData;
import org.openbravo.erpCommon.businessUtility.PriceAdjustment;
import org.openbravo.erpCommon.businessUtility.Tax;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.plm.Product;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.softnetworks.serviceorder.SSNSO_Ticket;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;
import com.spocsys.vigfurniture.productavailability.utils.Utils;

public class SSNSO_Product_partandlabor extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
      String strChanged = vars.getStringParameter("inpLastFieldChanged");
      log4j.debug("CHANGED: " + strChanged);
      String strUOM = vars.getStringParameter("inpmProductId_UOM");
      String strPriceList = vars.getNumericParameter("inppricelist");
      String strPriceStd = vars.getNumericParameter("inppriceunit");
      String strPriceLimit = vars.getNumericParameter("inpmProductId_PLIM");
      String strCurrency = vars.getStringParameter("inpmProductId_CURR");
      String strQty = vars.getNumericParameter("inpquantityordered");

      Enumeration<String> params = vars.getParameterNames();

      String strMProductID = vars.getStringParameter("inpmProductId");
      String strServiceorderID = vars.getStringParameter("inpssnsoServiceorderId");
      String strTicketID = vars.getStringParameter("inpssnsoTicketId");

      String strCBPartnerLocationID = vars.getStringParameter("inpcBpartnerLocationId");
      String strADOrgID = vars.getStringParameter("inpadOrgId");
      String strMWarehouseID = vars.getStringParameter("inpmWarehouseId");
      String strCOrderId = vars.getStringParameter("inpcOrderId");
      String strWindowId = vars.getStringParameter("inpwindowId");
      String strIsSOTrx = Utility.getContext(this, vars, "isSOTrx", strWindowId);
      String cancelPriceAd = vars.getStringParameter("inpcancelpricead");

      String strWarehouseEntered = vars.getStringParameter("inpmProductId_WARE");

      BigDecimal QuantityEntered = Utils
          .getBiDecimal(vars.getNumericParameter("inpminpquantityordered"));

      if (QuantityEntered != null) {
        HttpSession Session = request.getSession(true);
        Session.setAttribute("M_Product_ID", strMProductID);
        Session.setAttribute("inpquantityordered", QuantityEntered.toString());
      }

      // ---
      // added by: gpantoja

      // -------------------------------------------------------------------

      try {
        printPage(response, vars, strUOM, strPriceList, strPriceStd, strPriceLimit, strCurrency,
            strMProductID, strCBPartnerLocationID, strADOrgID, strMWarehouseID, strCOrderId,
            strIsSOTrx, strQty, cancelPriceAd, strWarehouseEntered, QuantityEntered,
            strServiceorderID, strTicketID);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String _strUOM,
      String strPriceList, String _strPriceStd, String _strPriceLimit, String strCurrency,
      String strMProductID, String strCBPartnerLocationID, String strADOrgID,
      String strMWarehouseID, String strCOrderId, String strIsSOTrx, String strQty,
      String cancelPriceAd, String strWarehouseEntered, BigDecimal QuantityEntered,
      String strServiceorderID, String strTicketID) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate(
            "com/spocsys/vigfurniture/productavailability/erpCommon/ad_callouts/CallOut")
        .createXmlDocument();

    String strPriceActual = "";
    String strHasSecondaryUOM = "\"\"";
    String strUOM = _strUOM;
    String strPriceLimit = _strPriceLimit;
    String strPriceStd = _strPriceStd;
    String strPriceStd_Popup = vars.getStringParameter("inpmProductId_PSTD");
    String strPriceList_Popup = vars.getStringParameter("inpmProductId_PLIST");
    String strNetPriceList = strPriceList;
    if (strPriceStd_Popup != null && !strPriceStd_Popup.equals("")
        && new BigDecimal(strPriceStd_Popup).compareTo(BigDecimal.ZERO) > 0) {
      strPriceStd = strPriceStd_Popup;
    }
    if (strPriceList_Popup != null && !strPriceList_Popup.equals("")
        && new BigDecimal(strPriceList_Popup).compareTo(BigDecimal.ZERO) > 0) {
      strNetPriceList = strPriceList_Popup;
    }

    String strGrossPriceList = strPriceList;
    String strGrossBaseUnitPrice = _strPriceStd;
    if (strPriceList.startsWith("\"")) {
      strNetPriceList = strPriceList.substring(1, strPriceList.length() - 1);
      strGrossPriceList = strPriceList.substring(1, strPriceList.length() - 1);
    }
    if (_strPriceStd.startsWith("\"")) {
      strPriceStd = _strPriceStd.substring(1, _strPriceStd.length() - 1);
    }
    boolean isTaxIncludedPriceList = false;
    if (strCOrderId != null && !strCOrderId.equals("")) {
      isTaxIncludedPriceList = OBDal.getInstance().get(Order.class, strCOrderId).getPriceList()
          .isPriceIncludesTax();
    }

    ServiceOrder serviceorder = OBDal.getInstance().get(ServiceOrder.class, strServiceorderID);
    SSNSO_Ticket ticket = OBDal.getInstance().get(SSNSO_Ticket.class, strTicketID);

    if (!strMProductID.equals("")) {

      Order order = OBDal.getInstance().get(Order.class, strCOrderId);
      Product product = OBDal.getInstance().get(Product.class, strMProductID);

      if (!"Y".equals(cancelPriceAd)) {
        if (isTaxIncludedPriceList) {
          strGrossBaseUnitPrice = (strGrossBaseUnitPrice == null || strGrossBaseUnitPrice.equals("")
              || new BigDecimal(strGrossBaseUnitPrice).compareTo(BigDecimal.ZERO) == 0)
                  ? strPriceStd : strGrossBaseUnitPrice;

          strPriceActual = PriceAdjustment
              .calculatePriceActual(order, product,
                  "".equals(strQty) ? BigDecimal.ZERO : new BigDecimal(strQty),
                  new BigDecimal(strGrossBaseUnitPrice.equals("") ? "0" : strGrossBaseUnitPrice))
              .toString();
          strNetPriceList = "0";
        } else {
          strPriceActual = PriceAdjustment.calculatePriceActual(order, product,
              "".equals(strQty) ? BigDecimal.ZERO : new BigDecimal(strQty),
              new BigDecimal(strPriceStd.equals("") ? "0" : strPriceStd)).toString();
          strGrossPriceList = "0";
        }
      } else {
        if (isTaxIncludedPriceList)
          strPriceActual = strGrossBaseUnitPrice;
        else
          strPriceActual = strPriceList;
      }
    } else {
      strUOM = strNetPriceList = strGrossPriceList = strPriceLimit = strPriceStd = "";
    }

    StringBuffer resultado = new StringBuffer();
    // Discount...
    BigDecimal discount = BigDecimal.ZERO;
    BigDecimal priceStd = null;
    if (isTaxIncludedPriceList) {
      strGrossPriceList = (strGrossPriceList == null || strGrossPriceList.equals("")
          || new BigDecimal(strGrossPriceList).compareTo(BigDecimal.ZERO) == 0) ? strPriceList
              : strGrossPriceList;
      BigDecimal priceList = (strGrossPriceList.equals("") ? BigDecimal.ZERO
          : new BigDecimal(strGrossPriceList));
      strGrossPriceList = (strGrossPriceList.equals("0.00") ? "" : strGrossPriceList);
      BigDecimal grossBaseUnitPrice = (strGrossBaseUnitPrice.equals("") ? BigDecimal.ZERO
          : new BigDecimal(strGrossBaseUnitPrice));
      if (priceList.compareTo(BigDecimal.ZERO) != 0) {
        discount = priceList.subtract(grossBaseUnitPrice).multiply(new BigDecimal("100"))
            .divide(priceList, 2, BigDecimal.ROUND_HALF_UP);
      }
    } else {
      BigDecimal priceList = (strNetPriceList.equals("") ? BigDecimal.ZERO
          : new BigDecimal(strNetPriceList));
      priceStd = (strPriceStd.equals("") ? BigDecimal.ZERO : new BigDecimal(strPriceStd));
      if (priceList.compareTo(BigDecimal.ZERO) != 0) {
        discount = priceList.subtract(priceStd).multiply(new BigDecimal("100")).divide(priceList, 2,
            BigDecimal.ROUND_HALF_UP);
      }
    }

    resultado.append("var calloutName='SSNSO_Product_partandlabor';\n\n");
    resultado.append("var respuesta = new Array(");
    resultado.append("new Array(\"inpcUomId\", \"" + strUOM + "\"),");
    if (isTaxIncludedPriceList) {
      resultado.append("new Array(\"inpgrossUnitPrice\", "
          + (strPriceActual.equals("") ? "0" : strPriceActual) + "),");
      resultado.append("new Array(\"inpgrosslistprice\", "
          + (strGrossPriceList.equals("") ? strGrossBaseUnitPrice : strGrossPriceList) + "),");

      String grosspricelist = (strGrossPriceList.equals("") ? strGrossBaseUnitPrice
          : strGrossPriceList);
      resultado.append("new Array(\"inpgrosspricelist\", " + grosspricelist + "),");
      resultado.append("new Array(\"inpgrosspricestd\", "
          + (strGrossBaseUnitPrice.equals("") ? "0" : strGrossBaseUnitPrice) + "),");
    } else {
      resultado.append("new Array(\"inppricelist\", "
          + (strNetPriceList.equals("") ? "0" : strNetPriceList) + "),");
      resultado.append("new Array(\"inppricelimit\", "
          + (strPriceLimit.equals("") ? "0" : strPriceLimit) + "),");

      resultado.append(
          "new Array(\"inppriceunit\", " + (strPriceStd.equals("") ? "0" : strPriceStd) + "),");
      resultado.append("new Array(\"inppriceactual\", "
          + (strPriceActual.equals("") ? "0" : strPriceActual) + "),");
      // Get the Gross Price from Net

    }
    if (!"".equals(strCurrency)) {
      resultado.append("new Array(\"inpcCurrencyId\", \"" + strCurrency + "\"),");
    }
    resultado.append("new Array(\"inpdiscount\", " + discount.toString() + "),");
    if (!strMProductID.equals("")) {
      PAttributeSetData[] dataPAttr = PAttributeSetData.selectProductAttr(this, strMProductID);
      if (dataPAttr != null && dataPAttr.length > 0 && dataPAttr[0].attrsetvaluetype.equals("D")) {
        PAttributeSetData[] data2 = PAttributeSetData.select(this, dataPAttr[0].mAttributesetId);
        if (PAttributeSet.isInstanceAttributeSet(data2)) {
          resultado.append("new Array(\"inpmAttributesetinstanceId\", \"\"),");
          resultado.append("new Array(\"inpmAttributesetinstanceId_R\", \"\"),");
        } else {
          resultado.append("new Array(\"inpmAttributesetinstanceId\", \""
              + dataPAttr[0].mAttributesetinstanceId + "\"),");
          resultado.append("new Array(\"inpmAttributesetinstanceId_R\", \""
              + FormatUtilities.replaceJS(dataPAttr[0].description) + "\"),");
        }
      } else {
        resultado.append("new Array(\"inpmAttributesetinstanceId\", \"\"),");
        resultado.append("new Array(\"inpmAttributesetinstanceId_R\", \"\"),");
      }

      resultado.append("new Array(\"inpattributeset\", \""
          + FormatUtilities.replaceJS(dataPAttr[0].mAttributesetId) + "\"),\n");
      resultado.append("new Array(\"inpattrsetvaluetype\", \""
          + FormatUtilities.replaceJS(dataPAttr[0].attrsetvaluetype) + "\"),\n");
      // strHasSecondaryUOM = SLOrderProductData.hasSecondaryUOM(this, strMProductID);
      resultado.append("new Array(\"inphasseconduom\", " + strHasSecondaryUOM + "),\n");
    }

    String strCTaxID = "";
    String orgLocationID = "";

    Organization org = OBDal.getInstance().get(Organization.class, strADOrgID);
    List<OrganizationInformation> info = org.getOrganizationInformationList();
    if (info.size() > 0) {
      orgLocationID = info.get(0).getId();
    }

    if (orgLocationID.equals("")) {
      resultado
          .append("new Array('MESSAGE', \""
              + FormatUtilities.replaceJS(
                  Utility.messageBD(this, "NoLocationNoTaxCalculated", vars.getLanguage()))
              + "\"),\n");
    } else {
      strCTaxID = Tax.get(this, strMProductID, getDate(ticket.getStartingDate()), strADOrgID,
          strMWarehouseID, serviceorder.getShiptoLoc().getId(), serviceorder.getShiptoLoc().getId(),
          "", true, false);
    }
    if (!strCTaxID.equals("")) {
      resultado.append("new Array(\"inpcTaxId\", \"" + strCTaxID + "\"),\n");
    }

    resultado.append("new Array(\"inpmProductUomId\", ");
    if (strUOM.startsWith("\""))
      strUOM = strUOM.substring(1, strUOM.length() - 1);
    // String strmProductUOMId = SLOrderProductData.strMProductUOMID(this,strMProductID,strUOM);

    if (vars.getLanguage().equals("en_US")) {
      FieldProvider[] tld = null;
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLE", "", "M_Product_UOM",
            "", Utility.getContext(this, vars, "#AccessibleOrgTree", "SSNSO_Product_partandlabor"),
            Utility.getContext(this, vars, "#User_Client", "SSNSO_Product_partandlabor"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "SSNSO_Product_partandlabor",
            "");
        tld = comboTableData.select(false);
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }

      if (tld != null && tld.length > 0) {
        resultado.append("new Array(");
        for (int i = 0; i < tld.length; i++) {
          resultado.append("new Array(\"" + tld[i].getField("id") + "\", \""
              + FormatUtilities.replaceJS(tld[i].getField("name")) + "\", \"false\")");
          if (i < tld.length - 1) {
            resultado.append(",\n");
          }
        }
        resultado.append("\n)");
      } else {
        resultado.append("null");
      }
      resultado.append("\n),");
    } else {
      FieldProvider[] tld = null;
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLE", "", "M_Product_UOM",
            "", Utility.getContext(this, vars, "#AccessibleOrgTree", "SSNSO_Product_partandlabor"),
            Utility.getContext(this, vars, "#User_Client", "SSNSO_Product_partandlabor"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "SSNSO_Product_partandlabor",
            "");
        tld = comboTableData.select(false);
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }

      if (tld != null && tld.length > 0) {
        resultado.append("new Array(");
        for (int i = 0; i < tld.length; i++) {
          resultado.append("new Array(\"" + tld[i].getField("id") + "\", \""
              + FormatUtilities.replaceJS(tld[i].getField("name")) + "\", \"false\")");
          if (i < tld.length - 1) {
            resultado.append(",\n");
          }
        }
        resultado.append("\n)");
      } else {
        resultado.append("null");
      }
      resultado.append("\n),");
    }

    // ---------------------------------------------------------------

    if (QuantityEntered != null) {
      resultado
          .append("new Array(\"inpquantityordered\", \"" + QuantityEntered.toString() + "\"),\n");
    }

    if ((strWarehouseEntered != null) && (strWarehouseEntered.equals("") != true)) {
      resultado
          .append("new Array(\"inpemSvfpaStockLocationId\", \"" + strWarehouseEntered + "\"),\n");
    }

    // ---------------------------------------------------------------

    resultado.append("new Array(\"EXECUTE\", \"displayLogic();\"),\n");
    // Para posicionar el cursor en el campo de cantidad
    resultado.append("new Array(\"CURSOR_FIELD\", \"inpquantityordered\")\n");
    if (!strHasSecondaryUOM.equals("0")) {
      resultado.append(", new Array(\"CURSOR_FIELD\", \"inpquantityordered\")\n");
    }

    resultado.append(");");
    xmlDocument.setParameter("array", resultado.toString());
    xmlDocument.setParameter("frameName", "appFrame");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    // System.out.println(xmlDocument.print());
    out.println(xmlDocument.print());
    out.close();
  }

  public static String getDate(Date date) {
    try {
      OBContext.setAdminMode(true);
      String dateFormat = "dd-MM-yyyy";
      SimpleDateFormat df = new SimpleDateFormat(dateFormat);
      String res = df.format(date);
      return res;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

}
