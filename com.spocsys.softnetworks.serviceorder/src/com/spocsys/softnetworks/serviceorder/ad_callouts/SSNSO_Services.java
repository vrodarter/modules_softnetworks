package com.spocsys.softnetworks.serviceorder.ad_callouts;

import javax.servlet.ServletException;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openbravo.base.session.OBPropertiesProvider;
import com.spocsys.softnetworks.serviceorder.SSNSO_Service;

public class SSNSO_Services extends SimpleCallout {

	private static final long serialVersionUID = 1L;

	@Override
	protected void execute(CalloutInfo info) throws ServletException {

		String strServiceId = info
				.getStringParameter("inpssnsoServiceId", null);

		// Retrieve the Warranty and Expiration Date of the Service

		OBContext.setAdminMode();
		try {
			final SSNSO_Service Service = OBDal.getInstance().get(
					SSNSO_Service.class, strServiceId);

			if (Service != null) {
				Boolean isWarranty = Service.isWarranty();
				Date ExpDate = Service.getWarrantyexpdate();
				String strExpDate = getStringDateTime(ExpDate) ;
				info.addResult("inpwarranty", isWarranty);
				info.addResult("inpexpirationdate", strExpDate); 
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			OBContext.restorePreviousMode();
		}
	}

	public static String getStringDateTime(Date date) {
		if (date == null)
			return null;
		String format = OBPropertiesProvider.getInstance().getOpenbravoProperties().getProperty("dateTimeFormat.java");
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String fecha = sdf.format(date);
		return fecha;
	}
}