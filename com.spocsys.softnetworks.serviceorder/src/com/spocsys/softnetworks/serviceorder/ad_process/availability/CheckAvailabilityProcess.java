package com.spocsys.softnetworks.serviceorder.ad_process.availability;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;

import com.spocsys.softnetworks.serviceorder.ServiceOrder;
import com.spocsys.softnetworks.serviceorder.ad_process.create_invoice.create_invoice;

public class CheckAvailabilityProcess extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(create_invoice.class);

  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    try {
      final JSONObject JSONData = new JSONObject(content);
      String ServiceOrderID = JSONData.getString("Ssnso_Serviceorder_ID");
      ServiceOrder ServiceOrder = OBDal.getInstance().get(ServiceOrder.class, ServiceOrderID);

      PLaborsAvailabilityProcess PAvailability = new PLaborsAvailabilityProcess();
      String Result = PAvailability.CheckAvailabilityProcess(ServiceOrder);

      JSONObject msg = new JSONObject();

      // put the message if it's success ///////
      if (Result != null) {
        msg.put("msgType", "success");
        msg.put("msgTitle", "Check Availability Succesfull");
        msg.put("msgText", Result);
      } else {
        msg.put("msgType", "error");
        msg.put("msgTitle", "Check Availability Error");
        msg.put("msgText", Result);
      }
      JSONObject msgInBPTabAction = new JSONObject();
      msgInBPTabAction.put("showMsgInView", msg);
      JSONData.put("responseActions", msgInBPTabAction);
      return JSONData;

    } catch (Exception e) {
      System.out.println("main void error: " + e.getMessage());
      log.error(e.getMessage());
      return new JSONObject();

      // esto es una prueba, pero normalmente me debieras oír
    }

  }
}