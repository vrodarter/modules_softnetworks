package com.spocsys.softnetworks.serviceorder.ad_process.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.service.db.CallStoredProcedure;

import com.spocsys.softnetworks.serviceorder.SSNSO_Partandlabor;
import com.spocsys.softnetworks.serviceorder.SSNSO_Ticket;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;
import com.spocsys.vigfurniture.incomingproduct.ProductTotalIncProdV;

//import com.spocsys.vigfurniture.productavailability.erpCommon.ad_callouts.StockAvailable;

class StockAvailable {
      
      Organization regOrganization = null;
      Warehouse    regWareHouse    = null;
      Locator      regLocator      = null;
      Product      regProduct      = null;
      BigDecimal   QtyAvailable    = null;
      BigDecimal   QtyOnhand       = null;
      
      public Organization getRegOrganization() {
            return regOrganization;
      }
      
      public void setRegOrganization(Organization regOrganization) {
            this.regOrganization = regOrganization;
      }
      
      public Warehouse getRegWareHouse() {
            return regWareHouse;
      }
      
      public Locator getRegLocator() {
            return regLocator;
      }
      
      public void setRegWareHouse(Warehouse regWareHouse) {
            this.regWareHouse = regWareHouse;
      }
      
      public void setRegLocator(Locator regLocator) {
            this.regLocator = regLocator;
      }
      
      public Product getProduct() {
            return regProduct;
      }
      
      public void setProduct(Product regProduct) {
            this.regProduct = regProduct;
      }
      
      public BigDecimal getQtyAvailable() {
            return QtyAvailable;
      }
      
      public void setQtyAvailable(BigDecimal qtyAvailable) {
            QtyAvailable = qtyAvailable;
      }
      
      public BigDecimal getQtyOnhand() {
            return QtyOnhand;
      }
      
      public void setQtyOnhand(BigDecimal qtyOnhand) {
            QtyOnhand = qtyOnhand;
      }
      
      public StockAvailable(Organization regOrganization, Warehouse regWareHouse,
                  Locator regLocator, Product regProduct, BigDecimal QtyAvailable,
                  BigDecimal QtyOnhand) {
            
            this.regOrganization = regOrganization;
            this.regWareHouse = regWareHouse;
            this.regLocator = regLocator;
            this.regProduct = regProduct;
            this.QtyAvailable = QtyAvailable;
            this.QtyOnhand = QtyOnhand;
      }
      
}

public class PLaborsAvailabilityProcess {
      
      String                                TYPE_SOURCE                = "VIG FURNITURE";
      String                                TYPE_SOURCE_VALUE          = "";
      String                                TYPE_SOURCE_VALUE_LOCATION = "";
      Organization                          OrgServiceOrder            = null;
      Warehouse                             WareSalesOrder             = null;
      Order                                 SalesOrderGlobal           = null;
      ServiceOrder                          ServiceOrderGlobal         = null;
      Warehouse                             StockLocationGlobal        = null;
      Warehouse                             ShipFromGlobal             = null;
      List<OrderLine>                       ListOrderLine              = null;
      List<SSNSO_Partandlabor>              PlaborLine                 = null;
      List<OrderLine>                       ListOrderLineNewest        = null;
      List<StockAvailable>                  ListStockAvailableTemp     = null;
      HashMap<String, List<StockAvailable>> HashMapProductsAvailable   = new HashMap<String, List<StockAvailable>>();
      HashMap<String, List<StockAvailable>> HashMapProductsAvailablePO = new HashMap<String, List<StockAvailable>>();
      int                                   LastNoLine                 = 0;
      List<StockAvailable>                  ListStockAvailable         = new ArrayList<StockAvailable>();
      int                                   regProcess                 = 0;
      int                                   regAdded                   = 0;
      int                                   regUpdated                 = 0;
      
      // ---
      // ---
      private static final Logger           log                        = Logger.getLogger(PLaborsAvailabilityProcess.class);
      
      public BigDecimal getAvailablePO(ProductTotalIncProdV regIncProduct) {
            
            BigDecimal QuantityAvailable = BigDecimal.ZERO;
            if (regIncProduct != null) {
                  
                  QuantityAvailable = regIncProduct.getQuantity();
                  
            }
            return QuantityAvailable;
      }
      
      Organization getOrgLegal(String OrgId) {
            
            Organization regOrganization = null;
            
            if ((OrgId != null) && (OrgId.trim().equals("") != true)) {
                  String ID = getOrgParent(OrgId);
                  
                  if (ID != null) {
                        try {
                              regOrganization = OBDal.getInstance().get(Organization.class, ID);
                        } catch (Exception e) {
                              e.printStackTrace();
                              log.error(e.getMessage());
                              regOrganization = null;
                        }
                  }
            }
            return regOrganization;
            
      }
      
      String getActionWhenisInStock(SSNSO_Partandlabor regPLabor) {
            String Action = null;
            
            try {
                  
                  if ((regPLabor.getStocklocation() != null) && (regPLabor.getShipfrom() != null)) {
                        
                        OBQuery<Organization> obq = getOrgToCompare(regPLabor.getStocklocation());
                        OBQuery<Organization> obq1 = getOrgToCompare(regPLabor.getShipfrom());
                        
                        if ((obq != null) && (obq.list().size() > 0) && (obq1 != null)
                                    && (obq1.list().size() > 0)) {
                              
                              if (regPLabor.getStocklocation().getId()
                                          .equalsIgnoreCase(regPLabor.getShipfrom().getId()) == true) {
                                    Action = "RTP";
                              } else {
                                    Action = "TRANSFER";
                              }
                              
                        } else {
                              Action = "INTERCOMPANY";
                        }
                  }
            } catch (Exception e) {
                  e.printStackTrace();
                  log.error(e);
                  Action = null;
            }
            
            return Action;
      }
      
      String getActionWhenisOutOfStock(OrderLine regOrderLine) {
            
            String Action = null;
            
            Organization regOrganization = getOrgLegal(regOrderLine.getSalesOrder()
                        .getOrganization().getId());
            
            Organization regOrganizationStockLocation = getOrgLegal(regOrderLine
                        .getSvfpaStockLocation().getSvfpaOrgsource().getId());
            
            TYPE_SOURCE_VALUE = "";
            TYPE_SOURCE_VALUE_LOCATION = "";
            
            if (regOrganization != null) {
                  TYPE_SOURCE_VALUE = regOrganization.getSearchKey();
            }
            
            if (regOrganizationStockLocation != null) {
                  TYPE_SOURCE_VALUE_LOCATION = regOrganizationStockLocation.getSearchKey();
            }
            
            if ((TYPE_SOURCE_VALUE.equals("") != true)
                        && (TYPE_SOURCE_VALUE_LOCATION.equals("") != true)) {
                  
                  if (TYPE_SOURCE_VALUE.equalsIgnoreCase(TYPE_SOURCE) == true) {
                        
                        if (TYPE_SOURCE_VALUE_LOCATION.equalsIgnoreCase(TYPE_SOURCE_VALUE) == true) {
                              Action = "SOURCE";
                        } else {
                              if (regOrderLine.getProduct().isSvfpaIsvigproduct() == true) {
                                    Action = "SOURCE";
                              } else {
                                    Action = "INTERCOMPANY";
                              }
                        }
                  } else {
                        if (TYPE_SOURCE_VALUE_LOCATION.equalsIgnoreCase(TYPE_SOURCE_VALUE) != true) {
                              Action = "INTERCOMPANY";
                              
                        } else {
                              if (regOrderLine.getProduct().isSvfpaIsvigproduct() == true) {
                                    Action = "INTERCOMPANY";
                              } else {
                                    Action = "SOURCE";
                              }
                        }
                  }
            }
            
            if ((regOrderLine.getProduct().getSvfsvProductCharact() != null)
                        && (regOrderLine.getProduct().getSvfsvProductCharact()
                                    .equalsIgnoreCase("SO") == true)) {
                  Action = "ISSUEPO";
            }
            
            if (regOrderLine.getProduct().isDiscontinued() == true) {
                  Action = "DISCONTINUED";
            }
            return Action;
      }
      
      String getOrgParent(String strOrgId) {
            String id_org = "";
            String strOrgIdNew = "0";
            
            if ((strOrgId != null) && (strOrgId.trim().equals("") != true)) {
                  strOrgIdNew = strOrgId;
            }
            
            try {
                  
                  final StringBuilder queryExtend = new StringBuilder();
                  queryExtend.append(" AS org");
                  queryExtend.append(" WHERE ");
                  queryExtend.append(" (ad_isorgincluded('" + strOrgIdNew
                              + "', org.id, org.client.id)<>-1)");
                  queryExtend.append(" AND (org.organizationType='1')");
                  
                  OBQuery<Organization> obq = OBDal.getInstance().createQuery(Organization.class,
                              queryExtend.toString());
                  
                  if ((obq != null) && (obq.list().size() > 0)) {
                        id_org = obq.list().get(0).getId();
                  }
                  
            } catch (Exception e) {
                  e.printStackTrace();
                  log.error(e);
            }
            return id_org;
      }
      
      String getOrgAccess(String strOrgId) {
            String ids_org = "";
            try {
                  
                  if (strOrgId.equalsIgnoreCase("") != true) {
                        final CallStoredProcedure svfpa_getorgparent = new CallStoredProcedure();
                        
                        List<Object> Parametros = new ArrayList<Object>();
                        Parametros.add(strOrgId);
                        
                        ids_org = svfpa_getorgparent.call("svfpa_getorgparent", Parametros, null,
                                    false).toString();
                  }
            } catch (Exception e) {
                  e.printStackTrace();
                  log.error(e);
            }
            return ids_org;
      }
      
      OBQuery<Organization> getOrgToCompare(Warehouse regStockLocation) {
            
            try {
                  
                  String strOrgParent = getOrgParent(SalesOrderGlobal.getOrganization().getId());
                  String strOrgAccessIds = getOrgAccess(strOrgParent);
                  String IDs = "'" + regStockLocation.getSvfpaOrgsource().getId() + "'";
                  
                  final StringBuilder queryExtend = new StringBuilder();
                  queryExtend.append(" AS org");
                  queryExtend.append(" WHERE ");
                  queryExtend.append(" (org.id IN (" + strOrgAccessIds + "))");
                  queryExtend.append(" AND (org.id IN (" + IDs + "))");
                  // System.out.println("----------------------------------------------------");
                  // System.out.println(queryExtend.toString());
                  OBQuery<Organization> obq = OBDal.getInstance().createQuery(Organization.class,
                              queryExtend.toString());
                  
                  return obq;
            } catch (Exception e) {
                  e.printStackTrace();
                  log.error(e.getMessage());
            }
            return null;
      }
      
      private void setListAvailability() {
            
            OBCriteria<StorageDetail> critStorageDetail = OBDal.getInstance().createCriteria(
                        StorageDetail.class);
            Organization[] orgs = { OrgServiceOrder,
                        OBDal.getInstance().get(Organization.class, "0") };
            critStorageDetail.add(Restrictions.in(StorageDetail.PROPERTY_ORGANIZATION, orgs));
            critStorageDetail.addOrderBy(StorageDetail.PROPERTY_PRODUCT, true);
            critStorageDetail.addOrderBy(StorageDetail.PROPERTY_STORAGEBIN, true);
            List<StorageDetail> ListStorageDetail = critStorageDetail.list();
            
            if (ListStorageDetail != null) {
                  
                  for (StorageDetail regStorageDetail : ListStorageDetail) {
                        StockAvailable regStockAvailable = new StockAvailable(OrgServiceOrder,
                                    regStorageDetail.getStorageBin().getWarehouse(),
                                    regStorageDetail.getStorageBin(),
                                    regStorageDetail.getProduct(), BigDecimal.ZERO,
                                    regStorageDetail.getQuantityOnHand());
                        ListStockAvailable.add(regStockAvailable);
                        
                  }
            }
            
      }
      
      public SSNSO_Partandlabor getAvailability(SSNSO_Partandlabor PL, long line) {
            Product product = PL.getProduct();
            BigDecimal qty = PL.getQuantityordered();
            SSNSO_Partandlabor PL_Resultante = null;
            BigDecimal qtyAvail = BigDecimal.ZERO;
            Integer Index = 0;
            PL.setLineNo(line);
            Boolean IsAvailability = false;
            regProcess += 1;
            
            while (Index < ListStockAvailable.size() - 1 && !IsAvailability) {
                  Index += 1;
                  if ((ListStockAvailable.get(Index).getProduct() == product)
                              && (ListStockAvailable.get(Index).getQtyOnhand().doubleValue() > 0)
                              && PL.getQuantityordered().compareTo(BigDecimal.ZERO) > 0) {
                        qtyAvail = ListStockAvailable.get(Index).getQtyOnhand();
                        IsAvailability = true;
                        if (qtyAvail.compareTo(qty) < 0 && !qtyAvail.equals(BigDecimal.ZERO)) {
                              // No alcanza lo suficiente "OUTOFSTOCK"
                              ListStockAvailable.get(Index).setQtyOnhand(BigDecimal.ZERO);
                              PL.setStocklocation(ListStockAvailable.get(Index).getRegWareHouse());
                              PL.setStorageBin(ListStockAvailable.get(Index).getRegLocator());
                              PL.setAction(null);
                              PL.setQuantityordered(qtyAvail);
                              PL.setStockavailability("INSTOCK");
                              regAdded += 1;
                              PL_Resultante = (SSNSO_Partandlabor) DalUtil.copy(PL, false);
                              PL_Resultante.setQuantityordered(qty.subtract(qtyAvail));
                              OBDal.getInstance().save(PL);
                        } else {
                              // Hay disponibilidad,
                              PL_Resultante = null;
                              qtyAvail = ListStockAvailable.get(Index).getQtyOnhand().subtract(qty);
                              ListStockAvailable.get(Index).setQtyOnhand(qtyAvail);
                              PL.setStocklocation(ListStockAvailable.get(Index).getRegWareHouse());
                              PL.setStorageBin(ListStockAvailable.get(Index).getRegLocator());
                              PL.setAction(getActionWhenisInStock(PL));
                              PL.setQuantityordered(qty);
                              PL.setStockavailability("INSTOCK");
                              regUpdated += 1;
                              OBDal.getInstance().save(PL);
                        }
                  }
            }
            if (!IsAvailability) {
                  PL_Resultante = null;
                  PL.setStocklocation(null);
                  PL.setStorageBin(null);
                  PL.setAction(null);
                  PL.setStockavailability("OUTOFSTOCK");
                  regUpdated += 1;
                  OBDal.getInstance().save(PL);
            }
            return PL_Resultante;
      }
      
      public String CheckAvailabilityProcess(ServiceOrder serviceOrder) {
            try {
                  
                  OBContext.setAdminMode(true);
                  try {
                        
                        if (serviceOrder.getOrganization().getOrganizationType().equals("1") == true) {
                              TYPE_SOURCE_VALUE = serviceOrder.getOrganization().getSearchKey();
                        } else {
                              
                              Organization regOrg = OBDal.getInstance().get(Organization.class,
                                          getOrgParent(serviceOrder.getOrganization().getId()));
                              
                              if (regOrg != null) {
                                    if (regOrg.getOrganizationType().equals("1") == true) {
                                          TYPE_SOURCE_VALUE = regOrg.getSearchKey();
                                    }
                              }
                        }
                        
                        ServiceOrderGlobal = serviceOrder;
                        OrgServiceOrder = serviceOrder.getOrganization();
                        // WareSalesOrder = serviceOrder.getWarehouse();
                        
                        PlaborLine = new ArrayList<SSNSO_Partandlabor>();
                        ListOrderLineNewest = new ArrayList<OrderLine>();
                        
                        long line = 10;
                        
                        List<SSNSO_Ticket> Tickets = serviceOrder.getSSNSOTICKETList();
                        List<SSNSO_Partandlabor> PLabors = null;
                        
                        setListAvailability();
                        SSNSO_Partandlabor AuxPL = new SSNSO_Partandlabor();
                        Boolean contin = true;
                        for (int i = 0; i < Tickets.size(); i++) {
                              // for (SSNSO_Ticket iterTickets : Tickets) {
                              PLabors = Tickets.get(i).getSSNSOPARTANDLABORList();
                              line = 10;
                              for (int j = 0; j < PLabors.size(); j++) {
                                    AuxPL = getAvailability(PLabors.get(j), line);
                                    contin = AuxPL != null;
                                    while (contin) {
                                          line = line + 10;
                                          AuxPL = getAvailability(AuxPL, line);
                                          contin = AuxPL != null;
                                    }
                                    line = line + 10;
                                    // Borrar las Partlabors Viejas
                                    // OBDal.getInstance().remove(PLabors.get(j));
                              }
                        }
                        OBDal.getInstance().flush();
                        OBDal.getInstance().commitAndClose();
                        
                        String msg = "Total processed: " + regProcess + ", row added: " + regAdded
                                    + ", row updated: " + regUpdated;
                        return msg;
                  } catch (Exception e) {
                        e.printStackTrace();
                        log.error(e);
                  }
            } finally {
                  OBContext.restorePreviousMode();
            }
            return null;
      }
      
}
