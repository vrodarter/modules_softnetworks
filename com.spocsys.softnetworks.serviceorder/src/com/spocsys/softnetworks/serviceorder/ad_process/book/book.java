package com.spocsys.softnetworks.serviceorder.ad_process.book;
import java.util.Map;

import org.apache.log4j.Logger;
import java.util.Date;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;

import com.spocsys.softnetworks.serviceorder.ServiceOrder;

public class book extends BaseProcessActionHandler{
  private static final Logger log= Logger.getLogger(book.class);
  protected JSONObject doExecute(Map<String, Object> parameters, String content){
    try{
      JSONObject request= new JSONObject (content);
       
        String SO_id= request.getString("Ssnso_Serviceorder_ID");
        
        final ServiceOrder headSO=OBDal.getInstance().get(ServiceOrder.class, SO_id);
        headSO.setDocumentStatus("BK");
        headSO.setCompleted(new Date());
        OBDal.getInstance().save(headSO);
        OBDal.getInstance().flush();
        OBDal.getInstance().commitAndClose();
      return request; 
    }catch (Exception e){
      return new JSONObject();
    }
  }
}