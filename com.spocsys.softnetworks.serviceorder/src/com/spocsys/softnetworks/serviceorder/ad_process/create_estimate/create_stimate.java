package com.spocsys.softnetworks.serviceorder.ad_process.create_estimate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.utility.OBObjectFieldProvider;

import java.util.Date;


import com.spocsys.softnetworks.serviceorder.ServiceOrder;
import com.spocsys.softnetworks.serviceorder.ServiceOrderLine;

public class create_stimate extends BaseProcessActionHandler{
  private static final Logger log= Logger.getLogger(create_stimate.class);
  protected JSONObject doExecute(Map<String, Object> parameters, String content){
    try{
     JSONObject request= new JSONObject (content);
     String id= request.getString("Ssnso_Serviceorder_ID");
     JSONObject msg= new JSONObject();
     
    /// put the message if it's success /////// 
     if (Managesorder(id)){
       msg.put("msgType", "success");
       msg.put("msgTitle", "Create Quotation");
       msg.put("msgText", "A new quotation has been created from the  selected service order.");
       JSONObject msgInBPTabAction = new JSONObject();
       msgInBPTabAction.put("showMsgInView", msg);
       request.put("responseActions", msgInBPTabAction);
       return request;
     }else{
       msg.put("msgType", "error");
       msg.put("msgTitle", "The creation of the new quotation failed;");
       msg.put("msgText", "please, check the the service order data");
       JSONObject msgInBPTabAction = new JSONObject();
       msgInBPTabAction.put("showMsgInView", msg);
       request.put("responseActions", msgInBPTabAction);
       return request;
     }
     
     
    }catch(Exception e){
      log.error(e.getMessage());
    }
    return new JSONObject();
  }
  ///// the main method where all the magic is done ///////
  public boolean Managesorder(String id){
     try{
       OBContext.setAdminMode(true);
       //// took the id and get the header of serviceorderline table
      final ServiceOrder headSO = OBDal.getInstance().get(ServiceOrder.class, id);
      //// with document type where the name = quotation take an object to determine that it's a quotation
      final OBCriteria <DocumentType> doclist=OBDal.getInstance().createCriteria(DocumentType.class);
      doclist.add(Restrictions.eq(DocumentType.PROPERTY_CLIENT,OBContext.getOBContext().getCurrentClient() ));
      doclist.add(Restrictions.eq(DocumentType.PROPERTY_NAME, "Quotation"));
      ////// call the fucntion that create Order object with the info of the header of serviceorder/////////////////
         Order order= createcorder(doclist.list().get(0), headSO);
      /////   create a list with all the lines in ServieOrderLine where is equal to the header////////////
       final OBCriteria <ServiceOrderLine> SOlines = OBDal.getInstance().createCriteria(ServiceOrderLine.class);
             SOlines.add(Restrictions.eq(ServiceOrderLine.PROPERTY_SSNSOSERVICEORDER, headSO));
             
      if (order!=null){
        /// create a list where its going to be the lines OrderLine
        List <OrderLine> OLlist= new ArrayList<OrderLine>();
        
        ///BigDecimal b=new BigDecimal(0);
         for (ServiceOrderLine listline: SOlines.list()){
           /// call the function that create OrderLine Object and fill the list////
           
           OLlist.add(createorderline(listline, order));
           
          
         }
         //// set the lines to the header Order //////////////
         order.setOrderLineList(OLlist);
         
        //// save it and flush it to the database 
         OBDal.getInstance().save(order);
          OBDal.getInstance().flush();
          OBDal.getInstance().commitAndClose();
          return true;
          
      }
     }catch(Exception e){
       log.error(e.getMessage());
       
     }finally{
       OBContext.restorePreviousMode();
     }
    return false;
  }// end managesorder
  
 /////// function where creates a OrderLine object ////////////////// 
   public OrderLine createorderline(ServiceOrderLine SOline, Order headorder){
     try{
       OrderLine Oline= OBProvider.getInstance().get(OrderLine.class);
       
       ////////////Mandatory info//////
       Oline.setClient(SOline.getClient());
       Oline.setOrganization(SOline.getOrganization());
       Oline.setCreatedBy(OBContext.getOBContext().getUser());
       Oline.setUpdatedBy(OBContext.getOBContext().getUser());
       //////////////////////////////
       
       //////////////////////
       Oline.setDescription(headorder.getDescription());
       Oline.setSvfpaShipFrom(OBContext.getOBContext().getWarehouse());
       Oline.setSvfpaStockLocation(OBContext.getOBContext().getWarehouse());
       Oline.setSalesOrder(headorder);
       Oline.setWarehouse(SOline.getWarehouse());
       Oline.setUOM(SOline.getUOM());
       Oline.setCurrency(headorder.getCurrency());
       Oline.setTax(SOline.getTax());
       Oline.setLineNo(SOline.getLineNo());
       Oline.setOrderDate(headorder.getOrderDate());
       Oline.setProduct(SOline.getProduct());
       Oline.setUnitPrice(SOline.getNETUnitPrice());
       ///////////////////
       Oline.setNewOBObject(true);
       return Oline;
     }catch(Exception e){
       log.error(e.getMessage());
     }
     return null;
   }// createorderline
   
  /////// function where create a Order Object/////////
  public Order createcorder(DocumentType doc, ServiceOrder headSO){
    try{ 
    Order COrder= OBProvider.getInstance().get(Order.class);
      COrder.setNewOBObject(true);
      
      /// mandatory info ////
      COrder.setClient(headSO.getClient());
      COrder.setOrganization(headSO.getOrganization());
      COrder.setCreatedBy(OBContext.getOBContext().getUser());
      COrder.setUpdatedBy(OBContext.getOBContext().getUser());
      ///////////////////////////////
      COrder.setDocumentType(doc);
      COrder.setOrderDate(new Date());
      COrder.setBusinessPartner(headSO.getClientid());
      
      COrder.setDescription(headSO.getWorkDescription());
      COrder.setSalesTransaction(true);
      COrder.setSvfpaStockLocation(OBContext.getOBContext().getWarehouse());
      COrder.setSvfpaShipFrom(OBContext.getOBContext().getWarehouse());
      COrder.setWarehouse(OBContext.getOBContext().getWarehouse());
      COrder.setPartnerAddress(headSO.getShiptoLoc());
      COrder.setPriceList(headSO.getClientid().getPriceList());
      COrder.setCurrency(headSO.getClientid().getCurrency());
      COrder.setPaymentTerms(headSO.getClientid().getPaymentTerms());
      COrder.setTransactionDocument(doc);
      COrder.setInvoiceAddress(headSO.getShiptoLoc());
      COrder.setDocumentStatus("DR");
      COrder.setDocumentAction("PR");
      COrder.setOrderDate(new Date());
      COrder.setAccountingDate(new Date());
      COrder.setFormOfPayment("B");
      COrder.setInvoiceTerms("O");
      COrder.setDeliveryTerms("R");
      COrder.setFreightCostRule("F");
      COrder.setDeliveryMethod("P");
      COrder.setPriority("5");
      return COrder;
      /////
    }catch(Exception e){
      log.error(e.getMessage());
    }
     return null;
  }//end create order
  
}