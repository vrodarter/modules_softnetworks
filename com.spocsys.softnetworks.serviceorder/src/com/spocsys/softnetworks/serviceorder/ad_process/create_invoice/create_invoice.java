package com.spocsys.softnetworks.serviceorder.ad_process.create_invoice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.pricing.pricelist.PriceList;

import com.spocsys.softnetworks.serviceorder.SSNSO_Partandlabor;
import com.spocsys.softnetworks.serviceorder.SSNSO_Ticket;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;

public class create_invoice extends BaseProcessActionHandler {
      private static final Logger log = Logger.getLogger(create_invoice.class);
      
      protected JSONObject doExecute(Map<String, Object> parameters, String content) {
            try {
                  
                  JSONObject request = new JSONObject(content);
                  JSONObject msg = new JSONObject();
                  JSONObject params = request.getJSONObject("_params");
                  Boolean GROSSPRICE = params.has("GROSSPRICE");
                  
                  String SO_id = request.getString("Ssnso_Serviceorder_ID");
                  ServiceOrder headSO = OBDal.getInstance().get(ServiceOrder.class, SO_id);
                  
                  String mI = manageInvoice(headSO, GROSSPRICE);
                  
                  // / put the message if it's success ///////
                  if (!mI.contains("Error")) {
                        msg.put("msgType", "success");
                        msg.put("msgTitle", "Create Invoice Succesfull");
                        msg.put("msgText", mI);
                        JSONObject msgInBPTabAction = new JSONObject();
                        msgInBPTabAction.put("showMsgInView", msg);
                        request.put("responseActions", msgInBPTabAction);
                  } else {
                        msg.put("msgType", "error");
                        msg.put("msgTitle", "The creation of the new Sales Invoice failed;");
                        msg.put("msgText", mI);
                        JSONObject msgInBPTabAction = new JSONObject();
                        msgInBPTabAction.put("showMsgInView", msg);
                        request.put("responseActions", msgInBPTabAction);
                  }
                  
                  return request;
            } catch (Exception e) {
                  System.out.println("main void error: " + e.getMessage());
                  log.error(e.getMessage());
                  return new JSONObject();
            }
      }
      
      public String manageInvoice(ServiceOrder headSO, Boolean gROSSPRICE) {
            String mssage = "";
            PriceList pL = null;
            DocumentType doctype = null;
            
            try {
                  
                  // Validando la tarifa de precios m_pricelist
                  
                  if (headSO.getSalesOrder() == null)
                        pL = headSO.getClientid().getPriceList();
                  else
                        pL = headSO.getSalesOrder().getPriceList();
                  
                  if (pL == null) {
                        OBCriteria<PriceList> pricelist = OBDal.getInstance().createCriteria(
                                    PriceList.class);
                        pricelist.add(Restrictions.eq(PriceList.PROPERTY_SALESPRICELIST, true));
                        pricelist.add(Restrictions.eq(PriceList.PROPERTY_DEFAULT, true));
                        pricelist.setMaxResults(1);
                        pL = (PriceList) pricelist.uniqueResult();
                  }
                  if (pL == null) {
                        mssage = "Error. Cannot be generated the Sales Invoice due to Pricelist default not founded.";
                  }
                  OBCriteria<Table> tablelist = OBDal.getInstance().createCriteria(Table.class);
                  tablelist.add(Restrictions.eq(Table.PROPERTY_DBTABLENAME, "C_Invoice"));
                  tablelist.setMaxResults(1);
                  Table tableInvoice = (Table) tablelist.uniqueResult();
                  
                  OBCriteria<DocumentType> dTlist = OBDal.getInstance().createCriteria(
                              DocumentType.class);
                  // dTlist.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY,
                  // "D8EBFF2A790C458B997DF8EEA23C6A16"));
                  dTlist.add(Restrictions.eq(DocumentType.PROPERTY_TABLE, tableInvoice));
                  dTlist.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, true));
                  dTlist.add(Restrictions.eq(DocumentType.PROPERTY_DEFAULT, true));
                  dTlist.setMaxResults(1);
                  doctype = (DocumentType) dTlist.uniqueResult();
                  if (doctype == null) {
                        mssage = "Error. Document type default for Sales Invoice not founded.";
                  }
                  
                  if (mssage == "") {
                        Invoice newInvoice = getInvoice(headSO, pL, doctype);
                        
                        List<InvoiceLine> newinline = new ArrayList<InvoiceLine>();
                        List<SSNSO_Ticket> Tickets = headSO.getSSNSOTICKETList();
                        List<SSNSO_Partandlabor> PLabors = null;
                        long line = 10;
                        
                        for (SSNSO_Ticket iterTickets : Tickets) {
                              PLabors = iterTickets.getSSNSOPARTANDLABORList();
                              for (SSNSO_Partandlabor iterPLabors : PLabors) {
                                    if (iterPLabors.isBillable()) {
                                          newinline.add(getInvoiceLines(newInvoice, iterPLabors,
                                                      line, pL.isPriceIncludesTax(), gROSSPRICE));
                                          line = line + 10;
                                    }
                              }
                        }
                        headSO.setInvoice(newInvoice);
                        newInvoice.setInvoiceLineList(newinline);
                        OBDal.getInstance().save(newInvoice);
                        OBDal.getInstance().save(headSO);
                        OBDal.getInstance().flush();
                        OBDal.getInstance().commitAndClose();
                        mssage = "Sales Invoice created No: " + newInvoice.getDocumentNo();
                  }
            } catch (Exception e) {
                  mssage = "Error. The Sale Invoice could not be generated. Check the Service Order data.";
                  System.out.println("mangeInvoce erro: " + e.getMessage());
            }
            return mssage;
      }
      
      // //////// create the header of the Invoice /////
      public Invoice getInvoice(ServiceOrder headSO, PriceList pL, DocumentType doctype) {
            PaymentTerm pT = null;
            
            try {
                  Invoice headIN = OBProvider.getInstance().get(Invoice.class);
                  headIN.setNewOBObject(true);
                  // /////Mandatory Info ////
                  headIN.setClient(headSO.getClient());
                  headIN.setOrganization(headSO.getOrganization());
                  headIN.setCreatedBy(OBContext.getOBContext().getUser());
                  headIN.setUpdatedBy(OBContext.getOBContext().getUser());
                  headIN.setPriceList(pL);
                  headIN.setCurrency(pL.getCurrency());
                  // ////////////////////////
                  headIN.setDocumentNo(FIN_Utility.getDocumentNo(doctype, "C_Invoice", true));
                  headIN.setDocumentStatus("DR");
                  headIN.setDocumentType(doctype);
                  if (headSO.isIncludeWd()) {
                        headIN.setSsnsoWorkDescription(headSO.getWorkDescription());
                  }
                  if (headSO.isIncludeWp()) {
                        headIN.setSsnsoWorkDescription(headSO.getWorkPerformed());
                  }
                  headIN.setTransactionDocument(doctype);
                  headIN.setInvoiceDate(new Date());
                  headIN.setAccountingDate(new Date());
                  headIN.setBusinessPartner(headSO.getClientid());
                  headIN.setAccountingDate(new Date());
                  headIN.setPartnerAddress(headSO.getShiptoLoc());
                  headIN.setFormOfPayment("P");
                  if (headSO.getSalesOrder() == null)
                        pT = headSO.getClientid().getPaymentTerms();
                  else
                        pT = headSO.getSalesOrder().getPaymentTerms();
                  
                  if (pT == null) {
                        OBCriteria<PaymentTerm> pTermnlist = OBDal.getInstance().createCriteria(
                                    PaymentTerm.class);
                        pTermnlist.add(Restrictions.eq(PriceList.PROPERTY_DEFAULT, true));
                        pTermnlist.setMaxResults(1);
                        pT = (PaymentTerm) pTermnlist.uniqueResult();
                  }
                  headIN.setPaymentTerms(pT);
                  // /////////
                  return headIN;
            } catch (Exception e) {
                  System.out.println("getInvoice error: " + e.getMessage());
                  return null;
            }
      }// / end getInvoice
      
      // / create a single line of invoice line to feed the array list/////
      public InvoiceLine getInvoiceLines(Invoice headIN, SSNSO_Partandlabor iterPLabors, long line,
                  Boolean isTaxIncludedPriceList, Boolean gROSSPRICE) {
            try {
                  
                  InvoiceLine inline = OBProvider.getInstance().get(InvoiceLine.class);
                  
                  BigDecimal QTY = iterPLabors.getQuantityordered();
                  inline.setNewOBObject(true);
                  // // mandatory info////
                  inline.setClient(headIN.getClient());
                  inline.setOrganization(headIN.getOrganization());
                  inline.setCreatedBy(OBContext.getOBContext().getUser());
                  inline.setUpdatedBy(OBContext.getOBContext().getUser());
                  inline.setInvoice(headIN);
                  // ///////////////////////////////
                  inline.setProduct(iterPLabors.getProduct());
                  inline.setInvoicedQuantity(QTY);
                  inline.setUOM(iterPLabors.getUOM());
                  inline.setTax(iterPLabors.getTax());
                  inline.setDescription(iterPLabors.getDescription());
                  inline.setLineNo(line);
                  // Cálculo de los Precios según la tarifa recibida
                  
                  BigDecimal TaxAmt = BigDecimal.ZERO;
                  BigDecimal LineAmt = BigDecimal.ZERO;
                  BigDecimal PriceCalculado = BigDecimal.ZERO;
                  BigDecimal rate = iterPLabors.getTax().getRate();
                  
                  if (gROSSPRICE == isTaxIncludedPriceList) {
                        inline.setGrossListPrice(getNotNull(iterPLabors.getGrossListPrice()));
                        inline.setUnitPrice(getNotNull(iterPLabors.getPriceunit()));
                        inline.setListPrice(getNotNull(iterPLabors.getListPrice()));
                        inline.setLineNetAmount(getNotNull(iterPLabors.getLineNetAmount()));
                        inline.setGrossAmount(getNotNull(iterPLabors.getLineGrossAmount()));
                        inline.setGrossUnitPrice(getNotNull(iterPLabors.getGrossListPrice()));
                  } else {
                        if (!gROSSPRICE && isTaxIncludedPriceList) {
                              // Si Service Order No IncluyeTax pero la Pricelist de la
                              // Invoice SI
                              TaxAmt = iterPLabors.getLineNetAmount().multiply(rate);
                              LineAmt = iterPLabors.getLineNetAmount().add(TaxAmt);
                              PriceCalculado = LineAmt.divide(QTY);
                              inline.setGrossUnitPrice(PriceCalculado);
                              inline.setGrossAmount(PriceCalculado.multiply(QTY));
                              inline.setGrossListPrice(getNotNull(iterPLabors.getGrossListPrice()));
                              inline.setUnitPrice(BigDecimal.ZERO);
                              inline.setListPrice(BigDecimal.ZERO);
                              inline.setLineNetAmount(BigDecimal.ZERO);
                        } else { // Si Service Order IncluyeTax pero la Pricelist de la
                                 // Invoice NO
                              TaxAmt = iterPLabors.getLineGrossAmount().multiply(rate);
                              LineAmt = iterPLabors.getLineGrossAmount().add(TaxAmt);
                              PriceCalculado = LineAmt.divide(QTY);
                              inline.setUnitPrice(PriceCalculado);
                              inline.setLineNetAmount(PriceCalculado.multiply(QTY));
                              inline.setListPrice(getNotNull(iterPLabors.getListPrice()));
                              inline.setGrossListPrice(BigDecimal.ZERO);
                              inline.setGrossAmount(BigDecimal.ZERO);
                              inline.setGrossUnitPrice(BigDecimal.ZERO);
                        }
                  }
                  
                  return inline;
            } catch (Exception e) {
                  System.out.println("getInvoiceLine error: " + e.getMessage());
                  return null;
            }
      }
      
      private BigDecimal getNotNull(BigDecimal priceValid) {
            // TODO Auto-generated method stub
            BigDecimal price = BigDecimal.ZERO;
            if (priceValid != null)
                  price = priceValid;
            return price;
      }
}
