/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2014 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.spocsys.softnetworks.serviceorder.ad_process.createservices;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.softnetworks.serviceorder.SSNSO_Service;

public class CreateServices extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  private static final BigDecimal ZERO = BigDecimal.ZERO;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");
      String strKey = vars.getStringParameter("inpssnsoServiceId");
      printPage(response, vars, strKey, strWindow, strTab, strProcessId);
    } else if (vars.commandIn("PROCESS")) {
      String strWindow = vars.getStringParameter("inpwindowId");
      String strOrder = vars.getStringParameter("inpcOrderId");
      String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|C_Order_ID");
      String strTab = vars.getStringParameter("inpTabId");

      String strWindowPath = Utility.getTabURL(strTab, "R", true);
      if (strWindowPath.equals("")) {
        strWindowPath = strDefaultServlet;
      }

      printPageServiceRelationGrid(response, vars, strKey, strWindow, strTab);

      // OBError myError = processButton(vars, strKey, strOrder);
      // log4j.debug(myError.getMessage());
      // vars.setMessage(strTab, myError);
      // printPageClosePopUp(response, vars, strWindowPath);
    } else
      pageErrorPopUp(response);
  }

  private OBError processButton(VariablesSecureApp vars, String strKey, String strOrder) {
    OBError myError = null;
    myError = new OBError();
    myError.setType("Success");
    myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
    myError.setMessage(Utility.messageBD(this, "RecordsCopied", vars.getLanguage()));

    return myError;
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId) throws IOException, ServletException {
    log4j.debug("Output: Button process Create services");

    String[] discard = { "" };
    if ("".equals("")) {
      discard[0] = "helpDiscard";
    }

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/spocsys/softnetworks/serviceorder/ad_process/createservices/CreateServices", discard)
        .createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("tab", strTab);
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("question",
        Utility.messageBD(this, "StartProcess?", vars.getLanguage()));
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("description", "Descripción del proceso");
    xmlDocument.setParameter("help", "Help process");

    SSNSO_Service service = OBDal.getInstance().get(SSNSO_Service.class, strKey);
    String customerId = (service.getBusinessPartner() != null)
        ? service.getBusinessPartner().getId() : null;
    String salesorderId = (service.getSalesOrder() != null) ? service.getSalesOrder().getId()
        : null;
    String serviceorderId = (service.getSsnsoServiceorder() != null)
        ? service.getSsnsoServiceorder().getId() : null;
    String contract = service.getServicecontract();

    xmlDocument.setParameter("customerId", customerId);
    xmlDocument.setParameter("salesorderId", salesorderId);
    xmlDocument.setParameter("serviceorderId", serviceorderId);
    xmlDocument.setParameter("contract", contract);

    // Customer
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "C_BPartner_ID",
          "", "", Utility.getContext(this, vars, "#AccessibleOrgTree", "CreateServices"),
          Utility.getContext(this, vars, "#User_Client", "CreateServices"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "CreateServices", customerId);
      xmlDocument.setData("reportCUSTOMER", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    // Sales order
    try {
      OBContext.setAdminMode();
      Vector<FieldsData> vFD = new Vector<FieldsData>();
      OBCriteria<Order> critOrder = OBDal.getInstance().createCriteria(Order.class);
      critOrder.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true));
      List<Order> lSO = critOrder.list();
      for (Order order : lSO) {
        FieldsData data = new FieldsData();
        data.id = order.getId();
        data.name = order.getIdentifier();
        vFD.add(data);
      }
      FieldsData[] aFD = new FieldsData[vFD.size()];
      vFD.copyInto(aFD);
      xmlDocument.setData("reportSALESORDER", "liststructure", aFD);

    } catch (Exception ex) {
      throw new ServletException(ex);
    } finally {
      OBContext.restorePreviousMode();
    }
    // Service order
    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
          "Ssnso_Serviceorder_ID", "", "",
          Utility.getContext(this, vars, "#AccessibleOrgTree", "CreateServices"),
          Utility.getContext(this, vars, "#User_Client", "CreateServices"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "CreateServices", customerId);
      xmlDocument.setData("reportSERVICEORDER", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    // Contract

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageServiceRelationGrid(HttpServletResponse response, VariablesSecureApp vars,
      String strKey, String windowId, String strTab) throws IOException, ServletException {
    log4j.debug("Output: Button process Create services");

    String[] discard = { "" };
    if ("".equals("")) {
      discard[0] = "helpDiscard";
    }

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "com/spocsys/softnetworks/serviceorder/ad_process/createservices/ServiceRelation", discard)
        .createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("tab", strTab);
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("question",
        Utility.messageBD(this, "StartProcess?", vars.getLanguage()));
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("description", "Descripción del proceso");
    xmlDocument.setParameter("help", "Help process");

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  public String getServletInfo() {
    return "Servlet Copy from order";
  } // end of the getServletInfo() method
}
