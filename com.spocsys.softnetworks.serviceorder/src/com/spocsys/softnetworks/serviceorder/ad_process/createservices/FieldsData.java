//Sqlc generated V1.O00-1
package com.spocsys.softnetworks.serviceorder.ad_process.createservices;

import org.apache.log4j.Logger;
import org.openbravo.data.FieldProvider;

public class FieldsData implements FieldProvider {
  static Logger log4j = Logger.getLogger(FieldsData.class);
  private String InitRecordNumber = "0";
  public String name;
  public String id;
  public String description;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else {
      log4j.debug("Field does not exist: " + fieldName);
      return null;
    }
  }

}