package com.spocsys.softnetworks.serviceorder.ad_process.fillservices;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.service.db.DbUtility;

import com.spocsys.softnetworks.serviceorder.SSNSO_Service;
import com.spocsys.softnetworks.serviceorder.SSNSO_Ticket;
import com.spocsys.softnetworks.serviceorder.SSNSO_Ticketassignitem;

/**
 * @author AGES team
 * 
 */
public class FillServicesToAssignedItems extends BaseProcessActionHandler {
      
      private static final Logger log         = Logger.getLogger(FillServicesToAssignedItems.class);
      private static Exception    MyException = new OBException();
      
      @Override
      protected JSONObject doExecute(Map<String, Object> parameters, String content) {
            OBContext.setAdminMode(true);
            int count = 0;
            JSONObject request = null;
            
            try {
                  request = new JSONObject(content);
                  JSONObject params = request.getJSONObject("_params");
                  JSONObject message = params.getJSONObject("ssnso_service_id");
                  String TicketID = request.getString("inpssnsoTicketId");
                  SSNSO_Ticket Ticket = OBDal.getInstance().get(SSNSO_Ticket.class, TicketID);
                  JSONArray selection = new JSONArray(
                              message.getString(ApplicationConstants.SELECTION_PROPERTY));
                  // Buscando el último Line
                  OBCriteria<SSNSO_Ticketassignitem> ASI_List = OBDal.getInstance().createCriteria(
                              SSNSO_Ticketassignitem.class);
                  ASI_List.add(Restrictions.eq(SSNSO_Ticketassignitem.PROPERTY_SSNSOTICKET, Ticket));
                  ASI_List.addOrderBy("lineNo", false);
                  ASI_List.setMaxResults(1);
                  Long Line = (long) 0;
                  if (((SSNSO_Ticketassignitem) ASI_List.uniqueResult()) != null) {
                        Line = ((SSNSO_Ticketassignitem) ASI_List.uniqueResult()).getLineNo();
                  }
                  // Poniendo los services selectos en Assigned Items si no están ya puestos
                  for (int i = 0; i < selection.length(); i++) {
                        if (!isAssignedItems_inList(selection.getJSONObject(i), Ticket)) {
                              count += 1;
                              Line = Line + (long) 10;
                              FillService(selection.getJSONObject(i), Ticket, Line);
                        }
                  }
                  
                  OBDal.getInstance().flush();
                  OBDal.getInstance().commitAndClose();
                  String messageText = OBMessageUtils.messageBD("Sucess. " + count
                              + " services was added to Assigned Items.");
                  JSONObject msg = new JSONObject();
                  if (count == 0)
                        msg.put("severity", "info");
                  else
                        msg.put("severity", "success");
                  msg.put("text", messageText);
                  request.put("message", msg);
                  
                  // return request;
            } catch (Exception e) {
                  log.error("Error processing request: " + e.getMessage(), e);
                  
                  try {
                        OBDal.getInstance().rollbackAndClose();
                        request = new JSONObject();
                        Throwable ex = DbUtility.getUnderlyingSQLException(e);
                        String message = OBMessageUtils.translateError(ex.getMessage())
                                    .getMessage();
                        JSONObject errorMessage = new JSONObject();
                        errorMessage.put("severity", "error");
                        errorMessage.put("text", message);
                        request.put("message", errorMessage);
                  } catch (Exception e2) {
                        log.error(e.getMessage(), e2);
                  }
            } finally {
                  OBContext.restorePreviousMode();
            }
            return request;
      }
      
      private boolean isAssignedItems_inList(JSONObject selection, SSNSO_Ticket ticket)
                  throws JSONException {
            Boolean exist = false;
            
            OBCriteria<SSNSO_Ticketassignitem> ASI_List = OBDal.getInstance().createCriteria(
                        SSNSO_Ticketassignitem.class);
            String ServiceID = selection.getString("id");
            SSNSO_Service Service = OBDal.getInstance().get(SSNSO_Service.class, ServiceID);
            ASI_List.add(Restrictions.eq(SSNSO_Ticketassignitem.PROPERTY_SERVICE, Service));
            ASI_List.add(Restrictions.eq(SSNSO_Ticketassignitem.PROPERTY_SSNSOTICKET, ticket));
            ASI_List.setMaxResults(1);
            if (ASI_List.uniqueResult() != null) {
                  exist = true;
            }
            
            return exist;
      }
      
      private void FillService(JSONObject selection, SSNSO_Ticket ticket, Long line)
                  throws Exception {
            
            String ServiceID = selection.getString("service");
            SSNSO_Service Service = OBDal.getInstance().get(SSNSO_Service.class, ServiceID);
            SSNSO_Ticketassignitem AssignedItem = OBProvider.getInstance().get(
                        SSNSO_Ticketassignitem.class);
            AssignedItem.setLineNo(line);
            AssignedItem.setService(Service);
            AssignedItem.setSsnsoTicket(ticket);
            if (ticket.getOrganization().getSsnsoSowarehouse() == null) {
                  MyException = new Exception(
                              "Error. The field warehouse in the Organization of this Service Order are empty.");
                  throw MyException;
            } else
                  AssignedItem.setWarehouse(ticket.getOrganization().getSsnsoSowarehouse());
            // Buscando el uom por defecto
            OBCriteria<UOM> UOM_List = OBDal.getInstance().createCriteria(UOM.class);
            UOM_List.add(Restrictions.eq(UOM.PROPERTY_DEFAULT, true));
            UOM_List.setMaxResults(1);
            AssignedItem.setUOM((UOM) UOM_List.uniqueResult());
            AssignedItem.setExpirationdate(new Date());
            OBDal.getInstance().save(AssignedItem);
            
      }
}