package com.spocsys.softnetworks.serviceorder.ad_process.fillservices;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.service.db.DbUtility;

import com.spocsys.softnetworks.serviceorder.SSNSO_Service;
import com.spocsys.softnetworks.serviceorder.SSNSO_itemstoworkon;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;

/**
 * @author AGES Team
 * 
 */
public class FillServicesToItemsToWorkOn extends BaseProcessActionHandler {
      
      private static final Logger log = Logger.getLogger(FillServicesToItemsToWorkOn.class);
      
      @Override
      protected JSONObject doExecute(Map<String, Object> parameters, String content) {
            OBContext.setAdminMode(true);
            int count = 0;
            JSONObject request = null;
            try {
                  request = new JSONObject(content);
                  JSONObject params = request.getJSONObject("_params");
                  JSONObject message = params.getJSONObject("ssnso_service_id");
                  String ServiceorderID = request.getString("inpssnsoServiceorderId");
                  ServiceOrder ServiceOrder = OBDal.getInstance().get(ServiceOrder.class,
                              ServiceorderID);
                  JSONArray selection = new JSONArray(
                              message.getString(ApplicationConstants.SELECTION_PROPERTY));
                  // Buscando el último Line
                  OBCriteria<SSNSO_itemstoworkon> ITW_List = OBDal.getInstance().createCriteria(
                              SSNSO_itemstoworkon.class);
                  ITW_List.add(Restrictions.eq(SSNSO_itemstoworkon.PROPERTY_SERVICEORDER,
                              ServiceOrder));
                  ITW_List.addOrderBy("lineNo", false);
                  ITW_List.setMaxResults(1);
                  Long Line = (long) 0;
                  if (((SSNSO_itemstoworkon) ITW_List.uniqueResult()) != null) {
                        Line = ((SSNSO_itemstoworkon) ITW_List.uniqueResult()).getLineNo();
                  }
                  // Poniendo los services selectos en Items to Work On si no están ya puestos
                  for (int i = 0; i < selection.length(); i++) {
                        if (!isIworkOn_inList(selection.getJSONObject(i), ServiceOrder)) {
                              count += 1;
                              Line = Line + (long) 10;
                              FillService(selection.getJSONObject(i), ServiceOrder, Line);
                        }
                  }
                  
                  OBDal.getInstance().flush();
                  OBDal.getInstance().commitAndClose();
                  
                  String messageText = OBMessageUtils.messageBD("Sucess. " + count
                              + " services was added to Items to Work on.");
                  
                  JSONObject msg = new JSONObject();
                  if (count == 0)
                        msg.put("severity", "info");
                  else
                        msg.put("severity", "success");
                  msg.put("text", messageText);
                  request.put("message", msg);
                  
                  // return request;
            } catch (Exception e) {
                  log.error("Error processing request: " + e.getMessage(), e);
                  
                  try {
                        OBDal.getInstance().rollbackAndClose();
                        request = new JSONObject();
                        Throwable ex = DbUtility.getUnderlyingSQLException(e);
                        String message = OBMessageUtils.translateError(ex.getMessage())
                                    .getMessage();
                        JSONObject errorMessage = new JSONObject();
                        errorMessage.put("severity", "error");
                        errorMessage.put("text", message);
                        request.put("message", errorMessage);
                  } catch (Exception e2) {
                        log.error(e.getMessage(), e2);
                  }
            } finally {
                  OBContext.restorePreviousMode();
            }
            return request;
      }
      
      private boolean isIworkOn_inList(JSONObject selection, ServiceOrder serviceOrder)
                  throws JSONException {
            Boolean exist = false;
            
            OBCriteria<SSNSO_itemstoworkon> ITW_List = OBDal.getInstance().createCriteria(
                        SSNSO_itemstoworkon.class);
            String ServiceID = selection.getString("id");
            SSNSO_Service Service = OBDal.getInstance().get(SSNSO_Service.class, ServiceID);
            ITW_List.add(Restrictions.eq(SSNSO_itemstoworkon.PROPERTY_SERVICE, Service));
            ITW_List.add(Restrictions.eq(SSNSO_itemstoworkon.PROPERTY_SERVICEORDER, serviceOrder));
            ITW_List.setMaxResults(1);
            if (ITW_List.uniqueResult() != null) {
                  exist = true;
            }
            
            return exist;
      }
      
      private void FillService(JSONObject selection, ServiceOrder serviceOrder, Long line)
                  throws JSONException {
            
            String ServiceID = selection.getString("id");
            SSNSO_Service Service = OBDal.getInstance().get(SSNSO_Service.class, ServiceID);
            SSNSO_itemstoworkon ItemtWorkOn = OBProvider.getInstance().get(
                        SSNSO_itemstoworkon.class);
            ItemtWorkOn.setLineNo(line);
            ItemtWorkOn.setService(Service);
            ItemtWorkOn.setServiceOrder(serviceOrder);
            OBDal.getInstance().save(ItemtWorkOn);
            
      }
}