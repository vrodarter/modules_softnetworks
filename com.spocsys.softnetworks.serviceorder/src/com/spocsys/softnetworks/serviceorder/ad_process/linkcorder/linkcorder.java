package com.spocsys.softnetworks.serviceorder.ad_process.linkcorder;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.model.common.order.Order;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.common.order.OrderLine;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.base.provider.OBProvider;
import java.math.*;
import java.util.Iterator;

import javassist.bytecode.stackmap.BasicBlock.Catch;
import java.util.Date;
import java.util.List;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;
import com.spocsys.softnetworks.serviceorder.ServiceOrderLine;



public class linkcorder extends BaseProcessActionHandler{
    
    private static final Logger log= Logger.getLogger(linkcorder.class);
    @Override
    protected JSONObject doExecute(Map<String, Object> parameters, String content){
      try{
        
        JSONObject request= new JSONObject(content);
        // take the service order id header in a string var
        String id= request.getString("Ssnso_Serviceorder_ID");
        // position in the json file until the selected in the pick and execute window
        String params = request.getString("_params");
        JSONObject parameter= new JSONObject(params);
        String window= parameter.getString("window");
        JSONObject win= new JSONObject(window);
        
        
       
       JSONArray selection =new JSONArray(win.getString("_selection"));
            JSONObject corder=  JSONObject.class.cast(selection.get(0));
            String CorderId= corder.getString("id");
           
            
            final Order salesOrder= OBDal.getInstance().get(Order.class, CorderId);
            //array list with all the lines in c_order_line
            final OBCriteria<OrderLine> OrderL = OBDal.getInstance().createCriteria(OrderLine.class);
            /// restriction WHERE sales_order is equal to a sale order
            OrderL.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER,salesOrder));
            /// obj of the lines in Service_order_line table
            ServiceOrderLine lines= OBProvider.getInstance().get(ServiceOrderLine.class);
           
           
            return managerserviceOrderLines(OrderL.list(),id,salesOrder,corder);
            
             
           
            
      }catch(Exception e){
        log.error("Error processing request: "+e.getMessage());
      }
      return new JSONObject();
    }
     
    //manager to insert lines in "sales order line" table
    private JSONObject managerserviceOrderLines( List<OrderLine> OLines, String SalesOrderID,Order Or, JSONObject win){
      //head of service order
      final ServiceOrder headSO= OBDal.getInstance().get(ServiceOrder.class, SalesOrderID);
      try{
        OBContext.setAdminMode(true);
        final OBCriteria<ServiceOrderLine>SOList=OBDal.getInstance().createCriteria(ServiceOrderLine.class);
        SOList.add(Restrictions.eq(ServiceOrderLine.PROPERTY_SSNSOSERVICEORDER, headSO));
        long LineNO=10;
        /// count searching for the last line no
        if (SOList.list().size()!=0){
          LineNO = (SOList.list().size()+1)*10; 
        }
        // set description
        if (win.getString("description")!=null){
        headSO.setWorkDescription(win.getString("description"));
        }else{
          headSO.setWorkDescription("");
        }
        
         int i=0;
          for (OrderLine OLine : OLines){
              
            ServiceOrderLine newSOline = createServiceOrderLine(OLine,headSO,LineNO);
            LineNO = LineNO + 10;
            
            if(newSOline != null)
            {
              i++;
              OBDal.getInstance().save(newSOline);
              
              
              
            }
          }
          
          headSO.setSalesOrder(Or);
          //headSO.setSorder(Or);
          OBDal.getInstance().save(headSO);
          OBDal.getInstance().flush();
          //OBDal.getInstance().getSession().clear();
          OBDal.getInstance().commitAndClose();
          //// message ///////////
          JSONObject request =new JSONObject();
          JSONObject msg= new JSONObject();
          msg.put("msgType", "success");
          msg.put("msgTitle", "Create a Service Order");
          msg.put("msgText", i+" New lines has been copy from document number: "+Or.getDocumentNo());
          JSONObject msgInBPTabAction = new JSONObject();
          msgInBPTabAction.put("showMsgInView", msg);
          request.put("responseActions", msgInBPTabAction);
          return request;
      }catch(Exception e){
        
        
        
        
        System.out.println(e.getMessage()+" aqui");
        return new JSONObject();
        
      }finally{
        OBContext.restorePreviousMode();
      }   
      
    }//End Void managerServiceOrderLines
    
    
    //create new Service Order Line, insert data from Order Line Selected
    private ServiceOrderLine createServiceOrderLine( OrderLine OLine, ServiceOrder headSO,long lineNO)
    {
      try{
        ServiceOrderLine SOLine=OBProvider.getInstance().get(ServiceOrderLine.class);
        
        
        SOLine.setNewOBObject(true);//create new objet
        
        //-----------Mandatory info----------------------
        SOLine.setOrganization(headSO.getOrganization());
        SOLine.setClient(headSO.getClient());
        SOLine.setUpdatedBy(OBContext.getOBContext().getUser());
        SOLine.setCreatedBy(OBContext.getOBContext().getUser());
        //SOLine.setActive(true);
        
        
        //-----------------------------------------------
        final String DefaultProductType="pt1";
        
        
        //---------Info line----------------------------
        SOLine.setDescription(headSO.getWorkDescription());
        SOLine.setSsnsoServiceorder(headSO);
        SOLine.setLineNo(lineNO);
        SOLine.setProducType(DefaultProductType);
        SOLine.setProduct(OLine.getProduct());
        SOLine.setNETUnitPrice(OLine.getUnitPrice());
        
        SOLine.setOrderedQuantity(OLine.getOrderedQuantity().longValue());
        
        SOLine.setWarehouseRule(OLine.getWarehouseRule());
        SOLine.setUOM(OLine.getUOM());
        SOLine.setTax(OLine.getTax());
        SOLine.setWarehouse(OLine.getWarehouse());
        
        SOLine.setListPrice(OLine.getListPrice().longValue());
        SOLine.setCreationDate(new Date());
        SOLine.setUpdated(new Date());
        SOLine.setInvoicezerocost(false);
        SOLine.setNotinvoice(false);
        SOLine.setSuportcontract(false);
        SOLine.setWarranty(false);
        //---------------------------------------------
        
        return SOLine;
      }catch(Exception e){
        //print and log error
        log.error(e.getMessage());
        
        
      }
      
      return null;
    }
   
    
  
}