package com.spocsys.softnetworks.serviceorder.ad_process.printservlabel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;

import com.spocsys.softnetworks.serviceorder.SSNSO_Service;

import fr.w3blog.zpl.model.ZebraLabel;

public class PrintServiceLabel extends BaseProcessActionHandler {

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    JSONObject msg = new JSONObject();
    try {
      JSONObject request = new JSONObject(content);
      JSONArray params = request.getJSONArray("recordIdList");
      List<ZebraLabel> serviceLabels = new ArrayList<ZebraLabel>();
      // Message
      msg.put("severity", "info");
      msg.put("text", "Sucess");
      request.put("message", msg);
      // Call Printing
      for (int i = 0; i < params.length(); i++) {
        String selectedProductJSON = params.getString(i);
        String serviceID = selectedProductJSON;
        SSNSO_Service service = OBDal.getInstance().get(SSNSO_Service.class, serviceID);
        try {
          if (service != null) {
            ZebraLabel label = ZebraPrinterService.getZebraServiceLabel(service);
            serviceLabels.add(label);
          }
        } catch (Exception e) {
          msg.remove("severity");
          msg.remove("text");
          msg.put("severity", "error");
          msg.put("text", e.getMessage());
          request.put("message", msg);
          return request;
        }
      }
      try {
        ZebraPrinterService.printLabel(serviceLabels);
      } catch (Exception e) {
        msg.remove("severity");
        msg.remove("text");
        msg.put("severity", "error");
        msg.put("text", e.getMessage());
        request.put("message", msg);
        return request;
      }
      request.put("message", msg);
      return request;
    } catch (JSONException e) {
      e.printStackTrace();
    }
    return new JSONObject();
  }

}
