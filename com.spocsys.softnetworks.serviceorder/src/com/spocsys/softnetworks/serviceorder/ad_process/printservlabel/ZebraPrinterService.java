package com.spocsys.softnetworks.serviceorder.ad_process.printservlabel;

import java.util.List;

import org.hibernate.Hibernate;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.system.ClientInformation;
import org.openbravo.model.common.enterprise.Organization;

import com.spocsys.softnetworks.serviceorder.SSNSO_Service;
import com.spocsys.softnetworks.serviceorder.utils.Utils;
import com.spocsys.vigfurniture.zebra.utils.StringUtils;

import fr.w3blog.zpl.constant.ZebraFont;
import fr.w3blog.zpl.model.ZebraElement;
import fr.w3blog.zpl.model.ZebraLabel;
import fr.w3blog.zpl.model.element.ZebraBarCode128;
import fr.w3blog.zpl.model.element.ZebraGraficBox;
import fr.w3blog.zpl.model.element.ZebraNativeZpl;
import fr.w3blog.zpl.model.element.ZebraText;
import fr.w3blog.zpl.utils.ZebraUtils;

public class ZebraPrinterService {

  public static ZebraLabel getZebraServiceLabel(SSNSO_Service service) {
    ZebraLabel zebraLabel = new ZebraLabel(900, 750);
    zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);
    zebraLabel.addElement(getHeaderCommands());
    // Identifier
    zebraLabel.addElement(new ZebraText(50, 65, service.getSearchKey(), 8));
    zebraLabel.addElement(new ZebraBarCode128(50, 95, service.getSearchKey(), 20, 1, 30));
    zebraLabel.addElement(new ZebraGraficBox(10, 120, 860, 1, 3, "B"));
    // Model
    zebraLabel.addElement(new ZebraText(10, 145,
        "Model Number: " + (service.getModel() != null ? service.getModel() : ""), 2));
    // Serial Number
    zebraLabel.addElement(new ZebraText(10, 175,
        "Serial Number: " + (service.getSerialnumber() != null ? service.getSerialnumber() : ""),
        2));
    zebraLabel.addElement(new ZebraGraficBox(10, 240, 860, 1, 3, "B"));
    // Receiving/Warranty date
    zebraLabel.addElement(new ZebraText(10, 265,
        "Receiving Date: " + Utils.getStringDate(service.getWarrantyexpdate()), 2));
    zebraLabel.addElement(new ZebraGraficBox(10, 330, 860, 1, 3, "B"));
    // Description
    zebraLabel.addElement(new ZebraText(10, 360,
        "Description: " + (service.getName() != null ? service.getName() : ""), 2));

    String string = zebraLabel.getZplCode();
    System.out.println(string);
    return zebraLabel;

  }

  public static String senddingImageToPrinter() throws Exception {
    String zplCode = "";
    Client current = OBContext.getOBContext().getCurrentClient();
    if (current.getClientInformationList() != null && !current.getClientInformationList().isEmpty()
        && current.getClientInformationList().get(0) != null) {
      ClientInformation clientInformation = current.getClientInformationList().get(0);
      clientInformation = OBDal.getInstance().get(ClientInformation.class,
          clientInformation.getId());
      Hibernate.initialize(clientInformation);
      org.openbravo.model.ad.utility.Image log = clientInformation.getYourCompanyBigImage();
      Hibernate.initialize(log);
      if (log != null) {
        byte[] image = log.getBindaryData();
        String string = StringUtils.getHexString(image);
        zplCode = "~DGR:LOGO.GRF," + image.length + "," + 39 + "," + string;
        // zebraLabel.addElement(new ZebraNativeZpl("^KD0\n"));
      }
    }
    System.out.println(zplCode);
    return zplCode;
  }

  public static void printLabel(List<ZebraLabel> labels) throws Exception {

    Organization organization = OBContext.getOBContext().getCurrentOrganization();
    if (organization.getSvfzePrinterName() != null
        && !organization.getSvfzePrinterName().isEmpty()) {
      ZebraUtils.printZpl(labels, organization.getSvfzePrinterName());

    } else {
/*      if (organization.getSvfzePrinterIp() != null && !organization.getSvfzePrinterIp().isEmpty()
          && organization.getSvfzePrinterPort() != null
          && organization.getSvfzePrinterPort() != 0) {
        ZebraUtils.printZpl(labels, organization.getSvfzePrinterIp(),
            organization.getSvfzePrinterPort().intValue());

      } else {
        throw new OBException(
            "Can not print these service labels. There are not printer configuration defined for the Organization: "
                + organization.getIdentifier());

      }
*/    }
  }

  private static ZebraElement getHeaderCommands() {
    return new ZebraNativeZpl(
        "^FO10,460^GFA,2002,2002,22,K01gPFE,K07gQF8,K0CgQ0C,K0CR01MFQ0C,J018R01LFEQ06,J018T07IFER06,J018T03IF8R06,J018T01IFS06,J018U0FFEK0FF001I06,J0181KFI03IF80FFCJ0JF03I06,J01803IF8J07FF00FFCI07JFE3I06,J01800FFEK01FC00FFC001FE003FFI06,J018007FEK01F001FFC003FJ07FI06,J018003FEK01F001FF800FEJ03FI06,J018001FEK01E001FF801F8K0F8006,J018001FFK03C001FF803FL078006,J018I0FFK03C001FF807FL038006,J018I0FF8J078003FF00FEL038006,J018I07F8J078003FF00FCL018006,J018I07FCJ07I03FF01FCL018006,J018I03FCJ0FI03FF03F8M08006,J018I03FEJ0EI03FF03F8P06,J018I01FEI01EI07FF07F8P06,J018I01FFI01CI07FE07FQ06,J018J0FFI03CI07FE07FQ06,J018J0FF80038I07FE07FQ06,J018J07F80078I07FE0FFQ06,J018J07FC007J0FFE0FFK07IFE06,J018J03FC00FJ0FFC0FFK03IFC06,J018J03FE00EJ0FFC0FFL07FE006,J018J01FE01EJ0FFC0FFL03FC006,J018J01FF01CJ0FFC0FFL01FC006,J018K0FF03CI01FFC0FFL01F8006,J018K0FF838I01FF80FFL01F8006,J018K07F878I01FF807FL01F8006,J018K07FC7J01FF807F8K01F8006,J018K03FCFJ01FF807F8K01F8006,J018K03FEEJ03FF803FCK01F8006,J018K01FFEJ03FF003FCK01F8006,J018K01FFEJ03FF001FEK01F8006,J018L0FFCJ03FFI0FFK01F8006,J018L0FFCJ03FFI07FK01F8006,J018L07F8J03FFI03F8J01F8006,J018L07F8J07FEI01FEJ01F8006,J018L03FK07FEJ0FFJ03F8006,J018L03FK07FEJ07FCI07F8006,J018L01EK07FEJ01FF803FEI06,J018L01EK0FFEK03KFJ06,J018M0CK0FFEL07IF8J06,J018R01FFEU06,J018R03IFU06,J018R0JF8T06,J018Q0LFES06,K0CP01LFES0C,K0CgQ0C,K07gP038,K03gQF,,::::::::::L07FF381C7FF0F038E7FF9C0F3FF87FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF,L0780381C783CFC38E0781C0F3C1E78,:L0780381C7838FE38E0781C0F3C1E78,L07FE381C7878EF38E0781C0F3C3C7FF,L07FE381C7FF0E738E0781C0F3FF87FF,L07FE381C7FF0E3B8E0781C0F3FFC7FF,L0780381C7878E3F8E0781C0E3C3C78,L0780381C7838E1F8E0781C0E3C1C78,L07803C3C7838E1F8E0781E0E3C1E78,L07803FFC783CE0F8E0781FFE3C1E7FF8,L07801FF8783CE078E0780FFC3C0E7FF8,L07I0FE0701CE078E07803F83C0E7FF8,,:::::::^FS");
  }

}
