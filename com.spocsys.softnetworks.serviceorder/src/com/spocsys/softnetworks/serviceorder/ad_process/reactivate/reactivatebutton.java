package com.spocsys.softnetworks.serviceorder.ad_process.reactivate;


import java.util.Map;

import org.apache.log4j.Logger;

import java.util.Date;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;

import com.spocsys.softnetworks.serviceorder.ServiceOrder;

public class reactivatebutton extends BaseProcessActionHandler{
  private static final Logger log= Logger.getLogger(reactivatebutton.class);
  
    protected JSONObject doExecute(Map<String, Object> parameters, String content){
      try{
        JSONObject request= new JSONObject (content);
        String SO_id= request.getString("Ssnso_Serviceorder_ID");
        ServiceOrder headSO= OBDal.getInstance().get(ServiceOrder.class, SO_id);
        
        if (headSO.getInvoice()!=null){
          throw new OBException("@ssnso_error_reactivate@");
        }else{
          headSO.setDocumentStatus("DR");
          OBDal.getInstance().save(headSO);
          OBDal.getInstance().flush();
          OBDal.getInstance().commitAndClose();  
        }
        
        return request; 
      }catch(Exception e){
        log.error(e.getMessage());
        return new JSONObject();
      }
  }
  
}