package com.spocsys.softnetworks.serviceorder.ad_process.serverparts;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.service.db.DalConnectionProvider;

import com.spocsys.softnetworks.serviceorder.SSNSO_Partandlabor;
import com.spocsys.softnetworks.serviceorder.SSNSO_Ticket;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;
import com.spocsys.softnetworks.serviceorder.utils.Utils;
import com.spocsys.vigfurnitures.services.SVFSV_Source;

public class ServeParts extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(ServeParts.class);

  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    try {
      JSONObject request = new JSONObject(content);
      String serviceOrderId = request.getString("Ssnso_Serviceorder_ID");
      final ServiceOrder serviceOrder = OBDal.getInstance().get(ServiceOrder.class, serviceOrderId);
      List<SSNSO_Ticket> tickets = serviceOrder.getSSNSOTICKETList();
      // recorrer Tickets
      boolean anyParts = false;
      boolean none = true;
      boolean partially = false;
      InternalMovement movement = null;
      int entryST = 0;
      for (int i = 0; i < tickets.size(); i++) {
        SSNSO_Ticket ticket = tickets.get(i);
        // Recorrer Parts
        List<SSNSO_Partandlabor> partsAndLabors = ticket.getSSNSOPARTANDLABORList();
        for (SSNSO_Partandlabor part : partsAndLabors) {
          // si no es Servicio se considera una parte
          if (!part.getProduct().getProductType().equals("S")) {
            anyParts = true;
            // revisar stock y realizar la accion
            if (part.getStockavailability() != null
                && part.getStockavailability().equals("INSTOCK")) {
              // Create Goods Movement or add line
              if (movement == null) {
                movement = OBProvider.getInstance().get(InternalMovement.class);
                createOrUpdateMovement(serviceOrder, ticket, part, movement, true);
              } else
                createOrUpdateMovement(serviceOrder, ticket, part, movement, false);

              none = false;
            } else {
              // enviar a Sourcing Table
              addEntryToSourcingTable(serviceOrder, ticket, part);
              entryST++;
              partially = true;
            }
          }
        }
        if (anyParts && !none && !partially) {
          // Todos en stock (Change Status of ticket to " Need to pick parts")
          ticket.setStatus("NPP");
          OBDal.getInstance().save(ticket);

        } else if (anyParts && !none && partially) {
          // change status of ticket " Partial parts"
          ticket.setStatus("PPP");
          OBDal.getInstance().save(ticket);
        }
        anyParts = false;
        none = true;
        partially = false;
      }
      // if (anyParts) {
      // OBDal.getInstance().save(serviceOrder);
      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();
      // }
      OBError msg = new OBError();
      msg.setType("Success");
      msg.setTitle("Serving Parts");
      String extraMessage = (movement != null) ? "<br>Goods Movement: " + movement.getIdentifier()
          : "";
      String extraMessage2 = (entryST > 0) ? "<br>" + entryST + " entries on Sourcing table" : "";
      msg.setMessage("The process has finished successfully " + extraMessage + extraMessage2);

      JSONObject response = new JSONObject();
      response.put("message", convertOBErrorToJSON(msg));

      /*
       * JSONObject msgInBPTabAction = new JSONObject(); msgInBPTabAction.put("showMsgInView", msg);
       * request.put("responseActions", msgInBPTabAction);
       */

      return response;
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();

      OBError msg = new OBError();
      msg.setType("Error");
      msg.setTitle("Serve Parts");
      msg.setMessage(e.getMessage());

      JSONObject response = new JSONObject();
      try {
        response.put("message", convertOBErrorToJSON(msg));
      } catch (JSONException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
      return response;
    }
  }

  public static JSONObject convertOBErrorToJSON(OBError obError) {
    JSONObject errorMessage = new JSONObject();
    try {
      errorMessage.put("severity", obError.getType().toLowerCase());
      errorMessage.put("text", obError.getMessage());
    } catch (JSONException ignore) {
      ignore.printStackTrace();
    }
    return errorMessage;
  }

  private void addEntryToSourcingTable(ServiceOrder serviceOrder, SSNSO_Ticket ticket,
      SSNSO_Partandlabor part) {
    SVFSV_Source sourcingTable = OBProvider.getInstance().get(SVFSV_Source.class);
    Date date = serviceOrder.getDUEDate() != null ? serviceOrder.getDUEDate()
        : serviceOrder.getCompleted();
    sourcingTable.setOrganization(serviceOrder.getOrganization());
    sourcingTable.setProduct(part.getProduct());
    sourcingTable.setCustomerCBpartner(serviceOrder.getClientid());
    sourcingTable.setOrderReference("" + serviceOrder.getDocumentno() + " (Need to Order parts)");
    sourcingTable.setQty(part.getQuantityordered().longValue());
    sourcingTable.setOrderdate(date);
    OBDal.getInstance().save(sourcingTable);
  }

  private void createOrUpdateMovement(ServiceOrder serviceOrder, SSNSO_Ticket ticket,
      SSNSO_Partandlabor part, InternalMovement movement, boolean create) throws Exception {
    long lineNo = 0L;
    if (create) {
      movement.setName(Utils.getStringDate(serviceOrder.getDUEDate() != null
          ? serviceOrder.getDUEDate() : serviceOrder.getCompleted()));
      movement
          .setDescription("Movement from parts of Service Order: " + serviceOrder.getDocumentno());
      movement.setMovementDate(serviceOrder.getDUEDate() != null ? serviceOrder.getDUEDate()
          : serviceOrder.getCompleted());
      movement.setSSNSOServiceOrder(serviceOrder);
      OBDal.getInstance().save(movement);
      OBDal.getInstance().flush();
      lineNo += 10L;
    }
    // Line
    if (lineNo != 10L) {
      lineNo = getMaxLineNo(movement) + 10L;
    }
    InternalMovementLine line = OBProvider.getInstance().get(InternalMovementLine.class);
    line.setMovement(movement);
    line.setLineNo(lineNo);
    line.setProduct(part.getProduct());
    line.setUOM(part.getProduct().getUOM());
    // Locations
    Warehouse fromWarehouse = part.getShipfrom() == null ? part.getStocklocation()
        : part.getShipfrom();
    // To
    Locator locatorTo = getDefaultWarehouse(serviceOrder.getOrganization().getSsnsoSowarehouse(),
        null);
    if (locatorTo == null) {
      throw new Exception("Service Order Warehouse was not founded on the Organization: "
          + serviceOrder.getOrganization().getIdentifier());
    }
    // From
    // Locator locator = getLocatorInStock(part, fromWarehouse, locatorTo);
    // Locator storage = (locator != null) ? locator : getDefaultWarehouse(fromWarehouse,
    // locatorTo);
    Locator locator = part.getStorageBin();
    Locator storage = (locator != null && !locator.getId().equals(locatorTo.getId()))
        ? part.getStorageBin() : null;
    // Validate Bins
    if (storage == null) {
      throw new Exception(
          "The bin target(LAB) is the same that stock bin or the stock bin not exists on "
              + fromWarehouse.getIdentifier());
    }
    line.setStorageBin(storage);

    line.setNewStorageBin(locatorTo);
    line.setMovementQuantity(part.getQuantityordered());
    OBDal.getInstance().save(line);
    movement.getMaterialMgmtInternalMovementLineList().add(line);
    OBDal.getInstance().save(movement);
  }

  private Locator getLocatorInStock(SSNSO_Partandlabor part, Warehouse fromWarehouse,
      Locator locatorTo) {
    try {
      return ServePartsData.getLocatorWithStock(new DalConnectionProvider(), fromWarehouse.getId(),
          part.getQuantityordered().toString(), part.getProduct().getId(), part.getUOM().getId(),
          part.getAttributeSetValue() != null ? part.getAttributeSetValue().getId() : null,
          locatorTo.getId());
    } catch (ServletException e) {
      return null;
    }
  }

  private Locator getDefaultWarehouse(Warehouse fromWarehouse, Locator locatorTo) {
    if (fromWarehouse != null) {
      OBCriteria<Locator> critLocator = OBDal.getInstance().createCriteria(Locator.class);
      critLocator.add(Restrictions.eq("default", true));
      critLocator.add(Restrictions.eq("warehouse", fromWarehouse));
      if (locatorTo != null)
        critLocator.add(Restrictions.not(Restrictions.eq("id", locatorTo.getId())));
      if (critLocator.list().size() > 0) {
        return critLocator.list().get(0);
      } else if (fromWarehouse.getLocatorList().size() > 0) {
        for (Locator loc : fromWarehouse.getLocatorList()) {
          if (locatorTo != null && !loc.getId().equals(locatorTo.getId())) {
            return loc;
          } else if (locatorTo == null) {
            return fromWarehouse.getLocatorList().get(0);
          }
        }
      }
    }
    return null;
  }

  private long getMaxLineNo(InternalMovement movement) {
    if (movement.getMaterialMgmtInternalMovementLineList().size() == 0) {
      return 0L;
    }
    int noLines = movement.getMaterialMgmtInternalMovementLineList().size();

    return Long.parseLong(String.valueOf(noLines * 10));
  }

}