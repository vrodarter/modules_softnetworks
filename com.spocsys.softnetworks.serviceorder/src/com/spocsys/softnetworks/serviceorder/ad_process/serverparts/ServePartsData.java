//Sqlc generated V1.O00-1
package com.spocsys.softnetworks.serviceorder.ad_process.serverparts;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.database.SessionInfo;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.service.db.QueryTimeOutUtil;

public class ServePartsData implements FieldProvider {
  static Logger log4j = Logger.getLogger(ServePartsData.class);
  private String InitRecordNumber = "0";
  public String name;
  public String id;
  public String description;
  public String stock;
  public String locatorId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else {
      log4j.debug("Field does not exist: " + fieldName);
      return null;
    }
  }

  public static Locator getLocatorWithStock(ConnectionProvider connectionProvider,
      String warehouseId, String qty, String product, String cUomId, String attributeSIId,
      String notInBin) throws ServletException {
    String strSql = "";
    strSql = strSql + "         select sum(qtyonhand) as stock, m_storage_detail.m_locator_id "
        + "                     from m_storage_detail"
        + "                         inner join m_locator on m_storage_detail.m_locator_id = m_locator.m_locator_id"
        + "                     " + "                     where 1 = 1" + "                         "
        + "                         and m_storage_detail.m_locator_id in (select m_locator_id from m_locator where m_warehouse_id = ?) "
        + "                             and qtyonhand >= to_number(?) "
        + "                             and m_product_id = ? "
        + "                             and c_uom_id = ? "
        + ((attributeSIId != null && !attributeSIId.equals(""))
            ? "                            and m_attributesetinstance_id = ? " : "")
        + "                             and m_storage_detail.m_locator_id <> ? "
        + "                     group by m_storage_detail.m_locator_id";
    ResultSet result;
    Vector<ServePartsData> vector = new Vector<ServePartsData>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
      st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, warehouseId);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, qty);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, product);
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, cUomId);
      if (attributeSIId != null && !attributeSIId.equals("")) {
        iParameter++;
        UtilSql.setValue(st, iParameter, 12, null, attributeSIId);
      }
      iParameter++;
      UtilSql.setValue(st, iParameter, 12, null, notInBin);
      result = st.executeQuery();
      while (result.next()) {
        ServePartsData objectPrintControllerData = new ServePartsData();
        objectPrintControllerData.stock = UtilSql.getValue(result, "stock");
        objectPrintControllerData.locatorId = UtilSql.getValue(result, "m_locator_id");
        vector.addElement(objectPrintControllerData);
      }
      result.close();
    } catch (SQLException e) {
      log4j.error("SQL error in query: " + strSql + "Exception:" + e);
      throw new ServletException(
          "@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch (Exception ex) {
      log4j.error("Exception in query: " + strSql + "Exception:" + ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch (Exception ignore) {
        ignore.printStackTrace();
      }
    }
    if (vector.size() > 0) {
      for (int i = 0; i < vector.size(); i++) {
        Locator locator = OBDal.getInstance().get(Locator.class, vector.get(0).locatorId);
        if (locator.isDefault())
          return locator;
      }
      return OBDal.getInstance().get(Locator.class, vector.get(0).locatorId);
    }
    return null;
  }

}