package com.spocsys.softnetworks.serviceorder.event;

import java.util.List;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.client.kernel.event.EntityDeleteEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.softnetworks.serviceorder.SSNSO_Partandlabor;
import com.spocsys.softnetworks.serviceorder.SSNSO_Ticket;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;

public class ServiceQuoteHandler extends EntityPersistenceEventObserver {

      private static Entity[] entities = { ModelProvider.getInstance().getEntity(ServiceOrder.ENTITY_NAME),
                  ModelProvider.getInstance().getEntity(Order.class),
                  ModelProvider.getInstance().getEntity(OrderLine.class) };
      protected Logger        logger   = Logger.getLogger(this.getClass());

      protected Entity[] getObservedEntities() {
            return entities;
      }

      public void onDelete(@Observes EntityDeleteEvent event) {
            if (!isValidEvent(event)) {
                  return;
            }
            String ob = event.getTargetInstance().getEntityName();

            /*
             * Deleting rows from serviceOrder. If it is linked to Order remove service_order_id
             * from Order
             */
            if (event.getTargetInstance().getEntityName().equals("ssnso_serviceorder")) {
                  final ServiceOrder so = (ServiceOrder) event.getTargetInstance();
                  OBCriteria<Order> order_crit = OBDal.getInstance().createCriteria(Order.class);
                  order_crit.add(Restrictions.eq(Order.PROPERTY_SSNSOSERVICEORDER, so));
                  List<Order> order_list = order_crit.list();
                  if (!order_list.isEmpty()) {
                        for (Order o : order_list) {
                              o.setSsnsoServiceorder(null);
                              OBDal.getInstance().save(o);
                        }
                  }
            }

            // The same but from Order.
            if (event.getTargetInstance().getEntityName().equals("Order")) {
                  final Order order = (Order) event.getTargetInstance();
                  OBCriteria<ServiceOrder> serv_order_crit = OBDal.getInstance().createCriteria(ServiceOrder.class);
                  serv_order_crit.add(Restrictions.eq(ServiceOrder.PROPERTY_SALESORDER, order));
                  List<ServiceOrder> serv_o_list = serv_order_crit.list();
                  if (!serv_o_list.isEmpty()) {
                        for (ServiceOrder serv_order : serv_o_list) {
                              serv_order.setSalesOrder(null);
                              OBDal.getInstance().save(serv_order);

                              // Removing reference _order from PartadnLabor

                              List<SSNSO_Ticket> tk_list = serv_order.getSSNSOTICKETList();
                              for (SSNSO_Ticket tk : tk_list) {
                                    final OBCriteria<SSNSO_Partandlabor> pl_crit = OBDal.getInstance()
                                                .createCriteria(SSNSO_Partandlabor.class);
                                    pl_crit.add(Restrictions.eq(SSNSO_Partandlabor.PROPERTY_SSNSOTICKET, tk));
                                    List<SSNSO_Partandlabor> pl_list = pl_crit.list();
                                    for (SSNSO_Partandlabor pl : pl_list) {
                                          pl.setPurchaseorder(null);
                                          pl.setSalesOrderLine(null);
                                          OBDal.getInstance().save(pl);
                                    }
                              }
                        }
                  }
            }
            // Removing orderLine ref in part and labor
            if (event.getTargetInstance().getEntityName().equals("OrderLine")) {
                  final OrderLine orderLine = (OrderLine) event.getTargetInstance();
                  OBCriteria<SSNSO_Partandlabor> plCrit = OBDal.getInstance().createCriteria(SSNSO_Partandlabor.class);
                  plCrit.add(Restrictions.eq(SSNSO_Partandlabor.PROPERTY_SALESORDERLINE, orderLine));
                  List<SSNSO_Partandlabor> plList = plCrit.list();
                  if (!plList.isEmpty()) {
                        for (SSNSO_Partandlabor pl : plList) {
                              pl.setSalesOrderLine(null);
                              OBDal.getInstance().save(pl);

                        }
                  }
            }
      }
}
