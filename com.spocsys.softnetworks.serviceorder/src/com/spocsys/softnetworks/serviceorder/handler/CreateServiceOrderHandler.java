package com.spocsys.softnetworks.serviceorder.handler;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.system.Client;
import org.openbravo.service.db.DbUtility;

public class CreateServiceOrderHandler extends BaseActionHandler {

  static Logger log4j = Logger.getLogger(CreateServiceOrderHandler.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {
    JSONObject response = new JSONObject();
    JSONArray actions = new JSONArray();

    try {
      final JSONObject request = new JSONObject(content);
      final String _action = (String) parameters.get("action");
      final String action = _action == null ? SCConstants.DEFAULT.toString() : _action;
      final Client client = OBContext.getOBContext().getCurrentClient();
      // Processes not implementing issales parameter are defaulted to issales = true

      if (SCConstants.DEFAULT.toString().equals(action)) {
        JSONObject params = request.getJSONObject("_params");
        JSONArray names = request.getJSONObject("_params").names();

        // String sPartner = null, sContract = null, sOrder = null, sServiceorder = null;

        String startingDate = request.getString("SDate");
        String dueDate = request.getString("DueDate");

        String technician = request.getString("C_BPartner_ID");
        String outsorced = request.getString("C_BPartner_ID");

        final String cOrderId = request.getString("C_Order_ID");
        final String serviceOrderId = request.getString("EM_Ssnso_Serviceorder_ID");

        // int iLines = 1;
        //
        // if (request.has("_params") && params.has("C_Bpartner_ID")) {
        // sPartner = params.getString("C_Bpartner_ID");
        // }
        // if (request.has("_params") && params.has("Contract")) {
        // sContract = params.getString("Contract");
        // }
        // if (request.has("_params") && params.has("C_Order_ID")) {
        // sOrder = params.getString("C_Order_ID");
        // }
        // if (request.has("_params") && params.has("Ssnso_Serviceorder_ID")) {
        // sServiceorder = params.getString("Ssnso_Serviceorder_ID");
        // }
        // if (request.has("_params") && params.has("Lines")) {
        // iLines = Integer.parseInt(params.getString("Lines"));
        // }

        // OBDal.getInstance().flush();
        // OBDal.getInstance().getSession().clear();

        JSONObject goToInfo = new JSONObject();
        final String tabId = "DA48C885321B4ACEAE4EEA483C16535A";
        goToInfo.put("tabId", tabId);
        goToInfo.put("wait", "true");
        JSONObject openTabAction = new JSONObject();
        openTabAction.put("openDirectTab", goToInfo);
        actions.put(openTabAction);

        response.put("responseActions", actions);
      }

      else if (SCConstants.PROCESS.toString().equals(action)) {
        int j = 0;

        OBError result = new OBError();
        // result.setMessage(OBMessageUtils.messageBD("SSNSO_ServicesGenerated").replace("%",
        // String.valueOf(j)));
        result.setType("Success");
        result.setTitle("PROCESS");
        response.put("message", convertOBErrorToJSON(result));

      } else {
        log4j.error("Invalid action: " + action);
        throw new OBException("Invalid action: " + action);
      }

      // OBDal.getInstance().commitAndClose();

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      log4j.error("BaseInvoiceProcess error: " + e.getMessage(), e);

      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
      try {
        response.put("retryExecution", true);
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", message);
        response.put("message", errorMessage);
      } catch (JSONException ignore) {
        ignore.printStackTrace();
      }
    }

    return response;

  }

  public static JSONObject convertOBErrorToJSON(OBError obError) {
    JSONObject errorMessage = new JSONObject();
    try {
      errorMessage.put("severity", obError.getType().toLowerCase());
      errorMessage.put("text", obError.getMessage());
    } catch (JSONException ignore) {
      ignore.printStackTrace();
    }
    return errorMessage;
  }
}
