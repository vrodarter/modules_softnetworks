package com.spocsys.softnetworks.serviceorder.handler;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.order.Order;
import org.openbravo.service.db.DbUtility;

import com.spocsys.softnetworks.serviceorder.SSNSO_Service;
import com.spocsys.softnetworks.serviceorder.ServiceOrder;

public class CreateServices extends BaseActionHandler {
  static Logger log4j = Logger.getLogger(CreateServices.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {
    JSONObject response = new JSONObject();
    JSONArray actions = new JSONArray();

    try {
      final JSONObject request = new JSONObject(content);
      final String _action = (String) parameters.get("action");
      final String action = _action == null ? SCConstants.DEFAULT.toString() : _action;
      final Client client = OBContext.getOBContext().getCurrentClient();
      // Processes not implementing issales parameter are defaulted to issales = true

      if (SCConstants.DEFAULT.toString().equals(action)) {
        JSONObject params = request.getJSONObject("_params");
        String sService = request.getString("Ssnso_Service_ID");
        String sPartner = null, sContract = null, sOrder = null, sServiceorder = null;
        int iLines = 1;

        if (request.has("_params") && params.has("C_Bpartner_ID")) {
          sPartner = params.getString("C_Bpartner_ID");
        }
        if (request.has("_params") && params.has("Contract")) {
          sContract = params.getString("Contract");
        }
        if (request.has("_params") && params.has("C_Order_ID")) {
          sOrder = params.getString("C_Order_ID");
        }
        if (request.has("_params") && params.has("Ssnso_Serviceorder_ID")) {
          sServiceorder = params.getString("Ssnso_Serviceorder_ID");
        }
        if (request.has("_params") && params.has("Lines")) {
          iLines = Integer.parseInt(params.getString("Lines"));
        }

        // Insert products
        final SSNSO_Service service = OBDal.getInstance().get(SSNSO_Service.class, sService);
        BusinessPartner partner = null;
        Order order = null;
        ServiceOrder serviceOrder = null;
        if (sPartner != null && !sPartner.equals("")) {
          partner = OBDal.getInstance().get(BusinessPartner.class, sPartner);
        }
        if (sOrder != null && !sOrder.equals("")) {
          order = OBDal.getInstance().get(Order.class, sOrder);
        }
        if (sServiceorder != null && !sServiceorder.equals("")) {
          serviceOrder = OBDal.getInstance().get(ServiceOrder.class, sServiceorder);
        }
        String uuid = null;
        if (iLines > 0) {
          uuid = SequenceIdData.getUUID();
          // Borrado de los servicios anteriores
          OBCriteria<SSNSO_Service> qServ = OBDal.getInstance().createCriteria(SSNSO_Service.class);
          qServ.setFilterOnActive(false);
          qServ.add(Restrictions.eq("active", false));
          qServ.add(Restrictions.eq("ongeneration", true));
          List<SSNSO_Service> lServ = qServ.list();
          for (int i = 0; i < lServ.size(); i++) {
            OBDal.getInstance().remove(lServ.get(i));
          }
          OBDal.getInstance().flush();
        }

        for (int i = 0; i < iLines; i++) {
          // insertarProduct(sService, i, sPartner, sContract, sOrder, sServiceorder);
          SSNSO_Service newService = (SSNSO_Service) DalUtil.copy(service, false);
          newService.setSalesOrder(order);
          newService.setSsnsoServiceorder(serviceOrder);
          newService.setBusinessPartner(partner);
          newService.setSearchKey(service.getSearchKey() + " " + (i + 2));
          newService.setName(service.getName() + " " + (i + 2));
          newService.setActive(false);
          newService.setOngeneration(true);
          newService.setGroup(uuid);
          OBDal.getInstance().save(newService);
        }
        OBDal.getInstance().flush();
        OBDal.getInstance().getSession().clear();

        // Set the session variable for filtering the Invoice Proposal window
        if (iLines > 0) {
          HttpSession httpSession = RequestContext.get().getSession();
          String sessionId = (String) httpSession.getAttribute("#SSNSO_CreateServices");
          httpSession.setAttribute("#SSNSO_CreateServices", uuid);

          JSONObject goToInfo = new JSONObject();
          final String tabId = "DA48C885321B4ACEAE4EEA483C16535A";
          goToInfo.put("tabId", tabId);
          goToInfo.put("wait", "true");
          JSONObject openTabAction = new JSONObject();
          openTabAction.put("openDirectTab", goToInfo);
          actions.put(openTabAction);

          response.put("responseActions", actions);
        }

      } else if (SCConstants.PROCESS.toString().equals(action)) {
        int j = 0;
        JSONArray registries = request.getJSONArray("recordIdList");
        for (int i = 0; i < registries.length(); i++) {
          SSNSO_Service service = OBDal.getInstance().get(SSNSO_Service.class, registries.get(i));
          if (service.isOngeneration()) {
            service.setActive(true);
            service.setOngeneration(false);
            OBDal.getInstance().save(service);
            j++;
          }
        }
        OBDal.getInstance().flush();
        OBDal.getInstance().getSession().clear();

        OBError result = new OBError();
        result.setMessage(
            OBMessageUtils.messageBD("SSNSO_ServicesGenerated").replace("%", String.valueOf(j)));
        result.setType("Success");
        result.setTitle("PROCESS");
        response.put("message", convertOBErrorToJSON(result));

      } else {
        log4j.error("Invalid action: " + action);
        throw new OBException("Invalid action: " + action);
      }

      OBDal.getInstance().commitAndClose();

    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();
      log4j.error("BaseInvoiceProcess error: " + e.getMessage(), e);

      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
      try {
        response.put("retryExecution", true);
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", message);
        response.put("message", errorMessage);
      } catch (JSONException ignore) {
        ignore.printStackTrace();
      }
    }

    return response;

  }

  public static JSONObject convertOBErrorToJSON(OBError obError) {
    JSONObject errorMessage = new JSONObject();
    try {
      errorMessage.put("severity", obError.getType().toLowerCase());
      errorMessage.put("text", obError.getMessage());
    } catch (JSONException ignore) {
      ignore.printStackTrace();
    }
    return errorMessage;
  }
}
