package com.spocsys.softnetworks.serviceorder.handler;

import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.client.kernel.KernelUtils;
import org.openbravo.erpCommon.utility.OBMessageUtils;

public class ProductActionHandler extends BaseActionHandler {

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {
    try {
      final JSONObject request = new JSONObject(content);
      final JSONArray selection = request.getJSONArray("selection");
      System.out.println(">>>>" + selection.toString());
      //return request;
    } catch (Exception e) {
      e.printStackTrace();
    }
    try{
    	//Preparando mensaje
    	JSONObject jsonResponse = new JSONObject();
    	jsonResponse.put(
                "error", "Hola mensaje!!!");
        return jsonResponse;
    }catch(JSONException ex){
    	        
    }
    
    return new JSONObject();
  }

}