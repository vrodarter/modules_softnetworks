package com.spocsys.softnetworks.serviceorder.provider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;

@ApplicationScoped
@ComponentProvider.Qualifier(ServiceComponentProvider.EXAMPLE_VIEW_COMPONENT_TYPE)
public class ServiceComponentProvider extends BaseComponentProvider {
  public static final String EXAMPLE_VIEW_COMPONENT_TYPE = "SSNSO_ServiceProvider";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> resources = new ArrayList<ComponentResource>();
    resources.add(
        createStaticResource("web/com.spocsys.softnetworks.serviceorder/js/service.js", false));
    resources.add(createStaticResource(
        "web/com.spocsys.softnetworks.serviceorder/js/services-multirecordprocess.js", false));
    return resources;
  }

}