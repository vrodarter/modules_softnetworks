package com.spocsys.softnetworks.serviceorder.utils;
/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2014 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.SQLReturnObject;
import org.openbravo.erpCommon.utility.Utility;

public class ActionButtonUtility {
      static Logger log4j = Logger.getLogger(ActionButtonUtility.class);

      public static FieldProvider[] docStatus(ConnectionProvider conn, VariablesSecureApp vars, String strDocAction,
                  String strDocStatus, String strTableId, String windowId, String tabId) {

            FieldProvider[] ld = null;
            // Id of reference in ad_reference table
            String referenceId = "";

            if (log4j.isDebugEnabled())
                  log4j.debug("DocAction - generating combo elements for table: " + strTableId + " - actual status: "
                              + strDocAction);

            if (log4j.isDebugEnabled())
                  log4j.debug("DocAction - generating combo elements for table: " + strTableId + " - actual status: "
                              + strDocAction);

            // Table "Order" - Reference ""
            if (strTableId.equals("259")) {
                  // reference from ad_reference document action
                  referenceId = "61566DDCEA0A4D3A85F18C688BE73159";
            }
            try {
                  ComboTableData comboTableData = new ComboTableData(vars, conn, "LIST", "Docstatus", referenceId, "",
                              Utility.getContext(conn, vars, "#AccessibleOrgTree", "ActionButtonUtility"),
                              Utility.getContext(conn, vars, "#User_Client", "ActionButtonUtility"), 0);
                  Utility.fillSQLParameters(conn, vars, null, comboTableData, "ActionButtonUtility", "");
                  ld = comboTableData.select(false);
                  comboTableData = null;
            } catch (Exception e) {
                  return null;
            }
            SQLReturnObject[] data = null;
            if (ld != null) {
                  Vector<Object> v = new Vector<Object>();
                  SQLReturnObject data1 = new SQLReturnObject();
                  /// C_Order Table ///
                  if (strTableId.equals("259")) {
                        // Handling cases
                        if (strDocStatus.equals("DR")) {
                              data1.setData("ID", "IP");
                              v.addElement(data1);
                        } else if (strDocStatus.equals("IP")) {
                              data1.setData("ID", "CO");
                              v.addElement(data1);
                              data1 = new SQLReturnObject();
                              data1.setData("ID", "RJ");
                              v.addElement(data1);
                              data1 = new SQLReturnObject();
                              data1.setData("ID", "RE");
                              v.addElement(data1);
                        } else if (strDocStatus.equals("CO")) {
                              data1.setData("ID", "CL");
                              v.addElement(data1);
                              data1 = new SQLReturnObject();
                              data1.setData("ID", "IP");
                              v.addElement(data1);
                        } else if (strDocStatus.equals("SSNSO_RJ")) {
                              data1.setData("ID", "CL");
                              v.addElement(data1);
                              data1 = new SQLReturnObject();
                              data1.setData("ID", "IP");
                              v.addElement(data1);
                        } else {
                              throw new OBException(
                                          "Document Status of Service Quote is not allowed for this operation");
                        }

                  }

                  data = new SQLReturnObject[v.size()];
                  if (log4j.isDebugEnabled())
                        log4j.debug("DocAction - total combo elements: " + data.length);
                  int ind1 = 0, ind2 = 0;
                  while (ind1 < ld.length && ind2 < v.size()) {
                        for (int j = 0; j < v.size(); j++) {
                              SQLReturnObject sqlro = (SQLReturnObject) v.get(j);
                              if (sqlro.getField("ID").equals(ld[ind1].getField("ID"))) {
                                    if (log4j.isDebugEnabled())
                                          log4j.debug("DocAction - Element: " + ind1 + " - ID: "
                                                      + sqlro.getField("ID"));
                                    data[ind2] = sqlro;
                                    data[ind2].setData("NAME", ld[ind1].getField("NAME"));
                                    data[ind2].setData("DESCRIPTION", ld[ind1].getField("DESCRIPTION"));
                                    ind2++;
                                    break;
                              }
                        }
                        ind1++;
                  }
                  // Exclude null values in the array
                  List<SQLReturnObject> result = new ArrayList<SQLReturnObject>();
                  for (SQLReturnObject sqlr : data) {
                        if (sqlr != null) {
                              result.add(sqlr);
                        }
                  }
                  data = result.toArray(new SQLReturnObject[0]);

            }
            return data;
      }
}
