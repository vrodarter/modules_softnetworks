package com.spocsys.softnetworks.serviceorder.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openbravo.base.session.OBPropertiesProvider;

public class DateUtils {

  public static String getStringDate(Date date) {
    if (date == null)
      return null;
    String format = OBPropertiesProvider.getInstance().getOpenbravoProperties().getProperty("dateFormat.java");
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    String fecha = sdf.format(date);
    return fecha;
  }

  public static Date getDateFromString(String date) throws ParseException {
    // String formato =
    // OBPropertiesProvider.getInstance().getOpenbravoProperties().getProperty("dateFormat.java");
    String formato = "dd-MM-yyyy HH:mm:ss"; // 09-12-2014 09:30:00
    SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formato);
    java.util.Date dDate = formatoDelTexto.parse(date);
    return dDate;
  }

  public static Date getCurrentDate() throws ParseException {
    // String formato =
    // OBPropertiesProvider.getInstance().getOpenbravoProperties().getProperty("dateFormat.java");
    String formato = "dd-MM-yyyy HH:mm:ss"; // 09-12-2014 09:30:00
    Date currentdate = new Date();
    String c_datetime = getStringDate(currentdate) + " 00:00:00";

    SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formato);
    java.util.Date dDate = formatoDelTexto.parse(c_datetime);
    return dDate;
  }
}
