package com.spocsys.softnetworks.serviceorder.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openbravo.base.session.OBPropertiesProvider;

public class Utils {

  public static String getStringDate(Date date) {
    if (date == null)
      return null;
    String format = OBPropertiesProvider.getInstance().getOpenbravoProperties()
        .getProperty("dateFormat.java");
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    String fecha = sdf.format(date);
    return fecha;
  }

}
