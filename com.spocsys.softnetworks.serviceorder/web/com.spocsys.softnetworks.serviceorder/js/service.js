isc.defineClass('SSNSO_ServiceView', isc.VLayout);
//isc.defineClass('SSNSO_MainView', isc.ListGrid);


isc.SSNSO_ServiceView.addProperties({
	/**INVENTO**/ 
	showsItself: false,
	/**INVENTO**/
	  
	grid: null,
	button: null,
	
	initWidget: function () {
		this.grid = isc.ListGrid.create({
			setDataSource: function (ds, fields) {
				var selectedFields = [],
				fs = OB.Constants.FIELDSEPARATOR,
				ident = OB.Constants.IDENTIFIER,
			    fieldNames = ['name', 'productCategory'+fs+ident],
			    i, j;
			for(i = 0; i < fieldNames.length; i++) {
				for(j in ds.fields) {
					if(ds.fields.hasOwnProperty(j) && j === fieldNames[i]) {
						selectedFields.push(ds.fields[j]);
					}
				}
			}
			this.Super('setDataSource', [ds, selectedFields]);
				this.fetchData();
		    }
		});
		this.grid.canEdit=true;
				
		OB.Datasource.get('Product', this.grid, null, true);
		
		this.button = isc.OBFormButton.create({
			title: 'Process',
			main: this,
			action: function () {
				var me = this.main,
				selection = me.grid.getSelectedRecords(), i, data;
				
				data = {
				 selection: []
				};
				
				for(i = 0; i < selection.length; i++) {
					data.selection.push(selection[i].id);
				}
				
				OB.RemoteCallManager.call('com.spocsys.softnetworks.serviceorder.handler.ServiceActionHandler', data, {}, function (rpcResponse, result, rpcRequest) {		    	  
			    	  //Tratamiento
					console.log(result);
					console.log(this);
					//console.log(form);
					
			    	  if (result.error) {
			    	        me.handleError(result.error);
			    	      } else {
			    	    	console.log(arguments);
			    	  }
			      });			
			}
		});
		this.addMember(null);
		this.addMember(this.grid);
		this.addMember(this.button);
		
		this.Super('initWidget', arguments);
	},

	handleError: function (error) {
		////* {{{msgType}}}: The message type. It can be 'success', 'error', 'info' or 'warning'
		var messageBar = isc.OBMessageBar.create({});	
		if (this.getMember(0)) {
			//this.removeMember(this.getMember(0));
		}
		messageBar = isc.OBMessageBar.create({});
		messageBar.setMessage('success', error);
		this.setMember(messageBar, 0);		
	}
});

/*function pasteText (text) {
    var fieldNames = [];

    var selection = countryList.selection.getSelectedCells();
    if (selection.length == 0) {
        countryList.selection.selectCell(0, 0);
        selection = countryList.selection.getSelectedCells();
    }
    var firstCol = selection[0][1];
    var fields = countryList.fields;
    for (var col = firstCol; col < fields.length; col++) {
        fieldNames.add(countryList.getFieldName(col));
    }
    var settings = {
        fieldList: fieldNames,
        fieldSeparator: "\t",
        escapingMode: "double"
    };
    var dataSource = countryList.dataSource;
    var records = dataSource.recordsFromText(text, settings);
    countryList.applyRecordData(records);
}

function createPasteDialog () {

    var width = 525;
    var height = 300;

    var guidance = "Press Ctrl-V (\u2318V on Mac) or right click (Control-click on Mac) to paste values, then click \"Apply\".";

    var resultsForm = isc.DynamicForm.create({

        numCols: 1,

        width: width,

        height: height,

        autoFocus: true,

        fields: [{ type: "text",
                   name: "guidance",
                   showTitle: false,
                   editorType: "StaticTextItem",
                   value: guidance
                 },
                 { type: "text",
                   name: "textArea",
                   canEdit: true,
                   showTitle: false,
                   height: "*",
                   width: "*",
                   editorType: "TextAreaItem" },
                 { type : "text",
                   name : "apply",
                   align: "center",
                   editorType: "ButtonItem",
                   title: "Apply",
                   click: function (form) { form.pasteAndClose(); }
                 }],

        pasteAndClose : function () {
            pasteText(this.textArea.getValue());
            this.dialog.hide();
            this.dialog.markForDestroy();
            this.dialog.removeItem(this);
        }

    });

    resultsForm.textArea = resultsForm.getField("textArea");

    resultsForm.dialog = isc.Dialog.create({ 
        autoSize: true,
        showToolbar: false,
        canDragReposition: true,
        title: "Paste Cells",
        items: [ resultsForm ],
        isModal: true,
        showModalMask: true
    });
}

isc.ListGrid.create({ 
    ID: "countryList",
    width: "100%",
    height: 288,
    canEdit: true,
    autoFetchData: true,
    canDragSelect: true,
    canSelectCells: true,
    dataSource: countryDS,
    dataArrived : function (startRow, endRow) {
       countryList.selection.selectCell(0, 0);
    }
});

isc.Button.create({ 
    top: 300, 
    left: 10,
    title: "Paste Cells",
    click: function () { createPasteDialog(); }
});
*/