/************************************************************************************ 
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/

OB.SSNSO = OB.SSNSO || {};

OB.SSNSO.Process = {
  execute: function (params, view) {
    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
        recordIdList = [],
        sortedRecordIdList = [],
        messageBar = view.getView(params.adTabId).messageBar,
        callback, validationMessage, validationOK = true,
        record = {};

    callback = function (rpcResponse, data, rpcRequest) {
      var status = rpcResponse.status,
          view = rpcRequest.clientContext.view.getView(params.adTabId);
      view.messageBar.setMessage(data.message.severity, null, data.message.text);

      // close process to refresh current view
      params.button.closeProcessPopup();
    };

    for (i = 0; i < selection.length; i++) {
      if (params.vadidate) {
        validationMessage = params.vadidate(selection[i]);
        if (validationMessage) {
          validationOK = false;
          break;
        }
      }
      record = new Object();
      record.id = selection[i].id;
      record.searchKey = selection[i].searchKey;
      sortedRecordIdList.push(record);
    };

    for (i = 0; i < sortedRecordIdList.length; i++) {
      recordIdList.push(sortedRecordIdList[i].id);
    }

    if (!validationOK) {
      messageBar.setMessage(isc.OBMessageBar.TYPE_ERROR, null, validationMessage);
      return;
    }

    OB.RemoteCallManager.call(params.actionHandler, {
      recordIdList: recordIdList,
      action: params.action,
      processInstanceId: ''
    }, {
      processId: params.processId,
      action: params.action
    }, callback, {
      view: view
    });
  },

  generateServices: function (params, view) {
    params.action = 'PROCESS';
    params.actionHandler = 'com.spocsys.softnetworks.serviceorder.handler.CreateServices';
    //params.adTabId = '322E0DD3454F4C91A51E7F79D6A15012';
    params.adTabId = view.tabId;
    params.processId = 'A4648FD5710144E6A0F2AC825C1A9A6B';
    OB.SSNSO.Process.execute(params, view);
  },
  printServiceLabel: function (params, view) {
	    params.action = 'PROCESS';
	    params.actionHandler = 'com.spocsys.softnetworks.serviceorder.ad_process.printservlabel.PrintServiceLabel';
	    //params.adTabId = '322E0DD3454F4C91A51E7F79D6A15012';
	    params.adTabId = view.tabId;
	    params.processId = '1B2D92B75D124EAC9BC5913D1504798F';
	    OB.SSNSO.Process.execute(params, view);
	  }
  
};