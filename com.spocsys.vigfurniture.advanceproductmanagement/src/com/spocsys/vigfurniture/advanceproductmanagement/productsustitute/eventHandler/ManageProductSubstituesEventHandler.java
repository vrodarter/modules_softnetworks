package com.spocsys.vigfurniture.advanceproductmanagement.productsustitute.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.kernel.event.EntityDeleteEvent;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
//
import org.openbravo.model.common.plm.Product;

import com.spocsys.vigfurniture.advanceproductmanagement.SubstituteProduct;

//
public class ManageProductSubstituesEventHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      SubstituteProduct.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  //
  //
  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final SubstituteProduct regSubstitute = (SubstituteProduct) event.getTargetInstance();
    logger.info("onUpdate..." + event.getCurrentState(getFlagOperationProperty()));
    if (regSubstitute != null) {
      if (event.getCurrentState(getFlagOperationProperty()).equals("DELETED")) {
        // deleteDuplaInterchange(regSubstitute.getProduct().getId(), regSubstitute
        // .getSubstituteProduct().getId());
        // nothing
        // } else if (event.getPreviousState(getFlagOperationProperty()).equals("UPDATED")
        // && event.getCurrentState(getFlagOperationProperty()).equals("NULL")) {
        // nothing
      } else if (!event.getCurrentState(getFlagOperationProperty()).equals("UPDATED")) {
        updateInterchange(regSubstitute, (Product) event.getPreviousState(getSubstituteProperty()));
      }
      logger.info("Interchange " + event.getTargetInstance().getId() + " is being updated");
    }

  }// onUpdate

  //
  //
  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final SubstituteProduct regSubstitute = (SubstituteProduct) event.getTargetInstance();
    logger.info("onSave...");
    if (regSubstitute != null && event.getCurrentState(getFlagOperationProperty()) != null) {
      if (event.getCurrentState(getFlagOperationProperty()).equals("ORIG")) {
        // is operation ORIG
        generateInterchange(regSubstitute);
        updateInterchangeOperation();
        logger.info("Interchange " + event.getTargetInstance().getId() + " is being created");
      }
    }
  }// onSave
   //

  public void onDelete(@Observes EntityDeleteEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final SubstituteProduct regSubstitute = (SubstituteProduct) event.getTargetInstance();
    logger.info("onDelete..." + event.getCurrentState(getFlagOperationProperty()));
    if (regSubstitute != null && event.getCurrentState(getFlagOperationProperty()) != null) {
      if (!event.getCurrentState(getFlagOperationProperty()).equals("DELETED")) {
        // is operation ORIG
        deleteInterchange(regSubstitute);
        logger.info("Interchange " + event.getTargetInstance().getId() + " is being deleted");
      }
    }

  }// onDelete

  //
  private int deleteInterchangeOperation(String pm_product_id) {
    String hqlDelete = "delete from " + SubstituteProduct.ENTITY_NAME + " where "
        + SubstituteProduct.PROPERTY_FLAGOPERATION + " = :pm_product_id ";
    Query queryDelete = OBDal.getInstance().getSession().createQuery(hqlDelete);
    queryDelete.setParameter("pm_product_id", pm_product_id);
    int deletedRecord = queryDelete.executeUpdate();
    //
    logger.info("deleteInterchangeOperation[" + deletedRecord + "][" + hqlDelete + "]");
    //
    return deletedRecord;

  }// deleteInterchangeOperation

  private int deleteDuplaInterchange(String pm_product_id, String pm_substitute) {
    String hqlDelete = "delete from " + SubstituteProduct.ENTITY_NAME + " where "
        + SubstituteProduct.PROPERTY_PRODUCT + ".id = :pm_product_id and "
        + SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT + ".id = :pm_substitute";
    Query queryDelete = OBDal.getInstance().getSession().createQuery(hqlDelete);
    queryDelete.setParameter("pm_product_id", pm_product_id);
    queryDelete.setParameter("pm_substitute", pm_substitute);
    int deletedRecord = queryDelete.executeUpdate();
    //
    logger.info("deleteDuplaInterchange[" + deletedRecord + "][" + hqlDelete + "]");
    //
    return deletedRecord;

  }

  //
  private int deleteRecordMarked(String pMarked) {
    String hqlDelete = "delete from " + SubstituteProduct.ENTITY_NAME + " where "
        + SubstituteProduct.PROPERTY_FLAGOPERATION + " = :pMarked ";
    Query queryDelete = OBDal.getInstance().getSession().createQuery(hqlDelete);
    queryDelete.setParameter("pMarked", pMarked);
    int deletedRecord = queryDelete.executeUpdate();
    //
    logger.info("deleteRecordMarked[" + deletedRecord + "][" + hqlDelete + "]");
    //
    return deletedRecord;

  }

  private void deleteInterchange(SubstituteProduct regSubstitute) {
    String hqlWhere = Product.PROPERTY_ID + ".id in (select "
        + SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT + ".id from "
        + SubstituteProduct.ENTITY_NAME + " where " + SubstituteProduct.PROPERTY_PRODUCT
        + ".id = :pm_product_id) or " + Product.PROPERTY_ID
        + ".id in ( :pm_substitute_id, :pm_product_id) ";

    OBQuery<Product> queryProd = OBDal.getInstance().createQuery(Product.class, hqlWhere);
    queryProd.setNamedParameter("pm_product_id", regSubstitute.getProduct().getId());
    queryProd.setNamedParameter("pm_substitute_id", regSubstitute.getSubstituteProduct().getId());
    //

    for (Product listProd : queryProd.list()) {
      // the query returns a list of arrays (columns of the query)
      if (!listProd.getId().equals(regSubstitute.getProduct().getId())) {
        if (!listProd.getId().equals(regSubstitute.getSubstituteProduct().getId())) {
          updateFlagInterchange(listProd.getId(), regSubstitute.getSubstituteProduct().getId(),
              "DELETED");
        } else {
          if (listProd.getId().equals(regSubstitute.getSubstituteProduct().getId())) {
            OBQuery<Product> queryInter = OBDal.getInstance().createQuery(Product.class, hqlWhere);
            queryInter.setNamedParameter("pm_product_id", regSubstitute.getProduct().getId());
            queryInter.setNamedParameter("pm_substitute_id", regSubstitute.getSubstituteProduct()
                .getId());
            for (Product listInter : queryInter.list()) {
              if (listProd.getId().equals(regSubstitute.getProduct().getId())
                  && listInter.getId().equals(regSubstitute.getSubstituteProduct().getId())) {
                // nothing
              } else if (!listProd.getId().equals(listInter.getId())) {
                updateFlagInterchange(listProd.getId(), listInter.getId(), "DELETED");
              }
            }
          }
        }
      }
    }// for
    deleteRecordMarked("DELETED");
  }// deleteInterchange

  //
  //
  private Property getFlagOperationProperty() {
    return entities[0].getProperty(SubstituteProduct.PROPERTY_FLAGOPERATION);
  }

  private Property getSubstituteProperty() {
    return entities[0].getProperty(SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT);
  }

  private void generateInterchange(SubstituteProduct regSubstitute) {
    /*
     * Query que obtiene los productos que se encuentran definidos como sustitutos + el nuevo
     * producto + producto sustituido
     */
    String hqlWhere = Product.PROPERTY_ID + " in (select "
        + SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT + ".id from "
        + SubstituteProduct.ENTITY_NAME + " where " + SubstituteProduct.PROPERTY_PRODUCT
        + ".id = :pm_product_id) or " + Product.PROPERTY_ID + " = :pm_substitute_id or "
        + Product.PROPERTY_ID + " = :pm_product_id ";
    logger.info("Generated Interchange: " + hqlWhere);

    //
    OBQuery<Product> queryProd = OBDal.getInstance().createQuery(Product.class, hqlWhere);
    queryProd.setNamedParameter("pm_product_id", regSubstitute.getProduct().getId());
    queryProd.setNamedParameter("pm_substitute_id", regSubstitute.getSubstituteProduct().getId());

    // queryProd.list().add(regSubstitute.getSubstituteProduct());
    logger.info("Comenzando el loop...1");
    for (Product listProd : queryProd.list()) {
      // the query returns a list of arrays (columns of the query)
      if (!listProd.getId().equals(regSubstitute.getProduct().getId())) {
        if (!listProd.getId().equals(regSubstitute.getSubstituteProduct().getId())) {
          insertInterchange(regSubstitute, listProd, regSubstitute.getSubstituteProduct(),
              "INTERCHANGE");
        } else {
          if (listProd.getId().equals(regSubstitute.getSubstituteProduct().getId())) {
            OBQuery<Product> queryInter = OBDal.getInstance().createQuery(Product.class, hqlWhere);
            queryInter.setNamedParameter("pm_product_id", regSubstitute.getProduct().getId());
            queryInter.setNamedParameter("pm_substitute_id", regSubstitute.getSubstituteProduct()
                .getId());
            for (Product listInter : queryInter.list()) {
              if (!listProd.getId().equals(listInter.getId())) {
                insertInterchange(regSubstitute, listProd, listInter, "INTERCHANGE");
              }
            }
          }
        }
      }
    }// for
  }// generateInterchange

  public SubstituteProduct insertInterchange(SubstituteProduct regSubstitute, Product pObjProduct,
      Product pobjSubstitute, String pFlagOperation) {
    logger.info("Inserting...");
    SubstituteProduct objSubstitute = null;
    try {

      try {
        OBContext.setAdminMode(true);
        objSubstitute = OBProvider.getInstance().get(SubstituteProduct.class);
        /*
         * INSERT INTO svfadpm_substitute( svfadpm_substitute_id, m_product_id, substitute_id,
         * ad_client_id, ad_org_id, createdby, updatedby, name, flag_operation) VALUES
         * (ad_sequence_nextno('Svfadpm_Substitute'), regProd.product_id, regInter.product_id,
         * NEW.Ad_Client_Id, NEW.Ad_Org_Id, NEW.CreatedBy, NEW.UpdatedBy, regInter.name,
         * 'INTERCHANGE');
         */

        objSubstitute.setProduct(pObjProduct);
        objSubstitute.setSubstituteProduct(pobjSubstitute);
        objSubstitute.setClient(regSubstitute.getClient());
        objSubstitute.setOrganization(regSubstitute.getOrganization());
        objSubstitute.setCreatedBy(regSubstitute.getCreatedBy());
        objSubstitute.setUpdatedBy(regSubstitute.getUpdatedBy());
        objSubstitute.setName(pobjSubstitute.getName());
        objSubstitute.setFlagOperation(pFlagOperation);
        OBDal.getInstance().save(objSubstitute);
      } catch (Exception e) {
        e.printStackTrace();
        logger.error(e);
        objSubstitute = null;
      }

    } finally {
      OBContext.restorePreviousMode();
    }
    return objSubstitute;
  }// insertInterchange

  private void updateInterchange(SubstituteProduct regSubstitute, Product pold_substitute) {
    String hqlWhere = Product.PROPERTY_ID + ".id in (select "
        + SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT + ".id from "
        + SubstituteProduct.ENTITY_NAME + " where " + SubstituteProduct.PROPERTY_PRODUCT
        + ".id = :pm_product_id) or " + Product.PROPERTY_ID + ".id in (:pm_product_id) ";

    OBQuery<Product> queryProd = OBDal.getInstance().createQuery(Product.class, hqlWhere);
    queryProd.setNamedParameter("pm_product_id", regSubstitute.getProduct().getId());
    // queryProd.setNamedParameter("pm_substitute_id",
    // regSubstitute.getSubstituteProduct().getId());
    //
    logger.info("updateInterchange...[" + pold_substitute.getName() + "]");
    for (Product listProd : queryProd.list()) {
      logger.info("updateInterchange [" + listProd.getName() + "]");
      // the query returns a list of arrays (columns of the query)
      if (!listProd.getId().equals(regSubstitute.getProduct().getId())) {
        if (listProd.getId().equals(pold_substitute.getId())) {
          updateProductInterchange(pold_substitute.getId(), regSubstitute.getSubstituteProduct(),
              "UPDATED");
        } else if (!listProd.getId().equals(regSubstitute.getSubstituteProduct().getId())) {
          updateDuplaInterchange(listProd.getId(), pold_substitute.getId(), regSubstitute,
              "UPDATED");
        }
      }
    }// for
  }// updateInterchange

  //
  private int updateInterchangeOperation() {
    String hqlUpdate = "update " + SubstituteProduct.ENTITY_NAME + " set "
        + SubstituteProduct.PROPERTY_FLAGOPERATION + " = 'ORIG' where "
        + SubstituteProduct.PROPERTY_FLAGOPERATION + " <> 'ORIG' ";
    Query queryDelete = OBDal.getInstance().getSession().createQuery(hqlUpdate);

    int updatedRecord = queryDelete.executeUpdate();
    //
    logger.info("updateInterchangeOperation[" + updatedRecord + "][" + hqlUpdate + "]");
    //
    return updatedRecord;

  }

  private int updateFlagOperation(String pProduct_id, String pSubstitute_id, String pFlagOperation) {
    String hqlUpdate = "update " + SubstituteProduct.ENTITY_NAME + " set "
        + SubstituteProduct.PROPERTY_FLAGOPERATION + " = :pFlagOperation where "
        + SubstituteProduct.PROPERTY_PRODUCT + " = :pProduct_id and "
        + SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT + " = :pSubstitute_id ";
    Query queryDelete = OBDal.getInstance().getSession().createQuery(hqlUpdate);
    queryDelete.setParameter("pProduct_id", pProduct_id);
    queryDelete.setParameter("pSubstitute_id", pSubstitute_id);
    queryDelete.setParameter("pFlagOperation", pFlagOperation);
    int updatedRecord = queryDelete.executeUpdate();
    //
    logger.info("updateFlagOperation[" + updatedRecord + "][" + hqlUpdate + "]");
    //
    return updatedRecord;

  }

  private void updateFlagInterchange(String pProduct_id, String pSubstitute_id, String pOperation) {
    OBQuery<SubstituteProduct> querySubstitute = OBDal.getInstance().createQuery(
        SubstituteProduct.class,
        SubstituteProduct.PROPERTY_PRODUCT + ".id = :pProduct_id and "
            + SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT + ".id = :pSubstitute_id");
    querySubstitute.setNamedParameter("pProduct_id", pProduct_id);
    querySubstitute.setNamedParameter("pSubstitute_id", pSubstitute_id);
    for (SubstituteProduct objSubstitute : querySubstitute.list()) {
      logger.info("updateFlagInterchange: [" + objSubstitute.getProduct().getName() + "]["
          + objSubstitute.getSubstituteProduct().getName() + "]");
      objSubstitute.setFlagOperation(pOperation);
    }//

  }// updateFlagInterchange

  private void updateProductInterchange(String pOldProduct_id, Product pProduct, String pOperation) {
    OBQuery<SubstituteProduct> querySubstitute = OBDal.getInstance().createQuery(
        SubstituteProduct.class, SubstituteProduct.PROPERTY_PRODUCT + ".id = :pOldProduct_id");
    querySubstitute.setNamedParameter("pOldProduct_id", pOldProduct_id);
    logger.info("updateProductInterchange:[" + pOldProduct_id + "]");
    for (SubstituteProduct objSubstitute : querySubstitute.list()) {
      logger.info("updateProductInterchange: [" + objSubstitute.getProduct().getName() + "]["
          + objSubstitute.getSubstituteProduct().getName() + "]");
      objSubstitute.setProduct(pProduct);
      objSubstitute.setFlagOperation(pOperation);
    }//
  }

  //
  private void updateDuplaInterchange(String pm_product_id, String pmold_substitute,
      SubstituteProduct pm_substitute, String pOperation) {
    OBQuery<SubstituteProduct> querySubstitute = OBDal.getInstance().createQuery(
        SubstituteProduct.class,
        SubstituteProduct.PROPERTY_PRODUCT + ".id = :pProduct_id and "
            + SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT + ".id = :pSubstitute_id");
    querySubstitute.setNamedParameter("pProduct_id", pm_product_id);
    querySubstitute.setNamedParameter("pSubstitute_id", pmold_substitute);
    for (SubstituteProduct objSubstitute : querySubstitute.list()) {
      logger.info("updateDuplaInterchange: [" + objSubstitute.getProduct().getName() + "]["
          + objSubstitute.getSubstituteProduct().getName() + "]");
      objSubstitute.setSubstituteProduct(pm_substitute.getSubstituteProduct());
      objSubstitute.setFlagOperation(pOperation);
    }//
  }

}// ManageProductSubstituesEventHandler