package com.spocsys.vigfurniture.advanceproductmanagement.productsustitute.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;

import com.spocsys.vigfurniture.advanceproductmanagement.SubstituteProduct;

public class ProductEventHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Product.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Product regProduct = (Product) event.getTargetInstance();
    if (regProduct != null) {
      if (regProduct.isActive() == false) {
        DesactiveSubstitutes(regProduct);
      }
    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final Product regProduct = (Product) event.getTargetInstance();
    if (regProduct != null) {
      if (regProduct.isActive() == false) {
        DesactiveSubstitutes(regProduct);
      }
    }
  }

  void DesactiveSubstitutes(Product regProduct) {
    for (SubstituteProduct regSubstituteProduct : regProduct.getSVFADPMSubstituteList()) {

      if (regSubstituteProduct.getSubstituteProduct().isActive() == true) {
        regSubstituteProduct.getSubstituteProduct().setActive(false);
        OBDal.getInstance().save(regSubstituteProduct.getSubstituteProduct());
      }
    }
  }
}
