package com.spocsys.vigfurniture.approval;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;

@ApplicationScoped
@ComponentProvider.Qualifier(ApprovalComponentProvider.Approval_COMPONENT_PROVIDER)
public class ApprovalComponentProvider extends BaseComponentProvider {

  public static final String Approval_COMPONENT_PROVIDER = "SVFAPPR_ComponentProvider";

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();

    globalResources.add(createStaticResource(
        "web/com.spocsys.vigfurniture.approval/js/js_img/WindowLoading.js", false));

    globalResources.add(createStaticResource(
        "web/com.spocsys.vigfurniture.approval/js/approval/ApprovalClass.js", false));

    return globalResources;
  }

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<String> getTestResources() {
    final List<String> testResources = new ArrayList<String>();
    return testResources;
  }

}