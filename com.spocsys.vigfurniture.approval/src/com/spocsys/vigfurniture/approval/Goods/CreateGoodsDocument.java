package com.spocsys.vigfurniture.approval.Goods;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.warehouse.pickinglist.PickingList;

import com.spocsys.vigfurniture.approval.utils.GetFunctions;

public class CreateGoodsDocument {
  private static final Logger log = Logger.getLogger(CreateGoodsDocument.class);

  ShipmentInOut createHeader(Order regOrder) {

    try {

      ShipmentInOut regShipmentInOut = OBProvider.getInstance().get(ShipmentInOut.class);

      // ----------------------------

      regShipmentInOut.setClient(regOrder.getClient());
      regShipmentInOut.setOrganization(regOrder.getOrganization());
      regShipmentInOut.setCreatedBy(OBContext.getOBContext().getUser());
      regShipmentInOut.setUpdatedBy(OBContext.getOBContext().getUser());
      regShipmentInOut.setSalesTransaction(true);

      // -----------------------------
      regShipmentInOut.setDocumentNo("From Order: " + regOrder.getDocumentNo());
      regShipmentInOut.setDocumentAction("CO");
      regShipmentInOut.setDocumentStatus("DR");
      regShipmentInOut.setPosted("N");
      regShipmentInOut.setProcessNow(false);
      regShipmentInOut.setProcessed(false);
      regShipmentInOut
          .setDocumentType(GetFunctions.getDocument("MMS", regOrder.getOrganization(), true));
      regShipmentInOut.setSalesOrder(regOrder);
      regShipmentInOut.setPrint(false);
      regShipmentInOut.setMovementType("C-");
      regShipmentInOut.setMovementDate(new Date());
      regShipmentInOut.setAccountingDate(new Date());
      // -------------------------------
      regShipmentInOut.setBusinessPartner(regOrder.getBusinessPartner());
      regShipmentInOut.setPartnerAddress(regOrder.getPartnerAddress());
      regShipmentInOut.setDeliveryLocation(regOrder.getDeliveryLocation());
      // -------------------------------
      regShipmentInOut
          .setWarehouse(regOrder.getOrderLineList().get(0).getSvfpaStockLocation() == null
              ? regOrder.getWarehouse()
              : regOrder.getOrderLineList().get(0).getSvfpaStockLocation());
      regShipmentInOut.setDeliveryTerms("A");
      regShipmentInOut.setFreightCostRule("I");
      regShipmentInOut.setFreightAmount(BigDecimal.ZERO);
      regShipmentInOut.setDeliveryMethod("P");
      regShipmentInOut.setChargeAmount(BigDecimal.ZERO);
      regShipmentInOut.setPriority("5");
      regShipmentInOut.setCreateLinesFrom(false);
      regShipmentInOut.setGenerateTo(false);
      regShipmentInOut.setUpdateLines(false);
      regShipmentInOut.setLogistic(false);
      regShipmentInOut.setGenerateLines(false);
      regShipmentInOut.setCalculateFreight(false);
      regShipmentInOut.setReceiveMaterials(false);
      regShipmentInOut.setSendMaterials(false);
      regShipmentInOut.setProcessGoodsJava("CO");
      regShipmentInOut.setObwpackPacking(false);
      regShipmentInOut.setObwpackProcessed("DR");
      regShipmentInOut.setObwpackReactivated("DR");
      regShipmentInOut.setObwpackPackingrequired(true);

      return regShipmentInOut;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  ShipmentInOutLine createLine(OrderLine regOrderLine, ShipmentInOut regShipmentInOut,
      PickingList regPickingList, Product regProduct, BigDecimal Qty, long Line) {

    try {

      ShipmentInOutLine regShipmentInOutLine = OBProvider.getInstance()
          .get(ShipmentInOutLine.class);
      // ---------------------------------------------

      regShipmentInOutLine.setClient(regOrderLine.getClient());
      regShipmentInOutLine.setOrganization(regOrderLine.getOrganization());
      regShipmentInOutLine.setCreatedBy(OBContext.getOBContext().getUser());
      regShipmentInOutLine.setUpdatedBy(OBContext.getOBContext().getUser());
      // ------------------------------
      regShipmentInOutLine.setLineNo(Line);
      regShipmentInOutLine.setShipmentReceipt(regShipmentInOut);

      if ((regProduct.getId().equalsIgnoreCase(regOrderLine.getProduct().getId()) == true)
          && (regOrderLine.getOrderedQuantity().doubleValue() >= Qty.doubleValue())) {
        regShipmentInOutLine.setSalesOrderLine(regOrderLine);
      }

      regShipmentInOutLine
          .setStorageBin(GetFunctions.getLocator(regOrderLine.getSvfpaStockLocation()));
      regShipmentInOutLine.setProduct(regProduct);
      regShipmentInOutLine.setUOM(regOrderLine.getUOM());
      regShipmentInOutLine.setMovementQuantity(Qty);
      regShipmentInOutLine.setReinvoice(false);
      regShipmentInOutLine.setAttributeSetValue(regOrderLine.getAttributeSetValue());
      regShipmentInOutLine.setDescriptionOnly(false);
      regShipmentInOutLine.setManagePrereservation(false);
      regShipmentInOutLine.setManagePrereservation(false);
      regShipmentInOutLine.setExplode(false);
      // ----------------------------------------------
      regShipmentInOutLine.setObwplPickinglist(regPickingList);
      // ----------------------------------------------
      regShipmentInOutLine.setObwplEditlinesPe(false);
      regShipmentInOutLine.setObwplRemoveline(false);

      return regShipmentInOutLine;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public ShipmentInOut create(List<OrderLine> ListOrderLine, PickingList regPickingList) {

    List<ShipmentInOutLine> regShipmentInOutLineList = new ArrayList<ShipmentInOutLine>();
    ShipmentInOut regShipmentInOut = null;
    if (ListOrderLine.size() > 0) {
      regShipmentInOut = createHeader(ListOrderLine.get(0).getSalesOrder());

      if (regShipmentInOut == null) {
        return null;
      }
      OBDal.getInstance().save(regShipmentInOut);
      OBDal.getInstance().flush();

      int LastNoLine = 0;
      Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
      for (OrderLine regOrderLine : ListOrderLine) {

        LastNoLine = LastNoLine + 10;
        nroLine = Long.parseLong(String.valueOf(LastNoLine));
        ShipmentInOutLine regShipmentInOutLine = null;

        if (regOrderLine.getProduct().isBillOfMaterials() == true) {
          for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {

            nroLine = Long.parseLong(String.valueOf(LastNoLine));

            regShipmentInOutLine = createLine(regOrderLine, regShipmentInOut, regPickingList,
                regProductBOM.getBOMProduct(),
                regProductBOM.getBOMQuantity().multiply(regOrderLine.getOrderedQuantity()),
                nroLine);

            LastNoLine = LastNoLine + 10;

            if (regShipmentInOutLine == null) {
              return null;
            }
            OBDal.getInstance().save(regShipmentInOutLine);
            // regShipmentInOutLineList.add(regShipmentInOutLine);

          }

        } else {

          regShipmentInOutLine = createLine(regOrderLine, regShipmentInOut, regPickingList,
              regOrderLine.getProduct(), regOrderLine.getOrderedQuantity(), nroLine);

          if (regShipmentInOutLine == null) {
            return null;
          }
          OBDal.getInstance().save(regShipmentInOutLine);
          // OBDal.getInstance().flush();
          // regShipmentInOutLineList.add(regShipmentInOutLine);

        }

      }

      try {
        // regShipmentInOut.setMaterialMgmtShipmentInOutLineList(regShipmentInOutLineList);
        OBDal.getInstance().save(regShipmentInOut);
        // OBDal.getInstance().flush();
        // OBDal.getInstance().commitAndClose();

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return null;
      }
      return regShipmentInOut;

    }
    return null;

  }
}
