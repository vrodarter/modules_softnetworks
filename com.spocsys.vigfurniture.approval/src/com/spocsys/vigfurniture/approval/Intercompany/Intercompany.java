package com.spocsys.vigfurniture.approval.Intercompany;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.interco.IntercoMatchingDocument;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.ProductPrice;

import com.spocsys.vigfurniture.approval.utils.GetFunctions;
import com.spocsys.vigfurniture.cancelaction.utils.CancelActionsUtil;
import com.spocsys.vigfurniture.incomingproduct.OrderReference;
import com.spocsys.vigfurniture.productavailability.OrderReference.LinkSOToPO;

public class Intercompany {

  private static final Logger log = Logger.getLogger(Intercompany.class);
  static String SVFSV_C_ORDER_POST_Process_ID = "7E1E5E36FF334525B6A6B8C82CF1FD3F";
  static Organization GlobalSourceOrg = null;
  static Organization GlobalTargetOrg = null;
  static Warehouse GlobalWarehouse = null;

  static Order createHeaderPO(Order regSalesOrder) {

    Order regOrder = OBProvider.getInstance().get(Order.class);

    try {
      OBContext.setAdminMode(true);

      regOrder.setClient(OBContext.getOBContext().getCurrentClient());
      regOrder.setOrganization(GlobalSourceOrg);
      regOrder.setCreatedBy(OBContext.getOBContext().getUser());
      regOrder.setUpdatedBy(OBContext.getOBContext().getUser());
      regOrder.setSalesTransaction(false);

      regOrder.setActive(true);
      regOrder.setDocumentStatus("DR");
      regOrder.setDocumentAction("CO");
      regOrder.setProcessed(false);

      IntercoMatchingDocument regIntercoMatchingDocument = GetFunctions
          .getDocumentIntercompany("POO", false, GlobalSourceOrg, GlobalTargetOrg);

      if (regIntercoMatchingDocument == null) {
        throw new OBException("Document Type for Intercompany not found ("
            + GlobalSourceOrg.getName() + "," + GlobalTargetOrg.getName() + ")");
      }

      regOrder.setDocumentType(regIntercoMatchingDocument.getDocumentType());
      regOrder.setTransactionDocument(
          regIntercoMatchingDocument.getDocumentType()/*
                                                       * GetFunctions. getDocument ("POO", false)
                                                       */);
      regOrder.setDelivered(false);
      regOrder.setOrderDate(new Date());
      regOrder.setAccountingDate(new Date());
      regOrder.setCurrency(regSalesOrder.getCurrency());

      regOrder.setPaymentTerms(regSalesOrder.getPaymentTerms());

      if ((GlobalTargetOrg.getOrganizationInformationList() == null)
          || (GlobalTargetOrg.getOrganizationInformationList().size() <= 0)) {

        throw new OBException(
            "The organization: " + GlobalTargetOrg.getSearchKey() + " doesn't have information");
      }

      if (GlobalTargetOrg.getOrganizationInformationList().get(0).getBusinessPartner() == null) {
        throw new OBException("The organization: " + GlobalTargetOrg.getSearchKey()
            + " doesn't have Business Partner to map it");
      }
      BusinessPartner regBusinessPartner = GlobalTargetOrg.getOrganizationInformationList().get(0)
          .getBusinessPartner();

      PriceList regPriceList = GetFunctions.getPriceList(regBusinessPartner, false);

      if (regPriceList == null) {
        throw new OBException("The Price List for Purchase Order not found");
      }

      regOrder.setPriceList(regPriceList);

      regOrder.setBusinessPartner(regBusinessPartner);

      if (regBusinessPartner.getBusinessPartnerLocationList() == null) {
        throw new OBException(
            "The Business Partner: " + regBusinessPartner.getName() + " doesn't have address");
      }

      regOrder.setPartnerAddress(regBusinessPartner.getBusinessPartnerLocationList().get(0));
      regOrder.setWarehouse(GlobalWarehouse);

      FIN_PaymentMethod regPaymentMethod = GetFunctions.getPaymentMethod(regSalesOrder, false);
      PaymentTerm regPaymentTerm = GetFunctions.getPaymentTerm(regBusinessPartner, false);

      if (regPaymentMethod == null) {
        throw new OBException("The Payment Method not found");
      }

      if (regPaymentTerm == null) {
        regPaymentTerm = regSalesOrder.getPaymentTerms();
      }

      regOrder.setPaymentMethod(regPaymentMethod);
      regOrder.setPaymentTerms(regPaymentTerm);
      regOrder.setScheduledDeliveryDate(new Date());
      regOrder.setSvfdsDropship(regSalesOrder.isSvfdsDropship());
      regOrder.setDropShipPartner(regSalesOrder.getDropShipPartner());
      regOrder.setDropShipContact(regSalesOrder.getDropShipContact());
      regOrder.setDropShipLocation(regSalesOrder.getDropShipLocation());
      //
      return regOrder;
      // OBDal.getInstance().save(newPO);

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);

    } finally {
      OBContext.restorePreviousMode();
    }
    return null;
  }

  static OrderLine createLinePO(Order regOrder, OrderLine regOrderLine, long nroLine) {

    OrderLine regOrderLineNew = OBProvider.getInstance().get(OrderLine.class);

    try {

      // OBContext.setAdminMode(true);

      regOrderLineNew.setClient(regOrder.getClient());
      regOrderLineNew.setOrganization(regOrder.getOrganization());
      regOrderLineNew.setCreatedBy(OBContext.getOBContext().getUser());
      regOrderLineNew.setUpdatedBy(OBContext.getOBContext().getUser());

      regOrderLineNew.setSalesOrder(regOrder);
      regOrderLineNew.setOrderDate(regOrder.getOrderDate());
      regOrderLineNew.setCurrency(regOrder.getCurrency());

      regOrderLineNew.setWarehouse(regOrder.getWarehouse());
      regOrderLineNew.setProduct(regOrderLine.getProduct());
      regOrderLineNew.setAttributeSetValue(regOrderLine.getAttributeSetValue());
      regOrderLineNew.setUOM(regOrderLine.getUOM());
      regOrderLineNew.setOrderedQuantity(regOrderLine.getOrderedQuantity());

      TaxRate regTaxRate = GetFunctions.calculateOrderLineTax(regOrder, regOrderLine);

      if (regTaxRate == null) {
        throw new OBException("The tax not found");
      }

      ProductPrice regProductPrice = GetFunctions.getProductPrice(regOrder.getPriceList());

      regOrderLineNew.setTax(regTaxRate);
      regOrderLineNew.setDirectShipment(false);
      regOrderLineNew.setReservedQuantity(BigDecimal.ZERO);
      regOrderLineNew.setDeliveredQuantity(BigDecimal.ZERO);
      regOrderLineNew.setInvoicedQuantity(BigDecimal.ZERO);
      // regOrderLineNew.setLineNetAmount(regOrderLine.getLineNetAmount());
      regOrderLineNew.setFreightAmount(BigDecimal.ZERO);
      regOrderLineNew.setDescriptionOnly(false);
      regOrderLineNew.setLineNo(nroLine);
      regOrderLineNew.setSvfapprCreatedolinefrom(regOrderLine);

      if (regProductPrice == null) {
        regOrderLineNew.setListPrice(regOrderLine.getListPrice());
        regOrderLineNew.setStandardPrice(regOrderLine.getStandardPrice());
        regOrderLineNew.setUnitPrice(regOrderLine.getUnitPrice());
        // regOrderLineNew.setLineNetAmount(regOrderLine.getLineNetAmount());
        regOrderLineNew.setPriceLimit(BigDecimal.ZERO);
      } else {
        regOrderLineNew.setUnitPrice(regProductPrice.getStandardPrice());
        regOrderLineNew.setListPrice(regProductPrice.getListPrice());
        regOrderLineNew.setStandardPrice(regProductPrice.getStandardPrice());
        // regOrderLineNew.setLineNetAmount(regProductPrice.getStandardPrice());
        regOrderLineNew.setPriceLimit(BigDecimal.ZERO);
      }
      //
      if (regOrderLine.getAttributeSetValue() != null) {
        regOrderLineNew.setAttributeSetValue(regOrderLine.getAttributeSetValue());
      }
      if (regOrderLine.getSvfsvPoreference() != null) {
        regOrderLineNew.setSvfsvPoreference(regOrderLine.getSvfsvPoreference());
      }
      return regOrderLineNew;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);

    }
    //
    // finally {
    // OBContext.restorePreviousMode();
    // }
    return null;
  }

  public static boolean create(List<OrderLine> ListOrderLine) {
    List<OrderLine> regOLinetList = new ArrayList<OrderLine>();
    Order regOrder = null;
    if (ListOrderLine.size() > 0) {

      if (ListOrderLine.get(0).getSvfpaStockLocation() == null) {
        throw new OBException("Stock Location not found");
      }

      GlobalSourceOrg = ListOrderLine.get(0).getSalesOrder().getOrganization();// ListOrderLine.get(0).getSvfpaStockLocation().getSvfpaOrgsource();
      GlobalTargetOrg = ListOrderLine.get(0).getSvfpaStockLocation().getSvfpaOrgsource();
      GlobalWarehouse = ListOrderLine.get(0).getSvfpaStockLocation();

      regOrder = createHeaderPO(ListOrderLine.get(0).getSalesOrder());

      if (regOrder == null) {
        return false;
      }

      OBDal.getInstance().save(regOrder);
      OBDal.getInstance().flush();

      int LastNoLine = 0;
      Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
      for (OrderLine regOrderLine : ListOrderLine) {

        LastNoLine = LastNoLine + 10;
        nroLine = Long.parseLong(String.valueOf(LastNoLine));
        OrderLine NewregLine = null;

        NewregLine = createLinePO(regOrder, regOrderLine, nroLine);
        if (NewregLine == null) {
          return false;
        }
        OBDal.getInstance().save(NewregLine);
        OBDal.getInstance().flush();

        regOrderLine.setSvfpaPurchaseorder(regOrder);
        OBDal.getInstance().save(regOrderLine);

        OrderReference regLinkSOToPO = LinkSOToPO.create(regOrderLine, NewregLine,
            regOrderLine.getOrderedQuantity());

        if (regLinkSOToPO == null) {
          throw new OBException(
              "There was an error creating Sales Order Reference (SalesOrderLine: "
                  + regOrderLine.getIdentifier() + ")");
        }
        OBDal.getInstance().save(regLinkSOToPO);
        OBDal.getInstance().flush();

        // regOLinetList.add(NewregLine);

      }

      try {

        // regOrder.setOrderLineList(regOLinetList);
        OBDal.getInstance().save(regOrder);
        // OBDal.getInstance().flush();
        String ID = regOrder.getId();
        // OBDal.getInstance().commitAndClose();

        // --
        if (GetFunctions.OrderPost(ID, SVFSV_C_ORDER_POST_Process_ID) == null) {

          try {

            OBDal.getInstance().rollbackAndClose();

            // Order regOrderToRemove = OBDal.getInstance().get(Order.class, ID);
            // OBDal.getInstance().remove(regOrderToRemove);
            // OBDal.getInstance().flush();
            // OBDal.getInstance().commitAndClose();
          } catch (Exception e) {
            e.printStackTrace();
            // log.error(e);

          }
        }
        //
        LastNoLine = 0;
        nroLine = Long.parseLong(String.valueOf(LastNoLine));
        for (OrderLine regOrderLine : ListOrderLine) {
          LastNoLine = LastNoLine + 10;
          nroLine = Long.parseLong(String.valueOf(LastNoLine));
          if (regOrderLine.getSvfpaRemotepo() != null) {
            updateISO_Remote(regOrder, nroLine, regOrderLine);
          }
          //
        }
      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return false;
      }
      return true;

    }
    return false;

  }

  public static Boolean updateISO_Remote(Order preg_PO, Long pNroLine, OrderLine pOrderLineSO) {
    Boolean isUpdate = false;
    final OBCriteria<Order> orderList = OBDal.getInstance().createCriteria(Order.class);
    //
    orderList.add(Restrictions.eq(Order.PROPERTY_INTERCOORIGORDER, preg_PO));
    CancelActionsUtil.viewPolice("list: " + orderList.list().size());
    for (Order regOrder : orderList.list()) {
      final OBCriteria<OrderLine> orderLineList = OBDal.getInstance()
          .createCriteria(OrderLine.class);
      orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, regOrder));
      orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_LINENO, pNroLine));
      for (OrderLine regOrderLine : orderLineList.list()) {
        regOrderLine.setSvfpaRemotepo(pOrderLineSO.getSvfpaRemotepo());
        //
        regOrderLine.setSvfpaIgnorechkpa(true);
        regOrderLine.setSvfpaAction("WAITINGFORPO");
        regOrderLine.setSvfpaStockavailability("OUTOFSTOCK");
        //
        OBDal.getInstance().save(regOrderLine);
        //
        OrderLine idRemotePO = GetFunctions.getOrderLineByDocumentNo(regOrderLine);
        if (idRemotePO != null) {
          OrderReference regLinkSOToPO = LinkSOToPO.create(regOrderLine, idRemotePO,
              regOrderLine.getOrderedQuantity());
          OBDal.getInstance().save(regLinkSOToPO);
          OBDal.getInstance().flush();
        } else {
          throw new OBException(
              "Purchase Remote not found (" + pOrderLineSO.getSvfpaRemotepo() + ")");
        }
      }
    } // for
    isUpdate = true;
    //
    return isUpdate;
  }// updateISO_Remote
}// Intercompany
