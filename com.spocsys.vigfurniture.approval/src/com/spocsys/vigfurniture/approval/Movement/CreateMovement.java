package com.spocsys.vigfurniture.approval.Movement;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.materialmgmt.onhandquantity.Reservation;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.warehouse.pickinglist.PickingList;
import org.openbravo.dal.core.SessionHandler;

import com.spocsys.vigfurniture.approval.utils.GetFunctions;
import com.spocsys.vigfurniture.approval.utils.ReservationUtils;
import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

public class CreateMovement {
  private static final Logger log = Logger.getLogger(CreateMovement.class);

  static InternalMovement createHeader(OrderLine regOrderLine) {

    try {

      InternalMovement regMovement = OBProvider.getInstance().get(InternalMovement.class);

      // ----------------------------
      regMovement.setClient(regOrderLine.getClient());
      regMovement.setOrganization(regOrderLine.getOrganization());
      regMovement.setCreatedBy(OBContext.getOBContext().getUser());
      regMovement.setUpdatedBy(OBContext.getOBContext().getUser());
      regMovement.setActive(true);
      // -----------------------------
      regMovement.setName("From Order: " + regOrderLine.getSalesOrder().getDocumentNo());
      regMovement.setMovementDate(new Date());
      regMovement.setPosted("N");
      regMovement.setProcessed(false);
      regMovement.setMoveBetweenLocators(false);
      // ------------------------------

      // regMovement.setInitialWarehouse(GetFunctions.getLocator(regOrderLine.getWarehouse()));

      // regMovement.setTransitWarehouse(null);
      // regMovement.setFinalWarehouse(regTableTransf.getFinalWarehouse());

      return regMovement;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  static InternalMovementLine createMovementLine(PickingList regPickingList, OrderLine regOrderLine,
      InternalMovement regMovement, Product regProduct, BigDecimal Qty, long Line,
      Reservation reserObj) {

    try {

      InternalMovementLine regMovementLine = OBProvider.getInstance()
          .get(InternalMovementLine.class);
      // ---------------------------------------------
      regMovementLine.setClient(regOrderLine.getClient());
      regMovementLine.setOrganization(regOrderLine.getOrganization());
      regMovementLine.setCreatedBy(OBContext.getOBContext().getUser());
      regMovementLine.setUpdatedBy(OBContext.getOBContext().getUser());
      regMovementLine.setActive(true);
      // ------------------------------
      regMovementLine.setMovement(regMovement);
      SalesOrderUtil.viewPolice("GetFunctions.getLocatorReserved");
      Locator regStoraBin = GetFunctions.getLocatorReserved(regOrderLine);
      if (regStoraBin != null) {
        regMovementLine.setStorageBin(regStoraBin);
      } else {
        SalesOrderUtil.viewPolice("GetFunctions.getLocatorWareHouse");
        regStoraBin = GetFunctions.getLocatorWareHouse(regOrderLine.getSalesOrder().getWarehouse());
        if (regStoraBin != null) {
          regMovementLine.setStorageBin(regStoraBin);
        } else {
          throw new OBException("Storagebin not found");
        }
      }
      regMovementLine.setNewStorageBin(regPickingList.getOutboundStorageBin());
      regMovementLine.setLineNo(Line);
      regMovementLine.setMovementQuantity(Qty);
      regMovementLine
          .setDescription("From Order No: " + regOrderLine.getSalesOrder().getDocumentNo());
      regMovementLine.setProduct(regProduct);
      regMovementLine.setAttributeSetValue(regProduct.getAttributeSetValue());
      regMovementLine.setUOM(regProduct.getUOM());
      regMovementLine.setOBWPLWarehousePickingList(regPickingList);
      if(regOrderLine != null){
    	  regMovementLine.setCustgsOrderline(regOrderLine);
      }
      // Reservation
      // Reservation reserId =
      // ReservationUtils.createReserveFromSalesOrderLine(regOrderLine);
      if (reserObj != null) {
        regMovementLine.setStockReservation(reserObj);
      }
      // ------------------------------
      return regMovementLine;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static InternalMovement create(List<OrderLine> ListOrderLine, PickingList regPickingList) {

    List<InternalMovementLine> regMovementList = new ArrayList<InternalMovementLine>();
    List<Reservation> reservationtList = new ArrayList<Reservation>();
    InternalMovement regMovement = null;
    if (ListOrderLine.size() > 0) {
      regMovement = createHeader(ListOrderLine.get(0));

      if (regMovement == null) {
        return null;
      }
      //
      // Reservations
      for (OrderLine regLine : ListOrderLine) {
        try {
          SalesOrderUtil.viewPolice("ReservationUtils.createReserveFromSalesOrderLine");
          Reservation reserId = ReservationUtils.getReservation(regLine, "DR");
          if (reserId == null) {
            reserId = ReservationUtils.createReserveFromSalesOrderLine(regLine, false);
            if (reserId != null) {
              OBDal.getInstance().save(reserId);
            } else {
              SalesOrderUtil.viewPolice("Stock Reservation is null");
            }
          } else {
            OBDal.getInstance().save(reserId);
          }
        } catch (Exception e) {
          e.printStackTrace();
          log.error(e.getMessage());
        }
      }
      OBDal.getInstance().flush();
      //
      SessionHandler.getInstance().commitAndStart();
      int LastNoLine = 0;
      Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
      for (OrderLine regOrderLine : ListOrderLine) {

        LastNoLine = LastNoLine + 10;
        nroLine = Long.parseLong(String.valueOf(LastNoLine));
        InternalMovementLine regMovementLine = null;
        //
	//Product productReview = OBDal.getInstance().get(Product.class, regOrderLine.getProduct().getId());
	SessionHandler.getInstance().commitAndStart();
        SalesOrderUtil.viewPolice("isBOM: " + regOrderLine.getProduct().isBillOfMaterials());
	//SalesOrderUtil.viewPolice("isBOM: " + productReview.isBillOfMaterials());
        if (regOrderLine.getProduct().isBillOfMaterials() == true) {
	//if (productReview.isBillOfMaterials() == true) {
          for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {
	  //for (ProductBOM regProductBOM : productReview.getProductBOMList()) {
            regMovementLine = createMovementLine(regPickingList, regOrderLine, regMovement,
                regProductBOM.getBOMProduct(),
                regProductBOM.getBOMQuantity().multiply(regOrderLine.getOrderedQuantity()), nroLine,
                ReservationUtils.getReservation(regOrderLine, "DR"));
            LastNoLine = LastNoLine + 10;
            nroLine = Long.parseLong(String.valueOf(LastNoLine));
            if (regMovementLine != null) {
              regMovementList.add(regMovementLine);
            } else {
              return null;
            }
          }
        } else {
          regMovementLine = createMovementLine(regPickingList, regOrderLine, regMovement,
              regOrderLine.getProduct(), regOrderLine.getOrderedQuantity(), nroLine,
              ReservationUtils.getReservation(regOrderLine, "DR"));
          if (regMovementLine != null) {
            regMovementList.add(regMovementLine);
          }
        }
        if (regMovementLine == null) {
          return null;
        }
      } // for

      try {
        regMovement.setMaterialMgmtInternalMovementLineList(regMovementList);
        OBDal.getInstance().save(regMovement);
        // OBDal.getInstance().flush();
        // OBDal.getInstance().commitAndClose();
      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return null;
      }
      return regMovement;

    }
    return null;

  }
}
