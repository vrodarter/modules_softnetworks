package com.spocsys.vigfurniture.approval.SourceList;

import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.ApprovedVendor;

import com.spocsys.vigfurniture.approval.utils.GetFunctions;
import com.spocsys.vigfurnitures.services.SVFSV_Source;

public class InsertSourceList {
  private static final Logger log = Logger.getLogger(SVFSV_Source.class);

  static SVFSV_Source createRecord(OrderLine regOrderLine) {

    try {

      SVFSV_Source regSource = OBProvider.getInstance().get(SVFSV_Source.class);

      // ----------------------------
      regSource.setNewOBObject(true);
      regSource.setClient(regOrderLine.getClient());

      regSource.setOrganization(regOrderLine.getOrganization());

      regSource.setCreatedBy(OBContext.getOBContext().getUser());

      regSource.setUpdatedBy(OBContext.getOBContext().getUser());

      //
      ApprovedVendor bPurchase = GetFunctions.getPurchasing(regOrderLine.getProduct());
      if (bPurchase != null) {
        regSource.setBpartner(bPurchase.getBusinessPartner());
      } else if (regOrderLine.getSalesOrder().getSalesRepresentative() != null) {
        regSource.setBpartner(
            regOrderLine.getSalesOrder().getSalesRepresentative().getBusinessPartner());
      }
      //
      regSource.setOrder(regOrderLine.getSalesOrder());

      regSource.setProduct(regOrderLine.getProduct());

      regSource.setOrderline(regOrderLine);

      if (regOrderLine.getSvfpaPurchaseorder() != null)
        regSource.setOrderReference(regOrderLine.getSvfpaPurchaseorder().getId());
      else
        regSource.setOrderReference(" ");

      if (regOrderLine.getOrderedQuantity() != null)
        regSource.setQty(regOrderLine.getOrderedQuantity().longValue());

      regSource.setOrderdate(regOrderLine.getSalesOrder().getOrderDate());

      if (regOrderLine.getSalesOrder().getBusinessPartner() != null)
        regSource.setCustomerCBpartner(regOrderLine.getSalesOrder().getBusinessPartner());

      // ------------------------------

      return regSource;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static boolean create(List<OrderLine> ListOrderLine) {

    SVFSV_Source regSource = null;

    if (ListOrderLine.size() > 0) {

      for (OrderLine regOrderLine : ListOrderLine) {

        if (regOrderLine.getSvfpaAction().compareTo("SOURCE") == 0) {
          regSource = null;
          regSource = createRecord(regOrderLine);
          try {
            OBDal.getInstance().save(regSource);
            updateDropShipOrderLine(regOrderLine);
            // OBDal.getInstance().flush();
            // OBDal.getInstance().commitAndClose();

          } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return false;
          } finally {

          }
        }
      }
    }

    return true;
  }

  public static boolean updateDropShipOrderLine(OrderLine objOrderLine) {
    boolean isUpdate = false;
    boolean status = false;
    try {
      //
      OrderLine oOrderLine = OBDal.getInstance().get(OrderLine.class, objOrderLine.getId());
      if ((boolean) objOrderLine.getSalesOrder().get(Order.PROPERTY_SVFDSDROPSHIP)) {
        status = true;
      }
      oOrderLine.setSvfdsDropship(status);
      OBDal.getInstance().save(oOrderLine);
      isUpdate = true;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    //
    return isUpdate;
  }
}
