package com.spocsys.vigfurniture.approval.Transfer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.approval.utils.GetFunctions;
import com.spocsys.vigfurniture.transfer.Movement;
import com.spocsys.vigfurniture.transfer.TableTransf;

public class CreateTransferDocument {
  private static final Logger log = Logger.getLogger(Movement.class);

  // static Movement createHeader(Order regOrder) {
  //
  // try {
  //
  // Movement regMovement = OBProvider.getInstance().get(Movement.class);
  //
  // // ----------------------------
  // regMovement.setClient(regOrder.getClient());
  // regMovement.setOrganization(regOrder.getOrganization());
  // regMovement.setCreatedBy(OBContext.getOBContext().getUser());
  // regMovement.setUpdatedBy(OBContext.getOBContext().getUser());
  // // -----------------------------
  // regMovement.setName("Order No: " + regOrder.getDocumentNo());
  // regMovement.setMovementDate(new Date());
  // // ------------------------------
  //
  // if ((regOrder.getOrderLineList() != null) && (regOrder.getOrderLineList().size() > 0)) {
  //
  // if (regOrder.getOrderLineList().get(0).getSvfpaStockLocation() != null) {
  // regMovement.setInitialWarehouse(GetFunctions.getLocator(regOrder.getOrderLineList()
  // .get(0).getSvfpaStockLocation()));
  // }
  //
  // regMovement.setTransitWarehouse(null);
  //
  // if (regOrder.getOrderLineList().get(0).getSvfpaShipFrom() != null) {
  // regMovement.setFinalWarehouse(GetFunctions.getLocator(regOrder.getOrderLineList().get(0)
  // .getSvfpaShipFrom()));
  // }
  //
  // }
  //
  // return regMovement;
  // } catch (Exception e) {
  // e.printStackTrace();
  // log.error(e.getMessage());
  // }
  // return null;
  // }

  // static Movementline createMovementLine(OrderLine regOrderLine, Movement regMovement,
  // Product regProduct, BigDecimal Qty, long Line) {
  //
  // try {
  //
  // Movementline regMovementLine = OBProvider.getInstance().get(Movementline.class);
  // // ---------------------------------------------
  //
  // regMovementLine.setClient(regOrderLine.getClient());
  // regMovementLine.setOrganization(regOrderLine.getOrganization());
  // regMovementLine.setCreatedBy(OBContext.getOBContext().getUser());
  // regMovementLine.setUpdatedBy(OBContext.getOBContext().getUser());
  // // ------------------------------
  // regMovementLine.setMovement(regMovement);
  // regMovementLine.setLineNo(Line);
  // regMovementLine.setMovementQuantity(Qty);
  // regMovementLine.setProduct(regProduct);
  // regMovementLine.setAttributeSetValue(regOrderLine.getAttributeSetValue());
  // regMovementLine.setUOM(regOrderLine.getUOM());
  // // ------------------------------
  // return regMovementLine;
  // } catch (Exception e) {
  // e.printStackTrace();
  // log.error(e.getMessage());
  // }
  //
  // return null;
  // }

  // public static boolean create(List<OrderLine> ListOrderLine) {
  //
  // List<Movementline> regMovementList = new ArrayList<Movementline>();
  // Movement regMovement = null;
  // if (ListOrderLine.size() > 0) {
  // regMovement = createHeader(ListOrderLine.get(0).getSalesOrder());
  //
  // if (regMovement == null) {
  // return false;
  // }
  // int LastNoLine = 0;
  // Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
  // for (OrderLine regOrderLine : ListOrderLine) {
  //
  // LastNoLine = LastNoLine + 10;
  // nroLine = Long.parseLong(String.valueOf(LastNoLine));
  // Movementline regMovementLine = null;
  //
  // if (regOrderLine.getProduct().isBillOfMaterials() == true) {
  // for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {
  //
  // nroLine = Long.parseLong(String.valueOf(LastNoLine));
  // regMovementLine = createMovementLine(regOrderLine, regMovement,
  // regProductBOM.getBOMProduct(),
  // regProductBOM.getBOMQuantity().multiply(regOrderLine.getOrderedQuantity()), nroLine);
  // LastNoLine = LastNoLine + 10;
  //
  // if (regMovementLine == null) {
  // return false;
  // }
  // regMovementList.add(regMovementLine);
  //
  // }
  //
  // } else {
  //
  // regMovementLine = createMovementLine(regOrderLine, regMovement,
  // regOrderLine.getProduct(), regOrderLine.getOrderedQuantity(), nroLine);
  // if (regMovementLine == null) {
  // return false;
  // }
  // regMovementList.add(regMovementLine);
  //
  // }
  //
  // }
  //
  // try {
  // regMovement.setSVFTRAFMovementlineList(regMovementList);
  // OBDal.getInstance().save(regMovement);
  // OBDal.getInstance().flush();
  // OBDal.getInstance().commitAndClose();
  //
  // } catch (Exception e) {
  // e.printStackTrace();
  // log.error(e.getMessage());
  // return false;
  // }
  // return true;
  //
  // }
  // return false;
  //
  // }

  static TableTransf createTableTransfer(OrderLine regOrderLine) {

    try {
      Locator storeBin = null;
      TableTransf regTableTransf = OBProvider.getInstance().get(TableTransf.class);

      // ----------------------------
      regTableTransf.setClient(regOrderLine.getClient());
      regTableTransf.setOrganization(regOrderLine.getOrganization());
      regTableTransf.setCreatedBy(OBContext.getOBContext().getUser());
      regTableTransf.setUpdatedBy(OBContext.getOBContext().getUser());
      // -----------------------------
      regTableTransf.setMovementDate(new Date());
      //
      storeBin = GetFunctions.getLocatorWareHouse(regOrderLine.getSvfpaStockLocation());
      if (storeBin != null) {
        regTableTransf.setInitialWarehouse(storeBin);
      } else {
        if (regOrderLine.getSvfpaStockLocation() != null) {
          regTableTransf
              .setInitialWarehouse(regOrderLine.getSvfpaStockLocation().getLocatorList().get(0));
        }
      }
      if (regOrderLine.getSvfpaShipFrom() != null) {
        storeBin = GetFunctions.getLocatorWareHouse(regOrderLine.getSvfpaShipFrom());
        if (storeBin != null) {
          regTableTransf.setFinalWarehouse(storeBin);
        } else {
          regTableTransf.setFinalWarehouse(regOrderLine.getSvfpaShipFrom().getLocatorList().get(0));
        }
      }
      //
      regTableTransf.setProduct(regOrderLine.getProduct());
      regTableTransf.setMovementQuantity(regOrderLine.getOrderedQuantity());
      regTableTransf.setAttributeSetValue(regOrderLine.getAttributeSetValue());
      regTableTransf.setUOM(regOrderLine.getUOM());
      regTableTransf.setSalesOrderLine(regOrderLine);

      return regTableTransf;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static boolean create(List<OrderLine> ListOrderLine) {
    TableTransf regTableTransf = null;
    if (ListOrderLine.size() > 0) {
      List<TableTransf> listTransfer = new ArrayList<TableTransf>();
      for (OrderLine regOrderLine : ListOrderLine) {
        regTableTransf = createTableTransfer(regOrderLine);
        if (regTableTransf == null) {
          return false;
        }
        listTransfer.add(regTableTransf);
      }

      try {

        // OBDal.getInstance().save(regTableTransf);
        for (TableTransf regTransfer : listTransfer) {
          OBDal.getInstance().save(regTransfer);
        }
        // OBDal.getInstance().flush();
        // OBDal.getInstance().commitAndClose();

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return false;
      }
      return true;

    }
    return false;

  }

}
