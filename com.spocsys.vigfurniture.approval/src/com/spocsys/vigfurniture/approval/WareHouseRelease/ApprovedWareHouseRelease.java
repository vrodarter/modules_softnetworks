/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.WareHouseRelease;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

//

/**
 */
public class ApprovedWareHouseRelease implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    SalesOrderUtil.viewPolice(this.getClass().getName());
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables()
          .get(SalesOrderConstants.ORDER_ID_WF_PARAM);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      SalesOrderUtil.viewPolice("cOrderId: " + cOrderId);

      objOrder.setSvfapprWhRelease(true);
      OBDal.getInstance().save(objOrder);
      //
      execution.setVariable(SalesOrderConstants.ACTION_WF_NUMRECORD, 1);
      execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
          SalesOrderConstants.ACTION_CANCEL_PACK);
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
