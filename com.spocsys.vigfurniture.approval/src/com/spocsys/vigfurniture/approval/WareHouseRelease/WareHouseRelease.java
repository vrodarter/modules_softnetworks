/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.WareHouseRelease;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

//

/**
 */
public class WareHouseRelease implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    SalesOrderUtil.viewPolice(this.getClass().getName());
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    String nextStep = null;
    try {
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final String decision = (String) execution.getVariables().get(
          SalesOrderConstants.SALES_ORDER_WF_DECISION);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      SalesOrderUtil.viewPolice("cOrderId: " + cOrderId);
      if (SalesOrderUtil.isValidSalesOrder(objOrder)) {
        //
        OBDal.getInstance().save(objOrder);
        //
        if (decision != null) {
          if (decision.equals("Y")) {
            nextStep = SalesOrderConstants.ACTION_APPROVAL;
          } else {
            objOrder.setSvfpaManualHold(false);
            objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_NOT_APPROVED_RELEASE);
            nextStep = SalesOrderConstants.ACTION_NOT_APPROVAL;
          }
          OBDal.getInstance().save(objOrder);
        } else
          nextStep = SalesOrderConstants.ACTION_NOT_APPROVAL;
      } else
        nextStep = SalesOrderConstants.ACTION_NOT_APPROVAL;
      //
      execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, nextStep);
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
