package com.spocsys.vigfurniture.approval.WarehousePickList;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.warehouse.pickinglist.PickingList;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.service.db.DalConnectionProvider;

import com.spocsys.vigfurniture.approval.Goods.CreateGoodsDocument;
import com.spocsys.vigfurniture.approval.Movement.CreateMovement;
import com.spocsys.vigfurniture.approval.utils.GetFunctions;

public class WarehousePickList {

  private static final Logger log = Logger.getLogger(WarehousePickList.class);

  static PickingList createHeader(Order regOrder) {

    try {

      PickingList regPickingList = OBProvider.getInstance().get(PickingList.class);

      // ----------------------------
      regPickingList.setClient(regOrder.getClient());
      regPickingList.setOrganization(regOrder.getOrganization());
      regPickingList.setCreatedBy(OBContext.getOBContext().getUser());
      regPickingList.setUpdatedBy(OBContext.getOBContext().getUser());

      // -----------------------------
      regPickingList.setDocumentNo("From Order: " + regOrder.getDocumentNo());
      regPickingList.setPickliststatus("DR");
      regPickingList.setProcessProcess(false);
      regPickingList.setProcessValidate(false);
      regPickingList.setDocumentType(GetFunctions.getDocument("OBWPL_doctype", "%outbound%",
          regOrder.getOrganization(), true));
      System.out.println("Organization regOrder = " + regOrder.getOrganization().getName() + "\n");
      System.out.println("GetFunctions.getDocument = " + GetFunctions.getDocument("OBWPL_doctype", "%outbound%",
          regOrder.getOrganization(), true).toString() + "\n");
      if (regPickingList.getDocumentType().isOBWPLUseOutbound()) {
        Locator regLocator = GetFunctions.getLocatorType(regOrder.getWarehouse(), "OUT", "Prep");
        if (regLocator != null) {
          regPickingList.setOutboundStorageBin(regLocator);
        }
      }
      regPickingList.setSVFPARoute(regOrder.getSvfpaRoute());
      regPickingList.setDescription("Order No: " + regOrder.getDocumentNo()
          + " and Business Partner: " + regOrder.getBusinessPartner().getName());
      regPickingList.setDocumentdate(new Date());
      regPickingList.setProcessCancel(false);
      regPickingList.setSelectOrders(false);
      regPickingList.setProcessAssign(false);
      regPickingList.setClose(false);
      regPickingList.setReAssign(false);
      if (regOrder.getSnmtSalesChannel()!=null){
    	  regPickingList.setSsplSalesChannel(regOrder.getSnmtSalesChannel());
      }
      if (regOrder.getDlmgtoShippingMethod()!=null){
    	  regPickingList.setSsplShippingMethod(regOrder.getDlmgtoShippingMethod());
      }
      if (regOrder.getDlmgtoShippingName()!=null){
    	  regPickingList.setSsplShippingName(regOrder.getDlmgtoShippingName());
      }
      if (regOrder.getDlmgtoChannelOrder()!=null){
          regPickingList.setSsplChannelOrder(regOrder.getDlmgtoChannelOrder());
      }
      regPickingList.setSsplVip(regOrder.isSvfpaVip());
      ConnectionProvider connectionProvider = new DalConnectionProvider(); //OBDal.getInstance().getConnection();
      String strisSingleLine = WarehousePickListData.isSingleLine(connectionProvider, regOrder.getId());
      if(strisSingleLine.equals("1")){
    	  regPickingList.setSsplSingleline(true);
    	  String strBin = WarehousePickListData.getBin(connectionProvider, regOrder.getId());
    	  Locator locatorSL = OBDal.getInstance().get(Locator.class, strBin);
    	  regPickingList.setSsplStoreBin(locatorSL);
      }

      return regPickingList;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static boolean create(List<OrderLine> ListOrderLine) {

    PickingList regPickingList = null;

    if (ListOrderLine.size() > 0) {
      regPickingList = createHeader(ListOrderLine.get(0).getSalesOrder());

      if (regPickingList == null) {
        return false;
      }
      if (regPickingList.getDocumentType().isOBWPLUseOutbound()) {
        CreateMovement objMov = new CreateMovement();
        InternalMovement regMovement = objMov.create(ListOrderLine, regPickingList);
        if (regMovement == null) {
          return false;
        }
      } else {
        CreateGoodsDocument Obj = new CreateGoodsDocument();
        ShipmentInOut regShipmentInOut = Obj.create(ListOrderLine, regPickingList);

        if (regShipmentInOut == null) {
          return false;
        }
      }
      try {

        OBDal.getInstance().save(regPickingList);
        // OBDal.getInstance().flush();
        // OBDal.getInstance().commitAndClose();

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return false;
      }
      return true;

    }
    return false;

  }
}
