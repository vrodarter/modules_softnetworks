/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.ad_process.salesOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.activiti.engine.task.Task;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.approval.Intercompany.Intercompany;
import com.spocsys.vigfurniture.approval.SourceList.InsertSourceList;
import com.spocsys.vigfurniture.approval.Transfer.CreateTransferDocument;
import com.spocsys.vigfurniture.approval.WarehousePickList.WarehousePickList;
import com.spocsys.vigfurniture.approval.purchaseOrder.approvalCreatePO;
import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

/**
 * 
 */

public class ProcessOrderLineSalesOrder {

  private static final Logger log = Logger.getLogger(ProcessOrderLineSalesOrder.class);
  private static final Boolean isDebug = true;

  @OneToMany(fetch = FetchType.EAGER)
  public static Map<String, java.util.List<OrderLine>> mapOrder = new HashMap<String, List<OrderLine>>();
  private static SessionFactory factory;

  public static List<Task> getInvolvedTasks(BaseOBObject bob) {
    return null;
  }

  public void ProcessOrderLineSalesOrder() {
    mapOrder = new HashMap<String, List<OrderLine>>();
    factory = new Configuration().configure().buildSessionFactory();
    mapOrder.clear();
  }

  public Map<String, List<OrderLine>> getGroups() {
    return mapOrder;
  }

  public Boolean forEveryOrderLine(OrderLine pOrderLine) {
    OrderLine obOrderLine = OBDal.getInstance().get(OrderLine.class, pOrderLine.getId());
    //
    Boolean forEveryOrderLine = false;
    Boolean isOk = false;
    Boolean isPreprocess = false;
    Boolean isProcess = false;
    //
    if (pOrderLine.getSvfpaStockavailability() != null && pOrderLine.getSvfpaAction() != null) {

      if (!(Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPAPROCESSED)) {
        final OrderLine objOrderLine = setFlagProcessLine(obOrderLine);
        obOrderLine = objOrderLine;
        if (isDebug && !(Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPAPROCESSED)) {
          SalesOrderUtil.viewPolice("process line: " + obOrderLine.getSalesOrder().getDocumentNo()
              + "-" + obOrderLine.getSalesOrder().getSvfpaStockStatus() + "-"
              + obOrderLine.getSalesOrder().getSVFPAAction() + "-"
              + obOrderLine.getSvfpaStockavailability() + "-" + obOrderLine.getSvfpaAction() + "-"
              + obOrderLine.getSalesOrder().get(Order.PROPERTY_SVFPAALLSTOCKIN) + "-"
              + obOrderLine.getSalesOrder().get(Order.PROPERTY_SVFPAFINANCIALAPPROVED) + "-"
              + obOrderLine.getSalesOrder().get(Order.PROPERTY_SVFPAMANUALHOLD) + "/"
              + obOrderLine.get(OrderLine.PROPERTY_SVFPAPROCESSED) + "-"
              + obOrderLine.get(OrderLine.PROPERTY_SVFAPPRNEEDPREPROCESS) + "-"
              + obOrderLine.get(OrderLine.PROPERTY_SVFAPPRPREPROCESS));
        }
        if (objOrderLine.getProduct().getProductType().equals("S")) {
          obOrderLine.setSvfpaProcessed(true);
          // obOrderLine.setSvfpaStatus("Sourcing");
          OBDal.getInstance().save(obOrderLine);
        } else {

          if ((Boolean) objOrderLine.get(OrderLine.PROPERTY_SVFAPPRNEEDPREPROCESS)) {
            if ((Boolean) objOrderLine.get(OrderLine.PROPERTY_SVFAPPRPREPROCESS)) {
              // next line
              isOk = true;
            } else {
              if (objOrderLine.getSvfpaStockavailability().equals(SalesOrderConstants.CONF_OUTSTOCK)
                  && objOrderLine.getSvfpaAction().equals("WAITINGFORPO")) {
                isPreprocess = true;
                isOk = true;
              } else if (objOrderLine.getSvfpaStockavailability()
                  .equals(SalesOrderConstants.CONF_OUTSTOCK)
                  && (objOrderLine.getSvfpaAction().equals("ISSUEPO"))) {
                if (!(Boolean) objOrderLine.getSalesOrder()
                    .get(Order.PROPERTY_SVFAPPRDEPFAPAPPROVED)) {
                  setStatusHeaderOrder(objOrderLine.getSalesOrder().getId(),
                      SalesOrderConstants.SO_ACTION_WAIT_DEPOSIT_FAP);
                  isOk = true;
                  // final Order objOrder = OBDal.getInstance().get(Order.class,
                  // objOrderLine.getSalesOrder().getId());
                  // objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_FAP);
                  // OBDal.getInstance().save(objOrder);
                } else {
                  SalesOrderUtil.viewPolice("Execute action, Line Status = Issue PO");
                  if (groupActionWarehouse(objOrderLine, "pre")) {
                    obOrderLine.setSvfpaStatus(SalesOrderConstants.SO_ACTION_ISSUE_PO);
                    isPreprocess = true;
                    isOk = true;
                  }
                }
                // source
              } else if (objOrderLine.getSvfpaStockavailability()
                  .equals(SalesOrderConstants.CONF_OUTSTOCK)
                  && objOrderLine.getSvfpaAction().equals("SOURCE")) {
                SalesOrderUtil.viewPolice("execute action Line Status = Sourcing");
                if (groupActionWarehouse(objOrderLine, "pre")) {
                  obOrderLine.setSvfpaStatus("Sourcing");
                  isPreprocess = true;
                  isOk = true;
                }
              } else if (objOrderLine.getSvfpaStockavailability()
                  .equals(SalesOrderConstants.CONF_INSTOCK)
                  && objOrderLine.getSvfpaAction().equals("TRANSFER")) {
                SalesOrderUtil.viewPolice("Execute action, Line Status = Transfer");
                if (groupActionWarehouse(objOrderLine, "pre")) {
                  obOrderLine.setSvfpaStatus(SalesOrderConstants.SO_ACTION_TRANSFER);
                  isPreprocess = true;
                  isOk = true;
                }
                // execute action: Lines Status = Transfer
              } else if ((objOrderLine.getSvfpaStockavailability()
                  .equals(SalesOrderConstants.CONF_INSTOCK)
                  || objOrderLine.getSvfpaStockavailability()
                      .equals(SalesOrderConstants.CONF_OUTSTOCK))
                  && objOrderLine.getSvfpaAction().equals("INTERCOMPANY")) {
                SalesOrderUtil.viewPolice("Execute action: Lines Status = Intercompany");
                if (groupActionWarehouse(objOrderLine, "pre")) {
                  obOrderLine.setSvfpaStatus(SalesOrderConstants.SO_ACTION_INTERCOMPANY);
                  setProcessedOrderLine(obOrderLine, true);
                  //
                  if (SalesOrderUtil.allItemStatus(objOrderLine.getSalesOrder(),
                      OrderLine.PROPERTY_SVFPAACTION, "INTERCOMPANY").equals("N")) {
                    final Order objOrder = OBDal.getInstance().get(Order.class,
                        objOrderLine.getSalesOrder().getId());
                    objOrder.setSvfpaSplit(true);
                    OBDal.getInstance().save(objOrder);
                  }
                  isPreprocess = true;
                  isProcess = true;
                  isOk = true;
                }
              }
            }
            if (isPreprocess) {
              obOrderLine.setSvfapprPreProcess(isPreprocess);
              OBDal.getInstance().save(obOrderLine);
            }
            if (isProcess) {
              obOrderLine.setSvfpaProcessed(isProcess);
              OBDal.getInstance().save(obOrderLine);
            }
          } else {
            // all-in
            SalesOrderUtil.viewPolice("all-in");
            if ((Boolean) obOrderLine.getSalesOrder().get(Order.PROPERTY_SVFPAALLSTOCKIN)) {
              SalesOrderUtil.viewPolice("allInLineProcess");
              forEveryOrderLine = allInLineProcess(obOrderLine);
              isOk = true;
            } else {
              if (groupActionWarehouse(obOrderLine, "pro")) {
                obOrderLine.setSvfpaStatus(SalesOrderConstants.SO_ACTION_ISSUED);
                setProcessedOrderLine(obOrderLine, true);
                OBDal.getInstance().save(obOrderLine);
                isOk = true;
              }
            }
          }
        }
      }
    }
    return isOk;
  }

  public static Boolean allInLineProcess(OrderLine objOrderLine) {
    Boolean allIn = false;
    //
    /*
     * if ((Boolean) objOrderLine.getSalesOrder().get(Order.PROPERTY_SVFPAALLSTOCKIN) &&
     * objOrderLine.getSalesOrder().getSVFPAAction().equals("RTP"))
     */
    if (isConditionValid(objOrderLine.getSalesOrder().getId(),
        OrderLine.PROPERTY_SVFPASTOCKAVAILABILITY, "INSTOCK")
        && isConditionValid(objOrderLine.getSalesOrder().getId(), OrderLine.PROPERTY_SVFPAACTION,
            "RTP")) {
      if ((Boolean) objOrderLine.get(OrderLine.PROPERTY_SVFAPPRCONFIRMED)) {
        setStatusHeaderOrder(objOrderLine.getSalesOrder().getId(),
            SalesOrderConstants.SO_ACTION_WAIT_CUSTOMER);
        // final Order objOrder = OBDal.getInstance().get(Order.class,
        // objOrderLine.getSalesOrder().getId());
        // objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_WAIT_CUSTOMER);
        // OBDal.getInstance().save(objOrder);
      } else {
        if (groupActionWarehouse(objOrderLine, "pro")) {
          setProcessedOrderLine(objOrderLine, true);
          OBDal.getInstance().save(objOrderLine);
        }
      }
    }
    allIn = true;
    //
    return allIn;
  }

  public static void setProcessedOrderLine(OrderLine objOrderLine, Boolean isProcess) {
    final OrderLine lineOrderUpdate = OBDal.getInstance().get(OrderLine.class,
        objOrderLine.getId());
    lineOrderUpdate.setSvfpaProcessed(isProcess);
    OBDal.getInstance().save(lineOrderUpdate);
  }

  public static Boolean isConditionValid(String salesOrderNO, String nameField, String valueField) {
    Boolean isProcess = false;
    final String hql = "select count(" + nameField + ") as noReg, COALESCE(sum(case " + nameField
        + " when '" + valueField + "' then 1 else 0 end),0) as SI from " + OrderLine.ENTITY_NAME
        + " where " + OrderLine.PROPERTY_SALESORDER + ".id = :porder_id and "
        + OrderLine.PROPERTY_ACTIVE + " = 'Y' ";

    final Query query = OBDal.getInstance().getSession().createQuery(hql);
    query.setParameter("porder_id", salesOrderNO);
    //
    Long countReg = Long.valueOf("0");
    Long qtyProcess = Long.valueOf("0");
    //
    for (Object ol : query.list()) {
      // the query returns a list of arrays (columns of the query)
      final Object[] os = (Object[]) ol;
      // get element of result
      countReg = (Long) os[0]; // as noReg
      qtyProcess = (Long) os[1]; // as SI
      //
      // logger.log(countReg.toString() + "|" + qtyProcess.toString() + "|");

      if (countReg.compareTo(qtyProcess) == 0 && countReg.compareTo(Long.valueOf("0")) > 0) {
        isProcess = true;
      }
    } // for

    Order objOrder = OBDal.getInstance().get(Order.class, salesOrderNO);
    //
    if (!objOrder.get(Order.PROPERTY_SVFPAPROCESSED).equals(isProcess)) {
      objOrder.setSvfpaProcessed(isProcess);
    }
    return isProcess;
  }//

  // public static Boolean eXecuteAction(OrderLine objOrderLine) {
  // Boolean isOk = false;
  // String lineAction = (String) objOrderLine.get(OrderLine.PROPERTY_SVFPAACTION);
  // //
  // List<OrderLine> listOrderLine = new ArrayList<OrderLine>();
  // listOrderLine.add(objOrderLine);
  // switch (lineAction) {
  // case "RTP":
  // isOk = WarehousePickList.create(listOrderLine);
  // break;
  // case "SOURCE":
  // isOk = InsertSourceList.create(listOrderLine);
  // break;
  // case "INTERCOMPANY":
  // isOk = Intercompany.create(listOrderLine);
  // break;
  // case "TRANSFER":
  // isOk = CreateTransferDocument.create(listOrderLine);
  // break;
  // case "ISSUEPO":
  // isOk = approvalCreatePO.create(listOrderLine);
  // break;
  // case "DISCONTINUED":
  // case "WAITINGFORSTOCK":
  // case "WAITINGFORPO":
  // isOk = true;
  // break;
  // }
  // return isOk;
  // }

  public static Boolean groupActionWarehouse(OrderLine pOrderLine, String pOrigin) {
    Boolean isOk = false;
    String keyGroup = null;
    //
    keyGroup = pOrderLine.getSvfpaAction() + "-" + pOrderLine.getWarehouse().getId() + "-"
        + pOrigin;
    List<OrderLine> listGroup = mapOrder.get(keyGroup);
    if (listGroup == null) {
      listGroup = new ArrayList<OrderLine>();
    }
    //
    switch (pOrderLine.getSvfpaAction()) {
    case "RTP":
    case "SOURCE":
    case "INTERCOMPANY":
    case "TRANSFER":
    case "ISSUEPO":
      SalesOrderUtil.viewPolice("add: " + pOrderLine.getSvfpaAction()
          + pOrderLine.getSalesOrder().getDocumentNo() + "-" + pOrderLine.getLineNo() + "-"
          + pOrderLine.getSalesOrder().getBusinessPartner().getName());
      listGroup.add(pOrderLine);
      mapOrder.put(keyGroup, listGroup);
      isOk = true;
      break;
    case "DISCONTINUED":
    case "WAITINGFORSTOCK":
    case "WAITINGFORPO":
      isOk = true;
      break;
    }
    return isOk;
  }

  public static boolean setStatusHeaderOrder(String orderId, String strStatus) {
    final Order objOrder = OBDal.getInstance().get(Order.class, orderId);
    objOrder.setSvfpaStatus(strStatus);
    OBDal.getInstance().save(objOrder);
    return true;
  }

  public static OrderLine setFlagProcessLine(OrderLine objOrderLine) {
    boolean needPreProcessed = false;
    boolean isModify = false;
    OrderLine vOrderLine = objOrderLine;
    //
    switch (objOrderLine.getSvfpaStockavailability()) {
    case "INSTOCK":
      switch (objOrderLine.getSvfpaAction()) {
      case "RTP":
        needPreProcessed = false;
        isModify = true;
        break;
      case "TRANSFER":
        needPreProcessed = true;
        isModify = true;
        break;
      case "INTERCOMPANY":
        needPreProcessed = true;
        isModify = true;
        break;
      }
      break;
    case "OUTOFSTOCK":
      switch (objOrderLine.getSvfpaAction()) {
      case "WAITINGFORSTOCK":
        needPreProcessed = false;
        isModify = true;
        break;
      case "SOURCE":
        needPreProcessed = true;
        isModify = true;
        break;
      case "INTERCOMPANY":
        needPreProcessed = true;
        isModify = true;
        break;
      case "WAITINGFORPO":
      case "ISSUEPO":
        needPreProcessed = true;
        isModify = true;
        break;
      }
      break;
    }
    //
    if (isModify) {
      vOrderLine = SalesOrderUtil.updatePreProcessLineSalesOrder(objOrderLine.getId(),
          needPreProcessed);
    }
    //
    return vOrderLine;
  }// setFlagProcessLine

  public boolean processGroup() {
    Boolean isOk = false;
    Boolean excludePick = true;
    //
    // Hibernate.initialize(mapOrder);
    for (String key : mapOrder.keySet()) {
      isOk = false;
      String sOrigin = null;
      String sAction = null;

      SalesOrderUtil.viewPolice("it: " + key + "|" + mapOrder.get(key).size());
      List<OrderLine> listOrderLine = mapOrder.get(key);
      try {
        sAction = getActionKey(key);
        sOrigin = getSourceKey(key);
        List<OrderLine> lOrderLine = new ArrayList<OrderLine>();
        excludePick = true;
        for (OrderLine order : listOrderLine) {
          OrderLine objOrderLine = OBDal.getInstance().get(OrderLine.class, order.getId());
          SalesOrderUtil.viewPolice("element :" + order.getSalesOrder().getDocumentNo() + "-"
              + order.getLineNo() + "-" + order.getSalesOrder().getBusinessPartner().getName());
          //
          if (!(Boolean) order.getSalesOrder().get(Order.PROPERTY_OBWPLREADYPL)) {
            lOrderLine.add(objOrderLine);
            excludePick = false;
          }
          //
        }
        // Hibernate.initialize(lOrderLine);
        switch (sAction) {
        case "RTP":
          if (!excludePick) {
            isOk = WarehousePickList.create(lOrderLine);
          } else {
            isOk = true;
          }
          break;
        case "SOURCE":
          isOk = InsertSourceList.create(lOrderLine);
          break;
        case "INTERCOMPANY":
          isOk = Intercompany.create(lOrderLine);
          break;
        case "TRANSFER":
          isOk = CreateTransferDocument.create(lOrderLine);
          break;
        case "ISSUEPO":
          isOk = approvalCreatePO.create(lOrderLine);
          break;
        }
      } catch (Exception e) {
        e.printStackTrace();
        log.error("Error: " + e.getMessage());
      } finally {
        System.out.println("Process Group Finally: " + isOk + "|" + mapOrder.size());
        if (!isOk) {
          OBDal.getInstance().rollbackAndClose();
        } else {
          OBDal.getInstance().flush();
          OBDal.getInstance().commitAndClose();
        }
      }
      if (!isOk) {
        try {

          isOk = setearAllListPreProcess(listOrderLine, null, false, sOrigin, sAction);
        } catch (Exception e) {
          e.printStackTrace();
          log.error("Error: " + e.getMessage());
        } finally {
          System.out.println("Rollback Process Group Finally: " + isOk + "|" + mapOrder.size());
          if (!isOk) {
            OBDal.getInstance().rollbackAndClose();

          } else {
            OBDal.getInstance().flush();
            OBDal.getInstance().commitAndClose();
          }
        }
      }
    }
    mapOrder.clear();
    SalesOrderUtil.viewPolice("size map: " + mapOrder.size());
    return isOk;
  }

  public boolean setearAllListPreProcess(List<OrderLine> pOrderLine, String pStatusLine,
      Boolean pFlag, String pOrigin, String pAction) {
    Boolean isOk = false;
    //
    for (OrderLine orderLine : pOrderLine) {
      final OrderLine objOrderLine = OBDal.getInstance().get(OrderLine.class, orderLine.getId());
      if (pOrigin.equals("pre")) {
        objOrderLine.setSvfapprPreProcess(pFlag);
        if (pAction.equals("INTERCOMPANY")) {
          objOrderLine.setSvfpaProcessed(pFlag);
        }
      } else if (pOrigin.equals("pro")) {
        objOrderLine.setSvfpaProcessed(pFlag);
      }
      objOrderLine.setSvfpaStatus(pStatusLine);
      OBDal.getInstance().save(objOrderLine);
    }
    isOk = true;
    return isOk;
  }

  public String getActionKey(String pKey) {
    String sAction = null;
    //
    if (pKey.contains("RTP")) {
      sAction = "RTP";
    } else if (pKey.contains("SOURCE")) {
      sAction = "SOURCE";
    } else if (pKey.contains("INTERCOMPANY")) {
      sAction = "INTERCOMPANY";
    } else if (pKey.contains("TRANSFER")) {
      sAction = "TRANSFER";
    } else if (pKey.contains("ISSUEPO")) {
      sAction = "ISSUEPO";
    }
    //
    return sAction;
  }

  public String getSourceKey(String pKey) {
    String sSource = null;
    //
    if (pKey.contains("pre")) {
      sSource = "pre";
    } else if (pKey.contains("pro")) {
      sSource = "pro";
    }
    //
    return sSource;
  }
}
