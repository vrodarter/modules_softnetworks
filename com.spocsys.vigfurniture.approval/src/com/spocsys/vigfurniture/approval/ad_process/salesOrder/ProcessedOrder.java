package com.spocsys.vigfurniture.approval.ad_process.salesOrder;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.quartz.JobExecutionException;
import org.openbravo.dal.core.OBContext;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;
//import java.util.List;

public class ProcessedOrder extends DalBaseProcess {

  private ProcessLogger logger;
  private static final Logger log = Logger.getLogger(Order.class);

  // begin to background process
  public void doExecute(ProcessBundle bundle) throws Exception {
    // this logger logs into the LOG column of
    // the AD_PROCESS_RUN database table
    int counter = 0;

    String sqlQuery = Order.PROPERTY_DOCUMENTTYPE + ".id in ('AD') "; // select doc.id
                                                                      // from "
    // + DocumentType.ENTITY_NAME + " doc where doc." + DocumentType.PROPERTY_DOCUMENTCATEGORY
    // + " = 'SOO')";

    logger = bundle.getLogger();
    logger.log("Starting background search order non Processed: ");
    //
    OBContext.setAdminMode(true);
    try {
      /*
       * final OBCriteria<Order> orderList = OBDal.getInstance().createCriteria(Order.class); //
       * IsSOTrx = 'Y' and em_svfpa_processed = 'N'
       * orderList.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true)); //
       * orderList.add(Restrictions.eq(Order.PROPERTY_PROCESSED, false));
       * orderList.add(Restrictions.eq(Order.PROPERTY_SVFPAPROCESSED, false));
       * orderList.add(Restrictions.sqlRestriction(sqlQuery));
       * orderList.add(Restrictions.not(Restrictions.in(Order.PROPERTY_SVFPASTATUS, new String[] {
       * "Waiting for Manager Approval", "Waiting for Deposit-FAP", "Waiting for Customer Confirm",
       * "HOLD", "New", "Waiting for Financial Approval", "Automatically Approved" })));
      */
      // loop through all order
      ////////////////////
      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS ord");
      queryExtend.append(" WHERE ");
      queryExtend.append(" ord." + Order.PROPERTY_SALESTRANSACTION + " = 'Y' and ");
      queryExtend.append(" ord." + Order.PROPERTY_SVFPAPROCESSED + " = 'N' and ");
      queryExtend.append(" ord." + Order.PROPERTY_SVFPASTATUS
          + " not in ( 'Waiting for Manager Approval', 'Waiting for Deposit-FAP', "
          + "'Waiting for Customer Confirm', 'HOLD', 'New', 'Waiting for Financial Approval' ) and ");
      queryExtend.append(" ord." + Order.PROPERTY_DOCUMENTTYPE + ".id IN (select doc.id from "
          + DocumentType.ENTITY_NAME + " doc where doc.id = ord." + Order.PROPERTY_DOCUMENTTYPE
          + " and COALESCE(" + DocumentType.PROPERTY_SOSUBTYPE + ",'INNOVA') <> 'WR'))");

      OBQuery<Order> orderList = OBDal.getInstance().createQuery(Order.class,
          queryExtend.toString());
      ////////////////////
      logger.log("No of Orders = " + orderList.list().size() + "\n");
      for (Order order : orderList.list()) {
        SalesOrderUtil.viewPolice("ORDER: <" + order.getDocumentNo() + ">");
        ProcessOrderLineSalesOrder procLine = new ProcessOrderLineSalesOrder();
        if (SalesOrderUtil.isValidSalesOrder(order)) {
          // if (isOrderProcessed(order.getId())) {
          if ((Boolean) order.get(Order.PROPERTY_SVFPAPROCESSED)) {
            logger.log("status: " + SalesOrderConstants.SO_ACTION_IN_WAREHOUSE);
            order.setSvfpaStatus(SalesOrderConstants.SO_ACTION_IN_WAREHOUSE);
            OBDal.getInstance().save(order);
          } else {
            final OBCriteria<OrderLine> orderLineList = OBDal.getInstance()
                .createCriteria(OrderLine.class);
            orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, order));
            for (OrderLine orderLine : orderLineList.list()) {
              boolean err = true;
              try {
                err = !procLine.forEveryOrderLine(orderLine);
              } catch (Exception e) {
                e.printStackTrace();
                log.error("Error: " + e.getMessage());
              } finally {
                System.out.println("finally: " + err + "|" + procLine.mapOrder.size());
                if (err) {
                  OBDal.getInstance().rollbackAndClose();
                } else {
                  OBDal.getInstance().flush();
                  OBDal.getInstance().commitAndClose();
                }
              }
            }
            procLine.processGroup();
          }
          counter++;
        }
      } // for
      logger.log("Processed " + counter);
    } catch (Exception e) {
      System.out.println("error void doExecute" + e.getCause() + "\n");
      logger.log("error void doExecute" + e + "\n");
      throw new JobExecutionException(e.getMessage(), e);
    }
    OBContext.restorePreviousMode();
  }

  public Boolean isOrderProcessed(String salesOrderNO) {
    Boolean isProcess = false;
    final String hql = "select count(" + OrderLine.PROPERTY_SVFPAPROCESSED
        + ") as noReg, COALESCE(sum(case " + OrderLine.PROPERTY_SVFPAPROCESSED
        + " when 'N' then 1 else 0 end),0) as NO, " + "COALESCE(sum(case "
        + OrderLine.PROPERTY_SVFPAPROCESSED + " when 'Y' then 1 else 0 end), 0) as SI " + " from "
        + OrderLine.ENTITY_NAME + " where " + OrderLine.PROPERTY_SALESORDER
        + ".id = :porder_id and " + OrderLine.PROPERTY_ACTIVE + " = 'Y' ";

    final Query query = OBDal.getInstance().getSession().createQuery(hql);
    query.setParameter("porder_id", salesOrderNO);
    //
    Long countReg = Long.valueOf("0");
    Long qtyProcess = Long.valueOf("0");
    //
    for (Object ol : query.list()) {
      // the query returns a list of arrays (columns of the query)
      final Object[] os = (Object[]) ol;
      // get element of result
      countReg = (Long) os[0]; // as noReg
      qtyProcess = (Long) os[2]; // as SI
      //
      // logger.log(countReg.toString() + "|" + qtyProcess.toString() + "|");

      if (countReg.compareTo(qtyProcess) == 0 && countReg.compareTo(Long.valueOf("0")) > 0) {
        isProcess = true;
      }
    } // for

    Order objOrder = OBDal.getInstance().get(Order.class, salesOrderNO);
    //
    if (!objOrder.get(Order.PROPERTY_SVFPAPROCESSED).equals(isProcess)) {
      objOrder.setSvfpaProcessed(isProcess);
    }
    return isProcess;
  }//
}//
