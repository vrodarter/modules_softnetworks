/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.depositFap;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;

//

/**
 */
public class ConfirmPartial implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    System.err.println("DepositFAProcess");
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      System.err.println("cOrderId: " + cOrderId);
      //
      String orderStatus = null;
      if ((Boolean) objOrder.get(Order.PROPERTY_SVFPAALLSTOCKIN)) {
        orderStatus = SalesOrderConstants.SO_ACTION_WAIT_STOCK;
      } else if (objOrder.get(Order.PROPERTY_SVFPASHIPFROM) != null) {
        orderStatus = SalesOrderConstants.SO_ACTION_MAP;
        objOrder.setSvfpaManagerApproved(null);
      }
      if (orderStatus != null) {
        objOrder.setSvfpaStatus(orderStatus);
        OBDal.getInstance().save(objOrder);
      }
      //
      execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
