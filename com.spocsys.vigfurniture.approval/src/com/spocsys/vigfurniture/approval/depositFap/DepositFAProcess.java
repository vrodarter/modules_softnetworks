/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.depositFap;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

//

/**
 */
public class DepositFAProcess implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    System.err.println("DepositFAProcess");
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final String decision = (String) execution.getVariables().get(
          SalesOrderConstants.SALES_ORDER_WF_DECISION);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      System.err.println("cOrderId: " + cOrderId);
      if (SalesOrderUtil.isValidSalesOrder(objOrder)) {
        //
        objOrder.setSvfpaFinancialApproved(true);
        OBDal.getInstance().save(objOrder);
        //
        if (decision != null) {
          if (decision.equals("Y")) {
            objOrder.setSvfapprDepFapApproved(true);
            objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_ISSUE_PO);
          } else {
            objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_SR);
          }
          OBDal.getInstance().save(objOrder);
        }
      }
      execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);

      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
