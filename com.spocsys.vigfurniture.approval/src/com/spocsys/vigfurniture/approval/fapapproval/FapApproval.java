/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.fapapproval;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;

//

/**
 */
public class FapApproval implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    System.err.println("FapApproval");
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final String decision = (String) execution.getVariables().get(
          SalesOrderConstants.SALES_ORDER_WF_DECISION);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      System.err.println("cOrderId: " + cOrderId + "-" + decision);
      //
      objOrder.setSvfpaFinancialApproved(true);
      OBDal.getInstance().save(objOrder);
      //
      if (decision != null) {
        if (decision.equals("Y")) {
          objOrder.setSvfpaFinancialApproved(true);
          OBDal.getInstance().save(objOrder);
          if (objOrder.getSvfpaStockStatus() != null) {
            if (objOrder.getSvfpaStockStatus().equals("INSTOCK")) {
              execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
                  SalesOrderConstants.ACTION_IN_STOCK);

            } else if (objOrder.getSvfpaStockStatus().equals("OUTOFSTOCK")) {
              execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
                  SalesOrderConstants.ACTION_OUT_STOCK);

            } else if (objOrder.getSvfpaStockStatus().equals("PARTIAL")) {
              execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
                  SalesOrderConstants.ACTION_IS_PARTIAL);

            } else {
              execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
                  SalesOrderConstants.ACTION_EXIT);
            }

          } else {
            execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
                SalesOrderConstants.ACTION_EXIT);
          }
        } else {
          objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_SR);
          OBDal.getInstance().save(objOrder);
          execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
              SalesOrderConstants.ACTION_EXIT);
        }
      } else {
        execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      }
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
