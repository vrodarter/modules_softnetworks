/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.fapapproval;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.order.Order;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

/**
 * 
 */
public class StartFAPApprovalProcess extends DalBaseProcess {

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {
    System.err.println("IN doExecute");
    try {
      final String cOrderId = (String) bundle.getParams().get("C_Order_ID");
      OBContext.setAdminMode(true);
      //
      if (cOrderId != null) {
        final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
        if (SalesOrderUtil.isValidSalesOrder(objOrder)) {
          SalesOrderUtil.logInformation(objOrder, "StartFAPApprovalProcess", null);
          System.err.println("StartFAPApprovalProcess");
          SalesOrderUtil.startSalesOrderWorkFlow(objOrder, "fapapprovalsalesorder", "nothing");
        }
      }
    } catch (final Exception e) {
      e.printStackTrace(System.err);
      final OBError msg = new OBError();
      msg.setType("Error");
      msg.setMessage(e.getMessage());
      msg.setTitle("Error ocurred");
      bundle.setResult(msg);
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
