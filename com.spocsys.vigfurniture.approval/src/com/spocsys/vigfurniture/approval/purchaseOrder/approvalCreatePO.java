package com.spocsys.vigfurniture.approval.purchaseOrder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.ApprovedVendor;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.ProductPrice;

import com.spocsys.vigfurniture.approval.utils.GetFunctions;
import com.spocsys.vigfurniture.incomingproduct.OrderReference;
import com.spocsys.vigfurniture.productavailability.OrderReference.LinkSOToPO;

public class approvalCreatePO {
  private static final Logger log = Logger.getLogger(Order.class);
  static String SVFSV_C_ORDER_POST_Process_ID = "7E1E5E36FF334525B6A6B8C82CF1FD3F";

  public approvalCreatePO() {
    // TODO Auto-generated constructor stub
  }

  static OrderReference createOrderReference(OrderLine regOrderLineSO, OrderLine regOrderLinePO) {

    try {

      OrderReference regOrderReference = OBProvider.getInstance().get(OrderReference.class);
      regOrderReference.setClient(regOrderLinePO.getClient());
      regOrderReference.setOrganization(regOrderLinePO.getOrganization());
      regOrderReference.setCreatedBy(regOrderLinePO.getCreatedBy());
      regOrderReference.setUpdatedBy(regOrderLinePO.getUpdatedBy());
      regOrderReference.setSalesOrder(regOrderLineSO.getSalesOrder());
      regOrderReference.setOrderlineref(regOrderLineSO);
      regOrderReference.setSalesOrderLine(regOrderLinePO);
      regOrderReference.setQuantityAllocated(regOrderLinePO.getOrderedQuantity());

      return regOrderReference;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  static Order createHeaderPO(Order iniOrder, OrderLine regOrderLine) {

    Order newPO = OBProvider.getInstance().get(Order.class);

    try {
      OBContext.setAdminMode(true);

      newPO.setClient(iniOrder.getClient());
      newPO.setOrganization(iniOrder.getOrganization());
      newPO.setCreatedBy(OBContext.getOBContext().getUser());
      newPO.setUpdatedBy(OBContext.getOBContext().getUser());
      newPO.setSalesTransaction(false);

      newPO.setActive(true);
      newPO.setDocumentStatus("DR");
      newPO.setDocumentAction("CO");
      newPO.setProcessed(false);
      newPO.setDocumentType(GetFunctions.getDocument("POO", iniOrder.getOrganization(), false));
      newPO.setTransactionDocument(
          GetFunctions.getDocument("POO", iniOrder.getOrganization(), false));
      newPO.setDelivered(false);
      newPO.setOrderDate(new Date());
      newPO.setScheduledDeliveryDate(new Date());
      newPO.setAccountingDate(new Date());
      newPO.setCurrency(iniOrder.getCurrency());
      newPO.setPaymentTerms(iniOrder.getPaymentTerms());

      FIN_PaymentMethod regPaymentMethod = GetFunctions.getPaymentMethod(iniOrder, false);

      if (regPaymentMethod != null) {
        newPO.setPaymentMethod(regPaymentMethod);
      }

      ApprovedVendor regApprovedVendor = GetFunctions.getPurchasing(regOrderLine.getProduct());

      if (regApprovedVendor == null) {
        throw new OBException("Purchasing not found");
      }

      if (regApprovedVendor.getBusinessPartner() == null) {
        throw new OBException("Business Partner for Purchase Order not found");
      }

      newPO.setBusinessPartner(regApprovedVendor.getBusinessPartner());

      if ((regApprovedVendor.getBusinessPartner().getBusinessPartnerLocationList() == null)
          || (regApprovedVendor.getBusinessPartner().getBusinessPartnerLocationList()
              .size() <= 0)) {
        throw new OBException("Business Partner Address for Purchase Order not found");
      }

      newPO.setPartnerAddress(
          regApprovedVendor.getBusinessPartner().getBusinessPartnerLocationList().get(0));

      PriceList regPriceList = GetFunctions.getPriceList(regApprovedVendor.getBusinessPartner(),
          false);

      if (regPriceList == null) {
        throw new OBException("Price List for Purchase Order not found");
      }

      newPO.setPriceList(regPriceList);

      if (regOrderLine.getSvfpaStockLocation() == null) {
        newPO.setWarehouse(iniOrder.getWarehouse());
      } else {
        newPO.setWarehouse(regOrderLine.getSvfpaStockLocation());
      }

      if (regOrderLine.isSvfdsDropship() == true) {
        newPO.setSvfdsDropship(true);
        newPO.setDropShipPartner(iniOrder.getDropShipPartner());
        newPO.setDropShipContact(iniOrder.getDropShipContact());
        newPO.setDropShipLocation(iniOrder.getDropShipLocation());
      }

      return newPO;
      // OBDal.getInstance().save(newPO);

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);

    } finally {
      OBContext.restorePreviousMode();
    }
    return null;
  }

  // LINE ORDER

  static OrderLine createLinesPO(Order newPO, Product newProduct, BigDecimal qtyOrd, long nroLine,
      OrderLine regOrderLine) {

    OrderLine newLinePO = OBProvider.getInstance().get(OrderLine.class);

    try {
      OBContext.setAdminMode(true);

      newLinePO.setClient(newPO.getClient());
      newLinePO.setOrganization(newPO.getOrganization());
      newLinePO.setCreatedBy(OBContext.getOBContext().getUser());
      newLinePO.setUpdatedBy(OBContext.getOBContext().getUser());

      newLinePO.setSalesOrder(newPO);
      newLinePO.setOrderDate(newPO.getOrderDate());
      newLinePO.setCurrency(newPO.getCurrency());

      if (regOrderLine.getSvfpaStockLocation() == null) {
        newLinePO.setWarehouse(newPO.getWarehouse());
      } else {
        newLinePO.setWarehouse(regOrderLine.getSvfpaStockLocation());
      }
      newLinePO.setActive(true);

      newLinePO.setProduct(newProduct);
      // newLinePO.setUOM(newProduct.getUOM());
      newLinePO.setUOM(regOrderLine.getUOM());
      newLinePO.setOrderedQuantity(qtyOrd);

      // find supplier info from product
      // final OBCriteria<ApprovedVendor> approvedList = OBDal.getInstance().createCriteria(
      // ApprovedVendor.class);
      // approvedList.add(Restrictions.eq(ApprovedVendor.PROPERTY_PRODUCT, newPO.getOrderLineList()
      // .get(0).getProduct()));
      // approvedList.addOrderBy(ApprovedVendor.PROPERTY_UPDATED, false);
      //
      // ProjectionList projectionList = Projections.projectionList();
      // projectionList.add(Projections.groupProperty("updated"));
      // projectionList.add(Projections.rowCount());
      // approvedList.setProjection(projectionList);

      ApprovedVendor regApprovedVendor = GetFunctions.getPurchasing(regOrderLine.getProduct());

      if (regApprovedVendor == null) {
        throw new OBException("Purchasing not found");
      }

      // if (approvedList != null) {
      // if (approvedList.list().size() > 0) {
      // newLinePO.setUnitPrice(approvedList.list().get(0).getLastPurchasePrice());
      // newLinePO.setListPrice(approvedList.list().get(0).getLastPurchasePrice());

      TaxRate regTaxRate = GetFunctions.calculateOrderLineTax(newPO, regOrderLine);

      if (regTaxRate == null) {
        throw new OBException("Tax not found");
      }

      newLinePO.setTax(regTaxRate);
      newLinePO.setDirectShipment(false);
      newLinePO.setReservedQuantity(BigDecimal.ZERO);
      newLinePO.setDeliveredQuantity(BigDecimal.ZERO);
      newLinePO.setInvoicedQuantity(BigDecimal.ZERO);
      // newLinePO.setUnitPrice(regApprovedVendor.getLastPurchasePrice());
      // newLinePO.setLineNetAmount(regApprovedVendor.getLastPurchasePrice());
      newLinePO.setFreightAmount(BigDecimal.ZERO);
      newLinePO.setDescriptionOnly(false);
      // Long NOorderLine = (orderList.list().get(0).getOrderLineList().size() + 1L) * 10L;
      newLinePO.setLineNo(nroLine);
      newLinePO.setSvfapprCreatedolinefrom(regOrderLine);

      ProductPrice regProductPrice = GetFunctions.getProductPrice(newPO.getPriceList());

      if (regProductPrice == null) {
        newLinePO.setUnitPrice(regApprovedVendor.getLastPurchasePrice());
        newLinePO.setListPrice(regApprovedVendor.getLastPurchasePrice());
        newLinePO.setStandardPrice(regApprovedVendor.getLastPurchasePrice());
        // newLinePO.setLineNetAmount(regApprovedVendor.getLastPurchasePrice());
        newLinePO.setPriceLimit(BigDecimal.ZERO);
      } else {
        newLinePO.setUnitPrice(regProductPrice.getStandardPrice());
        newLinePO.setListPrice(regProductPrice.getListPrice());
        newLinePO.setStandardPrice(regProductPrice.getStandardPrice());
        // newLinePO.setLineNetAmount(regProductPrice.getStandardPrice());
        newLinePO.setPriceLimit(BigDecimal.ZERO);
      }
      //
      if (regOrderLine.getAttributeSetValue() != null) {
        newLinePO.setAttributeSetValue(regOrderLine.getAttributeSetValue());
      }
      if (regOrderLine.getSvfsvPoreference() != null) {
        newLinePO.setSvfsvPoreference(regOrderLine.getSvfsvPoreference());
      }

      // // find supplier tax rate
      // final OBCriteria<TaxRate> taxList = OBDal.getInstance().createCriteria(TaxRate.class);
      // taxList.add(Restrictions.eq(TaxRate.PROPERTY_ID, approvedList.list().get(0)
      // .getBusinessPartner().getTaxID()));
      // taxList.addOrderBy(TaxRate.PROPERTY_UPDATED, false);

      return newLinePO;
      // } else {
      // System.err.println("Error: approvedList = 0");
      // return null;
      // }
      // // OBDal.getInstance().save(newPO);
      // } else {
      // System.err.println("Error: NULL approvedList");
      // return null;
      // }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);

    } finally {
      OBContext.restorePreviousMode();
    }
    return null;
  }

  public static boolean create(List<OrderLine> ListOrderLine) {

    // List<OrderLine> regOLinetList = new ArrayList<OrderLine>();
    Order regOrder = null;
    if (ListOrderLine.size() > 0) {
      regOrder = createHeaderPO(ListOrderLine.get(0).getSalesOrder(), ListOrderLine.get(0));

      if (regOrder == null) {
        return false;
      }

      OBDal.getInstance().save(regOrder);
      OBDal.getInstance().flush();

      int LastNoLine = 0;
      Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
      for (OrderLine regOrderLine : ListOrderLine) {
        //
        // regOrder.setOrderLineList(ListOrderLine);
        //
        LastNoLine = LastNoLine + 10;
        nroLine = Long.parseLong(String.valueOf(LastNoLine));
        OrderLine oLine = null;

        oLine = createLinesPO(regOrder, regOrderLine.getProduct(),
            regOrderLine.getOrderedQuantity(), nroLine, regOrderLine);
        if (oLine == null) {
          return false;
        }

        OBDal.getInstance().save(oLine);
        OBDal.getInstance().flush();

        regOrderLine.setSvfpaPurchaseorder(regOrder);
        OBDal.getInstance().save(regOrderLine);

        OrderReference regLinkSOToPO = LinkSOToPO.create(regOrderLine, oLine,
            regOrderLine.getOrderedQuantity());

        if (regLinkSOToPO == null) {
          throw new OBException(
              "There was an error creating Sales Order Reference (SalesOrderLine: "
                  + regOrderLine.getIdentifier() + ")");
        }
        OBDal.getInstance().save(regLinkSOToPO);
        OBDal.getInstance().flush();

        // regOLinetList.add(oLine);

      }

      try {

        // regOrder.setOrderLineList(regOLinetList);
        OBDal.getInstance().save(regOrder);

        // OBDal.getInstance().flush();
        // OBDal.getInstance().commitAndClose();

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return false;
      }
      return true;

    }
    return false;

  }

}
