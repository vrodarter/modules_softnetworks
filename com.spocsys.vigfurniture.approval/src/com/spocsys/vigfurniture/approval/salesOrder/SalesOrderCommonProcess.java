/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.salesOrder;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

//

/**
 */
public class SalesOrderCommonProcess implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    System.err.println("SalesOrderCommonProcess");
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      String salesOrderStatus = null;
      String allVig = null;
      //
      System.err.println("cOrderId: " + cOrderId);
      //
      if (objOrder.get(Order.PROPERTY_SVFPASPECIALORDER) != null) {
        if ((Boolean) objOrder.get(Order.PROPERTY_SVFPASPECIALORDER)) {
          if ((Boolean) objOrder.get(Order.PROPERTY_SVFPAINTERCOMPANY)) {
            // continue
          } else {
            salesOrderStatus = SalesOrderConstants.SO_ACTION_WAIT_DEPOSIT_FAP;
          }
        }
        if (salesOrderStatus == null) {
          if ((Boolean) objOrder.get(Order.PROPERTY_SVFPAFINANCIALAPPROVED)) {
            if (objOrder.getSvfpaStockStatus() != null) {
              if (objOrder.getSvfpaStockStatus().equals("INSTOCK")) {
                if (objOrder.getSVFPAAction() != null) {
                  if (objOrder.getSVFPAAction().equals("INTERCOMPANY")) {
                    salesOrderStatus = SalesOrderConstants.SO_ACTION_INTERCOMPANY;
                  } else if (objOrder.getSVFPAAction().equals("RTP")) {
                    salesOrderStatus = SalesOrderConstants.SO_ACTION_RTP; // "Ready to Pick"
                  } else if (objOrder.getSVFPAAction().equals("MIXED")) {
                    salesOrderStatus = SalesOrderConstants.SO_ACTION_MIX; // "Mix";
                  }
                }
              } else if (objOrder.getSvfpaStockStatus().equals("OUTOFSTOCK")) {
                salesOrderStatus = SalesOrderConstants.SO_ACTION_WAIT_STOCK; // "Waiting for Stock"
              } else if (objOrder.getSvfpaStockStatus().equals("PARTIAL")) {
                allVig = SalesOrderUtil.allItemStatus(objOrder, OrderLine.PROPERTY_SVFPAACTION,
                    "INTERCOMPANY");
                if (allVig.equals("Y")) {
                  salesOrderStatus = SalesOrderConstants.SO_ACTION_INTERCOMPANY; // "Intercompany"
                } else {
                  salesOrderStatus = SalesOrderConstants.SO_ACTION_PARTIAL; // "Partial"
                }
              }
            }
          } else {
            salesOrderStatus = SalesOrderConstants.SO_ACTION_FAP; // "Waiting for Financial Approval"
          }
        }
      }
      //
      if (salesOrderStatus != null) {
        objOrder.setSvfpaStatus(salesOrderStatus);
        OBDal.getInstance().save(objOrder);
      }
      execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
