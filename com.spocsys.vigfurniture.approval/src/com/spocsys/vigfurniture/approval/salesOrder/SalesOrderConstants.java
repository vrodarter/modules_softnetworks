/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.salesOrder;

/**
 * Constants used in Submit Sales Order
 * 
 * @author mtaal
 */
public class SalesOrderConstants {

  public static final long ONE_DAY = 24 * 3600 * 1000;
  public static final Boolean isDebug = true;
  public static final String SALES_ORDER_WF_PARAM = "salesOrder";
  public static final String SALES_ORDER_WF_DECISION = "decision";
  public static final String BP_WF_PARAM = "businessPartner";
  public static final String ORDER_ID_WF_PARAM = "orderId";
  public static final String ORDERLINE_ID_WF_PARAM = "orderLineId";
  public static final String DUNNING_POLICY_WF_PARAM = "dunningPolicyId";
  public static final String ACTION_WF_PARAM = "action";
  public static final String ACTION_WF_NUMRECORD = "numrecord";
  public static final String ACTION_WF_NUMCANCEL = "numCancel";
  public static final String ACTION_WF_CANCELPACK = "cancel_pack";
  public static final String ACTION_WF_VOIDORDER = "void_order";
  public static final String ACTION_WF_WARERELEASE = "ware_release";
  public static final String ACTION_VOID_LIST = "voidList";
  public static final String ACTION_WAREHOUSE_LIST = "wareHouseList";
  public static final String ACTION_CANCEL_LIST = "cancelList";
  public static final String ACTION_DUPLICATE = "duplicate";
  public static final String ORDER_ID_VOID = "orderIdVoid";
  public static final String ACTION_EXIT = "exit";
  public static final String ACTION_WAIT = "wait";
  public static final String ACTION_CONTINUE = "continue";
  public static final String ACTION_ONHOLD = "onHold";
  public static final String ACTION_USER = "user";
  public static final String ACTION_EMAIL = "email";
  public static final String ACTION_LETTER = "sendLetter";
  public static final String ACTION_ORDERFULLY = "order_fully";
  public static final String ACTION_ORDER_NOT_FULLY = "order_not_fully";
  public static final String ACTION_ORDER_NOT_FULLY_OUT_STOCK = "order_is_not_fully_out_stock";
  public static final String ACTION_INTERCOMPANY = "is_intercompany";
  public static final String ACTION_NOT_INTERCOMPANY = "is_not_intercompany";
  public static final String ACTION_NOT_ALL_IN = "not_all_in";
  public static final String ACTION_ALL_IN = "all_in";
  public static final String ACTION_IN_STOCK = "in_stock";
  public static final String ACTION_NOT_STOCK = "not_stock";
  public static final String ACTION_OUT_STOCK = "out_stock";
  public static final String ACTION_IS_PARTIAL = "is_partial";
  public static final String ACTION_PARTIAL_SHIPMENT = "partial_shipment";
  public static final String ACTION_NOT_MAP = "not_map";
  public static final String ACTION_COMMON_PROCESS = "common_process";
  public static final String ACTION_VOID_ORDER = "void_order";
  public static final String ACTION_CANCEL_FOR_EVERY_LINE = "cancel_for_every_line";
  public static final String ACTION_WAREHOUSE_RELEASE = "warehouse_release";
  public static final String ACTION_CANCEL_PACK = "cancel_pack";
  public static final String ACTION_APPROVAL = "approval";
  public static final String ACTION_NOT_APPROVAL = "not_approval";
  public static final String ACTION_VOID = "void";
  //
  public static final String CONF_INSTOCK = "INSTOCK";
  public static final String CONF_PARTIAL = "PARTIAL";
  public static final String CONF_OUTSTOCK = "OUTOFSTOCK";
  //
  public static final String SO_ACTION_VOID = "VOID";
  public static final String SO_ACTION_VOIDED = "Voided";
  public static final String SO_ACTION_NEW = "New";
  public static final String SO_ACTION_COMPLETE = "Complete";
  public static final String SO_ACTION_MAP = "Waiting for Manager Approval";
  public static final String SO_ACTION_NOT_APPROVED = "Not approved";
  public static final String SO_ACTION_NEED_REFUND = "Need to Refund ";
  public static final String SO_ACTION_FAP = "Waiting for Financial Approval";
  public static final String SO_ACTION_MIX = "Mix";
  public static final String SO_ACTION_TRANSFER = "Transfer";
  public static final String SO_ACTION_WAIT_STOCK = "Waiting for Stock";
  public static final String SO_ACTION_WAIT_DEPOSIT_FAP = "Waiting for Deposit-FAP";
  public static final String SO_ACTION_RTP = "RTP";
  public static final String SO_ACTION_PARTIAL = "Partial";
  public static final String SO_ACTION_INTERCOMPANY = "Intercompany";
  public static final String SO_ACTION_SR = "SR Attention";
  public static final String SO_ACTION_WAIT_PO = "Waiting for PO";
  public static final String SO_ACTION_RTP_LONG = "Ready to Pick";
  public static final String SO_ACTION_WAIT_CUSTOMER = "Waiting for Customer Confirm";
  public static final String SO_ACTION_ISSUE_PO = "Issue PO";
  public static final String SO_ACTION_ISSUED = "Issued";
  public static final String SO_ACTION_IN_WAREHOUSE = "In Warehouse";
  public static final String SO_ACTION_PROCESS = "PROCESS";
  public static final String SO_ACTION_SOURCE = "SOURCE";
  public static final String SO_ACTION_WAIT_WH_RELEASE = "Waiting WH Release";
  public static final String SO_ACTION_WAITINGFORPO = "WAITINGFORPO";
  public static final String SO_ACTION_NOT_APPROVED_RELEASE = "Not Approved for release";
  public static final String SO_ACTION_LINE_ISSUEPO = "ISSUEPO";
  public static final String SO_ACTION_LINE_TRANS = "TRANSFER";
  public static final String SO_ACTION_LINE_INTERCOMPANY = "INTERCOMPANY";
  public static final String SO_ACTION_AUTO_APROVED = "Automatically Approved";

  //
  public static final String SO_ERR_SHIPMENT = "order_shipment";
  //
  public static final String ROLE_WF_PARAM = "role";
}
