/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.salesOrder;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

//

/**
 */
public class SalesOrderMapApproval implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    System.err.println("SalesOrderCommonProcess");
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final String decision = (String) execution.getVariables().get(
          SalesOrderConstants.SALES_ORDER_WF_DECISION);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      System.err.println("cOrderId: " + cOrderId);
      if (decision != null) {
        if (decision.equals("Y")) {
          objOrder.setSvfpaManagerApproved(true);
          execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
              SalesOrderConstants.ACTION_COMMON_PROCESS);
        } else {
          objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_SR);
          execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
              SalesOrderConstants.ACTION_EXIT);
        }
        OBDal.getInstance().save(objOrder);
      } else {
        execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      }
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
