/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.salesOrder;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

//

/**
 * 
 */
public class SalesOrderPartialAllIn implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    System.err.println("SalesOrderPartialAllIn");
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      String salesOrderStatus = null;
      //
      System.err.println("cOrderId: " + cOrderId);

      if (SalesOrderUtil.existSpecialProduct(objOrder, "").equals("Y")) {
        if (SalesOrderUtil.existSpecialProduct(objOrder,
            " and " + OrderLine.PROPERTY_SVFPASTOCKAVAILABILITY + "= 'INSTOCK' ").equals("Y")) {
          salesOrderStatus = SalesOrderConstants.SO_ACTION_WAIT_STOCK; // "Waiting for Stock"
        } else {
          if (objOrder.get(Order.PROPERTY_SVFPAINTERCOMPANY) != null) {
            if ((Boolean) objOrder.get(Order.PROPERTY_SVFPAINTERCOMPANY)) {
              salesOrderStatus = SalesOrderConstants.SO_ACTION_WAIT_STOCK; // ""Waiting for Stock"
            } else {
              salesOrderStatus = SalesOrderConstants.SO_ACTION_WAIT_DEPOSIT_FAP; // "Waiting for Deposit-FAP"
            }
          }
        }
      } else {
        salesOrderStatus = SalesOrderConstants.SO_ACTION_WAIT_STOCK; // "Waiting for Stock"
      }
      if (salesOrderStatus != null) {
        objOrder.setSvfpaStatus(salesOrderStatus);
      } else {
        execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      }

      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
