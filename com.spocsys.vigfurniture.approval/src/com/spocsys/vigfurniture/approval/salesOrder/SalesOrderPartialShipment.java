/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.salesOrder;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

//

/**
 * The Activiti Component Provider.
 * 
 * @author mtaal
 */
public class SalesOrderPartialShipment implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    System.err.println("SalesOrderPartialShipment");
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      if (objOrder.get(Order.PROPERTY_SVFPAMANAGERAPPROVED) != null) {
        if (!(Boolean) objOrder.get(Order.PROPERTY_SVFPAMANAGERAPPROVED)) {
          objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_MAP);
          OBDal.getInstance().save(objOrder);
          //
          execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
              SalesOrderConstants.ACTION_EXIT);
        } else {
          execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
              SalesOrderConstants.ACTION_COMMON_PROCESS);
        }
      } else {
        execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      }
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
