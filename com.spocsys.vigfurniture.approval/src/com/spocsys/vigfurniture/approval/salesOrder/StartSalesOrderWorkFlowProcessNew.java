/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.salesOrder;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

import com.spocsys.vigfurniture.approval.utils.SalesOrderUtil;

/**
 * 
 */

public class StartSalesOrderWorkFlowProcessNew extends BaseActionHandler {

  private static final Logger log = Logger.getLogger(StartSalesOrderWorkFlowProcessNew.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {
    JSONObject ResultJSON = new JSONObject();
    JSONObject regJSON = new JSONObject();
    String result = null;
    String resultMessage = null;
    try {

      final JSONObject JSONData = new JSONObject(content);
      String OrderID = JSONData.getString("OrderID");
      String Approved = JSONData.getString("Approved");// This parameter can be Y or N
      String execProcess = null;
      OBContext.setAdminMode(true);
      //
      if (OrderID != null) {
        final Order objOrder = OBDal.getInstance().get(Order.class, OrderID);
        if (SalesOrderUtil.isValidSalesOrder(objOrder)) {
          if (objOrder.getSvfpaStatus() != null) {
            if (objOrder.getDocumentType().getSOSubType() != null) {
              if (objOrder.getDocumentType().getSOSubType().equals("WR")) {
                execProcess = null;
                result = "nook";
                resultMessage = "Svfappr_order_ignored";
              }
            }
            if (result == null) {
              if (objOrder.getSvfpaStatus().equals(SalesOrderConstants.SO_ACTION_MAP)) {
                execProcess = "mapapprovalsalesorder";
              } else if (objOrder.getSvfpaStatus().equals(SalesOrderConstants.SO_ACTION_FAP)) {
                execProcess = "fapapprovalsalesorder";
              } else if (objOrder.getSvfpaStatus()
                  .equals(SalesOrderConstants.SO_ACTION_WAIT_DEPOSIT_FAP)) {
                execProcess = "depositfap";
              } else if (objOrder.getSvfpaStatus()
                  .equals(SalesOrderConstants.SO_ACTION_WAIT_CUSTOMER)) {
                execProcess = "waitcustomerconfirmation";
              } else if (objOrder.getSvfpaStatus()
                  .equals(SalesOrderConstants.SO_ACTION_WAIT_WH_RELEASE)) {
                execProcess = "warehouserelease";
              }
            }
            //
            if (execProcess != null) {
              SalesOrderUtil.logInformation(objOrder, "StartSalesOrderWorkFlowProcess", null);
              System.err
                  .println("StartSalesOrderWorkFlowProcessNew: " + execProcess + "-" + Approved);
              SalesOrderUtil.startSalesOrderWorkFlow(objOrder, execProcess, Approved);
              //
              result = "ok";
              resultMessage = "Sales Order Processed";
            } else {
              result = "ok";
              resultMessage = "No workflow defined for this status";
            }
          }
          //
        } else {
          result = "nook";
          resultMessage = "Svfappr_Error_Hold";
        }
      } else {
        result = "nook";
        resultMessage = "Invalid Sales Order";
      }
      if (result != null) {
        regJSON.put("status", result);
        regJSON.put("msg", resultMessage);
      }
      // return regJSON;
    } catch (final Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());

      try {

        regJSON.put("status", "nook");
        regJSON.put("msg", e.getMessage().toString());

      } catch (JSONException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
        log.error(e1.getMessage());
      }

    } finally {
      OBContext.restorePreviousMode();
    }

    try {
      ResultJSON.put("result", regJSON);
    } catch (JSONException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
      log.error(e1.getMessage());
    }
    return ResultJSON;
  }
}