package com.spocsys.vigfurniture.approval.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.security.OrganizationStructureProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.interco.IntercoMatchingDocument;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.ApprovedVendor;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.materialmgmt.onhandquantity.ReservationStock;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.db.DalConnectionProvider;

public class GetFunctions {

  private static final Logger log = Logger.getLogger(GetFunctions.class);

  public static PriceList getPriceList(BusinessPartner regBusinessPartner, boolean issoTrx) {

    try {
      if (issoTrx == false) {
        if (regBusinessPartner.getPurchasePricelist() != null) {
          return regBusinessPartner.getPurchasePricelist();
        }
      } else {
        if (regBusinessPartner.getPriceList() != null) {
          return regBusinessPartner.getPriceList();
        }
      }

      OBCriteria<PriceList> ListPriceList = OBDal.getInstance().createCriteria(PriceList.class);
      ListPriceList.add(Restrictions.eq(PriceList.PROPERTY_SALESPRICELIST, issoTrx));

      if ((ListPriceList != null) && (ListPriceList.count() > 0)) {
        return ListPriceList.list().get(0);
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static FIN_PaymentMethod getPaymentMethod(Order regOrder, boolean issoTrx) {

    try {
      if (regOrder.getPaymentMethod() != null) {
        if ((issoTrx == false) && (regOrder.getPaymentMethod().isPayoutAllow() == true)) {
          return regOrder.getPaymentMethod();
        } else {
          if ((issoTrx == true) && (regOrder.getPaymentMethod().isPayinAllow() == true)) {
            return regOrder.getPaymentMethod();
          }
        }
      }
      BusinessPartner regBusinessPartner = regOrder.getBusinessPartner();

      if (issoTrx == false) {
        if (regBusinessPartner.getPOPaymentMethod() != null) {
          return regBusinessPartner.getPOPaymentMethod();
        }
      } else {
        if (regBusinessPartner.getPaymentMethod() != null) {
          return regBusinessPartner.getPaymentMethod();
        }
      }

      OBCriteria<FIN_PaymentMethod> ListPaymentMethod = OBDal.getInstance()
          .createCriteria(FIN_PaymentMethod.class);
      ListPaymentMethod.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_PAYINALLOW, issoTrx));

      if ((ListPaymentMethod != null) && (ListPaymentMethod.count() > 0)) {
        return ListPaymentMethod.list().get(0);
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static PaymentTerm getPaymentTerm(BusinessPartner regBusinessPartner, boolean issoTrx) {

    try {
      if (issoTrx == false) {
        if (regBusinessPartner.getPOPaymentTerms() != null) {
          return regBusinessPartner.getPOPaymentTerms();
        }
      } else {
        if (regBusinessPartner.getPOPaymentTerms() != null) {
          return regBusinessPartner.getPOPaymentTerms();
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static DocumentType getDocument(String Base, Organization OrgTrx, boolean issoTrx) {
    // Client client = null;
    //
    /*
     * if ("0".equals(OrgTrx.getId())) { client = OBContext.getOBContext().getCurrentClient(); if
     * ("0".equals(client.getId())) { return null; } } else { client = OrgTrx.getClient(); } //
     * OrganizationStructureProvider osp = OBContext.getOBContext()
     * .getOrganizationStructureProvider(client.getId()); //
     */
    try {
      OBCriteria<DocumentType> ListDocumentType = OBDal.getInstance()
          .createCriteria(DocumentType.class);
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, Base));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, issoTrx));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_RETURN, false));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_ORGANIZATION, OrgTrx));
      // ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_CLIENT, client));
      // ListDocumentType.add(Restrictions.in("organization.id", osp.getParentTree(OrgTrx.getId(),
      // true)));

      if ((ListDocumentType.list() != null) && (ListDocumentType.list().size() > 0)) {
        return ListDocumentType.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static DocumentType getDocument(String Base, String strLike, Organization OrgTrx,
      boolean issoTrx) {
    Client client = null;
    //
    if ("0".equals(OrgTrx.getId())) {
      client = OBContext.getOBContext().getCurrentClient();
      if ("0".equals(client.getId())) {
        return null;
      }
    } else {
      client = OrgTrx.getClient();
    }
    //
    OrganizationStructureProvider osp = OBContext.getOBContext()
        .getOrganizationStructureProvider(client.getId());
    //
    try {
      OBCriteria<DocumentType> ListDocumentType = OBDal.getInstance()
          .createCriteria(DocumentType.class);
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, Base));
      ListDocumentType.add(Restrictions.like(DocumentType.PROPERTY_NAME, strLike));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, issoTrx));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_RETURN, false));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_ORGANIZATION, OrgTrx));
      // ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_CLIENT, client));
      // ListDocumentType.add(Restrictions.in("organization.id", osp.getParentTree(OrgTrx.getId(),
      // true)));

      if ((ListDocumentType.list() != null) && (ListDocumentType.list().size() > 0)) {
        return ListDocumentType.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static List<DocumentType> getDocumentIntercompanyList(String Base, Organization OrgTrx,
      boolean issoTrx) {
    //
    Client client = null;

    if ("0".equals(OrgTrx.getId())) {
      client = OBContext.getOBContext().getCurrentClient();
      if ("0".equals(client.getId())) {
        return null;
      }
    } else {
      client = OrgTrx.getClient();
    }
    OrganizationStructureProvider osp = OBContext.getOBContext()
        .getOrganizationStructureProvider(client.getId());
    try {
      OBCriteria<DocumentType> ListDocumentType = OBDal.getInstance()
          .createCriteria(DocumentType.class);
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, Base));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, issoTrx));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_RETURN, false));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_INTERCOISINTERCOMPANY, true));
      //
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_CLIENT, client));
      ListDocumentType
          .add(Restrictions.in("organization.id", osp.getParentTree(OrgTrx.getId(), true)));
      //
      if ((ListDocumentType.list() != null) && (ListDocumentType.list().size() > 0)) {
        return ListDocumentType.list();
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static IntercoMatchingDocument getDocumentIntercompany(String Base, boolean issoTrx,
      Organization OrgSource, Organization OrgTarget) {

    try {
      OBCriteria<IntercoMatchingDocument> ListIntercoMatchingDocument = OBDal.getInstance()
          .createCriteria(IntercoMatchingDocument.class);

      List<DocumentType> ListDocumentType = GetFunctions.getDocumentIntercompanyList(Base,
          OrgSource, issoTrx);

      ListIntercoMatchingDocument
          .add(Restrictions.in(IntercoMatchingDocument.PROPERTY_DOCUMENTTYPE, ListDocumentType));
      ListIntercoMatchingDocument
          .add(Restrictions.eq(IntercoMatchingDocument.PROPERTY_SOURCEORGANIZATION, OrgSource));
      ListIntercoMatchingDocument
          .add(Restrictions.eq(IntercoMatchingDocument.PROPERTY_TARGETORGANIZATION, OrgTarget));

      if ((ListIntercoMatchingDocument.list() != null)
          && (ListIntercoMatchingDocument.list().size() > 0)) {
        return ListIntercoMatchingDocument.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static Locator getLocator(Warehouse regWarehouse) {
    try {

      if ((regWarehouse.getLocatorList() != null) && (regWarehouse.getLocatorList().size() > 0)) {

        return regWarehouse.getLocatorList().get(0);

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  public static Locator getLocatorType(Warehouse regWareHouse, String strType, String vigMovement) {
    final OBCriteria<Locator> locatorList = OBDal.getInstance().createCriteria(Locator.class);
    locatorList.add(Restrictions.eq(Locator.PROPERTY_WAREHOUSE, regWareHouse));
    if (strType != null) {
      locatorList.add(Restrictions.eq(Locator.PROPERTY_OBWHSTYPE, strType));
    }
    if (vigMovement != null) {
      locatorList.add(Restrictions.eq(Locator.PROPERTY_SVFPMOVEMTSTAGE, vigMovement));
    }
    // locatorList.add(Restrictions.eq(Locator.PROPERTY_DEFAULT, true));
    Locator resLocator = null;
    for (Locator regLocator : locatorList.list()) {
      return regLocator;
    }
    return resLocator;
  }// getLocatorType

  public static Locator getLocatorWareHouse(Warehouse regWareHouse) {
    try {
      final OBCriteria<Locator> locatorList = OBDal.getInstance().createCriteria(Locator.class);
      locatorList.add(Restrictions.eq(Locator.PROPERTY_WAREHOUSE, regWareHouse));
      locatorList.add(Restrictions.eq(Locator.PROPERTY_DEFAULT, true));
      locatorList.add(Restrictions.or(Restrictions.ne(Locator.PROPERTY_OBWHSTYPE, "OUT"),
          Restrictions.isNull(Locator.PROPERTY_OBWHSTYPE)));
      locatorList.add(Restrictions.eq(Locator.PROPERTY_ACTIVE, true));
      Locator resLocator = null;
      for (Locator regLocator : locatorList.list()) {
        return regLocator;
      }
      return resLocator;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }// getLocatorWareHouse

  public static Locator getLocatorReserved(OrderLine pOrderLine) {
    Locator regLocator = null;
    final OBCriteria<ReservationStock> reservedList = OBDal.getInstance()
        .createCriteria(ReservationStock.class);
    reservedList.add(Restrictions.eq(ReservationStock.PROPERTY_SALESORDERLINE, pOrderLine));
    reservedList.add(Restrictions.eq(ReservationStock.PROPERTY_ACTIVE, true));
    for (ReservationStock regReserved : reservedList.list()) {
      return regReserved.getStorageBin();
    }
    return regLocator;
  }

  public static TaxRate calculateOrderLineTax(Order regOrder, OrderLine regOrderLine) {

    try {

      if (regOrderLine.getTax().getSalesPurchaseType().equalsIgnoreCase("B") == true) {
        return regOrderLine.getTax();
      } else {
        List<Object> parameters = new ArrayList<Object>();
        parameters.add(regOrderLine.getProduct().getId());
        parameters.add(regOrder.getOrderDate());
        parameters.add(regOrder.getOrganization().getId());
        parameters.add(regOrder.getWarehouse().getId());
        parameters.add(regOrder.getPartnerAddress().getId());
        parameters.add(regOrder.getInvoiceAddress().getId());
        parameters.add(regOrder);
        parameters.add(regOrder.isSalesTransaction());

        String taxId = (String) CallStoredProcedure.getInstance().call("C_Gettax", parameters,
            null);
        if (taxId != null) {
          return OBDal.getInstance().get(TaxRate.class, taxId);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static ProcessInstance OrderPost(String ID, String ProcessID) {
    ProcessInstance pInstance = null;
    try {
      OBContext.setAdminMode();
      try {
        final org.openbravo.model.ad.ui.Process process = OBDal.getInstance()
            .get(org.openbravo.model.ad.ui.Process.class, ProcessID);

        pInstance = OBProvider.getInstance().get(ProcessInstance.class);

        pInstance.setClient(OBContext.getOBContext().getCurrentClient());
        pInstance.setOrganization(OBContext.getOBContext().getCurrentOrganization());
        pInstance.setCreatedBy(OBContext.getOBContext().getUser());
        pInstance.setUpdatedBy(OBContext.getOBContext().getUser());
        pInstance.setProcess(process);
        pInstance.setActive(true);
        pInstance.setRecordID(ID);
        pInstance.setUserContact(OBContext.getOBContext().getUser());

        // final Parameter parameter =
        // OBProvider.getInstance().get(Parameter.class);
        // parameter.setSequenceNumber("1");
        // parameter.setParameterName("Selection");
        // parameter.setString("Y");

        // // set both sides of the bidirectional association
        // pInstance.getADParameterList().add(parameter);
        // parameter.setProcessInstance(pInstance);

        // persist to the db
        OBDal.getInstance().save(pInstance);

        // flush, this gives pInstance an ID
        OBDal.getInstance().flush();

        final Connection connection = OBDal.getInstance().getConnection();
        final PreparedStatement ps = connection
            .prepareStatement("SELECT * FROM SVFSV_C_Order_Post(?)");
        ps.setString(1, pInstance.getId());
        ps.execute();

        // refresh the pInstance as the SP has changed it
        OBDal.getInstance().getSession().refresh(pInstance);

        if (pInstance.getResult() != 1) {
          throw new OBException(getMessage(pInstance.getErrorMsg()));
        } else {
          String Msg = getMessage(pInstance.getErrorMsg());
          System.out.println("OrderPost: " + Msg);
          log.info(Msg);
        }

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        pInstance = null;
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return pInstance;
  }

  static String getMessage(String Value) {
    String KeyMessage = "";
    String Message = "";

    String language = OBContext.getOBContext().getLanguage().getLanguage();
    ConnectionProvider conn = new DalConnectionProvider(false);
    // throw new OBException(Utility.messageBD(conn, getMessage(Value),
    // language));

    try {

      if ((Value.startsWith("@ERROR=@")) && (Value.endsWith("@"))) {
        KeyMessage = Value.substring(8, Value.length() - 1);
        Message = Utility.messageBD(conn, KeyMessage, language);
      } else {
        if ((Value.startsWith("@CODE=@")) && (Value.endsWith("@"))) {
          KeyMessage = Value.substring(8, Value.length() - 1);
          Message = Utility.messageBD(conn, KeyMessage, language);
        } else {
          // @INTERCO_ordCreated@10000028

          try {
            int First = Value.indexOf('@');
            int Last = Value.lastIndexOf('@');

            if ((First != -1) && (Last != -1) && (First != Last)) {
              KeyMessage = Value.substring(First + 1, Last);
              Message = Value.replaceAll("@" + KeyMessage + "@",
                  Utility.messageBD(conn, KeyMessage, language));

            } else {
              KeyMessage = Value;
            }
          } catch (Exception e) {
            e.printStackTrace();
            Message = Value;
          }

        }
      }
    } catch (Exception e) {
      e.printStackTrace();

    }

    return Message;
  }

  static public ApprovedVendor getPurchasing(Product regProduct) {

    ApprovedVendor regApprovedVendorMax = null;

    try {

      if ((regProduct.getApprovedVendorList() != null)
          && (regProduct.getApprovedVendorList().size() > 0)) {

        for (ApprovedVendor regApprovedVendor : regProduct.getApprovedVendorList()) {
          if (regApprovedVendorMax == null) {
            regApprovedVendorMax = regApprovedVendor;
          } else {
            if (regApprovedVendorMax.getUpdated().before(regApprovedVendor.getUpdated())) {
              regApprovedVendorMax = regApprovedVendor;
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      regApprovedVendorMax = null;
    }
    return regApprovedVendorMax;
  }

  public static ProductPrice getProductPrice(PriceList regPriceList) {

    try {

      java.util.Date utilDate = new java.util.Date();
      long lnMilisegundos = utilDate.getTime();
      java.sql.Date sqlDate = new java.sql.Date(lnMilisegundos);

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS pp");
      queryExtend
          .append(" WHERE (pp.priceListVersion.priceList.id = '" + regPriceList.getId() + "')");
      queryExtend
          .append(" AND (pp.priceListVersion.validFromDate <= '" + sqlDate.toString() + "')");
      queryExtend.append(" ORDER BY pp.priceListVersion.validFromDate DESC");

      OBQuery<ProductPrice> ListProductPrice = OBDal.getInstance().createQuery(ProductPrice.class,
          queryExtend.toString());

      if ((ListProductPrice.list() != null) && (ListProductPrice.list().size() > 0)) {
        return ListProductPrice.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

    // pp.priceListVersion.priceList;validFromDate

  }

  public static OrderLine getOrderLineByDocumentNo(OrderLine regSalesOrderLine) {
    try {

      final Criteria orderLineList = OBDal.getInstance().createCriteria(OrderLine.class);

      orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_PRODUCT + ".id",
          regSalesOrderLine.getProduct().getId()));
      //
      orderLineList.createAlias(OrderLine.PROPERTY_SALESORDER, "order")
          .add(Restrictions.eq("order." + Order.PROPERTY_SALESTRANSACTION, false)).add(Restrictions
              .eq("order." + Order.PROPERTY_DOCUMENTNO, regSalesOrderLine.getSvfpaRemotepo()));

      //
      System.out.println("getOrderLineByDocumentNo: " + orderLineList.list().size() + "|"
          + regSalesOrderLine.getSvfpaRemotepo());
      if ((orderLineList != null) && (orderLineList.list().size() > 0)) {
        return (OrderLine) orderLineList.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }// getOrderLineByDocumentNo
}