package com.spocsys.vigfurniture.approval.utils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.base.exception.OBException;
import java.util.List;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
public class OrgDocumentType {
  
  public static DocumentType GetOrgDocType (Organization org, String basedoctype, boolean issotrx, boolean isreturn){
    try{
      final DocumentType DocTypes;
      
      final OBCriteria<DocumentType> RegDocType= OBDal.getInstance().createCriteria(DocumentType.class);
      
      if (!basedoctype.equalsIgnoreCase("0")){
        RegDocType.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, basedoctype));
        RegDocType.add(Restrictions.eq(DocumentType.PROPERTY_ORGANIZATION, org));
        RegDocType.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, issotrx));
        RegDocType.add(Restrictions.eq(DocumentType.PROPERTY_RETURN, isreturn));
      }else{
        RegDocType.add(Restrictions.eq(DocumentType.PROPERTY_ID, "0"));
      }
      if(RegDocType.list().size()!=0){
      DocTypes= RegDocType.list().get(0);
      return DocTypes;
      }else{
        throw new OBException("None of the document Types for "+basedoctype+" in "+org.getName()+" Organization");
      }
      
      
    }catch (Exception e){
      return null;  
    }

  }/// getdoctypes
 
  
}//class