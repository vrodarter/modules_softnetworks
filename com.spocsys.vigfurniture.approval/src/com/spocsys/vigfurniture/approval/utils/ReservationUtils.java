/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012-2015 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package com.spocsys.vigfurniture.approval.utils;

import java.math.BigDecimal;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.onhandquantity.Reservation;
import org.openbravo.service.db.DalConnectionProvider;

class CSResponse {
	String returnValue;
	String returnValueMsg;
	String exito;
}

public class ReservationUtils {
	String returnValue;
	String exito;

	public static Reservation createReserveFromSalesOrderLine(OrderLine soLine, boolean doProcess) throws OBException {
		if (!soLine.getSalesOrder().isSalesTransaction()) {
			throw new OBException(OBMessageUtils.messageBD("cannotReservePurchaseOrder", false));
		}
		if (soLine.getOrderedQuantity().subtract(soLine.getDeliveredQuantity()).compareTo(BigDecimal.ZERO) == 0) {
			throw new OBException(OBMessageUtils.messageBD("cannotReserveDeliveredSalesOrderLine", false));
		}

		// OBDal.getInstance().flush();
		CSResponse cs = null;
		try {
			cs = ReservationUtilsData.createReserveFromSalesOrderLine(OBDal.getInstance().getConnection(false),
					new DalConnectionProvider(false), soLine.getId(), doProcess ? "Y" : "N",
					(String) DalUtil.getId(OBContext.getOBContext().getUser()));
		} catch (ServletException e) {
		}

		if (cs != null && cs.returnValue != null) {
			return OBDal.getInstance().get(Reservation.class, cs.returnValue);
		}

		return null;
	}

	public static Reservation createReserveFromSalesOrderLine(OrderLine soLine) throws OBException {
		if (!soLine.getSalesOrder().isSalesTransaction()) {
			throw new OBException(OBMessageUtils.messageBD("cannotReservePurchaseOrder", false));
		}
		if (soLine.getOrderedQuantity().subtract(soLine.getDeliveredQuantity()).compareTo(BigDecimal.ZERO) == 0) {
			throw new OBException(OBMessageUtils.messageBD("cannotReserveDeliveredSalesOrderLine", false));
		}
		try {
			Reservation regReservation = OBProvider.getInstance().get(Reservation.class);
			//
			regReservation.setClient(soLine.getClient());
			regReservation.setOrganization(soLine.getOrganization());
			regReservation.setCreatedBy(OBContext.getOBContext().getUser());
			regReservation.setUpdatedBy(OBContext.getOBContext().getUser());
			regReservation.setActive(true);
			//
			regReservation.setSalesOrderLine(soLine);
			regReservation.setProduct(soLine.getProduct());
			regReservation.setQuantity(soLine.getOrderedQuantity());
			regReservation.setReservedQty(soLine.getDeliveredQuantity());
			regReservation.setRESStatus("DR");
			regReservation.setRESProcess("PR");
			regReservation.setAttributeSetValue(soLine.getAttributeSetValue());
			regReservation.setUOM(soLine.getUOM());
			//
			// OBDal.getInstance().save(regReservation);
			// OBDal.getInstance().flush();
			return regReservation;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Reservation getReservation(OrderLine pOrderLine, String resStatus) {
		final OBCriteria<Reservation> reservList = OBDal.getInstance().createCriteria(Reservation.class);
		reservList.add(Restrictions.eq(Reservation.PROPERTY_SALESORDERLINE, pOrderLine));
		reservList.add(Restrictions.eq(Reservation.PROPERTY_RESSTATUS, resStatus));
		for (Reservation regReservation : reservList.list()) {
			return regReservation;
		}
		return null;
	}// getReservation

}
