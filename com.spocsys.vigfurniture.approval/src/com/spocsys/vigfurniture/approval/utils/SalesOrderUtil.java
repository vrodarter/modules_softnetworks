/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.approval.utils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.Task;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.openbravo.activiti.ActivitiConstants;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.kernel.KernelUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedOrdV;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.transfer.TableTransf;
import com.spocsys.vigfurnitures.services.SVFSV_Source;

/**
 * 
 */
public class SalesOrderUtil {

  private static final Logger log = Logger.getLogger(SalesOrderUtil.class);

  public static List<Task> getInvolvedTasks(BaseOBObject bob) {
    return null;
  }

  public static boolean isSalesOrderWorkFlowPresent(Order objOrder) {
    BusinessPartner bp;
    //

    final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    final RuntimeService runtimeService = processEngine.getRuntimeService();

    String wfId = "submitApproval";
    // check if there is already a dunning process for this payment plan final
    ProcessInstanceQuery piq = runtimeService.createProcessInstanceQuery()
        .variableValueEquals(SalesOrderConstants.ORDER_ID_WF_PARAM, objOrder.getId())
        .processDefinitionKey(wfId);
    if (piq.singleResult() != null) {
      return true;
    }
    return false;

  }

  public static boolean startSalesOrderWorkFlow(Order objOrder, String wfId, String decision) {
    //
    System.err.println("startSalesOrderWorkFlow");
    //
    if (objOrder.getId() == null) {
      return false;
    }

    final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    final RuntimeService runtimeService = processEngine.getRuntimeService();
    //
    final String localWfId = wfId;
    String tabId = null;
    //
    if (wfId.equals("submitApproval")) {
      tabId = "186";
    } else {
      tabId = "A0B9CBACD323425484E501F7A21946F7";
    }

    // check if there is already process for order id
    final ProcessInstanceQuery piq = runtimeService.createProcessInstanceQuery()
        .variableValueEquals(SalesOrderConstants.ORDER_ID_WF_PARAM, objOrder.getId())
        .processDefinitionKey(wfId);
    if (piq.singleResult() != null) {
      return false;
    }
    //
    System.err.println("params.put");
    //
    final Map<String, Object> params = new HashMap<String, Object>();
    List<String> wareHouseList = Arrays.asList(objOrder.getId());
    params.put(SalesOrderConstants.ORDER_ID_WF_PARAM, objOrder.getId());
    params.put(SalesOrderConstants.SALES_ORDER_WF_DECISION, decision);
    params.put(SalesOrderConstants.BP_WF_PARAM, ActivitiUtil.toSerializableObject(objOrder, 1));
    params.put(SalesOrderConstants.ACTION_WAREHOUSE_LIST, wareHouseList);
    params.put(SalesOrderConstants.ACTION_CANCEL_LIST, wareHouseList);
    params.put(SalesOrderConstants.ACTION_VOID_LIST, wareHouseList);
    params.put(SalesOrderConstants.SALES_ORDER_WF_PARAM,
        ActivitiUtil.toSerializableObject(objOrder, 1));

    // set the target information
    params.put(ActivitiConstants.TARGET_RECORD_ID, objOrder.getId());
    params.put(ActivitiConstants.TARGET_IDENTIFIER, objOrder.getIdentifier());
    params.put(ActivitiConstants.TARGET_ENTITY, Order.ENTITY_NAME);
    params.put(ActivitiConstants.TARGET_TAB_ID, tabId);
    params.put(ActivitiConstants.TARGET_USER_ID, OBContext.getOBContext().getUser().getId());

    // SalesOrderUtil.logInformation(objOrder, "OBDUN_StartedDunningWorkFlow", new String[] { wfId
    // });
    System.err.println("runtimeService.startProcessInstanceByKey: " + wfId + "-" + decision);
    try {
      // start the process in a separate thread to prevent thread variables in the process
      // to interfere with this process
      Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
          System.err.println("runtimeService.startProcessInstanceByKey");
          runtimeService.startProcessInstanceByKey(localWfId, params);
        }
      });
      System.err.println("thread.start()");
      thread.start();
    } catch (Throwable t) {
      // DunningUtil.logError(paymentSchedule, "OBDUN_ErrorWhenStartingWorkFlow",
      // new String[] { t.getMessage() });
      throw new OBException(t);
    }
    return true;
  }

  public static Boolean isValidSalesOrder(Order objOrder) {
    Boolean isValid = false;

    if (!(Boolean) objOrder.get(Order.PROPERTY_SVFPAMANUALHOLD)) {
      isValid = true;
    } else if (objOrder.getSvfpaStatus().equals(SalesOrderConstants.SO_ACTION_WAIT_WH_RELEASE)) {
      isValid = true;
    }
    return isValid;
  }

  public static Boolean updateStatusLineSalesOrder(Order objOrder, String statusLine) {
    Boolean isProcess = false;
    final OBCriteria<OrderLine> orderLineList = OBDal.getInstance().createCriteria(OrderLine.class);
    orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, objOrder.getId()));
    orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_ACTIVE, true));
    //
    for (OrderLine orderLine : orderLineList.list()) {
      orderLine.setSvfpaStatus(statusLine);
      OBDal.getInstance().save(orderLine);
    }

    //
    return isProcess;
  }// updateStatusLineSalesOrder

  public static OrderLine updatePreProcessLineSalesOrder(String salesOrderLineId,
      boolean needPreProcessed) {
    //
    final OrderLine objOrderLine = OBDal.getInstance().get(OrderLine.class, salesOrderLineId);
    objOrderLine.setSvfapprNeedPreprocess(needPreProcessed);
    OBDal.getInstance().save(objOrderLine);
    //
    return objOrderLine;
  }// updatePreProcessLineSalesOrder

  public static void logInformation(Order objOrder, String key, String[] params) {
    log("Information", objOrder, key, params);
  }

  public static void logWarning(Order objOrder, String key, String[] params) {
    log("Warning", objOrder, key, params);
  }

  public static void logError(Order objOrder, String key, String[] params) {
    log("Error", objOrder, key, params);
  }

  private static void log(String logType, Order objOrder, String key, String[] params) {
    //
    final String label = KernelUtils.getInstance().getI18N(key, params);
    log.info(logType + ": " + key);
  }

  public static String allItemStatus(Order objOrder, String strProperty, String strStatus) {
    String allItem = "N";
    final String hql = "select sum(1) as noReg, COALESCE(sum(case " + strProperty + " when '"
        + strStatus + "' then 1 else 0 end),0) as INSTATUS " + " from " + OrderLine.ENTITY_NAME
        + " where " + OrderLine.PROPERTY_SALESORDER + " = :porder_id and "
        + OrderLine.PROPERTY_ACTIVE + " = 'Y' ";

    final Query query = OBDal.getInstance().getSession().createQuery(hql);
    query.setParameter("porder_id", objOrder);
    //
    Long countReg = Long.valueOf("0");
    Long qtyInStock = Long.valueOf("0");
    //
    for (Object ol : query.list()) {
      // the query returns a list of arrays (columns of the query)
      final Object[] os = (Object[]) ol;
      // get element of result
      countReg = (Long) os[0]; // as noReg
      qtyInStock = (Long) os[1]; // as instock
      //

      if (countReg.compareTo(qtyInStock) == 0 && countReg.compareTo(Long.valueOf("0")) > 0) {
        allItem = "Y";
      } else {
        allItem = "N";
      }
    } // for
    return allItem;
  }

  public static String existSpecialProduct(Order objOrder, String strCondition) {
    String oneSpecial = "N";
    final String hql = "select COALESCE(sum(1),0) as noReg, sum(1) as dummy from "
        + OrderLine.ENTITY_NAME + " where " + OrderLine.PROPERTY_SALESORDER
        + ".id = :porder_id and " + OrderLine.PROPERTY_ACTIVE + " = 'Y' and "
        + OrderLine.PROPERTY_PRODUCT + "." + Product.PROPERTY_SVFSVSPECIALORDER + "= 'Y' "
        + strCondition;

    final Query query = OBDal.getInstance().getSession().createQuery(hql);
    query.setParameter("porder_id", objOrder.getId());
    //
    Long countReg = Long.valueOf("0");
    //
    for (Object ol : query.list()) {
      // the query returns a list of arrays (columns of the query)
      final Object[] os = (Object[]) ol;
      // get element of result
      countReg = (Long) os[0]; // as noReg
      //

      if (countReg.compareTo(Long.valueOf("0")) > 0) {
        oneSpecial = "Y";
      }
    } // for
    return oneSpecial;
  }

  public static Boolean isPackingDocument(Order pOrder) {
    boolean ispacking = false;
    final OBCriteria<ShipmentInOut> shipList = OBDal.getInstance()
        .createCriteria(ShipmentInOut.class);
    //
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESTRANSACTION, true));
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_ACTIVE, true));
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESORDER, pOrder));
    if (shipList.count() > 0)
      ispacking = true;
    //
    return ispacking;
  }

  public static boolean deleteSource(OrderLine pOrderLine) {
    boolean iserr = true;
    final OBCriteria<SVFSV_Source> sourceList = OBDal.getInstance()
        .createCriteria(SVFSV_Source.class);
    //
    sourceList.add(Restrictions.eq(SVFSV_Source.PROPERTY_ACTIVE, true));
    sourceList.add(Restrictions.eq(SVFSV_Source.PROPERTY_ORDERLINE, pOrderLine));
    try {
      for (SVFSV_Source sourceItem : sourceList.list()) {
        OBDal.getInstance().remove(sourceItem);
      }
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (iserr) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().flush();
        OBDal.getInstance().commitAndClose();
      }
    }
    return !iserr;
  }

  public static boolean deleteTransfer(OrderLine pOrderLine) {
    boolean iserr = true;
    final OBCriteria<TableTransf> transferList = OBDal.getInstance()
        .createCriteria(TableTransf.class);
    //
    transferList.add(Restrictions.eq(TableTransf.PROPERTY_ACTIVE, true));
    transferList.add(Restrictions.eq(TableTransf.PROPERTY_SALESORDERLINE, pOrderLine));
    try {
      for (TableTransf transferItem : transferList.list()) {
        OBDal.getInstance().remove(transferItem);
      }
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (iserr) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().flush();
        OBDal.getInstance().commitAndClose();
      }
    }
    //
    return !iserr;
  }

  public static boolean disassociatePO(OrderLine pOrderLine) {
    boolean iserr = true;
    final Order objOrder = OBDal.getInstance().get(Order.class, pOrderLine.getSalesOrder().getId());
    final OrderLine objOrderLine = OBDal.getInstance().get(OrderLine.class, pOrderLine.getId());
    // so.poreference::text = c_orderline.em_svfsv_poreference
    try {
      objOrder.setOrderReference(null);
      objOrderLine.setSOPOReference(null);
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (iserr) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().flush();
        OBDal.getInstance().commitAndClose();
      }
    }
    //
    return !iserr;
  }// disassociatePO

  public static boolean disassociateTransfer(OrderLine pOrderLine) {
    boolean iserr = true;
    //
    return !iserr;
  }

  public static boolean voidPO(OrderLine pOrderLine) {
    boolean iserr = true;
    //
    try {
      if (pOrderLine.getSOPOReference() != null) {
        final Order objPurchase = OBDal.getInstance().get(Order.class,
            pOrderLine.getSOPOReference().getSalesOrder().getId());
        objPurchase.setDocumentStatus("VO"); // VOIDED
      }
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (iserr) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().flush();
        OBDal.getInstance().commitAndClose();
      }
    }
    return !iserr;
  }

  public static void viewPolice(String strPolice) {
    if (SalesOrderConstants.isDebug)
      System.err.println(strPolice);
  }// viewPolice

  public static Boolean isSOPay(Order pOrder) {
    Boolean isPay = false;
    BigDecimal total = new BigDecimal("0");
    BigDecimal pay = new BigDecimal("0");
    final String hql = "select COALESCE(sum(" + FIN_PaymentSchedOrdV.PROPERTY_EXPECTED
        + "),0) as total, COALESCE(sum(" + FIN_PaymentSchedOrdV.PROPERTY_RECEIVED
        + "),0) as pay from " + FIN_PaymentSchedOrdV.ENTITY_NAME + " where "
        + OrderLine.PROPERTY_SALESORDER + ".id = :porder_id ";
    //
    final Query query = OBDal.getInstance().getSession().createQuery(hql);
    query.setParameter("porder_id", pOrder.getId());
    //
    for (Object ol : query.list()) {
      // the query returns a list of arrays (columns of the query)
      final Object[] os = (Object[]) ol;
      // get element of result
      total = (BigDecimal) os[0]; // as total
      pay = (BigDecimal) os[1]; // as total
      //
      if (total.compareTo(new BigDecimal("0")) > 0 && total.compareTo(pay) == 0) {
        isPay = true;
      }
    } // for
    return isPay;
  }// isSOPay
}
