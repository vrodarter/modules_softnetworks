isc.Dialog.create
({
	 ID:"SVFAPPR_ApprovalDialog",
     width: "auto",
	 height: "170",
	 title: "Approval",
	 showCloseButton:true,
	 align:"center",
	 items:
     [
        isc.VLayout.create
		({
			 height: "100%",
			 width: "100%",
			 membersMargin: 2,
			 defaultLayoutAlign: "center",
			 members:
			 [
				isc.HTMLFlow.create
				({
				   padding:5,
				   width: "100%",
				   height: "100%",
				   contents:"<p><strong><div>Do you want to approve this Order?</div></p>"
				
                }),
				isc.HLayout.create
				({
					width: "auto",
					membersMargin: 2,
					defaultLayoutAlign: "center",
					members:
					[
					  isc.OBFormButton.create
					  ({
						  title:"Yes",
						  click: function ()
						  {
							 OB.SVFAPPR.Process.VarApproval= "Y";
							 SVFAPPR_ApprovalDialog.hide();
							 OB.SVFAPPR.Process.beginProcess(); 
						  }
					  }),

					  isc.OBFormButton.create
					  ({
						  title:"No",
						  click: function ()
						  {
							 OB.SVFAPPR.Process.VarApproval= "N";
							 SVFAPPR_ApprovalDialog.hide();
							 OB.SVFAPPR.Process.beginProcess();
						  }
					  })
					]
				})
			 ]
		})
 	 ]
});





isc.OBPopup.create
({
    ID: "SVFAPPR_WindowPopupProcess",
    title: "Approval",
    autoSize:true,
    autoCenter: true,
    showMinimizeButton: false,
    showMaximizeButton: false,
    isModal: true,
    showModalMask: true,
    autoDraw: false,
    height: "30%",
    width:"50%",
    closeClick: function () {this.hide();},
    items:
    [
		
		isc.VStack.create
		({
			ID: "SVFAPPR_WindowPopupProcessCapaData",
			width:"100%",
			//visible: false,
			autoDraw: false,
			members:
		    [
		     
		    ]
		})
    ],
    begin: function()
    {
      try
      {
    	  var Height = (this.getHeight() - 28)+"px";
    	  OB.SVFAPPR.WindoLoadingImg = OB.SVFAPPR.WindoLoadingImg.replace(/000HEIGHT/g, Height);
    	  var SVFAPPR_WindoLoadingImgVar = isc.HTMLFlow.create
    	  ({
    	        ID: "SVFAPPRWindoLoadingImg",
    	        contents: OB.SVFAPPR.WindoLoadingImg,
    	        width:"100%",
    	        height:"100%"
    	  });
    	  SVFAPPR_WindowPopupProcessCapaData.removeMembers(SVFAPPR_WindowPopupProcessCapaData.getMembers());
    	  SVFAPPR_WindowPopupProcessCapaData.addMember(SVFAPPR_WindoLoadingImgVar);
       }
       catch(e)
       {
    	  this.hide();
    	  isc.warn("There was an error while it was executing the action");
    	  console.log(e); 
       }
    }
});



OB.SVFAPPR = OB.SVFAPPR || {};

OB.SVFAPPR.Process =
{ 
  VarOrderId:null,		
  VarApproval:null,
  View: null,
  grid: null,
  callback: function (rpcResponse, data, rpcRequest)
  {
	 try
	 {
		 
		 if((data!= null) && (data.result))
		 {
		  
		   if(data.result.status==='nook')
		   {
			   isc.say(OB.I18N.getLabel(data.result.msg));
			   OB.SVFAPPR.Process.View.refresh();
		   }
		   else if (data.result.status==='ok')
		   {
         isc.say(OB.I18N.getLabel('SVFAPPR_WorkflowProcess'));
			   OB.SVFAPPR.Process.View.refresh();
		   }
		 }
	 }
	 catch(e)
	 {
	   console.log(e.message);	 
	 }
	 SVFAPPR_WindowPopupProcess.hide();
	 
  },
  beginProcess: function ()
  {
	  SVFAPPR_WindowPopupProcess.show();
	  SVFAPPR_WindowPopupProcess.begin();
	  
	  //-------------------------------------------
	  var OrderID= OB.SVFAPPR.Process.VarOrderId;
	  var Approved= OB.SVFAPPR.Process.VarApproval;
	  //-------------------------------------------
	  
	  OB.RemoteCallManager.call('com.spocsys.vigfurniture.approval.salesOrder.StartSalesOrderWorkFlowProcessNew', 
	  {
	     "OrderID": OrderID,
	     "Approved": Approved
	  }, {}, OB.SVFAPPR.Process.callback);
  },
  execute: function (params, view)
  {
	try
	{
	  OB.SVFAPPR.Process.grid= params.button.contextView.viewGrid;
	  var RegistroView = OB.SVFAPPR.Process.grid.getSelectedRecords();
	  //---
	  OB.SVFAPPR.Process.VarOrderId = RegistroView[0].id;
	  //---
	  SVFAPPR_ApprovalDialog.show();
	}
	catch(e)
	{
	  console.log(e.message);	
	}
	  
  },
  Approval: function (params, view)
  {
	try
	{
	  OB.SVFAPPR.Process.View= params.button.contextView;	
	  params.action = 'Approval';
      OB.SVFAPPR.Process.execute(params, view);
	}
	catch(e)
	{
	  console.log(e.message);	
	}
  }
};
