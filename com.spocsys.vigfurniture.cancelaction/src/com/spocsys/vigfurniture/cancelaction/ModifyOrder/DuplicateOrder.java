/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.cancelaction.ModifyOrder;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.cancelaction.utils.CancelActionsUtil;
import com.spocsys.vigfurniture.cancelaction.utils.CopyFromOrder;

//

/**
 */
public class DuplicateOrder implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    CancelActionsUtil.viewPolice(this.getClass().getName());
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      // OBContext.setAdminMode(true);
      final String cOrderId = (String) execution.getVariables().get(
          SalesOrderConstants.ORDER_ID_WF_PARAM);
      final String decision = (String) execution.getVariables().get(
          SalesOrderConstants.SALES_ORDER_WF_DECISION);
      final String isDuplicate = (String) execution.getVariables().get(
          SalesOrderConstants.ACTION_DUPLICATE);
      //
      CancelActionsUtil.viewPolice("cOrderId: " + cOrderId + "|" + decision + "|" + isDuplicate);
      if (isDuplicate == null) {
        execution.setVariable(SalesOrderConstants.ACTION_DUPLICATE, "true");
        CopyFromOrder.copyFromOrder(cOrderId);
      }
      execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
