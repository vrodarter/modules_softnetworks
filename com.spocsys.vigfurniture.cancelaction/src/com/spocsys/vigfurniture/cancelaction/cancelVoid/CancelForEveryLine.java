/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.cancelaction.cancelVoid;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.hibernate.criterion.Restrictions;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.cancelaction.utils.CancelActionsUtil;

//

/**
 */
public class CancelForEveryLine implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    CancelActionsUtil.viewPolice(this.getClass().getName());
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    boolean isVoid;
    try {
      final String cOrderId = (String) execution.getVariables()
          .get(SalesOrderConstants.ORDER_ID_WF_PARAM);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      CancelActionsUtil.viewPolice("cOrderId: " + cOrderId);
      List<String> voidOrderList = new ArrayList<String>();
      List<String> cancelList = new ArrayList<String>();
      List<String> wareHouseList = new ArrayList<String>();
      voidOrderList.clear();
      cancelList.clear();
      wareHouseList.clear();
      //
      final OBCriteria<OrderLine> orderLineList = OBDal.getInstance()
          .createCriteria(OrderLine.class);
      orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, objOrder));
      execution.setVariable(SalesOrderConstants.ACTION_WF_NUMRECORD, orderLineList.count());
      for (OrderLine orderLine : orderLineList.list()) {
        if (orderLine.getSvfpaStockavailability() != null && orderLine.getSvfpaAction() != null) {
          CancelActionsUtil
              .viewPolice("orderLine: " + orderLine.getId() + "-" + orderLine.getSvfpaAction() + "-"
                  + orderLine.get(OrderLine.PROPERTY_SVFAPPRPREPROCESS) + "-"
                  + orderLine.get(OrderLine.PROPERTY_SVFPAPROCESSED));
          isVoid = true;
          if (orderLine.getSvfpaAction().equals(SalesOrderConstants.SO_ACTION_RTP)
              && (Boolean) orderLine.get(OrderLine.PROPERTY_SVFPAPROCESSED)) {
            if ((Boolean) orderLine.getSalesOrder().get(Order.PROPERTY_SVFAPPRWHRELEASE)) {
              execution.setVariable(SalesOrderConstants.ORDERLINE_ID_WF_PARAM, orderLine.getId());
              CancelActionsUtil.viewPolice("Add cancel: " + orderLine.getId());
              cancelList.add(orderLine.getId());
            } else {
              CancelActionsUtil.viewPolice("Add warehouse: " + orderLine.getId());
              wareHouseList.add(orderLine.getId());
              isVoid = false;
            }
          } else if (orderLine.getSvfpaAction().equals(SalesOrderConstants.SO_ACTION_SOURCE)
              && (Boolean) orderLine.get(OrderLine.PROPERTY_SVFAPPRPREPROCESS)) {
            if (CancelActionsUtil.existsInSource(orderLine)) {
              CancelActionsUtil.deleteSource(orderLine);
            } else {
              CancelActionsUtil.disassociatePO(orderLine);
            }

          } else if (orderLine.getSvfpaAction().equals(SalesOrderConstants.SO_ACTION_WAITINGFORPO)
              && (Boolean) orderLine.get(OrderLine.PROPERTY_SVFPAPROCESSED)) {
            CancelActionsUtil.disassociatePO(orderLine);
          } else if (orderLine.getSvfpaAction().equals(SalesOrderConstants.SO_ACTION_LINE_ISSUEPO)
              && (Boolean) orderLine.get(OrderLine.PROPERTY_SVFAPPRPREPROCESS)) {
            CancelActionsUtil.voidPO(orderLine);
          } else if (orderLine.getSvfpaAction().equals(SalesOrderConstants.SO_ACTION_LINE_TRANS)
              && (Boolean) orderLine.get(OrderLine.PROPERTY_SVFAPPRPREPROCESS)) {
            if (CancelActionsUtil.isTransferTable(orderLine)) {
              CancelActionsUtil.deleteTransfer(orderLine);
            } else {
              CancelActionsUtil.disassociateTransfer(orderLine);
            }
          } else if (orderLine.getSvfpaAction()
              .equals(SalesOrderConstants.SO_ACTION_LINE_INTERCOMPANY)
              && (Boolean) orderLine.get(OrderLine.PROPERTY_SVFAPPRPREPROCESS)) {
            CancelActionsUtil.voidPO(orderLine);
            CancelActionsUtil.voidPOothersCompany(orderLine);
          } /*
             * else { isVoid = false; }
             */
          //
          if (isVoid) {
            CancelActionsUtil.viewPolice("Add void: " + orderLine.getId());
            voidOrderList.add(orderLine.getId());
          }

        }
      }

      CancelActionsUtil.viewPolice(
          "size: " + wareHouseList.size() + "|" + cancelList.size() + "|" + voidOrderList.size());
      execution.setVariable(SalesOrderConstants.ACTION_WF_CANCELPACK, false);
      execution.setVariable(SalesOrderConstants.ACTION_WF_VOIDORDER, false);
      execution.setVariable(SalesOrderConstants.ACTION_WF_WARERELEASE, false);
      execution.setVariable(SalesOrderConstants.ACTION_WF_WARERELEASE,
          SalesOrderConstants.ACTION_EXIT);
      //
      if (wareHouseList.size() > 0) {
        execution.setVariable(SalesOrderConstants.ACTION_WF_WARERELEASE, true);
        execution.setVariable(SalesOrderConstants.ACTION_WF_NUMRECORD, wareHouseList.size());
        execution.setVariable(SalesOrderConstants.ACTION_WAREHOUSE_LIST, wareHouseList);
        execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
            SalesOrderConstants.ACTION_WAREHOUSE_RELEASE);
      }
      if (cancelList.size() > 0) {
        execution.setVariable(SalesOrderConstants.ACTION_WF_CANCELPACK, true);
        execution.setVariable(SalesOrderConstants.ACTION_WF_NUMRECORD, cancelList.size());
        execution.setVariable(SalesOrderConstants.ACTION_CANCEL_LIST, cancelList);
        execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
            SalesOrderConstants.ACTION_CANCEL_PACK);
      }
      if (voidOrderList.size() > 0) {
        execution.setVariable(SalesOrderConstants.ACTION_WF_VOIDORDER, true);
        execution.setVariable(SalesOrderConstants.ACTION_WF_NUMRECORD, voidOrderList.size());
        execution.setVariable(SalesOrderConstants.ACTION_VOID_LIST, voidOrderList);
        execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
            SalesOrderConstants.ACTION_VOID_ORDER);
      }
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}// CancelForEveryLine
