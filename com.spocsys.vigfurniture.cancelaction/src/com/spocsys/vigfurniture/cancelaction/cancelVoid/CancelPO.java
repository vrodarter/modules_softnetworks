/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.cancelaction.cancelVoid;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.hibernate.criterion.Restrictions;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.cancelaction.utils.CancelActionsUtil;

//

/**
 */
public class CancelPO implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    CancelActionsUtil.viewPolice("CancelPO");
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables()
          .get(SalesOrderConstants.ORDER_ID_WF_PARAM);
      final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      //
      CancelActionsUtil.viewPolice("cOrderId: " + cOrderId + "-" + objOrder.getDocumentNo());
      List<String> voidOrder = new ArrayList<String>();
      //
      if (CancelActionsUtil.isShipmentOrder(objOrder)) {
        execution.setVariable(SalesOrderConstants.ACTION_WF_NUMRECORD, 1);
        execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
            SalesOrderConstants.SO_ERR_SHIPMENT);
      } else {
        objOrder.setSvfpaManualHold(true);
        if (objOrder.getSvfpaStatus().equals(SalesOrderConstants.SO_ACTION_FAP)
            || objOrder.getSvfpaStatus().equals(SalesOrderConstants.SO_ACTION_MAP)
            || objOrder.getSvfpaStatus().equals(SalesOrderConstants.SO_ACTION_WAIT_DEPOSIT_FAP)
            || objOrder.getSvfpaStatus().equals(SalesOrderConstants.SO_ACTION_WAIT_CUSTOMER)) {
          //
          final OBCriteria<OrderLine> orderLineList = OBDal.getInstance()
              .createCriteria(OrderLine.class);
          orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, objOrder));
          for (OrderLine orderLine : orderLineList.list()) {
            voidOrder.add(orderLine.getId());
          }
          execution.setVariable(SalesOrderConstants.ACTION_VOID_LIST, voidOrder);
          execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
              SalesOrderConstants.ACTION_VOID_ORDER);
        } else
          execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM,
              SalesOrderConstants.ACTION_CANCEL_FOR_EVERY_LINE);
      }
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
