/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.cancelaction.cancelVoid;

import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.hibernate.criterion.Restrictions;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.cancelaction.utils.CancelActionsUtil;

//

/**
 */
public class CancelPack implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    CancelActionsUtil.viewPolice(this.getClass().getName());
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables()
          .get(SalesOrderConstants.ORDER_ID_WF_PARAM);
      final String cOrderLineId = (String) execution.getVariables()
          .get(SalesOrderConstants.ORDERLINE_ID_WF_PARAM);
      final OBCriteria<OrderLine> orderLineList = OBDal.getInstance()
          .createCriteria(OrderLine.class);
      final String cancelCurrent = (String) execution.getVariables().get("cancelCurrent");
      List<String> voidOrderList = new ArrayList<String>();
      voidOrderList.clear();
      //

      if (cancelCurrent != null) {
        orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_ID, cancelCurrent));
      } else {
        final Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
        orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, objOrder));
        orderLineList.add(
            Restrictions.ne(OrderLine.PROPERTY_SVFPASTATUS, SalesOrderConstants.SO_ACTION_VOIDED));
      }
      execution.setVariable(SalesOrderConstants.ACTION_WF_NUMRECORD, 1);
      for (OrderLine objOrderLine : orderLineList.list()) {
        //
        /*
         * if (CancelActionsUtil.isPackingDocument(objOrderLine.getSalesOrder())) {
         * CancelActionsUtil.moveBacktoWH(objOrderLine);
         * CancelActionsUtil.voidPacking(objOrderLine.getSalesOrder()); } else
         */ if (CancelActionsUtil.isPickingOrder(objOrderLine)) {
          CancelActionsUtil.viewPolice("Picking VOID");
          CancelActionsUtil.voidMovePicking(objOrderLine);
          OBDal.getInstance().save(objOrderLine);
        }
        voidOrderList.add(objOrderLine.getId());
      }
      // execution.setVariable(SalesOrderConstants.ACTION_VOID_LIST, voidOrderList);
      execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
