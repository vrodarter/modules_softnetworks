/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.cancelaction.cancelVoid;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.activiti.ActivitiUtil;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.cancelaction.utils.CancelActionsUtil;

//

/**
 */
public class VoidOrder implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {
    //
    CancelActionsUtil.viewPolice(this.getClass().getName());
    //
    ActivitiUtil.setOBContext(execution.getVariables(), true);
    boolean err = true;
    try {
      final String cOrderId = (String) execution.getVariables()
          .get(SalesOrderConstants.ORDER_ID_WF_PARAM);
      final String cOrderLineId = (String) execution.getVariables()
          .get(SalesOrderConstants.ORDERLINE_ID_WF_PARAM);
      final String orderIdVoid = (String) execution.getVariables()
          .get(SalesOrderConstants.ORDER_ID_VOID);
      Order objOrder = OBDal.getInstance().get(Order.class, cOrderId);
      final String voidCurrent = (String) execution.getVariables().get("voidCurrent");
      //
      CancelActionsUtil.viewPolice("cOrderLineId: " + cOrderLineId + "|" + voidCurrent + "|"
          + objOrder.get(Order.PROPERTY_SVFPASTATUS) + "|" + orderIdVoid);
      //
      if (!objOrder.get(Order.PROPERTY_SVFPASTATUS)
          .equals(SalesOrderConstants.SO_ACTION_WAIT_WH_RELEASE)) {
        if (orderIdVoid == null) {
          CancelActionsUtil.viewPolice("CancelActionsUtil.reversePayment");
          CancelActionsUtil.reversePayment(objOrder);
          CancelActionsUtil.viewPolice("Set VOID");
          objOrder.setDocumentStatus("VO"); // VOIDED
          objOrder.setSvfpaStatus(SalesOrderConstants.SO_ACTION_VOID);
          OBDal.getInstance().save(objOrder);
          //
          execution.setVariable(SalesOrderConstants.ORDER_ID_VOID, cOrderId);
        }

      }
      //
      OrderLine objOrderLine = OBDal.getInstance().get(OrderLine.class, voidCurrent);
      if (objOrderLine != null) {
        objOrderLine.setSvfpaStatus(SalesOrderConstants.SO_ACTION_VOIDED);
        OBDal.getInstance().save(objOrderLine);
      }
      //
      execution.setVariable(SalesOrderConstants.ACTION_WF_PARAM, SalesOrderConstants.ACTION_EXIT);
      //
      err = false;
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().commitAndClose();
      }
      ActivitiUtil.clearOBContext(true);
    }
  }
}
