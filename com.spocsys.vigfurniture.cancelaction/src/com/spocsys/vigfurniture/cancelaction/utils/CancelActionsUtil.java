/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.cancelaction.utils;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.process.FIN_PaymentProcess;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.pickinglist.PickingList;

import com.spocsys.vigfurniture.approval.salesOrder.SalesOrderConstants;
import com.spocsys.vigfurniture.transfer.Movementline;
import com.spocsys.vigfurniture.transfer.TableTransf;
import com.spocsys.vigfurnitures.services.SVFSV_Source;

/**
 * 
 */
public class CancelActionsUtil {
  private static final Logger log = Logger.getLogger(CancelActionsUtil.class);

  public static Boolean isPackingDocument(Order pOrder) {
    boolean ispacking = false;
    final OBCriteria<ShipmentInOut> shipList = OBDal.getInstance()
        .createCriteria(ShipmentInOut.class);
    //
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESTRANSACTION, true));
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_ACTIVE, true));
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESORDER, pOrder));
    shipList.add(Restrictions.not(Restrictions.in(ShipmentInOut.PROPERTY_DOCUMENTSTATUS,
        new String[] { "VO", "CO", "AP", "NA", "IN", "CL" })));
    shipList.add(Restrictions.ne(ShipmentInOut.PROPERTY_OBWPACKPACKING, true));
    //
    return shipList.count() > 0;
  }

  public static Boolean isPickingDocument(OrderLine pOrderLine) {
    boolean ispicking = false;
    StringBuffer where = new StringBuffer();
    //
    where.append(" as o");
    where.append(" where o." + ShipmentInOut.PROPERTY_SALESORDER + " = :pOrderId");
    where.append("   and o." + ShipmentInOut.PROPERTY_DOCUMENTSTATUS + " <> 'VO' ");
    where.append("   and exists (select 1 from " + ShipmentInOutLine.ENTITY_NAME + " shl");
    where.append("         where shl." + ShipmentInOutLine.PROPERTY_SHIPMENTRECEIPT + ".id = o.id");
    where.append(
        "           and shl." + ShipmentInOutLine.PROPERTY_OBWPLPICKINGLIST + " is not null )");
    //
    OBQuery<ShipmentInOut> shipList = OBDal.getInstance().createQuery(ShipmentInOut.class,
        where.toString());
    shipList.setNamedParameter("pOrderId", pOrderLine.getSalesOrder());
    //
    return shipList.count() > 0;

  }// isPickingDocument

  public static Boolean isPickingOrder(OrderLine pOrderLine) {
    Boolean isPick = false;
    final OBCriteria<PickingList> pickList = OBDal.getInstance().createCriteria(PickingList.class);
    //
    pickList.add(Restrictions.eq(PickingList.PROPERTY_ACTIVE, true));
    pickList.add(Restrictions.eq(PickingList.PROPERTY_DOCUMENTNO,
        "From Order: " + pOrderLine.getSalesOrder().getDocumentNo()));
    pickList.add(Restrictions
        .not(Restrictions.in(PickingList.PROPERTY_PICKLISTSTATUS, new String[] { "CL", "CA" })));

    return pickList.count() > 0;
  }

  public static Boolean isTransferTable(OrderLine pOrderLine) {
    final OBCriteria<TableTransf> transferList = OBDal.getInstance()
        .createCriteria(TableTransf.class);
    //
    transferList.add(Restrictions.eq(TableTransf.PROPERTY_ACTIVE, true));
    transferList.add(Restrictions.eq(TableTransf.PROPERTY_SALESORDERLINE, pOrderLine));
    //
    return transferList.count() > 0;
  }// isTransferTable

  public static boolean deleteSource(OrderLine pOrderLine) {
    boolean iserr = true;
    final OBCriteria<SVFSV_Source> sourceList = OBDal.getInstance()
        .createCriteria(SVFSV_Source.class);
    //
    sourceList.add(Restrictions.eq(SVFSV_Source.PROPERTY_ACTIVE, true));
    sourceList.add(Restrictions.eq(SVFSV_Source.PROPERTY_ORDERLINE, pOrderLine));
    try {
      for (SVFSV_Source sourceItem : sourceList.list()) {
        OBDal.getInstance().remove(sourceItem);
      }
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    } /*
       * finally { if (iserr) { OBDal.getInstance().rollbackAndClose(); } else {
       * OBDal.getInstance().flush(); OBDal.getInstance().commitAndClose(); } }
       */
    return !iserr;
  }// deleteSource

  public static boolean deleteTransfer(OrderLine pOrderLine) {
    boolean iserr = true;
    final OBCriteria<TableTransf> transferList = OBDal.getInstance()
        .createCriteria(TableTransf.class);
    //
    transferList.add(Restrictions.eq(TableTransf.PROPERTY_ACTIVE, true));
    transferList.add(Restrictions.eq(TableTransf.PROPERTY_SALESORDERLINE, pOrderLine));
    try {
      for (TableTransf transferItem : transferList.list()) {
        OBDal.getInstance().remove(transferItem);
      }
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    }
    //
    return !iserr;
  }// deleteTransfer

  public static boolean disassociatePO(OrderLine pOrderLine) {
    boolean iserr = true;
    final Order objOrder = OBDal.getInstance().get(Order.class, pOrderLine.getSalesOrder().getId());
    final OrderLine objOrderLine = OBDal.getInstance().get(OrderLine.class, pOrderLine.getId());
    // so.poreference::text = c_orderline.em_svfsv_poreference
    try {
      objOrder.setOrderReference(null);
      objOrderLine.setSOPOReference(null);
      OBDal.getInstance().save(objOrderLine);
      OBDal.getInstance().save(objOrder);
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    }
    //
    return !iserr;
  }// disassociatePO

  public static boolean disassociateTransfer(OrderLine pOrderLine) {
    boolean iserr = true;
    final OBCriteria<Movementline> transferList = OBDal.getInstance()
        .createCriteria(Movementline.class);
    //
    transferList.add(Restrictions.eq(Movementline.PROPERTY_ACTIVE, true));
    transferList.add(Restrictions.eq(Movementline.PROPERTY_ORDERLINE, pOrderLine));
    for (Movementline regTransfer : transferList.list()) {
      regTransfer.setOrderline(null);
      OBDal.getInstance().save(regTransfer);
    }
    return !iserr;
  }

  public static boolean voidPO(OrderLine pOrderLine) {
    boolean iserr = true;
    //
    try {
      if (pOrderLine.getSOPOReference() != null) {
        final Order objPurchase = OBDal.getInstance().get(Order.class,
            pOrderLine.getSOPOReference().getSalesOrder().getId());
        objPurchase.setDocumentStatus("VO"); // VOIDED
        OBDal.getInstance().save(objPurchase);
      }
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return !iserr;
  }

  public static boolean voidPOothersCompany(OrderLine pOrderLine) {
    Boolean isVoid = false;
    final OBCriteria<OrderLine> orderLineList = OBDal.getInstance().createCriteria(OrderLine.class);
    orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_ACTIVE, true));
    orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SVFAPPRCREATEDOLINEFROM, pOrderLine));
    //
    for (OrderLine regOrderLine : orderLineList.list()) {
      voidPO(regOrderLine);
    } // for
    isVoid = true;
    //
    return isVoid;
  }

  public static boolean voidPacking(Order pOrder) {
    boolean isErr = true;
    final OBCriteria<ShipmentInOut> shipList = OBDal.getInstance()
        .createCriteria(ShipmentInOut.class);
    //
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESTRANSACTION, true));
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_ACTIVE, true));
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESORDER, pOrder));
    shipList.add(Restrictions.not(Restrictions.in(ShipmentInOut.PROPERTY_DOCUMENTSTATUS,
        new String[] { "VO", "CO", "AP", "NA", "IN", "CL" })));
    shipList.add(Restrictions.ne(ShipmentInOut.PROPERTY_OBWPACKPACKING, true));
    for (ShipmentInOut shipInOut : shipList.list()) {
      // void
      shipInOut.setDocumentStatus("VO");
      final OBCriteria<Packing> packList = OBDal.getInstance().createCriteria(Packing.class);
      packList.add(Restrictions.eq(Packing.PROPERTY_ID, shipInOut.getObwpackPackingh().getId()));
      for (Packing pack : packList.list()) {
        pack.setDescription("Pack Voided. " + pack.getDescription());
        OBDal.getInstance().save(pack);
      }
      OBDal.getInstance().save(shipInOut);
    }
    isErr = false;
    //
    return !isErr;
  }

  public static boolean voidShipPicking(Order pOrder) {
    boolean isVoid = false;
    //
    StringBuffer where = new StringBuffer();
    where.append(" as o");
    where.append(" where o." + ShipmentInOutLine.PROPERTY_OBWPLPICKINGLIST + " is not null )");
    where.append(" and exists (select 1 from " + ShipmentInOut.ENTITY_NAME + " sh");
    where.append("               where sh." + ShipmentInOut.PROPERTY_ID + " = o."
        + ShipmentInOutLine.PROPERTY_SHIPMENTRECEIPT + ".id");
    where.append("              and sh." + ShipmentInOut.PROPERTY_DOCUMENTSTATUS + " <> 'VO' ");
    where.append("              and sh." + ShipmentInOut.PROPERTY_SALESORDER + " = :pOrderId)");

    //
    OBQuery<ShipmentInOutLine> shipList = OBDal.getInstance().createQuery(ShipmentInOutLine.class,
        where.toString());
    shipList.setNamedParameter("pOrderId", pOrder);
    for (ShipmentInOutLine shipInOutLine : shipList.list()) {
      // cancel pick list
      setPickListStatus(shipInOutLine.getObwplPickinglist(), "CA");
      // void
      setShipInOutStatus(shipInOutLine.getShipmentReceipt(), "VO");
    }
    isVoid = true;
    //
    return isVoid;
  }// voidShipPicking

  public static boolean voidMovePicking(OrderLine pOrderLine) {
    boolean isVoid = false;
    final OBCriteria<PickingList> pickList = OBDal.getInstance().createCriteria(PickingList.class);
    //
    pickList.add(Restrictions.eq(PickingList.PROPERTY_ACTIVE, true));
    pickList.add(Restrictions.eq(PickingList.PROPERTY_DOCUMENTNO,
        "From Order: " + pOrderLine.getSalesOrder().getDocumentNo()));
    pickList.add(Restrictions
        .not(Restrictions.in(PickingList.PROPERTY_PICKLISTSTATUS, new String[] { "CL", "CA" })));
    //
    for (PickingList regPick : pickList.list()) {
      setPickListStatus(regPick, "CA");
    }
    isVoid = true;
    return isVoid;
  }

  public static void setPickListStatus(PickingList pPickList, String pStatus) {
    final PickingList objPick = OBDal.getInstance().get(PickingList.class, pPickList.getId());
    //
    objPick.setPickliststatus(pStatus);
    OBDal.getInstance().save(objPick);
  }// setPickListStatus

  public static void setShipInOutStatus(ShipmentInOut pShip, String pStatus) {
    final ShipmentInOut objShip = OBDal.getInstance().get(ShipmentInOut.class, pShip);
    //
    objShip.setDocumentStatus(pStatus);
    OBDal.getInstance().save(objShip);
  }// setShipInOutStatus

  public static void viewPolice(String strPolice) {
    if (SalesOrderConstants.isDebug)
      System.err.println(strPolice);
  }// viewPolice

  public static void reversePayment(Order objOrder) {
    String strAction = "RV";
    Boolean isPosOrder = false;
    String paymentDate = OBDateUtils.formatDate(new Date(System.currentTimeMillis()));
    String comingFrom = "TRANSACTION";
    boolean iserr = true;
    final OBCriteria<FIN_PaymentSchedule> paySheduleList = OBDal.getInstance()
        .createCriteria(FIN_PaymentSchedule.class);
    //
    try {
      paySheduleList.add(Restrictions.eq(FIN_PaymentSchedule.PROPERTY_ORDER, objOrder));

      for (FIN_PaymentSchedule payShedule : paySheduleList.list()) {
        final OBCriteria<FIN_PaymentScheduleDetail> paySheduleDetailList = OBDal.getInstance()
            .createCriteria(FIN_PaymentScheduleDetail.class);
        paySheduleDetailList.add(
            Restrictions.eq(FIN_PaymentScheduleDetail.PROPERTY_ORDERPAYMENTSCHEDULE, payShedule));
        paySheduleDetailList
            .add(Restrictions.isNotNull(FIN_PaymentScheduleDetail.PROPERTY_PAYMENTDETAILS));
        for (FIN_PaymentScheduleDetail payScheduleDetail : paySheduleDetailList.list()) {
          final OBCriteria<FIN_PaymentDetail> payDetailList = OBDal.getInstance()
              .createCriteria(FIN_PaymentDetail.class);
          payDetailList.add(Restrictions.eq(FIN_PaymentDetail.PROPERTY_ID,
              payScheduleDetail.getPaymentDetails().getId()));
          for (FIN_PaymentDetail payDetail : payDetailList.list()) {
            final OBCriteria<FIN_Payment> payList = OBDal.getInstance()
                .createCriteria(FIN_Payment.class);
            payList
                .add(Restrictions.eq(FIN_Payment.PROPERTY_ID, payDetail.getFinPayment().getId()));
            for (FIN_Payment pay : payList.list()) {
              FIN_PaymentProcess.doProcessPayment(pay, strAction, isPosOrder, paymentDate,
                  comingFrom);
            }
          }
        }
      }
      iserr = false;
    } catch (Exception e) {
      e.printStackTrace();
    } /*
       * finally { if (iserr) { OBDal.getInstance().rollbackAndClose(); } else {
       * OBDal.getInstance().flush(); OBDal.getInstance().commitAndClose(); } }
       */

  }

  public static Boolean existsInSource(OrderLine pOrderLine) {
    Boolean exist = false;
    //
    final OBCriteria<SVFSV_Source> sourceList = OBDal.getInstance()
        .createCriteria(SVFSV_Source.class);
    sourceList.add(Restrictions.eq(SVFSV_Source.PROPERTY_ORDERLINE, pOrderLine));
    //
    for (SVFSV_Source regSource : sourceList.list()) {
      return true;
    }
    return exist;
  }// existsInSource

  public static Boolean isShipmentOrder(Order pOrder) {
    Boolean isShip = false;
    //
    final OBCriteria<ShipmentInOut> shipList = OBDal.getInstance()
        .createCriteria(ShipmentInOut.class);
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_SALESORDER, pOrder));
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_ACTIVE, true));
    shipList.add(Restrictions.eq(ShipmentInOut.PROPERTY_DOCUMENTSTATUS, "CO"));
    for (ShipmentInOut regShip : shipList.list()) {
      return true;
    } // for
    //
    return isShip;
  }// isShipmentOrder

  public static Boolean moveBacktoWH(OrderLine pOrderLine) {
    Boolean isMove = false;
    //
    //
    return isMove;
  }
}
