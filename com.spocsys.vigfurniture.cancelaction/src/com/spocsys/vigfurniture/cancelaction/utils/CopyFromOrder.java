package com.spocsys.vigfurniture.cancelaction.utils;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

public class CopyFromOrder {
  static Logger log4j = Logger.getLogger(CopyFromOrder.class);

  //
  public static int copyFromOrder(String pOrderId) {
    final Order objOrder = OBDal.getInstance().get(Order.class, pOrderId);
    return copyFromOrder(objOrder);
  }

  public static int copyFromOrder(Order pOrder) {
    String strCOrderID = SequenceIdData.getUUID();
    Boolean err = true;
    //
    try {
      Order objOrder = DuplicateOrder(pOrder, strCOrderID, "DR", "CO");
      OBDal.getInstance().save(objOrder);
      final OBCriteria<OrderLine> orderLineList = OBDal.getInstance()
          .createCriteria(OrderLine.class);
      orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, pOrder));
      for (OrderLine objOrderLine : orderLineList.list()) {
        String strCOrderlineID = SequenceIdData.getUUID();
        OrderLine oOrderLine = DuplicateOrderLine(objOrder, objOrderLine, strCOrderlineID);
        //
        OBDal.getInstance().save(oOrderLine);
      }
      CancelActionsUtil.viewPolice("copyFromOrder");
      err = false;
    } catch (Exception e) {
      e.printStackTrace();
      log4j.error(e.getMessage());
    } finally {
      if (err) {
        OBDal.getInstance().rollbackAndClose();
      } else {
        OBDal.getInstance().flush();
        OBDal.getInstance().commitAndClose();
      }
    }
    //
    return 1;
  }// copyFromOrder

  public static Order DuplicateOrder(Order pOrder, String strOrderId, String strDocStatus,
      String strDocAction) {
    Order objOrder = OBProvider.getInstance().get(Order.class);
    //
    // objOrder.setId(strOrderId);
    objOrder.setClient(pOrder.getClient());
    objOrder.setOrganization(pOrder.getOrganization());
    objOrder.setActive(true);
    objOrder.setCreationDate(pOrder.getCreationDate());
    objOrder.setCreatedBy(pOrder.getCreatedBy());
    objOrder.setUpdated(pOrder.getUpdated());
    objOrder.setUpdatedBy(pOrder.getUpdatedBy());
    objOrder.setSalesTransaction((Boolean) pOrder.get(Order.PROPERTY_SALESTRANSACTION));
    objOrder.setDocumentNo(pOrder.getDocumentNo());
    objOrder.setDocumentStatus(strDocStatus);
    objOrder.setDocumentAction(strDocAction);
    objOrder.setProcessNow((Boolean) pOrder.get(Order.PROPERTY_PROCESSNOW));
    objOrder.setProcessed(false);
    objOrder.setDocumentType(pOrder.getDocumentType());
    objOrder.setTransactionDocument(pOrder.getTransactionDocument());
    objOrder.setDescription(pOrder.getDescription());
    objOrder.setDelivered(false);
    objOrder.setReinvoice((Boolean) pOrder.get(Order.PROPERTY_REINVOICE));
    objOrder.setPrint((Boolean) pOrder.get(Order.PROPERTY_PRINT));
    objOrder.setSelected((Boolean) pOrder.get(Order.PROPERTY_SELECTED));
    objOrder.setSalesRepresentative(pOrder.getSalesRepresentative());
    objOrder.setOrderDate(pOrder.getOrderDate());
    objOrder.setDatePrinted(pOrder.getDatePrinted());
    objOrder.setAccountingDate(pOrder.getAccountingDate());
    objOrder.setBusinessPartner(pOrder.getBusinessPartner());
    objOrder.setInvoiceAddress(pOrder.getInvoiceAddress());
    objOrder.setPartnerAddress(pOrder.getPartnerAddress());
    objOrder.setOrderReference(pOrder.getOrderReference());
    objOrder.setPrintDiscount((Boolean) pOrder.get(Order.PROPERTY_PRINTDISCOUNT));
    objOrder.setCurrency(pOrder.getCurrency());
    objOrder.setFormOfPayment(pOrder.getFormOfPayment());
    objOrder.setPaymentTerms(pOrder.getPaymentTerms());
    objOrder.setInvoiceTerms(pOrder.getInvoiceTerms());
    objOrder.setDeliveryTerms(pOrder.getDeliveryTerms());
    objOrder.setFreightCostRule(pOrder.getFreightCostRule());
    objOrder.setFreightAmount(pOrder.getFreightAmount());
    objOrder.setDeliveryMethod(pOrder.getDeliveryMethod());
    objOrder.setShippingCompany(pOrder.getShippingCompany());
    objOrder.setCharge(pOrder.getCharge());
    objOrder.setChargeAmount(pOrder.getChargeAmount());
    objOrder.setPriority(pOrder.getPriority());
    objOrder.setSummedLineAmount(pOrder.getSummedLineAmount());
    objOrder.setGrandTotalAmount(new BigDecimal(0)/* pOrder.getGrandTotalAmount() */);
    objOrder.setWarehouse(pOrder.getWarehouse());
    objOrder.setPriceList(pOrder.getPriceList());
    objOrder.setPriceIncludesTax((Boolean) pOrder.get(Order.PROPERTY_PRICEINCLUDESTAX));
    objOrder.setSalesCampaign(pOrder.getSalesCampaign());
    objOrder.setProject(pOrder.getProject());
    objOrder.setActivity(pOrder.getActivity());
    objOrder.setPosted(pOrder.getPosted());
    objOrder.setUserContact(pOrder.getUserContact());
    objOrder.setCopyFrom(true);
    objOrder.setDropShipPartner(pOrder.getDropShipPartner());
    objOrder.setDropShipLocation(pOrder.getDropShipLocation());
    objOrder.setDropShipContact(pOrder.getDropShipContact());
    objOrder.setSelfService((Boolean) pOrder.get(Order.PROPERTY_SELFSERVICE));
    objOrder.setTrxOrganization(pOrder.getTrxOrganization());
    objOrder.setStDimension(pOrder.getStDimension());
    objOrder.setNdDimension(pOrder.getNdDimension());
    objOrder.setDeliveryNotes(pOrder.getDeliveryNotes());
    objOrder.setIncoterms(pOrder.getIncoterms());
    objOrder.setINCOTERMSDescription(pOrder.getINCOTERMSDescription());
    objOrder.setGenerateTemplate((Boolean) pOrder.get(Order.PROPERTY_GENERATETEMPLATE));
    objOrder.setDeliveryLocation(pOrder.getDeliveryLocation());
    objOrder.setCopyFromPO(false);
    objOrder.setPaymentMethod(pOrder.getPaymentMethod());
    objOrder.setFINPaymentPriority(pOrder.getFINPaymentPriority());
    objOrder.setPickFromShipment((Boolean) pOrder.get(Order.PROPERTY_PICKFROMSHIPMENT));
    objOrder.setReceiveMaterials((Boolean) pOrder.get(Order.PROPERTY_RECEIVEMATERIALS));
    objOrder.setCreateInvoice((Boolean) pOrder.get(Order.PROPERTY_CREATEINVOICE));
    objOrder.setReturnReason(pOrder.getReturnReason());
    objOrder.setAddOrphanLine((Boolean) pOrder.get(Order.PROPERTY_ADDORPHANLINE));
    objOrder.setAsset(pOrder.getAsset());
    objOrder.setCalculatePromotions((Boolean) pOrder.get(Order.PROPERTY_CALCULATEPROMOTIONS));
    objOrder.setCostcenter(pOrder.getCostcenter());
    objOrder.setQuotation(pOrder.getQuotation()); // convertquotation
    objOrder.setRejectReason(pOrder.getRejectReason());
    objOrder.setValidUntil(pOrder.getValidUntil());
    objOrder.setQuotation(pOrder.getQuotation());
    objOrder.setReservationStatus(pOrder.getReservationStatus()); // so_res_status
    objOrder.setCreatePOLines((Boolean) pOrder.get(Order.PROPERTY_CREATEPOLINES));
    objOrder.setCashVAT((Boolean) pOrder.get(Order.PROPERTY_CASHVAT));
    objOrder.setPickfromreceipt((Boolean) pOrder.get(Order.PROPERTY_PICKFROMRECEIPT));
    //
    return objOrder;
  }

  public static OrderLine DuplicateOrderLine(Order pOrder, OrderLine pOrderLine,
      String strOrderLineId) {
    OrderLine objOrderLine = OBProvider.getInstance().get(OrderLine.class);
    //
    // objOrderLine.setId(strOrderLineId);
    objOrderLine.setClient(pOrderLine.getClient());
    objOrderLine.setOrganization(pOrderLine.getOrganization());
    objOrderLine.setActive((Boolean) pOrderLine.get(OrderLine.PROPERTY_ACTIVE));
    objOrderLine.setCreationDate(pOrderLine.getCreationDate());
    objOrderLine.setCreatedBy(pOrderLine.getCreatedBy());
    objOrderLine.setUpdated(pOrderLine.getUpdated());
    objOrderLine.setUpdatedBy(pOrderLine.getUpdatedBy());
    objOrderLine.setSalesOrder(pOrder);
    objOrderLine.setLineNo(pOrderLine.getLineNo());
    objOrderLine.setBusinessPartner(pOrderLine.getBusinessPartner());
    objOrderLine.setPartnerAddress(pOrderLine.getPartnerAddress());
    objOrderLine.setOrderDate(pOrderLine.getOrderDate());
    objOrderLine.setScheduledDeliveryDate(pOrderLine.getScheduledDeliveryDate());
    objOrderLine.setDateDelivered(pOrderLine.getDateDelivered());
    objOrderLine.setInvoiceDate(pOrderLine.getInvoiceDate());
    objOrderLine.setDescription(pOrderLine.getDescription());
    objOrderLine.setProduct(pOrderLine.getProduct());
    objOrderLine.setWarehouse(pOrderLine.getWarehouse());
    objOrderLine.setDirectShipment((Boolean) pOrderLine.get(OrderLine.PROPERTY_DIRECTSHIPMENT));
    objOrderLine.setUOM(pOrderLine.getUOM());
    objOrderLine.setOrderedQuantity(pOrderLine.getOrderedQuantity());
    objOrderLine.setReservedQuantity(pOrderLine.getReservedQuantity());
    objOrderLine.setDeliveredQuantity(pOrderLine.getDeliveredQuantity());
    objOrderLine.setInvoicedQuantity(pOrderLine.getInvoicedQuantity());
    objOrderLine.setShippingCompany(pOrderLine.getShippingCompany());
    objOrderLine.setCurrency(pOrderLine.getCurrency());
    objOrderLine.setListPrice(pOrderLine.getListPrice());
    objOrderLine.setUnitPrice(pOrderLine.getUnitPrice());
    objOrderLine.setPriceLimit(pOrderLine.getPriceLimit());
    objOrderLine.setLineNetAmount(pOrderLine.getLineNetAmount());
    objOrderLine.setDiscount(pOrderLine.getDiscount());
    objOrderLine.setFreightAmount(pOrderLine.getFreightAmount());
    objOrderLine.setCharge(pOrderLine.getCharge());
    objOrderLine.setChargeAmount(pOrderLine.getChargeAmount());
    objOrderLine.setTax(pOrderLine.getTax());
    objOrderLine.setResourceAssignment(pOrderLine.getResourceAssignment());
    objOrderLine.setSOPOReference(pOrderLine.getSOPOReference());
    objOrderLine.setAttributeSetValue(pOrderLine.getAttributeSetValue());
    objOrderLine.setDescription(pOrderLine.getDescription());
    objOrderLine.setOrderQuantity(pOrderLine.getOrderQuantity());
    objOrderLine.setOrderUOM(pOrderLine.getOrderUOM());
    objOrderLine.setPriceAdjustment(pOrderLine.getPriceAdjustment());
    objOrderLine.setStandardPrice(pOrderLine.getStandardPrice());
    objOrderLine.setCancelPriceAdjustment(
        (Boolean) pOrderLine.get(OrderLine.PROPERTY_CANCELPRICEADJUSTMENT));
    objOrderLine.setOrderDiscount(pOrderLine.getOrderDiscount());
    objOrderLine.setEditLineAmount((Boolean) pOrderLine.get(OrderLine.PROPERTY_EDITLINEAMOUNT));
    objOrderLine.setTaxableAmount(pOrderLine.getTaxableAmount());
    objOrderLine.setGoodsShipmentLine(pOrderLine.getGoodsShipmentLine());
    objOrderLine.setReturnReason(pOrderLine.getReturnReason());
    objOrderLine.setGrossUnitPrice(pOrderLine.getGrossUnitPrice());
    objOrderLine.setLineGrossAmount(pOrderLine.getLineGrossAmount());
    objOrderLine.setGrossListPrice(pOrderLine.getGrossListPrice());
    objOrderLine.setCostcenter(pOrderLine.getCostcenter());
    objOrderLine.setGrossListPrice(null); // grosspricestd
    objOrderLine.setAsset(pOrderLine.getAsset());
    objOrderLine.setWarehouseRule(pOrderLine.getWarehouseRule());
    objOrderLine.setStDimension(pOrderLine.getStDimension());
    objOrderLine.setQuotationLine(pOrderLine.getQuotationLine());
    objOrderLine.setNdDimension(pOrderLine.getNdDimension());
    objOrderLine.setCreateReservation(pOrderLine.getCreateReservation());
    objOrderLine.setProject(pOrderLine.getProject());
    objOrderLine.setReservationStatus(pOrderLine.getReservationStatus());
    objOrderLine
        .setManageReservation((Boolean) pOrderLine.get(OrderLine.PROPERTY_MANAGERESERVATION));
    objOrderLine
        .setManagePrereservation((Boolean) pOrderLine.get(OrderLine.PROPERTY_MANAGEPRERESERVATION));
    objOrderLine.setExplode((Boolean) pOrderLine.get(OrderLine.PROPERTY_EXPLODE));
    objOrderLine.setBOMParent(pOrderLine.getBOMParent());
    //
    objOrderLine.setObwplReadypl((Boolean) pOrderLine.get(OrderLine.PROPERTY_OBWPLREADYPL));
    objOrderLine
        .setSvfapprPreProcess((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFAPPRPREPROCESS));
    objOrderLine.setSvfapprConfirmed((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFAPPRCONFIRMED));
    objOrderLine.setSvfpaOrderlineParent(pOrderLine.getSvfpaOrderlineParent());
    objOrderLine.setSvfpaStockavailability(pOrderLine.getSvfpaStockavailability());
    objOrderLine.setSvfpaAction(pOrderLine.getSvfpaAction());
    objOrderLine.setSvfpaItem((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPAITEM));
    objOrderLine
        .setSvfpaSustituteItem((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPASUSTITUTEITEM));
    objOrderLine.setSvfpaOrderedQty((Long) pOrderLine.get(OrderLine.PROPERTY_SVFPAORDEREDQTY));
    objOrderLine.setSvfpaShipFrom(pOrderLine.getSvfpaShipFrom());
    objOrderLine
        .setSvfpaSpecialOrder((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPASPECIALORDER));
    objOrderLine.setSvfpaSoftKit((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPASOFTKIT));
    objOrderLine.setSvfpaHardKit((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPAHARDKIT));
    objOrderLine.setSvfpaProcessed((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPAPROCESSED));
    objOrderLine.setSvfpaInPickList((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPAINPICKLIST));
    objOrderLine.setSvfpaDiscount((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFPADISCOUNT));
    objOrderLine.setSvfpaStatus(null/* pOrderLine.getSvfpaStatus() */);
    objOrderLine.setSvfpaStockLocation(pOrderLine.getSvfpaStockLocation());
    objOrderLine.setSvfpaPurchaseorder(pOrderLine.getSvfpaPurchaseorder());
    objOrderLine.setSvftrafMovement(pOrderLine.getSvftrafMovement());
    objOrderLine.setSvfsvPoreference(pOrderLine.getSvfsvPoreference());
    objOrderLine.setSvfsvBox(pOrderLine.getSvfsvBox());
    objOrderLine.setSvfsvOrigCOrderline(pOrderLine.getSvfsvOrigCOrderline());
    objOrderLine.setSvfsvOrigCOrder(pOrderLine.getSvfsvOrigCOrder());
    objOrderLine.setSvfsvContainernum(pOrderLine.getSvfsvContainernum());
    objOrderLine.setSvfsvEta(pOrderLine.getSvfsvEta());
    objOrderLine.setSvfsvQtyallocated(pOrderLine.getSvfsvQtyallocated());
    objOrderLine.setSvfapprNeedPreprocess(
        (Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFAPPRNEEDPREPROCESS));
    objOrderLine.setSvfdsDropship((Boolean) pOrderLine.get(OrderLine.PROPERTY_SVFDSDROPSHIP));
    //
    return objOrderLine;
  }
}// CopyFromOrder
