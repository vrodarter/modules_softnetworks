/*
 ************************************************************************************
 * Copyright (C) 2012 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 ************************************************************************************
 */

package com.spocsys.vigfurniture.cancelaction.utils;

import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

/**
 * 
 */
public class HandlerError extends DalBaseProcess {
  protected void doExecute(ProcessBundle bundle) throws Exception {
    final OBError msg = new OBError();
    String message = null;
    String messageType = null;
    //
    message = "@svfcact_err_shipment@";
    messageType = "Error";
    msg.setType(messageType);
    msg.setMessage(message);
    bundle.setResult(msg);
  }
}
