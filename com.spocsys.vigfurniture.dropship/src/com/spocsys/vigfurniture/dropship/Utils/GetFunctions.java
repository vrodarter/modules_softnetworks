package com.spocsys.vigfurniture.dropship.Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.interco.IntercoMatchingDocument;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.ApprovedVendor;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.db.DalConnectionProvider;

public class GetFunctions {

  private static final Logger log = Logger.getLogger(GetFunctions.class);

  public static PriceList getPriceList(BusinessPartner regBusinessPartner, boolean issoTrx) {

    try {
      if (issoTrx == false) {
        if (regBusinessPartner.getPurchasePricelist() != null) {
          return regBusinessPartner.getPurchasePricelist();
        }
      } else {
        if (regBusinessPartner.getPriceList() != null) {
          return regBusinessPartner.getPriceList();
        }
      }

      OBCriteria<PriceList> ListPriceList = OBDal.getInstance().createCriteria(PriceList.class);
      ListPriceList.add(Restrictions.eq(PriceList.PROPERTY_SALESPRICELIST, issoTrx));

      if ((ListPriceList != null) && (ListPriceList.count() > 0)) {
        return ListPriceList.list().get(0);
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static FIN_PaymentMethod getPaymentMethod(Order regOrder, boolean issoTrx) {

    try {
      if (regOrder.getPaymentMethod() != null) {
        if ((issoTrx == false) && (regOrder.getPaymentMethod().isPayoutAllow() == true)) {
          return regOrder.getPaymentMethod();
        } else {
          if ((issoTrx == true) && (regOrder.getPaymentMethod().isPayinAllow() == true)) {
            return regOrder.getPaymentMethod();
          }
        }
      }
      BusinessPartner regBusinessPartner = regOrder.getBusinessPartner();

      if (issoTrx == false) {
        if (regBusinessPartner.getPOPaymentMethod() != null) {
          return regBusinessPartner.getPOPaymentMethod();
        }
      } else {
        if (regBusinessPartner.getPaymentMethod() != null) {
          return regBusinessPartner.getPaymentMethod();
        }
      }

      OBCriteria<FIN_PaymentMethod> ListPaymentMethod = OBDal.getInstance().createCriteria(
          FIN_PaymentMethod.class);
      ListPaymentMethod.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_PAYINALLOW, issoTrx));

      if ((ListPaymentMethod != null) && (ListPaymentMethod.count() > 0)) {
        return ListPaymentMethod.list().get(0);
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static PaymentTerm getPaymentTerm(BusinessPartner regBusinessPartner, boolean issoTrx) {

    try {
      if (issoTrx == false) {
        if (regBusinessPartner.getPOPaymentTerms() != null) {
          return regBusinessPartner.getPOPaymentTerms();
        }
      } else {
        if (regBusinessPartner.getPOPaymentTerms() != null) {
          return regBusinessPartner.getPOPaymentTerms();
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static DocumentType getDocument(String Base, boolean issoTrx) {

    try {
      OBCriteria<DocumentType> ListDocumentType = OBDal.getInstance().createCriteria(
          DocumentType.class);
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, Base));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, issoTrx));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_RETURN, false));

      if ((ListDocumentType.list() != null) && (ListDocumentType.list().size() > 0)) {
        return ListDocumentType.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static List<DocumentType> getDocumentIntercompanyList(String Base, boolean issoTrx) {

    try {
      OBCriteria<DocumentType> ListDocumentType = OBDal.getInstance().createCriteria(
          DocumentType.class);
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, Base));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, issoTrx));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_RETURN, false));
      ListDocumentType.add(Restrictions.eq(DocumentType.PROPERTY_INTERCOISINTERCOMPANY, true));

      if ((ListDocumentType.list() != null) && (ListDocumentType.list().size() > 0)) {
        return ListDocumentType.list();
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static IntercoMatchingDocument getDocumentIntercompany(String Base, boolean issoTrx,
      Organization OrgSource, Organization OrgTarget) {

    try {
      OBCriteria<IntercoMatchingDocument> ListIntercoMatchingDocument = OBDal.getInstance()
          .createCriteria(IntercoMatchingDocument.class);

      List<DocumentType> ListDocumentType = GetFunctions.getDocumentIntercompanyList(Base, issoTrx);

      ListIntercoMatchingDocument.add(Restrictions.in(
          IntercoMatchingDocument.PROPERTY_DOCUMENTTYPE, ListDocumentType));
      ListIntercoMatchingDocument.add(Restrictions.eq(
          IntercoMatchingDocument.PROPERTY_SOURCEORGANIZATION, OrgSource));
      ListIntercoMatchingDocument.add(Restrictions.eq(
          IntercoMatchingDocument.PROPERTY_TARGETORGANIZATION, OrgTarget));

      if ((ListIntercoMatchingDocument.list() != null)
          && (ListIntercoMatchingDocument.list().size() > 0)) {
        return ListIntercoMatchingDocument.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static Locator getLocator(Warehouse regWarehouse) {
    try {

      if ((regWarehouse.getLocatorList() != null) && (regWarehouse.getLocatorList().size() > 0)) {

        return regWarehouse.getLocatorList().get(0);

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  public static TaxRate calculateOrderLineTax(Order regOrder, OrderLine regOrderLine) {

    try {

      if (regOrderLine.getTax().getSalesPurchaseType().equalsIgnoreCase("B") == true) {
        return regOrderLine.getTax();
      } else {
        List<Object> parameters = new ArrayList<Object>();
        parameters.add(regOrderLine.getProduct().getId());
        parameters.add(regOrder.getOrderDate());
        parameters.add(regOrder.getOrganization().getId());
        parameters.add(regOrder.getWarehouse().getId());
        parameters.add(regOrder.getPartnerAddress().getId());
        parameters.add(regOrder.getInvoiceAddress().getId());
        parameters.add(regOrder);
        parameters.add(regOrder.isSalesTransaction());

        String taxId = (String) CallStoredProcedure.getInstance()
            .call("C_Gettax", parameters, null);
        if (taxId != null) {
          return OBDal.getInstance().get(TaxRate.class, taxId);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static ProcessInstance OrderPost(String ID, String ProcessID) {
    ProcessInstance pInstance = null;
    try {
      OBContext.setAdminMode();
      try {
        final org.openbravo.model.ad.ui.Process process = OBDal.getInstance().get(
            org.openbravo.model.ad.ui.Process.class, ProcessID);

        pInstance = OBProvider.getInstance().get(ProcessInstance.class);

        pInstance.setClient(OBContext.getOBContext().getCurrentClient());
        pInstance.setOrganization(OBContext.getOBContext().getCurrentOrganization());
        pInstance.setCreatedBy(OBContext.getOBContext().getUser());
        pInstance.setUpdatedBy(OBContext.getOBContext().getUser());
        pInstance.setProcess(process);
        pInstance.setActive(true);
        pInstance.setRecordID(ID);
        pInstance.setUserContact(OBContext.getOBContext().getUser());

        // final Parameter parameter = OBProvider.getInstance().get(Parameter.class);
        // parameter.setSequenceNumber("1");
        // parameter.setParameterName("Selection");
        // parameter.setString("Y");

        // // set both sides of the bidirectional association
        // pInstance.getADParameterList().add(parameter);
        // parameter.setProcessInstance(pInstance);

        // persist to the db
        OBDal.getInstance().save(pInstance);

        // flush, this gives pInstance an ID
        OBDal.getInstance().flush();

        final Connection connection = OBDal.getInstance().getConnection();
        final PreparedStatement ps = connection
            .prepareStatement("SELECT * FROM SVFSV_C_Order_Post(?)");
        ps.setString(1, pInstance.getId());
        ps.execute();

        // refresh the pInstance as the SP has changed it
        OBDal.getInstance().getSession().refresh(pInstance);

        if (pInstance.getResult() != 1) {
          throw new OBException(pInstance.getErrorMsg());
        } else {
          log.info(getMessage(pInstance.getErrorMsg()));
        }

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        pInstance = null;
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return pInstance;
  }

  public static ProcessInstance InOutPost(String ID, String ProcessID) {
    ProcessInstance pInstance = null;
    try {
      OBContext.setAdminMode();
      try {
        final org.openbravo.model.ad.ui.Process process = OBDal.getInstance().get(
            org.openbravo.model.ad.ui.Process.class, ProcessID);

        pInstance = OBProvider.getInstance().get(ProcessInstance.class);

        pInstance.setClient(OBContext.getOBContext().getCurrentClient());
        pInstance.setOrganization(OBContext.getOBContext().getCurrentOrganization());
        pInstance.setCreatedBy(OBContext.getOBContext().getUser());
        pInstance.setUpdatedBy(OBContext.getOBContext().getUser());
        pInstance.setProcess(process);
        pInstance.setActive(true);
        pInstance.setRecordID(ID);
        pInstance.setUserContact(OBContext.getOBContext().getUser());

        // final Parameter parameter = OBProvider.getInstance().get(Parameter.class);
        // parameter.setSequenceNumber("1");
        // parameter.setParameterName("Selection");
        // parameter.setString("Y");

        // // set both sides of the bidirectional association
        // pInstance.getADParameterList().add(parameter);
        // parameter.setProcessInstance(pInstance);

        // persist to the db
        OBDal.getInstance().save(pInstance);

        // flush, this gives pInstance an ID
        OBDal.getInstance().flush();

        final Connection connection = OBDal.getInstance().getConnection();
        final PreparedStatement ps = connection.prepareStatement("SELECT * FROM m_inout_post0(?)");
        ps.setString(1, pInstance.getId());
        ps.execute();

        // refresh the pInstance as the SP has changed it
        OBDal.getInstance().getSession().refresh(pInstance);

        if (pInstance.getResult() != 1) {
          throw new OBException(getMessage(pInstance.getErrorMsg()));
        } else {
          System.out.println(getMessage(pInstance.getErrorMsg()));
          log.info(getMessage(pInstance.getErrorMsg()));
        }

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return null;

      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return pInstance;
  }

  static String getMessage(String Value) {
    String KeyMessage = "";
    String Message = "";

    String language = OBContext.getOBContext().getLanguage().getLanguage();
    ConnectionProvider conn = new DalConnectionProvider(false);
    // throw new OBException(Utility.messageBD(conn, getMessage(Value), language));

    try {
      if (Value != null) {

        if ((Value.startsWith("@ERROR=@")) && (Value.endsWith("@"))) {
          KeyMessage = Value.substring(8, Value.length() - 1);
          Message = Utility.messageBD(conn, KeyMessage, language);
        } else {
          if ((Value.startsWith("@CODE=@")) && (Value.endsWith("@"))) {
            KeyMessage = Value.substring(8, Value.length() - 1);
            Message = Utility.messageBD(conn, KeyMessage, language);
          } else {
            // @INTERCO_ordCreated@10000028

            try {
              int First = Value.indexOf('@');
              int Last = Value.lastIndexOf('@');

              if ((First != -1) && (Last != -1) && (First != Last)) {
                KeyMessage = Value.substring(First + 1, Last);
                Message = Value.replaceAll("@" + KeyMessage + "@",
                    Utility.messageBD(conn, KeyMessage, language));

              } else {
                KeyMessage = Value;
              }
            } catch (Exception e) {
              e.printStackTrace();
              Message = Value;
            }

          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();

    }

    return Message;
  }

  static public ApprovedVendor getPurchasing(Product regProduct) {

    ApprovedVendor regApprovedVendorMax = null;

    try {

      if ((regProduct.getApprovedVendorList() != null)
          && (regProduct.getApprovedVendorList().size() > 0)) {

        for (ApprovedVendor regApprovedVendor : regProduct.getApprovedVendorList()) {
          if (regApprovedVendorMax == null) {
            regApprovedVendorMax = regApprovedVendor;
          } else {
            if (regApprovedVendorMax.getUpdated().before(regApprovedVendor.getUpdated())) {
              regApprovedVendorMax = regApprovedVendor;
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      regApprovedVendorMax = null;
    }
    return regApprovedVendorMax;
  }
}
