package com.spocsys.vigfurniture.dropship.Utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

public class createGoodReceipt {

  private static final Logger log = Logger.getLogger(createGoodReceipt.class);

  public createGoodReceipt() {
    // TODO Auto-generated constructor stub
  }

  static ShipmentInOut createHeader(Date MovementDate, Order regOrder, boolean useTemporaryWarehouse) {

    try {

      ShipmentInOut regShipmentInOut = OBProvider.getInstance().get(ShipmentInOut.class);

      // ----------------------------

      regShipmentInOut.setClient(regOrder.getClient());
      regShipmentInOut.setOrganization(regOrder.getOrganization());
      regShipmentInOut.setCreatedBy(OBContext.getOBContext().getUser());
      regShipmentInOut.setUpdatedBy(OBContext.getOBContext().getUser());
      regShipmentInOut.setSalesTransaction(false);

      // -----------------------------
      // regShipmentInOut.setDocumentNo("From Order: " + regOrder.getDocumentNo());
      regShipmentInOut.setDocumentAction("CO");
      regShipmentInOut.setDocumentStatus("DR");
      regShipmentInOut.setPosted("N");
      regShipmentInOut.setProcessNow(false);
      regShipmentInOut.setProcessed(false);
      regShipmentInOut.setDocumentType(GetFunctions.getDocument("MMR", false));
      regShipmentInOut.setSalesOrder(regOrder);
      regShipmentInOut.setPrint(false);
      regShipmentInOut.setMovementType("V+");
      regShipmentInOut.setMovementDate(MovementDate);
      regShipmentInOut.setAccountingDate(MovementDate);
      // -------------------------------
      regShipmentInOut.setBusinessPartner(regOrder.getBusinessPartner());
      regShipmentInOut.setPartnerAddress(regOrder.getPartnerAddress());
      regShipmentInOut.setDeliveryLocation(regOrder.getDeliveryLocation());
      // -------------------------------

      if ((useTemporaryWarehouse == true)
          && (regOrder.getOrganization().getSvfdsTempwarehouse() != null)) {
        regShipmentInOut.setWarehouse(regOrder.getOrganization().getSvfdsTempwarehouse());
      } else {
        regShipmentInOut.setWarehouse(regOrder.getWarehouse());
      }

      regShipmentInOut.setDeliveryTerms("A");
      regShipmentInOut.setFreightCostRule("I");
      regShipmentInOut.setFreightAmount(BigDecimal.ZERO);
      regShipmentInOut.setDeliveryMethod("P");
      regShipmentInOut.setChargeAmount(BigDecimal.ZERO);
      regShipmentInOut.setPriority("5");
      regShipmentInOut.setCreateLinesFrom(false);
      regShipmentInOut.setGenerateTo(false);
      regShipmentInOut.setUpdateLines(false);
      regShipmentInOut.setLogistic(false);
      regShipmentInOut.setGenerateLines(false);
      regShipmentInOut.setCalculateFreight(false);
      regShipmentInOut.setReceiveMaterials(false);
      regShipmentInOut.setSendMaterials(false);
      regShipmentInOut.setProcessGoodsJava("CO");
      // regShipmentInOut.setObwpackPacking(false);
      // regShipmentInOut.setObwpackProcessed("DR");
      // regShipmentInOut.setObwpackReactivated("DR");
      // regShipmentInOut.setObwpackPackingrequired(true);

      return regShipmentInOut;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  static ShipmentInOutLine createLine(OrderLine regOrderLine, ShipmentInOut regShipmentInOut,
      Product regProduct, BigDecimal Qty, long Line) {

    try {

      ShipmentInOutLine regShipmentInOutLine = OBProvider.getInstance()
          .get(ShipmentInOutLine.class);
      // ---------------------------------------------

      regShipmentInOutLine.setClient(regShipmentInOut.getClient());
      regShipmentInOutLine.setOrganization(regShipmentInOut.getOrganization());
      regShipmentInOutLine.setCreatedBy(OBContext.getOBContext().getUser());
      regShipmentInOutLine.setUpdatedBy(OBContext.getOBContext().getUser());
      // ------------------------------
      regShipmentInOutLine.setLineNo(Line);
      regShipmentInOutLine.setShipmentReceipt(regShipmentInOut);
      if (regOrderLine.getProduct().getId().equals(regProduct.getId()) == true) {
        regShipmentInOutLine.setSalesOrderLine(regOrderLine);
      }
      regShipmentInOutLine.setStorageBin(GetFunctions.getLocator(regShipmentInOut.getWarehouse()));
      regShipmentInOutLine.setProduct(regProduct);
      regShipmentInOutLine.setUOM(regOrderLine.getUOM());
      regShipmentInOutLine.setMovementQuantity(Qty);
      regShipmentInOutLine.setReinvoice(false);
      regShipmentInOutLine.setAttributeSetValue(regOrderLine.getAttributeSetValue());
      regShipmentInOutLine.setDescriptionOnly(false);
      regShipmentInOutLine.setManagePrereservation(false);
      regShipmentInOutLine.setManagePrereservation(false);
      regShipmentInOutLine.setExplode(false);
      regShipmentInOutLine.setSvfdsPurchaseorderid(regOrderLine.getId());
      // // ----------------------------------------------
      // regShipmentInOutLine.setObwplPickinglist(regPickingList);
      // // ----------------------------------------------
      // regShipmentInOutLine.setObwplEditlinesPe(false);
      // regShipmentInOutLine.setObwplRemoveline(false);

      return regShipmentInOutLine;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static ShipmentInOut create(Date MovementDate, List<OrderLine> ListOrderLine,
      boolean useTemporaryWarehouse, HashMap<String, RecordRequest> ListRecordRequest) {

    // List<ShipmentInOutLine> regShipmentInOutLineList = new ArrayList<ShipmentInOutLine>();
    ShipmentInOut regShipmentInOut = null;

    if (ListOrderLine.size() > 0) {

      regShipmentInOut = createHeader(MovementDate, ListOrderLine.get(0).getSalesOrder(),
          useTemporaryWarehouse);

      if (regShipmentInOut == null) {
        throw new OBException("There was an error creating Good receipt header");
      }

      OBDal.getInstance().save(regShipmentInOut);
      OBDal.getInstance().flush();

      int LastNoLine = 0;
      Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
      for (OrderLine regOrderLine : ListOrderLine) {

        LastNoLine = LastNoLine + 10;
        nroLine = Long.parseLong(String.valueOf(LastNoLine));
        ShipmentInOutLine regShipmentInOutLine = null;

        BigDecimal newQuantity = regOrderLine.getOrderedQuantity();

        // if ((ListRecordRequest != null)
        // && (ListRecordRequest.containsKey(regOrderLine.getId()) == true)) {
        //
        // newQuantity = ListRecordRequest.get(regOrderLine.getId()).Quantity;
        //
        // }

        // regShipmentInOutLine = createLine(regOrderLine, regShipmentInOut,
        // regOrderLine.getProduct(), newQuantity, nroLine);

        if ((regOrderLine.getProduct().isBillOfMaterials() == true)
            && (regOrderLine.getProduct().getProductBOMList().size() > 0)) {

          // regShipmentInOut.setSalesOrder(null);
          // OBDal.getInstance().save(regShipmentInOut);
          // OBDal.getInstance().flush();

          for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {

            nroLine = Long.parseLong(String.valueOf(LastNoLine));

            regShipmentInOutLine = createLine(regOrderLine, regShipmentInOut,
                regProductBOM.getBOMProduct(),
                regProductBOM.getBOMQuantity().multiply(newQuantity), nroLine);

            LastNoLine = LastNoLine + 10;

            if (regShipmentInOutLine == null) {
              throw new OBException("There was an error creating Good receipt line");
            }

            OBDal.getInstance().save(regShipmentInOutLine);
            OBDal.getInstance().flush();
            // regShipmentInOutLineList.add(regShipmentInOutLine);

          }

        } else {

          regShipmentInOutLine = createLine(regOrderLine, regShipmentInOut,
              regOrderLine.getProduct(), newQuantity, nroLine);

          if (regShipmentInOutLine == null) {
            throw new OBException("There was an error creating Good receipt line");
          }
          OBDal.getInstance().save(regShipmentInOutLine);
          OBDal.getInstance().flush();
          // OBDal.getInstance().flush();
          // regShipmentInOutLineList.add(regShipmentInOutLine);

        }

        // if (regShipmentInOutLine == null) {
        // throw new OBException("There was an error creating Good receipt line");
        // }
        // regShipmentInOutLineList.add(regShipmentInOutLine);

      }

      try {
        // regShipmentInOut.setMaterialMgmtShipmentInOutLineList(regShipmentInOutLineList);
        OBDal.getInstance().save(regShipmentInOut);
        OBDal.getInstance().flush();

        ProcessInstance regProcessInstance = GetFunctions
            .InOutPost(regShipmentInOut.getId(), "109");

        if (regProcessInstance == null) {
          throw new OBException("There was an error booking Good Receipt document");
        }
        // OBDal.getInstance().commitAndClose();

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        throw new OBException(e.getMessage());
      }
      return regShipmentInOut;

    }
    return null;

  }
}
