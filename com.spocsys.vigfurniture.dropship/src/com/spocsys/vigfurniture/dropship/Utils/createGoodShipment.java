package com.spocsys.vigfurniture.dropship.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

import com.spocsys.vigfurniture.incomingproduct.OrderReference;

public class createGoodShipment {

  private static final Logger log = Logger.getLogger(createGoodShipment.class);

  public createGoodShipment() {
    // TODO Auto-generated constructor stub
  }

  static ShipmentInOut createHeader(Date MovementDate, Order regPurchaseOrder, Order regSalesOrder,
      boolean useTemporaryWarehouse, boolean useDropShipSO) {

    try {

      ShipmentInOut regShipmentInOut = OBProvider.getInstance().get(ShipmentInOut.class);

      // ----------------------------

      regShipmentInOut.setClient(regSalesOrder.getClient());
      regShipmentInOut.setOrganization(regSalesOrder.getOrganization());
      regShipmentInOut.setCreatedBy(OBContext.getOBContext().getUser());
      regShipmentInOut.setUpdatedBy(OBContext.getOBContext().getUser());
      regShipmentInOut.setSalesTransaction(true);

      // -----------------------------
      // regShipmentInOut.setDocumentNo("From Order: " + regOrder.getDocumentNo());
      regShipmentInOut.setDocumentAction("CO");
      regShipmentInOut.setDocumentStatus("DR");
      regShipmentInOut.setPosted("N");
      regShipmentInOut.setProcessNow(false);
      regShipmentInOut.setProcessed(false);
      regShipmentInOut.setDocumentType(GetFunctions.getDocument("MMS", true));

      regShipmentInOut.setSalesOrder(regSalesOrder);

      regShipmentInOut.setPrint(false);
      regShipmentInOut.setMovementType("C-");
      regShipmentInOut.setMovementDate(MovementDate);
      regShipmentInOut.setAccountingDate(MovementDate);
      // -------------------------------

      if ((useDropShipSO == true) && (regSalesOrder.isSvfdsDropship() == true)
          && (regSalesOrder.getDropShipPartner() != null)
          && (regSalesOrder.getDropShipLocation() != null)) {
        regShipmentInOut.setBusinessPartner(regSalesOrder.getDropShipPartner());
        regShipmentInOut.setPartnerAddress(regSalesOrder.getDropShipLocation());
      } else {
        regShipmentInOut.setBusinessPartner(regSalesOrder.getBusinessPartner());
        regShipmentInOut.setPartnerAddress(regSalesOrder.getPartnerAddress());
      }
      regShipmentInOut.setDeliveryLocation(regSalesOrder.getDeliveryLocation());
      // -------------------------------

      if ((useTemporaryWarehouse == true)
          && (regSalesOrder.getOrganization().getSvfdsTempwarehouse() != null)) {
        regShipmentInOut.setWarehouse(regSalesOrder.getOrganization().getSvfdsTempwarehouse());
      } else {
        regShipmentInOut.setWarehouse(regSalesOrder.getWarehouse());
      }

      regShipmentInOut.setDeliveryTerms("A");
      regShipmentInOut.setFreightCostRule("I");
      regShipmentInOut.setFreightAmount(BigDecimal.ZERO);
      regShipmentInOut.setDeliveryMethod("P");
      regShipmentInOut.setChargeAmount(BigDecimal.ZERO);
      regShipmentInOut.setPriority("5");
      regShipmentInOut.setCreateLinesFrom(false);
      regShipmentInOut.setGenerateTo(false);
      regShipmentInOut.setUpdateLines(false);
      regShipmentInOut.setLogistic(false);
      regShipmentInOut.setGenerateLines(false);
      regShipmentInOut.setCalculateFreight(false);
      regShipmentInOut.setReceiveMaterials(false);
      regShipmentInOut.setSendMaterials(false);
      regShipmentInOut.setProcessGoodsJava("CO");
      // regShipmentInOut.setObwpackPacking(false);
      // regShipmentInOut.setObwpackProcessed("DR");
      // regShipmentInOut.setObwpackReactivated("DR");
      // regShipmentInOut.setObwpackPackingrequired(true);

      return regShipmentInOut;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  static ShipmentInOutLine createLine(OrderLine regOrderLine, ShipmentInOut regShipmentInOut,
      Product regProduct, BigDecimal Qty, long Line) {

    try {

      ShipmentInOutLine regShipmentInOutLine = OBProvider.getInstance()
          .get(ShipmentInOutLine.class);
      // ---------------------------------------------

      regShipmentInOutLine.setClient(regShipmentInOut.getClient());
      regShipmentInOutLine.setOrganization(regShipmentInOut.getOrganization());
      regShipmentInOutLine.setCreatedBy(OBContext.getOBContext().getUser());
      regShipmentInOutLine.setUpdatedBy(OBContext.getOBContext().getUser());
      // ------------------------------
      regShipmentInOutLine.setLineNo(Line);
      regShipmentInOutLine.setShipmentReceipt(regShipmentInOut);

      if (regOrderLine.getProduct().getId().equals(regProduct.getId()) == true) {
        regShipmentInOutLine.setSalesOrderLine(regOrderLine);
      }
      regShipmentInOutLine.setStorageBin(GetFunctions.getLocator(regShipmentInOut.getWarehouse()));
      regShipmentInOutLine.setProduct(regProduct);
      regShipmentInOutLine.setUOM(regOrderLine.getUOM());
      regShipmentInOutLine.setMovementQuantity(Qty);
      regShipmentInOutLine.setReinvoice(false);
      regShipmentInOutLine.setAttributeSetValue(regOrderLine.getAttributeSetValue());
      regShipmentInOutLine.setDescriptionOnly(false);
      regShipmentInOutLine.setManagePrereservation(false);
      regShipmentInOutLine.setManagePrereservation(false);
      regShipmentInOutLine.setExplode(false);
      // // ----------------------------------------------
      // regShipmentInOutLine.setObwplPickinglist(regPickingList);
      // // ----------------------------------------------
      // regShipmentInOutLine.setObwplEditlinesPe(false);
      // regShipmentInOutLine.setObwplRemoveline(false);

      return regShipmentInOutLine;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static ShipmentInOut create(Date MovementDate, Order regPurchaseOrder,
      List<OrderReference> ListOrderLineReference,/* List<OrderLine> ListOrderLine, */
      boolean useTemporaryWarehouse, boolean useDropShipSO,
      HashMap<String, RecordRequest> ListRecordRequest) {

    // List<ShipmentInOutLine> regShipmentInOutLineList = new ArrayList<ShipmentInOutLine>();
    ShipmentInOut regShipmentInOut = null;
    if (ListOrderLineReference.size() > 0) {
      regShipmentInOut = createHeader(MovementDate, regPurchaseOrder, ListOrderLineReference.get(0)
          .getSalesOrder(), useTemporaryWarehouse, useDropShipSO);

      if (regShipmentInOut == null) {
        throw new OBException("There was an error creating Good shipment header");
      }

      OBDal.getInstance().save(regShipmentInOut);
      OBDal.getInstance().flush();

      int LastNoLine = 0;
      Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
      for (OrderReference regOrderLineReference : ListOrderLineReference) {
        OrderLine regOrderLine = regOrderLineReference.getOrderlineref();
        LastNoLine = LastNoLine + 10;
        nroLine = Long.parseLong(String.valueOf(LastNoLine));
        ShipmentInOutLine regShipmentInOutLine = null;

        BigDecimal newQuantity = regOrderLineReference.getQuantityAllocated();

        // BigDecimal newQuantity = regOrderLine.getOrderedQuantity();
        //
        // if ((ListRecordRequest != null)
        // && (ListRecordRequest.containsKey(regOrderLine.getId()) == true)) {
        //
        // newQuantity = ListRecordRequest.get(regOrderLine.getId()).Quantity;
        //
        // }

        // regShipmentInOutLine = createLine(regOrderLine, regShipmentInOut,
        // regOrderLine.getProduct(), newQuantity, nroLine);

        if ((regOrderLine.getProduct().isBillOfMaterials() == true)
            && (regOrderLine.getProduct().getProductBOMList().size() > 0)) {

          // regShipmentInOut.setSalesOrder(null);
          // OBDal.getInstance().save(regShipmentInOut);
          // OBDal.getInstance().flush();

          for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {

            nroLine = Long.parseLong(String.valueOf(LastNoLine));

            regShipmentInOutLine = createLine(regOrderLine, regShipmentInOut,
                regProductBOM.getBOMProduct(),
                regProductBOM.getBOMQuantity().multiply(newQuantity), nroLine);

            LastNoLine = LastNoLine + 10;

            if (regShipmentInOutLine == null) {
              throw new OBException("There was an error creating Good Shipment line");
            }
            regOrderLine.setSvfpaStatus("SVFAPPR_Delivered");
            OBDal.getInstance().save(regOrderLine);
            OBDal.getInstance().save(regShipmentInOutLine);
            OBDal.getInstance().flush();
            // regShipmentInOutLineList.add(regShipmentInOutLine);

          }

        } else {

          regShipmentInOutLine = createLine(regOrderLine, regShipmentInOut,
              regOrderLine.getProduct(), newQuantity, nroLine);

          if (regShipmentInOutLine == null) {
            throw new OBException("There was an error creating Good Shipment line");
          }
          regOrderLine.setSvfpaStatus("SVFAPPR_Delivered");
          OBDal.getInstance().save(regOrderLine);
          OBDal.getInstance().save(regShipmentInOutLine);
          OBDal.getInstance().flush();
          // regShipmentInOutLineList.add(regShipmentInOutLine);

        }
        // if (regShipmentInOutLine == null) {
        // throw new OBException("There was an error creating Good shipment line");
        // }
        // regShipmentInOutLineList.add(regShipmentInOutLine);

        regOrderLineReference.setDelivered(true);
        OBDal.getInstance().save(regOrderLineReference);

      }

      try {
        // regShipmentInOut.setMaterialMgmtShipmentInOutLineList(regShipmentInOutLineList);
        OBDal.getInstance().save(regShipmentInOut);
        OBDal.getInstance().flush();

        ProcessInstance regProcessInstance = GetFunctions
            .InOutPost(regShipmentInOut.getId(), "109");

        if (regProcessInstance == null) {
          throw new OBException("There was an error booking Good Shipment document");
        }
        // OBDal.getInstance().commitAndClose();

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        throw new OBException(e.getMessage());
      }
      return regShipmentInOut;

    }
    return null;

  }

  public static HashMap<String, List<OrderReference>> ProcessLines(
      List<OrderReference> ListOrderReference) {

    HashMap<String, List<OrderReference>> HashMapOrderReference = new HashMap<String, List<OrderReference>>();

    // Organization + Business Partner + Location + Stock Location (Campo esta a nivel linea de la
    // orden de venta)
    // Organization + Drop Ship BP + Drop Ship Location + Stock Location (Campo esta a nivel linea
    // de la orden de venta)

    try {

      for (OrderReference regOrderReference : ListOrderReference) {

        String OrgKey = regOrderReference.getOrderlineref().getSalesOrder().getOrganization()
            .getId();
        String BPKey = regOrderReference.getOrderlineref().getSalesOrder().getBusinessPartner()
            .getId();
        String BPLocationKey = regOrderReference.getOrderlineref().getSalesOrder()
            .getPartnerAddress().getId();
        // String WarehouseKey = regOrderReference.getOrderlineref().getSvfpaStockLocation() != null
        // ?regOrderReference.getOrderlineref().getSvfpaStockLocation().getId();

        if (regOrderReference.getOrderlineref().getSalesOrder().isSvfdsDropship() == true) {

          if (regOrderReference.getOrderlineref().getSalesOrder().getDropShipPartner() != null) {
            if (regOrderReference.getOrderlineref().getSalesOrder().getDropShipLocation() != null) {
              BPKey = regOrderReference.getOrderlineref().getSalesOrder().getDropShipPartner()
                  .getId();
              BPLocationKey = regOrderReference.getOrderlineref().getSalesOrder()
                  .getDropShipLocation().getId();
            }
          }
        }

        String Key = OrgKey + "_" + BPKey + "_" + BPLocationKey;// + "_" + WarehouseKey;

        if (HashMapOrderReference.containsKey(Key) == true) {
          List<OrderReference> ListOrderReferenceNew = HashMapOrderReference.get(Key);
          ListOrderReferenceNew.add(regOrderReference);
          HashMapOrderReference.put(Key, ListOrderReferenceNew);
        } else {
          List<OrderReference> ListOrderReferenceNew = new ArrayList<OrderReference>();
          ListOrderReferenceNew.add(regOrderReference);
          HashMapOrderReference.put(Key, ListOrderReferenceNew);
        }

      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      throw new OBException(e.getMessage());
    }
    return HashMapOrderReference;
  }

  public static List<ShipmentInOut> createList(Date MovementDate, Order regPurchaseOrder,
      List<OrderReference> ListOrderLineReference, boolean useTemporaryWarehouse,
      boolean useDropShipSO, HashMap<String, RecordRequest> ListRecordRequest) {

    List<ShipmentInOut> ListShipmentInOut = new ArrayList<ShipmentInOut>();
    HashMap<String, List<OrderReference>> HashMapOrderReference = ProcessLines(ListOrderLineReference);

    try {

      Iterator it = HashMapOrderReference.entrySet().iterator();
      while (it.hasNext()) {
        Map.Entry e = (Map.Entry) it.next();

        List<OrderReference> ListOrderLineReferenceNew = (List<OrderReference>) e.getValue();

        ShipmentInOut regShipmentInOut = create(MovementDate, regPurchaseOrder,
            ListOrderLineReferenceNew, useTemporaryWarehouse, useDropShipSO, ListRecordRequest);

        if (regShipmentInOut != null) {
          ListShipmentInOut.add(regShipmentInOut);
        } else {
          throw new OBException("There was an error creating shipment");
        }

      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      throw new OBException(e.getMessage());
    }

    return ListShipmentInOut;

  }
}
