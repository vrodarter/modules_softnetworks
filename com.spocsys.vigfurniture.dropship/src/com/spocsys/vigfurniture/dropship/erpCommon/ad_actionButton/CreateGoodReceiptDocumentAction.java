package com.spocsys.vigfurniture.dropship.erpCommon.ad_actionButton;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

import com.spocsys.vigfurniture.dropship.Utils.RecordRequest;
import com.spocsys.vigfurniture.dropship.Utils.createGoodReceipt;
import com.spocsys.vigfurniture.dropship.Utils.createGoodShipment;
import com.spocsys.vigfurniture.incomingproduct.OrderReference;

public class CreateGoodReceiptDocumentAction extends BaseActionHandler {

  private static final Logger log = Logger.getLogger(CreateGoodReceiptDocumentAction.class);

  private HashMap<String, RecordRequest> ListRecord = new HashMap<String, RecordRequest>();

  protected JSONObject execute(Map<String, Object> parameters, String data) {
    try {

      final JSONObject JSONreg = new JSONObject(data);
      final JSONObject JSONResult = new JSONObject();
      final JSONObject JSONRegResult = new JSONObject();

      JSONRegResult.put("status", "nook");
      JSONRegResult.put("msg", "");

      JSONArray JSONOrderLines = JSONreg.getJSONArray("Lines");
      String PurchaseID = JSONreg.getString("salesOrder");
      String MovementDate = JSONreg.getString("MovementDate");
      String createShitment = "Y";// JSONreg.getString("createShipment");

      String IDs = getIDs(JSONOrderLines);
      String IDsOrderReference = getIDsOrderLineReference(JSONOrderLines);

      String dateFormat = "yyyy-MM-dd";

      Date MovDate = new Date();

      try {

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        MovDate = formatter.parse(MovementDate);

      } catch (Exception e) {
        e.printStackTrace();
      }

      List<OrderLine> ListOrderLine = getOrderLines(IDs);
      List<OrderReference> ListSalesOrderLineReference = getSalesOrderLinesReference(IDsOrderReference);

      String GoodReceiptDocumentCreated = "";

      try {

        if ((ListOrderLine != null) && (ListOrderLine.size() > 0)) {
          ShipmentInOut regShipmentInOutReceipt = createGoodReceipt.create(MovDate, ListOrderLine,
              true, ListRecord);

          if (regShipmentInOutReceipt == null) {
            throw new OBException("There was an error creating the Good Receipt document");
          }
          GoodReceiptDocumentCreated = regShipmentInOutReceipt.getDocumentNo();

          JSONRegResult.put("status", "ok");
          JSONRegResult.put("msg",
              "Good Receipt document: " + regShipmentInOutReceipt.getDocumentNo()
                  + " has been created");

          // -------------------------------------------
        }

        if ((createShitment != null) && (createShitment.trim().equalsIgnoreCase("Y") == true)) {

          Order regPurchaseOrder = OBDal.getInstance().get(Order.class, PurchaseID);// ListOrderLine.get(0).getSalesOrder();

          // ListOrderLine = ListOrderLineSalesAssociated(ListOrderLine);

          List<ShipmentInOut> ListShipmentInOutShiptment = createGoodShipment.createList(MovDate,
              regPurchaseOrder, ListSalesOrderLineReference, true, true, ListRecord);

          // ShipmentInOut regShipmentInOutShipment = createGoodShipment.create(MovDate,
          // regPurchaseOrder, ListSalesOrderLineReference, true, true, ListRecord);

          String ShipmentDocumentCreated = "";

          try {

            int I = 0;
            for (ShipmentInOut regShipmentInOut : ListShipmentInOutShiptment) {

              if (I > 0) {
                ShipmentDocumentCreated += ", ";
              }
              ShipmentDocumentCreated += regShipmentInOut.getDocumentNo();
              I++;
            }
          } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            ShipmentDocumentCreated = "";
          }

          if (ShipmentDocumentCreated.trim().equals("") == true) {
            throw new OBException("There was an error creating the Good Shipment documents");
          }

          String strMsg = "";

          if ((GoodReceiptDocumentCreated != null)
              && (GoodReceiptDocumentCreated.trim().equals("") != true)) {
            strMsg = "Good Receipt document: " + GoodReceiptDocumentCreated + ", ";
          }

          strMsg += "Good Shipment documents: (" + ShipmentDocumentCreated + ") have been created";

          JSONRegResult.put("status", "ok");
          JSONRegResult.put("msg", strMsg);

        }

        OBDal.getInstance().commitAndClose();
        // --------------------------------------------

      } catch (Exception e) {

        try {
          OBDal.getInstance().rollbackAndClose();
        } catch (Exception e1) {
          e1.printStackTrace();
          log.error(e1.getMessage());
        }

        JSONRegResult.put("status", "nook");
        JSONRegResult.put("msg", e.getMessage());

      }

      JSONResult.put("result", JSONRegResult);
      return JSONResult;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      throw new OBException(e);
    }
  }

  String getIDs(JSONArray JSONOrderLines) {

    String Ids = "";
    try {

      int Fin = JSONOrderLines.length();

      for (int I = 0; I < Fin; I++) {
        if (I > 0) {
          Ids += ",";
        }

        JSONObject regJSONObject = JSONOrderLines.getJSONObject(I);
        RecordRequest newRecordRequest = new RecordRequest();
        // ------------------------------------------
        newRecordRequest.Quantity = new BigDecimal(regJSONObject.getString("qtyreceived"));
        ListRecord.put(regJSONObject.getString("salesOrderLine"), newRecordRequest);

        // -------------------------------------------
        Ids += "'" + regJSONObject.getString("salesOrderLine") + "'";

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return Ids;
  }

  String getIDsOrderLineReference(JSONArray JSONOrderLines) {

    String Ids = "";
    try {

      int Fin = JSONOrderLines.length();

      for (int I = 0; I < Fin; I++) {
        if (I > 0) {
          Ids += ",";
        }

        JSONObject regJSONObject = JSONOrderLines.getJSONObject(I);
        Ids += "'" + regJSONObject.getString("svfpincRefOrder") + "'";

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return Ids;
  }

  List<OrderLine> ListOrderLineSalesAssociated(List<OrderLine> ListOrderLineSalesAssociated) {

    List<OrderLine> ListOrderLineNew = new ArrayList<OrderLine>();
    HashMap<String, RecordRequest> ListRecordNew = new HashMap<String, RecordRequest>();
    for (OrderLine regOrderLineNew : ListOrderLineSalesAssociated) {

      if (regOrderLineNew.getSvfapprCreatedolinefrom() != null) {
        ListOrderLineNew.add(regOrderLineNew.getSvfapprCreatedolinefrom());
        RecordRequest regRecordRequest = new RecordRequest();
        regRecordRequest.Quantity = ListRecord.get(regOrderLineNew.getId()).Quantity;
        ListRecordNew.put(regOrderLineNew.getSvfapprCreatedolinefrom().getId(), regRecordRequest);
      }

    }
    ListRecord = ListRecordNew;

    return ListOrderLineNew;

  }

  List<OrderLine> getListOrderLineWithoutReceipt(List<OrderLine> ListOrderLine) {

    List<OrderLine> ListOrderLineNew = new ArrayList<OrderLine>();

    try {

      for (OrderLine regOrderLine : ListOrderLine) {

        OBCriteria<ShipmentInOutLine> ListShipmentInOutLine = OBDal.getInstance().createCriteria(
            ShipmentInOutLine.class);
        ListShipmentInOutLine.add(Restrictions.eq(ShipmentInOutLine.PROPERTY_SVFDSPURCHASEORDERID,
            regOrderLine.getId()));

        if ((ListShipmentInOutLine == null) || (ListShipmentInOutLine.list().size() <= 0)) {
          ListOrderLineNew.add(regOrderLine);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return ListOrderLineNew;
  }

  List<OrderLine> getOrderLines(String LinesId) {

    try {

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS ord");
      queryExtend.append(" WHERE ");
      queryExtend.append(" (ord.id IN (" + LinesId + "))");

      // final OBCriteria<TableTransf> ListTableTransf = OBDal.getInstance().createCriteria(
      // TableTransf.class, queryExtend.toString());

      OBQuery<OrderLine> obq = OBDal.getInstance().createQuery(OrderLine.class,
          queryExtend.toString());

      if ((obq != null) && (obq.list().size() > 0)) {

        List<OrderLine> ListOrderLineNew = getListOrderLineWithoutReceipt(obq.list());

        return ListOrderLineNew;
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return null;

  }

  List<OrderReference> getSalesOrderLinesReference(String LinesId) {

    try {

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS ordref");
      queryExtend.append(" WHERE ");
      queryExtend.append(" (ordref.id IN (" + LinesId + "))");

      // final OBCriteria<TableTransf> ListTableTransf = OBDal.getInstance().createCriteria(
      // TableTransf.class, queryExtend.toString());

      OBQuery<OrderReference> obq = OBDal.getInstance().createQuery(OrderReference.class,
          queryExtend.toString());

      if ((obq != null) && (obq.list().size() > 0)) {

        return obq.list();

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return null;

  }
}