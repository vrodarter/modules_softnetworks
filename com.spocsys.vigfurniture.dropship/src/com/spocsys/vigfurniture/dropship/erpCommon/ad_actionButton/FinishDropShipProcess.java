package com.spocsys.vigfurniture.dropship.erpCommon.ad_actionButton;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

import com.spocsys.vigfurniture.dropship.OrderLineIncoming_V;
import com.spocsys.vigfurniture.dropship.Utils.RecordRequest;
import com.spocsys.vigfurniture.dropship.Utils.createGoodReceipt;
import com.spocsys.vigfurniture.dropship.Utils.createGoodShipment;
import com.spocsys.vigfurniture.incomingproduct.OrderReference;
import com.spocsys.vigfurniture.incomingproduct.erpCommon.ad_actionButton.LinkToPOProcess;

public class FinishDropShipProcess extends BaseActionHandler {

  private static final Logger log = Logger.getLogger(FinishDropShipProcess.class);

  private HashMap<String, RecordRequest> ListRecord = new HashMap<String, RecordRequest>();

  protected JSONObject execute(Map<String, Object> parameters, String data) {
    try {

      final JSONObject JSONreg = new JSONObject(data);
      final JSONObject JSONResult = new JSONObject();
      final JSONObject JSONRegResult = new JSONObject();

      JSONRegResult.put("status", "nook");
      JSONRegResult.put("msg", "");

      JSONArray JSONOrderLines = JSONreg.getJSONArray("Lines");

      String strMsgVerify = verifyIntercompanyOrder(JSONOrderLines);
      if ((strMsgVerify == null) || (strMsgVerify.trim().equals("") == true)) {

        System.out.println("0");

        String SalesOrderIds = getSalesOrderIDs(JSONOrderLines);
        List<Order> ListSalesOrder = getOrder(SalesOrderIds);
        List<Order> ListPurchaseOrder = getOrder(getPurchaseOrderIDs(ListSalesOrder));

        try {

          int I = 0;
          String GoodReceiptDocumentCreated = "";
          String ShipmentDocumentCreated = "";

          if ((ListPurchaseOrder != null) && (ListPurchaseOrder.size() > 0)) {
            System.out.println("1");
            for (Order regPurchaseOrder : ListPurchaseOrder) {

              if ((regPurchaseOrder.getOrderLineList() != null)
                  && (regPurchaseOrder.getOrderLineList().size() > 0)) {
                System.out.println("2");

                List<OrderLine> ListOrderLine = getListOrderLineWithoutReceipt(regPurchaseOrder
                    .getOrderLineList());

                // -----------------------------------------------------------------------------
                if ((ListOrderLine != null) && (ListOrderLine.size() > 0)) {
                  ShipmentInOut regShipmentInOutReceipt = createGoodReceipt.create(new Date(),
                      ListOrderLine, true, ListRecord);
                  System.out.println("3");

                  if (regShipmentInOutReceipt == null) {
                    throw new OBException("There was an error creating the Good Receipt document");
                  }

                  if (I > 0) {
                    GoodReceiptDocumentCreated += ", ";
                  }
                  GoodReceiptDocumentCreated += " " + regShipmentInOutReceipt.getDocumentNo();
                  I++;
                }
              }
              // -----------------------------------------------------------------------------
              String IDsOrderReference = getIDsOrderLineReference(regPurchaseOrder);
              List<OrderReference> ListSalesOrderLineReference = getSalesOrderLinesReference(IDsOrderReference);

              if ((ListSalesOrderLineReference != null) && (ListSalesOrderLineReference.size() > 0)) {
                List<ShipmentInOut> ListShipmentInOutShiptment = createGoodShipment.createList(
                    new Date(), regPurchaseOrder, ListSalesOrderLineReference, true, true,
                    ListRecord);
                System.out.println("4");
                // ShipmentInOut regShipmentInOutShipment = createGoodShipment.create(MovDate,
                // regPurchaseOrder, ListSalesOrderLineReference, true, true, ListRecord);

                try {

                  int J = 0;
                  for (ShipmentInOut regShipmentInOut : ListShipmentInOutShiptment) {

                    if (J > 0) {
                      ShipmentDocumentCreated += ",";
                    }
                    ShipmentDocumentCreated += " " + regShipmentInOut.getDocumentNo();
                    J++;
                  }
                } catch (Exception e) {
                  e.printStackTrace();
                  log.error(e.getMessage());
                  ShipmentDocumentCreated = "";
                }

                if (ShipmentDocumentCreated.trim().equals("") == true) {
                  throw new OBException("There was an error creating the Good Shipment documents");
                }

              }

              // -----------------------------------------------------------------------------

            }
          }
          String strMsg = "";

          if ((GoodReceiptDocumentCreated != null)
              && (GoodReceiptDocumentCreated.trim().equals("") != true)) {
            strMsg = "Good Receipt (" + GoodReceiptDocumentCreated + ")";
          }

          if ((ShipmentDocumentCreated != null)
              && (ShipmentDocumentCreated.trim().equals("") != true)) {

            if (strMsg.equals("") != true) {
              strMsg += ",";
            }
            strMsg += "Good Shipment (" + ShipmentDocumentCreated + ")";

          }
          String Status = "ok";

          if ((strMsg == null) || (strMsg.trim().equals("") == true)) {
            strMsg = "This process can't be executed, because the documents already exist.";
            Status = "war";
          } else {
            strMsg += " have been created";
          }

          JSONRegResult.put("status", Status);
          JSONRegResult.put("msg", strMsg);

          OBDal.getInstance().commitAndClose();
        } catch (Exception e) {

          try {
            OBDal.getInstance().rollbackAndClose();
          } catch (Exception e1) {
            e1.printStackTrace();
            log.error(e1.getMessage());
          }

          JSONRegResult.put("status", "nook");
          JSONRegResult.put("msg", e.getMessage());

        }

      } else {
        JSONRegResult.put("status", "war");
        JSONRegResult.put("msg", strMsgVerify);
      }

      JSONResult.put("result", JSONRegResult);
      return JSONResult;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      throw new OBException(e);
    }
  }

  String verifyIntercompanyOrder(JSONArray JSONOrderLines) {

    String Result = "";

    try {

      int I = 0, Fin = JSONOrderLines.length();

      while (I < Fin) {

        JSONObject ObjJSON = JSONOrderLines.getJSONObject(I);

        if (ObjJSON != null) {
          String ID = ObjJSON.getString("salesOrder");
          Order regOrder = OBDal.getInstance().get(Order.class, ID);

          if (regOrder != null) {
            if (regOrder.getIntercoOrigOrder() == null) {
              Result += (I + 1);
            }
          }
        }

        I++;
      }

      if ((Result != null) && (Result.trim().equals("") != true)) {
        Result = "The Lines Nº " + Result
            + ", Can not be processed, because The documents are not drop ship-intercompany order";
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return Result;

  }

  String getSalesOrderIDs(JSONArray JSONOrderLines) {

    String Ids = "";
    try {

      int Fin = JSONOrderLines.length();

      for (int I = 0; I < Fin; I++) {
        if (I > 0) {
          Ids += ",";
        }

        JSONObject regJSONObject = JSONOrderLines.getJSONObject(I);
        Ids += "'" + regJSONObject.getString("salesOrder") + "'";

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return Ids;

  }

  String getPurchaseOrderIDs(List<Order> ListOrder) {

    String Ids = "";

    try {

      int I = 0;
      if ((ListOrder != null) && (ListOrder.size() > 0)) {
        for (Order regOrder : ListOrder) {

          if (I > 0) {
            Ids += ",";
          }

          if (regOrder.getIntercoOrigOrder() != null) {
            Ids += "'" + regOrder.getIntercoOrigOrder().getId() + "'";
          }
          I++;
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return Ids;
  }

  String getPurchaseOrderLineIDs(Order regOrder) {

    String Ids = "";
    try {

      if (regOrder.getOrderLineList() != null) {
        int I = 0;
        for (OrderLine regOrderLine : regOrder.getOrderLineList()) {

          if (I > 0) {
            Ids += ",";
          }
          Ids += "'" + regOrderLine.getId() + "'";
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());

    }
    return Ids;
  }

  List<OrderLine> getListOrderLineWithoutReceipt(List<OrderLine> ListOrderLine) {

    List<OrderLine> ListOrderLineNew = new ArrayList<OrderLine>();

    try {
      if ((ListOrderLine != null) && (ListOrderLine.size() > 0)) {
        for (OrderLine regOrderLine : ListOrderLine) {

          OBCriteria<ShipmentInOutLine> ListShipmentInOutLine = OBDal.getInstance().createCriteria(
              ShipmentInOutLine.class);
          ListShipmentInOutLine.add(Restrictions.eq(
              ShipmentInOutLine.PROPERTY_SVFDSPURCHASEORDERID, regOrderLine.getId()));

          if ((ListShipmentInOutLine == null) || (ListShipmentInOutLine.list().size() <= 0)) {
            ListOrderLineNew.add(regOrderLine);
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return ListOrderLineNew;
  }

  List<Order> getOrder(String Ids) {
    try {
      if ((Ids != null) && (Ids.trim().equals("") != true)) {
        final StringBuilder queryExtend = new StringBuilder();
        queryExtend.append(" AS ord");
        queryExtend.append(" WHERE ");
        queryExtend.append(" (ord.id IN (" + Ids + "))");

        OBQuery<Order> obq = OBDal.getInstance().createQuery(Order.class, queryExtend.toString());

        if ((obq != null) && (obq.list().size() > 0)) {
          return obq.list();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return null;
  }

  String getIDsOrderLineReference(Order regOrder) {
    String Ids = "";
    try {

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS ordref");
      queryExtend.append(" WHERE ");
      queryExtend.append(" (ordref.salesOrder.id IN ('" + regOrder.getId() + "'))");

      OBQuery<OrderLineIncoming_V> obq = OBDal.getInstance().createQuery(OrderLineIncoming_V.class,
          queryExtend.toString());

      if ((obq != null) && (obq.list().size() > 0)) {
        int I = 0;
        for (OrderLineIncoming_V regOrderLineIncoming_V : obq.list()) {
          if (I > 0) {
            Ids += ',';
          }
          Ids += "'" + regOrderLineIncoming_V.getSvfpincRefOrder() + "'";
          I++;
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return Ids;

  }

  List<OrderReference> getOrderReference(String Ids) {
    try {

      if ((Ids != null) && (Ids.trim().equals("") != true)) {

        final StringBuilder queryExtend = new StringBuilder();
        queryExtend.append(" AS ordref");
        queryExtend.append(" WHERE ");
        queryExtend.append(" (ordref.salesOrderLine.id IN (" + Ids + "))");

        OBQuery<OrderReference> obq = OBDal.getInstance().createQuery(OrderReference.class,
            queryExtend.toString());

        if ((obq != null) && (obq.list().size() > 0)) {
          return obq.list();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return null;

  }

  List<OrderReference> getSalesOrderLinesReference(String LinesId) {

    try {

      if ((LinesId != null) && (LinesId.trim().equals("") != true)) {

        final StringBuilder queryExtend = new StringBuilder();
        queryExtend.append(" AS ordref");
        queryExtend.append(" WHERE ");
        queryExtend.append(" (ordref.id IN (" + LinesId + "))");

        // final OBCriteria<TableTransf> ListTableTransf = OBDal.getInstance().createCriteria(
        // TableTransf.class, queryExtend.toString());

        OBQuery<OrderReference> obq = OBDal.getInstance().createQuery(OrderReference.class,
            queryExtend.toString());

        if ((obq != null) && (obq.list().size() > 0)) {

          return obq.list();

        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return null;

  }

}