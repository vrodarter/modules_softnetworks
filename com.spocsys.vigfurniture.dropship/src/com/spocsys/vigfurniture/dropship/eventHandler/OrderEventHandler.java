package com.spocsys.vigfurniture.dropship.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

public class OrderEventHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Order.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Order regOrder = (Order) event.getTargetInstance();

    if (regOrder != null) {
      if (regOrder.isSalesTransaction() == true) {

        Entity OrderEntity = ModelProvider.getInstance().getEntity(Order.ENTITY_NAME);
        // Property PropertyDropShip = OrderEntity.getProperty(Order.PROPERTY_SVFDSDROPSHIP);
        // event.setCurrentState(PropertyDropShip, regOrder.getOrganization().isSVFDSDropShip());

        changeAllLines(regOrder);

        if (regOrder.isSvfdsDropship() == true) {

          if (regOrder.getDropShipPartner() == null) {

            Property PropertyBP = OrderEntity.getProperty(Order.PROPERTY_DROPSHIPPARTNER);
            Property PropertyBPUser = OrderEntity.getProperty(Order.PROPERTY_DROPSHIPCONTACT);
            Property PropertyBPLocation = OrderEntity.getProperty(Order.PROPERTY_DROPSHIPLOCATION);

            event.setCurrentState(PropertyBP, regOrder.getBusinessPartner());
            event.setCurrentState(PropertyBPUser,
                getBusinessPartnerUser(regOrder.getBusinessPartner()));
            event.setCurrentState(
                PropertyBPLocation,
                regOrder.getPartnerAddress() == null ? getBusinessPartnerLocation(regOrder
                    .getBusinessPartner()) : regOrder.getPartnerAddress());
          }

        }
        // Just to create a buffer on hibernate and to do commitAndClose()
        Property PropertyDescription = OrderEntity.getProperty(Order.PROPERTY_DESCRIPTION);
        event.setCurrentState(PropertyDescription, regOrder.getDescription());

      }
    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Order regOrder = (Order) event.getTargetInstance();

    if (regOrder != null) {
      if (regOrder.isSalesTransaction() == true) {

        Entity OrderEntity = ModelProvider.getInstance().getEntity(Order.ENTITY_NAME);
        if (regOrder.getOrganization().isSvfdsDropship() == true) {
          Property PropertyDropShip = OrderEntity.getProperty(Order.PROPERTY_SVFDSDROPSHIP);
          event.setCurrentState(PropertyDropShip, regOrder.getOrganization().isSvfdsDropship());
        }

        if ((regOrder.getOrganization().isSvfdsDropship() == true)
            || (regOrder.isSvfdsDropship() == true)) {
          Property PropertyBP = OrderEntity.getProperty(Order.PROPERTY_DROPSHIPPARTNER);
          Property PropertyBPUser = OrderEntity.getProperty(Order.PROPERTY_DROPSHIPCONTACT);
          Property PropertyBPLocation = OrderEntity.getProperty(Order.PROPERTY_DROPSHIPLOCATION);

          event.setCurrentState(PropertyBP, regOrder.getBusinessPartner());
          event.setCurrentState(PropertyBPUser,
              getBusinessPartnerUser(regOrder.getBusinessPartner()));
          event.setCurrentState(
              PropertyBPLocation,
              regOrder.getPartnerAddress() == null ? getBusinessPartnerLocation(regOrder
                  .getBusinessPartner()) : regOrder.getPartnerAddress());

        }

      }
    }

  }

  Location getBusinessPartnerLocation(BusinessPartner regBusinessPartner) {

    try {

      try {

        OBContext.setAdminMode();

        final StringBuilder queryExtend = new StringBuilder();
        queryExtend.append(" AS lc");
        queryExtend.append(" WHERE (1=1)");
        queryExtend.append(" AND (lc.businessPartner.id = '" + regBusinessPartner.getId() + "')");
        queryExtend.append(" AND (lc.shipToAddress = true)");

        OBQuery<Location> ListLocation = OBDal.getInstance().createQuery(Location.class,
            queryExtend.toString());

        if ((ListLocation != null) && (ListLocation.count() > 0)) {
          return ListLocation.list().get(0);
        }
      } finally {
        OBContext.restorePreviousMode();
      }
    } catch (Exception e) {
      e.printStackTrace();
      logger.error(e.getMessage());
    }
    return null;
  }

  User getBusinessPartnerUser(BusinessPartner regBusinessPartner) {

    try {

      try {

        OBContext.setAdminMode();

        final StringBuilder queryExtend = new StringBuilder();
        queryExtend.append(" AS us");
        queryExtend.append(" WHERE (1=1)");
        queryExtend.append(" AND (us.businessPartner.id = '" + regBusinessPartner.getId() + "')");

        OBQuery<User> ListUser = OBDal.getInstance()
            .createQuery(User.class, queryExtend.toString());

        if ((ListUser != null) && (ListUser.count() > 0)) {
          return ListUser.list().get(0);
        }
      } finally {
        OBContext.restorePreviousMode();
      }
    } catch (Exception e) {
      e.printStackTrace();
      logger.error(e.getMessage());
    }
    return null;
  }

  void changeAllLines(Order regOrder) {
    try {
      if ((regOrder.getOrderLineList() != null) && (regOrder.getOrderLineList().size() > 0)) {
        for (OrderLine regOrderLine : regOrder.getOrderLineList()) {

          regOrderLine.setSvfdsDropship(regOrder.isSvfdsDropship());
          OBDal.getInstance().save(regOrderLine);
          // OBDal.getInstance().flush();
          // OBDal.getInstance().commitAndClose();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      logger.error(e.getMessage());
    }
  }

}