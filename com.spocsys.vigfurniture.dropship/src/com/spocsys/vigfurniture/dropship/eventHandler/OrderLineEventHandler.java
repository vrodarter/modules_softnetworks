package com.spocsys.vigfurniture.dropship.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.model.common.order.OrderLine;

public class OrderLineEventHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(OrderLine.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final OrderLine regOrderLine = (OrderLine) event.getTargetInstance();

    if (regOrderLine != null) {
      if (regOrderLine.getSalesOrder().isSalesTransaction() == true) {

        Entity OrderLineEntity = ModelProvider.getInstance().getEntity(OrderLine.ENTITY_NAME);
        Property PropertyDropShip = OrderLineEntity.getProperty(OrderLine.PROPERTY_SVFDSDROPSHIP);
        event.setCurrentState(PropertyDropShip, regOrderLine.getSalesOrder().isSvfdsDropship());

      }
    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final OrderLine regOrderLine = (OrderLine) event.getTargetInstance();

    if (regOrderLine != null) {
      if (regOrderLine.getSalesOrder().isSalesTransaction() == true) {

        Entity OrderLineEntity = ModelProvider.getInstance().getEntity(OrderLine.ENTITY_NAME);
        Property PropertyDropShip = OrderLineEntity.getProperty(OrderLine.PROPERTY_SVFDSDROPSHIP);
        event.setCurrentState(PropertyDropShip, regOrderLine.getSalesOrder().isSvfdsDropship());

      }
    }

  }

}