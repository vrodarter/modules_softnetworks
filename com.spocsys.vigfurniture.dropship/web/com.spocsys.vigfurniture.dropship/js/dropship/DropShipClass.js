isc.defineClass('SVFDS_PurchaseOrderGrid', isc.OBGrid/*isc.OBPickAndExecuteGrid*/);
isc.defineClass('SVFDS_DropShipInformationView', isc.OBBaseView);

var SVFDS_WindowDropShip = null;
var SVFDS_PurchaseOrderID = '';

isc.SVFDS_PurchaseOrderGrid.addProperties
({
	  dataSource: null,
	  showFilterEditor: true,
	  height: '85%',
	  dataProperties: {
	    useClientFiltering: false
	  },
	  margin: 4,
	  selectionAppearance: 'checkbox',
	  gridFields:
	  [
	   
           {
			    name: 'organization$_identifier',
			    title: 'Organization',
			    type: '_id_10',
			    width: 180,
			    filterOnKeypress: false, 
			    // allow filtering on name
			    
		   },
	       {
		      name: 'documentNo',
		      title: 'Purchase Order',
		      type: '_id_10',
		      width: 120,
		      filterOnKeypress: false, 
		      // allow filtering on name
		      
		   },
		   {
		      name: 'orderDate',
		      title: 'Purchase Order Date',
		      // allow filtering on name
		      editorType: 'OBDateTimeItem',
			  filterEditorType: 'OBMiniDateRangeItem',
			  width: 150, 
	          type: '_id_16',
	          align: 'right',
	          filterOnKeypress: false, 
			  titleOrientation : 'top',
		   },
		   {
		      name: 'eta',
		      title: 'ETA (Estimated Time of Arrival)',
		      // allow filtering on name
		      editorType: 'OBDateTimeItem',
			  filterEditorType: 'OBMiniDateRangeItem',
			  width: 150, 
	          type: '_id_16',
	          align: 'right',
	          filterOnKeypress: false, 
			  titleOrientation : 'top',
		   },
		   {
			  name: 'businessPartner$_identifier',
			  title: 'Purchase Order Business Partner',
			  type: '_id_10',
			 // allow filtering on name
			  align: 'right',
			  filterOnKeypress: true,
			  titleOrientation : 'top',
			  width: 200
		    },
		    {
			   name: 'warehouse$_identifier',
			   title: 'Warehouse',
			   type: '_id_10',
			   // allow filtering on name
			   align: 'right',
			   filterOnKeypress: true,
			   titleOrientation : 'top',
			   width: 200
			},
			{
			   name: 'tempwarehouse$_identifier',
			   title: 'Temporary warehouse',
			   type: '_id_10',
			   // allow filtering on name
			   align: 'right',
			   filterOnKeypress: true,
			   titleOrientation : 'top',
			   width: 200
			},
		    {
			  name: 'documentnoso',
			  title: 'Sales Order',
			  type: '_id_10',
			  width: 120,
			  filterOnKeypress: false, 
			      // allow filtering on name
			},
			{
			  name: 'dateorderedso',
			  title: 'Sales Order Date',
			  // allow filtering on name
			  editorType: 'OBDateTimeItem',
			  filterEditorType: 'OBMiniDateRangeItem',
			  width: 150, 
		      type: '_id_16',
		      align: 'right',
		      filterOnKeypress: false, 
			  titleOrientation : 'top',
			},
			{
			   name: 'bpso$_identifier',
			   title: 'Sales Order Business Partner',
			   type: '_id_10',
			   //allow filtering on name
			   align: 'right',
			   filterOnKeypress: true,
			   titleOrientation : 'top',
			   width: 200
			},
		    {
			  name: 'dropshipbpartnerso$_identifier',
			  title: 'Drop Ship Business Partner',
			  type: '_id_10',
			 // allow filtering on name
			  align: 'right',
			  filterOnKeypress: true,
			  titleOrientation : 'top',
			  width: 200
			},
			{
			  name: 'dropshiplocationso$_identifier',
			  title: 'Drop Ship Address',
			  type: '_id_10',
			 // allow filtering on name
			  align: 'right',
			  filterOnKeypress: true,
			  titleOrientation : 'top',
			  width: 280
			},
		    {
			  name: 'product$_identifier',
			  title: 'Product',
			  type: '_id_10',
			 // allow filtering on name
			  align: 'right',
			  filterOnKeypress: true,
			  titleOrientation : 'top',
			  width: 200
			},
			{
			  name: 'qtyincoming',
			 // allow filtering on name,
			  title: 'Quantity PO',
			  align: 'right',
			  filterOnKeypress: true,
			  titleOrientation : 'top',
			  width: 100
			},
			{
				  name: 'qtyAllocated',
				 // allow filtering on name,
				  title: 'Quantity SO',
				  align: 'right',
				  filterOnKeypress: true,
				  titleOrientation : 'top',
				  width: 100
			},
			{
			   filterOnKeypress: false, 
			   name: 'qtyreceived',
			   title: 'Quantity to Receive',
			   type: '_id_29',
			   canEdit: true,
			   width: 150,
			   hidden:true,
			   change: function(a, item, value)
			   {
				   try
				   {
					  var V= value.toString();
					  V = V.replace(/\,/g, '');
					  V = V.replace(/\./g, '');
					  
					  if((V==null) || (V=='') || (V.length<=0))
					  {
						V=0;
						value=0;
					  }
					  
					  if ((V!= null) && (V!= '') && (V.length > 0))
					  {
						 var re=/^\d+(\.\d+)?$/;
						
						 if((V.length > 11) || (re.test(V)==false))
						 {
						   return false;  
						 }
						 
						 if(V > item.record.qtyincoming)
						 {
						    return false;	 
						 }
						 console.log(item);
						 item.grid.setEditValue(item.rowNum, 'qtyreceived', value);
					     item.grid.data.localData[item.rowNum]['qtyreceived'] = value;
					  } 
					   	  //---------------------------
				    }
				    catch(e)
				    {
					  console.log(e.message); 
				    }
				}
			 }
	  ],
	  cellClick: function (record, rowNum, colNum)
	  {
	     try
	     {
			if(this.getFieldName(colNum)=='documentNo')
		    {
		      OB.Utilities.openDirectTab('294', record.salesOrder, 'GRID', 1, null);
		    }
			else
		    {
				if(this.getFieldName(colNum)=='businessPartner$_identifier')
			    {
			      OB.Utilities.openDirectTab('220', record.businessPartner,'GRID', 1, null);
			    }
				else
				{
					if(this.getFieldName(colNum)=='product$_identifier')
				    {
				      OB.Utilities.openDirectTab('180', record.product,'GRID', 1, null);
				    }
					else
					{
						if(this.getFieldName(colNum)=='dropShipPartner$_identifier')
						{
							OB.Utilities.openDirectTab('220', record.dropShipPartner,'GRID', 1, null);	
						}
						else
						{
							if(this.getFieldName(colNum)=='warehouse$_identifier')
							{
								OB.Utilities.openDirectTab('177', record.warehouse,'GRID', 1, null);	
							}
							else
							{
								if(this.getFieldName(colNum)=='tempwarehouse$_identifier')
								{
									OB.Utilities.openDirectTab('177', record.tempwarehouse,'GRID', 1, null);	
								}
								else
								{
									if(this.getFieldName(colNum)=='organization$_identifier')
									{
										OB.Utilities.openDirectTab('143', record.organization,'GRID', 1, null);	
									}
								}	
							}	
						}	
					}	
				}	
		    }	
	     }
		 catch(e)
		 {
		   console.log(e); 
		 }
	  },
	  cellContextClick: "this.sayCellEvent('Context-clicked', record, colNum); return false;",
	  getCellCSSText: function (record, rowNum, colNum)
	  {
			try
			{
				var Properties = 'color: #006600; text-decoration:underline;cursor:pointer;font-weight:bold;';
				
				if((this.getFieldName(colNum) == 'documentNo') || (this.getFieldName(colNum) == 'businessPartner$_identifier') || (this.getFieldName(colNum) =='product$_identifier') || (this.getFieldName(colNum) =='dropShipPartner$_identifier') || (this.getFieldName(colNum) =='warehouse$_identifier') || (this.getFieldName(colNum) =='tempwarehouse$_identifier') || (this.getFieldName(colNum) =='organization$_identifier'))
				{
				  return Properties;							
				}
					
			}
			catch(e)
			{
			  console.log(e.message);	
			}
	  },
	  onFetchData: function(criteria, requestProperties)
	  {
		  requestProperties = requestProperties || {};
		  requestProperties.params = requestProperties.params || {};
		  requestProperties.params[OB.Constants.WHERE_PARAMETER] = "salesOrder.id='" + SVFDS_PurchaseOrderID + "'";
	  },
	  setDataSource: function(ds)
	  {
	    // is called when the datasource is retrieved from the server
	    this.Super('setDataSource', [ds, this.gridFields]);
	    this.refreshFields();
	    //this.sort('name');
	    //this.fetchData();
	  },
	 
	  initWidget: function()
	  {
	    // get the datasource, if it is not yet defined
	    // it is retrieved from the server and returned
	    // Datasources refer to tables using the entity name
	    OB.Datasource.get('SVFDS_OrderLine_V', this, null, true);
	    this.Super('initWidget', arguments);
	  }
});

//OBPickAndExecuteGrid
//View

isc.SVFDS_DropShipInformationView.addProperties
({
  
  ID: 'SVFDS_DropShipInformationViewVar',
  me: null,
  initWidget: function ()
  {
    
    this.me = this;
    //---------------------------------------------------
    
    this.me.WindowPopupProcess = isc.OBPopup.create
    ({
        ID: "SVFDS_WindowPopupProcess",
        title: "Creating Good receipt Document",
        autoSize:true,
        autoCenter: true,
        showMinimizeButton: false,
        showMaximizeButton: false,
        isModal: true,
        showModalMask: true,
        autoDraw: false,
        height: "30%",
        width:"50%",
        closeClick: function () {this.hide();},
        items:
        [
    		
    		isc.VStack.create
    		({
    			ID: "SVFDS_WindowPopupProcessCapaData",
    			width:"100%",
    			//visible: false,
    			autoDraw: false,
    			members:
    		    [
    		     
    		    ]
    		})
        ],
        begin: function()
        {
          try
          {
        	  var Height = (this.getHeight() - 28)+"px";
        	  OB.SVFDS.WindoLoadingImg = OB.SVFDS.WindoLoadingImg.replace(/000HEIGHT/g, Height);
        	  var SVFDS_WindoLoadingImgVar = isc.HTMLFlow.create
        	  ({
        	        ID: "SVFDSWindoLoadingImg",
        	        contents: OB.SVFDS.WindoLoadingImg,
        	        width:"100%",
        	        height:"100%"
        	  });
        	  SVFDS_WindowPopupProcessCapaData.removeMembers(SVFDS_WindowPopupProcessCapaData.getMembers());
        	  SVFDS_WindowPopupProcessCapaData.addMember(SVFDS_WindoLoadingImgVar);
           }
           catch(e)
           {
        	  this.hide();
        	  isc.warn("There was an error while it was executing the action");
        	  console.log(e); 
           }
        }
    });
    
    //---------------------------------------------------
    this.me.Dialogo= isc.Dialog.create
    ({
    	 ID:"SVFDS_DropShipDialog",
         width: "auto",
    	 height: "200",
    	 title: "Create Good Receipt document",
    	 showCloseButton:true,
    	 align:"left",
    	 items:
         [
			isc.OBViewForm.create
			({
			   align:"left",	
			   numCols: 2,
			   fields: 
			   [
				  {
					  name: '_MovementDate',
					  title: 'Movement Date',
					  type: '_id_15',
					  required: true,
					  defaultValue: new Date(),
					  keyPress: function (item, form, keyName, characterValue)
					  {
					      
					  }
				   }/*,
			       {
				       name: '_CreateShipment',
				       title: 'Create Shipment',
				       type: '_id_20',
				       defaultValue: true,
				       hidden:true,
				       keyPress: function (item, form, keyName, characterValue)
				       {
				        
				       }
				   }*/
				   
			   ]
			   
			}),
          
            isc.VLayout.create
    		({
    			 height: "100%",
    			 width: "100%",
    			 membersMargin: 2,
    			 defaultLayoutAlign: "center",
    			 members:
    			 [
    			    
    			  
    				/*isc.HTMLFlow.create
    				({
    				   padding:5,
    				   width: "100%",
    				   height: "100%",
    				   contents:"<p><strong><div>Do you want to approve this Order?</div></p>"
    				
                    }),*/
    				isc.HLayout.create
    				({
    					width: "auto",
    					membersMargin: 2,
    					defaultLayoutAlign: "center",
    					members:
    					[
    					  isc.OBFormButton.create
    					  ({
    						  title:"Ok",
    						  click: function ()
    						  {
    							  if (SVFDS_WindowDropShip.Dialogo.items[0].validate()==true)
    							  {
    							    SVFDS_WindowDropShip.GenerateProcess();
    							  }
    						  }
    					  }),

    					  isc.OBFormButton.create
    					  ({
    						  title:"Cancel",
    						  click: function ()
    						  {
    							SVFDS_DropShipDialog.hide();
    						  }
    					  })
    					]
    				})
    			 ]
    		})
     	 ]
    });
    
    //---------------------------------------------------
    
    this.me.MainLayout = isc.VLayout.create
    (
     {
       width:"100%",
       height:"100%",
       membersMargin:4
     }
    );
    
    this.me.MessageLayout = isc.VLayout.create
    (
     {
       width:"100%",
       membersMargin:0
     }
    );
    
    this.me.FormSearchLayout = HLayout.create
    (
      {
        width:"100%",
        membersMargin:4
      }
    );
    
    this.me.FooterLayout = HLayout.create
    (
      {
    	 autoDraw: false,
    	 width: "100%",
    	 //height: "100%",
    	 layoutMargin: 6,
    	 membersMargin: 6,
    	 //border: "1px dashed blue",
    	 align: "center",  // As promised!
      }
    );
    
    //---------------------------------------------------
    
    this.me.MessageBar = isc.OBMessageBar.create
    (
      {
    	  ID: 'SVFDS_MessageBar',
    	  type: 'success',
    	  autoDraw: false,
    	  hiden: true,
      }
    );
    
    //---------------------------------------------------
    
    this.me.FormSearch = isc.OBViewForm.create
    ({
       numCols: 6,
       me: this.me,
       fields: 
       [
           {
    	       ID:'_SVFDS_PurchaseOrder',	
    	       name: '_SVFDS_PurchaseOrder',
    	       title: 'Purchase Order',
    	       width: 250,
    	       type: '_id_19',
    	       editorType: 'OBFKComboItem', 
    	       length: 60,
    	       required: true,
    	       optionDataSource: OB.Datasource.create
    	       (
    	         {
    	        	requestProperties:
    	        	{
    	        		params:
    	        		{
    	        			fieldId: '5E034686FAC642EEAE978C15AEDF0608'
    	        		}
    	        	}	
    	         }
    	       ), 
    		   displayField:"_identifier",
    		   valueField:"id",
    		   columnName: 'C_Order_ID',
    		   targetEntity: 'Order',
    		   refColumnName: 'C_Order_ID',
    	       pickListFields:
    	       [
    	          {name: '_identifier'},
    	       ],
    	       filterFields:["_identifier"],
    	       keyPress: function (item, form, keyName, characterValue)
    	       {
    	         
    	       },
    	       id:'5E034686FAC642EEAE978C15AEDF0608'
    	   }/*,
    	   {
    	       ID: '_SVFDS_Shipper',
    	       name: '_SVFDS_Shipper',
    	       title: 'Shipper',
    	       width: 250,
    	       type: '_id_10',
    	       length: 60,
    	       keyPress: function (item, form, keyName, characterValue)
    	       {
    	        
    	       }
    	   },
    	   {
    	       ID:'_SVFDS_Tracking',	
    	       name: '_SVFDS_Tracking',
    	       title: 'Tracking Number',
    	       width: 250,
    	       type: '_id_10',
    	       length: 60,
    	       keyPress: function (item, form, keyName, characterValue)
    	       {
    	         
    	       }
    	   }*/
       ]
       
    }); 
    
    //-----------------------------------------------------
    
    this.me.actionSearch= function(me)
    {
    	   me.PurchaseGrid.filterData
		   (
			  {
			  	 _constructor: 'AdvancedCriteria',
			  	 criteria:
			   	 [
			         {
			           'fieldName': 'salesOrder.id',
			           'operator': 'iEquals',
			           'value': this.me.FormSearch.getValue('_SVFDS_PurchaseOrder')
			         }
			      ],
			  	  operator: 'and'
			   }
		    );	
       
    };
    
    this.me.BtnSearch = isc.OBFormButton.create
    ({
        title: 'Search',
        margin: 5,
        me: this.me,
        click: function ()
        {
        	if(this.me.FormSearch.validate()==true)
        	{	
        		SVFDS_PurchaseOrderID= this.me.FormSearch.getValue('_SVFDS_PurchaseOrder');
		        
		        if((SVFDS_PurchaseOrderID != null) &&(SVFDS_PurchaseOrderID!=''))
		        {
		          this.me.actionSearch(this.me);	
		        }
        	}    
          	
        },
        initWidget: function ()
        {
          this.Super('initWidget', arguments);
        }
    });
    
    //-------------------------------------------------------
    
    this.me.PurchaseGrid = isc.SVFDS_PurchaseOrderGrid.create
    ({
        selectionUpdated: function(record, recordList)
        {
          
        }
    });
    
    
    //-------------------------------------------------------
    
    this.me.verifyCreateShipment= function(me)
    {
        var ArraySelection= me.PurchaseGrid.getSelection();
      	
    	if(ArraySelection.length > 0)
    	{
    	   var I=0, Fin = ArraySelection.length;
    	   
    	   for(I = 0; I< Fin; I++)
    	   {
	           if((ArraySelection[I].createlinefrom == null) || (ArraySelection[I].createlinefrom.length <= 0))
	           {	   
	        	  return false;
	           }
	       }
           	   
    	}
    	
    	return true;
    };
    
    this.me.verifyTemporaryWarehouse= function(me)
    {
        var ArraySelection= me.PurchaseGrid.getSelection();
      	
    	if(ArraySelection.length > 0)
    	{
    	   var I=0, Fin = ArraySelection.length;
    	   
    	   for(I = 0; I< Fin; I++)
    	   {
	           if((ArraySelection[I].tempwarehouse == null) || (ArraySelection[I].tempwarehouse.length <= 0))
	           {	   
	        	  return false;
	           }
	       }
           	   
    	}
    	
    	return true;
    };
    
    this.me.Generate= function(createShipment)
    {
    	SVFDS_WindowDropShip.Dialogo.hide();
    	var ArraySelection= SVFDS_WindowDropShip.PurchaseGrid.getSelection();
    	
    	if(ArraySelection.length > 0)
    	{
    	   SVFDS_WindowDropShip.WindowPopupProcess.show();
           SVFDS_WindowDropShip.WindowPopupProcess.begin();
           
           var MovementDate= SVFDS_WindowDropShip.Dialogo.items[0].getValue('_MovementDate');
    	   var callback = function (rpcResponse, data, rpcRequest)
           {
    		     try
    			 {
    		    	 console.log(data);
    				 if((data!= null) && (data.result))
    				 {
    				   if(data.result.status=='nook')
    				   {
    					 isc.warn(data.result.msg/*'There was an error while it was executing the action'*/);
    					 console.log(data.result.msg);
    				   }
    				   else
    				   {
    					 SVFDS_PurchaseOrderID='';  
    					 SVFDS_WindowDropShip.PurchaseGrid.deselectAllRecords();
    					 SVFDS_WindowDropShip.FormSearch.clearValues();
    					 SVFDS_WindowDropShip.FormSearch.items[0].fetchData();
    					 SVFDS_WindowDropShip.actionSearch(SVFDS_WindowDropShip);
    					 SVFDS_WindowDropShip.MessageBar.setMessage('success', null, data.result.msg);
    				   }
    				 }
    			 }
    			 catch(e)
    			 {
    			   console.log(e.message);	 
    			 }
    			 SVFDS_WindowDropShip.WindowPopupProcess.hide();	
           }	
    		
    	   OB.RemoteCallManager.call('com.spocsys.vigfurniture.dropship.erpCommon.ad_actionButton.CreateGoodReceiptDocumentAction', 
		   {
		     "Lines": ArraySelection,
		     "MovementDate": MovementDate,
		     "salesOrder": ArraySelection[0].salesOrder,
		     "createShipment": createShipment
		   }, {}, callback);
    	}
    };
    
    this.me.GenerateProcess= function()
    {
    	//var CreateShipment= SVFDS_WindowDropShip.Dialogo.items[0].getValue('_CreateShipment');
    	SVFDS_WindowDropShip.Dialogo.hide();
    	if(this.me.verifyTemporaryWarehouse(this.me)==false)
    	{
          isc.ask("The organization doesn't have temporary warehouse, do you want to create documents with Purchase Order Warehouse?", function(ok){
    	        if (ok) {
    	           	SVFDS_WindowDropShip.Generate("N");
    	        }
    	      });
    	}
        else
        {	
          SVFDS_WindowDropShip.Generate("N");
        }
    	
	    //SVFDS_WindowDropShip = this.me;
		
    };
    
//    this.me.GenerateProcess1= function()
//    {
//    	var CreateShipment= SVFDS_WindowDropShip.Dialogo.items[0].getValue('_CreateShipment');
//    	SVFDS_WindowDropShip.Dialogo.hide();
//    	if(CreateShipment == true)
//        {
//    		if(this.me.verifyCreateShipment(this.me)==false)
//    		{
//    	    	// ask for confirmation
//    	        isc.ask("There are lines without Sales Order associated. The Good Shipment document won't be created, do you want to do this action?", function(ok){
//    	            if (ok) {
//    	            	
//    	            	if(SVFDS_WindowDropShip.verifyTemporaryWarehouse(SVFDS_WindowDropShip)==false)
//    	        		{
//    	            		isc.ask("The organization doesn't have temporary warehouse, do you want to create documents with Purchase Order Warehouse?", function(ok){
//    	        	            if (ok) {
//    	        	            	SVFDS_WindowDropShip.Generate("N");
//    	        	            }
//    	        	        });
//    	        		}
//    	            	else
//    	            	{	
//    	            	  SVFDS_WindowDropShip.Generate("N");
//    	            	}
//    	            }
//    	        });
//    	    }
//    		else
//    	    {
//    			if(this.me.verifyTemporaryWarehouse(this.me)==false)
//        		{
//            		isc.ask("The organization doesn't have temporary warehouse, do you want to create documents with Purchase Order Warehouse?", function(ok){
//        	            if (ok) {
//        	            	SVFDS_WindowDropShip.Generate("Y");
//        	            }
//        	        });
//        		}
//            	else
//            	{	
//            	  SVFDS_WindowDropShip.Generate("Y");
//            	}
//    		}	
//        }
//    	else
//    	{
//    		if(this.me.verifyTemporaryWarehouse(this.me)==false)
//    		{
//        		isc.ask("The organization doesn't have temporary warehouse, do you want to create documents with Purchase Order Warehouse?", function(ok){
//    	            if (ok) {
//    	            	SVFDS_WindowDropShip.Generate("N");
//    	            }
//    	        });
//    		}
//        	else
//        	{	
//        	  SVFDS_WindowDropShip.Generate("N");
//        	}
//    	}	
//    	
//	    //SVFDS_WindowDropShip = this.me;
//		
//    };
    
    this.me.BtnGenerate = isc.OBFormButton.create
    ({
    	me: this.me,
        title: 'Generate',
        click: function ()
        {
          SVFDS_WindowDropShip = this.me;
          var ArraySelection= SVFDS_WindowDropShip.PurchaseGrid.getSelection();
          
          if(ArraySelection.length > 0)
          {	
        	this.me.Dialogo.items[0].resetValues();  
            this.me.Dialogo.show();
          }
          else
          {
        	isc.warn('There are no lines selected');
          }  
        }
    });
    
    //-------------------------------------------------------
    
    this.me.FormSearchLayout.addMember(this.me.FormSearch);
    this.me.FormSearchLayout.addMember(this.me.BtnSearch);
    
    //-------------------------------------------------------
    
    this.me.FooterLayout.addMember(this.me.BtnGenerate);
    
    //-------------------------------------------------------
    this.me.MainLayout.addMember(this.me.MessageBar);
    this.me.MessageBar.hide();
    this.me.MainLayout.addMember(this.me.FormSearchLayout);
    this.me.MainLayout.addMember(this.me.PurchaseGrid);
    this.me.MainLayout.addMember(this.me.FooterLayout);
    this.me.addMember(this.me.MainLayout);
	this.me.Super('initWidget', arguments);
	
  }
});