isc.OBPopup.create
({
    ID: "SVFDS_WindowPopupProcessFinishDroShipProcess",
    title: "Finish Drop Ship Process",
    autoSize:true,
    autoCenter: true,
    showMinimizeButton: false,
    showMaximizeButton: false,
    isModal: true,
    showModalMask: true,
    autoDraw: false,
    height: "30%",
    width:"50%",
    closeClick: function () {this.hide();},
    items:
    [
		
		isc.VStack.create
		({
			ID: "SVFDS_WindowPopupProcessCapaDataFinishDroShipProcess",
			width:"100%",
			//visible: false,
			autoDraw: false,
			members:
		    [
		     
		    ]
		})
    ],
    begin: function()
    {
      try
      {
    	  var Height = (this.getHeight() - 28)+"px";
    	  OB.SVFDS.WindoLoadingImg = OB.SVFDS.WindoLoadingImg.replace(/000HEIGHT/g, Height);
    	  var SVFDS_WindoLoadingImgVar = isc.HTMLFlow.create
    	  ({
    	        ID: "SVFDSWindoLoadingImgFinishDropShipProcess",
    	        contents: OB.SVFDS.WindoLoadingImg,
    	        width:"100%",
    	        height:"100%"
    	  });
    	  SVFDS_WindowPopupProcessCapaDataFinishDroShipProcess.removeMembers(SVFDS_WindowPopupProcessCapaDataFinishDroShipProcess.getMembers());
    	  SVFDS_WindowPopupProcessCapaDataFinishDroShipProcess.addMember(SVFDS_WindoLoadingImgVar);
       }
       catch(e)
       {
    	  this.hide();
    	  isc.warn("There was an error while it was executing the action");
    	  console.log(e); 
       }
    }
});

OB.SVFDS = OB.SVFDS || {};

OB.SVFDS.FinishProcess =
{ 
  View: null,
  grid: null,
  callback: function (rpcResponse, data, rpcRequest)
  {
	 try
	 {
		 if((data!= null) && (data.result))
		 {
		   if(data.result.status==='nook')
		   {
			 isc.warn('There was an error while it was executing the action');
			 console.log(data.result.msg);
		   }
		   else 
		   {
			   if (data.result.status==='ok')
			   {	   
			     OB.SVFDS.FinishProcess.View.messageBar.setMessage('success', null, data.result.msg);
			     console.log(data.result.msg);
			     OB.SVFDS.FinishProcess.View.refresh();
		       }
			   else
			   {
				 if(data.result.status==='war')
				 {	 
				   OB.SVFDS.FinishProcess.View.messageBar.setMessage('warning', null, data.result.msg);
				   console.log(data.result.msg);
				 }  
			   }	   
		   }
		 }
	 }
	 catch(e)
	 {
	   console.log(e.message);	 
	 }
	 SVFDS_WindowPopupProcessFinishDroShipProcess.hide();
	 
  },
  verifyIntercompanyOrder: function()
  {
	  
	var ArraySelection=  OB.SVFDS.FinishProcess.grid.getSelectedRecords();
	var strMsg="";
	
	for(var I=0; I< ArraySelection.length; I++)
	{
		console.log(ArraySelection[I].salesOrder$intercoOrigOrder);
	  if((ArraySelection[I].salesOrder$intercoOrigOrder == null) || (ArraySelection[I].salesOrder$intercoOrigOrder == ""))
	  {	  
		strMsg+= I+1;
	  }	
	}
	
	if(strMsg!= "")
	{
		strMsg = "The Lines Nº " + strMsg + ", Can not be processed, because The documents are not drop ship-intercompany order";
	}	
	
	if(strMsg!= "")
	{
		SVFDS_WindowPopupProcessFinishDroShipProcess.hide();
		OB.SVFDS.FinishProcess.View.messageBar.setMessage('warning', null, strMsg);
		return true;
	}	
	return false;
	
  },
  
  beginProcess: function ()
  {
	  SVFDS_WindowPopupProcessFinishDroShipProcess.show();
	  SVFDS_WindowPopupProcessFinishDroShipProcess.begin();
	  
	  if(OB.SVFDS.FinishProcess.verifyIntercompanyOrder()==false)
	  {	  
	     OB.RemoteCallManager.call('com.spocsys.vigfurniture.dropship.erpCommon.ad_actionButton.FinishDropShipProcess', 
		  {
		     "Lines": OB.SVFDS.FinishProcess.grid.getSelectedRecords(),
		  }, {}, OB.SVFDS.FinishProcess.callback);
	  }	  
  },
  execute: function (params, view)
  {
	try
	{
		OB.SVFDS.FinishProcess.grid= params.button.contextView.viewGrid;
		//---
		isc.ask("Good receipt and Good shipment documents will be created. Do you want to execute this action?", function(ok){
	        if (ok) {
	        	OB.SVFDS.FinishProcess.beginProcess();
	        }
	    }); 
	}
	catch(e)
	{
	  console.log(e.message);	
	}
	  
  },
  FinishDropShipProcess: function (params, view)
  {
	try
	{
	  params.action = 'FinishDropShipProcess';
	  OB.SVFDS.FinishProcess.View= params.button.contextView;	
	  OB.SVFDS.FinishProcess.execute(params, view);
	}
	catch(e)
	{
	  console.log(e.message);	
	}
  }
};
