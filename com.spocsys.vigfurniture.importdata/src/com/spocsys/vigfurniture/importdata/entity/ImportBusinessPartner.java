package com.spocsys.vigfurniture.importdata.entity;

import java.io.FileReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.idl.proc.Parameter;
import org.openbravo.idl.proc.Validator;
import org.openbravo.idl.proc.Value;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Language;
import org.openbravo.model.common.businesspartner.BankAccount;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.geography.Location;
import org.openbravo.model.common.geography.Region;
import org.openbravo.model.common.invoice.InvoiceSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.module.idljava.proc.IdlServiceJava;

import au.com.bytecode.opencsv.CSVReader;

import com.spocsys.vigfurniture.importdata.IDLFormatDefaults;
import com.spocsys.vigfurniture.importdata.utils.CustomValidator;
import com.spocsys.vigfurniture.importdata.utils.IDLFormatUtility;
import com.spocsys.vigfurniture.importdata.utils.Utils;
import com.spocsys.vigfurnitures.services.CreditCard;

public class ImportBusinessPartner extends IdlServiceJava {
  private static Logger log = Logger.getLogger(ImportBusinessPartner.class);
  private int TotalLines;
  private int ActualLine;
  private CustomValidator cValidator;

  IDLFormatDefaults regIDLFormatDefaults = null;
  HashMap<String, Category> HashMapBusinessPartnerCategory = new HashMap<String, Category>();
  HashMap<String, FIN_PaymentMethod> HashMapPaymentMethod = new HashMap<String, FIN_PaymentMethod>();
  HashMap<String, PaymentTerm> HashMapPaymentTerms = new HashMap<String, PaymentTerm>();
  HashMap<String, org.openbravo.model.common.businesspartner.TaxCategory> HashMapTaxCategoryBusinessPartner = new HashMap<String, org.openbravo.model.common.businesspartner.TaxCategory>();

  @Override
  protected boolean executeImport(String filename, boolean insert) throws Exception {

    regIDLFormatDefaults = IDLFormatUtility.getDefaultFormats();

    CSVReader reader = new CSVReader(new FileReader(filename), IDLFormatUtility.getDelimiter(),
        '\"', '\\', 0, false, true);

    List AllLinesList = reader.readAll();
    TotalLines = AllLinesList.size();

    String[] nextLine;

    // Check header
    nextLine = (String[]) AllLinesList.get(0);
    if (nextLine == null) {
      throw new OBException(Utility.messageBD(conn, "IDLJAVA_HEADER_MISSING", vars.getLanguage()));
    }
    Parameter[] parameters = getParameters();
    if (parameters.length > nextLine.length) {
      throw new OBException(
          Utility.messageBD(conn, "IDLJAVA_HEADER_BAD_LENGTH", vars.getLanguage()));
    }

    Validator validator;

    for (int i = 1; i < TotalLines; i++) {

      nextLine = (String[]) AllLinesList.get(i);
      ActualLine = i;

      if (nextLine.length > 1 || nextLine[0].length() > 0) {
        // It is not an empty line
        // Log Progress
        IDLFormatUtility.logProgress(ActualLine, (TotalLines - 1), this);
        // Validate types
        if (parameters.length > nextLine.length) {
          throw new OBException(Utility.messageBD(conn, "IDLJAVA_LINE_BAD_LENGTH",
              vars.getLanguage()));
        }

        validator = getValidator(getEntityName());
        cValidator = new CustomValidator(this, getEntityName());
        Object[] result = validateProcess(validator, nextLine);
        if ("0".equals(cValidator.getErrorCode())) {
          finishRecordProcess(result);
        } else {
          OBDal.getInstance().rollbackAndClose();
          // We need rollback here becouse the intention is load ALL or NOTHING
          logRecordError(cValidator.getErrorMessage(), result);
        }
      }
    }

    return true;
  }

  @Override
  protected String getEntityName() {
    // TODO Auto-generated method stub
    return "Product";
  }

  @Override
  public Parameter[] getParameters() {
    return new Parameter[] { new Parameter("Organization", Parameter.STRING),
        new Parameter("SearchKey", Parameter.STRING), new Parameter("Name", Parameter.STRING),
        new Parameter("Fiscal Name", Parameter.STRING),
        new Parameter("Description", Parameter.STRING), new Parameter("TaxID", Parameter.STRING),
        new Parameter("Tax Exempt", Parameter.STRING),
        new Parameter("ReferenceNo", Parameter.STRING),
        new Parameter("Category", Parameter.STRING), new Parameter("Language", Parameter.STRING),
        new Parameter("Location1stLine", Parameter.STRING),
        new Parameter("Location2ndLine", Parameter.STRING),
        new Parameter("LocationPostalCode", Parameter.STRING),
        new Parameter("LocationCity", Parameter.STRING),
        new Parameter("LocationRegion", Parameter.STRING),
        new Parameter("LocationCountry", Parameter.STRING),
        new Parameter("LocationPhone", Parameter.STRING),
        new Parameter("LocationAlternativePhone", Parameter.STRING),
        new Parameter("LocationFax", Parameter.STRING),
        new Parameter("LocationShipToAddress", Parameter.STRING),
        new Parameter("LocationPayFromAddress", Parameter.STRING),
        new Parameter("LocationInvoiceToAddress", Parameter.STRING),
        new Parameter("LocationRemitToAddress", Parameter.STRING),
        new Parameter("ConsumptionDays", Parameter.STRING),
        new Parameter("Bank AccountShowGeneric", Parameter.STRING),
        new Parameter("BankAccountShowIBAN", Parameter.STRING),
        new Parameter("BankAccountNumber", Parameter.STRING),
        new Parameter("BankAccountIBAN", Parameter.STRING),
        new Parameter("ContactUser", Parameter.STRING),
        new Parameter("ContactFirstName", Parameter.STRING),
        new Parameter("ContactLastName", Parameter.STRING),
        new Parameter("ContactDescription", Parameter.STRING),
        new Parameter("ContactEmail", Parameter.STRING),
        new Parameter("ContactPosition", Parameter.STRING),
        new Parameter("ContactPhone", Parameter.STRING),
        new Parameter("ContactAlternativePhone", Parameter.STRING),
        new Parameter("ContactFax", Parameter.STRING), new Parameter("Vendor", Parameter.STRING),
        new Parameter("VendorPaymentMethod", Parameter.STRING),
        new Parameter("VendorPaymentTerms", Parameter.STRING),
        new Parameter("VendorPurchasePriceList", Parameter.STRING),
        new Parameter("VendorTaxCategory", Parameter.STRING),
        new Parameter("VendorFinancialAccount", Parameter.STRING),
        new Parameter("Customer", Parameter.STRING),
        new Parameter("CustomerPaymentMethod", Parameter.STRING),
        new Parameter("CustomerPaymentTerms", Parameter.STRING),
        new Parameter("CustomerPriceList", Parameter.STRING),
        new Parameter("CustomerTaxCategory", Parameter.STRING),
        new Parameter("CustomerFinancialAccount", Parameter.STRING),
        new Parameter("CustomerInvoiceSchedule", Parameter.STRING),
        new Parameter("CustomerInvoiceTerms", Parameter.STRING),
        new Parameter("Employee", Parameter.STRING),
        new Parameter("EmployeeSalesRepresentative", Parameter.STRING),
        new Parameter("Maximum Discount Level Allowed", Parameter.STRING),
        new Parameter("Business Name", Parameter.STRING),
        new Parameter("Seller's Permit Number", Parameter.STRING),
        new Parameter("Approved Payment Method", Parameter.STRING),
        new Parameter("Approved Discount", Parameter.STRING),
        new Parameter("Outside Sales Person Assigned", Parameter.STRING),
        new Parameter("Approved By Insurance", Parameter.STRING),
        new Parameter("Name On Card", Parameter.STRING),
        new Parameter("Card No", Parameter.STRING), new Parameter("Card Type", Parameter.STRING),
        new Parameter("EXP Date", Parameter.STRING),
        new Parameter("Billing Address", Parameter.STRING),
        new Parameter("Billing Zip Code", Parameter.STRING),
        new Parameter("Date", Parameter.STRING) };
  }

  @Override
  protected Object[] validateProcess(Validator validator, String... values) throws Exception {
    // TODO Auto-generated method stub

    if (values[0] == null) {
      values[0] = "0";
    } else {
      if (values[0].trim().equalsIgnoreCase("*")) {
        values[0] = "0";
      }
    }
    validator.checkOrganization(values[0]);// 0
    validator.checkNotNull(validator.checkString(values[1], 40), "SearchKey");// 1
    validator.checkNotNull(validator.checkString(values[2], 60), "Name");// 2
    validator.checkString(values[3], 60, "Fiscal Name");// 3
    validator.checkString(values[4], 255, "Description");// 4
    validator.checkString(values[5], 20, "TaxID");// 5
    validator.checkNotNull(validator.checkBoolean(values[6]), "Tax Exempt");// 6
    validator.checkString(values[7], 40, "ReferenceNo");// 7
    validator.checkNotNull(validator.checkString(values[8], 40), "Category");// 8
    validator.checkString(values[9], 60, "Language");// 9
    validator.checkString(values[10], 60, "Location1stLine");// 10
    validator.checkString(values[11], 60, "Location2ndLine");// 11
    validator.checkString(values[12], 10, "LocationPostalCode");// 12
    validator.checkString(values[13], 60, "LocationCity");// 13
    validator.checkString(values[14], 40, "LocationRegion");// 14
    validator.checkString(values[15], 60, "LocationCountry");// 15
    validator.checkString(values[16], 40, "LocationPhone");// 16
    validator.checkString(values[17], 40, "LocationAlternativePhone");// 17
    validator.checkString(values[18], 40, "LocationFax");// 18
    validator.checkBoolean(values[19], "LocationShipToAddress");// 19
    validator.checkBoolean(values[20], "LocationPayFromAddress");// 20
    validator.checkBoolean(values[21], "LocationInvoiceToAddress");// 21
    validator.checkBoolean(values[22], "LocationRemitToAddress");// 22
    validator.checkLong(values[23], "ConsumptionDays");// 23
    validator.checkBoolean(values[24], "Bank AccountShowGeneric");// 24
    validator.checkBoolean(values[25], "BankAccountShowIBAN");// 25
    validator.checkString(values[26], 20, "BankAccountNumber");// 26
    validator.checkString(values[27], 34, "BankAccountIBAN");// 27
    validator.checkString(values[28], 60, "ContactUser");// 28
    validator.checkString(values[29], 60, "ContactFirstName");// 29
    validator.checkString(values[30], 60, "ContactLastName");// 30
    validator.checkString(values[31], 255, "ContactDescription");// 31
    validator.checkString(values[32], 40, "ContactEmail");// 32
    validator.checkString(values[33], 40, "ContactPosition");// 33
    validator.checkString(values[34], 40, "ContactPhone");// 34
    validator.checkString(values[35], 40, "ContactAlternativePhone");// 35
    validator.checkString(values[36], 40, "ContactFax");// 36
    validator.checkNotNull(validator.checkBoolean(values[37]), "Vendor");// 37
    validator.checkString(values[38], 60, "VendorPaymentMethod");// 38
    validator.checkString(values[39], 40, "VendorPaymentTerms");// 39
    validator.checkString(values[40], 40, "VendorPurchasePriceList");// 40
    validator.checkString(values[41], 40, "VendorTaxCategory");// 41
    validator.checkString(values[42], 60, "VendorFinancialAccount");// 42
    validator.checkNotNull(validator.checkBoolean(values[43]), "Customer");// 43
    validator.checkString(values[44], 60, "CustomerPaymentMethod");// 44
    validator.checkString(values[45], 60, "CustomerPaymentTerms");// 45
    validator.checkString(values[46], 60, "CustomerPriceList");// 46
    validator.checkString(values[47], 60, "CustomerTaxCategory");// 47
    validator.checkString(values[48], 60, "CustomerFinancialAccount");// 48
    validator.checkString(values[49], 60, "CustomerInvoiceSchedule");// 49
    validator.checkString(values[50], 60, "CustomerInvoiceTerms");// 50
    validator.checkNotNull(validator.checkBoolean(values[51]), "Employee");// 51
    validator.checkBoolean(values[52], "EmployeeSalesRepresentative");// 52
    validator.checkBigDecimal(values[53], "Maximum Discount Level Allowed");// 53
    validator.checkString(values[54], 60, "Business Name");// 54
    validator.checkString(values[55], 60, "Seller's Permit Number");// 55
    validator.checkString(values[56], 60, "Approved Payment Method");// 56
    validator.checkLong(values[57], "Approved Discount");// 57
    validator.checkString(values[58], 40, "Outside Sales Person Assigned");// 58
    validator.checkBoolean(values[59], "Approved By Insurance");// 59
    validator.checkString(values[60], 32, "Name On Card");// 60
    validator.checkString(values[61], 60, "Card No");// 61
    validator.checkString(values[62], 32, "Card Type");// 62
    validator.checkDate(values[63], "EXP Date"); // 63
    validator.checkString(values[64], 32, "Billing Address");// 64
    validator.checkString(values[65], 32, "Billing Zip Code");// 65
    validator.checkDate(values[66], "Date");// 66

    // ------------------------------------------------------
    String strLanguage = values[9];
    String strLocationRegion = values[14];
    String strLocationCountry = values[15];
    boolean boolBankAccountShowGeneric = Utils.isBoolean(values[24]) == true ? Parameter.BOOLEAN
        .parse(values[24]) : false;
    boolean boolBankAccountShowIBAN = Utils.isBoolean(values[25]) == true ? Parameter.BOOLEAN
        .parse(values[25]) : false;
    String strBankAccountNumber = values[26];
    String strBankAccountIBAN = values[27];
    String strContactEmail = values[32];
    String strVendorPurchasePriceList = values[40];
    String strVendorTaxCategory = values[41];
    String strVendorFinancialAccount = values[42];
    String strCustomerPriceList = values[46];
    String strCustomerTaxCategory = values[47];
    String strCustomerFinancialAccount = values[48];
    String strCustomerInvoiceSchedule = values[49];
    String strCustomerInvoiceTerms = values[50];
    String strOutsideSalesPersonAssigned = values[58];
    String strNameOnCard = values[60];
    String strCardNo = values[61];// 61
    String strCardType = values[62];// 62
    String strEXPDate = values[63];
    String strBillingAddress = values[64];
    String strBillingZipCode = values[65];
    String strDate = values[66];
    // ------------------------------------------------------

    Language regLanguage = null;
    Region regLocationRegion = null;
    Country regLocationCountry = null;
    PriceList regVendorPurchasePriceList = null;
    TaxCategory regVendorTaxCategory = null;
    FIN_FinancialAccount regVendorFinancialAccount = null;
    PriceList regCustomerPriceList = null;
    TaxCategory regCustomerTaxCategory = null;
    FIN_FinancialAccount regCustomerFinancialAccount = null;
    InvoiceSchedule regCustomerInvoiceSchedule = null;
    BusinessPartner regOutsideSalesPersonAssigned = null;
    // ------------------------------------------------------

    if ((strLanguage != null) && (strLanguage.trim().equals("") != true)) {
      regLanguage = findDALInstance(false, Language.class, new Value(Language.PROPERTY_NAME,
          strLanguage));

      if (regLanguage == null) {
        cValidator.setErrorMessage("Language doesn't exist:  " + strLanguage);
      }
    }

    if ((strLocationCountry != null) && (strLocationCountry.trim().equals("") != true)) {

      regLocationCountry = findDALInstance(false, Country.class, new Value(Country.PROPERTY_NAME,
          strLocationCountry));

      if (regLocationCountry == null) {
        cValidator.setErrorMessage("LocationCountry doesn't exist:  " + strLocationCountry);
      } else {
        if ((strLocationRegion != null) && (strLocationRegion.trim().equals("") != true)) {
          regLocationRegion = Utils.getRegion(regLocationCountry, strLocationRegion);

          if (regLocationRegion == null) {
            cValidator.setErrorMessage("LocationRegion doesn't exist:  " + strLocationRegion);
          }
        }
      }

    }

    if (boolBankAccountShowGeneric == true) {
      if ((strBankAccountNumber == null) || (strBankAccountNumber.trim().equals("") == true)) {
        cValidator.setErrorMessage("BankAccountNumber needed in a Bank Account");
      }
    }

    if (boolBankAccountShowIBAN == true) {
      if ((strBankAccountIBAN == null) || (strBankAccountIBAN.trim().equals("") == true)) {
        cValidator.setErrorMessage("BankAccountIBAN needed in a Bank Account");
      }
      if (regLocationCountry == null) {
        cValidator.setErrorMessage("Country needed in an IBAN account.");
      }
    }

    if ((strContactEmail != null) && (strContactEmail.trim().equals("") != true)) {

      if (Utils.isEmail(strContactEmail) == false) {
        cValidator.setErrorMessage("ContactEmail is invalid: " + strContactEmail);
      }

    }

    if ((strVendorPurchasePriceList != null)
        && (strVendorPurchasePriceList.trim().equals("") != true)) {

      regVendorPurchasePriceList = findDALInstance(false, PriceList.class, new Value(
          PriceList.PROPERTY_NAME, strVendorPurchasePriceList));

      if (regVendorPurchasePriceList == null) {
        cValidator.setErrorMessage("VendorPurchasePriceList doesn't exist: "
            + strVendorPurchasePriceList);
      }

    }

    // if ((strVendorTaxCategory != null) && (strVendorTaxCategory.trim().equals("") != true)) {
    //
    // regVendorTaxCategory = findDALInstance(false, TaxCategory.class, new Value(
    // TaxCategory.PROPERTY_NAME, strVendorTaxCategory));
    //
    // if (regVendorTaxCategory == null) {
    // cValidator.setErrorMessage("VendorTaxCategory doesn't exist: " + strVendorTaxCategory);
    // }
    //
    // }

    if ((strVendorFinancialAccount != null)
        && (strVendorFinancialAccount.trim().equals("") != true)) {

      regVendorFinancialAccount = findDALInstance(false, FIN_FinancialAccount.class, new Value(
          FIN_FinancialAccount.PROPERTY_NAME, strVendorFinancialAccount));

      if (regVendorFinancialAccount == null) {
        cValidator.setErrorMessage("VendorFinancialAccount doesn't exist: "
            + strVendorFinancialAccount);
      }

    }

    if ((strCustomerPriceList != null) && (strCustomerPriceList.trim().equals("") != true)) {

      regCustomerPriceList = findDALInstance(false, PriceList.class, new Value(
          PriceList.PROPERTY_NAME, strCustomerPriceList));

      if (regCustomerPriceList == null) {
        cValidator.setErrorMessage("CustomerPriceList doesn't exist: " + strCustomerPriceList);
      }

    }

    // if ((strCustomerTaxCategory != null) && (strCustomerTaxCategory.trim().equals("") != true)) {
    //
    // regCustomerTaxCategory = findDALInstance(false, TaxCategory.class, new Value(
    // PriceList.PROPERTY_NAME, strCustomerTaxCategory));
    //
    // if (regCustomerTaxCategory == null) {
    // cValidator.setErrorMessage("CustomerTaxCategory doesn't exist: " + strCustomerTaxCategory);
    // }
    //
    // }

    if ((strCustomerFinancialAccount != null)
        && (strCustomerFinancialAccount.trim().equals("") != true)) {

      regCustomerFinancialAccount = findDALInstance(false, FIN_FinancialAccount.class, new Value(
          FIN_FinancialAccount.PROPERTY_NAME, strCustomerFinancialAccount));

      if (regCustomerFinancialAccount == null) {

        cValidator.setErrorMessage("CustomerFinancialAccount doesn't exist: "
            + strCustomerFinancialAccount);
      }

    }

    if ((strCustomerInvoiceSchedule != null)
        && (strCustomerInvoiceSchedule.trim().equals("") != true)) {

      regCustomerInvoiceSchedule = findDALInstance(false, InvoiceSchedule.class, new Value(
          InvoiceSchedule.PROPERTY_NAME, strCustomerInvoiceSchedule));

      if (regCustomerInvoiceSchedule == null) {
        cValidator.setErrorMessage("CustomerInvoiceSchedule doesn't exist: "
            + strCustomerInvoiceSchedule);
      }

    }

    if ((strCustomerInvoiceTerms != null) && (strCustomerInvoiceTerms.trim().equals("") != true)) {

      if (Utils.getInvoiceTerms(strCustomerInvoiceTerms) == null) {
        cValidator
            .setErrorMessage("CustomerInvoiceTerms doesn't exist: " + strCustomerInvoiceTerms);
      }

    }

    if ((strOutsideSalesPersonAssigned != null)
        && (strOutsideSalesPersonAssigned.trim().equals("") != true)) {

      regOutsideSalesPersonAssigned = findDALInstance(false, BusinessPartner.class, new Value(
          BusinessPartner.PROPERTY_SEARCHKEY, strOutsideSalesPersonAssigned));

      if (regOutsideSalesPersonAssigned == null) {
        cValidator.setErrorMessage("Outside Sales Person Assigned doesn't exist: "
            + strOutsideSalesPersonAssigned);
      }

    }

    if ((strNameOnCard != null) && (strNameOnCard.trim().equals("") != true)) {

      if ((strCardNo == null) || (strCardNo.trim().equals("") == true)) {
        cValidator.setErrorMessage("CardNo needed in Credit Cards");
      }

      if ((strCardType == null) || (strCardType.trim().equals("") == true)) {
        cValidator.setErrorMessage("CardType needed in Credit Cards");
      }

      if ((strEXPDate == null) || (strEXPDate.trim().equals("") == true)) {
        cValidator.setErrorMessage("EXPDate needed in Credit Cards");
      }

      if ((strBillingAddress == null) || (strBillingAddress.trim().equals("") == true)) {
        cValidator.setErrorMessage("BillingAddress needed in Credit Cards");
      }

      if ((strBillingZipCode == null) || (strBillingZipCode.trim().equals("") == true)) {
        cValidator.setErrorMessage("BillingZipCode needed in Credit Cards");
      }

      if ((strDate == null) || (strDate.trim().equals("") == true)) {
        cValidator.setErrorMessage("Date needed in Credit Cards");
      }

    }

    return values;
  }

  @Override
  protected BaseOBObject internalProcess(Object... values) throws Exception {
    // TODO Auto-generated method stub
    return create((String) values[0], (String) values[1], (String) values[2], (String) values[3],
        (String) values[4], (String) values[5], (String) values[6], (String) values[7],
        (String) values[8], (String) values[9], (String) values[10], (String) values[11],
        (String) values[12], (String) values[13], (String) values[14], (String) values[15],
        (String) values[16], (String) values[17], (String) values[18], (String) values[19],
        (String) values[20], (String) values[21], (String) values[22], (String) values[23],
        (String) values[24], (String) values[25], (String) values[26], (String) values[27],
        (String) values[28], (String) values[29], (String) values[30], (String) values[31],
        (String) values[32], (String) values[33], (String) values[34], (String) values[35],
        (String) values[36], (String) values[37], (String) values[38], (String) values[39],
        (String) values[40], (String) values[41], (String) values[42], (String) values[43],
        (String) values[44], (String) values[45], (String) values[46], (String) values[47],
        (String) values[48], (String) values[49], (String) values[50], (String) values[51],
        (String) values[52], (String) values[53], (String) values[54], (String) values[55],
        (String) values[56], (String) values[57], (String) values[58], (String) values[59],
        (String) values[60], (String) values[61], (String) values[62], (String) values[63],
        (String) values[64], (String) values[65], (String) values[66]);

  }

  public BaseOBObject create(final String strOrganization, final String strSearchKey,
      final String strName, final String strFiscalName, final String strDescription,
      final String strTaxID, final String strTaxExempt, final String strReferenceNo,
      final String strCategory, final String strLanguage, final String strLocation1stLine,
      final String strLocation2ndLine, final String strLocationPostalCode,
      final String strLocationCity, final String strLocationRegion,
      final String strLocationCountry, final String strLocationPhone,
      final String strLocationAlternativePhone, final String strLocationFax,
      final String strLocationShipToAddress, final String strLocationPayFromAddress,
      final String strLocationInvoiceToAddress, final String strLocationRemitToAddress,
      final String strConsumptionDays, final String strBankAccountShowGeneric,
      final String strBankAccountShowIBAN, final String strBankAccountNumber,
      final String strBankAccountIBAN, final String strContactUser,
      final String strContactFirstName, final String strContactLastName,
      final String strContactDescription, final String strContactEmail,
      final String strContactPosition, final String strContactPhone,
      final String strContactAlternativePhone, final String strContactFax, final String strVendor,
      final String strVendorPaymentMethod, final String strVendorPaymentTerms,
      final String strVendorPurchasePriceList, final String strVendorTaxCategory,
      final String strVendorFinancialAccount, final String strCustomer,
      final String strCustomerPaymentMethod, final String strCustomerPaymentTerms,
      final String strCustomerPriceList, final String strCustomerTaxCategory,
      final String strCustomerFinancialAccount, final String strCustomerInvoiceSchedule,
      final String strCustomerInvoiceTerms, final String strEmployee,
      final String strEmployeeSalesRepresentative, final String strMaximumDiscountLevelAllowed,
      final String strBusinessName, final String strSellersPermitNumber,
      final String strApprovedPaymentMethod, final String strApprovedDiscount,
      final String strOutsideSalesPersonAssigned, final String strApprovedByInsurance,
      final String strNameOnCard, final String strCardNo, final String strCardType,
      final String strEXPDate, final String strBillingAddress, final String strBillingZipCode,
      final String strDate) throws Exception {

    HashMapBusinessPartnerCategory = new HashMap<String, Category>();
    HashMapPaymentMethod = new HashMap<String, FIN_PaymentMethod>();
    HashMapPaymentTerms = new HashMap<String, PaymentTerm>();
    HashMapTaxCategoryBusinessPartner = new HashMap<String, org.openbravo.model.common.businesspartner.TaxCategory>();

    boolean Exist = true;

    Organization regOrganization = Utils.getOrgBySearchKey(strOrganization);

    BusinessPartner regBusinessPartner = findDALInstance(false, BusinessPartner.class, new Value(
        BusinessPartner.PROPERTY_SEARCHKEY, strSearchKey));

    Language regLanguage = findDALInstance(false, Language.class, new Value(Language.PROPERTY_NAME,
        strLanguage));

    PriceList regVendorPurchasePriceList = findDALInstance(false, PriceList.class, new Value(
        PriceList.PROPERTY_NAME, strVendorPurchasePriceList));

    FIN_FinancialAccount regVendorFinancialAccount = findDALInstance(false,
        FIN_FinancialAccount.class, new Value(FIN_FinancialAccount.PROPERTY_NAME,
            strVendorFinancialAccount));

    PriceList regCustomerPriceList = findDALInstance(false, PriceList.class, new Value(
        PriceList.PROPERTY_NAME, strCustomerPriceList));

    FIN_FinancialAccount regCustomerFinancialAccount = findDALInstance(false,
        FIN_FinancialAccount.class, new Value(FIN_FinancialAccount.PROPERTY_NAME,
            strCustomerFinancialAccount));

    InvoiceSchedule regCustomerInvoiceSchedule = findDALInstance(false, InvoiceSchedule.class,
        new Value(InvoiceSchedule.PROPERTY_NAME, strCustomerInvoiceSchedule));

    BusinessPartner regOutsideSalesPersonAssigned = findDALInstance(false, BusinessPartner.class,
        new Value(BusinessPartner.PROPERTY_SEARCHKEY, strOutsideSalesPersonAssigned));

    if (regBusinessPartner == null) {
      Exist = false;
      regBusinessPartner = OBProvider.getInstance().get(BusinessPartner.class);
      regBusinessPartner.setClient(OBContext.getOBContext().getCurrentClient());
      regBusinessPartner.setOrganization(regOrganization);
      regBusinessPartner.setCreatedBy(OBContext.getOBContext().getUser());
      regBusinessPartner.setUpdatedBy(OBContext.getOBContext().getUser());
      regBusinessPartner.setSearchKey(strSearchKey);
    }
    regBusinessPartner.setName(strName);
    regBusinessPartner.setName2(strFiscalName);
    regBusinessPartner.setDescription(strDescription);
    regBusinessPartner.setTaxID(strTaxID);
    regBusinessPartner.setTaxExempt(Utils.isBoolean(strTaxExempt) == true ? Parameter.BOOLEAN
        .parse(strTaxExempt) : false);
    regBusinessPartner.setReferenceNo(strReferenceNo);
    regBusinessPartner.setBusinessPartnerCategory(createBusinesPartnerCategory(regOrganization,
        strCategory));
    regBusinessPartner.setLanguage(regLanguage);
    regBusinessPartner
        .setConsumptionDays(Utils.parseBigDecimal(strConsumptionDays) == null ? new BigDecimal(
            "1000").longValue() : Utils.parseBigDecimal(strConsumptionDays).longValue());

    OBDal.getInstance().save(regBusinessPartner);
    OBDal.getInstance().flush();

    // --

    if ((strLocation1stLine != null) && (strLocation1stLine.trim().equals("") != true)) {

      createLocationBusinessPartner(Exist, regBusinessPartner, regOrganization, strLocation1stLine,
          strLocation2ndLine, strLocationPostalCode, strLocationCity, strLocationRegion,
          strLocationCountry, strLocationPhone, strLocationAlternativePhone, strLocationFax,
          strLocationShipToAddress, strLocationPayFromAddress, strLocationInvoiceToAddress,
          strLocationRemitToAddress);
    }

    // --

    User regContactUser = null;

    if ((strContactFirstName != null) && (strContactFirstName.equals("") != true)) {
      regContactUser = createContactUser(Exist, regOrganization, regBusinessPartner,
          strContactFirstName, strContactLastName, strContactDescription, strContactEmail,
          strContactPosition, strContactPhone, strContactAlternativePhone, strContactFax);
    }
    // ---

    if ((strBankAccountNumber != null) && (strBankAccountNumber.equals("") != true)) {

      createBankAccount(Exist, regOrganization, regBusinessPartner, strBankAccountShowGeneric,
          strBankAccountShowIBAN, strBankAccountNumber, strBankAccountIBAN, regContactUser,
          strLocationCountry);
    }

    // --

    if (Utils.isBoolean(strVendor) == true) {
    regBusinessPartner.setVendor(Parameter.BOOLEAN.parse(strVendor));
    }

    regBusinessPartner.setPOPaymentMethod(createPaymentMethod(regOrganization,
        strVendorPaymentMethod));
    regBusinessPartner
        .setPOPaymentTerms(createPaymentTerms(regOrganization, strVendorPaymentTerms));
    regBusinessPartner.setPurchasePricelist(regVendorPurchasePriceList);
    regBusinessPartner.setPOFinancialAccount(regVendorFinancialAccount);
    regBusinessPartner.setTaxCategory(createTaxCategoryBusinessPartner(regOrganization,
        strVendorTaxCategory));
    regBusinessPartner.setPurchasePricelist(regVendorPurchasePriceList);

    if (Utils.isBoolean(strCustomer) == true) {
      regBusinessPartner.setCustomer(Parameter.BOOLEAN.parse(strCustomer));
    }
    regBusinessPartner.setPaymentMethod(createPaymentMethod(regOrganization,
        strCustomerPaymentMethod));
    regBusinessPartner
        .setPaymentTerms(createPaymentTerms(regOrganization, strCustomerPaymentTerms));
    regBusinessPartner.setPriceList(regCustomerPriceList);
    regBusinessPartner.setSOBPTaxCategory(createTaxCategoryBusinessPartner(regOrganization,
        strCustomerTaxCategory));
    regBusinessPartner.setAccount(regCustomerFinancialAccount);
    regBusinessPartner.setInvoiceSchedule(regCustomerInvoiceSchedule);
    regBusinessPartner.setInvoiceTerms(Utils.getInvoiceTerms(strCustomerInvoiceTerms));

    if (Utils.isBoolean(strEmployee) == true) {
      regBusinessPartner.setEmployee(Parameter.BOOLEAN.parse(strEmployee));
    }

    if (Utils.isBoolean(strEmployeeSalesRepresentative) == true) {
      regBusinessPartner.setSalesRepresentative(Parameter.BOOLEAN
          .parse(strEmployeeSalesRepresentative));
    }

    if (Utils.parseBigDecimal(strMaximumDiscountLevelAllowed) != null) {
      regBusinessPartner.setSvfsvMaximumDiscountLevelAllowed(Utils
          .parseBigDecimal(strMaximumDiscountLevelAllowed));
    }

    regBusinessPartner.setSvfsvBusinessName(strBusinessName);
    regBusinessPartner.setSvfsvSellerPermitNumber(strSellersPermitNumber);
    regBusinessPartner.setSvfsvApdPaymentMethod(strApprovedPaymentMethod);

    if (Utils.parseBigDecimal(strApprovedDiscount) != null) {
      regBusinessPartner
          .setSvfsvApdDiscount(Utils.parseBigDecimal(strApprovedDiscount).longValue());
    }

    regBusinessPartner.setSvfsvBpartner(regOutsideSalesPersonAssigned);

    if (Utils.isBoolean(strApprovedByInsurance) == true) {
      regBusinessPartner.setSvfsvApdByInsurance(Parameter.BOOLEAN.parse(strApprovedByInsurance));
    }

    if ((strNameOnCard != null) && (strNameOnCard.trim().equals("") != true) && (strCardNo != null)
        && (strCardNo.trim().equals("") != true) && (strCardType != null)
        && (strCardType.trim().equals("") != true) && (strEXPDate != null)
        && (strEXPDate.trim().equals("") != true) && (strBillingAddress != null)
        && (strBillingAddress.trim().equals("") != true) && (strBillingZipCode != null)
        && (strBillingZipCode.trim().equals("") != true) && (strDate != null)
        && (strDate.trim().equals("") != true)) {
      createCreditCard(Exist, regOrganization, regBusinessPartner, strNameOnCard, strCardNo,
          strCardType, strEXPDate, strBillingAddress, strBillingZipCode, strDate);
    }

    OBDal.getInstance().save(regBusinessPartner);
    OBDal.getInstance().flush();
    return regBusinessPartner;

  }

  Category createBusinesPartnerCategory(Organization regOrganization, String Value) {

    Category regBusinessPartnerCategory = findDALInstance(false, Category.class, new Value(
        Category.PROPERTY_SEARCHKEY, Value));

    if (regBusinessPartnerCategory == null) {
      if (HashMapBusinessPartnerCategory.containsKey(Value) == true) {
        return HashMapBusinessPartnerCategory.get(Value);
      } else {
        regBusinessPartnerCategory = OBProvider.getInstance().get(Category.class);
        regBusinessPartnerCategory.setClient(OBContext.getOBContext().getCurrentClient());
        regBusinessPartnerCategory.setOrganization(regOrganization);
        regBusinessPartnerCategory.setCreatedBy(OBContext.getOBContext().getUser());
        regBusinessPartnerCategory.setUpdatedBy(OBContext.getOBContext().getUser());
        regBusinessPartnerCategory.setSearchKey(Value);
        regBusinessPartnerCategory.setName(Value);
        regBusinessPartnerCategory.setDescription("Category created from Business Partner IDL");

        OBDal.getInstance().save(regBusinessPartnerCategory);
        OBDal.getInstance().flush();

        HashMapBusinessPartnerCategory.put(Value, regBusinessPartnerCategory);

        return regBusinessPartnerCategory;
      }
    } else {
      return regBusinessPartnerCategory;
    }

  }

  Location createLocation(
      org.openbravo.model.common.businesspartner.Location regBusinessPartnerLocation,
      Organization regOrganization, String strLocation1stLine, String strLocation2ndLine,
      String strLocationPostalCode, String strLocationCity, String strLocationRegion,
      String strLocationCountry) {

    Location regLocation = null;
    if (regBusinessPartnerLocation == null) {

      regLocation = OBProvider.getInstance().get(Location.class);
      regLocation.setClient(OBContext.getOBContext().getCurrentClient());
      regLocation.setOrganization(regOrganization);
      regLocation.setCreatedBy(OBContext.getOBContext().getUser());
      regLocation.setUpdatedBy(OBContext.getOBContext().getUser());

    } else {
      regLocation = regBusinessPartnerLocation.getLocationAddress();
    }

    regLocation.setAddressLine1(strLocation1stLine);
    regLocation.setAddressLine2(strLocation2ndLine);
    regLocation.setPostalCode(strLocationPostalCode);

    Country regCountry = findDALInstance(false, Country.class, new Value(Country.PROPERTY_NAME,
        (strLocationCountry == null) ? "United States" : strLocationCountry));

    if (regCountry == null) {
      regCountry = findDALInstance(false, Country.class, new Value(Country.PROPERTY_NAME,
          "United States"));
    }

    if (regCountry != null) {
      regLocation.setCountry(regCountry);
      Region regRegion = Utils.getRegion(regCountry, strLocationRegion);
      regLocation.setRegion(regRegion);
    }
    regLocation.setCityName(strLocationCity);
    OBDal.getInstance().save(regLocation);
    OBDal.getInstance().flush();
    return regLocation;

  }

  org.openbravo.model.common.businesspartner.Location createLocationBusinessPartner(boolean exist,
      BusinessPartner regBusinesPartner, Organization regOrganization, String strLocation1stLine,
      String strLocation2ndLine, String strLocationPostalCode, String strLocationCity,
      String strLocationRegion, String strLocationCountry, String strLocationPhone,
      String strLocationAlternativePhone, String strLocationFax, String strLocationShipToAddress,
      String strLocationPayFromAddress, String strLocationInvoiceToAddress,
      String strLocationRemitToAddress) {
    try {
      org.openbravo.model.common.businesspartner.Location regLocationBusinessPartner = null;

      String strNAME = "..,";

      if ((strLocation1stLine != null) && (strLocation1stLine.trim().equals("") != true)) {
        strNAME = strLocation1stLine;
      } else {
        if ((strLocation2ndLine != null) && (strLocation2ndLine.trim().equals("") != true)) {
          strNAME = strLocation2ndLine;
        } else {
          if ((strLocationCity != null) && (strLocationCity.trim().equals("") != true)) {
            strNAME = strLocationCity;
          }
        }
      }

      if (exist == true) {
        regLocationBusinessPartner = Utils.getBusinnesParnerLocation(regBusinesPartner, strNAME);
      }

      Location regLocation = createLocation(regLocationBusinessPartner, regOrganization,
          strLocation1stLine, strLocation2ndLine, strLocationPostalCode, strLocationCity,
          strLocationRegion, strLocationCountry);

      if (regLocationBusinessPartner == null) {

        regLocationBusinessPartner = OBProvider.getInstance().get(
            org.openbravo.model.common.businesspartner.Location.class);
        regLocationBusinessPartner.setClient(OBContext.getOBContext().getCurrentClient());
        regLocationBusinessPartner.setOrganization(regOrganization);
        regLocationBusinessPartner.setCreatedBy(OBContext.getOBContext().getUser());
        regLocationBusinessPartner.setUpdatedBy(OBContext.getOBContext().getUser());
        regLocationBusinessPartner.setName(strNAME);
      }
      regLocationBusinessPartner.setPhone(strLocationPhone);
      regLocationBusinessPartner.setAlternativePhone(strLocationAlternativePhone);
      regLocationBusinessPartner.setFax(strLocationFax);

      if (Utils.isBoolean(strLocationShipToAddress) == true) {
        regLocationBusinessPartner.setShipToAddress(Parameter.BOOLEAN
            .parse(strLocationShipToAddress));
      }

      if (Utils.isBoolean(strLocationPayFromAddress) == true) {
        regLocationBusinessPartner.setPayFromAddress(Parameter.BOOLEAN
            .parse(strLocationPayFromAddress));
      }

      if (Utils.isBoolean(strLocationInvoiceToAddress) == true) {
        regLocationBusinessPartner.setInvoiceToAddress(Parameter.BOOLEAN
            .parse(strLocationInvoiceToAddress));
      }

      if (Utils.isBoolean(strLocationRemitToAddress) == true) {
        regLocationBusinessPartner.setRemitToAddress(Parameter.BOOLEAN
            .parse(strLocationRemitToAddress));
      }

      regLocationBusinessPartner.setLocationAddress(regLocation);
      regLocationBusinessPartner.setBusinessPartner(regBusinesPartner);
      OBDal.getInstance().save(regLocationBusinessPartner);
      OBDal.getInstance().flush();

    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;

  }

  BankAccount createBankAccount(boolean exist, Organization regOrganization,
      BusinessPartner regBusinessPartner, String strBankAccountShowGeneric,
      final String strBankAccountShowIBAN, final String strBankAccountNumber,
      final String strBankAccountIBAN, User regContactUser, String strLocationCountry) {

    try {

      BankAccount regBankAccount = null;

      if (exist == true) {
        regBankAccount = Utils.getBankAccountBusinessPartner(regBusinessPartner,
            strBankAccountNumber);
      }

      if (regBankAccount == null) {
        regBankAccount = OBProvider.getInstance().get(BankAccount.class);
        regBankAccount.setClient(OBContext.getOBContext().getCurrentClient());
        regBankAccount.setOrganization(regOrganization);
        regBankAccount.setCreatedBy(OBContext.getOBContext().getUser());
        regBankAccount.setUpdatedBy(OBContext.getOBContext().getUser());
      }

      if (Utils.isBoolean(strBankAccountShowGeneric) == true) {

        if (Parameter.BOOLEAN.parse(strBankAccountShowGeneric) == true) {
          regBankAccount.setBankFormat(Utils.getBankFormat("Use Generic Account No."));
        }

      }

      if (Utils.isBoolean(strBankAccountShowGeneric) == true) {

        if (Parameter.BOOLEAN.parse(strBankAccountShowIBAN) == true) {
          regBankAccount.setBankFormat(Utils.getBankFormat("Use IBAN"));
          regBankAccount.setIBAN(strBankAccountIBAN);

        }

      }

      regBankAccount.setAccountNo(strBankAccountNumber);
      regBankAccount.setBusinessPartner(regBusinessPartner);
      regBankAccount.setUserContact(regContactUser);

      Country regLocationCountry = findDALInstance(false, Country.class, new Value(
          Country.PROPERTY_NAME, strLocationCountry));

      if (regLocationCountry == null) {
        regLocationCountry = findDALInstance(false, Country.class, new Value(Country.PROPERTY_NAME,
            "United States"));
      }
      regBankAccount.setCountry(regLocationCountry);

      OBDal.getInstance().save(regBankAccount);
      OBDal.getInstance().flush();
      return regBankAccount;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  User createContactUser(boolean exist, Organization regOrganization,
      BusinessPartner regBusinessPartner, String strContactFirstName, String strContactLastName,
      String strContactDescription, String strContactEmail, String strContactPosition,
      String strContactPhone, String strContactAlternativePhone, String strContactFax) {

    User regContactUser = null;
    if (exist == true) {
      regContactUser = Utils.getContactUser(regBusinessPartner, strContactFirstName);
    }

    if (regContactUser == null) {
      regContactUser = OBProvider.getInstance().get(User.class);
      regContactUser.setClient(OBContext.getOBContext().getCurrentClient());
      regContactUser.setOrganization(regOrganization);
      regContactUser.setCreatedBy(OBContext.getOBContext().getUser());
      regContactUser.setUpdatedBy(OBContext.getOBContext().getUser());
      regContactUser.setName(strContactFirstName);
      regContactUser.setFirstName(strContactFirstName);
    }

    regContactUser.setLastName(strContactLastName);
    regContactUser.setDescription(strContactDescription);
    regContactUser.setEmail(strContactEmail);
    regContactUser.setPosition(strContactPosition);
    regContactUser.setPhone(strContactPhone);
    regContactUser.setAlternativePhone(strContactAlternativePhone);
    regContactUser.setFax(strContactFax);
    regContactUser.setBusinessPartner(regBusinessPartner);
    OBDal.getInstance().save(regContactUser);
    OBDal.getInstance().flush();

    return regContactUser;

  }

  FIN_PaymentMethod createPaymentMethod(Organization regOrganization, String strPaymentMethod) {

    FIN_PaymentMethod regPaymentMethod = findDALInstance(false, FIN_PaymentMethod.class, new Value(
        FIN_PaymentMethod.PROPERTY_NAME, strPaymentMethod));

    if (regPaymentMethod == null) {

      if (HashMapPaymentMethod.containsKey(regPaymentMethod) == true) {
        return HashMapPaymentMethod.get(regPaymentMethod);
      } else {

        regPaymentMethod = OBProvider.getInstance().get(FIN_PaymentMethod.class);
        regPaymentMethod.setClient(OBContext.getOBContext().getCurrentClient());
        regPaymentMethod.setOrganization(regOrganization);
        regPaymentMethod.setCreatedBy(OBContext.getOBContext().getUser());
        regPaymentMethod.setUpdatedBy(OBContext.getOBContext().getUser());
        regPaymentMethod.setName(strPaymentMethod);
        regPaymentMethod.setDescription("Payment Method created from Business Partner IDL");
        OBDal.getInstance().save(regPaymentMethod);
        OBDal.getInstance().flush();
        HashMapPaymentMethod.put(strPaymentMethod, regPaymentMethod);

        return regPaymentMethod;
      }
    } else {
      return regPaymentMethod;
    }

  }

  PaymentTerm createPaymentTerms(Organization regOrganization, String strPaymentTerm) {

    PaymentTerm regPaymentTerm = findDALInstance(false, PaymentTerm.class, new Value(
        PaymentTerm.PROPERTY_SEARCHKEY, strPaymentTerm));

    if (regPaymentTerm == null) {

      if (HashMapPaymentTerms.containsKey(regPaymentTerm) == true) {
        return HashMapPaymentTerms.get(regPaymentTerm);
      } else {

        regPaymentTerm = OBProvider.getInstance().get(PaymentTerm.class);
        regPaymentTerm.setClient(OBContext.getOBContext().getCurrentClient());
        regPaymentTerm.setOrganization(regOrganization);
        regPaymentTerm.setCreatedBy(OBContext.getOBContext().getUser());
        regPaymentTerm.setUpdatedBy(OBContext.getOBContext().getUser());
        regPaymentTerm.setSearchKey(strPaymentTerm);
        regPaymentTerm.setName(strPaymentTerm);
        regPaymentTerm.setOverduePaymentDaysRule(BigDecimal.ZERO.longValue());
        regPaymentTerm.setOffsetMonthDue(BigDecimal.ZERO.longValue());
        regPaymentTerm.setDescription("Payment Method created from Business Partner IDL");
        OBDal.getInstance().save(regPaymentTerm);
        OBDal.getInstance().flush();
        HashMapPaymentTerms.put(strPaymentTerm, regPaymentTerm);
        return regPaymentTerm;
      }
    } else {
      return regPaymentTerm;
    }

  }

  org.openbravo.model.common.businesspartner.TaxCategory createTaxCategoryBusinessPartner(
      Organization regOrganization, String strTaxCategory) {

    org.openbravo.model.common.businesspartner.TaxCategory regTaxCategoryBusinessPartner = findDALInstance(
        false, org.openbravo.model.common.businesspartner.TaxCategory.class, new Value(
            org.openbravo.model.common.businesspartner.TaxCategory.PROPERTY_NAME, strTaxCategory));

    if (regTaxCategoryBusinessPartner == null) {
      if (HashMapTaxCategoryBusinessPartner.containsKey(regTaxCategoryBusinessPartner) == true) {
        return HashMapTaxCategoryBusinessPartner.get(strTaxCategory);
      } else {
        regTaxCategoryBusinessPartner = OBProvider.getInstance().get(
            org.openbravo.model.common.businesspartner.TaxCategory.class);
        regTaxCategoryBusinessPartner.setClient(OBContext.getOBContext().getCurrentClient());
        regTaxCategoryBusinessPartner.setOrganization(regOrganization);
        regTaxCategoryBusinessPartner.setCreatedBy(OBContext.getOBContext().getUser());
        regTaxCategoryBusinessPartner.setUpdatedBy(OBContext.getOBContext().getUser());
        regTaxCategoryBusinessPartner.setName(strTaxCategory);
        regTaxCategoryBusinessPartner
            .setDescription("Payment Method created from Business Partner IDL");
        OBDal.getInstance().save(regTaxCategoryBusinessPartner);
        OBDal.getInstance().flush();
        HashMapTaxCategoryBusinessPartner.put(strTaxCategory, regTaxCategoryBusinessPartner);
        return regTaxCategoryBusinessPartner;
      }
    } else {
      return regTaxCategoryBusinessPartner;
    }

  }

  CreditCard createCreditCard(boolean exist, Organization regOrganization,
      BusinessPartner regBusinessPartner, String strNameOnCard, String strCardNo,
      String strCardType, String strEXPDate, String strBillingAddress, String strBillingZipCode,
      String strDate) throws Exception {

    CreditCard regCreditCard = null;

    if (exist == true) {
      regCreditCard = Utils.getCreditCard(regBusinessPartner, strCardNo);

      if (regCreditCard == null) {
        regCreditCard = OBProvider.getInstance().get(CreditCard.class);
        regCreditCard.setClient(OBContext.getOBContext().getCurrentClient());
        regCreditCard.setOrganization(regOrganization);
        regCreditCard.setCreatedBy(OBContext.getOBContext().getUser());
        regCreditCard.setUpdatedBy(OBContext.getOBContext().getUser());
        regCreditCard.setCreditCardNo(strCardNo);
      }

    } else {
      regCreditCard = OBProvider.getInstance().get(CreditCard.class);
      regCreditCard.setClient(OBContext.getOBContext().getCurrentClient());
      regCreditCard.setOrganization(regOrganization);
      regCreditCard.setCreatedBy(OBContext.getOBContext().getUser());
      regCreditCard.setUpdatedBy(OBContext.getOBContext().getUser());
      regCreditCard.setCreditCardNo(strCardNo);
    }

    regCreditCard.setNameOnCard(strNameOnCard);
    regCreditCard.setCreditCardType(strCardType);
    regCreditCard.setEXPDate(IDLFormatUtility.getDate(strEXPDate));
    regCreditCard.setBillingAddress(strBillingAddress);
    regCreditCard.setBillingZipCode(strBillingZipCode);
    regCreditCard.setCreditCardDate(IDLFormatUtility.getDate(strDate));
    regCreditCard.setBusinessPartner(regBusinessPartner);

    OBDal.getInstance().save(regCreditCard);
    OBDal.getInstance().flush();

    return regCreditCard;

  }
}
