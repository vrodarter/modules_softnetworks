package com.spocsys.vigfurniture.importdata.entity;

import java.io.FileReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.idl.proc.Parameter;
import org.openbravo.idl.proc.Validator;
import org.openbravo.idl.proc.Value;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.module.idljava.proc.IdlServiceJava;

import au.com.bytecode.opencsv.CSVReader;

import com.spocsys.vigfurniture.importdata.utils.CustomValidator;
import com.spocsys.vigfurniture.importdata.utils.IDLFormatUtility;
import com.spocsys.vigfurniture.importdata.utils.Utils;

public class ImportPO extends IdlServiceJava {

  private int TotalLines;
  private int ActualLine;
  private CustomValidator cValidator;
  HashMap<String, Order> ListOrder = new HashMap<String, Order>();

  @Override
  protected boolean executeImport(String filename, boolean insert) throws Exception {

    CSVReader reader = new CSVReader(new FileReader(filename), IDLFormatUtility.getDelimiter(),
        '\"', '\\', 0, false, true);

    List AllLinesList = reader.readAll();
    TotalLines = AllLinesList.size();

    String[] nextLine;

    // Check header
    nextLine = (String[]) AllLinesList.get(0);
    if (nextLine == null) {
      throw new OBException(Utility.messageBD(conn, "IDLJAVA_HEADER_MISSING", vars.getLanguage()));
    }
    Parameter[] parameters = getParameters();
    if (parameters.length > nextLine.length) {
      throw new OBException(
          Utility.messageBD(conn, "IDLJAVA_HEADER_BAD_LENGTH", vars.getLanguage()));
    }

    Validator validator;

    for (int i = 1; i < TotalLines; i++) {

      nextLine = (String[]) AllLinesList.get(i);
      ActualLine = i;

      if (nextLine.length > 1 || nextLine[0].length() > 0) {
        // It is not an empty line
        // Log Progress
        IDLFormatUtility.logProgress(ActualLine, (TotalLines - 1), this);
        // Validate types
        if (parameters.length > nextLine.length) {
          throw new OBException(Utility.messageBD(conn, "IDLJAVA_LINE_BAD_LENGTH",
              vars.getLanguage()));
        }

        validator = getValidator(getEntityName());
        cValidator = new CustomValidator(this, getEntityName());
        Object[] result = validateProcess(validator, nextLine);
        if ("0".equals(cValidator.getErrorCode())) {
          finishRecordProcess(result);
        } else {
          OBDal.getInstance().rollbackAndClose();
          // We need rollback here becouse the intention is load ALL or NOTHING
          logRecordError(cValidator.getErrorMessage(), result);
        }
      }
    }

    return true;
  }

  @Override
  protected String getEntityName() {
    // TODO Auto-generated method stub
    return "Purchase Order";
  }

  @Override
  public Parameter[] getParameters() {
    return new Parameter[] { new Parameter("Organization", Parameter.STRING),
        new Parameter("Document Number", Parameter.STRING),
        new Parameter("Order Date", Parameter.DATE),
        new Parameter("Estimated Time of Arrival", Parameter.DATE),
        new Parameter("Business Partner", Parameter.STRING),
        new Parameter("Warehouse", Parameter.STRING),
        new Parameter("Scheduled Delivery Date", Parameter.DATE),
        new Parameter("Payment Method", Parameter.STRING),
        new Parameter("Payment Terms", Parameter.STRING),
        new Parameter("Price List", Parameter.STRING),
        new Parameter("Customer Order Reference Header", Parameter.STRING),
        new Parameter("Price includes Tax", Parameter.BOOLEAN),
        new Parameter("Box Header", Parameter.BIGDECIMAL),
        new Parameter("Description Header", Parameter.STRING),
        new Parameter("Product", Parameter.STRING),
        new Parameter("Ordered Quantity", Parameter.BIGDECIMAL),
        new Parameter("Net Unit Price", Parameter.BIGDECIMAL),
        new Parameter("Tax", Parameter.STRING),
        new Parameter("Net List Price", Parameter.BIGDECIMAL),
        new Parameter("Discount %", Parameter.BIGDECIMAL),
        new Parameter("Customer Order Reference Line", Parameter.STRING),
        new Parameter("Box Line", Parameter.BIGDECIMAL),
        new Parameter("Allocated Qty", Parameter.BIGDECIMAL),
        new Parameter("Description Line", Parameter.STRING) };
  }

  @Override
  protected Object[] validateProcess(Validator validator, String... values) throws Exception {
    // TODO Auto-generated method stub

    validator.checkOrganization(values[0]);// 0
    validator.checkNotNull(validator.checkString(values[1], 30), "Document Number");// 1
    validator.checkNotNull(IDLFormatUtility.validateDate(values[2]), "Order Date");// 2

    if ((values[3] != null) && (values[3].trim().equals("") != true)) {
      validator.checkNotNull(validator.checkDate(values[3]), "Estimated Time of Arrival");// 3
    }

    validator.checkNotNull(validator.checkString(values[4], 40), "Business Partner");// 4
    validator.checkNotNull(validator.checkString(values[5], 40), "Warehouse");// 5

    if ((values[6] != null) && (values[6].trim().equals("") != true)) {
      validator.checkNotNull(validator.checkDate(values[6]), "Scheduled Delivery Date");// 6
    }

    validator.checkNotNull(validator.checkString(values[7], 60), "Payment Method");// 7
    validator.checkNotNull(validator.checkString(values[8], 40), "Payment Terms");// 8
    validator.checkNotNull(validator.checkString(values[9], 60), "Price List");// 9
    validator.checkString(values[10], 20, "Customer Order Reference Header");// 10
    validator.checkBoolean(values[11], "Price includes Tax");// 11
    validator.checkBigDecimal(values[12], "Box Header");// 12
    validator.checkString(values[13], 255, "Description Header");// 13
    validator.checkNotNull(validator.checkString(values[14], 40), "Product");// 14
    validator.checkNotNull(validator.checkBigDecimal(values[15]), "Ordered Quantity");// 15
    validator.checkNotNull(validator.checkBigDecimal(values[16]), "Net Unit Price");// 16
    validator.checkNotNull(validator.checkString(values[17], 60), "Tax");// 17
    validator.checkNotNull(validator.checkBigDecimal(values[18]), "Net List Price");// 18
    validator.checkBigDecimal(values[19], "Discount %");// 19
    validator.checkString(values[20], 20, "Customer Order Reference Line");// 20
    validator.checkBigDecimal(values[21], "Box Line");// 21
    validator.checkBigDecimal(values[22], "Allocated Qty");// 22
    validator.checkString(values[23], 255, "Description Line");// 23
    // ------------------------------------------
    String strOrg = values[0];
    String strBusinessPartner = values[4];
    String strWarehouse = values[5];
    String strPaymentMethod = values[7];
    String strPaymentTerms = values[8];
    String strPriceList = values[9];
    String strProduct = values[14];
    String strTax = values[17];

    // ------------------------------------------

    BusinessPartner regBusinessPartner = findDALInstance(false, BusinessPartner.class, new Value(
        BusinessPartner.PROPERTY_SEARCHKEY, strBusinessPartner));

    Warehouse regWarehouse = findDALInstance(false, Warehouse.class, new Value(
        Warehouse.PROPERTY_SEARCHKEY, strWarehouse));

    FIN_PaymentMethod regPaymentMethod = findDALInstance(false, FIN_PaymentMethod.class, new Value(
        FIN_PaymentMethod.PROPERTY_NAME, strPaymentMethod));

    PaymentTerm regPaymentTerms = findDALInstance(false, PaymentTerm.class, new Value(
        PaymentTerm.PROPERTY_NAME, strPaymentTerms));

    PriceList regPriceList = findDALInstance(false, PriceList.class, new Value(
        PriceList.PROPERTY_NAME, strPriceList));

    Product regProduct = findDALInstance(false, Product.class, new Value(
        Product.PROPERTY_SEARCHKEY, strProduct));

    TaxRate regTax = findDALInstance(false, TaxRate.class, new Value(TaxRate.PROPERTY_NAME, strTax));

    // -------------------------------------------

    Organization regOrganization = Utils.getOrgBySearchKey(strOrg);

    if (regOrganization == null) {
      cValidator.setErrorMessage("Organization doesn't exist:  " + strOrg);
    } else {
      Currency regCurrency = Utils.getCurrency(regOrganization);

      if (regCurrency == null) {
        cValidator.setErrorMessage("Currency not found for Organization:  " + strOrg);
      }

    }

    if (regBusinessPartner == null) {
      cValidator.setErrorMessage("Business Partner doesn't exist:  " + strBusinessPartner);
    } else {

      if (regBusinessPartner.getBusinessPartnerLocationList().size() <= 0) {
        cValidator
            .setErrorMessage("Business Partner doesn't have location:  " + strBusinessPartner);
      }
    }

    if (regWarehouse == null) {
      cValidator.setErrorMessage("Warehouse doesn't exist:  " + strWarehouse);
    }

    if (regPaymentMethod == null) {
      cValidator.setErrorMessage("Payment Method doesn't exist:  " + strPaymentMethod);
    }

    if (regPaymentTerms == null) {
      cValidator.setErrorMessage("Payment Terms doesn't exist:  " + strPaymentTerms);
    }

    if (regPriceList == null) {
      cValidator.setErrorMessage("Price List doesn't exist:  " + strPriceList);
    }

    if (regProduct == null) {
      cValidator.setErrorMessage("Product doesn't exist:  " + strProduct);
    }

    if (regTax == null) {
      cValidator.setErrorMessage("Tax doesn't exist:  " + strTax);
    }

    return values;
  }

  @Override
  protected BaseOBObject internalProcess(Object... values) throws Exception {
    // TODO Auto-generated method stub
    return create((String) values[0], (String) values[1], (String) values[2], (String) values[3],
        (String) values[4], (String) values[5], (String) values[6], (String) values[7],
        (String) values[8], (String) values[9], (String) values[10], (String) values[11],
        (String) values[12], (String) values[13], (String) values[14], (String) values[15],
        (String) values[16], (String) values[17], (String) values[18], (String) values[19],
        (String) values[20], (String) values[21], (String) values[22], (String) values[23]);

  }

  public BaseOBObject create(final String strOrganization, final String strDocumentNumber,
      final String strOrderDate, final String strEstimatedTimeofArrival,
      final String strBusinessPartner, final String strWarehouse,
      final String strScheduledDeliveryDate, final String strPaymentMethod,
      final String strPaymentTerms, final String strPriceList,
      final String strCustomerOrderReferenceHeader, final String strPriceincludesTax,
      final String strBoxHeader, final String strDescriptionHeader, final String strProduct,
      final String strOrderedQuantity, final String strNetUnitPrice, final String strTax,
      final String strNetListPrice, final String strDiscount,
      final String strCustomerOrderReferenceLine, final String strBoxLine,
      final String strAllocatedQty, final String strDescriptionLine) throws Exception {

    Organization regOrganization = Utils.getOrgBySearchKey(strOrganization);
    Date regOrderDate = IDLFormatUtility.getDate(strOrderDate);
    Date regEstimatedTimeofArrival = null;

    if ((strEstimatedTimeofArrival != null) && (strEstimatedTimeofArrival.equals("") != true)) {
      regEstimatedTimeofArrival = IDLFormatUtility.getDate(strEstimatedTimeofArrival);
    }

    BusinessPartner regBusinessPartner = findDALInstance(false, BusinessPartner.class, new Value(
        BusinessPartner.PROPERTY_SEARCHKEY, strBusinessPartner));
    Warehouse regWarehouse = findDALInstance(false, Warehouse.class, new Value(
        Warehouse.PROPERTY_SEARCHKEY, strWarehouse));
    Date regScheduledDeliveryDate = regOrderDate;

    if ((strScheduledDeliveryDate != null) && (strScheduledDeliveryDate.equals("") != true)) {
      regScheduledDeliveryDate = IDLFormatUtility.getDate(strScheduledDeliveryDate);
    }

    FIN_PaymentMethod regPaymentMethod = findDALInstance(false, FIN_PaymentMethod.class, new Value(
        FIN_PaymentMethod.PROPERTY_NAME, strPaymentMethod));
    PaymentTerm regPaymentTerms = findDALInstance(false, PaymentTerm.class, new Value(
        PaymentTerm.PROPERTY_NAME, strPaymentTerms));
    PriceList regPriceList = findDALInstance(false, PriceList.class, new Value(
        PriceList.PROPERTY_NAME, strPriceList));
    boolean regPriceincludesTax = Parameter.BOOLEAN.parse(strPriceincludesTax);
    BigDecimal regBoxHeader = Parameter.BIGDECIMAL.parse(strBoxHeader);
    Product regProduct = findDALInstance(false, Product.class, new Value(
        Product.PROPERTY_SEARCHKEY, strProduct));
    BigDecimal regOrderedQuantity = Parameter.BIGDECIMAL.parse(strOrderedQuantity);
    BigDecimal regNetUnitPrice = Parameter.BIGDECIMAL.parse(strNetUnitPrice);
    TaxRate regTax = findDALInstance(false, TaxRate.class, new Value(TaxRate.PROPERTY_NAME, strTax));
    BigDecimal regNetListPrice = Parameter.BIGDECIMAL.parse(strNetListPrice);
    BigDecimal regDiscount = Parameter.BIGDECIMAL.parse(strDiscount);
    BigDecimal regBoxLine = Parameter.BIGDECIMAL.parse(strBoxLine);
    BigDecimal regAllocatedQty = Parameter.BIGDECIMAL.parse(strAllocatedQty);

    // ---------------------------------------------------------------
    OrderLine regOrderLine = OBProvider.getInstance().get(OrderLine.class);

    regOrderLine.setClient(OBContext.getOBContext().getCurrentClient());
    regOrderLine.setOrganization(regOrganization);
    regOrderLine.setCreatedBy(OBContext.getOBContext().getUser());
    regOrderLine.setUpdatedBy(OBContext.getOBContext().getUser());
    regOrderLine.setOrderDate(regOrderDate);
    regOrderLine.setDescription(strDescriptionLine);
    regOrderLine.setProduct(regProduct);
    regOrderLine.setWarehouse(regWarehouse);
    regOrderLine.setUOM(regProduct.getUOM());
    regOrderLine.setCurrency(Utils.getCurrency(regOrganization));
    regOrderLine.setUnitPrice(regNetUnitPrice);
    regOrderLine.setListPrice(regNetListPrice);
    regOrderLine.setDiscount(regDiscount);
    regOrderLine.setTax(regTax);
    regOrderLine.setOrderedQuantity(regOrderedQuantity);
    regOrderLine.setSvfsvPoreference(strCustomerOrderReferenceLine);
    regOrderLine.setSvfsvBox(regBoxLine);
    regOrderLine.setSvfsvQtyallocated(regAllocatedQty.longValue());

    // ---
    regOrderLine.setLineNetAmount(BigDecimal.ZERO);
    regOrderLine.setPriceLimit(BigDecimal.ZERO);
    regOrderLine.setFreightAmount(BigDecimal.ZERO);
    regOrderLine.setStandardPrice(BigDecimal.ZERO);
    regOrderLine.setDirectShipment(false);
    regOrderLine.setReservedQuantity(BigDecimal.ZERO);
    regOrderLine.setDeliveredQuantity(BigDecimal.ZERO);
    regOrderLine.setInvoicedQuantity(BigDecimal.ZERO);
    regOrderLine.setDescriptionOnly(false);

    // ---

    Order regOrder = getOrder(regOrganization, strDocumentNumber, regOrderDate,
        regEstimatedTimeofArrival, regBusinessPartner, regWarehouse, regScheduledDeliveryDate,
        regPaymentMethod, regPaymentTerms, regPriceList, strCustomerOrderReferenceHeader,
        regPriceincludesTax, regBoxHeader, strDescriptionHeader);

    int LineNro = regOrder.getOrderLineList() != null ? regOrder.getOrderLineList().size() : 0;
    LineNro = LineNro + 10;
    Long LineNroNew = Long.parseLong(String.valueOf(LineNro));
    regOrderLine.setLineNo(LineNroNew);
    regOrder.getOrderLineList().add(regOrderLine);
    regOrderLine.setSalesOrder(regOrder);

    OBDal.getInstance().save(regOrder);
    OBDal.getInstance().save(regOrderLine);

    OBDal.getInstance().flush();

    // ---------------------------------------------------------------

    return regOrderLine;

  }

  Order getOrder(Organization regOrganization, String DocumentNumber, Date OrderDate,
      Date EstimatedTimeofArrival, BusinessPartner regBusinessPartner, Warehouse regWarehouse,
      Date ScheduledDeliveryDate, FIN_PaymentMethod regPaymentMethod, PaymentTerm regPaymentTerms,
      PriceList regPriceList, String CustomerOrderReferenceHeader, boolean PriceincludesTax,
      BigDecimal BoxHeader, String DescriptionHeader) {

    String ID = regOrganization.getId();
    ID += DocumentNumber;
    ID += OrderDate.toString();
    ID += regBusinessPartner.getId();
    ID += regWarehouse.getId();

    if (ListOrder.get(ID) != null) {
      return ListOrder.get(ID);
    } else {
      Order regOrder = OBProvider.getInstance().get(Order.class);

      regOrder.setClient(OBContext.getOBContext().getCurrentClient());
      regOrder.setOrganization(regOrganization);
      regOrder.setCreatedBy(OBContext.getOBContext().getUser());
      regOrder.setUpdatedBy(OBContext.getOBContext().getUser());
      regOrder.setSalesTransaction(false);
      regOrder.setDocumentNo(DocumentNumber);
      regOrder.setDocumentStatus("DR");
      regOrder.setDocumentAction("CO");
      DocumentType regDocumentType = Utils.getDocumentType("0", false, false);

      if (regDocumentType == null) {
        throw new OBException("Document Type not found");
      }
      regOrder.setDocumentType(regDocumentType);

      DocumentType regDoctypeTarget = Utils.getDocumentType("POO", false, false);

      if (regDoctypeTarget == null) {
        throw new OBException("Document Type Target not found");
      }

      regOrder.setTransactionDocument(regDoctypeTarget);
      regOrder.setDocumentNo(DocumentNumber);
      regOrder.setDescription(DescriptionHeader);
      regOrder.setOrderDate(OrderDate);
      regOrder.setScheduledDeliveryDate(ScheduledDeliveryDate);
      regOrder.setAccountingDate(OrderDate);
      regOrder.setBusinessPartner(regBusinessPartner);
      regOrder.setPartnerAddress(regBusinessPartner.getBusinessPartnerLocationList().get(0));
      regOrder.setOrderReference(CustomerOrderReferenceHeader);
      regOrder.setCurrency(Utils.getCurrency(regOrganization));
      regOrder.setFormOfPayment("P");
      regOrder.setInvoiceTerms("I");
      regOrder.setDeliveryTerms("A");
      regOrder.setFreightCostRule("I");
      regOrder.setDeliveryMethod("P");
      regOrder.setPriority("5");
      regOrder.setWarehouse(regWarehouse);
      regOrder.setPriceList(regPriceList);
      regOrder.setPriceIncludesTax(PriceincludesTax);
      regOrder.setPaymentMethod(regPaymentMethod);
      regOrder.setPaymentTerms(regPaymentTerms);
      regOrder.setSvfpaEta(EstimatedTimeofArrival);
      regOrder.setSvfsvBox(BoxHeader.longValue());
      ListOrder.put(ID, regOrder);
      OBDal.getInstance().save(regOrder);
      return regOrder;
    }

    // return null;
  }
}
