package com.spocsys.vigfurniture.importdata.entity;

import java.io.FileReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.*;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.idl.proc.Parameter;
import org.openbravo.idl.proc.Validator;
import org.openbravo.idl.proc.Value;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.plm.ApprovedVendor;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.module.idljava.proc.IdlServiceJava;

import au.com.bytecode.opencsv.CSVReader;

import com.spocsys.vigfurniture.importdata.utils.CustomValidator;
import com.spocsys.vigfurniture.importdata.utils.IDLFormatUtility;
import com.spocsys.vigfurniture.importdata.utils.Utils;

public class ImportPaymentPO extends IdlServiceJava {

  private static Logger log=Logger.getLogger(FIN_Payment.class);
 
  private CustomValidator cValidator;
  
  private int TotalLines;
  private int ActualLine;
 
  HashMap<String, FIN_Payment> ListPayment = new HashMap<String, FIN_Payment>();

  @Override
  protected boolean executeImport(String filename, boolean insert) throws Exception {

    CSVReader reader = new CSVReader(new FileReader(filename), IDLFormatUtility.getDelimiter(),
        '\"', '\\', 0, false, true);

    List AllLinesList = reader.readAll();
    TotalLines = AllLinesList.size();

    String[] nextLine;

    // Check header
    nextLine = (String[]) AllLinesList.get(0);
    if (nextLine == null) {
      throw new OBException(Utility.messageBD(conn, "IDLJAVA_HEADER_MISSING", vars.getLanguage()));
    }
    Parameter[] parameters = getParameters();
    if (parameters.length > nextLine.length) {
      throw new OBException(
          Utility.messageBD(conn, "IDLJAVA_HEADER_BAD_LENGTH", vars.getLanguage()));
    }

    Validator validator;

    for (int i = 1; i < TotalLines; i++) {

      nextLine = (String[]) AllLinesList.get(i);
      ActualLine = i;

      if (nextLine.length > 1 || nextLine[0].length() > 0) {
        // It is not an empty line
        // Log Progress
        IDLFormatUtility.logProgress(ActualLine, (TotalLines - 1), this);
        // Validate types
        if (parameters.length > nextLine.length) {
          throw new OBException(Utility.messageBD(conn, "IDLJAVA_LINE_BAD_LENGTH",
              vars.getLanguage()));
        }

        validator = getValidator(getEntityName());
        cValidator = new CustomValidator(this, getEntityName());
        Object[] result = validateProcess(validator, nextLine);
        if ("0".equals(cValidator.getErrorCode())) {
          finishRecordProcess(result);
        } else {
          OBDal.getInstance().rollbackAndClose();
          // We need rollback here becouse the intention is load ALL or NOTHING
          logRecordError(cValidator.getErrorMessage(), result);
        }
      }
    }

    return true;
  }
  
  
  
  
  
  public ImportPaymentPO() {
    // TODO Auto-generated constructor stub
  }

  @Override
  protected String getEntityName() {
    // TODO Auto-generated method stub
    return "Payment Purchase Order";
    
  }
  
  
  @Override
  public Parameter[] getParameters() {
    return new Parameter[] { new Parameter("Organization", Parameter.STRING),
        new Parameter("Document Number", Parameter.STRING),
        new Parameter("Payment Date", Parameter.DATE),
        new Parameter("Business Partner", Parameter.STRING),
        new Parameter("Total Amount", Parameter.BIGDECIMAL),
        new Parameter("Financial Acountd", Parameter.STRING),
        new Parameter("Payment Method", Parameter.STRING)
    };
  }

  
  
  

  @Override
  protected Object[] validateProcess(Validator validator, String... values) throws Exception {
    // TODO Auto-generated method stub
    
    validator.checkOrganization(values[0]);// 0
    validator.checkNotNull(validator.checkString(values[1], 30), "Document Number");// 1
    validator.checkNotNull(IDLFormatUtility.validateDate(values[2]), "Payment Date");// 2
    validator.checkNotNull(validator.checkString(values[3],200), "Business Partner");// 3 
    validator.checkNotNull(validator.checkBigDecimal(values[4]), "Total Amount");// 4
    validator.checkNotNull(validator.checkBigDecimal(values[5]), "Generated Credit");// 4
    validator.checkNotNull(validator.checkString(values[6],60), "Payment Method");// 5reg
    validator.checkNotNull(validator.checkString(values[7],60), "Financial Account");
    validator.checkNotNull(validator.checkString(values[8], 60), "Customer Order Reference Header");// 6
    validator.checkNotNull(validator.checkString(values[9],60), "Purchase Order");// 4
    


    // ------------------------------------------
    String strOrg = values[0];
    String strBusinessPartner = values[3];
    String strPaymentMethod = values[6]; 
    String strFinancialAcountd = values[7];
    String strPurchaseOrderId = values[9];

    // ------------------------------------------

    BusinessPartner regBusinessPartner = findDALInstance(false, BusinessPartner.class, new Value(
        BusinessPartner.PROPERTY_SEARCHKEY, strBusinessPartner));


    FIN_PaymentMethod regPaymentMethod = findDALInstance(false, FIN_PaymentMethod.class, new Value(
        FIN_PaymentMethod.PROPERTY_NAME, strPaymentMethod));

    FIN_FinancialAccount regFinancialAcountd = findDALInstance(false, FIN_FinancialAccount.class, new Value(
        FIN_FinancialAccount.PROPERTY_NAME, strFinancialAcountd)); 
    
    Order  regOrderId = findDALInstance(false, Order.class, new Value(
        Order.PROPERTY_DOCUMENTNO, strPurchaseOrderId)); 

    // ------ -------------------------------------

    Organization regOrganization = Utils.getOrgBySearchKey(strOrg);

    if (regOrganization == null) {
      cValidator.setErrorMessage("Organization doesn't exist:  " + strOrg);
    } else {
      Currency regCurrency = Utils.getCurrency(regOrganization);

      if (regCurrency == null) {
        cValidator.setErrorMessage("Currency not found for Organization:  " + strOrg);
      }

    }

    if (regBusinessPartner == null) {
      cValidator.setErrorMessage("Business Partner doesn't exist:  " + strBusinessPartner);
    } else {

      if (regBusinessPartner.getBusinessPartnerLocationList().size() <= 0) {
        cValidator
            .setErrorMessage("Business Partner doesn't have location:  " + strBusinessPartner);
      }
    }



    if (regPaymentMethod == null) {
      cValidator.setErrorMessage("Payment Method doesn't exist:  " + strPaymentMethod);
    }
    
    if (regFinancialAcountd == null) {
      cValidator.setErrorMessage("Financial Acountd doesn't exist:  " + strFinancialAcountd);
    }

   
    if ( regOrderId == null) {
      cValidator.setErrorMessage("Purchase Order doesn't exist:  " + strPurchaseOrderId);
    }
    return values;
  }

  @Override
  protected BaseOBObject internalProcess(Object... values) throws Exception {
    // TODO Auto-generated method stub
    return create((String) values[0], (String) values[1], (String) values[2], (String) values[3],
        (String) values[4], (String) values[5], (String) values[6], (String) values[7],(String) values[8],(String) values[9]);
  }
  
  
  
  
  
  
  public BaseOBObject create(
                              final String strOrganization,
                              final String strDocumentNumber,
                              final String strPaymentDate,  
                              final String strBusinessPartner, 
                              final String strTotalAmount, 
                              final String strGeneratedCredit,
                              final String strPaymentMethod,
                              final String strFinancialAcountd,
                              final String strCustomerOrderReferenceHeader,
                              final String strPurchaseOrderId
      
                                ) throws Exception {

  
    
    Organization regOrganization = Utils.getOrgBySearchKey(strOrganization);
    Date regPaymentrDate = IDLFormatUtility.getDate(strPaymentDate);



    BusinessPartner regBusinessPartner = findDALInstance(false, BusinessPartner.class, new Value(
        BusinessPartner.PROPERTY_SEARCHKEY, strBusinessPartner));



    FIN_PaymentMethod regPaymentMethod = findDALInstance(false, FIN_PaymentMethod.class, new Value(
        FIN_PaymentMethod.PROPERTY_NAME, strPaymentMethod));
    
    

    FIN_FinancialAccount regFinancialAcountd = findDALInstance(false, FIN_FinancialAccount.class, new Value(
        FIN_FinancialAccount.PROPERTY_NAME, strFinancialAcountd));
 
     DocumentType regDocumentType = findDALInstance(false, DocumentType.class, new Value(
         DocumentType.PROPERTY_NAME, "AP Payment"));    
     
     OBCriteria<Order>   listOrder =  OBDal.getInstance().createCriteria(Order.class);
     listOrder.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, false ));  
     listOrder.add(Restrictions.eq(Order.PROPERTY_DOCUMENTNO, strPurchaseOrderId));
     listOrder.add(Restrictions.eq(Order.PROPERTY_ORGANIZATION, regOrganization));
     
     Order regOrder = listOrder.list().get(0);   
     
    //----------------------------------------------------------------
   
     String strOrderNo = regOrder.getDocumentNo();

    // ---------------------------------------------------------------
     
     FIN_Payment regPayment =  getThePayment(regOrganization, regPaymentMethod, 
         regBusinessPartner,regPaymentrDate,strTotalAmount,
         strDocumentNumber,regFinancialAcountd,regDocumentType,
         strCustomerOrderReferenceHeader,strGeneratedCredit, strOrderNo); 
    
     //create payment detail
    if (( regOrder != null ) && (regPayment != null))
    {
      FIN_PaymentDetail regPaymentDetail = setPaymentDetail(regPayment, regOrder );
         
        //crete Payment_Schedule Detail
        if (regPaymentDetail != null)
        {
          
          FIN_PaymentScheduleDetail regpaymentSchedule = setPaymentScheduleDetail(regPaymentDetail , regOrder,strCustomerOrderReferenceHeader);
          
          
        }
      
      
    }  //OBDal.getInstance().save(regPayment);
   

   // OBDal.getInstance().flush();


    // ---------------------------------------------------------------

    return regPayment;
    
   }
 
  
  
  ///////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////
  
  
  FIN_PaymentScheduleDetail setPaymentScheduleDetail( FIN_PaymentDetail regPayment, Order regOrder, String strCustomerOrderReferenceHeader ){
    
    FIN_PaymentScheduleDetail regPaymentScheduleDET =  OBProvider.getInstance().get(FIN_PaymentScheduleDetail.class);
    
    FIN_PaymentSchedule  regPaymentSchudele = findDALInstance(false, FIN_PaymentSchedule.class, new Value(
        FIN_PaymentSchedule.PROPERTY_ORDER, regOrder)) ;   
          
          regPaymentScheduleDET.setClient(OBContext.getOBContext().getCurrentClient());
          regPaymentScheduleDET.setOrganization(regPayment.getOrganization());
          regPaymentScheduleDET.setCreatedBy(regPayment.getCreatedBy());
          regPaymentScheduleDET.setUpdatedBy(regPayment.getCreatedBy());
          regPaymentScheduleDET.setPaymentDetails(regPayment);
          regPaymentScheduleDET.setOrderPaymentSchedule(regPaymentSchudele);
          regPaymentScheduleDET.setAmount(regPayment.getAmount());
          regPaymentScheduleDET.setDoubtfulDebtAmount(BigDecimal.ZERO);
          regPaymentScheduleDET.setInvoicePaid(true);
          regPaymentScheduleDET.setSvfsvPoreference(strCustomerOrderReferenceHeader);
          regPaymentScheduleDET.setCanceled(false);
          
          
          OBDal.getInstance().save(regPaymentScheduleDET);
          return regPaymentScheduleDET   ;
    
    
  }
    
  
  
  ///////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////
  
  
  FIN_PaymentDetail setPaymentDetail( FIN_Payment regPayment, Order regOrder ){
    
          FIN_PaymentDetail regPaymentDetail =  OBProvider.getInstance().get(FIN_PaymentDetail.class);
          
          
          regPaymentDetail.setClient(OBContext.getOBContext().getCurrentClient());
          regPaymentDetail.setOrganization(regPayment.getOrganization());
          regPaymentDetail.setCreatedBy(regPayment.getCreatedBy());
          regPaymentDetail.setUpdatedBy(regPayment.getCreatedBy());
          regPaymentDetail.setFinPayment(regPayment);
          regPaymentDetail.setAmount(regPayment.getAmount());
          regPaymentDetail.setPrepayment(true);
          
          OBDal.getInstance().save(regPaymentDetail);
          return regPaymentDetail;   
    
    
  }
  
  
  
  ///////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////
  FIN_Payment  getThePayment( Organization regOrganization,  FIN_PaymentMethod regPaymentMethod,
                              BusinessPartner  regBusinessPartner,  Date regPaymentrDate, String strTotalAmount, String strDocumentNumber,
                              FIN_FinancialAccount regFinancialAcountd, DocumentType regDocumentType,String strCustomerOrderReferenceHeader,                           
                              String strGeneratedCredit, String strOrderNo   )                              
     {
    
       
              String ID = regOrganization.getId() + 
                  strDocumentNumber +
                  regPaymentrDate +
                  regBusinessPartner.getId() +
                  regFinancialAcountd.getId() +
                  strCustomerOrderReferenceHeader ;
    


         if(ListPayment.get(ID)!= null)
               {
                        return  ListPayment.get(ID);         
               }
              else{
 
    
                  /**/
                  FIN_Payment regPayment = OBProvider.getInstance().get(FIN_Payment.class);
                  regPayment.setClient(OBContext.getOBContext().getCurrentClient());//client    
                  regPayment.setOrganization(regOrganization);//organization    
                  regPayment.setCreatedBy(OBContext.getOBContext().getUser());    
                  regPayment.setUpdatedBy(OBContext.getOBContext().getUser());    
                  regPayment.setActive(true);    
                  regPayment.setReceipt(false);    
                  regPayment.setBusinessPartner(regBusinessPartner);    
                  regPayment.setPaymentDate(regPaymentrDate);    
                  regPayment.setCurrency(Utils.getCurrency(regOrganization));    
                  regPayment.setAmount(new BigDecimal(strTotalAmount));     
                  regPayment.setWriteoffAmount(BigDecimal.ZERO);    
                  regPayment.setPaymentMethod(regPaymentMethod);    
                  regPayment.setDocumentNo(strDocumentNumber);    
                  regPayment.setStatus("PWNC");    
                  regPayment.setProcessed(false);    
                  regPayment.setProcessNow(false);    
                  regPayment.setPosted("N");     
                  regPayment.setAccount(regFinancialAcountd);    
                  regPayment.setDocumentType(regDocumentType);     
                  regPayment.setCreatedByAlgorithm(false);   
                  regPayment.setFinancialTransactionConvertRate(BigDecimal.ONE);   
                  regPayment.setAPRMReversePayment(false);        
                  regPayment.setAPRMProcessPayment("RE");
                  regPayment.setAPRMReconcilePayment(false);
                  regPayment.setAPRMReconcilePayment(false);
                  regPayment.setAPRMReversePayment(false);
                  regPayment.setUSLOCCPSCHEDULEDPAYMENTS(false);
                  regPayment.setSvfsvPoreference(strCustomerOrderReferenceHeader);
                  regPayment.setDescription("Order No.: " + strOrderNo );
                  /**/
                  ListPayment.put(ID, regPayment);
                  OBDal.getInstance().save(regPayment);
                  return regPayment;
        
            }
    
     }

}
