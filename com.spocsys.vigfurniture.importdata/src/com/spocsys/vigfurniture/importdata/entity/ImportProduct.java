package com.spocsys.vigfurniture.importdata.entity;

import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.idl.proc.Parameter;
import org.openbravo.idl.proc.Validator;
import org.openbravo.idl.proc.Value;
import org.openbravo.model.ad.utility.Image;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.ApprovedVendor;
import org.openbravo.model.common.plm.Attribute;
import org.openbravo.model.common.plm.AttributeInstance;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.AttributeValue;
import org.openbravo.model.common.plm.Brand;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListSchema;
import org.openbravo.model.pricing.pricelist.PriceListSchemeLine;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.module.idljava.proc.IdlServiceJava;

import au.com.bytecode.opencsv.CSVReader;

import com.spocsys.vigfurniture.importdata.IDLFormatDefaults;
import com.spocsys.vigfurniture.importdata.utils.CustomValidator;
import com.spocsys.vigfurniture.importdata.utils.IDLFormatUtility;
import com.spocsys.vigfurniture.importdata.utils.Utils;

public class ImportProduct extends IdlServiceJava {
  private static Logger log = Logger.getLogger(ImportProduct.class);
  private int TotalLines;
  private int ActualLine;
  private CustomValidator cValidator;

  HashMap<String, Product> HashMapProduct = new HashMap<String, Product>();
  List<ProductCategory> ListProductCategory = new ArrayList<ProductCategory>();
  List<UOM> ListUOM = new ArrayList<UOM>();
  List<TaxCategory> ListTaxCategory = new ArrayList<TaxCategory>();
  List<ProductPrice> ProductPriceSales = new ArrayList<ProductPrice>();
  List<ProductPrice> ProductPricePurchase = new ArrayList<ProductPrice>();
  List<Brand> ListBrand = new ArrayList<Brand>();
  HashMap<String, PriceList> HashMapPriceListSales = new HashMap<String, PriceList>();
  HashMap<String, PriceListVersion> HashMapPriceListSalesVersion = new HashMap<String, PriceListVersion>();
  HashMap<String, PriceList> HashMapPriceListPurchase = new HashMap<String, PriceList>();
  HashMap<String, PriceListVersion> HashMapPriceListPurchaseVersion = new HashMap<String, PriceListVersion>();

  IDLFormatDefaults regIDLFormatDefaults = null;

  @Override
  protected boolean executeImport(String filename, boolean insert) throws Exception {

    regIDLFormatDefaults = IDLFormatUtility.getDefaultFormats();

    CSVReader reader = new CSVReader(new FileReader(filename), IDLFormatUtility.getDelimiter(),
        '\"', '\\', 0, false, true);

    List AllLinesList = reader.readAll();
    TotalLines = AllLinesList.size();

    String[] nextLine;

    // Check header
    nextLine = (String[]) AllLinesList.get(0);
    if (nextLine == null) {
      throw new OBException(Utility.messageBD(conn, "IDLJAVA_HEADER_MISSING", vars.getLanguage()));
    }
    Parameter[] parameters = getParameters();
    System.out.println("compare: " + parameters.length + "-" + nextLine.length);
    if (parameters.length > nextLine.length) {
      throw new OBException(
          Utility.messageBD(conn, "IDLJAVA_HEADER_BAD_LENGTH", vars.getLanguage()));
    }

    Validator validator;

    for (int i = 1; i < TotalLines; i++) {

      nextLine = (String[]) AllLinesList.get(i);
      ActualLine = i;

      if (nextLine.length > 1 || nextLine[0].length() > 0) {
        // It is not an empty line
        // Log Progress
        IDLFormatUtility.logProgress(ActualLine, (TotalLines - 1), this);
        // Validate types
        if (parameters.length > nextLine.length) {
          throw new OBException(Utility.messageBD(conn, "IDLJAVA_LINE_BAD_LENGTH",
              vars.getLanguage()));
        }

        validator = getValidator(getEntityName());
        cValidator = new CustomValidator(this, getEntityName());
        Object[] result = validateProcess(validator, nextLine);
        if ("0".equals(cValidator.getErrorCode())) {
          finishRecordProcess(result);
          OBDal.getInstance().getSession().clear();
        } else {
          OBDal.getInstance().rollbackAndClose();
          // We need rollback here becouse the intention is load ALL or NOTHING
          logRecordError(cValidator.getErrorMessage(), result);
          OBDal.getInstance().getSession().clear();
        }
      }
    }

    return true;
  }

  @Override
  protected String getEntityName() {
    // TODO Auto-generated method stub
    return "Product";
  }

  @Override
  public Parameter[] getParameters() {
    return new Parameter[] { new Parameter("Organization", Parameter.STRING),
        new Parameter("SearchKey", Parameter.STRING), new Parameter("Name", Parameter.STRING),
        new Parameter("Description", Parameter.STRING), new Parameter("UPCEAN", Parameter.STRING),
        new Parameter("ProductCategory", Parameter.STRING), new Parameter("UOM", Parameter.STRING),
        new Parameter("ProductType", Parameter.STRING),
        new Parameter("Production", Parameter.STRING),
        new Parameter("BillOfMaterial", Parameter.STRING),
        new Parameter("Discontinued", Parameter.STRING),
        new Parameter("CostType", Parameter.STRING),
        new Parameter("AttributeSet", Parameter.STRING),
        new Parameter("AttributeValue", Parameter.STRING),
        new Parameter("Stocked", Parameter.STRING), new Parameter("Purchase", Parameter.STRING),
        new Parameter("Sale", Parameter.STRING), new Parameter("TaxCategory", Parameter.STRING),
        new Parameter("StandardCost", Parameter.STRING),
        new Parameter("PriceSales", Parameter.STRING),
        new Parameter("PricePurchase", Parameter.STRING),
        new Parameter("VIG Product", Parameter.STRING),
        new Parameter("Product Charaterisic", Parameter.STRING),
        new Parameter("Product Base", Parameter.STRING),
        new Parameter("Detailed Description", Parameter.STRING),
        new Parameter("Brand", Parameter.STRING), new Parameter("Model", Parameter.STRING),
        new Parameter("Model #", Parameter.STRING), new Parameter("Color", Parameter.STRING),
        new Parameter("Color Code", Parameter.STRING),
        new Parameter("Finish Material", Parameter.STRING),
        new Parameter("Finish Material Code", Parameter.STRING),
        new Parameter("ItemType", Parameter.STRING),
        new Parameter("Item Specification", Parameter.STRING),
        new Parameter("Weight", Parameter.STRING), new Parameter("Volume", Parameter.STRING),
        new Parameter("Assemble Depth", Parameter.STRING),
        new Parameter("Assembled Height", Parameter.STRING),
        new Parameter("Assemble Width", Parameter.STRING),
        new Parameter("Shipping Height", Parameter.STRING),
        new Parameter("Shipping Depth", Parameter.STRING),
        new Parameter("Shipping Width", Parameter.STRING),
        new Parameter("Comments", Parameter.STRING), new Parameter("Box Number", Parameter.STRING),
        new Parameter("Total Number of Boxes", Parameter.STRING),
        new Parameter("Total Number Per Box", Parameter.STRING),
        new Parameter("Number of seats", Parameter.STRING),
        new Parameter("Assembly required", Parameter.STRING),
        new Parameter("White Globe delivery Required", Parameter.STRING),
        new Parameter("Delivery Method", Parameter.STRING),
        new Parameter("Regulations", Parameter.STRING),
        new Parameter("Magento SKU", Parameter.STRING),
        new Parameter("Link to Magento Page on VIG", Parameter.STRING),
        new Parameter("Parent", Parameter.STRING),
        new Parameter("Substitute Of", Parameter.STRING),
        new Parameter("Vendor", Parameter.STRING), new Parameter("Image", Parameter.STRING),
        new Parameter("Color 1", Parameter.STRING), new Parameter("Color 2", Parameter.STRING),
        new Parameter("Color Code 1", Parameter.STRING),
        new Parameter("Color Code 2", Parameter.STRING),
        new Parameter("Finish 1", Parameter.STRING), new Parameter("Finish 2", Parameter.STRING),
        new Parameter("Finish Code 1", Parameter.STRING),
        new Parameter("Finish Code 2", Parameter.STRING),
        new Parameter("Material 1", Parameter.STRING),
        new Parameter("Material 2", Parameter.STRING) };
  }

  @Override
  protected Object[] validateProcess(Validator validator, String... values) throws Exception {
    // TODO Auto-generated method stub

    if (values[0] == null) {
      values[0] = "0";
    } else {
      if (values[0].trim().equalsIgnoreCase("*")) {
        values[0] = "0";
      }
    }
    validator.checkOrganization(values[0]);// 0
    validator.checkNotNull(validator.checkString(values[1], 40), "SearchKey");// 1
    validator.checkNotNull(validator.checkString(values[2], 60), "Name");// 2
    validator.checkString(values[3], 255, "Description");// 3
    validator.checkString(values[4], 255, "UPCEAN");// 4
    validator.checkNotNull(validator.checkString(values[5], 40), "ProductCategory");// 5
    validator.checkNotNull(validator.checkString(values[6], 60), "UOM");// 6
    validator.checkNotNull(validator.checkString(values[7], 60), "ProductType");// 7
    validator.checkNotNull(validator.checkBoolean(values[8], "Production"), "Production");// 8
    validator.checkNotNull(validator.checkBoolean(values[9], "BillOfMaterial"), "BillOfMaterial");// 9
    validator.checkNotNull(validator.checkBoolean(values[10], "Discontinued"), "Discontinued");// 10
    validator.checkString(values[11], 60, "CostType");// 11
    validator.checkString(values[12], 60, "AttributeSet");// 12
    validator.checkString(values[13], 60, "AttributeValue");// 13
    validator.checkNotNull(validator.checkBoolean(values[14], "Stocked"), "Stocked");// 14
    validator.checkNotNull(validator.checkBoolean(values[15], "Purchase"), "Purchase");// 15
    validator.checkNotNull(validator.checkBoolean(values[16], "Sale"), "Sale");// 16
    validator.checkNotNull(validator.checkString(values[17], 60), "TaxCategory");// 17
    validator.checkBigDecimal(values[18], "StandardCost");// 18
    validator.checkBigDecimal(values[19], "PriceSales");// 19
    validator.checkBigDecimal(values[20], "PricePurchase");// 20
    validator.checkBigDecimal(values[21], "VIG Product");// 22
    validator.checkString(values[22], 60, "Product Charaterisic");// 22
    validator.checkBoolean(values[23], "Product Base");// 23
    validator.checkString(values[24], 60, "Detailed Description");// 24
    validator.checkString(values[25], 60, "Brand");// 25
    validator.checkString(values[26], 32, "Model");// 26
    validator.checkBigDecimal(values[27], "Model #");// 27
    validator.checkString(values[28], 60, "Color");// 28
    validator.checkString(values[29], 32, "Color Code");// 29
    validator.checkString(values[30], 60, "Finish Material");// 30
    validator.checkString(values[31], 32, "Finish Material Code");// 31
    validator.checkString(values[32], 60, "ItemType");// 32
    validator.checkString(values[33], 60, "Item Specification");// 33
    validator.checkBigDecimal(values[34], "Weight");// 34
    validator.checkBigDecimal(values[35], "Volume");// 35
    validator.checkBigDecimal(values[36], "Assemble Depth");// 36
    validator.checkBigDecimal(values[37], "Assembled Height");// 37
    validator.checkBigDecimal(values[38], "Assemble Width");// 38
    validator.checkBigDecimal(values[39], "Shipping Height");// 39
    validator.checkBigDecimal(values[40], "Shipping Depth");// 40
    validator.checkBigDecimal(values[41], "Shipping Width");// 41
    validator.checkString(values[42], 255, "Comments");// 42
    validator.checkBigDecimal(values[43], "Box Number");// 42
    validator.checkBigDecimal(values[44], "Total Number of Boxes");// 44
    validator.checkBigDecimal(values[45], "Total Number Per Box");// 45
    validator.checkBigDecimal(values[46], "Number of seats");// 46
    validator.checkBoolean(values[47], "Assembly required");// 47
    validator.checkBoolean(values[48], "White Globe delivery Required");// 48
    validator.checkString(values[49], 32, "Delivery Method");// 49
    validator.checkString(values[50], 32, "Regulations");// 50
    validator.checkString(values[51], 32, "Magento SKU");// 51
    validator.checkString(values[52], 250, "Link to Magento Page on VIG");// 52
    validator.checkString(values[53], 40, "Parent");// 53
    validator.checkString(values[54], 40, "Substitute Of");// 54
    validator.checkString(values[55], 40, "Vendor");// 55
    validator.checkString(values[56], 255, "Image");// 56
    validator.checkString(values[57], 60, "Color 1");
    validator.checkString(values[58], 60, "Color 2");
    validator.checkString(values[59], 32, "Color Code 1");
    validator.checkString(values[60], 32, "Color Code 2");
    validator.checkString(values[61], 60, "Finish 1");
    validator.checkString(values[62], 60, "Finish 2");
    validator.checkString(values[63], 32, "Finish Code 1");
    validator.checkString(values[64], 32, "Finish Code 2");
    validator.checkString(values[65], 60, "Material 1");
    validator.checkString(values[66], 60, "Material 2");

    String strProductType = values[7];
    String strCostType = values[11];
    String strAttributeSet = values[12];
    String strProductCharaterisic = values[22];
    // String strBrand = values[25];
    String strColor = values[28];
    String strFinishMaterial = values[30];
    String strItemType = values[32];
    String strItemSpecification = values[33];
    String strRegulations = values[50];
    String strVendor = values[55];
    String strImage = values[56];
    String strColor1 = values[57];
    String strColor2 = values[58];
    String strFinish1 = values[61];
    String strFinish2 = values[62];
    String strMaterial1 = values[65];
    String strMaterial2 = values[66];

    String NewstrProductType = Utils.getProductType(strProductType);
    String NewstrCostType = Utils.getCostType(strCostType);
    String NewstrProductCharaterisic = Utils.getProductCharaterisic(strProductCharaterisic);
    String NewstrColor = Utils.getColor(strColor);
    String NewstrFinishMaterial = Utils.getFinishMaterial(strFinishMaterial);
    String NewstrItemType = Utils.getItemType(strItemType);
    String NewstrItemSpecification = Utils.getItemSpecification(strItemSpecification);
    String NewstrRegulations = Utils.getRegulations(strRegulations);

    if (NewstrProductType == null) {
      cValidator.setErrorMessage("ProductType doesn't exist:  " + strProductType);
    }

    if ((strCostType != null) && (strCostType.trim().equalsIgnoreCase("") != true)) {
      if (NewstrCostType == null) {
        cValidator.setErrorMessage("CostType doesn't exist:  " + strCostType);
      }
    }

    AttributeSet regAttributeSetInstance = null;
    if ((strAttributeSet != null) && (strAttributeSet.trim().equalsIgnoreCase("") != true)) {
      regAttributeSetInstance = findDALInstance(false, AttributeSet.class, new Value(
          AttributeSet.PROPERTY_NAME, strAttributeSet));

      if (regAttributeSetInstance != null) {

      } else {
        cValidator.setErrorMessage("AttributeSet doesn't exist:  " + strAttributeSet);
      }

    }

    if ((strProductCharaterisic != null)
        && (strProductCharaterisic.trim().equalsIgnoreCase("") != true)) {
      if (NewstrProductCharaterisic == null) {
        cValidator
            .setErrorMessage("Product Charaterisic doesn't exist:  " + strProductCharaterisic);
      }
    }

    // if ((strBrand != null) && (strBrand.equalsIgnoreCase("") != true)) {
    // Brand regBrand = findDALInstance(false, Brand.class, new Value(Brand.PROPERTY_NAME,
    // strBrand));
    //
    // if (regBrand == null) {
    // cValidator.setErrorMessage("Brand doesn't exist: " + strBrand);
    // }
    //
    // }

    if ((strColor != null) && (strColor.trim().equalsIgnoreCase("") != true)) {

      if (NewstrColor == null) {
        cValidator.setErrorMessage("Color doesn't exist:  " + strColor);
      }
    }

    if ((strFinishMaterial != null) && (strFinishMaterial.trim().equalsIgnoreCase("") != true)) {

      if (NewstrFinishMaterial == null) {
        cValidator.setErrorMessage("Finish Material doesn't exist:  " + strFinishMaterial);
      }
    }

    if ((strItemType != null) && (strItemType.trim().equalsIgnoreCase("") != true)) {

      if (NewstrItemType == null) {
        cValidator.setErrorMessage("ItemType doesn't exist:  " + strItemType);
      }
    }

    if ((strItemSpecification != null)
        && (strItemSpecification.trim().equalsIgnoreCase("") != true)) {

      if (NewstrItemSpecification == null) {
        cValidator.setErrorMessage("Item Specification doesn't exist:  " + strItemSpecification);
      }
    }

    if ((strRegulations != null) && (strRegulations.trim().equalsIgnoreCase("") != true)) {

      if (NewstrRegulations == null) {
        cValidator.setErrorMessage("Regulations doesn't exist:  " + strRegulations);
      }
    }

    if ((strVendor != null) && (strVendor.trim().equalsIgnoreCase("") != true)) {
      BusinessPartner regBusinessPartner = findDALInstance(false, BusinessPartner.class, new Value(
          BusinessPartner.PROPERTY_SEARCHKEY, strVendor));

      if (regBusinessPartner == null) {
        cValidator.setErrorMessage("Vendor doesn't exist:  " + strVendor);
      }

    }

    if ((strImage != null) && (strImage.trim().equals("") != true)) {

      String ImagesDirectoty = regIDLFormatDefaults.getImagesdir();

      if ((ImagesDirectoty != null) && (ImagesDirectoty.trim().equals("") != true)
          && (ImagesDirectoty.trim().length() >= 1)) {

        String PathImage = ImagesDirectoty + File.separator + strImage;

        if (ImagesDirectoty.charAt(ImagesDirectoty.length() - 1) == File.separator.charAt(0)) {
          PathImage = ImagesDirectoty + strImage;

          if (existFile(PathImage) == false) {
            cValidator.setErrorMessage("The Image doesn't exist: " + PathImage);
          }
        }
      }
    }

    if ((strColor1 != null) && (strColor1.trim().equals("") != true)) {

      if (Utils.getColor(strColor1) == null) {
        cValidator.setErrorMessage("Color 1 doesn't exist: " + strColor1);
      }
    }

    if ((strColor2 != null) && (strColor2.trim().equals("") != true)) {

      if (Utils.getColor(strColor2) == null) {
        cValidator.setErrorMessage("Color 2 doesn't exist: " + strColor2);
      }
    }

    if ((strFinish1 != null) && (strFinish1.trim().equals("") != true)) {

      if (Utils.getFinish(strFinish1) == null) {
        cValidator.setErrorMessage("Finish 1 doesn't exist: " + strFinish1);
      }
    }

    if ((strFinish2 != null) && (strFinish2.trim().equals("") != true)) {

      if (Utils.getFinish(strFinish2) == null) {
        cValidator.setErrorMessage("Finish 2 doesn't exist: " + strFinish2);
      }
    }

    if ((strMaterial1 != null) && (strMaterial1.trim().equals("") != true)) {

      if (Utils.getMaterial(strMaterial1) == null) {
        cValidator.setErrorMessage("Material 1 doesn't exist: " + strMaterial1);
      }
    }

    if ((strMaterial2 != null) && (strMaterial2.trim().equals("") != true)) {

      if (Utils.getMaterial(strMaterial2) == null) {
        cValidator.setErrorMessage("Material 2 doesn't exist: " + strMaterial2);
      }
    }

    return values;
  }

  @Override
  protected BaseOBObject internalProcess(Object... values) throws Exception {
    // TODO Auto-generated method stub
    return create((String) values[0], (String) values[1], (String) values[2], (String) values[3],
        (String) values[4], (String) values[5], (String) values[6], (String) values[7],
        (String) values[8], (String) values[9], (String) values[10], (String) values[11],
        (String) values[12], (String) values[13], (String) values[14], (String) values[15],
        (String) values[16], (String) values[17], (String) values[18], (String) values[19],
        (String) values[20], (String) values[21], (String) values[22], (String) values[23],
        (String) values[24], (String) values[25], (String) values[26], (String) values[27],
        (String) values[28], (String) values[29], (String) values[30], (String) values[31],
        (String) values[32], (String) values[33], (String) values[34], (String) values[35],
        (String) values[36], (String) values[37], (String) values[38], (String) values[39],
        (String) values[40], (String) values[41], (String) values[42], (String) values[43],
        (String) values[44], (String) values[45], (String) values[46], (String) values[47],
        (String) values[48], (String) values[49], (String) values[50], (String) values[51],
        (String) values[52], (String) values[53], (String) values[54], (String) values[55],
        (String) values[56], (String) values[57], (String) values[58], (String) values[59],
        (String) values[60], (String) values[61], (String) values[62], (String) values[63],
        (String) values[64], (String) values[65], (String) values[66]);

  }

  public BaseOBObject create(final String strOrganization, final String strSearchKey,
      final String strName, final String strDescription, final String strUPCEAN,
      final String strProductCategory, final String strUOM, final String strProductType,
      final String strProduction, final String strBillOfMaterial, final String strDiscontinued,
      final String strCostType, final String strAttributeSet, final String strAttributeValue,
      final String strStocked, final String strPurchase, final String strSale,
      final String strTaxCategory, final String strStandardCost, final String strPriceSales,
      final String strPricePurchase, final String strVIGProduct,
      final String strProductCharaterisic, final String strProductBase,
      final String strDetailedDescription, final String strBrand, final String strModel,
      final String strModelCode, final String strColor, final String strColorCode,
      final String strFinishMaterial, final String strFinishMaterialCode, final String strItemType,
      final String strItemSpecification, final String strWeight, final String strVolume,
      final String strAssembleDepth, final String strAssembledHeight,
      final String strAssembleWidth, final String strShippingHeight, final String strShippingDepth,
      final String strShippingWidth, final String strComments, final String strBoxNumber,
      final String strTotalNumberofBoxes, final String strTotalNumberPerBox,
      final String strNumberofseats, final String strAssemblyrequired,
      final String strWhiteGlobedeliveryRequired, final String strDeliveryMethod,
      final String strRegulations, final String strMagentoSKU,
      final String strLinktoMagentoPageonVIG, final String strParent, final String strSubstituteOf,
      final String strVendor, final String strImage, final String strColor1,
      final String strColor2, final String strColorCode1, final String strColorCode2,
      final String strFinish1, final String strFinish2, final String strFinishCode1,
      final String strFinishCode2, final String strMaterial1, final String strMaterial2)
      throws Exception {

    HashMapProduct = new HashMap<String, Product>();
    ListProductCategory = new ArrayList<ProductCategory>();
    ListUOM = new ArrayList<UOM>();
    ListTaxCategory = new ArrayList<TaxCategory>();
    ProductPriceSales = new ArrayList<ProductPrice>();
    ProductPricePurchase = new ArrayList<ProductPrice>();
    HashMapPriceListSales = new HashMap<String, PriceList>();
    HashMapPriceListSalesVersion = new HashMap<String, PriceListVersion>();
    HashMapPriceListPurchase = new HashMap<String, PriceList>();
    HashMapPriceListPurchaseVersion = new HashMap<String, PriceListVersion>();

    long end = 0;
    long startTime = 0;
    //
    startTime = System.currentTimeMillis();
    String strOrganizationNew = strOrganization;

    if (strOrganization == null) {
      strOrganizationNew = "0";
    } else {
      if (strOrganization.trim().equalsIgnoreCase("*")) {
        strOrganizationNew = "0";
      }
    }

    Organization regOrganization = Utils.getOrgBySearchKey(strOrganizationNew);

    Product regProduct = findDALInstance(false, Product.class, new Value(
        Product.PROPERTY_SEARCHKEY, strSearchKey));

    String ProductKey = regOrganization.getId() + "_" + strSearchKey;

    if (regProduct == null) {

      if (HashMapProduct.containsKey(ProductKey) == true) {
        regProduct = HashMapProduct.get(ProductKey);
      } else {
        regProduct = OBProvider.getInstance().get(Product.class);
        regProduct.setClient(OBContext.getOBContext().getCurrentClient());
        regProduct.setOrganization(regOrganization);
        regProduct.setCreatedBy(OBContext.getOBContext().getUser());
        regProduct.setUpdatedBy(OBContext.getOBContext().getUser());
        regProduct.setSearchKey(strSearchKey);
      }
    }

    // else {
    // isNew = false;
    // regProduct = OBDal.getInstance().get(Product.class, regProduct.getId());
    // }

    regProduct.setName(strName);
    regProduct.setDescription(strDescription);
    regProduct.setUPCEAN(strUPCEAN);
    regProduct.setProductCategory(getCategory(strProductCategory, regOrganization));
    regProduct.setUOM(getUOM(strUOM));
    regProduct.setProductType(Utils.getProductType(strProductType));

    if (Utils.isBoolean(strProduction) == true) {
      regProduct.setProduction(Parameter.BOOLEAN.parse(strProduction));
    }

    if (Utils.isBoolean(strBillOfMaterial) == true) {
      regProduct.setBillOfMaterials(Parameter.BOOLEAN.parse(strBillOfMaterial));
    }

    if (Utils.isBoolean(strDiscontinued) == true) {
      regProduct.setDiscontinued(Parameter.BOOLEAN.parse(strDiscontinued));
    }

    regProduct.setCostType(Utils.getCostType(strCostType));

    // ---------------------------------------

    AttributeSet attset = findDALInstance(false, AttributeSet.class, new Value("name",
        strAttributeSet));
    regProduct.setAttributeSet(attset);

    if ((attset != null) && (strAttributeValue != null)
        && (strAttributeValue.trim().equals("") != true)) {

      String defAttSetValueType = searchDefaultValue("Product", "AttributeSetValueType", null);
      String attSetValueType = getReferenceValue("Use Attribute Set Value As", defAttSetValueType);

      // for some reason these entities need
      // to be processed in admin mode.
      try {

        OBContext.setAdminMode();

        AttributeSetInstance attsetinst = OBProvider.getInstance().get(AttributeSetInstance.class);
        attsetinst.setActive(true);
        attsetinst.setOrganization(regOrganization);
        attsetinst.setAttributeSet(attset); // not directly readable

        if (attSetValueType == null) {
          attSetValueType = "D";
        }
        regProduct.setUseAttributeSetValueAs(attSetValueType);

        if (attset.isSerialNo()) {
          attsetinst.setDescription("#" + strAttributeValue);
          attsetinst.setSerialNo(strAttributeValue);
        } else if (attset.isLot()) {
          attsetinst.setDescription("L" + strAttributeValue);
          attsetinst.setLotName(strAttributeValue);
        } else if (attset.getAttributeUseList().size() == 1) {

          Attribute att = attset.getAttributeUseList().get(0).getAttribute();

          AttributeValue attvalue = findDALInstance(false, AttributeValue.class, new Value(
              AttributeValue.PROPERTY_ATTRIBUTE, att), new Value("name", strAttributeValue));

          if (attvalue != null) {
            attsetinst.setDescription(attvalue.getName());

            AttributeInstance attinst = OBProvider.getInstance().get(AttributeInstance.class);
            attinst.setActive(true);
            attinst.setOrganization(regOrganization);
            attinst.setAttributeSetValue(attsetinst);
            attinst.setAttribute(attvalue.getAttribute());
            attinst.setAttributeValue(attvalue);

            attsetinst.getAttributeInstanceList().add(attinst);
          }
        }

        OBDal.getInstance().save(attsetinst);
        OBDal.getInstance().flush(); // uncomment
        regProduct.setAttributeSetValue(attsetinst);
      } finally {
        OBContext.restorePreviousMode();
      }
    }

    // ---------------------------------------

    if (Utils.isBoolean(strStocked) == true) {
      regProduct.setStocked(Parameter.BOOLEAN.parse(strStocked));
    }

    if (Utils.isBoolean(strPurchase) == true) {
      regProduct.setPurchase(Parameter.BOOLEAN.parse(strPurchase));
    }

    if (Utils.isBoolean(strSale) == true) {
      regProduct.setSale(Parameter.BOOLEAN.parse(strSale));
    }

    regProduct.setTaxCategory(getTaxCategoryM(strTaxCategory, regOrganization));

    // ----------------------------------------
    regProduct.setStandardCost(Utils.parseBigDecimal(strStandardCost));

    // 1) Create PriceListSchema
    if ((strPriceSales != null && !strPriceSales.equals(""))
        || (strPricePurchase != null && !strPricePurchase.equals(""))) {

      OBDal.getInstance().save(regProduct);
      OBDal.getInstance().flush();

      PriceListSchema pListSchema = getPriceListSchema(regOrganization);

      if (strPriceSales != null && !strPriceSales.equals("")) {
        getProductPriceSales(regOrganization, regProduct, strPriceSales, pListSchema);
      }

      if (strPricePurchase != null && !strPricePurchase.equals("")) {

        getProductPricePurchase(regOrganization, regProduct, strPricePurchase, pListSchema);
      }

    }

    // ----------------------------------------

    if (Utils.isBoolean(strVIGProduct) == true) {
      regProduct.setSvfpaIsvigproduct(Parameter.BOOLEAN.parse(strVIGProduct));
    }

    regProduct.setSvfsvProductCharact(Utils.getProductCharaterisic(strProductCharaterisic));

    if (Utils.isBoolean(strProductBase) == true) {
      regProduct.setSvfsvIsbase(Parameter.BOOLEAN.parse(strProductBase));
    }

    regProduct.setSvfsvDetailedDescription(strDetailedDescription);

    regProduct.setBrand(getBrand(regOrganization, strBrand));
    regProduct.setSvfsvModel(strModel);
    regProduct.setSvfsvModelnumber(strModelCode);
    regProduct.setSvfsvColor(Utils.getColor(strColor));
    regProduct.setSvfsvColorCode(strColorCode);
    regProduct.setSvfsvFinishMaterial(Utils.getFinishMaterial(strFinishMaterial));
    regProduct.setSvfsvFinishMaterialCode(strFinishMaterialCode);
    regProduct.setSvfsvItemtype(Utils.getItemType(strItemType));
    regProduct.setSvfsvItemspecification(Utils.getItemSpecification(strItemSpecification));

    if (Utils.parseBigDecimal(strWeight) != null) {
      regProduct.setWeight(Utils.parseBigDecimal(strWeight));
    }

    if (Utils.parseBigDecimal(strVolume) != null) {
      regProduct.setSvfsvVolume(Utils.parseBigDecimal(strVolume).longValue());
    }

    if (Utils.parseBigDecimal(strAssembleDepth) != null) {
      regProduct.setSvfsvAssembleDepth(strAssembleDepth);
    }

    if (Utils.parseBigDecimal(strAssembledHeight) != null) {
      regProduct.setSvfsvAssembledHeight(strAssembledHeight);
    }

    if (Utils.parseBigDecimal(strAssembleWidth) != null) {
      regProduct.setSvfsvAssembledWidth(strAssembleWidth);
    }

    if (Utils.parseBigDecimal(strShippingHeight) != null) {
      regProduct.setSvfsvShippingHeight(strShippingHeight);
    }

    if (Utils.parseBigDecimal(strShippingDepth) != null) {
      regProduct.setSvfsvShippingDepth(strShippingDepth);
    }

    if (Utils.parseBigDecimal(strShippingDepth) != null) {
      regProduct.setSvfsvShippingDepth(strShippingDepth);
    }

    if (Utils.parseBigDecimal(strShippingWidth) != null) {
      regProduct.setSvfsvShippingWidth(strShippingWidth);
    }

    regProduct.setSvfsvComments(strComments);

    if (Utils.parseBigDecimal(strBoxNumber) != null) {
      regProduct.setSvfsvBoxnumber(Utils.parseBigDecimal(strBoxNumber));
    }

    if (Utils.parseBigDecimal(strTotalNumberofBoxes) != null) {
      regProduct.setSvfsvTotalnumberofbox(Utils.parseBigDecimal(strTotalNumberofBoxes));
    }

    if (Utils.parseBigDecimal(strTotalNumberPerBox) != null) {
      regProduct.setSvfpTotalnumbeperbox(Utils.parseBigDecimal(strTotalNumberPerBox).longValue());
    }

    if (Utils.parseBigDecimal(strNumberofseats) != null) {
      regProduct.setSvfsvNumberSeats(Utils.parseBigDecimal(strNumberofseats).longValue());
    }

    if (Utils.isBoolean(strAssemblyrequired) == true) {
      regProduct.setSvfsvAssemblyReqFlag(Parameter.BOOLEAN.parse(strAssemblyrequired));
    }

    if (Utils.isBoolean(strWhiteGlobedeliveryRequired) == true) {
      regProduct.setSvfsvWhiteGlove(Parameter.BOOLEAN.parse(strWhiteGlobedeliveryRequired));
    }

    regProduct.setSvfsvDeliveryMethod(strDeliveryMethod);
    regProduct.setSvfsvRegulations(Utils.getRegulations(strRegulations));
    regProduct.setSvfsvMagentoSku(strMagentoSKU);
    regProduct.setSvfsvLinkToMagento(strLinktoMagentoPageonVIG);

    if ((strParent != null) && (strParent.trim().equals("") != true)) {

      Product regParent = findDALInstance(false, Product.class, new Value(
          Product.PROPERTY_SEARCHKEY, strParent));

      if (regParent == null) {
        throw new OBException("Parent doesn't exist: " + strParent);
      }
      regProduct.setSvfsvPartner(regParent);

      OBDal.getInstance().save(regProduct);
      OBDal.getInstance().flush(); // uncomment

      ProductBOM regProductBOM = getProductBOM(regProduct, regParent);

      if (regProductBOM != null) {

        OBDal.getInstance().save(regProductBOM);
        OBDal.getInstance().flush();

        // List<ProductBOM> ListProductBOM = regParent.getProductBOMList();
        //
        // if (ListProductBOM == null) {
        // ListProductBOM = new ArrayList<ProductBOM>();
        // }
        // ListProductBOM.add(regProductBOM);
        // OBDal.getInstance().save(regParent);
        // OBDal.getInstance().flush(); // uncomment

      }

    }

    if ((strSubstituteOf != null) && (strSubstituteOf.trim().equals("") != true)) {

      Product regSubstituteOf = findDALInstance(false, Product.class, new Value(
          Product.PROPERTY_SEARCHKEY, strSubstituteOf));

      if (regSubstituteOf == null) {
        throw new OBException("Substitute Of doesn't exist: " + strSubstituteOf);
      }

      regProduct.setSvfsvSubstituteOf(regSubstituteOf);

    }

    if ((strVendor != null) && (strVendor.trim().equals("") != true)) {

      ApprovedVendor regApprovedVendor = getPurchasing(regProduct.getApprovedVendorList(),
          strVendor);

      if (regApprovedVendor == null) {

        BusinessPartner regBusinessPartnerVendor = findDALInstance(false, BusinessPartner.class,
            new Value(BusinessPartner.PROPERTY_SEARCHKEY, strVendor));

        if (regBusinessPartnerVendor == null) {
          throw new OBException("Vendor doesn't exist: " + strVendor);
        }

        OBDal.getInstance().save(regProduct);
        OBDal.getInstance().flush(); // uncomment

        regApprovedVendor = OBProvider.getInstance().get(ApprovedVendor.class);
        regApprovedVendor.setClient(OBContext.getOBContext().getCurrentClient());
        regApprovedVendor.setOrganization(regOrganization);
        regApprovedVendor.setCreatedBy(OBContext.getOBContext().getUser());
        regApprovedVendor.setUpdatedBy(OBContext.getOBContext().getUser());
        regApprovedVendor.setBusinessPartner(regBusinessPartnerVendor);
        regApprovedVendor.setProduct(regProduct);
        OBDal.getInstance().save(regApprovedVendor);
        OBDal.getInstance().flush(); // uncomment

      }
    }

    if ((strImage != null) && (strImage.trim().equals("") != true)) {

      String ImagesDirectoty = regIDLFormatDefaults.getImagesdir();

      if ((ImagesDirectoty != null) && (ImagesDirectoty.trim().equals("") != true)
          && (ImagesDirectoty.trim().length() >= 1)) {

        String PathImage = ImagesDirectoty + File.separator + strImage;

        if (ImagesDirectoty.charAt(ImagesDirectoty.length() - 1) == File.separator.charAt(0)) {
          PathImage = ImagesDirectoty + strImage;
        }

        if (existFile(PathImage) == true) {

          Image regImage = OBProvider.getInstance().get(Image.class);
          regImage.setClient(OBContext.getOBContext().getCurrentClient());
          regImage.setOrganization(regOrganization);
          regImage.setCreatedBy(OBContext.getOBContext().getUser());
          regImage.setUpdatedBy(OBContext.getOBContext().getUser());
          regImage.setName(strImage.length() > 60 ? strImage.substring(0, 60) : strImage);

          byte[] ImageData = getFileBytes(PathImage);

          if (ImageData != null) {
            regImage.setBindaryData(ImageData);
            regImage.setMimetype(getMimeTypeImage(PathImage));
            OBDal.getInstance().save(regImage);
            OBDal.getInstance().flush(); // uncomment
            regProduct.setImage(regImage);

          } else {
            throw new OBException("There was an error when it was getting the image: " + PathImage);
          }

        } else {
          throw new OBException("The Image doesn't exist: " + PathImage);
        }

      }

    }

    regProduct.setSvfsvColor1(Utils.getColor(strColor1));
    regProduct.setSvfsvColor2(Utils.getColor(strColor2));
    regProduct.setSvfsvColorCode1(strColorCode1);
    regProduct.setSvfsvColorCode2(strColorCode2);
    regProduct.setSvfsvFinish1(Utils.getFinish(strFinish1));
    regProduct.setSvfsvFinish2(Utils.getFinish(strFinish2));
    regProduct.setSvfsvFinishCode1(strFinishCode1);
    regProduct.setSvfsvFinishCode2(strFinishCode2);
    regProduct.setSvfsvMaterial1(Utils.getMaterial(strMaterial1));
    regProduct.setSvfsvMaterial2(Utils.getMaterial(strMaterial2));

    OBDal.getInstance().save(regProduct);
    OBDal.getInstance().flush();
    HashMapProduct.put(ProductKey, regProduct);
    end = System.currentTimeMillis();
    System.out.println((end - startTime) + " milliseconds");
    //
    return regProduct;

  }

  ProductCategory getExistProductCategory(String str) {

    try {
      if ((ListProductCategory != null) && (ListProductCategory.size() > 0)) {

        for (ProductCategory regProductCategory : ListProductCategory) {

          if (regProductCategory.getSearchKey().equals(str) == true) {
            return regProductCategory;
          }
        }

      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;

  }

  ProductCategory getCategory(String str, Organization regOrganization) {

    ProductCategory regProductCategory = findDALInstance(false, ProductCategory.class, new Value(
        ProductCategory.PROPERTY_SEARCHKEY, str));

    if (regProductCategory == null) {
      regProductCategory = getExistProductCategory(str);

      if (regProductCategory == null) {
        regProductCategory = OBProvider.getInstance().get(ProductCategory.class);
        regProductCategory.setClient(OBContext.getOBContext().getCurrentClient());
        regProductCategory.setOrganization(regOrganization);
        regProductCategory.setCreatedBy(OBContext.getOBContext().getUser());
        regProductCategory.setUpdatedBy(OBContext.getOBContext().getUser());
        regProductCategory.setSearchKey(str);
        regProductCategory.setName(str);
        regProductCategory.setDescription("This category was created from Product IDL");
        regProductCategory.setPlannedMargin(BigDecimal.ZERO);
        OBDal.getInstance().save(regProductCategory);
        OBDal.getInstance().flush();
        ListProductCategory.add(regProductCategory);

      }

    }

    return regProductCategory;

  }

  UOM getExistUOM(String str) {

    try {
      if ((ListUOM != null) && (ListUOM.size() > 0)) {

        for (UOM regUOM : ListUOM) {

          if (regUOM.getName().equals(str) == true) {
            return regUOM;
          }
        }

      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;

  }

  UOM getUOM(String str) {

    UOM regUOM = findDALInstance(false, UOM.class, new Value(UOM.PROPERTY_NAME, str));
    Organization regOrganization = Utils.getOrgBySearchKey("0");

    if (regUOM == null) {
      regUOM = getExistUOM(str);

      if (regUOM == null) {

        String defStdPrecision = searchDefaultValue("Product", "StandardPrecision", null);
        String defCostingPrecision = searchDefaultValue("Product", "CostingPrecision", null);

        regUOM = OBProvider.getInstance().get(UOM.class);
        regUOM.setClient(OBContext.getOBContext().getCurrentClient());
        regUOM.setOrganization(regOrganization);
        regUOM.setCreatedBy(OBContext.getOBContext().getUser());
        regUOM.setUpdatedBy(OBContext.getOBContext().getUser());
        regUOM
            .setEDICode(str.length() >= 2 ? str.substring(0, 2).toUpperCase() : str.toUpperCase());
        regUOM.setName(str);
        regUOM.setDescription("This UOM was created from Product IDL");
        regUOM
            .setStandardPrecision(Utils.parseBigDecimal(defStdPrecision) == null ? BigDecimal.ZERO
                .longValue() : Utils.parseBigDecimal(defStdPrecision).longValue());
        regUOM
            .setCostingPrecision(Utils.parseBigDecimal(defCostingPrecision) == null ? BigDecimal.ZERO
                .longValue() : Utils.parseBigDecimal(defStdPrecision).longValue());
        OBDal.getInstance().save(regUOM);
        OBDal.getInstance().flush();
        ListUOM.add(regUOM);

      }

    }

    return regUOM;

  }

  TaxCategory getExistTaxCategory(String str) {

    try {
      if ((ListTaxCategory != null) && (ListTaxCategory.size() > 0)) {

        for (TaxCategory regTaxCategory : ListTaxCategory) {

          if (regTaxCategory.getName().equals(str) == true) {
            return regTaxCategory;
          }
        }

      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;

  }

  TaxCategory getTaxCategoryM(String str, Organization regOrganization) {

    TaxCategory regTaxCategory = findDALInstance(false, TaxCategory.class, new Value(
        TaxCategory.PROPERTY_NAME, str));

    if (regTaxCategory == null) {
      regTaxCategory = getExistTaxCategory(str);

      if (regTaxCategory == null) {
        regTaxCategory = OBProvider.getInstance().get(TaxCategory.class);
        regTaxCategory.setClient(OBContext.getOBContext().getCurrentClient());
        regTaxCategory.setOrganization(regOrganization);
        regTaxCategory.setCreatedBy(OBContext.getOBContext().getUser());
        regTaxCategory.setUpdatedBy(OBContext.getOBContext().getUser());
        regTaxCategory.setName(str);
        regTaxCategory.setDescription("This TaxCategory was created from Product IDL");
        OBDal.getInstance().save(regTaxCategory);
        OBDal.getInstance().flush();
        ListTaxCategory.add(regTaxCategory);

      }

    }

    return regTaxCategory;

  }

  PriceListSchema getPriceListSchema(Organization regOrganization) {
    // Time part of the date must be set to 0
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    String priceListSchema = searchDefaultValue("Product", "PriceListSchema", null);
    if (priceListSchema == null) {
      priceListSchema = "Default Price List Schema";
    }
    PriceListSchema pListSchema = findDALInstance(false, PriceListSchema.class, new Value("name",
        priceListSchema));
    if (pListSchema == null) {
      pListSchema = OBProvider.getInstance().get(PriceListSchema.class);
      pListSchema.setActive(true);
      pListSchema.setOrganization(regOrganization);
      pListSchema.setName(priceListSchema);
      pListSchema.setValidFromDate(cal.getTime());
      OBDal.getInstance().save(pListSchema);
      OBDal.getInstance().flush();

      PriceListSchemeLine pSaleListSchemaLine = OBProvider.getInstance().get(
          PriceListSchemeLine.class);
      pSaleListSchemaLine.setActive(true);
      pSaleListSchemaLine.setOrganization(regOrganization);
      pSaleListSchemaLine.setPriceListSchema(pListSchema);
      pSaleListSchemaLine.setSequenceNumber(10L);
      pSaleListSchemaLine.setActive(true);

      pSaleListSchemaLine.setConversionRateType("S");
      pSaleListSchemaLine.setConversionDate(new java.util.Date());
      pSaleListSchemaLine.setBaseListPrice("L");
      pSaleListSchemaLine.setSurchargeListPriceAmount(BigDecimal.ZERO);
      pSaleListSchemaLine.setListPriceDiscount(BigDecimal.ZERO);
      pSaleListSchemaLine.setStandardBasePrice("S");
      pSaleListSchemaLine.setSurchargeStandardPriceAmount(BigDecimal.ZERO);
      pSaleListSchemaLine.setStandardPriceDiscount(BigDecimal.ZERO);
      pSaleListSchemaLine.setBaseLimitPrice("X");
      pSaleListSchemaLine.setSurchargePriceLimitAmount(BigDecimal.ZERO);
      pSaleListSchemaLine.setPriceLimitDiscount(BigDecimal.ZERO);
      pSaleListSchemaLine.setMinListPriceMargin(BigDecimal.ZERO);
      pSaleListSchemaLine.setMaxPriceLimitMargin(BigDecimal.ZERO);
      pSaleListSchemaLine.setListPriceRounding("C");
      pSaleListSchemaLine.setMinStandardPriceMargin(BigDecimal.ZERO);
      pSaleListSchemaLine.setMaxStandardMargin(BigDecimal.ZERO);
      pSaleListSchemaLine.setStandardPriceRounding("C");
      pSaleListSchemaLine.setLimitPriceMinMargin(BigDecimal.ZERO);
      pSaleListSchemaLine.setMaxPriceLimitMargin(BigDecimal.ZERO);
      pSaleListSchemaLine.setPriceLimitRounding("C");
      pSaleListSchemaLine.setMaxListPriceMargin(BigDecimal.ZERO);

      OBDal.getInstance().save(pSaleListSchemaLine);
      OBDal.getInstance().flush();
    }
    return pListSchema;
  }

  ProductPrice getProductPrice(PriceListVersion regPriceListVersion, Product regProduct) {

    try {

      // OBDal.getInstance().refresh(regProduct);

      /*
       * List<ProductPrice> ListProductPrice = regPriceListVersion.getPricingProductPriceList();
       * 
       * if ((ListProductPrice != null) && (ListProductPrice.size() > 0)) {
       * 
       * for (ProductPrice regProductPrice : ListProductPrice) {
       * 
       * if ((regProduct != null) && (regProductPrice.getProduct() != null)) { if
       * (regProduct.getId().equals(regProductPrice.getProduct().getId()) == true) { return
       * regProductPrice; } }
       * 
       * }
       * 
       * }
       */
      final OBCriteria<ProductPrice> prodPrice = OBDal.getInstance().createCriteria(
          ProductPrice.class);
      prodPrice.add(Restrictions.eq(ProductPrice.PROPERTY_PRICELISTVERSION, regPriceListVersion));
      prodPrice.add(Restrictions.eq(ProductPrice.PROPERTY_PRODUCT, regProduct));
      //
      for (ProductPrice regProdPrice : prodPrice.list()) {
        return regProdPrice;
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  ProductPrice getProductPriceSales(Organization regOrganization, Product regProduct,
      String strPriceSales, PriceListSchema pListSchema) {

    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);

    // String priceListSale = searchDefaultValue("Product", "PriceListSale", null);
    String priceListSale = "Default Sale Price List";// searchDefaultValue("PriceList", "name",
                                                     // "PriceListSale");
                                                     // if (priceListSale == null) {
    // priceListSale = "Default Sale Price List";
    // }
    PriceList pSaleList = findDALInstance(false, PriceList.class, new Value("name", priceListSale));
    if (pSaleList == null) {

      if (HashMapPriceListSales.containsKey(priceListSale) == true) {
        pSaleList = HashMapPriceListSales.get(priceListSale);
      } else {

        // Search Default Currency
        Currency currencyinst = findDALInstance(false, Currency.class, new Value("iSOCode",
            searchDefaultValue("Product", "Currency", null)));

        pSaleList = OBProvider.getInstance().get(PriceList.class);
        pSaleList.setActive(true);
        pSaleList.setOrganization(regOrganization);
        pSaleList.setName(priceListSale);
        if (currencyinst == null) {
          currencyinst = rowOrganization.getCurrency();
        }
        pSaleList.setCurrency(currencyinst);
        pSaleList.setSalesPriceList(Boolean.TRUE);
        OBDal.getInstance().save(pSaleList);
        OBDal.getInstance().flush();
        HashMapPriceListSales.put(priceListSale, pSaleList);
      }
    }

    // Price
    // String defPLVSale = searchDefaultValue("Product", "PriceListVersionSale", null);
    String defPLVSale = "Default Sale Price List Version";
    // searchDefaultValue("PricingPriceListVersion",
    // PriceListVersion.PROPERTY_NAME, "PriceListVersionSale");
    // if (defPLVSale == null) {
    // defPLVSale = "Default Sale Price List Version";
    // }
    PriceListVersion plvSale = findDALInstance(false, PriceListVersion.class, new Value(
        PriceListVersion.PROPERTY_NAME, defPLVSale));
    if (plvSale == null) {

      String KeyPriceListVersion = priceListSale + "_" + defPLVSale;

      if (HashMapPriceListSales.containsKey(KeyPriceListVersion) == true) {
        plvSale = HashMapPriceListSalesVersion.get(KeyPriceListVersion);
      } else {

        plvSale = OBProvider.getInstance().get(PriceListVersion.class);
        plvSale.setActive(true);
        plvSale.setOrganization(regOrganization);
        plvSale.setName(defPLVSale);
        plvSale.setPriceList(pSaleList);
        plvSale.setPriceListSchema(pListSchema);
        plvSale.setValidFromDate(cal.getTime());
        OBDal.getInstance().save(plvSale);
        OBDal.getInstance().flush();
        HashMapPriceListSalesVersion.put(KeyPriceListVersion, plvSale);
      }
    }

    ProductPrice productPrice = getProductPrice(plvSale, regProduct);

    if (productPrice == null) {
      productPrice = OBProvider.getInstance().get(ProductPrice.class);
      productPrice.setActive(true);
      productPrice.setOrganization(regOrganization);
      productPrice.setProduct(regProduct);
    }
    productPrice.setListPrice(new BigDecimal(strPriceSales));
    productPrice.setStandardPrice(new BigDecimal(strPriceSales));
    productPrice.setPriceLimit(new BigDecimal(strPriceSales));
    productPrice.setPriceListVersion(plvSale);

    OBDal.getInstance().save(productPrice);
    OBDal.getInstance().flush();

    return productPrice;
  }

  ProductPrice getProductPricePurchase(Organization regOrganization, Product regProduct,
      String strPricePurchase, PriceListSchema pListSchema) {

    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);

    // String priceListPurchase = searchDefaultValue("Product", "PriceListPurchase", null);
    String priceListPurchase = "Default Purchase Price List";// searchDefaultValue("PriceList",
                                                             // "name", "PriceListPurchase");
                                                             // if (priceListPurchase == null) {
    // priceListPurchase = "Default Purchase Price List";
    // }
    PriceList pPurchaseList = findDALInstance(false, PriceList.class, new Value("name",
        priceListPurchase));
    if (pPurchaseList == null) {
      // Search Default Currency

      if (HashMapPriceListPurchase.containsKey(priceListPurchase) == true) {

        pPurchaseList = HashMapPriceListPurchase.get(priceListPurchase);

      } else {

        Currency currencyinst = findDALInstance(false, Currency.class, new Value("iSOCode",
            searchDefaultValue("Product", "Currency", null)));
        pPurchaseList = OBProvider.getInstance().get(PriceList.class);
        pPurchaseList.setActive(true);
        pPurchaseList.setOrganization(regOrganization);
        pPurchaseList.setName(priceListPurchase);
        if (currencyinst == null) {
          currencyinst = rowOrganization.getCurrency();
        }
        pPurchaseList.setCurrency(currencyinst);
        pPurchaseList.setSalesPriceList(Boolean.FALSE);
        OBDal.getInstance().save(pPurchaseList);
        OBDal.getInstance().flush();
        HashMapPriceListPurchase.put(priceListPurchase, pPurchaseList);
      }
    }

    // Price purchase
    // String defPLVPurchase = searchDefaultValue("Product", "PriceListVersionPurchase", null);
    String defPLVPurchase = "Default Purchase Price List Version";
    // searchDefaultValue("PricingPriceListVersion", "name",
    // "PriceListVersionPurchase");
    // if (defPLVPurchase == null) {
    // defPLVPurchase = "Default Purchase Price List Version";
    // }
    PriceListVersion plvPurchase = findDALInstance(false, PriceListVersion.class, new Value(
        PriceListVersion.PROPERTY_NAME, defPLVPurchase));
    if (plvPurchase == null) {

      String KeyPriceListVersion = priceListPurchase + "_" + defPLVPurchase;

      if (HashMapPriceListPurchaseVersion.containsKey(KeyPriceListVersion) == true) {
        plvPurchase = HashMapPriceListPurchaseVersion.get(KeyPriceListVersion);
      } else {

        plvPurchase = OBProvider.getInstance().get(PriceListVersion.class);
        plvPurchase.setActive(true);
        plvPurchase.setOrganization(rowOrganization);
        plvPurchase.setName(defPLVPurchase);
        plvPurchase.setPriceList(pPurchaseList);
        plvPurchase.setPriceListSchema(pListSchema);
        plvPurchase.setValidFromDate(cal.getTime());
        OBDal.getInstance().save(plvPurchase);
        OBDal.getInstance().flush();
        HashMapPriceListPurchaseVersion.put(KeyPriceListVersion, plvPurchase);
      }
    }

    ProductPrice productPricePurchase = getProductPrice(plvPurchase, regProduct);

    if (productPricePurchase == null) {
      productPricePurchase = OBProvider.getInstance().get(ProductPrice.class);
      productPricePurchase.setProduct(regProduct);
      productPricePurchase.setActive(true);
      productPricePurchase.setOrganization(regOrganization);
    }
    productPricePurchase.setStandardPrice(Utils.parseBigDecimal(strPricePurchase));
    productPricePurchase.setPriceLimit(Utils.parseBigDecimal(strPricePurchase));
    productPricePurchase.setListPrice(Utils.parseBigDecimal(strPricePurchase));
    productPricePurchase.setPriceListVersion(plvPurchase);
    OBDal.getInstance().save(productPricePurchase);
    OBDal.getInstance().flush();

    return productPricePurchase;
  }

  ProductBOM getProductBOM(Product regProduct, Product regProductParent) {

    int NLine = 0;

    OBDal.getInstance().getSession().refresh(regProduct);

    String SearchkeyProduct = regProduct.getSearchKey();
    String SeachKeyParent = regProductParent.getSearchKey();

    try {

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS bom");
      queryExtend.append(" WHERE ");
      queryExtend.append(" (bom.product.searchKey = :SeachKeyParent)");
      OBQuery<ProductBOM> obq = OBDal.getInstance().createQuery(ProductBOM.class,
          queryExtend.toString());
      obq.setNamedParameter("SeachKeyParent", SeachKeyParent);

      if ((obq != null) && (obq.list().size() > 0)) {

        List<ProductBOM> ListProductBOM = obq.list();

        for (ProductBOM regProductBOM : ListProductBOM) {

          if (regProductBOM.getBOMProduct() != null) {

            if (regProductBOM.getBOMProduct().getSearchKey().equals(SearchkeyProduct) == true) {
              return null;
            }
          }

        }
        NLine = ListProductBOM.size();

      }

    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e);
    }

    // if ((ListProductBOM != null) && (ListProductBOM.size() > 0)) {
    //
    // for (ProductBOM regProductBOM : ListProductBOM) {
    //
    // if (regProductBOM.getBOMProduct() != null) {
    //
    // if (regProductBOM.getBOMProduct().getSearchKey().equals(SearchkeyProduct) == true) {
    // return null;
    // }
    // }
    //
    // }
    // NLine = ListProductBOM.size();
    //
    // }

    NLine = (NLine * 10) + 10;

    ProductBOM regProductBOM = OBProvider.getInstance().get(ProductBOM.class);
    regProductBOM.setClient(OBContext.getOBContext().getCurrentClient());
    regProductBOM.setOrganization(regProduct.getOrganization());
    regProductBOM.setCreatedBy(OBContext.getOBContext().getUser());
    regProductBOM.setUpdatedBy(OBContext.getOBContext().getUser());
    regProductBOM.setProduct(regProductParent);
    regProductBOM.setBOMProduct(regProduct);
    regProductBOM.setLineNo(new BigDecimal(NLine).longValue());
    regProductBOM.setDescription("This BOM was added from IDL Product");
    // OBDal.getInstance().save(regProductBOM);
    // OBDal.getInstance().flush();

    return regProductBOM;

  }

  ApprovedVendor getPurchasing(List<ApprovedVendor> ListApprovedVendor, String strVendor) {

    if ((ListApprovedVendor != null) && (ListApprovedVendor.size() > 0)) {

      for (ApprovedVendor regApprovedVendor : ListApprovedVendor) {

        if (regApprovedVendor.getBusinessPartner() != null) {
          if (regApprovedVendor.getBusinessPartner().getSearchKey().equals(strVendor) == true) {
            return regApprovedVendor;
          }
        }

      }

    }

    return null;
  }

  Brand getBrand(Organization regOrganization, String strBrand) {

    Brand regBrand = null;

    if ((strBrand != null) && (strBrand.trim().equals("") != true)) {

      regBrand = findDALInstance(false, Brand.class, new Value(Brand.PROPERTY_NAME, strBrand));

      if (regBrand == null) {
        for (Brand regBrandNew : ListBrand) {
          if (strBrand.trim().equals(regBrandNew.getName()) == true) {
            return regBrandNew;
          }
        }

        regBrand = OBProvider.getInstance().get(Brand.class);
        regBrand.setClient(OBContext.getOBContext().getCurrentClient());
        regBrand.setOrganization(regOrganization);
        regBrand.setCreatedBy(OBContext.getOBContext().getUser());
        regBrand.setUpdatedBy(OBContext.getOBContext().getUser());
        regBrand.setName(strBrand);
        regBrand.setDescription("Brand created from IDL Product");
        OBDal.getInstance().save(regBrand);
        OBDal.getInstance().flush();
        ListBrand.add(regBrand);

      }

    }
    return regBrand;

  }

  boolean existFile(String Path) {

    try {

      File fichero = new File(Path);

      if (fichero.exists()) {
        return true;
      }
    } catch (Exception e) {

    }
    return false;
  }

  byte[] getFileBytes(String strPath) {

    Path path = Paths.get(strPath);
    byte[] data = null;
    try {
      data = Files.readAllBytes(path);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return data;
  }

  String getMimeTypeImage(String strPath) {

    Path source = Paths.get(strPath);
    try {
      return (Files.probeContentType(source));
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return "";
  }

}
