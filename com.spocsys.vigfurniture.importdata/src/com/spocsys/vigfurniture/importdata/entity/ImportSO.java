package com.spocsys.vigfurniture.importdata.entity;

import java.io.FileReader;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.idl.proc.Parameter;
import org.openbravo.idl.proc.Validator;
import org.openbravo.idl.proc.Value;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.module.idljava.proc.IdlServiceJava;

import au.com.bytecode.opencsv.CSVReader;

import com.spocsys.vigfurniture.importdata.utils.CustomValidator;
import com.spocsys.vigfurniture.importdata.utils.IDLFormatUtility;
import com.spocsys.vigfurniture.importdata.utils.Utils;

public class ImportSO extends IdlServiceJava {

  private int TotalLines;
  private int ActualLine;
  private CustomValidator cValidator;
  HashMap<String, Order> ListOrder = new HashMap<String, Order>();

  @Override
  protected boolean executeImport(String filename, boolean insert) throws Exception {

    CSVReader reader = new CSVReader(new FileReader(filename), IDLFormatUtility.getDelimiter(),
        '\"', '\\', 0, false, true);

    List AllLinesList = reader.readAll();
    TotalLines = AllLinesList.size();

    String[] nextLine;

    // Check header
    nextLine = (String[]) AllLinesList.get(0);
    if (nextLine == null) {
      throw new OBException(Utility.messageBD(conn, "IDLJAVA_HEADER_MISSING", vars.getLanguage()));
    }
    Parameter[] parameters = getParameters();
    if (parameters.length > nextLine.length) {
      throw new OBException(
          Utility.messageBD(conn, "IDLJAVA_HEADER_BAD_LENGTH", vars.getLanguage()));
    }

    Validator validator;

    for (int i = 1; i < TotalLines; i++) {

      nextLine = (String[]) AllLinesList.get(i);
      ActualLine = i;

      if (nextLine.length > 1 || nextLine[0].length() > 0) {
        // It is not an empty line
        // Log Progress
        IDLFormatUtility.logProgress(ActualLine, (TotalLines - 1), this);
        // Validate types
        if (parameters.length > nextLine.length) {
          throw new OBException(Utility.messageBD(conn, "IDLJAVA_LINE_BAD_LENGTH",
              vars.getLanguage()));
        }

        validator = getValidator(getEntityName());
        cValidator = new CustomValidator(this, getEntityName());
        Object[] result = validateProcess(validator, nextLine);
        if ("0".equals(cValidator.getErrorCode())) {
          finishRecordProcess(result);
        } else {
          OBDal.getInstance().rollbackAndClose();
          // We need rollback here becouse the intention is load ALL or NOTHING
          logRecordError(cValidator.getErrorMessage(), result);
        }
      }
    }

    return true;
  }

  @Override
  protected String getEntityName() {
    // TODO Auto-generated method stub
    return "Sales Order";
  }

  @Override
  public Parameter[] getParameters() {
    return new Parameter[] { new Parameter("Organization", Parameter.STRING),
        new Parameter("Document Number", Parameter.STRING),
        new Parameter("Order Date", Parameter.DATE),
        new Parameter("Business Partner", Parameter.STRING),
        new Parameter("Warehouse", Parameter.STRING),
        new Parameter("Scheduled Delivery Date", Parameter.DATE),
        new Parameter("Payment Method", Parameter.STRING),
        new Parameter("Payment Terms", Parameter.STRING),
        new Parameter("Price List", Parameter.STRING),
        new Parameter("Order Reference Header", Parameter.STRING),
        new Parameter("Description Header", Parameter.STRING),
        new Parameter("Special Order Header", Parameter.BOOLEAN),
        new Parameter("Processed Header", Parameter.BOOLEAN),
        new Parameter("Status Header", Parameter.STRING),
        new Parameter("Company", Parameter.STRING), new Parameter("Name", Parameter.STRING),
        new Parameter("Email", Parameter.STRING), new Parameter("Telephone", Parameter.STRING),
        new Parameter("Split", Parameter.BOOLEAN),
        new Parameter("Inter company order", Parameter.BOOLEAN),
        new Parameter("All Stock In", Parameter.BOOLEAN),
        new Parameter("Manual Hold", Parameter.BOOLEAN),
        new Parameter("Stock Location Header", Parameter.STRING),
        new Parameter("Ship From Header", Parameter.STRING),
        new Parameter("Manager Approved", Parameter.BOOLEAN),
        new Parameter("Finalcial Approved", Parameter.BOOLEAN),
        new Parameter("Route", Parameter.STRING), new Parameter("Vig", Parameter.BOOLEAN),
        new Parameter("Finalcial Approval Notes", Parameter.STRING),
        new Parameter("Manager Approval Notes", Parameter.STRING),
        new Parameter("Product", Parameter.STRING),
        new Parameter("Ordered Quantity", Parameter.BIGDECIMAL),
        new Parameter("Net Unit Price", Parameter.BIGDECIMAL),
        new Parameter("Tax", Parameter.STRING),
        new Parameter("Net List Price", Parameter.BIGDECIMAL),
        new Parameter("Discount %", Parameter.BIGDECIMAL),
        new Parameter("Order Reference Line", Parameter.STRING),
        new Parameter("Description Line", Parameter.STRING),
        new Parameter("Item", Parameter.BOOLEAN),
        new Parameter("Sustitute Item", Parameter.BOOLEAN),
        new Parameter("Stock Location Line", Parameter.STRING),
        new Parameter("Ship From Line", Parameter.STRING),
        new Parameter("Special Order Line", Parameter.BOOLEAN),
        new Parameter("Soft Kit", Parameter.BOOLEAN), new Parameter("Hard Kit", Parameter.BOOLEAN),
        new Parameter("Processed Line", Parameter.BOOLEAN),
        new Parameter("In Pick List", Parameter.BOOLEAN),
        new Parameter("Discount", Parameter.BOOLEAN), new Parameter("Status", Parameter.STRING),
        new Parameter("Purchase Order", Parameter.STRING),
        new Parameter("Remote PO", Parameter.STRING) };
  }

  @Override
  protected Object[] validateProcess(Validator validator, String... values) throws Exception {
    // TODO Auto-generated method stub

    validator.checkOrganization(values[0]);// 0
    validator.checkNotNull(validator.checkString(values[1], 30), "Document Number");// 1
    validator.checkNotNull(IDLFormatUtility.validateDate(values[2]), "Order Date");// 2

    validator.checkNotNull(validator.checkString(values[3], 40), "Business Partner");// 3
    validator.checkNotNull(validator.checkString(values[4], 40), "Warehouse");// 4

    if ((values[5] != null) && (values[5].trim().equals("") != true)) {
      validator.checkNotNull(validator.checkDate(values[5]), "Scheduled Delivery Date");// 5
    }

    validator.checkNotNull(validator.checkString(values[6], 60), "Payment Method");// 6
    validator.checkNotNull(validator.checkString(values[7], 40), "Payment Terms");// 7
    validator.checkNotNull(validator.checkString(values[8], 60), "Price List");// 8
    validator.checkString(values[9], 20, "Order Reference Header");// 9
    validator.checkString(values[10], 255, "Description Header");// 10
    validator.checkBoolean(values[11], "Special Order Header");// 11
    validator.checkBoolean(values[12], "Processed Header");// 12
    validator.checkString(values[13], 60, "Status Header");// 13
    validator.checkString(values[14], 60, "Company");// 14
    validator.checkString(values[15], 60, "Name");// 15
    validator.checkString(values[16], 30, "Email");// 16
    validator.checkString(values[17], 30, "Telephone");// 17
    validator.checkBoolean(values[18], "Split");// 18
    validator.checkBoolean(values[19], "Inter company order");// 19
    validator.checkBoolean(values[20], "All Stock In");// 20
    validator.checkBoolean(values[21], "Manual Hold");// 21
    validator.checkString(values[22], 40, "Stock Location Header");// 22
    validator.checkString(values[23], 40, "Ship From Header");// 23
    validator.checkBoolean(values[24], "Manager Approved");// 24
    validator.checkBoolean(values[25], "Finalcial Approved");// 25
    validator.checkString(values[26], 60, "Route");// 26
    validator.checkBoolean(values[27], "Vig");// 27
    validator.checkString(values[28], 255, "Finalcial Approval Notes");// 28
    validator.checkString(values[29], 255, "Manager Approval Notes");// 29

    validator.checkNotNull(validator.checkString(values[30], 40), "Product");// 30
    validator.checkNotNull(validator.checkBigDecimal(values[31]), "Ordered Quantity");// 31
    validator.checkNotNull(validator.checkBigDecimal(values[32]), "Net Unit Price");// 32
    validator.checkNotNull(validator.checkString(values[33], 60), "Tax");// 33
    validator.checkNotNull(validator.checkBigDecimal(values[34]), "Net List Price");// 34
    validator.checkBigDecimal(values[35], "Discount %");// 35
    validator.checkString(values[36], 20, "Order Reference Line");// 36
    validator.checkString(values[37], 255, "Description Line");// 37

    validator.checkBoolean(values[38], "Item");// 38
    validator.checkBoolean(values[39], "Sustitute Item");// 39
    validator.checkString(values[40], 40, "Stock Location Line");// 40
    validator.checkString(values[41], 40, "Ship From Line");// 41
    validator.checkBoolean(values[42], "Special Order Line");// 42
    validator.checkBoolean(values[43], "Soft Kit");// 43
    validator.checkBoolean(values[44], "Hard Kit");// 44
    validator.checkBoolean(values[45], "Processed Line");// 45
    validator.checkBoolean(values[46], "In Pick List");// 46
    validator.checkBoolean(values[47], "Discount");// 47
    validator.checkString(values[48], 60, "Status Line");// 48
    validator.checkString(values[49], 60, "Purchase Order");// 49
    validator.checkString(values[50], 32, "Remote PO");// 50

    // ------------------------------------------
    String strOrg = values[0];
    String strBusinessPartner = values[3];
    String strWarehouse = values[4];
    String strPaymentMethod = values[6];
    String strPaymentTerms = values[7];
    String strPriceList = values[8];
    String strStockLocationHeader = values[22];
    String strShipFromHeader = values[23];
    String strProduct = values[30];
    String strTax = values[33];
    String strStockLocationLine = values[40];
    String strShipFromLine = values[41];
    String strPurchaseOrder = values[49];

    // ------------------------------------------

    BusinessPartner regBusinessPartner = findDALInstance(false, BusinessPartner.class, new Value(
        BusinessPartner.PROPERTY_SEARCHKEY, strBusinessPartner));

    Warehouse regWarehouse = findDALInstance(false, Warehouse.class, new Value(
        Warehouse.PROPERTY_SEARCHKEY, strWarehouse));

    FIN_PaymentMethod regPaymentMethod = findDALInstance(false, FIN_PaymentMethod.class, new Value(
        FIN_PaymentMethod.PROPERTY_NAME, strPaymentMethod));

    PaymentTerm regPaymentTerms = findDALInstance(false, PaymentTerm.class, new Value(
        PaymentTerm.PROPERTY_NAME, strPaymentTerms));

    PriceList regPriceList = findDALInstance(false, PriceList.class, new Value(
        PriceList.PROPERTY_NAME, strPriceList));

    Warehouse regStockLocationHeader = null;

    Warehouse regShipFromHeader = null;

    if ((strStockLocationHeader != null) && (strStockLocationHeader.trim().equals("") != true)) {
      regStockLocationHeader = findDALInstance(false, Warehouse.class, new Value(
          Warehouse.PROPERTY_SEARCHKEY, strStockLocationHeader));
    }

    if ((strShipFromHeader != null) && (strShipFromHeader.trim().equals("") != true)) {
      regShipFromHeader = findDALInstance(false, Warehouse.class, new Value(
          Warehouse.PROPERTY_SEARCHKEY, strShipFromHeader));
    }

    Product regProduct = findDALInstance(false, Product.class, new Value(
        Product.PROPERTY_SEARCHKEY, strProduct));

    TaxRate regTax = findDALInstance(false, TaxRate.class, new Value(TaxRate.PROPERTY_NAME, strTax));

    Warehouse regStockLocationLine = null;

    Warehouse regShipFromLine = null;

    if ((strStockLocationLine != null) && (strStockLocationLine.trim().equals("") != true)) {
      regStockLocationLine = findDALInstance(false, Warehouse.class, new Value(
          Warehouse.PROPERTY_SEARCHKEY, strStockLocationLine));
    }

    if ((strShipFromLine != null) && (strShipFromLine.trim().equals("") != true)) {
      regShipFromLine = findDALInstance(false, Warehouse.class, new Value(
          Warehouse.PROPERTY_SEARCHKEY, strShipFromLine));
    }

    if ((strPurchaseOrder != null) && (strPurchaseOrder.trim().equals("") != true)) {

      OBCriteria<Order> ListPurchaseOrder = OBDal.getInstance().createCriteria(Order.class);
      ListPurchaseOrder.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, false));
      ListPurchaseOrder.add(Restrictions.eq(Order.PROPERTY_DOCUMENTNO, strPurchaseOrder));
      Order regPurchaseOrder = (Order) ListPurchaseOrder.uniqueResult();

      if (regPurchaseOrder == null) {

        cValidator.setErrorMessage("Purchase Order doesn't exist:  " + strPurchaseOrder);
      }

    }

    // -------------------------------------------

    Organization regOrganization = Utils.getOrgBySearchKey(strOrg);

    if (regOrganization == null) {
      cValidator.setErrorMessage("Organization doesn't exist:  " + strOrg);
    } else {
      Currency regCurrency = Utils.getCurrency(regOrganization);

      if (regCurrency == null) {
        cValidator.setErrorMessage("Currency not found for Organization:  " + strOrg);
      }

    }

    if (regBusinessPartner == null) {
      cValidator.setErrorMessage("Business Partner doesn't exist:  " + strBusinessPartner);
    } else {

      if (regBusinessPartner.getBusinessPartnerLocationList().size() <= 0) {
        cValidator
            .setErrorMessage("Business Partner doesn't have location:  " + strBusinessPartner);
      }
    }

    if (regWarehouse == null) {
      cValidator.setErrorMessage("Warehouse doesn't exist:  " + strWarehouse);
    }

    if (regPaymentMethod == null) {
      cValidator.setErrorMessage("Payment Method doesn't exist:  " + strPaymentMethod);
    }

    if (regPaymentTerms == null) {
      cValidator.setErrorMessage("Payment Terms doesn't exist:  " + strPaymentTerms);
    }

    if (regPriceList == null) {
      cValidator.setErrorMessage("Price List doesn't exist:  " + strPriceList);
    }

    if ((strStockLocationHeader != null) && (strStockLocationHeader.trim().equals("") != true)
        && (regStockLocationHeader == null)) {
      cValidator.setErrorMessage("Stock Location Header doesn't exist:  " + strStockLocationHeader);
    }

    if ((strShipFromHeader != null) && (strShipFromHeader.trim().equals("") != true)
        && (regShipFromHeader == null)) {
      cValidator.setErrorMessage("Ship From Header doesn't exist:  " + strShipFromHeader);
    }

    if (regProduct == null) {
      cValidator.setErrorMessage("Product doesn't exist:  " + strProduct);
    }

    if (regTax == null) {
      cValidator.setErrorMessage("Tax doesn't exist:  " + strTax);
    }

    if ((strStockLocationLine != null) && (strStockLocationLine.trim().equals("") != true)
        && (regStockLocationLine == null)) {
      cValidator.setErrorMessage("Stock Location Line doesn't exist:  " + strStockLocationLine);
    }

    if ((strShipFromLine != null) && (strShipFromLine.trim().equals("") != true)
        && (regShipFromLine == null)) {
      cValidator.setErrorMessage("Ship From Line doesn't exist:  " + strShipFromLine);
    }

    return values;
  }

  @Override
  protected BaseOBObject internalProcess(Object... values) throws Exception {
    // TODO Auto-generated method stub
    return create((String) values[0], (String) values[1], (String) values[2], (String) values[3],
        (String) values[4], (String) values[5], (String) values[6], (String) values[7],
        (String) values[8], (String) values[9], (String) values[10], (String) values[11],
        (String) values[12], (String) values[13], (String) values[14], (String) values[15],
        (String) values[16], (String) values[17], (String) values[18], (String) values[19],
        (String) values[20], (String) values[21], (String) values[22], (String) values[23],
        (String) values[24], (String) values[25], (String) values[26], (String) values[27],
        (String) values[28], (String) values[29], (String) values[30], (String) values[31],
        (String) values[32], (String) values[33], (String) values[34], (String) values[35],
        (String) values[36], (String) values[37], (String) values[38], (String) values[39],
        (String) values[40], (String) values[41], (String) values[42], (String) values[43],
        (String) values[44], (String) values[45], (String) values[46], (String) values[47],
        (String) values[48], (String) values[49], (String) values[50]);

  }

  public BaseOBObject create(final String strOrganization, final String strDocumentNumber,
      final String strOrderDate, final String strBusinessPartner, final String strWarehouse,
      final String strScheduledDeliveryDate, final String strPaymentMethod,
      final String strPaymentTerms, final String strPriceList,
      final String strOrderReferenceHeader, final String strDescriptionHeader,
      final String strSpecialOrderHeader, final String strProcessedHeader,
      final String strStatusHeader, final String strCompany, final String strName,
      final String strEmail, final String strTelephone, final String strSplit,
      final String strIntercompany, final String strAllStockIn, final String strManualHold,
      final String strStockLocationHeader, final String strShipFromHeader,
      final String strManagerApproved, final String strFinancialApproved, final String strRoute,
      final String strVig, final String strFinancialApprovedNotes,
      final String strManagerApprovalNotes, final String strProduct,
      final String strOrderedQuantity, final String strNetUnitPrice, final String strTax,
      final String strNetListPrice, final String strDiscount, final String strOrderReferenceLine,
      final String strDescriptionLine, final String strItem, final String strSustitute,
      final String strStockLocationLine, final String strShipFromLine,
      final String strSpecialOrderLine, final String strSoftkit, final String strHardkit,
      final String strProcessedLine, final String strInPickList, final String strDiscountBoolean,
      final String strStatusLine, final String strPurchaseOrder, final String strRemotePO)
      throws Exception {

    Organization regOrganization = Utils.getOrgBySearchKey(strOrganization);
    Date regOrderDate = IDLFormatUtility.getDate(strOrderDate);

    BusinessPartner regBusinessPartner = findDALInstance(false, BusinessPartner.class, new Value(
        BusinessPartner.PROPERTY_SEARCHKEY, strBusinessPartner));
    Warehouse regWarehouse = findDALInstance(false, Warehouse.class, new Value(
        Warehouse.PROPERTY_SEARCHKEY, strWarehouse));
    Date regScheduledDeliveryDate = regOrderDate;

    if ((strScheduledDeliveryDate != null) && (strScheduledDeliveryDate.equals("") != true)) {
      regScheduledDeliveryDate = IDLFormatUtility.getDate(strScheduledDeliveryDate);
    }

    FIN_PaymentMethod regPaymentMethod = findDALInstance(false, FIN_PaymentMethod.class, new Value(
        FIN_PaymentMethod.PROPERTY_NAME, strPaymentMethod));
    PaymentTerm regPaymentTerms = findDALInstance(false, PaymentTerm.class, new Value(
        PaymentTerm.PROPERTY_NAME, strPaymentTerms));
    PriceList regPriceList = findDALInstance(false, PriceList.class, new Value(
        PriceList.PROPERTY_NAME, strPriceList));

    boolean SpecialOrderHeader = Parameter.BOOLEAN.parse(strSpecialOrderHeader);
    boolean ProcessedHeader = Parameter.BOOLEAN.parse(strProcessedHeader);
    String StatusHeader = Utils.getStatusHeader(strStatusHeader);
    // strCompany
    // strName
    // strEmail
    // strTelephone
    boolean Split = Parameter.BOOLEAN.parse(strSplit);
    boolean Intercompany = Parameter.BOOLEAN.parse(strIntercompany);
    boolean AllStockIn = Parameter.BOOLEAN.parse(strAllStockIn);
    boolean ManualHold = Parameter.BOOLEAN.parse(strManualHold);
    Warehouse StockLocationHeader = null;

    if ((strStockLocationHeader != null) && (strStockLocationHeader.trim().equals("") != true)) {
      StockLocationHeader = findDALInstance(false, Warehouse.class, new Value(
          Warehouse.PROPERTY_SEARCHKEY, strStockLocationHeader));
    }

    Warehouse ShipFromHeader = null;

    if ((strShipFromHeader != null) && (strShipFromHeader.trim().equals("") != true)) {
      ShipFromHeader = findDALInstance(false, Warehouse.class, new Value(
          Warehouse.PROPERTY_SEARCHKEY, strShipFromHeader));
    }

    boolean ManagerApproved = Parameter.BOOLEAN.parse(strManagerApproved);
    boolean FinancialApproved = Parameter.BOOLEAN.parse(strFinancialApproved);
    // strRoute
    boolean Vig = Parameter.BOOLEAN.parse(strVig);
    // strFinancialApprovedNotes,
    // strManagerApprovalNotes

    Product regProduct = findDALInstance(false, Product.class, new Value(
        Product.PROPERTY_SEARCHKEY, strProduct));
    BigDecimal regOrderedQuantity = Parameter.BIGDECIMAL.parse(strOrderedQuantity);
    BigDecimal regNetUnitPrice = Parameter.BIGDECIMAL.parse(strNetUnitPrice);
    TaxRate regTax = findDALInstance(false, TaxRate.class, new Value(TaxRate.PROPERTY_NAME, strTax));
    BigDecimal regNetListPrice = Parameter.BIGDECIMAL.parse(strNetListPrice);
    BigDecimal regDiscount = Parameter.BIGDECIMAL.parse(strDiscount);

    // ---
    boolean Item = Parameter.BOOLEAN.parse(strItem);
    boolean Sustitute = Parameter.BOOLEAN.parse(strSustitute);

    Warehouse StockLocationLine = null;

    if ((strStockLocationLine != null) && (strStockLocationLine.trim().equals("") != true)) {
      StockLocationLine = findDALInstance(false, Warehouse.class, new Value(
          Warehouse.PROPERTY_SEARCHKEY, strStockLocationLine));
    }

    Warehouse ShipFromLine = null;

    if ((strShipFromLine != null) && (strShipFromLine.trim().equals("") != true)) {
      ShipFromLine = findDALInstance(false, Warehouse.class, new Value(
          Warehouse.PROPERTY_SEARCHKEY, strShipFromLine));
    }

    boolean SpecialOrderLine = Parameter.BOOLEAN.parse(strSpecialOrderLine);
    boolean Softkit = Parameter.BOOLEAN.parse(strSoftkit);
    boolean Hardkit = Parameter.BOOLEAN.parse(strHardkit);
    boolean ProcessedLine = Parameter.BOOLEAN.parse(strProcessedLine);
    boolean InPickList = Parameter.BOOLEAN.parse(strInPickList);
    boolean DiscountBoolean = Parameter.BOOLEAN.parse(strDiscountBoolean);
    String StatusLine = Utils.getStatusLine(strStatusLine);

    // ---

    // ---------------------------------------------------------------
    OrderLine regOrderLine = OBProvider.getInstance().get(OrderLine.class);

    regOrderLine.setClient(OBContext.getOBContext().getCurrentClient());
    regOrderLine.setOrganization(regOrganization);
    regOrderLine.setCreatedBy(OBContext.getOBContext().getUser());
    regOrderLine.setUpdatedBy(OBContext.getOBContext().getUser());
    regOrderLine.setOrderDate(regOrderDate);
    regOrderLine.setDescription(strDescriptionLine);
    regOrderLine.setProduct(regProduct);
    regOrderLine.setWarehouse(regWarehouse);
    regOrderLine.setUOM(regProduct.getUOM());
    regOrderLine.setCurrency(Utils.getCurrency(regOrganization));
    regOrderLine.setUnitPrice(regNetUnitPrice);
    regOrderLine.setListPrice(regNetListPrice);
    regOrderLine.setDiscount(regDiscount);
    regOrderLine.setTax(regTax);
    regOrderLine.setOrderedQuantity(regOrderedQuantity);
    regOrderLine.setSvfsvPoreference(strOrderReferenceLine);

    // ---
    regOrderLine.setLineNetAmount(BigDecimal.ZERO);
    regOrderLine.setPriceLimit(BigDecimal.ZERO);
    regOrderLine.setFreightAmount(BigDecimal.ZERO);
    regOrderLine.setStandardPrice(BigDecimal.ZERO);
    regOrderLine.setDirectShipment(false);
    regOrderLine.setReservedQuantity(BigDecimal.ZERO);
    regOrderLine.setDeliveredQuantity(BigDecimal.ZERO);
    regOrderLine.setInvoicedQuantity(BigDecimal.ZERO);
    regOrderLine.setDescriptionOnly(false);

    // ---

    regOrderLine.setSvfpaItem(Item);
    regOrderLine.setSvfpaSustituteItem(Sustitute);
    regOrderLine.setSvfpaStockLocation(StockLocationLine);
    regOrderLine.setSvfpaShipFrom(ShipFromLine);
    regOrderLine.setSvfpaSpecialOrder(SpecialOrderLine);
    regOrderLine.setSvfpaSoftKit(Softkit);
    regOrderLine.setSvfpaHardKit(Hardkit);
    regOrderLine.setSvfpaProcessed(ProcessedLine);
    regOrderLine.setSvfpaInPickList(InPickList);
    regOrderLine.setSvfpaDiscount(DiscountBoolean);
    regOrderLine.setSvfpaStatus(StatusLine);

    if ((strPurchaseOrder != null) && (strPurchaseOrder.trim().equals("") != true)) {

      OBCriteria<Order> ListPurchaseOrder = OBDal.getInstance().createCriteria(Order.class);
      ListPurchaseOrder.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, false));
      ListPurchaseOrder.add(Restrictions.eq(Order.PROPERTY_DOCUMENTNO, strPurchaseOrder));
      Order regPurchaseOrder = (Order) ListPurchaseOrder.uniqueResult();

      if (regPurchaseOrder != null) {
        regOrderLine.setSvfpaPurchaseorder(regPurchaseOrder);
      }

    }

    // ---

    Order regOrder = getOrder(regOrganization, strDocumentNumber, regOrderDate, regBusinessPartner,
        regWarehouse, regScheduledDeliveryDate, regPaymentMethod, regPaymentTerms, regPriceList,
        strOrderReferenceHeader, strDescriptionHeader, SpecialOrderHeader, ProcessedHeader,
        StatusHeader, strCompany, strName, strEmail, strTelephone, Split, Intercompany, AllStockIn,
        ManualHold, StockLocationHeader, ShipFromHeader, ManagerApproved, FinancialApproved,
        strRoute, Vig, strFinancialApprovedNotes, strManagerApprovalNotes);

    int LineNro = regOrder.getOrderLineList() != null ? regOrder.getOrderLineList().size() : 0;
    LineNro = LineNro + 10;
    Long LineNroNew = Long.parseLong(String.valueOf(LineNro));
    regOrderLine.setLineNo(LineNroNew);
    regOrder.getOrderLineList().add(regOrderLine);
    regOrderLine.setSalesOrder(regOrder);
    regOrderLine.setSvfpaRemotepo(strRemotePO != null ? strRemotePO.trim() : strRemotePO);
    OBDal.getInstance().save(regOrder);
    OBDal.getInstance().save(regOrderLine);

    OBDal.getInstance().flush();

    // ---------------------------------------------------------------

    return regOrderLine;

  }

  Order getOrder(Organization regOrganization, String DocumentNumber, Date OrderDate,
      BusinessPartner regBusinessPartner, Warehouse regWarehouse, Date ScheduledDeliveryDate,
      FIN_PaymentMethod regPaymentMethod, PaymentTerm regPaymentTerms, PriceList regPriceList,
      String CustomerOrderReferenceHeader, String DescriptionHeader, boolean SpecialOrderHeader,
      boolean ProcessedHeader, String StatusHeader, String strCompany, String strName,
      String strEmail, String strTelephone, boolean Split, boolean Intercompany,
      boolean AllStockIn, boolean ManualHold, Warehouse StockLocationHeader,
      Warehouse ShipFromHeader, boolean ManagerApproved, boolean FinancialApproved,
      String strRoute, boolean Vig, String strFinancialApprovedNotes, String strManagerApprovalNotes) {

    String ID = regOrganization.getId();
    ID += DocumentNumber;
    ID += OrderDate.toString();
    ID += regBusinessPartner.getId();
    ID += regWarehouse.getId();

    if (ListOrder.get(ID) != null) {
      return ListOrder.get(ID);
    } else {
      Order regOrder = OBProvider.getInstance().get(Order.class);

      regOrder.setClient(OBContext.getOBContext().getCurrentClient());
      regOrder.setOrganization(regOrganization);
      regOrder.setCreatedBy(OBContext.getOBContext().getUser());
      regOrder.setUpdatedBy(OBContext.getOBContext().getUser());
      regOrder.setSalesTransaction(true);
      regOrder.setDocumentNo(DocumentNumber);
      regOrder.setDocumentStatus("DR");
      regOrder.setDocumentAction("CO");
      DocumentType regDocumentType = Utils.getDocumentType("SOO", true, false);

      if (regDocumentType == null) {
        throw new OBException("Document Type not found");
      }
      regOrder.setDocumentType(regDocumentType);

      DocumentType regDoctypeTarget = Utils.getDocumentType("SOO", true, false);

      if (regDoctypeTarget == null) {
        throw new OBException("Document Type Target not found");
      }

      regOrder.setTransactionDocument(regDoctypeTarget);
      regOrder.setDocumentNo(DocumentNumber);
      regOrder.setDescription(DescriptionHeader);
      regOrder.setOrderDate(OrderDate);
      regOrder.setScheduledDeliveryDate(ScheduledDeliveryDate);
      regOrder.setAccountingDate(OrderDate);
      regOrder.setBusinessPartner(regBusinessPartner);
      regOrder.setPartnerAddress(regBusinessPartner.getBusinessPartnerLocationList().get(0));
      regOrder.setOrderReference(CustomerOrderReferenceHeader);
      regOrder.setCurrency(Utils.getCurrency(regOrganization));
      regOrder.setFormOfPayment("P");
      regOrder.setInvoiceTerms("I");
      regOrder.setDeliveryTerms("A");
      regOrder.setFreightCostRule("I");
      regOrder.setDeliveryMethod("P");
      regOrder.setPriority("5");
      regOrder.setWarehouse(regWarehouse);
      regOrder.setPriceList(regPriceList);
      regOrder.setPaymentMethod(regPaymentMethod);
      regOrder.setPaymentTerms(regPaymentTerms);

      // ---
      regOrder.setSvfpaSpecialOrder(SpecialOrderHeader);
      regOrder.setSvfpaProcessed(ProcessedHeader);
      regOrder.setSvfpaStatus(StatusHeader);
      regOrder.setSvfpaCompany(strCompany);
      regOrder.setSvfpaName(strName);
      regOrder.setSvfpaEmail(strEmail);
      regOrder.setSvfpaTelephone(strTelephone);
      regOrder.setSvfpaSplit(Split);
      regOrder.setSvfpaInterCompany(Intercompany);
      regOrder.setSvfpaAllStockIn(AllStockIn);
      regOrder.setSvfpaManualHold(ManualHold);
      regOrder.setSvfpaStockLocation(StockLocationHeader);
      regOrder.setSvfpaShipFrom(ShipFromHeader);
      regOrder.setSvfpaManagerApproved(ManagerApproved);
      regOrder.setSvfpaFinancialApproved(FinancialApproved);
      regOrder.setSvfpaRoute(strRoute);
      regOrder.setSvfpaVip(Vig);
      regOrder.setSvfpaFinancialNote(strFinancialApprovedNotes);
      regOrder.setSvfpaManagerNote(strManagerApprovalNotes);
      // ----

      ListOrder.put(ID, regOrder);
      OBDal.getInstance().save(regOrder);
      return regOrder;
    }

    // return null;
  }
}
