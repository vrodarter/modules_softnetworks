package com.spocsys.vigfurniture.importdata.intercompany.salesOrder;

import java.io.FileReader;
import java.math.BigDecimal;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.idl.proc.Parameter;
import org.openbravo.idl.proc.Validator;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.module.idljava.proc.IdlServiceJava;

import com.spocsys.vigfurniture.importdata.utils.CustomValidator;
import com.spocsys.vigfurniture.importdata.utils.IDLFormatUtility;

import au.com.bytecode.opencsv.CSVReader;

public class InterCompanySalesOrder extends IdlServiceJava {

  private int TotalLines;
  private int ActualLine;
  private CustomValidator cValidator;

  @Override
  protected boolean executeImport(String filename, boolean insert) throws Exception {

    CSVReader reader = new CSVReader(new FileReader(filename), IDLFormatUtility.getDelimiter(),
        '\"', '\\', 0, false, true);

    List AllLinesList = reader.readAll();
    TotalLines = AllLinesList.size();

    String[] nextLine;

    // Check header
    nextLine = (String[]) AllLinesList.get(0);
    if (nextLine == null) {
      throw new OBException(Utility.messageBD(conn, "IDLJAVA_HEADER_MISSING", vars.getLanguage()));
    }
    Parameter[] parameters = getParameters();
    if (parameters.length > nextLine.length) {
      throw new OBException(
          Utility.messageBD(conn, "IDLJAVA_HEADER_BAD_LENGTH", vars.getLanguage()));
    }

    Validator validator;

    for (int i = 1; i < TotalLines; i++) {

      nextLine = (String[]) AllLinesList.get(i);
      ActualLine = i;

      if (nextLine.length > 1 || nextLine[0].length() > 0) {
        // It is not an empty line
        // Log Progress
        IDLFormatUtility.logProgress(ActualLine, (TotalLines - 1), this);
        // Validate types
        if (parameters.length > nextLine.length) {
          throw new OBException(
              Utility.messageBD(conn, "IDLJAVA_LINE_BAD_LENGTH", vars.getLanguage()));
        }

        validator = getValidator(getEntityName());
        cValidator = new CustomValidator(this, getEntityName());
        Object[] result = validateProcess(validator, nextLine);
        if ("0".equals(cValidator.getErrorCode())) {
          finishRecordProcess(result);
        } else {
          OBDal.getInstance().rollbackAndClose();
          // We need rollback here becouse the intention is load ALL or NOTHING
          logRecordError(cValidator.getErrorMessage(), result);
        }
      }
    }

    return true;
  }

  @Override
  protected String getEntityName() {
    // TODO Auto-generated method stub
    return "InterCompany Sales Order";
  }// getEntityName

  @Override
  public Parameter[] getParameters() {
    return new Parameter[] { new Parameter("Sales Order", Parameter.STRING),
        new Parameter("Product Name", Parameter.STRING),
        new Parameter("Quantity Product", Parameter.BIGDECIMAL),
        new Parameter("Purchase Order", Parameter.STRING) };
  }// getParameters

  @Override
  protected Object[] validateProcess(Validator validator, String... values) throws Exception {
    // TODO Auto-generated method stub

    validator.checkNotNull(validator.checkString(values[0], 30), "Sales Order");
    validator.checkNotNull(validator.checkString(values[1], 60), "Product Name");
    validator.checkBigDecimal(values[2], "Quantity Product");
    validator.checkNotNull(validator.checkString(values[3], 30), "Purchase Order");
    //
    String strSalesOrder = values[0];
    String strProductName = values[1];
    String strPurchaseOrder = values[3];
    //
    if (strSalesOrder == null) {
      cValidator.setErrorMessage("Sales Order is null" + strSalesOrder);
    }

    return values;
  }// validateProcess

  @Override
  protected BaseOBObject internalProcess(Object... values) throws Exception {
    //
    return validate((String) values[0], (String) values[1], (String) values[2], (String) values[3]);

  }

  public BaseOBObject validate(final String strSalesOrder, final String strProductName,
      final String strQtyProduct, final String strPurchaseOrder) throws Exception {
    //
    final OBCriteria<Order> orderList = OBDal.getInstance().createCriteria(Order.class);
    OrderLine rOrderLine = null;
    // IsSOTrx = 'Y' and em_svfpa_processed = 'N'
    orderList.add(Restrictions.eq(Order.PROPERTY_DOCUMENTNO, strSalesOrder));
    orderList.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true));
    //
    if (orderList.list().size() > 1) {
      throw new OBException("Exists more then one Order");
    } else if (orderList.list().size() == 0) {
      throw new OBException("Order No Exists");
    }
    System.out.println("Proccess validate Sales Order " + orderList.list().size());
    for (Order order : orderList.list()) {
      String strIdProduct = getIdProductName(strProductName.replace(",", " "));
      BigDecimal qtyProduct = Parameter.BIGDECIMAL.parse(strQtyProduct);
      final OBCriteria<OrderLine> orderLineList = OBDal.getInstance()
          .createCriteria(OrderLine.class);
      orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, order));
      orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_PRODUCT + ".id", strIdProduct));
      // orderLineList.add(Restrictions.sqlRestriction(OrderLine.PROPERTY_PRODUCT+".id"));
      for (OrderLine regOrderLine : orderLineList.list()) {
        regOrderLine.set(OrderLine.PROPERTY_ID, regOrderLine.getId());
        regOrderLine.set(OrderLine.PROPERTY_SVFPAPURCHASEORDER, getPurchaseOrder(strPurchaseOrder));
        regOrderLine.set(OrderLine.PROPERTY_SVFPAORDEREDQTY, qtyProduct.longValue());
        System.out.println("Update");
        OBDal.getInstance().save(regOrderLine);
        OBDal.getInstance().flush();
        rOrderLine = regOrderLine;
      }
      // System.out.println("Proccess validate Sales Order");
    }
    //
    return rOrderLine;
  }// validate

  private String getIdProductName(String strProductName) {
    String idProduct = null;
    final OBCriteria<Product> productList = OBDal.getInstance().createCriteria(Product.class);
    //
    // productList.add(Restrictions.eq(Product.PROPERTY_NAME, strProductName));
    productList.add(Restrictions.sqlRestriction(
        "replace(" + Product.PROPERTY_NAME + ", ',', ' ') = '" + strProductName + "'"));
    productList.add(Restrictions.eq(Product.PROPERTY_ACTIVE, true));
    for (Product regProduct : productList.list()) {
      idProduct = regProduct.getId();
    }
    //
    return idProduct;
  }// getIdProductName

  private Order getPurchaseOrder(String strDocumentNo) {
    final OBCriteria<Order> orderList = OBDal.getInstance().createCriteria(Order.class);
    orderList.add(Restrictions.eq(Order.PROPERTY_DOCUMENTNO, strDocumentNo));
    orderList.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, false));
    Order objOrder = null;
    //
    for (Order regOrder : orderList.list()) {
      objOrder = regOrder;
    }
    //
    return objOrder;
  }// getPurchaseOrder

}// InterCompanySalesOrder
