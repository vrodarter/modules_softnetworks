package com.spocsys.vigfurniture.importdata.utils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.service.db.DalConnectionProvider;

import com.spocsys.vigfurniture.importdata.IDLFormatDefaults;

public class IDLFormatUtility {
  private static Logger Log = Logger.getLogger(IDLFormatUtility.class);

  public static Timestamp getDate(String strDate) throws Exception {
    if (strDate == null || strDate.trim().equals(""))
      return null;
    // Log.info("Received Date: " + strDate);
    // Get a valid timestamp
    Timestamp date = Timestamp.valueOf("2000-01-01 00:00:00");

    // Obtain the default format
    String expectedDateFormat = getDefaultFormats().getDateFormat();

    // Get the splitter
    String splitter = getSplitter(strDate.trim());

    String[] dateArray = strDate.split(splitter);

    // Log.info("dateArray.Length: " + dateArray.length + " Splitter: " + splitter);

    if (dateArray.length != 3) {
      throw new OBException(Utility.messageBD(new DalConnectionProvider(), "SVFIMD_INVALID_DATE",
          OBContext.getOBContext().getLanguage().getLanguage())
          + strDate
          + " Expected format: "
          + expectedDateFormat);
    }

    String YYYY = "2000", MM = "01", DD = "01";

    if (expectedDateFormat.equals("YYYY_MM_DD")) {
      YYYY = dateArray[0];
      MM = dateArray[1];
      DD = dateArray[2];
    }

    if (expectedDateFormat.equals("MM_DD_YYYY")) {
      YYYY = dateArray[2];
      MM = dateArray[0];
      DD = dateArray[1];
    }

    if (expectedDateFormat.equals("DD_MM_YYYY")) {
      YYYY = dateArray[2];
      MM = dateArray[1];
      DD = dateArray[0];
    }

    // Validate Date
    try {
      if (Integer.parseInt(YYYY) <= 1900 || Integer.parseInt(YYYY) > 2100) {
        throw new OBException("Invalid Year value: " + YYYY);
      }
      if (Integer.parseInt(MM) <= 0 || Integer.parseInt(MM) > 12) {
        throw new OBException("Invalid Month value: " + MM);
      }
      if (Integer.parseInt(DD) <= 0 || Integer.parseInt(DD) > 31) {
        throw new OBException("Invalid Day value: " + DD);
      }

    } catch (Exception e) {
      throw new OBException(e.getMessage());
    }

    try {
      date = Timestamp.valueOf(YYYY + "-" + MM + "-" + DD + " 00:00:00");
    } catch (Exception e) {
      throw new OBException(Utility.messageBD(new DalConnectionProvider(), "SVFIMD_INVALID_DATE",
          OBContext.getOBContext().getLanguage().getLanguage())
          + strDate
          + " Expected format: "
          + expectedDateFormat);
    }

    return date;
  }

  public static Timestamp getTime(String strTime) {
    if (strTime == null || strTime.trim().equals(""))
      return null;

    // Log.info("Received Time: " + strTime);

    // Get a valid timestamp
    Timestamp time = Timestamp.valueOf("2000-01-01 00:00:00");

    // Obtain the default format
    String expectedTimeFormat = getDefaultFormats().getTimeFormat();

    String splitter = "";

    // Get the splitter
    if (strTime.substring(1, 2).contains(":") || strTime.substring(1, 2).contains(".")) {
      splitter = strTime.substring(1, 2);
    } else {
      splitter = strTime.substring(2, 3);
    }
    String[] timeArray = strTime.split(splitter);

    if (timeArray.length != 3 && timeArray.length != 2) {
      throw new OBException(Utility.messageBD(new DalConnectionProvider(), "SVFIMD_INVALID_DATE",
          OBContext.getOBContext().getLanguage().getLanguage())
          + strTime
          + " Expected format: "
          + expectedTimeFormat);
    }

    String hh = "00", mm = "00", ss = "00";

    try {

      if (expectedTimeFormat.equals("HH_MM_SSAM") || expectedTimeFormat.equals("HH_MM_SSA.M.")) {
        hh = timeArray[0];
        mm = timeArray[1];
        ss = timeArray[2].substring(0, 2);

        if (timeArray[2].trim().toUpperCase().endsWith("A.M.")
            || timeArray[2].trim().toUpperCase().endsWith("AM")) {
          if (hh.equals("12")) {
            hh = "00";
          }
        }

        if (timeArray[2].trim().toUpperCase().endsWith("P.M.")
            || timeArray[2].trim().toUpperCase().endsWith("PM")) {
          if (!hh.equals("12")) {
            hh = String.valueOf((Integer.parseInt(hh) + 12));
          }
        }

      }

      if (expectedTimeFormat.equals("HH_MMAM") || expectedTimeFormat.equals("HH_MMA.M.")) {
        hh = timeArray[0];
        mm = timeArray[1].substring(0, 2);

        if (timeArray[1].trim().toUpperCase().endsWith("A.M.")
            || timeArray[1].trim().toUpperCase().endsWith("AM")) {
          if (hh.equals("12")) {
            hh = "00";
          }
        }

        if (timeArray[1].trim().toUpperCase().endsWith("P.M.")
            || timeArray[1].trim().toUpperCase().endsWith("PM")) {
          if (!hh.equals("12")) {
            hh = String.valueOf((Integer.parseInt(hh) + 12));
          }
        }

      }

      if (expectedTimeFormat.equals("HH_MM_SS")) {
        hh = timeArray[0];
        mm = timeArray[1];
        ss = timeArray[2];
      }

      if (expectedTimeFormat.equals("HH_MM")) {
        hh = timeArray[0];
        mm = timeArray[1];
      }

      // Log.info("2000-01-01 " + hh + ":" + mm + ":" + ss);
      time = Timestamp.valueOf("2000-01-01 " + hh + ":" + mm + ":" + ss);
    } catch (Exception e) {
      throw new OBException(Utility.messageBD(new DalConnectionProvider(), "SVFIMD_INVALID_DATE",
          OBContext.getOBContext().getLanguage().getLanguage())
          + strTime
          + " Expected format: "
          + expectedTimeFormat);
    }

    return time;
  }

  public static IDLFormatDefaults getDefaultFormats() {
    IDLFormatDefaults defaults = OBProvider.getInstance().get(IDLFormatDefaults.class);

    OBCriteria<IDLFormatDefaults> defaultsCriteria = OBDal.getInstance().createCriteria(
        IDLFormatDefaults.class);

    if (defaultsCriteria.count() > 0) {
      defaults = defaultsCriteria.list().get(0);
    } else {
      throw new OBException(Utility.messageBD(new DalConnectionProvider(),
          "SVFIMD_DEFAULT_FORMATS_NOT_EXIST", OBContext.getOBContext().getLanguage().getLanguage()));
    }

    return defaults;
  }

  public static char getDelimiter() {
    char delimiter = ',';

    String config = getDefaultFormats().getDelimiter();

    if ("COMMA".equals(config))
      delimiter = ',';

    if ("TAB".equals(config))
      delimiter = '\t';

    if ("VERTICAL".equals(config))
      delimiter = '|';

    if ("CUSTOM".equals(config)) {
      if (getDefaultFormats().getCustomDelimiter().length() == 1) {
        delimiter = getDefaultFormats().getCustomDelimiter().charAt(0);
      } else {
        System.out
            .println("The delimiter has been configured as Custom, but the actual delimiter is not valid, using default delimiter (COMMA)");
      }
    }

    return delimiter;
  }

  public static boolean logProgress() {
    boolean logProgress = false;

    try {
      logProgress = getDefaultFormats().isLOGProgress();
    } catch (Exception e) {
      // Do nothing just keep falses
    }

    return logProgress;
  }

  public static String validateTime(String strTime) {
    if (strTime != null && !strTime.equals(""))
      getTime(strTime);
    else
      return null;
    return strTime;
  }

  public static String validateTime(String strTime, String column) throws Exception {
    try {
      if (strTime != null && !strTime.trim().equals(""))
        getTime(strTime);
      else
        return null;
    } catch (Exception e) {
      throw new OBException("Error on [" + column + "]: " + e.getMessage());
    }
    return strTime;
  }

  public static String validateDate(String strDate) throws Exception {
    if (strDate != null && !strDate.trim().equals("")) {
      getDate(strDate);
    } else {
      return null;
    }
    return strDate;
  }

  public static String validateDate(String strDate, String column) throws Exception {
    try {
      if (strDate != null && !strDate.trim().equals(""))
        getDate(strDate);
      else
        return null;
    } catch (Exception e) {
      throw new OBException("Error on [" + column + "]: " + e.getMessage());
    }
    return strDate;
  }

  public static long validateLong(long number, long minValue, long maxValue, boolean throwExeption) {
    String msg = String.format(Utility
        .messageBD(new DalConnectionProvider(false), "SVFIMD_NUMBER_OUT_OF_RANGE",
            OBContext.getOBContext().getLanguage().getLanguage())
        .replaceAll("%0", String.valueOf(number)).replaceAll("%1", String.valueOf(minValue))
        .replaceAll("%2", String.valueOf(maxValue)));
    if (number < minValue || number > maxValue) {
      if (throwExeption) {
        throw new OBException(msg);
      } else {
        System.out.println("WARN: " + msg);
        Log.warn(msg);
      }
    }
    return number;
  }

  public static BigDecimal validateBigDecimal(BigDecimal number, BigDecimal minValue,
      BigDecimal maxValue, boolean throwExeption) {
    if (number == null)
      return null;

    String msg = String.format(Utility
        .messageBD(new DalConnectionProvider(false), "SVFIMD_NUMBER_OUT_OF_RANGE",
            OBContext.getOBContext().getLanguage().getLanguage())
        .replaceAll("%0", String.valueOf(number)).replaceAll("%1", String.valueOf(minValue))
        .replaceAll("%2", String.valueOf(maxValue)));
    if (number.compareTo(minValue) < 0 || number.compareTo(maxValue) > 0) {
      if (throwExeption) {
        throw new OBException(msg);
      } else {
        System.out.println("WARN: " + msg);
        Log.warn(msg);
      }
    }
    return number;
  }

  public static BigDecimal validateMinBigDecimal(BigDecimal number, BigDecimal minValue,
      boolean throwExeption) {
    if (number == null)
      return null;

    String msg = String.format(Utility
        .messageBD(new DalConnectionProvider(false), "SVFIMD_NUMBER_LOWER_THAN",
            OBContext.getOBContext().getLanguage().getLanguage())
        .replaceAll("%0", String.valueOf(number)).replaceAll("%1", String.valueOf(minValue)));
    if (number.compareTo(minValue) < 0) {
      if (throwExeption) {
        throw new OBException(msg);
      } else {
        System.out.println("WARN: " + msg);
        Log.warn(msg);
      }
    }
    return number;
  }

  public static Date validateDateRange(Date date, Date prevDate, Date postDate,
      boolean throwExeption) {
    if (date == null)
      return null;

    long number = date.getTime();
    long minValue = prevDate.getTime();
    long maxValue = postDate.getTime();

    SimpleDateFormat df = new SimpleDateFormat();
    df.applyPattern("yyyy-MM-dd");

    String msg = String.format(Utility
        .messageBD(new DalConnectionProvider(false), "SVFIMD_DATE_OUT_OF_RANGE",
            OBContext.getOBContext().getLanguage().getLanguage()).replaceAll("%0", df.format(date))
        .replaceAll("%1", df.format(prevDate)).replaceAll("%2", df.format(postDate)));
    if (number < minValue || number > maxValue) {
      if (throwExeption) {
        throw new OBException(msg);
      } else {
        System.out.println("WARN: " + msg);
        Log.warn(msg);
      }
    }
    return date;
  }

  public static PriceListVersion getPriceListVersion(String orgSearchKey) {
    PriceList pl = null;

    StringBuilder query = new StringBuilder();
    query.append(" where organization.searchKey = '" + orgSearchKey + "'");
    query.append(" AND salesPriceList = true");

    OBQuery<PriceList> genQuery = OBDal.getInstance()
        .createQuery(PriceList.class, query.toString());

    if (genQuery.count() > 0) {
      pl = genQuery.list().get(0);
    }

    PriceListVersion pricelistVersion = null;

    if (pl != null && !pl.getPricingPriceListVersionList().isEmpty()) {
      pricelistVersion = pl.getPricingPriceListVersionList().get(0);
    }

    return pricelistVersion;
  }

  public static PriceList getPriceList(String orgSearchKey) {
    PriceList pl = null;

    StringBuilder query = new StringBuilder();
    query.append(" where organization.searchKey = '" + orgSearchKey + "'");
    query.append(" AND salesPriceList = true");

    OBQuery<PriceList> genQuery = OBDal.getInstance()
        .createQuery(PriceList.class, query.toString());

    if (genQuery.count() > 0) {
      pl = genQuery.list().get(0);
    }

    return pl;
  }

  public static boolean isValidEmail(String email) {
    boolean isValid = true;

    if (email == null)
      return isValid;

    if (StringUtils.countMatches(email, "@") != 1) {
      return false;
    }

    String domain = email.split("@")[1];

    if (!domain.contains(".")) {
      return false;
    }

    return isValid;
  }

  public static String getSplitter(String str) throws Exception {
    String splitter = str.replace("0", "").replace("1", "").replace("2", "").replace("3", "")
        .replace("4", "").replace("5", "").replace("6", "").replace("7", "").replace("8", "")
        .replace("9", "").replace(" ", "");

    if (splitter.length() > 1) {
      String lastSplitter = splitter.substring(0, 1);
      for (int i = 1; i < splitter.length(); i++) {
        String actualSplitter = splitter.substring(i, i + 1);
        if (!lastSplitter.equals(actualSplitter)) {
          throw new Exception("[" + str + "] cannot be splitted with multiple splitters.");
        }
      }
      splitter = lastSplitter;
    }

    return splitter;
  }

  public static void logProgress(int actual, int total, Object cl) {
    Logger log = Logger.getLogger(cl.getClass());
    if (logProgress()) {
      if (total > 1000) {
        int printEvery = total / 1000;
        if (actual % printEvery == 0) {
          log.info("Progress: " + actual + "/" + total);
        }
      } else {
        log.info("Progress: " + actual + "/" + total);
      }
    }
  }
}