package com.spocsys.vigfurniture.importdata.utils;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BankAccount;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.geography.Region;

import com.spocsys.vigfurnitures.services.CreditCard;

public class Utils {

  private static Logger Log = Logger.getLogger(Utils.class);
  private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
      + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

  public static BigDecimal parseBigDecimal(String str) {

    BigDecimal R = null;

    try {
      R = new BigDecimal(str);

    } catch (Exception e) {

    }
    return R;

  }

  public static boolean isBoolean(String str) {

    boolean R = false;

    try {
      if ((str != null) && (str.trim().equals("") != true)) {
        if ((str.equalsIgnoreCase("false") == true) || (str.equalsIgnoreCase("true") == true)) {
          return true;
        }
      }

    } catch (Exception e) {

    }
    return R;

  }

  public static String getOrgParent(String strOrgId) {
    String id_org = "";
    String strOrgIdNew = "0";

    if ((strOrgId != null) && (strOrgId.trim().equals("") != true)) {
      strOrgIdNew = strOrgId;
    }

    try {

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS org");
      queryExtend.append(" WHERE ");
      queryExtend.append(" (ad_isorgincluded('" + strOrgIdNew + "', org.id, org.client.id)<>-1)");
      queryExtend.append(" AND (org.organizationType='1')");

      OBQuery<Organization> obq = OBDal.getInstance().createQuery(Organization.class,
          queryExtend.toString());

      if ((obq != null) && (obq.list().size() > 0)) {
        id_org = obq.list().get(0).getId();
      }

    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e);
    }
    return id_org;
  }

  public static Currency getCurrency(Organization regOrganization) {
    try {

      if (regOrganization.getCurrency() != null) {
        return regOrganization.getCurrency();
      } else {

        String IDParent = getOrgParent(regOrganization.getId());
        Organization regOrganizationNew = OBDal.getInstance().get(Organization.class, IDParent);

        if ((regOrganizationNew != null) && (regOrganizationNew.getCurrency() != null)) {
          return regOrganizationNew.getCurrency();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;
  }

  public static Organization getOrgBySearchKey(String searchKey) {
    try {
      String searchKeyNew = searchKey;

      if (searchKey == null) {
        searchKeyNew = "0";
      } else {
        if (searchKey.trim().equalsIgnoreCase("*")) {
          searchKeyNew = "0";
        }
      }

      OBCriteria<Organization> OrganizationList = OBDal.getInstance().createCriteria(
          Organization.class);
      OrganizationList.add(Restrictions.eq(Organization.PROPERTY_SEARCHKEY, searchKeyNew));

      if (OrganizationList.count() > 0) {
        return OrganizationList.list().get(0);
      }

    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }

    return null;

  }

  public static DocumentType getDocumentType(String DocBaseType, boolean issoTrx, boolean isReturn) {

    try {

      OBCriteria<DocumentType> DocumentTypeList = OBDal.getInstance().createCriteria(
          DocumentType.class);

      if (DocBaseType.equalsIgnoreCase("0") != true) {

        DocumentTypeList.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, DocBaseType));
        DocumentTypeList.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, issoTrx));
        DocumentTypeList.add(Restrictions.eq(DocumentType.PROPERTY_RETURN, isReturn));

      } else {
        DocumentTypeList.add(Restrictions.eq(DocumentType.PROPERTY_ID, "0"));
      }

      if (DocumentTypeList.count() > 0) {
        return DocumentTypeList.list().get(0);
      } else {
        return null;
      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;
  }

  public static String getStatusHeader(String str) {

    if ((str != null) && (str.trim().equals("") != true)) {

      if (str.equalsIgnoreCase("partial") == true) {
        return "Partial";
      } else {
        if (str.equalsIgnoreCase("VOID") == true) {
          return "VOID";
        } else {
          if (str.equalsIgnoreCase("Waiting for PO") == true) {
            return "Waiting for PO";
          } else {
            if (str.equalsIgnoreCase("Waiting for Deposit-FAP") == true) {
              return "Waiting for Deposit-FAP";
            } else {
              if (str.equalsIgnoreCase("Waiting for Stock") == true) {
                return "Waiting for Stock";
              } else {
                if (str.equalsIgnoreCase("Intercompany") == true) {
                  return "Intercompany";
                } else {
                  if (str.equalsIgnoreCase("Transfer") == true) {
                    return "Transfer";
                  } else {
                    if (str.equalsIgnoreCase("New") == true) {
                      return "New";
                    } else {
                      if (str.equalsIgnoreCase("Complete") == true) {
                        return "Complete";
                      } else {
                        if (str.equalsIgnoreCase("Mix") == true) {
                          return "Mix";
                        } else {
                          if (str.equalsIgnoreCase("Waiting for Manager Approval") == true) {
                            return "Waiting for Manager Approval";
                          } else {
                            if (str.equalsIgnoreCase("Not approved") == true) {
                              return "Not approved";
                            } else {
                              if (str.equalsIgnoreCase("Waiting for Customer Confirm") == true) {
                                return "Waiting for Customer Confirm";
                              } else {
                                if (str.equalsIgnoreCase("SR Attention") == true) {
                                  return "SR Attention";
                                } else {
                                  if (str.equalsIgnoreCase("RTP") == true) {
                                    return "RTP";
                                  } else {
                                    if (str.equalsIgnoreCase("Need to Refund") == true) {
                                      return "Need to Refund";
                                    } else {
                                      if (str.equalsIgnoreCase("Waiting for Financial Approval") == true) {
                                        return "Waiting for Financial Approval";
                                      } else {
                                        if (str.equalsIgnoreCase("Ready to Pick") == true) {
                                          return "Ready to Pick";
                                        } else {
                                          return null;
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      return null;
    }
  }

  public static String getStatusLine(String str) {

    if ((str != null) && (str.trim().equals("") != true)) {
      if (str.equalsIgnoreCase("Waiting for Confirmation") == true) {
        return "Waiting for Confirmation";
      } else {
        if (str.equalsIgnoreCase("Sourcing") == true) {
          return "Sourcing";
        } else {
          if (str.equalsIgnoreCase("Transfer") == true) {
            return "Transfer";
          } else {
            if (str.equalsIgnoreCase("Waiting for Stock") == true) {
              return "Waiting for Stock";
            } else {

              if (str.equalsIgnoreCase("Waiting for FAP") == true) {
                return "Waiting for FAP";
              } else {

                if (str.equalsIgnoreCase("Waiting for Deposit-FAP") == true) {
                  return "Waiting for Deposit-FAP";
                } else {

                  if (str.equalsIgnoreCase("Waiting for MAP") == true) {
                    return "Waiting for MAP";
                  } else {

                    if (str.equalsIgnoreCase("Intercompany") == true) {
                      return "Intercompany";
                    } else {
                      return null;
                    }
                  }

                }
              }
            }
          }

        }
      }

    } else {
      return null;
    }

  }

  public static String getProductType(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {

      if (str.equalsIgnoreCase("Item") == true) {
        R = "I";
      } else {
        if (str.equalsIgnoreCase("Expense type") == true) {
          R = "E";
        } else {
          if (str.equalsIgnoreCase("Resource") == true) {
            R = "R";
          } else {
            if (str.equalsIgnoreCase("Service") == true) {
              R = "S";
            } else {
              if (str.equalsIgnoreCase("Online") == true) {
                R = "O";
              }
            }
          }
        }
      }
    }
    return R;
  }

  public static String getCostType(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {

      if (str.equalsIgnoreCase("Average") == true) {
        R = "AVA";
      } else {
        if (str.equalsIgnoreCase("Standard") == true) {
          R = "STA";
        } else {
          if (str.equalsIgnoreCase("Legacy Average") == true) {
            R = "AV";
          } else {
            if (str.equalsIgnoreCase("Legacy Standard") == true) {
              R = "ST";
            }
          }
        }
      }
    }
    return R;
  }

  public static String getProductCharaterisic(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {

      if (str.equalsIgnoreCase("Hard Kit") == true) {
        R = "HK";
      } else {
        if (str.equalsIgnoreCase("Soft Kit") == true) {
          R = "SK";
        } else {
          if (str.equalsIgnoreCase("Special Order") == true) {
            R = "SO";
          } else {
            if (str.equalsIgnoreCase("Substitute Product") == true) {
              R = "SP";
            }
          }
        }
      }
    }
    return R;
  }

  public static String getBrand(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {

      if (str.equalsIgnoreCase("Hard Kit") == true) {
        R = "HK";
      } else {
        if (str.equalsIgnoreCase("Soft Kit") == true) {
          R = "SK";
        } else {
          if (str.equalsIgnoreCase("Special Order") == true) {
            R = "SO";
          } else {
            if (str.equalsIgnoreCase("Substitute Product") == true) {
              R = "SP";
            }
          }
        }
      }
    }
    return R;
  }

  public static String getColor(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {
      R = str;
     /*
      if (str.equalsIgnoreCase("Blue") == true) {
        R = "00";
      } else {
        if (str.equalsIgnoreCase("Green") == true) {
          R = "02";
        } else {
          if (str.equalsIgnoreCase("White") == true) {
            R = "01";
          }
        }
      }
     */
    }
    return R;
  }

  public static String getFinish(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {
      /*
      if (str.equalsIgnoreCase("Finish 1") == true) {
        R = "01";
      } else {
        if (str.equalsIgnoreCase("Finish 2") == true) {
          R = "02";
        }
      }
     */
     R = str;
    }
    return R;
  }

  public static String getFinishMaterial(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {
      /* 
      if (str.equalsIgnoreCase("Finish Material 1") == true) {
        R = "00";
      }
      */
     R = str;
    }
    return R;
  }

  public static String getMaterial(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {
      /*
      if (str.equalsIgnoreCase("Finish Material 1") == true) {
        R = "00";
      }
      */
      R = str;   
    }
    return R;
  }

  public static String getItemType(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {
     R = str;
     /*
      if (str.equalsIgnoreCase("Console") == true) {
        R = "Console";
      } else {
        if (str.equalsIgnoreCase("TV unit") == true) {
          R = "TV unit";
        } else {
          if (str.equalsIgnoreCase("Bed") == true) {
            R = "Bed";
          } else {
            if (str.equalsIgnoreCase("Table") == true) {
              R = "Table";
            } else {
              if (str.equalsIgnoreCase("Dressed") == true) {
                R = "Dressed";
              } else {
                if (str.equalsIgnoreCase("Chair") == true) {
                  R = "Chair";
                }
              }
            }
          }
        }
      }
*/
    }
    return R;
  }

  public static String getItemSpecification(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {
      /*
      if (str.equalsIgnoreCase("Right") == true) {
        R = " Right";
      } else {
        if (str.equalsIgnoreCase("Queen") == true) {
          R = "Queen";
        } else {
          if (str.equalsIgnoreCase("Square") == true) {
            R = " Square";
          } else {
            if (str.equalsIgnoreCase("6 Drawers") == true) {
              R = "6 Drawers";
            } else {
              if (str.equalsIgnoreCase("Long") == true) {
                R = "Long";
              } else {
                if (str.equalsIgnoreCase("Concrete") == true) {
                  R = "Concrete";
                } else {
                  if (str.equalsIgnoreCase("Eastern King") == true) {
                    R = "Eastern King";
                  } else {
                    if (str.equalsIgnoreCase("Hlf leather") == true) {
                      R = " Hlf leather";
                    } else {
                      if (str.equalsIgnoreCase("California King") == true) {
                        R = "California King";
                      } else {
                        if (str.equalsIgnoreCase("full leather") == true) {
                          R = "full leather";
                        } else {
                          if (str.equalsIgnoreCase("Short") == true) {
                            R = "Short";
                          } else {
                            if (str.equalsIgnoreCase("2 Drawers") == true) {
                              R = " 2 Drawers";
                            } else {
                              if (str.equalsIgnoreCase("Left") == true) {
                                R = "Left";
                              } else {
                                if (str.equalsIgnoreCase("Round") == true) {
                                  R = "Round";
                                } else {
                                  if (str.equalsIgnoreCase("bonded leather") == true) {
                                    R = "bonded leather";
                                  } else {
                                    if (str.equalsIgnoreCase("Tall") == true) {
                                      R = "Tall";
                                    } else {
                                      if (str.equalsIgnoreCase("Mirrored") == true) {
                                        R = "Mirrored";
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }

        }
      }
      */
     R = str;
    }
    return R;
  }

  public static String getRegulations(String str) {

    String R = null;

    if ((str != null) && (str.trim().equalsIgnoreCase("") != true)) {

      if (str.equalsIgnoreCase("CA Fire Foam") == true) {
        R = "CA Fire Foam";
      }
    }
    return R;
  }

  public static Region getRegion(Country regCountry, String strRegion) {

    try {

      try {
        OBContext.setAdminMode();

        OBCriteria<Region> obc = OBDal.getInstance().createCriteria(Region.class);
        obc.add(Restrictions.eq(Region.PROPERTY_COUNTRY + ".id", regCountry.getId()));
        obc.add(Restrictions.eq(Region.PROPERTY_NAME, strRegion));

        if ((obc != null) && (obc.count() > 0)) {
          return obc.list().get(0);
        }

      } finally {
        OBContext.restorePreviousMode();
      }

    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;
  }

  public static boolean isEmail(String email) {

    // Compiles the given regular expression into a pattern.
    Pattern pattern = Pattern.compile(PATTERN_EMAIL);

    // Match the given input against this pattern
    Matcher matcher = pattern.matcher(email);
    return matcher.matches();

  }

  public static String getInvoiceTerms(String str) {

    if ((str != null) && (str.trim().equals("") != true)) {

      if (str.equalsIgnoreCase("After Order Delivered") == true) {
        return "O";
      } else {
        if (str.equalsIgnoreCase("After Delivery") == true) {
          return "D";
        } else {
          if (str.equalsIgnoreCase("Customer Schedule After Delivery") == true) {
            return "S";
          } else {
            if (str.equalsIgnoreCase("Immediate") == true) {
              return "I";
            } else {
              if (str.equalsIgnoreCase("Do Not Invoice") == true) {
                return "N";
              }
            }
          }
        }
      }
    }
    return null;

  }

  public static org.openbravo.model.common.businesspartner.Location getBusinnesParnerLocation(
      BusinessPartner regBusinessPartner, String strLocationCity) {

    try {
      OBContext.setAdminMode();

      String PropBusinessPartner = org.openbravo.model.common.businesspartner.Location.PROPERTY_BUSINESSPARTNER
          + ".id";
      String PropLoc = org.openbravo.model.common.businesspartner.Location.PROPERTY_NAME;

      OBCriteria<org.openbravo.model.common.businesspartner.Location> obc = OBDal.getInstance()
          .createCriteria(org.openbravo.model.common.businesspartner.Location.class);
      obc.add(Restrictions.eq(PropBusinessPartner, regBusinessPartner.getId()));
      obc.add(Restrictions.eq(PropLoc, strLocationCity));

      if ((obc != null) && (obc.count() > 0)) {
        return obc.list().get(0);
      }

    } finally {
      OBContext.restorePreviousMode();
    }

    return null;
  }

  public static BankAccount getBankAccountBusinessPartner(BusinessPartner regBusinessPartner,
      String strBankAccountNumber) {

    try {

      OBCriteria<BankAccount> obc = OBDal.getInstance().createCriteria(BankAccount.class);
      obc.add(Restrictions.eq(BankAccount.PROPERTY_BUSINESSPARTNER + ".id",
          regBusinessPartner.getId()));
      obc.add(Restrictions.eq(BankAccount.PROPERTY_ACCOUNTNO, strBankAccountNumber));

      if ((obc != null) && (obc.count() > 0)) {
        return obc.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;

  }

  public static String getBankFormat(String str) {

    if ((str != null) && (str.trim().equals("") != true)) {

      if (str.equalsIgnoreCase("Use Generic Account No.") == true) {
        return "GENERIC";
      } else {
        if (str.equalsIgnoreCase("Use IBAN") == true) {
          return "IBAN";
        } else {
          if (str.equalsIgnoreCase("Use SWIFT + Generic Account No.") == true) {
            return "SWIFT";
          } else {
            if (str.equalsIgnoreCase("Use Spanish") == true) {
              return "SPANISH";
            }
          }
        }
      }
    }
    return null;
  }

  public static User getContactUser(BusinessPartner regBusinessPartner, String strContactFirstName) {
    try {
      OBContext.setAdminMode();
      try {
        OBCriteria<User> obc = OBDal.getInstance().createCriteria(User.class);
        obc.add(Restrictions.eq(User.PROPERTY_BUSINESSPARTNER + ".id", regBusinessPartner.getId()));
        obc.add(Restrictions.eq(User.PROPERTY_FIRSTNAME, strContactFirstName));

        if ((obc != null) && (obc.count() > 0)) {
          return obc.list().get(0);
        }
      } catch (Exception e) {
        e.printStackTrace();
        Log.error(e.getMessage());
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return null;
  }

  public static CreditCard getCreditCard(BusinessPartner regBusinessPartner, String strCardNo) {
    try {
      OBCriteria<CreditCard> obc = OBDal.getInstance().createCriteria(CreditCard.class);
      obc.add(Restrictions.eq(CreditCard.PROPERTY_BUSINESSPARTNER + ".id",
          regBusinessPartner.getId()));
      obc.add(Restrictions.eq(CreditCard.PROPERTY_CREDITCARDNO, strCardNo));

      if ((obc != null) && (obc.count() > 0)) {
        return obc.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;
  }

}
