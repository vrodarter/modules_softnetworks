package com.spocsys.vigfurniture.incomingproduct.erpCommon.ad_actionButton;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.incomingproduct.OrderReference;
import com.spocsys.vigfurniture.productavailability.OrderReference.LinkSOToPO;

public class LinkToPOProcess extends BaseActionHandler {

  private static final Logger log = Logger.getLogger(LinkToPOProcess.class);

  protected JSONObject execute(Map<String, Object> parameters, String data) {
    try {

      final JSONObject JSONreg = new JSONObject(data);
      final JSONObject JSONResult = new JSONObject();
      final JSONObject JSONRegResult = new JSONObject();

      JSONRegResult.put("status", "ok");
      JSONRegResult.put("msg", "");

      JSONArray JSONOrderLines = JSONreg.getJSONArray("Lines");
      String SalesOrderIds = getSalesOrderIDs(JSONOrderLines);
      List<Order> ListSalesOrder = getOrder(SalesOrderIds);

      try {
        int I = 0;
        if ((ListSalesOrder != null) && (ListSalesOrder.size() > 0)) {

          for (Order regOrder : ListSalesOrder) {

            if ((regOrder.getOrderLineList() != null) && (regOrder.getOrderLineList().size() > 0)) {

              List<OrderLine> ListOrderLine = regOrder.getOrderLineList();

              for (OrderLine regOrderLine : ListOrderLine) {

                if ((regOrderLine.getSvfpaRemotepo() != null)
                    && (regOrderLine.getSvfpaRemotepo().trim().equals("") != true)) {

                  OrderLine RemotePOLine = getOrderLineByDocumentNo(regOrderLine);

                  if (RemotePOLine != null) {

                    OrderReference regOrderReference = LinkSOToPO.create(regOrderLine,
                        RemotePOLine, regOrderLine.getOrderedQuantity());

                    if (regOrderReference == null) {
                      System.out
                          .println("There was an error creating Order Reference Sales Order Line: "
                              + regOrderLine.getIdentifier());
                      log.error("There was an error creating Order Reference Sales Order Line: "
                          + regOrderLine.getIdentifier());
                    } else {
                      // --
                      regOrderLine.setSvfpaPurchaseorder(RemotePOLine.getSalesOrder());
                      regOrderLine.setSvfpaIgnorechkpa(true);
                      regOrderLine.setSvfpaAction("WAITINGFORPO");
                      // --
                      OBDal.getInstance().save(regOrderReference);
                      OBDal.getInstance().save(regOrderLine);
                      OBDal.getInstance().flush();
                      I++;
                    }

                  }
                } else {

                  if (regOrderLine.getSvfpaPurchaseorder() != null) {

                    OrderReference regOrderReference = LinkSOToPO.create(regOrderLine, null,
                        regOrderLine.getOrderedQuantity());

                    if (regOrderReference == null) {

                      System.out
                          .println("There was an error creating Order Reference Sales Order Line: "
                              + regOrderLine.getIdentifier());
                      log.error("There was an error creating Order Reference Sales Order Line: "
                          + regOrderLine.getIdentifier());
                    } else {
                      OBDal.getInstance().save(regOrderReference);
                      OBDal.getInstance().flush();
                      I++;
                    }

                  }
                }
              }

            }

          }
          if (I > 0) {
            OBDal.getInstance().commitAndClose();
          }
          JSONRegResult.put("status", "ok");
          JSONRegResult.put("msg", "record updated: " + (I));
        }

      } catch (Exception e) {

        try {
          OBDal.getInstance().rollbackAndClose();
        } catch (Exception e1) {
          e1.printStackTrace();
          log.error(e1.getMessage());
        }
        log.error(e.getMessage());
        JSONRegResult.put("status", "nook");
        JSONRegResult.put("msg", e.getMessage());

      }

      JSONResult.put("result", JSONRegResult);
      return JSONResult;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      throw new OBException(e);
    }
  }

  String getSalesOrderIDs(JSONArray JSONOrderLines) {

    String Ids = "";
    try {

      int Fin = JSONOrderLines.length();

      for (int I = 0; I < Fin; I++) {
        if (I > 0) {
          Ids += ",";
        }

        JSONObject regJSONObject = JSONOrderLines.getJSONObject(I);
        Ids += "'" + regJSONObject.getString("id") + "'";

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return Ids;

  }

  List<Order> getOrder(String Ids) {
    try {
      if ((Ids != null) && (Ids.trim().equals("") != true)) {
        final StringBuilder queryExtend = new StringBuilder();
        queryExtend.append(" AS ord");
        queryExtend.append(" WHERE ");
        queryExtend.append(" (ord.id IN (" + Ids + ")) and ");
        queryExtend.append(" (ord." + Order.PROPERTY_DOCUMENTTYPE
            + ".id IN (select doc.id from DocumentType doc where doc.documentCategory = 'SOO'))");

        OBQuery<Order> obq = OBDal.getInstance().createQuery(Order.class, queryExtend.toString());

        if ((obq != null) && (obq.list().size() > 0)) {
          return obq.list();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return null;
  }

  OrderLine getOrderLineByDocumentNo(OrderLine regSalesOrderLine) {
    try {

      final Criteria orderLineList = OBDal.getInstance().createCriteria(OrderLine.class);

      orderLineList.add(Restrictions.eq(OrderLine.PROPERTY_PRODUCT + ".id", regSalesOrderLine
          .getProduct().getId()));
      orderLineList
          .createAlias(OrderLine.PROPERTY_SALESORDER, "order")
          .add(Restrictions.eq("order." + Order.PROPERTY_SALESTRANSACTION, false))
          .add(
              Restrictions.eq("order." + Order.PROPERTY_DOCUMENTNO,
                  regSalesOrderLine.getSvfpaRemotepo()));

      if ((orderLineList != null) && (orderLineList.list().size() > 0)) {
        return (OrderLine) orderLineList.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }
}
