isc.OBPopup.create
({
    ID: "SVFPINC_WindowPopupProcessLinkToPOProcess",
    title: "Link from SO to PO Process",
    autoSize:true,
    autoCenter: true,
    showMinimizeButton: false,
    showMaximizeButton: false,
    isModal: true,
    showModalMask: true,
    autoDraw: false,
    height: "30%",
    width:"50%",
    closeClick: function () {this.hide();},
    items:
    [
		
		isc.VStack.create
		({
			ID: "SVFPINC_WindowPopupProcessCapaDataLinkToPOProcess",
			width:"100%",
			//visible: false,
			autoDraw: false,
			members:
		    [
		     
		    ]
		})
    ],
    begin: function()
    {
      try
      {
    	  var Height = (this.getHeight() - 28)+"px";
    	  OB.SVFPINC.WindoLoadingImg = OB.SVFPINC.WindoLoadingImg.replace(/000HEIGHT/g, Height);
    	  var SVFPINC_WindoLoadingImgVar = isc.HTMLFlow.create
    	  ({
    	        ID: "SVFPINCWindoLoadingImgFinishDropShipProcess",
    	        contents: OB.SVFPINC.WindoLoadingImg,
    	        width:"100%",
    	        height:"100%"
    	  });
    	  SVFPINC_WindowPopupProcessCapaDataLinkToPOProcess.removeMembers(SVFPINC_WindowPopupProcessCapaDataLinkToPOProcess.getMembers());
    	  SVFPINC_WindowPopupProcessCapaDataLinkToPOProcess.addMember(SVFPINC_WindoLoadingImgVar);
       }
       catch(e)
       {
    	  this.hide();
    	  isc.warn("There was an error while it was executing the action");
    	  console.log(e); 
       }
    }
});

OB.SVFPINC = OB.SVFPINC || {};

OB.SVFPINC.LinkToPOProcess =
{ 
  View: null,
  grid: null,
  callback: function (rpcResponse, data, rpcRequest)
  {
	 try
	 {
		 if((data!= null) && (data.result))
		 {
		   if(data.result.status==='nook')
		   {
			 isc.warn('There was an error while it was executing the action');
			 console.log(data.result.msg);
		   }
		   else 
		   {
			   if (data.result.status==='ok')
			   {	   
			     OB.SVFPINC.LinkToPOProcess.View.messageBar.setMessage('success', null, data.result.msg);
			     console.log(data.result.msg);
			     OB.SVFPINC.LinkToPOProcess.View.refresh();
		       }
			   else
			   {
				 if(data.result.status==='war')
				 {	 
				   OB.SVFPINC.LinkToPOProcess.View.messageBar.setMessage('warning', null, data.result.msg);
				   console.log(data.result.msg);
				 }  
			   }	   
		   }
		 }
	 }
	 catch(e)
	 {
	   console.log(e.message);	 
	 }
	 SVFPINC_WindowPopupProcessLinkToPOProcess.hide();
	 
  },
  beginProcess: function ()
  {
	  SVFPINC_WindowPopupProcessLinkToPOProcess.show();
	  SVFPINC_WindowPopupProcessLinkToPOProcess.begin();
	  
	  OB.RemoteCallManager.call('com.spocsys.vigfurniture.incomingproduct.erpCommon.ad_actionButton.LinkToPOProcess', 
		  {
		     "Lines": OB.SVFPINC.LinkToPOProcess.grid.getSelectedRecords(),
		  }, {}, OB.SVFPINC.LinkToPOProcess.callback);
	 	  
  },
  execute: function (params, view)
  {
	try
	{
		OB.SVFPINC.LinkToPOProcess.grid= params.button.contextView.viewGrid;
		//---
		isc.ask("Link from SO to PO will be created. Do you want to execute this action?", function(ok){
	        if (ok) {
	        	OB.SVFPINC.LinkToPOProcess.beginProcess();
	        }
	    }); 
	}
	catch(e)
	{
	  console.log(e.message);	
	}
	  
  },
  executeProcess: function (params, view)
  {
	try
	{
	  params.action = 'LinkToPOProcess';
	  OB.SVFPINC.LinkToPOProcess.View= params.button.contextView;	
	  OB.SVFPINC.LinkToPOProcess.execute(params, view);
	}
	catch(e)
	{
	  console.log(e.message);	
	}
  }
};
