package com.spocsys.vigfurniture.picking.baseprocess;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.security.OrganizationStructureProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.domain.Reference;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.transaction.MaterialTransaction;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.service.db.DbUtility;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

public class MovementStage extends BaseActionHandler {

	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {
		JSONObject json = new JSONObject();
		StringBuilder builder= new StringBuilder();
		StringBuilder sucess= new StringBuilder();
		try{
			OBContext.getOBContext().setAdminMode(true);
			final JSONObject jsonData = new JSONObject(content);
			final JSONArray movementlinesIds = jsonData.getJSONArray("packings");
			// get the data as json
			
			final String action = jsonData.getString("action");
			if ("getstage".equals(action)) {
				json.put("valuecheck", getStageComboBox(json));
				return json;
			}else{
				
				for (int i = 0; i < movementlinesIds.length(); i++) {
					final String movementlinesID = movementlinesIds.getString(i);

					Locator locatorTo = getLocatorFromStage(action);
					if(locatorTo==null){
						throw new Exception("No Storage Bin with Movement Stage: " + action );
					}

					// get the movementLine
					StringBuilder hsqlScript= new StringBuilder();
				     hsqlScript.append("select line from MaterialMgmtShipmentInOutLine as line ");
				      hsqlScript.append("left outer join line.shipmentReceipt ship ");
			
				      hsqlScript.append("where ship.");
					 hsqlScript.append(ShipmentInOut.PROPERTY_OBWPACKPACKINGH);
					 
					 hsqlScript.append(".id = '");
					 hsqlScript.append(movementlinesID);
					 hsqlScript.append("'");
				     final Session session = OBDal.getInstance().getSession();
				     final Query query = session.createQuery(hsqlScript.toString());
				      
				     List<ShipmentInOutLine> list=   query.list();
				     
				     for (ShipmentInOutLine inOutLine : list) {
				    	 
				    	 
				    		
							

							Locator locatorFrom= inOutLine.getStorageBin();

							MaterialTransaction transactionFrom = OBProvider.getInstance().get(MaterialTransaction.class);

							MaterialTransaction transactionTo = OBProvider.getInstance().get(MaterialTransaction.class);

							Product product = OBDal.getInstance().get(Product.class, inOutLine.getProduct().getId());

							transactionFrom.setProduct(product);
							transactionTo.setProduct(product);

							transactionFrom.setStorageBin(locatorFrom);
							transactionTo.setStorageBin(locatorTo);
							transactionFrom.setCheckReservedQuantity(false);
							
							transactionFrom.setGoodsShipmentLine(inOutLine);
							transactionTo.setGoodsShipmentLine(inOutLine);
						//	transactionFrom.setMovementLine(movementLine);
						//	transactionTo.setMovementLine(movementLine);

							transactionFrom.setMovementDate(new Date());
							transactionTo.setMovementDate(new Date());

							transactionFrom.setMovementType("M-");
							transactionTo.setMovementType("M+");

							transactionFrom.setMovementQuantity(inOutLine.getMovementQuantity().negate());
							transactionTo.setMovementQuantity(inOutLine.getMovementQuantity());

							transactionFrom.setUOM(product.getUOM());
							transactionTo.setUOM(product.getUOM());


							transactionFrom.setAttributeSetValue(inOutLine.getAttributeSetValue());
							transactionTo.setAttributeSetValue(inOutLine.getAttributeSetValue());


							OBDal.getInstance().save(transactionFrom);
							OBDal.getInstance().save(transactionTo);

							OBDal.getInstance().flush();

							//Updating Sales Order Status & Stock Location...

							inOutLine.setStorageBin(locatorTo);
							OBDal.getInstance().save(locatorTo);
							
							
							OrderLine order = inOutLine.getSalesOrderLine();
							if(order!=null){
							
							order.setSvfpaStockLocation(locatorTo.getWarehouse());
							if(action.equalsIgnoreCase("Ready")){
								order.setSvfpaStatus("SVFAPPR_Delivered");
							}else
							order.setSvfpaStatus(action);

							OBDal.getInstance().save(order);
							OBDal.getInstance().flush();
							}
							sucess.append("New Transactions From :" +transactionFrom.getIdentifier() +"To "+transactionTo.getIdentifier() +" for product " + product.getIdentifier() + "<br/>");


				    	 
					}
				   
					 
					
				
							
				
				}


				JSONObject jsonMsg = new JSONObject();
			
				jsonMsg.put("severity", "info");

				jsonMsg.put("text",sucess.toString() );
				json.put("message", jsonMsg);
			}
		}catch(Exception e ){
			try{
				
				json = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", "".equals(message) ? e : message);
				json.put("message", errorMessage);
				OBDal.getInstance().rollbackAndClose();
			} catch (Exception e2) {

			}
		}finally{
			OBContext.getOBContext().restorePreviousMode();;
			OBDal.getInstance().commitAndClose();
		}


		///Acordarme de Validar que los movement line esten en complete
		return json;
	}


	public Locator getLocatorFromStage(String stage) {
		Locator locator= null;

		OBCriteria<Locator> criteria = OBDal.getInstance().createCriteria(Locator.class);

		criteria.add(Restrictions.eq(Locator.PROPERTY_SVFPMOVEMTSTAGE, stage));
		criteria.add(Restrictions.eq(Locator.PROPERTY_WAREHOUSE, OBContext.getOBContext().getWarehouse()));


		if(criteria.list()!=null&&! criteria.list().isEmpty())
            locator=criteria.list().get(0);

		return locator;

	}


	private JSONObject getStageComboBox(JSONObject jsonRequest) throws Exception {
		
		OBCriteria<org.openbravo.model.ad.domain.List> criteria = OBDal.getInstance().createCriteria(org.openbravo.model.ad.domain.List.class);

		criteria.add(Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_REFERENCE,OBDal.getInstance().get(Reference.class, "D42C1CC7ED4440AAAAB065EF637C1FE0")));

		JSONObject response = new JSONObject();
		JSONObject valueMap = new JSONObject();
		for (org.openbravo.model.ad.domain.List list : criteria.list()) {
			valueMap.put(list.getSearchKey(), list.getIdentifier());
		}
		response.put("valueMap", valueMap);
		response.put("defaultValue", "");
		
		return response;
	}

	

}
