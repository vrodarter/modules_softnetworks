/************************************************************************************
 * Copyright (C) 2013-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package com.spocsys.vigfurniture.picking.handler;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.security.OrganizationStructureProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.service.db.DbUtility;
import org.openbravo.warehouse.pickinglist.PickingList;

import com.spocsys.vigfurniture.picking.SVPF_Utils;

public class AssignActionHandler extends BaseActionHandler {
	final private static Logger log = Logger.getLogger(AssignActionHandler.class);

	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {
		JSONObject jsonResponse = new JSONObject();
		try {
			JSONObject jsonRequest = new JSONObject(content);
			final String strAction = jsonRequest.getString("action");
			if ("getemployees".equals(strAction)) {
				jsonResponse.put("valuecheck", getEmployeesComboBox(jsonRequest));
				return jsonResponse;
			} else if ("assign".equals(strAction)) {
				JSONObject jsonMsg = assign(jsonRequest);
				jsonResponse.put("message", jsonMsg);
				return jsonResponse;
			}

		} catch (Exception e) {
			log.error("Error in AssignActionHandler", e);
			OBDal.getInstance().rollbackAndClose();

			try {
				jsonResponse = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", "".equals(message) ? e : message);
				jsonResponse.put("message", errorMessage);
			} catch (Exception e2) {
				log.error("Error generating the error message", e2);
			}
		}

		return jsonResponse;
	}

	private JSONObject assign(JSONObject jsonRequest) throws JSONException {
		final JSONObject jsonMsg = new JSONObject();
		jsonMsg.put("severity", "success");
		jsonMsg.put("text", OBMessageUtils.messageBD("Success", false));
		final String strEmployeeId = jsonRequest.getString("employee");
		final User employee = OBDal.getInstance().get(User.class, strEmployeeId);

		final JSONArray movementLineIds = jsonRequest.getJSONArray("pickings");
		String movementLineId = (String) movementLineIds.get(0);
		InternalMovementLine internalMovementLine = OBDal.getInstance().get(InternalMovementLine.class, movementLineId);
		PickingList pickingList = internalMovementLine.getOBWPLWarehousePickingList();

		for (int i = 0; i < movementLineIds.length(); i++) {
			final String strMovementLineId = movementLineIds.getString(i);
			final InternalMovementLine inMovementLine = OBDal.getInstance().get(InternalMovementLine.class, strMovementLineId);

			inMovementLine.setSvfpAdUser(employee);
			inMovementLine.setSvfpMovementlinestatus("AS");
			OBDal.getInstance().save(inMovementLine);

		}
		OBDal.getInstance().flush();

		return jsonMsg;
	}

	private PickingList createGroupPicking(Organization org, Locator locator) {
		PickingList groupPL = OBProvider.getInstance().get(PickingList.class);
		groupPL.setOrganization(org);
		groupPL.setDocumentdate(new Date());
		DocumentType docType = SVPF_Utils.getGroupPLDocumentType(org);
		if (docType == null) {
			throw new OBException(OBMessageUtils.messageBD("SVFP_DoctypeMissing", false));
		}
		groupPL.setDocumentType(docType);
		groupPL.setDocumentNo(SVPF_Utils.getDocumentNo(docType, "SVFP_PickingList"));
		groupPL.setPickliststatus("DR");
		groupPL.setOutboundStorageBin(locator);

		return groupPL;
	}

	private JSONObject getEmployeesComboBox(JSONObject jsonRequest) throws Exception {

		final JSONArray pickingIds = jsonRequest.getJSONArray("pickings");
		String movementLineId = (String) pickingIds.get(0);
		InternalMovementLine internalMovementLine = OBDal.getInstance().get(InternalMovementLine.class, movementLineId);
		PickingList pickingList = internalMovementLine.getOBWPLWarehousePickingList();
		final String strOrgId =pickingList.getOrganization().getId();
		OrganizationStructureProvider osp = OBContext.getOBContext().getOrganizationStructureProvider(
				OBContext.getOBContext().getCurrentClient().getId());
		Set<String> orgs = osp.getNaturalTree(strOrgId);
		OBCriteria<User> critEmployees = OBDal.getInstance().createCriteria(User.class);
		critEmployees.createAlias(User.PROPERTY_ORGANIZATION, "org");
		critEmployees.add(Restrictions.isNotNull(User.PROPERTY_USERNAME));
		critEmployees.add(Restrictions.in("org.id", orgs));
		JSONObject response = new JSONObject();
		JSONObject valueMap = new JSONObject();
		for (User employee : critEmployees.list()) {
			valueMap.put(employee.getId(), employee.getIdentifier());
		}
		response.put("valueMap", valueMap);
		response.put("defaultValue", "");
		return response;
	}
}
