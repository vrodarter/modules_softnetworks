/************************************************************************************
 * Copyright (C) 2013-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package com.spocsys.vigfurniture.picking.handler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.lang.time.DateUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.materialmgmt.ReservationUtils;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.Reservation;
import org.openbravo.model.materialmgmt.onhandquantity.ReservationStock;
import org.openbravo.model.materialmgmt.onhandquantity.StockProposed;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.service.db.DbUtility;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.packing.PackingBox;
import org.openbravo.warehouse.packing.PackingBoxProduct;
import org.openbravo.warehouse.pickinglist.OutboundPickingListProcess;
import org.openbravo.warehouse.pickinglist.PickingList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MovementLineHandler extends BaseActionHandler {
	final private static Logger log = LoggerFactory.getLogger(MovementLineHandler.class);
	StringBuilder output = null;
	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {
		JSONObject jsonRequest = null;

		try {
			jsonRequest = new JSONObject(content);
			final String strAction = jsonRequest.getString("action");
			if ("confirm".equals(strAction)) {
				
				final JSONArray movLineIds = jsonRequest.getJSONArray("movementlines");
				createPacking( movLineIds);
				// Get movementlines
				for (int i = 0; i < movLineIds.length(); i++) {
					if (i == movLineIds.length() - 1) {
						processMovementLine(movLineIds.getString(i), "CF");
					} else {
						processMovementLine(movLineIds.getString(i), "CF", false);
					}
				}
				
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "success");
				errorMessage.put("text", OBMessageUtils.messageBD("Success", false));
				jsonRequest.put("message", errorMessage);
			} else if ("reject".equals(strAction)) {
				final JSONArray movLineIds = jsonRequest.getJSONArray("movementlines");
				// Get movementlines
				for (int i = 0; i < movLineIds.length(); i++) {
					processMovementLine(movLineIds.getString(i), "PE");
				}

				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "success");
				errorMessage.put("text", OBMessageUtils.messageBD("Success"));
				jsonRequest.put("message", errorMessage);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			OBDal.getInstance().rollbackAndClose();
			try {
				jsonRequest = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();

				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", message);
				jsonRequest.put("message", errorMessage);
			} catch (Exception e2) {
				log.error(e.getMessage(), e2);
			}
		}
		return jsonRequest;

	}

	public static void processMovementLine(String strMoveLineId, String strItemStatus,
			boolean checkStatus) throws PropertyException {
		InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
				strMoveLineId);
		mvmtLine.setOBWPLItemStatus(strItemStatus);
		if (mvmtLine.getOBWPLGroupPickinglist() != null) {
			PickingList groupPL = mvmtLine.getOBWPLGroupPickinglist();
			String strStatus = OutboundPickingListProcess.checkStatus(
					groupPL.getMaterialMgmtInternalMovementLineEMOBWPLGroupPickinglistList(), false);
			groupPL.setPickliststatus(strStatus);
		}
		if (checkStatus) {
			
			
			String strStatus = OutboundPickingListProcess.checkStatus(mvmtLine
					.getOBWPLWarehousePickingList().getMaterialMgmtInternalMovementLineEMOBWPLWarehousePickingListList(), true);
			mvmtLine.getOBWPLWarehousePickingList().setPickliststatus(strStatus);
			String prefValue = Preferences.getPreferenceValue("OBWPL_AutoClose", true, OBContext
					.getOBContext().getCurrentClient(), OBContext.getOBContext().getCurrentOrganization(),
					OBContext.getOBContext().getUser(), OBContext.getOBContext().getRole(), null);
			PickingList picking = (PickingList) mvmtLine.getOBWPLWarehousePickingList();
			if (prefValue.equals("Y") && strStatus.equals("CO")) {
				OutboundPickingListProcess.close(picking);
			}
		}
	}

	public static void processMovementLine(String strMoveLineId, String strItemStatus)
			throws PropertyException {
		processMovementLine(strMoveLineId, strItemStatus, true);


	}


	private void createPacking(  JSONArray movLineIds) throws JSONException, ServletException {

		InternalMovementLine mvmtLineFirst = OBDal.getInstance().get(InternalMovementLine.class,
				movLineIds.get(0));
		
		
		PickingList pickingList=	  mvmtLineFirst.getOBWPLWarehousePickingList();
		pickingList= (PickingList) OBDal.getInstance().getProxy(PickingList.ENTITY_NAME, pickingList.getId());
		List<InternalMovementLine> lines= new ArrayList<InternalMovementLine>();
		ShipmentInOut  inOut = createInOut(pickingList, mvmtLineFirst );
		Long lineNo = 10L;
		for (int i = 0; i < movLineIds.length(); i++) {
			InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
					movLineIds.get(i));
			if(mvmtLine.getSvfpPackingh()==null){
			inOut.getMaterialMgmtShipmentInOutLineList().add(createShipmentLine(pickingList.getOutboundStorageBin(), mvmtLine, inOut, pickingList, lineNo));

			lineNo += 10L;
			lines.add(mvmtLine);
			}
		}

		OBDal.getInstance().save(inOut);
		OBDal.getInstance().flush();

		Packing packing =packAutomationProcess(inOut, pickingList); 
		for (InternalMovementLine internalMovementLine : lines) {
			internalMovementLine.setSvfpPackingh(packing);
			OBDal.getInstance().save(internalMovementLine);

			OBDal.getInstance().flush();
		}
		OBDal.getInstance().flush();
		OBDal.getInstance().commitAndClose();


	}



	private Packing packAutomationProcess(ShipmentInOut  inOutd,PickingList pickingList) throws ServletException  {
		OBContext.getOBContext().setAdminMode(true);
		Packing packing = OBProvider.getInstance().get(Packing.class);
		packing.setOrganization(pickingList.getOrganization());
		packing.setPackdate(new Date());

		//Obteniendo los Shipment
		List<ShipmentInOut> inOuts = new ArrayList<ShipmentInOut>();
		BusinessPartner partner = inOutd.getBusinessPartner();
		Location location=inOutd.getPartnerAddress();
		ShipmentInOut inOut=inOutd;

		for (ShipmentInOutLine shipmentInOutLine : inOutd.getMaterialMgmtShipmentInOutLineList()) {
			inOut=	shipmentInOutLine.getShipmentReceipt();
			inOuts.add(inOut);
			inOut.setObwpackPackingh(packing);

		}
		//Create Packing

		packing.setBusinessPartner(partner);
		packing.setPartnerAddress(location);

		if(inOuts!=null && !inOuts.isEmpty()){
			packing.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().addAll(inOuts);
		}

		//Create Pack
		OBDal.getInstance().save(packing);
		OBDal.getInstance().flush();

		OBCriteria<ShipmentInOutLine> critShipLines = OBDal.getInstance().createCriteria(
				ShipmentInOutLine.class);
		critShipLines.add(Restrictions.in(ShipmentInOutLine.PROPERTY_SHIPMENTRECEIPT,
				inOuts));
		ProjectionList projections = Projections.projectionList();
		projections.add(Projections.sum(ShipmentInOutLine.PROPERTY_MOVEMENTQUANTITY));
		projections.add(Projections.groupProperty(ShipmentInOutLine.PROPERTY_PRODUCT));
		critShipLines.setProjection(projections);
		boolean hasBoxes = false;
		if(inOut!=null){
			hasBoxes= !inOut.getOBWPACKBoxList().isEmpty();
		}
		@SuppressWarnings("rawtypes")
		List products = critShipLines.list();
		int queryCount = 0;
		int startbox = 1; 
		for (Object o : products) {

			final Object[] qryRecord = (Object[]) o;
			Product product = (Product) qryRecord[1];
			if (!"I".equals(product.getProductType())) {
				continue;
			}
			// not show items that are non-stocked BOM
			if ("I".equals(product.getProductType()) && product.isBillOfMaterials()
					&& !product.isStocked()) {
				continue;
			}
			queryCount++;

			startbox= makeBoxes(product, (BigDecimal)qryRecord[0],packing,inOut,startbox);


		}
	

		OBDal.getInstance().save(pickingList);

		OBDal.getInstance().flush();

		OBDal.getInstance().commitAndClose();

		if(output==null){
			output=new StringBuilder();
			output.append(packing.getDocumentNo());

		}
		else{
			output.append(", "+packing.getDocumentNo());
		}


		OBContext.getOBContext().restorePreviousMode();
		return packing;


	}


	private int makeBoxes(Product product,BigDecimal quatity,Packing packing, ShipmentInOut shipmentInOut,int startbox) {

		Long  totalNumbeXBox	=product.getSvfpTotalnumbeperbox();
		if(totalNumbeXBox==null || totalNumbeXBox==0){
			totalNumbeXBox =quatity.longValue();
		}
		int quantityOfBox= quatity.divide(new  BigDecimal(totalNumbeXBox), 0, RoundingMode.CEILING).intValue();
		for (int i = 0; i < quantityOfBox; i++) {
			PackingBox box = OBProvider.getInstance().get(PackingBox.class);
			box.setBoxNumber(startbox+0L);
			box.setObwpackPackingh(packing);
			box.setOrganization(packing.getOrganization());
			box.setGoodsShipment(shipmentInOut);
			startbox=startbox+1;
			OBDal.getInstance().save(box);
			OBDal.getInstance().flush();
			PackingBoxProduct packingBoxProduct = OBProvider.getInstance().get(PackingBoxProduct.class);
			packingBoxProduct.setProduct(product);
			packingBoxProduct.setOrganization(packing.getOrganization());
			packingBoxProduct.setUOM(product.getUOM());
			BigDecimal decimal = new BigDecimal(0);
			if(quatity.compareTo(new BigDecimal(totalNumbeXBox))==1){
				decimal=quatity.subtract(new BigDecimal(totalNumbeXBox));
				quatity=decimal;
				packingBoxProduct.setQuantity(new BigDecimal(totalNumbeXBox));
			}else{
				packingBoxProduct.setQuantity(quatity);
			}
			packingBoxProduct.setPackingBox(box);
			OBDal.getInstance().save(packingBoxProduct);
			OBDal.getInstance().flush();
		}
		return quantityOfBox;
	}
	public Locator getLocatorFromStage(String stage) {
		Locator locator= null;
		OBCriteria<Locator> criteria = OBDal.getInstance().createCriteria(Locator.class);
		criteria.add(Restrictions.eq(Locator.PROPERTY_SVFPMOVEMTSTAGE, stage));
		//criteria.add(Restrictions.eq(Locator.PROPERTY_WAREHOUSE, OBContext.getOBContext().getWarehouse()));
		if(criteria.list()!=null&&! criteria.list().isEmpty())
			locator=criteria.list().get(0);
		return locator;

	}


	public static ShipmentInOut createInOut(PickingList pickingList,InternalMovementLine mvmtLineFirst ) throws ServletException {
		final Date now = DateUtils.truncate(new Date(), Calendar.DATE);
		ShipmentInOut shipment = OBProvider.getInstance().get(ShipmentInOut.class);
		shipment.setOrganization(pickingList.getOrganization());
		shipment.setSalesTransaction(true);
		shipment.setMovementType("C-");
		shipment.setMovementDate(now);
		shipment.setDocumentType(getDocType());
		shipment.setPosted("N");
	
		// Setting dummy document number. Final value set at the end to avoid contention

		shipment.setWarehouse(pickingList.getOutboundStorageBin().getWarehouse());


		BusinessPartner  businessPartner = getBusinessPartner(shipment,mvmtLineFirst, pickingList.getDescription());
		Location location = (businessPartner!=null && businessPartner.getBusinessPartnerLocationList()!=null && !businessPartner.getBusinessPartnerLocationList().isEmpty()) ? businessPartner.getBusinessPartnerLocationList().get(0): null;

		shipment.setBusinessPartner(businessPartner);
		shipment.setPartnerAddress(location);
		shipment.setAccountingDate(now);
		shipment.setProcessNow(false);
		OBDal.getInstance().save(shipment);

		return shipment;

	}

	private static BusinessPartner getBusinessPartner(ShipmentInOut shipment,InternalMovementLine movementLine,String description) {
		BusinessPartner businessPartner=null;
		if(movementLine.getStockReservation()!=null &&	movementLine.getStockReservation().getSalesOrderLine()!=null){

			OrderLine orderLine=	movementLine.getStockReservation().getSalesOrderLine();
			orderLine=(OrderLine) OBDal.getInstance().getProxy(OrderLine.ENTITY_NAME, orderLine.getId());

			Order order =orderLine.getSalesOrder();
			order=	(Order) OBDal.getInstance().getProxy(Order.ENTITY_NAME, order.getId());
			shipment.setSalesOrder(order);
			businessPartner=order.getBusinessPartner();
		}else
		{
			//Order No: SO-23 and
			String doc = description.substring(10, description.indexOf(" and"));

			OBCriteria<Order> obCriteria = OBDal.getInstance().createCriteria(Order.class);
			obCriteria.add(Restrictions.ilike(Order.PROPERTY_DOCUMENTNO, doc));

			List<Order>  orders=	obCriteria.list();
			if(!orders.isEmpty()){
				Order order= orders.get(0);
				order=	(Order) OBDal.getInstance().getProxy(Order.ENTITY_NAME, order.getId());
				businessPartner= order.getBusinessPartner();
				shipment.setSalesOrder(order);

			}
		}


		return businessPartner;
	}

	public static DocumentType getDocType() throws ServletException {

		OBCriteria<DocumentType> criteriaE = OBDal.getInstance().createCriteria(DocumentType.class);

		criteriaE.add(Restrictions.eq("name", "MM Shipment"));
		criteriaE.add(Restrictions.eq("active", true));
		criteriaE.add(Restrictions.eq("default", true));


		List<DocumentType> listE = criteriaE.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}

	private static ShipmentInOutLine createShipmentLine( Locator sb, 
			InternalMovementLine internalMovementLine, ShipmentInOut shipment, PickingList pickingList,Long lineNo) {
		ShipmentInOutLine line = OBProvider.getInstance().get(ShipmentInOutLine.class);
		line.setOrganization(shipment.getOrganization());
		line.setShipmentReceipt(shipment);
		line.setAttributeSetValue(internalMovementLine.getProduct().getAttributeSetValue());
		//line.setObwplPickinglist(pickingList);
		line.setLineNo(lineNo);

		line.setProduct(internalMovementLine.getProduct());
		line.setUOM(internalMovementLine.getUOM());

		line.setStorageBin(sb);
		line.setMovementQuantity(internalMovementLine.getMovementQuantity());
		if(internalMovementLine.getStockReservation()!=null && internalMovementLine.getStockReservation().getSalesOrderLine()!=null){
			line.setSalesOrderLine(internalMovementLine.getStockReservation().getSalesOrderLine());
		}



		shipment.getMaterialMgmtShipmentInOutLineList().add(line);
		OBDal.getInstance().save(line);
		OBDal.getInstance().save(shipment);

		return line;
	}

}
