/************************************************************************************
 * Copyright (C) 2012-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package com.spocsys.vigfurniture.picking.handler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.materialmgmt.ReservationUtils;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.Reservation;
import org.openbravo.model.materialmgmt.onhandquantity.ReservationStock;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.db.DbUtility;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.packing.PackingBox;
import org.openbravo.warehouse.packing.PackingBoxProduct;
import org.openbravo.warehouse.pickinglist.OutboundPickingListProcess;
import org.openbravo.warehouse.pickinglist.PickingList;
import org.openbravo.warehouse.pickinglist.hooks.ProcessPLHook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PickingListActionHandler extends BaseActionHandler {
	final private static Logger log = LoggerFactory.getLogger(PickingListActionHandler.class);
	StringBuilder output = null;
	@Inject
	@Any
	private Instance<ProcessPLHook> processPLHooks;

	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {
		JSONObject jsonResponse = null;

		try {
			JSONObject jsonRequest = new JSONObject(content);
			final String strAction = jsonRequest.getString("action");
			final JSONArray plIds = jsonRequest.getJSONArray("pickinglist");
			if ("cancel".equals(strAction)) {
				jsonResponse = doCancel(plIds);
			} else if ("process".equals(strAction)) {
				jsonResponse = doProcess(plIds);
			} else if ("close".equals(strAction)) {
				jsonResponse = doClose(plIds);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			OBDal.getInstance().rollbackAndClose();

			try {
				jsonResponse = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();

				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", message);
				jsonResponse.put("message", errorMessage);

			} catch (Exception e2) {
				log.error(e.getMessage(), e2);
			}
		}

		return jsonResponse;
	}

	private JSONObject doProcess(JSONArray plIds) throws JSONException, ServletException {
		JSONObject jsonResponse = new JSONObject();
		// Get pickinglist
		for (int i = 0; i < plIds.length(); i++) {
			PickingList pl = OBDal.getInstance().get(PickingList.class, plIds.getString(i));
			int pl_lines = 0;
			pl_lines = pl.getMaterialMgmtShipmentInOutLineEMObwplPickinglistIDList().size();
			if (pl_lines > 0) {
				processPL(plIds.getString(i));
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "success");
				errorMessage.put("text", OBMessageUtils.messageBD("Success", false));
				jsonResponse.put("message", errorMessage);
			} else {
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", OBMessageUtils.messageBD("OBWPL_NoLines", false));
				jsonResponse.put("message", errorMessage);
			}
		}
		return jsonResponse;
	}

	private void processPL(String plID) throws ServletException {
		PickingList pl = OBDal.getInstance().get(PickingList.class, plID);
		if(!pl.isSvfpCompletepack())
			packAutomationProcess(pl);
		pl.setPickliststatus("CO");
		OBDal.getInstance().save(pl);
		OBDal.getInstance().flush();

	}

	private void packAutomationProcess(PickingList pickingList) throws ServletException  {
		OBContext.getOBContext().setAdminMode(true);
		Packing packing = OBProvider.getInstance().get(Packing.class);
		Locator locator = getLocatorFromStage("Prep");
		if(locator==null){
			throw new OBException("No Storage Bin with Movement Stage: PREP");
		}
		//Obteniendo los Shipment
		List<ShipmentInOutLine> inOuLines = pickingList.getMaterialMgmtShipmentInOutLineEMObwplPickinglistIDList();
		List<ShipmentInOut> inOuts = new ArrayList<ShipmentInOut>();
		BusinessPartner partner = null;
		Location location=null;
		ShipmentInOut inOut=null;
		for (ShipmentInOutLine shipmentInOutLine : inOuLines) {
			inOut=	shipmentInOutLine.getShipmentReceipt();
			Order order= 	inOut.getSalesOrder();
			order=OBDal.getInstance().get(Order.class, order.getId());
			partner=inOut.getBusinessPartner();
			location =inOut.getPartnerAddress();
			if(!order.isSvfpaManualHold()){
				inOuts.add(inOut);
				inOut.setObwpackPackingh(packing);

			}

		}
		//Create Packing

		packing.setBusinessPartner(partner);
		packing.setPartnerAddress(location);

		if(inOuts!=null && !inOuts.isEmpty()){
			packing.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().addAll(inOuts);
		}


		//Create Pack
		OBDal.getInstance().save(packing);
		OBDal.getInstance().flush();
		InternalMovement movement = OBProvider.getInstance().get(InternalMovement.class);


		movement.setOrganization(packing.getOrganization());
		movement.setName("Pack List Automation"+pickingList.getDocumentNo());
		movement.setMovementDate(new Date());


		OBDal.getInstance().save(movement);
		OBDal.getInstance().flush();
		OBCriteria<ShipmentInOutLine> critShipLines1 = OBDal.getInstance().createCriteria(
				ShipmentInOutLine.class);
		critShipLines1.add(Restrictions.in(ShipmentInOutLine.PROPERTY_SHIPMENTRECEIPT,
				inOuts));

		List<ShipmentInOutLine> lines = critShipLines1.list();
		for (ShipmentInOutLine shipmentInOutLine : lines) {

			Long lineNo = 10L;
			if(movement!=null){
				InternalMovementLine  line =  OBProvider.getInstance().get(InternalMovementLine.class);

				if(!shipmentInOutLine.getStorageBin().getId().equalsIgnoreCase(locator.getId())){
					line.setLineNo(lineNo);
					line.setProduct(shipmentInOutLine.getProduct());

					line.setStorageBin(shipmentInOutLine.getStorageBin());
					line.setNewStorageBin(locator);
					line.setMovement(movement);
					line.setMovementQuantity(shipmentInOutLine.getMovementQuantity());
					lineNo=lineNo+10L;
					line.setUOM(shipmentInOutLine.getProduct().getUOM());
					OBDal.getInstance().save(line);
				}
				OBDal.getInstance().flush();
			
			}

			shipmentInOutLine.setStorageBin(locator);
			OBDal.getInstance().save(shipmentInOutLine);


		}
		OBDal.getInstance().flush();
	

		OBCriteria<ShipmentInOutLine> critShipLines = OBDal.getInstance().createCriteria(
				ShipmentInOutLine.class);
		critShipLines.add(Restrictions.in(ShipmentInOutLine.PROPERTY_SHIPMENTRECEIPT,
				inOuts));
		ProjectionList projections = Projections.projectionList();
		projections.add(Projections.sum(ShipmentInOutLine.PROPERTY_MOVEMENTQUANTITY));
		projections.add(Projections.groupProperty(ShipmentInOutLine.PROPERTY_PRODUCT));
		critShipLines.setProjection(projections);
		boolean hasBoxes = false;
		if(inOut!=null){
			hasBoxes= !inOut.getOBWPACKBoxList().isEmpty();
		}
		@SuppressWarnings("rawtypes")
		List products = critShipLines.list();
		int queryCount = 0;
		int startbox = 1; 
		for (Object o : products) {

			final Object[] qryRecord = (Object[]) o;
			Product product = (Product) qryRecord[1];
			if (!"I".equals(product.getProductType())) {
				continue;
			}
			// not show items that are non-stocked BOM
			if ("I".equals(product.getProductType()) && product.isBillOfMaterials()
					&& !product.isStocked()) {
				continue;
			}
			queryCount++;
			
			startbox= makeBoxes(product, (BigDecimal)qryRecord[0],packing,inOut,startbox);
		

		}


		//Create MovementLine to Prep StoraegBin
		movement.setSvfpObwpackPackingh(packing);
		OBDal.getInstance().save(movement);
		OBDal.getInstance().flush();

		pickingList.setSvfpCompletepack(true);
		pickingList.setSvfpPackingh(packing);
		OBDal.getInstance().save(pickingList);

		OBDal.getInstance().flush();

		OBDal.getInstance().commitAndClose();

		if(output==null){
			output=new StringBuilder();
			output.append(packing.getDocumentNo());

		}
		else{
			output.append(", "+packing.getDocumentNo());
		}
		
		movement=(InternalMovement) OBDal.getInstance().getProxy(InternalMovement.ENTITY_NAME, movement.getId());
		if(movement.getMaterialMgmtInternalMovementLineList()==null||movement.getMaterialMgmtInternalMovementLineList().isEmpty()  ){
			OBDal.getInstance().remove(movement);
			OBDal.getInstance().flush();
		}
		OBContext.getOBContext().restorePreviousMode();


	}


	private int makeBoxes(Product product,BigDecimal quatity,Packing packing, ShipmentInOut shipmentInOut,int startbox) {

		Long  totalNumbeXBox	=product.getSvfpTotalnumbeperbox();
		if(totalNumbeXBox==null || totalNumbeXBox==0){
			totalNumbeXBox =quatity.longValue();
		}
		int quantityOfBox= quatity.divide(new  BigDecimal(totalNumbeXBox), 0, RoundingMode.CEILING).intValue();
		for (int i = 0; i < quantityOfBox; i++) {
			PackingBox box = OBProvider.getInstance().get(PackingBox.class);
			box.setBoxNumber(startbox+0L);
			box.setObwpackPackingh(packing);
			box.setOrganization(packing.getOrganization());
			box.setGoodsShipment(shipmentInOut);
			startbox=startbox+1;
			OBDal.getInstance().save(box);
			OBDal.getInstance().flush();
			PackingBoxProduct packingBoxProduct = OBProvider.getInstance().get(PackingBoxProduct.class);
			packingBoxProduct.setProduct(product);
			packingBoxProduct.setOrganization(packing.getOrganization());
			packingBoxProduct.setUOM(product.getUOM());
			BigDecimal decimal = new BigDecimal(0);
			if(quatity.compareTo(new BigDecimal(totalNumbeXBox))==1){
				decimal=quatity.subtract(new BigDecimal(totalNumbeXBox));
				quatity=decimal;
				packingBoxProduct.setQuantity(new BigDecimal(totalNumbeXBox));
			}else{
				packingBoxProduct.setQuantity(quatity);
			}
			packingBoxProduct.setPackingBox(box);
			OBDal.getInstance().save(packingBoxProduct);
			OBDal.getInstance().flush();
		}
		return quantityOfBox;
	}
	public Locator getLocatorFromStage(String stage) {
		Locator locator= null;
		OBCriteria<Locator> criteria = OBDal.getInstance().createCriteria(Locator.class);
		criteria.add(Restrictions.eq(Locator.PROPERTY_SVFPMOVEMTSTAGE, stage));
		//criteria.add(Restrictions.eq(Locator.PROPERTY_WAREHOUSE, OBContext.getOBContext().getWarehouse()));
		if(criteria.list()!=null&&! criteria.list().isEmpty())
			locator=criteria.list().get(0);
		return locator;

	}

	private JSONObject doCancel(JSONArray plIds) throws JSONException {
		JSONObject jsonResponse = new JSONObject();
		boolean processedShip = false;
		// Get orders
		for (int i = 0; i < plIds.length(); i++) {
			PickingList pl = OBDal.getInstance().get(PickingList.class, plIds.getString(i));
			ShipmentInOut ship = null;
			for (ShipmentInOutLine iol : pl.getMaterialMgmtShipmentInOutLineEMObwplPickinglistIDList()) {
				OrderLine oline = iol.getSalesOrderLine();
				oline.getSalesOrder().setObwplIsinpickinglist(false);
				ship = iol.getShipmentReceipt();
				// if shipment is completed or voided,
				// then picklist status is set to cancel
				if ("CO".equals(ship.getDocumentStatus()) || "VO".equals(ship.getDocumentStatus())) {
					pl.setPickliststatus("CA");
					processedShip = true;
					OBDal.getInstance().save(pl);
				} else {
					Reservation res = ReservationUtils.getReservationFromOrder(oline);
					if (res.isOBWPLGeneratedByPickingList()) {
						// If the reservation was created using picking list it has to be deleted.
						removeReservation(iol);
					}
					OBDal.getInstance().remove(iol);
				}
			}
			OBDal.getInstance().flush();
			if (!processedShip) {
				// Only remove ship that has no lines.
				pl.getMaterialMgmtShipmentInOutLineEMObwplPickinglistIDList().clear();
				OBDal.getInstance().remove(pl);
				if (ship != null)
					OBDal.getInstance().remove(ship);
				OBDal.getInstance().flush();
			}
		}
		JSONObject errorMessage = new JSONObject();
		errorMessage.put("severity", "success");
		errorMessage.put("text", OBMessageUtils.messageBD("Success", false));
		jsonResponse.put("message", errorMessage);
		return jsonResponse;
	}

	private void removeReservation(ShipmentInOutLine iol) {
		Reservation res = ReservationUtils.getReservationFromOrder(iol.getSalesOrderLine());
		String iolLocator = iol.getStorageBin().getId();
		String iolAttr = iol.getAttributeSetValue() == null ? "0" : iol.getAttributeSetValue().getId();
		BigDecimal iolQty = iol.getMovementQuantity();
		ReservationStock resStock = null;
		for (ReservationStock rSAux : res.getMaterialMgmtReservationStockList()) {
			String rSLocator = rSAux.getStorageBin().getId();
			String rSAttr = rSAux.getAttributeSetValue() == null ? "0" : rSAux.getAttributeSetValue()
					.getId();
			BigDecimal rSQty = rSAux.getQuantity();
			if (rSLocator.equals(iolLocator) && iolAttr.equals(rSAttr) && iolQty.compareTo(rSQty) == 0) {
				resStock = rSAux;
				break;
			}
		}
		if (resStock != null) {
			resStock.setQuantity(resStock.getQuantity().subtract(iol.getMovementQuantity()));
			if (resStock.getQuantity().compareTo(resStock.getReleased()) < 0) {
				resStock.setQuantity(resStock.getReleased());
			}
			if (resStock.getQuantity().equals(BigDecimal.ZERO)) {
				res.getMaterialMgmtReservationStockList().remove(resStock);
				// Flush to persist changes in database and execute triggers.
				OBDal.getInstance().flush();
				OBDal.getInstance().refresh(res);
				if (res.getReservedQty().equals(BigDecimal.ZERO)
						&& res.getMaterialMgmtReservationStockList().isEmpty()) {
					if (res.getRESStatus().equals("CO")) {
						// Unprocess reservation
						ReservationUtils.processReserve(res, "RE");
						OBDal.getInstance().save(res);
						OBDal.getInstance().flush();
					}
					OBDal.getInstance().remove(res);
				}
			}
		}
	}

	private JSONObject doClose(JSONArray plIds) throws JSONException {
		boolean hasErrors = false;
		StringBuffer finalMsg = new StringBuffer();
		for (int i = 0; i < plIds.length(); i++) {
			final String strPLId = plIds.getString(i);
			final PickingList pl = OBDal.getInstance().get(PickingList.class, strPLId);
			if (finalMsg.length() > 0) {
				finalMsg.append("\n");
			}
			finalMsg.append(pl.getDocumentNo() + ": ");
			OBError plMsg = OutboundPickingListProcess.close(pl);
			if (!"success".equals(plMsg.getType())) {
				hasErrors = true;
			}
			finalMsg.append(plMsg.getMessage());
		}
		JSONObject jsonResponse = new JSONObject();
		JSONObject jsonMsg = new JSONObject();
		jsonMsg.put("severity", hasErrors ? "error" : "success");
		jsonMsg.put("text", finalMsg.toString());
		jsonResponse.put("message", jsonMsg);

		return jsonResponse;
	}

	private void executeProcessPLHooks(PickingList pickingList, Set<String> orders,
			Set<String> shipments, List<Order> completedOrders) throws Exception {
		for (ProcessPLHook hook : processPLHooks) {
			hook.exec(pickingList, orders, shipments, completedOrders);
		}
	}
}
