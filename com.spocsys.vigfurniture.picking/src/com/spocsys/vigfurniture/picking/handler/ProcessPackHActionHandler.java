/************************************************************************************ 
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package com.spocsys.vigfurniture.picking.handler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.db.DbUtility;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.packing.PackingBox;
import org.openbravo.warehouse.pickinglist.PickingList;
import org.openbravo.warehouse.pickinglist.hooks.ProcessPLHook;

import com.spocsys.vigfurniture.picking.utils.HKBOMEntity;

public class ProcessPackHActionHandler extends BaseActionHandler {
	final private static Logger log = Logger.getLogger(ProcessPackHActionHandler.class);

	@Inject
	@Any
	private Instance<ProcessPLHook> processPLHooks;

	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {
		JSONObject jsonRequest = null;
		// JSONObject response = null;
		try {
			jsonRequest = new JSONObject(content);
			final String action = jsonRequest.getString("action");
			final JSONArray packHIds = jsonRequest.getJSONArray("packHeaders");
			for (int i = 0; i < packHIds.length(); i++) {
				processPackH(packHIds.getString(i), action);

			}

			JSONObject errorMessage = new JSONObject();
			errorMessage.put("severity", "TYPE_SUCCESS");
			errorMessage.put("title", OBMessageUtils.messageBD("Success"));
			errorMessage.put("text", OBMessageUtils.messageBD("OBWPACK_DocumeCom"));
			jsonRequest.put("message", errorMessage);

		} catch (Exception e) {
			log.error("Error in CreateActionHandler", e);
			OBDal.getInstance().rollbackAndClose();

			try {
				jsonRequest = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "TYPE_ERROR");
				errorMessage.put("text", message);
				jsonRequest.put("message", errorMessage);
			} catch (Exception e2) {
				log.error("Error generating the error message", e2);
				// do nothing, give up
			}
		}
		return jsonRequest;
	}

	private void processPackH(String strPackHId, String action) throws Exception {

		if ("processShip".equals(action)) {

			ShipmentInOut ship = OBDal.getInstance().get(ShipmentInOut.class, strPackHId);


			if ("CO".equals(ship.getObwpackProcessed())) {
				ship.setObwpackProcessed("DR");
				ship.setObwpackReactivated("DR");
				ship.getObwpackPackingh().setProcessed("DR");
				ship.getObwpackPackingh().setReactivated("DR");
				ship.getObwpackPackingh().setTotalboxes(null);
				ship.getObwpackPackingh().setTotalweight(null);
				ship.getObwpackPackingh().setUOM(null);
			} else {
				ship.setObwpackProcessed("CO");
				ship.setObwpackReactivated("CO");
				ship.getObwpackPackingh().setProcessed("CO");
				ship.getObwpackPackingh().setReactivated("CO");
				ship.getObwpackPackingh().setTotalboxes(
						0L + ship.getObwpackPackingh().getOBWPACKBoxList().size());
				BigDecimal w = BigDecimal.ZERO;
				for (PackingBox pb : ship.getObwpackPackingh().getOBWPACKBoxList()) {
					if (pb.getWeight() == null)
						throw new Exception(String.format(OBMessageUtils.messageBD("OBWPACK_noWeight"),
								pb.getIdentifier()));
					w = w.add(pb.getWeight());
				}
				ship.getObwpackPackingh().setTotalweight(w);
				ship.getObwpackPackingh().setUOM(null);
			}
			OBDal.getInstance().save(ship);

		} else if ("process".equals(action)) {


			Packing pack = OBDal.getInstance().get(Packing.class, strPackHId);
			//Changes to complete shipment in process complete pack

			processPL(pack);


			//End Change
			if ("CO".equals(pack.getProcessed())) {
				pack.setProcessed("DR");
				pack.setReactivated("DR");
				if (pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().size() == 1
						&& pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().get(0)
						.getOBWPACKBoxList().size() != 0
						&& pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().get(0)
						.getOBWPACKBoxList().get(0).getGoodsShipment() != null) {

					pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().get(0).getOBWPACKBoxList()
					.get(0).getGoodsShipment().setObwpackProcessed("DR");
					pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().get(0).getOBWPACKBoxList()
					.get(0).getGoodsShipment().setObwpackReactivated("DR");
				}
				pack.setTotalboxes(null);
				pack.setTotalweight(null);
				pack.setUOM(null);
			} else {
				pack.setProcessed("CO");
				pack.setReactivated("CO");
				if (pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().size() == 1
						&& pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().get(0)
						.getOBWPACKBoxList().size() != 0
						&& pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().get(0)
						.getOBWPACKBoxList().get(0).getGoodsShipment() != null) {

					pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().get(0).getOBWPACKBoxList()
					.get(0).getGoodsShipment().setObwpackProcessed("CO");
					pack.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().get(0).getOBWPACKBoxList()
					.get(0).getGoodsShipment().setObwpackReactivated("CO");

				}
				pack.setTotalboxes(0L + pack.getOBWPACKBoxList().size());
				BigDecimal w = BigDecimal.ZERO;
				boolean weightCalculated = false;
				for (PackingBox pb : pack.getOBWPACKBoxList()) {
					if (pb.isWeightcalculated()) {
						weightCalculated = pb.isWeightcalculated();
						w = w.add(pb.getWeight());
					}
				}
				if (weightCalculated) {
					pack.setTotalweight(w);
					pack.setUOM(pack.getOBWPACKBoxList().get(0).getUOM());
				}

			}
			OBDal.getInstance().save(pack);
			OBDal.getInstance().flush();
			if ("CO".equals(pack.getProcessed()))
		      	createBookedInvoice(pack);
//			else{
//				deletedInvoice(pack);
//			}
				
			
		} else {
			throw new Exception("Not valid option");
		}



	}


	private void processPL(Packing packing) {

		final Set<String> ships = new HashSet<String>();
		final Set<String> orders = new HashSet<String>();
		List<Order> completedOrders = new ArrayList<Order>();
		PickingList  pickingLists = new PickingList();
		List<InternalMovement> movements = packing.getMaterialMgmtInternalMovementEMSvfpObwpackPackinghIDList();
		for (InternalMovement internalMovement : movements) {

			final List<Object> param = new ArrayList<Object>();
			param.add(internalMovement);
			param.add(OBContext.getOBContext().getUser().getId());


			CallStoredProcedure.getInstance().call("svfp_m_movement_post", param, null, true, false);
		}
		List<ShipmentInOut> pl = packing.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList();


		for (ShipmentInOut shipmentInOut : pl) {


			for (ShipmentInOutLine iol : shipmentInOut.getMaterialMgmtShipmentInOutLineList()) {
				if (("DR").equals(iol.getShipmentReceipt().getDocumentStatus())) {
					ships.add(iol.getShipmentReceipt().getId());
				}
				if(iol.getSalesOrderLine()!=null)
					orders.add(iol.getSalesOrderLine().getSalesOrder().getId());

				pickingLists =iol.getObwplPickinglist();
			}
			for (String strShipId : ships) {
				final List<Object> param = new ArrayList<Object>();
				param.add(null);
				param.add(strShipId);
				CallStoredProcedure.getInstance().call("M_Inout_POST", param, null, true, false);
			}

			// pl.setPickliststatus("CO");
			for (String strOrderId : orders) {
				// check there is no orderline in other draft pickinglist
				StringBuilder whereClause = new StringBuilder();
				List<Object> parameters = new ArrayList<Object>();
				whereClause.append(" as iol ");
				whereClause.append(" where iol.");
				whereClause.append(ShipmentInOutLine.PROPERTY_OBWPLPICKINGLIST);
				whereClause.append("." + PickingList.PROPERTY_PICKLISTSTATUS + " = 'DR'");
				whereClause.append(" and ");
				whereClause.append(ShipmentInOutLine.PROPERTY_OBWPLPICKINGLIST);
				whereClause.append(" is not null ");
				if(pickingLists!=null){
					whereClause.append(" and ");
					whereClause.append(ShipmentInOutLine.PROPERTY_OBWPLPICKINGLIST);
					whereClause.append(" <> ? ");
					parameters.add(pickingLists);
				}

				whereClause.append(" and ");
				whereClause.append(ShipmentInOutLine.PROPERTY_SALESORDERLINE);
				whereClause.append("." + OrderLine.PROPERTY_ID + " IN (");
				whereClause.append(" SELECT ");
				whereClause.append(OrderLine.PROPERTY_ID);
				whereClause.append(" FROM OrderLine as ol");
				whereClause.append(" WHERE ol." + OrderLine.PROPERTY_SALESORDER + ".id = ?");
				parameters.add(strOrderId);
				whereClause.append(" )");
				OBQuery<ShipmentInOutLine> obData = OBDal.getInstance().createQuery(ShipmentInOutLine.class,
						whereClause.toString(), parameters);
				if (obData.count() == 0) {
					Order o = OBDal.getInstance().get(Order.class, strOrderId);
					o.setObwplIsinpickinglist(false);
					completedOrders.add(o);
				}
			}
			OBDal.getInstance().flush();

			try {
				executeProcessPLHooks(pickingLists, orders, ships, completedOrders);
			} catch (Exception e) {
				System.out.print(e.getStackTrace());
			}}
	}

//	private void deletedInvoice(Packing packing) {
//          Invoice invoice =packing.getSvfpCInvoice();
//		if(invoice!=null){
//		final List<Object> param = new ArrayList<Object>();
//		param.add(null);
//		param.add(invoice.getId());
//		CallStoredProcedure.getInstance().call("c_invoice_post", param, null, true, false);
//		OBDal.getInstance().remove(invoice);
//		OBDal.getInstance().flush();
//		OBDal.getInstance().commitAndClose();
//		}
//	}
	private void createBookedInvoice(Packing packing) {
		Organization organization = packing.getOrganization();
		DocumentType documentType = null;
		BusinessPartner currentBusinessPartner = packing.getBusinessPartner();
		OBCriteria<DocumentType> criteria = OBDal.getInstance().createCriteria(DocumentType.class);
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_ORGANIZATION, organization));
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, "ARI"));
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, true));
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_TABLE, getinvoiceTable()));
		List<ShipmentInOut> inOuts = packing.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList();
		List<DocumentType> list= criteria.list();
		if(list!=null&& !list.isEmpty())
			documentType=list.get(0);
		else
			throw new OBException("No Document Type AR Invoice for the organization: "+organization.getName() );
		for (ShipmentInOut shipmentInOut : inOuts) {
		Invoice invoice = OBProvider.getInstance().get(Invoice.class);
		invoice.setOrganization(packing.getOrganization());
		invoice.setBusinessPartner(currentBusinessPartner);
		invoice.setCurrency(currentBusinessPartner.getCurrency());
		invoice.setPaymentMethod(shipmentInOut.getSalesOrder()!=null?shipmentInOut.getSalesOrder().getPaymentMethod():currentBusinessPartner.getPaymentMethod());
		invoice.setPaymentTerms(shipmentInOut.getSalesOrder()!=null?shipmentInOut.getSalesOrder().getPaymentTerms():currentBusinessPartner.getPaymentTerms());
		invoice.setPriceList(currentBusinessPartner.getPriceList());
		invoice.setSalesTransaction(true);
		invoice.setDocumentType(documentType);
		invoice.setTransactionDocument(documentType);
		invoice.setAccountingDate(new Date());
		invoice.setSalesOrder(shipmentInOut.getSalesOrder());
		invoice.setGrandTotalAmount(shipmentInOut.getSalesOrder()!=null?shipmentInOut.getSalesOrder().getGrandTotalAmount():null);
		invoice.setSummedLineAmount(shipmentInOut.getSalesOrder()!=null?shipmentInOut.getSalesOrder().getSummedLineAmount():null);

		invoice.setInvoiceDate(new Date());
		
	
		invoice.setPartnerAddress(currentBusinessPartner.getBusinessPartnerLocationList()!=null&&!currentBusinessPartner.getBusinessPartnerLocationList().isEmpty()? currentBusinessPartner.getBusinessPartnerLocationList().get(0):null);
		OBDal.getInstance().save(invoice);
		OBDal.getInstance().flush();

		List<FIN_PaymentSchedule> payments= shipmentInOut.getSalesOrder().getFINPaymentScheduleList();
	    for (FIN_PaymentSchedule schedule : payments) {
	    	schedule.setInvoice(invoice);
		}
		OBDal.getInstance().save(invoice);
		OBDal.getInstance().flush();
		shipmentInOut.setInvoice(invoice);
		OBDal.getInstance().save(shipmentInOut);
		OBDal.getInstance().flush();
			List<ShipmentInOutLine> inOutLines = shipmentInOut.getMaterialMgmtShipmentInOutLineList();
			long lineNo=10L;
		 	List<HKBOMEntity> entities= new ArrayList<>();
		 	 Comparator<HKBOMEntity> c = new Comparator<HKBOMEntity>() {
		 	      public int compare(HKBOMEntity u1, HKBOMEntity u2) {
		 	        return u1.getProduct().getId().compareTo(u1.getProduct().getId());
		 	      }
		 	    };

		 	
			for (ShipmentInOutLine shipmentInOutLine : inOutLines) {
				Product product = shipmentInOutLine.getProduct();
				boolean b= product.isBOMVerified();
				//if(product.isSvfsvHardKit()){
					ProductBOM bom=null;	
				int index =Collections.binarySearch(entities, new HKBOMEntity(shipmentInOutLine,product,shipmentInOutLine.getMovementQuantity(),false), c);
				OBCriteria<ProductBOM> criteriaBOM = OBDal.getInstance().createCriteria(ProductBOM.class);
				criteriaBOM.add(Restrictions.eq(ProductBOM.PROPERTY_BOMPRODUCT, product));
				if(criteriaBOM.list()!=null && !criteriaBOM.list().isEmpty()){
					bom = criteriaBOM.list().get(0);
				}
				if(bom!=null && bom.getProduct().isSvfsvHardKit()){
				if(index<0){
					
						entities.add(new HKBOMEntity(shipmentInOutLine, bom.getProduct(),bom.getBOMQuantity() ,true));
					
					
				}else{
				
					
					HKBOMEntity hkbomEntity= entities.get(index);
					hkbomEntity.setQty(hkbomEntity.getQty().add(bom.getBOMQuantity()));
					}
					
				}else
					entities.add(new HKBOMEntity(shipmentInOutLine, product, shipmentInOutLine.getMovementQuantity(),false));
				
				
				/*}else{
				entities.add(new HKBOMEntity(shipmentInOutLine, product, shipmentInOutLine.getMovementQuantity(),false));
			}*/
				
				
			}
			
			for (HKBOMEntity hkbomEntity : entities) {
				
				ShipmentInOutLine shipmentInOutLine= hkbomEntity.getInOutLine();
				InvoiceLine line = OBProvider.getInstance().get(InvoiceLine.class);
				line.setProduct(hkbomEntity.getProduct());
				
				if(hkbomEntity.ishkparent())
				line.setInvoicedQuantity(shipmentInOutLine.getMovementQuantity().divide(hkbomEntity.getQty()));
				else
					line.setInvoicedQuantity(hkbomEntity.getQty());
				line.setListPrice(shipmentInOutLine.getSalesOrderLine()!=null?shipmentInOutLine.getSalesOrderLine().getListPrice(): null);
				line.setUnitPrice(shipmentInOutLine.getSalesOrderLine()!=null?shipmentInOutLine.getSalesOrderLine().getUnitPrice(): null);
				line.setLineNetAmount(shipmentInOutLine.getSalesOrderLine()!=null?shipmentInOutLine.getSalesOrderLine().getLineNetAmount(): null);
				line.setGoodsShipmentLine(shipmentInOutLine);
				line.setPriceLimit(shipmentInOutLine.getSalesOrderLine()!=null?shipmentInOutLine.getSalesOrderLine().getPriceLimit(): null);
				line.setSalesOrderLine(shipmentInOutLine.getSalesOrderLine());
				line.setInvoice(invoice);
				line.setTax(shipmentInOutLine.getSalesOrderLine()!=null?shipmentInOutLine.getSalesOrderLine().getTax(): null);
				line.setBusinessPartner(shipmentInOutLine.getBusinessPartner());
				line.setOrganization(organization);
				line.setGrossAmount(shipmentInOutLine.getSalesOrderLine()!=null?shipmentInOutLine.getSalesOrderLine().getLineGrossAmount(): null);
				line.setGrossListPrice(shipmentInOutLine.getSalesOrderLine()!=null?shipmentInOutLine.getSalesOrderLine().getGrossListPrice(): null);
				line.setGrossUnitPrice(shipmentInOutLine.getSalesOrderLine()!=null?shipmentInOutLine.getSalesOrderLine().getGrossUnitPrice(): null);
				line.setUOM(shipmentInOutLine.getSalesOrderLine()!=null? shipmentInOutLine.getSalesOrderLine().getUOM():shipmentInOutLine.getUOM() );
				line.setLineNo(lineNo);
				lineNo=+10L;
				OBDal.getInstance().save(line);	
			}

				


			
			OBDal.getInstance().flush();
			
			final List<Object> param = new ArrayList<Object>();
			param.add(null);
			param.add(invoice.getId());
			CallStoredProcedure.getInstance().call("c_invoice_post", param, null, true, false);
		}
		
		OBDal.getInstance().commitAndClose();

	
	}


private Table getinvoiceTable() {
	Table table = null;
	OBCriteria<Table> criteria = OBDal.getInstance().createCriteria(Table.class);
	criteria.add(Restrictions.eq(Table.PROPERTY_DBTABLENAME, "C_Invoice"));
	List<Table> list= criteria.list();
    if(list!=null && !list.isEmpty()){
    	table=list.get(0);
    }
return table;
}
	private void executeProcessPLHooks(PickingList pickingList, Set<String> orders,
			Set<String> shipments, List<Order> completedOrders) throws Exception {
		for (ProcessPLHook hook : processPLHooks) {
			hook.exec(pickingList, orders, shipments, completedOrders);
		}
	}
}
