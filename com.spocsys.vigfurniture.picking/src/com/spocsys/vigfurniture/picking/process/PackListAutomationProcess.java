package com.spocsys.vigfurniture.picking.process;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.ad.ui.Process;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InternalMovement;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.service.db.CallProcess;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.packing.PackingBox;
import org.openbravo.warehouse.packing.PackingBoxProduct;
import org.openbravo.warehouse.pickinglist.PickingList;

public class PackListAutomationProcess extends BaseActionHandler  {

	DalConnectionProvider provider = new DalConnectionProvider();
	StringBuilder output = null;
	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {

		JSONObject jsonRequest = null;

		// JSONObject response = null;

		Connection connection=OBDal.getInstance().getConnection();
		provider.setConnection(connection);
		try {
			jsonRequest = new JSONObject(content);

			final JSONArray packHIds = jsonRequest.getJSONArray("packHeaders");
			for (int i = 0; i < packHIds.length(); i++) {
				packAutomationProcess(packHIds.getString(i));
			}
			JSONObject errorMessage = new JSONObject();
			errorMessage.put("severity", "TYPE_SUCCESS");
			errorMessage.put("title", OBMessageUtils.messageBD("Success"));
			errorMessage.put("text", OBMessageUtils.messageBD("Packings Generated: "+output.toString() ));
			jsonRequest.put("message", errorMessage);

		}catch (Exception e) {


			OBDal.getInstance().rollbackAndClose();

			try {
				jsonRequest = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "TYPE_ERROR");
				errorMessage.put("text", message);
				jsonRequest.put("message", errorMessage);
			} catch (Exception e2) {

				// do nothing, give up
			}

		}
		return jsonRequest;
	}

	private void packAutomationProcess(String obwpl_pickinglist_id) throws ServletException  {

		OBContext.getOBContext().setAdminMode(true);
		PickingList pickingList =OBDal.getInstance().get(PickingList.class,obwpl_pickinglist_id);
		Packing packing = OBProvider.getInstance().get(Packing.class);
		Locator locator = getLocatorFromStage("Prep");
		if(locator==null){
			throw new OBException("No Storage Bin with Movement Stage: PREP");
		}
		List<ShipmentInOutLine> inOuLines = pickingList.getMaterialMgmtShipmentInOutLineEMObwplPickinglistIDList();
		List<ShipmentInOut> inOuts = new ArrayList<ShipmentInOut>();
		BusinessPartner partner = null;
		Location location=null;
		ShipmentInOut inOut=null;
		for (ShipmentInOutLine shipmentInOutLine : inOuLines) {
			inOut=	shipmentInOutLine.getShipmentReceipt();
			Order order= 	inOut.getSalesOrder();
			order=OBDal.getInstance().get(Order.class, order.getId());
			partner=inOut.getBusinessPartner();
			location =inOut.getPartnerAddress();
			if(!order.isSvfpaManualHold()){
				inOuts.add(inOut);
				inOut.setObwpackPackingh(packing);
			}
		}
		packing.setBusinessPartner(partner);
		packing.setPartnerAddress(location);
		if(inOuts!=null && !inOuts.isEmpty()){
			packing.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList().addAll(inOuts);
		}
		OBDal.getInstance().save(packing);
		OBDal.getInstance().flush();
		InternalMovement movement = OBProvider.getInstance().get(InternalMovement.class);
		movement.setOrganization(packing.getOrganization());
		movement.setName("Pack List Automation"+pickingList.getDocumentNo());
		movement.setMovementDate(new Date());
		OBDal.getInstance().save(movement);
		OBDal.getInstance().flush();
		OBCriteria<ShipmentInOutLine> critShipLines = OBDal.getInstance().createCriteria(
				ShipmentInOutLine.class);
		critShipLines.add(Restrictions.in(ShipmentInOutLine.PROPERTY_SHIPMENTRECEIPT,
				inOuts));
		ProjectionList projections = Projections.projectionList();
		projections.add(Projections.sum(ShipmentInOutLine.PROPERTY_MOVEMENTQUANTITY));
		projections.add(Projections.groupProperty(ShipmentInOutLine.PROPERTY_PRODUCT));
		projections.add(Projections.groupProperty(ShipmentInOutLine.PROPERTY_ID));
		projections.add(Projections.property(ShipmentInOutLine.PROPERTY_ID));
		critShipLines.setProjection(projections);
		boolean hasBoxes = false;
		if(inOut!=null){
			hasBoxes= !inOut.getOBWPACKBoxList().isEmpty();
		}
		@SuppressWarnings("rawtypes")
		List products = critShipLines.list();
		int queryCount = 0;
		int startbox = 1; 
		for (Object o : products) {

			final Object[] qryRecord = (Object[]) o;
			Product product = (Product) qryRecord[1];
			if (!"I".equals(product.getProductType())) {
				continue;
			}
			// not show items that are non-stocked BOM
			if ("I".equals(product.getProductType()) && product.isBillOfMaterials()
					&& !product.isStocked()) {
				continue;
			}
			queryCount++;
			Long lineNo = 10L;
			startbox= makeBoxes(product, (BigDecimal)qryRecord[0],packing,inOut,startbox);
			if(movement!=null){
				InternalMovementLine  line =  OBProvider.getInstance().get(InternalMovementLine.class);
				ShipmentInOutLine inOutLine= OBDal.getInstance().get(ShipmentInOutLine.class,qryRecord[2] );
				if(!inOutLine.getStorageBin().getId().equalsIgnoreCase(locator.getId())){
					line.setLineNo(lineNo);
					line.setProduct(product);

					line.setStorageBin(inOutLine.getStorageBin());
					line.setNewStorageBin(locator);
					line.setMovement(movement);
					line.setMovementQuantity((BigDecimal)qryRecord[0]);
					lineNo=lineNo+10L;
					line.setUOM(product.getUOM());
					OBDal.getInstance().save(line);
				}
				inOutLine.setStorageBin(locator);			
				OBDal.getInstance().save(inOutLine);

				OBDal.getInstance().flush();
			}
		}
		//Create MovementLine to Prep StoraegBin
		movement.setSvfpObwpackPackingh(packing);
		OBDal.getInstance().save(movement);
		OBDal.getInstance().flush();
		pickingList.setSvfpCompletepack(true);
		pickingList.setSvfpPackingh(packing);
		OBDal.getInstance().save(pickingList);
		OBDal.getInstance().flush();
		OBDal.getInstance().commitAndClose();
		if(output==null){
			output=new StringBuilder();
			output.append(packing.getDocumentNo());
		}
		else{
			output.append(", "+packing.getDocumentNo());
		}
		OBContext.getOBContext().restorePreviousMode();
	}


	private int makeBoxes(Product product,BigDecimal quatity,Packing packing, ShipmentInOut shipmentInOut,int startbox) {
		Long  totalNumbeXBox	=product.getSvfpTotalnumbeperbox();
		if(totalNumbeXBox==null || totalNumbeXBox==0){
			totalNumbeXBox =quatity.longValue();
		}
		int quantityOfBox= quatity.divide(new  BigDecimal(totalNumbeXBox), 0, RoundingMode.CEILING).intValue();
		for (int i = 0; i < quantityOfBox; i++) {
			PackingBox box = OBProvider.getInstance().get(PackingBox.class);
			box.setBoxNumber(startbox+0L);
			box.setObwpackPackingh(packing);
			box.setOrganization(packing.getOrganization());
			box.setGoodsShipment(shipmentInOut);
			startbox=startbox+1;
			OBDal.getInstance().save(box);
			OBDal.getInstance().flush();

			PackingBoxProduct packingBoxProduct = OBProvider.getInstance().get(PackingBoxProduct.class);
			packingBoxProduct.setProduct(product);
			packingBoxProduct.setOrganization(packing.getOrganization());
			packingBoxProduct.setUOM(product.getUOM());
			BigDecimal decimal = new BigDecimal(0);
			if(quatity.compareTo(new BigDecimal(totalNumbeXBox))==1){
				decimal=quatity.subtract(new BigDecimal(totalNumbeXBox));
				quatity=decimal;
				packingBoxProduct.setQuantity(new BigDecimal(totalNumbeXBox));
			}else{
				packingBoxProduct.setQuantity(quatity);
			}

			packingBoxProduct.setPackingBox(box);

			OBDal.getInstance().save(packingBoxProduct);

			OBDal.getInstance().flush();

		}
		return quantityOfBox;


	}

	public Locator getLocatorFromStage(String stage) {
		Locator locator= null;

		OBCriteria<Locator> criteria = OBDal.getInstance().createCriteria(Locator.class);

		criteria.add(Restrictions.eq(Locator.PROPERTY_SVFPMOVEMTSTAGE, stage));
		//criteria.add(Restrictions.eq(Locator.PROPERTY_WAREHOUSE, OBContext.getOBContext().getWarehouse()));


		if(criteria.list()!=null&&! criteria.list().isEmpty())
			locator=criteria.list().get(0);

		return locator;

	}

	private  void post_unpostProcessShipment(ShipmentInOutLine inOutLine){

		Process post = null;
		post = OBDal.getInstance().get(Process.class, "109");
		final ProcessInstance pinstance = CallProcess.getInstance().call(post,
				(String) DalUtil.getId(inOutLine.getShipmentReceipt()), null);
		if (pinstance.getResult() != 1) {
			throw new OBException(OBMessageUtils.parseTranslation(pinstance.getErrorMsg()));
		}

	}


}
