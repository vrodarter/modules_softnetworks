package com.spocsys.vigfurniture.picking.report;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.ResolutionSyntax;
import javax.print.attribute.standard.PrinterResolution;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.utility.JRFieldProviderDataSource;
import org.openbravo.erpCommon.utility.JRFormatFactory;
import org.openbravo.erpCommon.utility.PrintJRData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.utils.Replace;

public class ImagePrintTool extends HttpSecureAppServlet {

  public static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public JasperPrint renderlocalJR(VariablesSecureApp variables, HttpServletResponse response,
      String strReportName, String strOutputType, HashMap<String, Object> designParameters,
      FieldProvider[] data, Map<Object, Object> exportParameters) throws ServletException {
    return renderlocalJR(variables, response, strReportName, null, strOutputType, designParameters,
        data, exportParameters);
  }
  
  public JasperPrint renderinternalJR(VariablesSecureApp variables, HttpServletResponse response,
	      String strReportName, String strFileName, String strOutputType,
	      HashMap<String, Object> designParameters, FieldProvider[] data,
	      Map<Object, Object> exportParameters) throws ServletException {

	    if (strReportName == null || strReportName.equals(""))
	      strReportName = PrintJRData.getReportName(this, classInfo.id);

	    final String strAttach = globalParameters.strFTPDirectory + "/284-" + classInfo.id;
	    System.out.println(strAttach);
	    final String strLanguage = variables.getLanguage();
	    final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

	    final String strBaseDesign = getBaseDesignPath(strLanguage);

	    strReportName = Replace.replace(Replace.replace(strReportName, "@basedesign@", strBaseDesign),
	        "@attach@", strAttach);
	    System.out.println(strReportName);
	    if (strFileName == null) {
	      strFileName = strReportName.substring(strReportName.lastIndexOf("/") + 1);
	    }

	    ServletOutputStream os = null;
	    UUID reportId = null;
	    try {

	      final JasperReport jasperReport = Utility.getTranslatedJasperReport(this, strReportName,
	          strLanguage, strBaseDesign);
	      if (designParameters == null)
	        designParameters = new HashMap<String, Object>();

	      Boolean pagination = true;
	      if (strOutputType.equals("pdf"))
	        pagination = false;

	      designParameters.put("IS_IGNORE_PAGINATION", pagination);
	      designParameters.put("BASE_WEB", strReplaceWithFull);
	      designParameters.put("BASE_DESIGN", strBaseDesign);
	      designParameters.put("ATTACH", strAttach);
	      designParameters.put("USER_CLIENT", Utility.getContext(this, variables, "#User_Client", ""));
	      designParameters.put("USER_ORG", Utility.getContext(this, variables, "#User_Org", ""));
	      designParameters.put("LANGUAGE", strLanguage);
	      designParameters.put("LOCALE", locLocale);
	      designParameters.put("REPORT_TITLE",
	          PrintJRData.getReportTitle(this, variables.getLanguage(), classInfo.id));

	      final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
	      dfs.setDecimalSeparator(variables.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
	      dfs.setGroupingSeparator(variables.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
	      final DecimalFormat numberFormat = new DecimalFormat(
	          variables.getSessionValue("#AD_ReportNumberFormat"), dfs);
	      designParameters.put("NUMBERFORMAT", numberFormat);
	      if (log4j.isDebugEnabled())
	        log4j.debug("creating the format factory: " + variables.getJavaDateFormat());
	      final JRFormatFactory jrFormatFactory = new JRFormatFactory();
	      jrFormatFactory.setDatePattern(variables.getJavaDateFormat());
	      designParameters.put(JRParameter.REPORT_FORMAT_FACTORY, jrFormatFactory);
	      JasperPrint jasperPrint;
	      Connection con = null;
	      try {
	        con = getTransactionConnection();
	        if (data != null) {
	          designParameters.put("REPORT_CONNECTION", con);
	          jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters,
	              new JRFieldProviderDataSource(data, variables.getJavaDateFormat()));
	          System.out.println(variables.getJavaDateFormat());
	        } else {
	        	designParameters.put("REPORT_CONNECTION", con);
	          jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters, con);
	        }
	        return jasperPrint;
	      } catch (final Exception e) {
	        throw new ServletException(e.getCause() instanceof SQLException ? e.getCause().getMessage()
	            : e.getMessage(), e);
	      }
	    } catch (final JRException e) {
	      log4j.error("JR: Error: ", e);
	      throw new ServletException(e.getMessage(), e);
	    }
	  }
  
  public JasperPrint renderlocalJR(VariablesSecureApp variables, HttpServletResponse response,
      String strReportName, String strFileName, String strOutputType,
      HashMap<String, Object> designParameters, FieldProvider[] data,
      Map<Object, Object> exportParameters) throws ServletException {

    if (strReportName == null || strReportName.equals(""))
      strReportName = PrintJRData.getReportName(this, classInfo.id);

    final String strAttach = globalParameters.strFTPDirectory + "/284-" + classInfo.id;
    final String strLanguage = variables.getLanguage();
    final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

    final String strBaseDesign = getBaseDesignPath(strLanguage);

    strReportName = Replace.replace(Replace.replace(strReportName, "@basedesign@", strBaseDesign),
        "@attach@", strAttach);
    if (strFileName == null) {
      strFileName = strReportName.substring(strReportName.lastIndexOf("/") + 1);
    }

    ServletOutputStream os = null;
    UUID reportId = null;
    try {

      final JasperReport jasperReport = Utility.getTranslatedJasperReport(this, strReportName,
          strLanguage, strBaseDesign);
      if (designParameters == null)
        designParameters = new HashMap<String, Object>();

      Boolean pagination = true;
      if (strOutputType.equals("pdf"))
        pagination = false;

      designParameters.put("IS_IGNORE_PAGINATION", pagination);
      designParameters.put("BASE_WEB", strReplaceWithFull);
      designParameters.put("BASE_DESIGN", strBaseDesign);
      designParameters.put("ATTACH", strAttach);
      designParameters.put("USER_CLIENT", Utility.getContext(this, variables, "#User_Client", ""));
      designParameters.put("USER_ORG", Utility.getContext(this, variables, "#User_Org", ""));
      designParameters.put("LANGUAGE", strLanguage);
      designParameters.put("LOCALE", locLocale);
      designParameters.put("REPORT_TITLE",
          PrintJRData.getReportTitle(this, variables.getLanguage(), classInfo.id));

      final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
      dfs.setDecimalSeparator(variables.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
      dfs.setGroupingSeparator(variables.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
      final DecimalFormat numberFormat = new DecimalFormat(
          variables.getSessionValue("#AD_ReportNumberFormat"), dfs);
      designParameters.put("NUMBERFORMAT", numberFormat);
      if (log4j.isDebugEnabled())
        log4j.debug("creating the format factory: " + variables.getJavaDateFormat());
      final JRFormatFactory jrFormatFactory = new JRFormatFactory();
      jrFormatFactory.setDatePattern(variables.getJavaDateFormat());
      designParameters.put(JRParameter.REPORT_FORMAT_FACTORY, jrFormatFactory);
      JasperPrint jasperPrint;
      Connection con = null;
      try {
        con = getTransactionConnection();
        if (data != null) {
          designParameters.put("REPORT_CONNECTION", con);
          jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters,
              new JRFieldProviderDataSource(data, variables.getJavaDateFormat()));
        } else {

        	  designParameters.put("REPORT_CONNECTION", con);
          jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters, con);
        }
        return jasperPrint;
      } catch (final Exception e) {
        throw new ServletException(e.getCause() instanceof SQLException ? e.getCause().getMessage()
            : e.getMessage(), e);
      }
    } catch (final JRException e) {
      log4j.error("JR: Error: ", e);
      throw new ServletException(e.getMessage(), e);
    }
  }

  public void localPrintReports(HttpServletResponse response,
      Collection<JasperPrint> jrPrintReports, boolean directPrint) {
    ServletOutputStream os = null;
    try {
      os = response.getOutputStream();
 
        for (Iterator<JasperPrint> iterator = jrPrintReports.iterator(); iterator.hasNext();) {
          JasperPrint jasperPrint = (JasperPrint) iterator.next();
          // dpi format
          PrintRequestAttributeSet attrs = new HashPrintRequestAttributeSet();
          attrs.add(new PrinterResolution(203, 203, ResolutionSyntax.DPI));
          // dpi format
          response.setContentType("application/pdf");
          //response.setHeader("Content-disposition", "attachment" + "; filename=" + jasperPrint);
          if (directPrint) {
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRPdfExporterParameter.PDF_JAVASCRIPT, "this.print();");
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET,
                attrs);
            exporter.exportReport();
          } else {
        	
        	 
            JasperExportManager.exportReportToPdfStream(jasperPrint, os);
            
            String reportId = UUID.randomUUID().toString();
    		response.setContentType("text/html;charset=UTF-8");
    		response.setHeader("Content-disposition", "inline" + "; filename=" + jasperPrint.getName() + "-"
    				+ (reportId) + ".html");
    		
    	  printPagePopUpDownloadAndRefresh(os, jasperPrint.getName() + "-"
					+ (reportId) + "." + "pdf");
          }
        }
    } catch (IOException e) {
      log4j.error(e.getMessage());
    } catch (JRException e) {
      e.printStackTrace();
    } catch (ServletException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} finally {
      try {
        os.close();
        response.flushBuffer();
      } catch (IOException e) {
        log4j.error(e.getMessage(), e);
      }
    }
  }
  
  public void localImageReports(HttpServletResponse response,
	      Collection<JasperPrint> jrPrintReports, String packingId,String width,String height) {
	      // afirmar la validacion del reporte
	        for (Iterator<JasperPrint> iterator = jrPrintReports.iterator(); iterator.hasNext();) {
	          JasperPrint jasperPrint = (JasperPrint) iterator.next();
	          Image image;
				try {
					image = JasperPrintManager.printPageToImage(jasperPrint, 0, 2.0f);
				//	createImage(image,jasperPrint.getName(),packingId,width,height);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	  }
//  public void internalImageReports(Collection<JasperPrint> jrPrintReports, PackingBox packingId,String width,String height) {
//	      // afirmar la validacion del reporte
//	        for (Iterator<JasperPrint> iterator = jrPrintReports.iterator(); iterator.hasNext();) {
//	          JasperPrint jasperPrint = (JasperPrint) iterator.next();
//			try {
//				String path = null;
//				  path=packingId.getObwpackPackingh().getOrganization().getDamiPath();
//				  if(path == null)
//					  path = "/opt/shiplabels/";
//				  if(!new File(path).isDirectory())
//					  path = "/tmp/";
//				JRExporter exporter = new JRPdfExporter();
//				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//				String reportpdf =path+"/"+jasperPrint.getName() +"_"+ packingId.getTrackingNo()+ ".pdf";
//				System.out.println("Report name:"+reportpdf);
//				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, new FileOutputStream(reportpdf));
//				exporter.exportReport();
//			} catch (IOException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			} catch (JRException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//				try {
//					System.out.println("Jasper page index:"+jasperPrint.getPages().size());
//					for(int i=0 ; i < jasperPrint.getPages().size() ; i++ ){
//						//image.add(JasperPrintManager.printPageToImage(jasperPrint, i, 2.0f));
//						createBoxImage(JasperPrintManager.printPageToImage(jasperPrint, i, 2.0f),jasperPrint.getName()+"_"+i+"_",packingId,width,height);
//					}
//				} catch (JRException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//	        }
//	  }

  public boolean isDirectPrint(VariablesSecureApp vars) {
    return true;
  }
  
//  private void createImage(Image image,String imageName,String PackingID,String width,String height){
//	  try{
//		  
//		  MediaTracker mediaTracker = new MediaTracker(new Container());
//		  mediaTracker.addImage(image, 0);
//		  mediaTracker.waitForID(0);
//		  // determine thumbnail size from WIDTH and HEIGHT
//		  int thumbWidth = Integer.parseInt(width);// size in pixels
//		  int thumbHeight = Integer.parseInt(height);//size in pixles
//		  double thumbRatio = (double)thumbWidth / (double)thumbHeight;
//		  int imageWidth = image.getWidth(null);
//		  int imageHeight = image.getHeight(null);
//		  double imageRatio = (double)imageWidth / (double)imageHeight;
//		  System.out.println("thumbRatio:"+thumbRatio);
//		  System.out.println("imageRatio:"+imageRatio);
//		  if (thumbRatio < imageRatio) {
//			  thumbHeight = (int)(thumbWidth / imageRatio);
//		  } else {
//			  thumbWidth = (int)(thumbHeight * imageRatio);
//		  }
//		  BufferedImage thumbImage = new BufferedImage(thumbWidth,thumbHeight, BufferedImage.TYPE_INT_RGB);
//		  Graphics2D graphics2D = thumbImage.createGraphics();
//		  graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//		  graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
//		  File outputfile = new File("/tmp/" +imageName +PackingID+ ".png");
//		  ImageIO.write(thumbImage, "png", outputfile);
//		  org.openbravo.model.ad.utility.Image savePacking = OBProvider.getInstance().get(org.openbravo.model.ad.utility.Image.class);
//		  savePacking.setName(imageName +PackingID);
//		  savePacking.setBindaryData(read(outputfile));
//		  savePacking.setMimetype("image/png");
//		  
//		  
//		  OBDal.getInstance().save(savePacking);
//		  OBDal.getInstance().flush();
//		  OBDal.getInstance().refresh(savePacking);
//		  Packing packing = OBDal.getInstance().get(Packing.class, PackingID);
//		  for(PackingBox pbx : packing.getOBWPACKBoxList()){
//			  pbx.setDamiPackingslip(savePacking);
//			  OBDal.getInstance().save(pbx);
//		  }
//	  }
//	  catch(Exception ex){
//	  System.out.println("Jasper Error:\n"
//	  +"Check if the jrxml file is the right one or check that the parameters names idetical.\n"
//	  +"System message - " + ex.getMessage());
//	  }
//	  }
  
//  private PackingBox createBoxImage(Image image,String imageName,PackingBox pbx,String width,String height){
//	  try{
//		  MediaTracker mediaTracker = new MediaTracker(new Container());
//		  mediaTracker.addImage(image, 0);
//		  mediaTracker.waitForID(0);
//		  // determine thumbnail size from WIDTH and HEIGHT
//		  int thumbWidth = Integer.parseInt(width);// size in pixels
//		  int thumbHeight = Integer.parseInt(height);//size in pixles
//		  double thumbRatio = (double)thumbWidth / (double)thumbHeight;
//		  int imageWidth = image.getWidth(null);
//		  int imageHeight = image.getHeight(null);
//		  double imageRatio = (double)imageWidth / (double)imageHeight;
//		  if (thumbRatio < imageRatio) {
//			  thumbHeight = (int)(thumbWidth / imageRatio);
//		  } else {
//			  thumbWidth = (int)(thumbHeight * imageRatio);
//		  }
//		  BufferedImage thumbImage = new BufferedImage(thumbWidth,thumbHeight, BufferedImage.TYPE_BYTE_GRAY);
//		  //BufferedImage thumbImage = new BufferedImage(thumbWidth,thumbHeight, BufferedImage.TYPE_BYTE_BINARY);
//		  Graphics2D graphics2D = thumbImage.createGraphics();
//		  //graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//		  graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR
//				  );
//
//		  graphics2D.drawImage(image, 0, 0, thumbWidth, thumbHeight, null);
//		  //PackingBox pbx = OBDal.getInstance().get(PackingBox.class, PackingID);
//		  String path = null;
//		  path=pbx.getObwpackPackingh().getOrganization().getDamiPath();
//		  if(path == null)
//			  path = "/opt/shiplabels/";
//		  if(!new File(path).isDirectory())
//			  path = "/tmp/";
//		  File outputfile = new File(path +"/"+imageName +pbx.getTrackingNo()+ ".gif");
//		  ImageIO.write(thumbImage, "gif", outputfile);
//		  File outputfile1 = new File(path +imageName +pbx.getTrackingNo()+ ".png");
//		  ImageIO.write(thumbImage, "png", outputfile1);
//		  org.openbravo.model.ad.utility.Image savePacking = OBProvider.getInstance().get(org.openbravo.model.ad.utility.Image.class);
//		  savePacking.setName(imageName +pbx.getTrackingNo());
//		  savePacking.setBindaryData(read(outputfile1));
//		  savePacking.setMimetype("image/png");
//		  OBDal.getInstance().save(savePacking);
//		  ImageBoxList box = OBProvider.getInstance().get(ImageBoxList.class);
//		  box.setPackingBox(pbx);
//		  box.setPackslipImage(savePacking);
//		  box.setReportPath(path +imageName +pbx.getTrackingNo()+ ".gif");
//		  box.setReportName(outputfile.getName());
//		  box.setOrganization(pbx.getOrganization());
//		  OBDal.getInstance().save(box);
//		  return pbx;
//	  }
//	  catch(Exception ex){
//	  System.out.println("Jasper Error:\n"
//	  +"Check if the jrxml file is the right one or check that the parameters names idetical.\n"
//	  +"System message - " + ex.getMessage());
//	  }
//	  return pbx;
//	  }
  
  public byte[] read(File file) throws IOException, FileNotFoundException {
	    byte []buffer = new byte[(int) file.length()];
	    InputStream ios = null;
	    try {
	        ios = new FileInputStream(file);
	        if ( ios.read(buffer) == -1 ) {
	            throw new IOException("EOF reached while trying to read the whole file");
	        }        
	    } finally { 
	        try {
	             if ( ios != null ) 
	                  ios.close();
	        } catch ( IOException e) {
	        }
	    }

	    return buffer;
	}
  
 
  /*
   * public boolean isDirectPrint(VariablesSecureApp vars) { OBContext context =
   * OBContext.getOBContext(); String preferenceValue = ""; try { OBContext.setAdminMode(true);
   * String tabId = vars.getSessionValue("inpTabId"); Tab tab = OBDal.getInstance().get(Tab.class,
   * tabId); try { preferenceValue = Preferences.getPreferenceValue("DirectPrint", true,
   * context.getCurrentClient(), context.getCurrentOrganization(), context.getUser(),
   * context.getRole(), tab.getWindow()); } catch (PropertyException e) { return false; } } finally
   * { OBContext.restorePreviousMode(); } return "Y".equals(preferenceValue); }
   */
}
