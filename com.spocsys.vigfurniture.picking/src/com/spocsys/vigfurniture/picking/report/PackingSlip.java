package com.spocsys.vigfurniture.picking.report;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.List;
import java.util.Enumeration;
import java.lang.Thread;
import java.util.Arrays;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.lang.StringEscapeUtils;
import org.openbravo.base.HttpBaseUtils;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.PrintJRData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.service.db.DbUtility;
import org.openbravo.utils.FileUtility;
import org.openbravo.utils.Replace;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.warehouse.pickinglist.PickingList;
import org.openbravo.base.exception.OBException;

import com.spocsys.vigfurniture.zebra.utils.ReportingUtils;
import com.spocsys.vigfurniture.zebra.utils.ReportingUtils.ExportType;

import com.spocsys.vigfurniture.zebra.model.Label;
import com.spocsys.vigfurniture.zebra.service.ZebraPrinterService;

import fr.w3blog.zpl.model.ZebraLabel;



public class PackingSlip extends PrintTool {
  private static final long serialVersionUID = 1L;
  
  
  public void init(ServletConfig config) {
	    super.init(config);
	    boolHist = false;
	  }
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
  ServletException {
VariablesSecureApp vars = new VariablesSecureApp(request);
/*for (Enumeration<String> e = vars.getParameterNames(); e.hasMoreElements();){
        System.out.println("e.nextElement().......$$");
	System.out.println(e.nextElement());
	

}*/
if (vars.commandIn("DEFAULT")) {

   
      String type = vars.getStringParameter("type");
    
    try {
	  if(vars.getStringParameter("pack") != null && vars.getStringParameter("pack").length() > 1 && !"".equalsIgnoreCase(vars.getStringParameter("pack"))){
	      String array = vars.getStringParameter("pack");
	        String[] pickingArray = array.split(",");
	        String id ="";
	        for (int i = 0; i < pickingArray.length; i++) {
	        	printPagePDF(response, vars,pickingArray[i],type);
	        	id = pickingArray[i];
	        	
	        }
	       
	        
	      
	  }      
  } catch (Exception e) {
    OBContext.restorePreviousMode();
    try {
      Throwable ex = DbUtility.getUnderlyingSQLException(e);
      response.setContentType("text/html; charset=UTF-8");
      Writer writer;
      writer = response.getWriter();
      writer.write("<HTML><BODY><script type=\"text/javascript\">");
      writer.write("top.isc.say('" + StringEscapeUtils.escapeJavaScript(ex.getMessage())+ "');");
      writer.write("</SCRIPT></BODY></HTML>");
      e.printStackTrace();
    } catch (Exception ex) {
      log4j.error("Print", e);
    }
  }
} else
  pageError(response);
}

  

  private void printPagePDF(HttpServletResponse response, VariablesSecureApp vars,
	      String id, String type) throws IOException, ServletException {
	    for (Enumeration<String> e = vars.getParameterNames(); e.hasMoreElements();){
	      System.out.println("e.nextElement()");
	      System.out.println(e.nextElement());
	      System.out.println("vars.getStringParameter(e.nextElement())");
              System.out.println(vars.getStringParameter(e.nextElement()));
	      System.out.println("Thread.currentThread().getStackTrace()");
	      System.out.println(Arrays.toString(Thread.currentThread().getStackTrace()));
	    }

	    if (log4j.isDebugEnabled())
	      log4j.debug("printPagePDF Obwpack_Packingh_ID = " + id);
	    String strReportName="";
	    String fileName= "SlipReport";
	    if(type.equalsIgnoreCase("picking")){
	    strReportName = "@basedesign@/com/spocsys/vigfurniture/picking/report/SlipReport_full1.jrxml";
	    PickingList list = OBDal.getInstance().get(PickingList.class, id);
	    fileName+="_"+list.getDocumentNo();
	    
	    }else{
	    	 strReportName = "@basedesign@/com/spocsys/vigfurniture/picking/report/SlipReport_pack.jrxml";
	    	 Packing list = OBDal.getInstance().get(Packing.class, id);
	 	    fileName+="_"+list.getDocumentNo();
	    	 
	    }

	    List<Label> labelsToPrint = new ArrayList<Label>();
	    //labelsToPrint.addAll(printPacking(OBDal.getInstance().get(Packing.class, id)));
	    try {
		System.out.println("");
	    	//ZebraPrinterService.printLabelRestClient2(labelsToPrint);
 	    } catch (Exception e) {
	    	System.out.println(e);
	    }
	
	    fileName+="_"+Utility.formatDate(new Date(), "MM_dd_yyyy");
	    String strOutput = "pdf";
	    fileName= fileName.replaceAll(" ", "_");
	    fileName= fileName.replaceAll(":", "_");
	    fileName= fileName.replaceAll("-", "_");
	    HashMap<String, Object> parameters = new HashMap<String, Object>();
//	    String id = "'" + strObwpackPickingListId.replaceAll(",", "','") + "'";
//	    System.out.println(id);
	    parameters.put("id", id);
	    response.setHeader("Content-disposition", "inline; filename="+fileName);
	    Collection<JasperPrint> jrPrintReports = new ArrayList<JasperPrint>();
//	    JasperPrint report = renderlocalJR(vars, response, strReportName, strOutput, parameters, null,
//	        null);
//	    jrPrintReports.add(report);
//	    localPrintReports(response, jrPrintReports,true);
	    
	    renderJRCustom(vars, response, strReportName,fileName, strOutput, parameters, null, null, false);
	    //else
	      //renderJR(vars, response, strReportName, strOutput, parameters, null, null);
	  }


  private List<Label> printPacking(Packing packing)  {
      List<Label> labelsToPrint = new ArrayList<Label>();
      labelsToPrint.add(new Label(ZebraPrinterService.printPackingSlip(packing,"3").getZplCode(),1));                        
      return labelsToPrint;
 }
  
  
  private void renderJRCustom(VariablesSecureApp variables, HttpServletResponse response,
			String strReportName, String strFileName, String strOutputType,
			HashMap<String, Object> designParameters, JRDataSource data,
			Map<Object, Object> exportParameters, boolean forceRefresh) throws ServletException {
		if (strReportName == null || strReportName.equals(""))
			strReportName = PrintJRData.getReportName(this, classInfo.id);

		final String strAttach = globalParameters.strFTPDirectory + "/284-" + classInfo.id;

		final String strLanguage = variables.getLanguage();
		final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

		final String strBaseDesign = getBaseDesignPath(strLanguage);

		strReportName = Replace.replace(Replace.replace(strReportName, "@basedesign@", strBaseDesign),
				"@attach@", strAttach);
		if (strFileName == null) {
			strFileName = strReportName.substring(strReportName.lastIndexOf("/") + 1);
		}

		ServletOutputStream os = null;
		UUID reportId = null;
		try {
			if (designParameters == null)
				designParameters = new HashMap<String, Object>();

			designParameters.put("BASE_WEB", strReplaceWithFull);
			designParameters.put("BASE_DESIGN", strBaseDesign);
			designParameters.put("ATTACH", strAttach);
			designParameters.put("USER_CLIENT", Utility.getContext(this, variables, "#User_Client", ""));
			designParameters.put("USER_ORG", Utility.getContext(this, variables, "#User_Org", ""));
			designParameters.put("LANGUAGE", strLanguage);
			designParameters.put("LOCALE", locLocale);
			designParameters.put("REPORT_TITLE",
					PrintJRData.getReportTitle(this, variables.getLanguage(), classInfo.id));

			final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator(variables.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
			dfs.setGroupingSeparator(variables.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
			final DecimalFormat numberFormat = new DecimalFormat(
					variables.getSessionValue("#AD_ReportNumberFormat"), dfs);
			designParameters.put("NUMBERFORMAT", numberFormat);

			os = response.getOutputStream();
			if (exportParameters == null)
				exportParameters = new HashMap<Object, Object>();
			if (strOutputType == null || strOutputType.equals(""))
				strOutputType = "html";
			final ExportType expType = ExportType.getExportType(strOutputType.toUpperCase());

			if (strOutputType.equals("html")) {
				if (log4j.isDebugEnabled())
					log4j.debug("JR: Print HTML");
				response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "."
						+ strOutputType);
				HttpServletRequest request = RequestContext.get().getRequest();
				String localAddress = HttpBaseUtils.getLocalAddress(request);
				exportParameters.put(ReportingUtils.IMAGES_URI, localAddress + "/servlets/image?image={0}");
				ReportingUtils.exportJR(strReportName, expType, designParameters, os, false, this, data,
						exportParameters);
			} else if (strOutputType.equals("pdf") || strOutputType.equalsIgnoreCase("xls")
					|| strOutputType.equalsIgnoreCase("txt") || strOutputType.equalsIgnoreCase("csv")) {
				reportId = UUID.randomUUID();
				File outputFile = new File(globalParameters.strFTPDirectory + "/" + strFileName + "-"
						+ (reportId) + "." + strOutputType);
				ReportingUtils.exportJR(strReportName, expType, designParameters, outputFile, false, this,
						data, exportParameters);
				response.setContentType("text/html;charset=UTF-8");
				response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "-"
						+ (reportId) + ".html");
				if (forceRefresh) {
					printPagePopUpDownloadAndRefresh(response.getOutputStream(), strFileName + "-"
							+ (reportId) + "." + strOutputType);
				} else {
					printPagePopUpDownload(response.getOutputStream(), strFileName + "-" + (reportId) + "."
							+ strOutputType);
				}
			}

		} catch (IOException ioe) {
			try {
				FileUtility f = new FileUtility(globalParameters.strFTPDirectory, strFileName + "-"
						+ (reportId) + "." + strOutputType, false, true);
				if (f.exists())
					f.deleteFile();
			} catch (IOException ioex) {
				log4j.error("Error trying to delete temporary report file " + strFileName + "-"
						+ (reportId) + "." + strOutputType + " : " + ioex.getMessage());
			}
		} catch (final Exception e) {
			throw new ServletException(e.getMessage(), e);
		} finally {
			try {
				os.close();
			} catch (final Exception e) {
			}
		}
	}
  public String getServletInfo() {
    return "Servlet that presents the Packing Report";
  }
}
