package com.spocsys.vigfurniture.picking.report;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.ResolutionSyntax;
import javax.print.attribute.standard.PrinterResolution;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.utility.JRFieldProviderDataSource;
import org.openbravo.erpCommon.utility.JRFormatFactory;
import org.openbravo.erpCommon.utility.PrintJRData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.utils.Replace;

public class PrintTool extends HttpSecureAppServlet {

  public static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public JasperPrint renderlocalJR(VariablesSecureApp variables, HttpServletResponse response,
      String strReportName, String strOutputType, HashMap<String, Object> designParameters,
      FieldProvider[] data, Map<Object, Object> exportParameters) throws ServletException {
    return renderlocalJR(variables, response, strReportName, null, strOutputType, designParameters,
        data, exportParameters);
  }

  public JasperPrint renderlocalJR(VariablesSecureApp variables, HttpServletResponse response,
      String strReportName, String strFileName, String strOutputType,
      HashMap<String, Object> designParameters, FieldProvider[] data,
      Map<Object, Object> exportParameters) throws ServletException {

    if (strReportName == null || strReportName.equals(""))
      strReportName = PrintJRData.getReportName(this, classInfo.id);

    final String strAttach = globalParameters.strFTPDirectory + "/284-" + classInfo.id;

    final String strLanguage = variables.getLanguage();
    final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

    final String strBaseDesign = getBaseDesignPath(strLanguage);

    strReportName = Replace.replace(Replace.replace(strReportName, "@basedesign@", strBaseDesign),
        "@attach@", strAttach);
    if (strFileName == null) {
      strFileName = strReportName.substring(strReportName.lastIndexOf("/") + 1);
    }

    ServletOutputStream os = null;
    UUID reportId = null;
    try {

      final JasperReport jasperReport = Utility.getTranslatedJasperReport(this, strReportName,
          strLanguage, strBaseDesign);
      if (designParameters == null)
        designParameters = new HashMap<String, Object>();

      Boolean pagination = true;
      if (strOutputType.equals("pdf"))
        pagination = false;

      designParameters.put("IS_IGNORE_PAGINATION", pagination);
      designParameters.put("BASE_WEB", strReplaceWithFull);
      designParameters.put("BASE_DESIGN", strBaseDesign);
      designParameters.put("ATTACH", strAttach);
      designParameters.put("USER_CLIENT", Utility.getContext(this, variables, "#User_Client", ""));
      designParameters.put("USER_ORG", Utility.getContext(this, variables, "#User_Org", ""));
      designParameters.put("LANGUAGE", strLanguage);
      designParameters.put("LOCALE", locLocale);
      designParameters.put("REPORT_TITLE",
          PrintJRData.getReportTitle(this, variables.getLanguage(), classInfo.id));

      final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
      dfs.setDecimalSeparator(variables.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
      dfs.setGroupingSeparator(variables.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
      final DecimalFormat numberFormat = new DecimalFormat(
          variables.getSessionValue("#AD_ReportNumberFormat"), dfs);
      designParameters.put("NUMBERFORMAT", numberFormat);

      if (log4j.isDebugEnabled())
        log4j.debug("creating the format factory: " + variables.getJavaDateFormat());
      final JRFormatFactory jrFormatFactory = new JRFormatFactory();
      jrFormatFactory.setDatePattern(variables.getJavaDateFormat());
      designParameters.put(JRParameter.REPORT_FORMAT_FACTORY, jrFormatFactory);
      JasperPrint jasperPrint;
      Connection con = null;
      try {
        con = getTransactionConnection();
        if (data != null) {
          designParameters.put("REPORT_CONNECTION", con);
          jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters,
              new JRFieldProviderDataSource(data, variables.getJavaDateFormat()));
        } else {
          jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters, con);
        }
        return jasperPrint;
      } catch (final Exception e) {
        throw new ServletException(e.getCause() instanceof SQLException ? e.getCause().getMessage()
            : e.getMessage(), e);
      }
    } catch (final JRException e) {
      log4j.error("JR: Error: ", e);
      throw new ServletException(e.getMessage(), e);
    }
  }

  public void localPrintReports(HttpServletResponse response,
      Collection<JasperPrint> jrPrintReports, boolean directPrint) {
    ServletOutputStream os = null;
    String filename = "";
    try {
      os = response.getOutputStream();
      response.setContentType("application/pdf");
      // afirmar la validacion del reporte

      if ("Y".equalsIgnoreCase("Y")) {
        if (!directPrint) {
          response.setHeader("Content-disposition", "attachment" + "; filename=" + filename);
        }
        for (Iterator<JasperPrint> iterator = jrPrintReports.iterator(); iterator.hasNext();) {
          JasperPrint jasperPrint = (JasperPrint) iterator.next();
          // dpi format
          PrintRequestAttributeSet attrs = new HashPrintRequestAttributeSet();
          attrs.add(new PrinterResolution(203, 203, ResolutionSyntax.DPI));
          // dpi format
          if (directPrint) {
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRPdfExporterParameter.PDF_JAVASCRIPT, "this.print();");
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET,
                attrs);
            exporter.exportReport();
          } else {
            JasperExportManager.exportReportToPdfStream(jasperPrint, os);
          }
        }
      } else {
        response.setContentType("application/pdf");
        // concatReport(reports.toArray(new Report[] {}), response, directPrint);
        // Nos e concatena reporte
      }
    } catch (IOException e) {
      log4j.error(e.getMessage());
    } catch (JRException e) {
      e.printStackTrace();
    } finally {
      try {
        os.close();
        response.flushBuffer();
      } catch (IOException e) {
        log4j.error(e.getMessage(), e);
      }
    }
  }

  public boolean isDirectPrint(VariablesSecureApp vars) {
    return true;
  }
  /*
   * public boolean isDirectPrint(VariablesSecureApp vars) { OBContext context =
   * OBContext.getOBContext(); String preferenceValue = ""; try { OBContext.setAdminMode(true);
   * String tabId = vars.getSessionValue("inpTabId"); Tab tab = OBDal.getInstance().get(Tab.class,
   * tabId); try { preferenceValue = Preferences.getPreferenceValue("DirectPrint", true,
   * context.getCurrentClient(), context.getCurrentOrganization(), context.getUser(),
   * context.getRole(), tab.getWindow()); } catch (PropertyException e) { return false; } } finally
   * { OBContext.restorePreviousMode(); } return "Y".equals(preferenceValue); }
   */
}
