package com.spocsys.vigfurniture.picking.utils;

import java.math.BigDecimal;

import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

public class HKBOMEntity {
private ShipmentInOutLine inOutLine;

private Product  product;

private BigDecimal qty;

private boolean ishkparent;



public HKBOMEntity(ShipmentInOutLine inOutLine, Product product, BigDecimal qty,boolean ishkparent ) {
	super();
	this.inOutLine = inOutLine;
	this.product = product;
	this.qty = qty;
	this.ishkparent = ishkparent;
}

public HKBOMEntity() {
	
}

public ShipmentInOutLine getInOutLine() {
	return inOutLine;
}

public void setInOutLine(ShipmentInOutLine inOutLine) {
	this.inOutLine = inOutLine;
}

public Product getProduct() {
	return product;
}

public void setProduct(Product product) {
	this.product = product;
}

public BigDecimal getQty() {
	return qty;
}

public void setQty(BigDecimal qty) {
	this.qty = qty;
}

public boolean ishkparent() {
	return ishkparent;
}

public void setIshkparent(boolean ishkparent) {
	this.ishkparent = ishkparent;
}
	
	
}
