package com.spocsys.vigfurniture.picking.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

/**
 *
 * @author jme
 */
public class ImageBase64 {
  public static String encodeToString(BufferedImage image, String type) {
    String imageString = null;
    ByteArrayOutputStream bos = new ByteArrayOutputStream();

    try {
      ImageIO.write(image, type, bos);
      //byte[] imageBytes = bos.toByteArray();
      //BASE64Encoder encoder = new BASE64Encoder();
      //imageString = encoder.encode(imageBytes);
      imageString=Base64.encode(bos.toByteArray());

      bos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return imageString;
  }
  public static String encodeByteToString(byte[] imageByte) {
	    return  Base64.encode(imageByte);
	  }

  public static BufferedImage decodeToImage(String imageString) {

    BufferedImage image = null;
    byte[] imageByte;
    try {
      //BASE64Decoder decoder = new BASE64Decoder();
      //imageByte = decoder.decodeBuffer(imageString);
      imageByte =Base64.decode(imageString);
      ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
      image = ImageIO.read(bis);
      bis.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return image;
  }

  public static String encodeFileToBase64Binary(File file) throws IOException {
    //
    byte[] bytes = loadFile(file);
    // byte[] encoded = Base64.encodeBase64(bytes);
    //BASE64Encoder encoder = new BASE64Encoder();
    // imageString = encoder.encode(bytes);
    //byte[] bytearray =Base64.decode(bytes);
    //String encodedString = encoder.encode(bytes);
    return Base64.encode(bytes);
  }

  public static byte[] loadFile(File file) throws IOException {
    InputStream is = new FileInputStream(file);
    long length = file.length();
    if (length > Integer.MAX_VALUE) {
      // File is too large
    }
    byte[] bytes = new byte[(int) length];
    int offset = 0;
    int numRead = 0;
    while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
      offset += numRead;
    }
    if (offset < bytes.length) {
      throw new IOException("Could not completely read file " + file.getName());
    }
    is.close();
    return bytes;
  }

}