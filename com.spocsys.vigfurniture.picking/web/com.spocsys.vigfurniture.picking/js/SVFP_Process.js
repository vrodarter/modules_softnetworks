/************************************************************************************
 * Copyright (C) 2012-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
OB.SVFP = OB.SVFP || {};
OB.SVFP.Process = {
  assignProcess: function (params, view, doGroup) {
    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
        pickings = [],
        org = null;

    for (i = 0; i < selection.length; i++) {
      pickings.push(selection[i].id);
    }
    if (selection.length === 1) {
      doGroup = false;
    }

    isc.SVFP_AssignPopup.create({
      pickings: pickings,
      view: view,
      params: params,
      organization: org,
      doGroup: doGroup
    }).show();
  },
   movementProcess: function (params, view, doGroup) {
   
   
       var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
       
       
       packings = [],
        org = selection[0].organization;

    for (i = 0; i < selection.length; i++) {
        
        packings.push(selection[i].id);
      }
 
    
    if (selection.length === 1) {
      doGroup = false;
    }

    isc.SVFP_StagePopup.create({
    packings: packings,
      view: view,
      params: params,
      organization: org,
      doGroup: doGroup
    }).show();
  },

  movement: function (params, view) {
    OB.SVFP.Process.movementProcess(params, view, true)
    
      },
  assign: function (params, view) {
    OB.SVFP.Process.assignProcess(params, view, true);
  },
  reassign: function (params, view) {
    OB.SVFP.Process.assignProcess(params, view, false);
  }, 
  
  process: function (params, view) {
    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
        packHeaders = [],
        callback;
    callback = function (rpcResponse, data, rpcRequest) {
      var message = data.message;
      view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
      // close process to refresh current view
      params.button.closeProcessPopup();
    };
    for (i = 0; i < selection.length; i++) {
      packHeaders.push(selection[i].id);
    }
    isc.confirm(OB.I18N.getLabel('OBWPACK_CreateConfirm'), function (clickedOK) {
      if (clickedOK) {
        OB.RemoteCallManager.call('com.spocsys.vigfurniture.picking.process.PackListAutomationProcess', {
          packHeaders: packHeaders,
          action: 'process'
        }, {}, callback);
      }
    });
  }
 ,
 validate: function (params, view) {
	    var recordId = params.button.contextView.viewGrid.getSelectedRecords()[0].id,
	        processOwnerView = view.getProcessOwnerView(params.processId),
	        processAction, callback;

	    processAction = function () {
	    	OB.SVFP.Process.processPickList(params, view);
	    };
	    callback = function (rpcResponse, data, rpcRequest) {
	      var processLayout, popupTitle;
	      if (!data) {
	        view.activeView.messageBar.setMessage(isc.OBMessageBar.TYPE_ERROR, null, null);
	        return;
	      }
	      if (data.message || !data.data) {
	        view.activeView.messageBar.setMessage(isc.OBMessageBar[data.message.severity], null, data.message.text);
	        return;
	      }
	      popupTitle = OB.I18N.getLabel('OBWPL_PickingList') + ' - ' + params.button.contextView.viewGrid.getSelectedRecords()[0]._identifier;
	      processLayout = isc.OBPickValidateProcess.create({
	        parentWindow: view,
	        sourceView: view.activeView,
	        buttonOwnerView: processOwnerView,
	        pickGridData: data.data,
	        processAction: processAction
	      });
	      view.openPopupInTab(processLayout, popupTitle, OB.Styles.OBWPL.PickValidateProcess.popupWidth, OB.Styles.OBWPL.PickValidateProcess.popupHeight, true, true, true, true);
	    };
	    OB.RemoteCallManager.call('com.spocsys.vigfurniture.picking.handler.ValidateActionHandler', {
	      recordId: recordId,
	      action: 'validate'
	    }, {}, callback);
	  },
	  
	  pickingHandlerCall: function (params, view, action, confirmMsg) {
		    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
		        pickinglist = [],
		        callback;

		    callback = function (rpcResponse, data, rpcRequest) {
		      var message = data.message,
		          processView = view.getProcessOwnerView(params.processId);
		      processView.messageBar.setMessage(message.severity, message.title, message.text);
		      if (action === 'cancel') {
		        // refresh the whole grid after executing the process
		        params.button.contextView.viewGrid.refreshGrid();
		      } else {
		        // close process to refresh current view (just refreshes the record from which the process has been invoked)
		        params.button.closeProcessPopup();
		      }
		      isc.clearPrompt();
		    };
		    for (i = 0; i < selection.length; i++) {
		      pickinglist.push(selection[i].id);
		    }
		    isc.confirm(OB.I18N.getLabel(confirmMsg), function (clickedOK) {
		      if (clickedOK) {
		        isc.showPrompt(OB.I18N.getLabel('OBUIAPP_PROCESSING') + isc.Canvas.imgHTML({
		          src: OB.Styles.LoadingPrompt.loadingImage.src
		        }));
		        OB.RemoteCallManager.call('com.spocsys.vigfurniture.picking.handler.PickingListActionHandler', {
		          pickinglist: pickinglist,
		          action: action
		        }, {}, callback);
		      }
		    });
		  },
		  processPickList: function (params, view) {
			  OB.SVFP.Process.pickingHandlerCall(params, view, 'process', 'OBWPL_ProcessConfirm');
			  },
		  
		  processPack: function (params, view) {
			    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
			        packHeaders = [],
			        callback;
			    callback = function (rpcResponse, data, rpcRequest) {
			      var message = data.message;
			      view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
			      // close process to refresh current view
			      params.button.closeProcessPopup();
			    };
			    for (i = 0; i < selection.length; i++) {
			      packHeaders.push(selection[i].id);
			    }
			    isc.confirm(OB.I18N.getLabel('OBWPACK_CreateConfirm'), function (clickedOK) {
			      if (clickedOK) {
			        OB.RemoteCallManager.call('com.spocsys.vigfurniture.picking.handler.ProcessPackHActionHandler', {
			          packHeaders: packHeaders,
			          action: 'process'
			        }, {}, callback);
			      }
			    });
			  }

};