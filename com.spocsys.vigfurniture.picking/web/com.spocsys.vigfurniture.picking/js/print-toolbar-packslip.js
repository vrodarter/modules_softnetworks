/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011-2012 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  _________
 ************************************************************************
 */

// put within a function to hide local vars etc.
(function () {
  var buttonProps2 = {
    action: function () {
    	
      var callback, pickid = [],
          i, view = this.view,
          grid = view.viewGrid,
          selectedRecords = grid.getSelectedRecords();
      for (i = 0; i < selectedRecords.length; i++) {
    	  pickid.push(selectedRecords[i].id);
      }
      // define the callback function which shows the result to the user
      OB.Utilities.postThroughHiddenForm('./com.spocsys.vigfurniture.picking.report/PackingSlip.pdf', {pack: pickid.toString(),type : 'packing'});
      //var quantity = prompt("Please enter the number of copies", "1");
      //var result = parseInt(quantity,10);
      callback = function (rpcResponse, data, rpcRequest) {
    	  if (data.status == "ok") {
    		  var array_servlet = data.server;
    		  for (i = 0; i < array_servlet.length; i++) {
    			  //for(j=1; j <= result;j++){
    				  var myWindow = window.open(array_servlet[i],"_blank","width=300,height=400");
    				  setTimeout(function(){ myWindow.close() }, 2000);
    			  //}
    		  }
    		  isc.say(data.message);
    		}else{
    			isc.say(data.message);
    		}
      };
      
    },
    buttonType: 'dami_print6',
    prompt: OB.I18N.getLabel('SVFP_Print6'),
    updateState: function () {
        var view = this.view,
            form = view.viewForm,
            grid = view.viewGrid;
        var selectedRecords = grid.getSelectedRecords();
        var disabled = false;
        if (selectedRecords.length === 0 || selectedRecords.length > 1 ) {
          disabled = true;
        }
        if (this.view.viewGrid.getTotalRows() === 0) {
          disabled = true;
        }
        if (view.isShowingForm && form.isNew) {
          disabled = true;
        }
        this.setDisabled(disabled);      
    }
  };

  
  OB.ToolbarRegistry.registerButton(buttonProps2.buttonType, isc.OBToolbarIconButton, buttonProps2, 185, 'E76A4C336626487296D862207E78317A');
}());