/************************************************************************************
 * Copyright (C) 2013-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
isc.defineClass('SVFP_StagePopup', isc.OBPopup);

isc.SVFP_StagePopup.addProperties({

  width: 320,
  height: 200,
  title: null,
  showMinimizeButton: false,
  showMaximizeButton: false,

  mainform: null,
  okButton: null,
  cancelButton: null,
  pickings: null,
  organization: null,
  doGroup: null,
  view: null,
  params: null,

  getStageList: function (form) {
    var send = {},
        stageField, popup = this;
    send.packings = this.packings;
    send.action = 'getstage';
    send.organization = this.organization;
    OB.RemoteCallManager.call('com.spocsys.vigfurniture.picking.baseprocess.MovementStage', send, {}, function (response, data, request) {
      if (response) {
    	  stageField = form.getField('Stage');
        if (response.data) {
        	stageField.setValueMap(response.data.valuecheck.valueMap);
        	stageField.setDefaultValue(response.data.valuecheck.defaultValue);
        }
      }
    });
  },

  initWidget: function () {

    var packings = this.packings,
        originalView = this.view,
        params = this.params;

    if (this.doGroup) {
      this.mainform = isc.DynamicForm.create({
        numCols: 2,
        colWidths: ['50%', '50%'],
        fields: [{
          name: 'Stage',
          height: 20,
          width: 255,
          required: true,
          type: '_id_17',
          defaultToFirstOption: true
        }]
      });
    } else {
      this.mainform = isc.DynamicForm.create({
        numCols: 2,
        colWidths: ['50%', '50%'],
        fields: [{
          name: 'Stage',
          height: 20,
          width: 255,
          required: true,
          type: '_id_17',
          defaultToFirstOption: true
        }]
      });
    }
    this.setTitle(OB.I18N.getLabel('SVFP_Movement'));

    this.okButton = isc.OBFormButton.create({
      title: OB.I18N.getLabel('OBUISC_Dialog.OK_BUTTON_TITLE'),
      popup: this,
      action: function () {
        var callback, action = false;

        callback = function (rpcResponse, data, rpcRequest) {
          var status = rpcResponse.status,
              context = rpcRequest.clientContext,
              view = context.originalView.getProcessOwnerView(context.popup.params.processId);
          if (data.message) {
            view.messageBar.setMessage(data.message.severity, data.message.title, data.message.text);

          }
          rpcRequest.clientContext.popup.closeClick();
          rpcRequest.clientContext.originalView.refresh(false, false);
        };

        action = this.popup.mainform.getItem('Stage').getValue();
        

        OB.RemoteCallManager.call('com.spocsys.vigfurniture.picking.baseprocess.MovementStage', {
        	packings: packings,
          action: action
        }, {}, callback, {
          originalView: this.popup.view,
          popup: this.popup
        });
      }
    });

    this.cancelButton = isc.OBFormButton.create({
      title: OB.I18N.getLabel('OBUISC_Dialog.CANCEL_BUTTON_TITLE'),
      popup: this,
      action: function () {
        this.popup.closeClick();
      }
    });

    this.getStageList(this.mainform);

    this.items = [
    isc.VLayout.create({
      defaultLayoutAlign: "center",
      align: "center",
      width: "100%",
      layoutMargin: 10,
      membersMargin: 6,
      members: [
      isc.HLayout.create({
        defaultLayoutAlign: "center",
        align: "center",
        layoutMargin: 30,
        membersMargin: 6,
        members: this.mainform
      }), isc.HLayout.create({
        defaultLayoutAlign: "center",
        align: "center",
        membersMargin: 10,
        members: [this.okButton, this.cancelButton]
      })]
    })];

    this.Super('initWidget', arguments);
  }

});