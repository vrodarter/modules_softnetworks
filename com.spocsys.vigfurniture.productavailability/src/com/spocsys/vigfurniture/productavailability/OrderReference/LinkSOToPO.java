package com.spocsys.vigfurniture.productavailability.OrderReference;

import java.math.BigDecimal;

import org.hibernate.criterion.Restrictions;
import org.jfree.util.Log;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.OrderLine;

import com.spocsys.vigfurniture.incomingproduct.OrderReference;

public class LinkSOToPO {

  public LinkSOToPO() {
    // TODO Auto-generated constructor stub
  }

  public static OrderLine getPurchaseLine(OrderLine regSalesOrderLine) {

    try {

      OBCriteria<OrderLine> obc = OBDal.getInstance().createCriteria(OrderLine.class);
      obc.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER + ".id", regSalesOrderLine
          .getSvfpaPurchaseorder().getId()));
      obc.add(Restrictions.eq(OrderLine.PROPERTY_PRODUCT + ".id", regSalesOrderLine.getProduct()
          .getId()));

      if ((obc != null) && (obc.list().size() > 0)) {
        return obc.list().get(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e.getMessage());
    }
    return null;

  }

  public static OrderReference create(OrderLine regSalesOrderLine, OrderLine regPurchaseOrderLine,
      BigDecimal Qty) {

    OrderLine regPurchaseOrderLineNew = regPurchaseOrderLine;

    if (regPurchaseOrderLineNew == null) {
      regPurchaseOrderLineNew = getPurchaseLine(regSalesOrderLine);
    }

    OrderReference regLinkSOToPO = null;

    if (regPurchaseOrderLineNew != null) {

      OBCriteria<OrderReference> obc = OBDal.getInstance().createCriteria(OrderReference.class);
      obc.add(Restrictions.eq(OrderReference.PROPERTY_SALESORDER + ".id", regSalesOrderLine
          .getSalesOrder().getId()));
      obc.add(Restrictions.eq(OrderReference.PROPERTY_SALESORDERLINE + ".id",
          regPurchaseOrderLineNew.getId()));
      obc.add(Restrictions.eq(OrderReference.PROPERTY_ORDERLINEREF + ".id",
          regSalesOrderLine.getId()));

      if ((obc != null) && (obc.count() > 0)) {
        regLinkSOToPO = obc.list().get(0);
      } else {

        regLinkSOToPO = OBProvider.getInstance().get(OrderReference.class);
        regLinkSOToPO.setClient(regPurchaseOrderLineNew.getClient());
        regLinkSOToPO.setOrganization(regPurchaseOrderLineNew.getOrganization());
        regLinkSOToPO.setCreatedBy(OBContext.getOBContext().getUser());
        regLinkSOToPO.setUpdatedBy(OBContext.getOBContext().getUser());
        regLinkSOToPO.setOrderlineref(regSalesOrderLine);
        regLinkSOToPO.setSalesOrderLine(regPurchaseOrderLineNew);
        regLinkSOToPO.setSalesOrder(regSalesOrderLine.getSalesOrder());

      }

      regLinkSOToPO.setQuantityAllocated(Qty);
    }
    return regLinkSOToPO;

  }
}
