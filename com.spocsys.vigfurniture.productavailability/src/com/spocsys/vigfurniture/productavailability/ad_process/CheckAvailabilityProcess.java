package com.spocsys.vigfurniture.productavailability.ad_process;

import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;

import com.spocsys.vigfurniture.productavailability.erpCommon.ad_callouts.ProductAvailabilityProcess;

public class CheckAvailabilityProcess extends BaseActionHandler {

  protected JSONObject execute(Map<String, Object> parameters, String data) {

    try {
      final JSONObject JSONData = new JSONObject(data);
      String OrderID = JSONData.getString("OrderID");

      Order SalesOrder = OBDal.getInstance().get(Order.class, OrderID);

      ProductAvailabilityProcess PAvailability = new ProductAvailabilityProcess();
      JSONObject Result = PAvailability.CheckAvailabilityProcess(SalesOrder);

      JSONObject JsonReturn = new JSONObject();

      if (Result != null) {
        JsonReturn.put("result", Result);
      } else {
        JsonReturn.put("result", "nook");
      }
      JsonReturn.put("error", "");

      return JsonReturn;

    } catch (Exception e) {
      throw new OBException(e);
    }

  }
}