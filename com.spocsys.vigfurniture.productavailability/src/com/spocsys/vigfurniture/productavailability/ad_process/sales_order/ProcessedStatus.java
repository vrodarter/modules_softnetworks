package com.spocsys.vigfurniture.productavailability.ad_process.sales_order;

import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.quartz.JobExecutionException;

public class ProcessedStatus extends DalBaseProcess {

	private ProcessLogger logger;

	// begin to background process
	public void doExecute(ProcessBundle bundle) throws Exception {
		// this logger logs into the LOG column of
		// the AD_PROCESS_RUN database table
		int counter = 0;
		logger = bundle.getLogger();
		logger.log("Starting background search order non Processed ");
		//

		try {

			final OBCriteria<Order> orderList = OBDal.getInstance().createCriteria(Order.class);
			// IsSOTrx = 'Y' and em_svfpa_processed = 'N'
			orderList.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true));
			orderList.add(Restrictions.eq(Order.PROPERTY_SVFPAPROCESSED, false));
			//
			logger.log("No of Orders = " + orderList.list().size() + "\n");
			// loop through all order
			for (Order order : orderList.list()) {
				if (Sales_Processed_Status(order.getId())) {
					logger.log("Document No: " + order.getDocumentNo() + "\n");
					counter++;
				}
			} // for
			logger.log("Processed " + counter);
		} catch (Exception e) {
			// catch any possible exception and throw it as a Quartz
			// JobExecutionException
			throw new JobExecutionException(e.getMessage(), e);
		}
	}

	public Boolean Sales_Processed_Status(String salesOrderNO) {
		Boolean isProcess = false;
		final String hql = "select count(" + OrderLine.PROPERTY_SVFPAPROCESSED + ") as noReg, COALESCE(sum(case "
				+ OrderLine.PROPERTY_SVFPAPROCESSED + " when 'N' then 1 else 0 end),0) as NO, " + "COALESCE(sum(case "
				+ OrderLine.PROPERTY_SVFPAPROCESSED + " when 'Y' then 1 else 0 end), 0) as SI " + " from "
				+ OrderLine.ENTITY_NAME + " where " + OrderLine.PROPERTY_SALESORDER + ".id = :porder_id and "
				+ OrderLine.PROPERTY_ACTIVE + " = 'Y' ";

		final Query query = OBDal.getInstance().getSession().createQuery(hql);
		query.setParameter("porder_id", salesOrderNO);
		//
		Long countReg = Long.valueOf("0");
		Long qtyNonProcess = Long.valueOf("0");
		Long qtyProcess = Long.valueOf("0");
		//
		for (Object ol : query.list()) {
			// the query returns a list of arrays (columns of the query)
			final Object[] os = (Object[]) ol;
			// get element of result
			countReg = (Long) os[0]; // as noReg
			qtyNonProcess = (Long) os[1]; // as NO
			qtyProcess = (Long) os[2]; // as SI
			//
			// logger.log(countReg.toString() + "|" + qtyProcess.toString() +
			// "|");
			// System.out.println(salesOrderNO + "|" + countReg.toString() + "|"
			// + qtyNonProcess.toString() + "|"
			// + qtyProcess.toString());
			if (countReg.compareTo(qtyProcess) == 0 && countReg.compareTo(Long.valueOf("0")) > 0) {
				isProcess = true;
			}
		} // for

		Order objOrder = OBDal.getInstance().get(Order.class, salesOrderNO);
		//
		if (!objOrder.get(Order.PROPERTY_SVFPAPROCESSED).equals(isProcess)) {
			objOrder.setSvfpaProcessed(isProcess);
		}
		return isProcess;
	}// Sales_Order_Stock_Action_Status
}// ProcessedStatus