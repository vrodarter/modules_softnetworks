package com.spocsys.vigfurniture.productavailability.ad_process.sales_order;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessLogger;
import org.openbravo.service.db.DalBaseProcess;
import org.quartz.JobExecutionException;

public class StockActionStatus extends DalBaseProcess {

  private ProcessLogger logger;
  private Boolean isDebug = false;

  public void doExecute(ProcessBundle bundle) throws Exception {

    logger = bundle.getLogger();
    logger.log("Starting background search order non Processed " + "\n");

    // Calendar timeSixMonthsAgo = Calendar.getInstance();
    // timeSixMonthsAgo.add(Calendar.DAY_OF_MONTH, -180);

    // begin to backgroud process
    try {
      OBContext.setAdminMode();
      // find sales order and call void Sales_Order_Stock_Action_Status(SO10001)
      final OBCriteria<Order> orderList = OBDal.getInstance().createCriteria(Order.class);
      orderList.add(Restrictions.eq(Order.PROPERTY_SALESTRANSACTION, true));
      orderList.add(Restrictions.eq(Order.PROPERTY_SVFPAPROCESSED, false));

      // non processed order sales
      //
      //
  
      for (Order order : orderList.list()) {

        Sales_Order_Stock_Action_Status(order);

      }

    } catch (Exception e) {
      
      throw new JobExecutionException(e.getMessage(), e);

    } finally {

    }

  }

  
//this void need c_order_id
 public void Sales_Order_Stock_Action_Status(Order salesOrder) {

   try {

   
     OBContext.setAdminMode();

     final OBCriteria<OrderLine> orderlineList = OBDal.getInstance().createCriteria(
         OrderLine.class);

     orderlineList.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, salesOrder));

     int intTotalLines = (int) orderlineList.list().size();

     int intIn = (int) 0;// count lines in stock
     int intRtp = (int) 0; // count lines rtp
     int intSource = (int) 0;// count lines source
     int intInter = (int) 0; // count lines inter company
     int intTransfer = (int) 0; // count lines transfer
     int intDiscontinued = (int) 0; // count lines discontinued
     int intWaitingPO = (int) 0; // count lines discontinued
     int intIssuePO = (int) 0; // count lines discontinued
     int intError = (int) 0;
     int intSpecial = (int) 0;
     int intStatusIntC = (int) 0 ;
    
     
     String strStockAvailavility = "";
     String strAction = "";
     String strStatus = "";
     String strSpecial ;
     
     
     for (OrderLine orderline : orderlineList.list()) {
       // /

       strStockAvailavility = orderline.getSvfpaStockavailability();
       strAction = orderline.getSvfpaAction();
       strStatus  = "";
       if ( orderline.getSvfpaStatus() != null )
             strStatus = orderline.getSvfpaStatus();
       
       if ( orderline.getProduct().getSvfsvProductCharact() != null )
          strSpecial = orderline.getProduct().getSvfsvProductCharact();
       else
         strSpecial = "";
       //
       
       // stock availability compare
       if (strStockAvailavility != null) {

         if (strStockAvailavility.equalsIgnoreCase("INSTOCK"))
           intIn++;
         else { 
                
                if( (strStockAvailavility.equalsIgnoreCase("OUTOFSTOCK"))&&(strSpecial.equalsIgnoreCase("SO")))
                      intSpecial++; 
                  
                
              if( (strStatus.equalsIgnoreCase("Intercompany")))
                intStatusIntC++; 
                
       
         }
       }
       // end
       // acction compare
       if (strAction != null) {
         switch (strAction) {
         case "RTP":
           intRtp++;
           break;
         case "SOURCE":
           intSource++;
           break;
         case "INTERCOMPANY":
           intInter++;
           break;
         case "DISCONTINUED":
           intDiscontinued++;
           break;
         case "TRANSFER":
           intTransfer++;
           break;
         case "WAITINGFORPO":
           intWaitingPO++;
           break;
         case "ISSUEPO":
           intIssuePO++;
           break;
         default:
           intError++;
           break;

         }

       }
       // end
     }
   


       int intDelta = intTotalLines - intIn;


       // stock status
       String strStatusResult = setStatus(intDelta,intIn);  
       
       

       // action status
       String strActionResult = setActionStatus( intTotalLines,intRtp,intSource,intInter,intDiscontinued,intTransfer,intWaitingPO,intIssuePO);

       boolean boolIsSpecialOrder = false;
       if(intSpecial  > 0)
         boolIsSpecialOrder = true;
       
       boolean boolIsInterCompany = false;
       if(intInter  > 0)
         boolIsInterCompany = true;
       
       
       setActionOrderStockStatus(salesOrder, strStatusResult, strActionResult,boolIsInterCompany,boolIsSpecialOrder);

 
   } catch (Exception e) {
     e.printStackTrace();
     System.out.println("2 " + e + "\n");
     logger.log("error void Sales_Order_Stock_Action_Status: " + e);

   } finally {
     OBContext.restorePreviousMode();
   }

 }

 
 
 
///////////////////
 
     private String setStatus(int intDelta,int intIn){
         String strStatusResult = "";
         if (intDelta == 0) {
         strStatusResult = "INSTOCK";
         } else {
         if (intIn == 0)
         strStatusResult = "OUTOFSTOCK";
         else
         strStatusResult = "PARTIAL";
         }
         return strStatusResult;

       }



//set stock statrus or header order
   private String setActionStatus(int intTotalLines, int intRtp, int intSource, int intInter, int intDiscontinued, int intTransfer, int intWaitingPO,int intIssuePO){

         String  strActionResult = "";       
         int intAllStatus = intRtp + intSource + intInter + intDiscontinued + intTransfer + intWaitingPO + intIssuePO;
         
         if (intAllStatus > 0 )
         strActionResult = "MIXED";
         
         if (intRtp == intTotalLines)
         strActionResult = "RTP";
         
         if (intSource == intTotalLines)
         strActionResult = "SOURCE";
         
         if (intInter == intTotalLines)
         strActionResult = "INTERCOMPANY";
         
         if (intDiscontinued == intTotalLines)
         strActionResult = "DISCONTINUED";
         
         if (intTransfer == intTotalLines)
         strActionResult = "TRANSFER";
         
         if (intWaitingPO == intTotalLines)
         strActionResult = "WAITINGFORPO";
         
         if (intIssuePO == intTotalLines)
         strActionResult = "ISSUEPO";
         
         return strActionResult;    
    }
 

 
 
 
 
 
 
   private void setActionOrderStockStatus(Order salesOrder, String strStatusResult, String strActionResult,boolean boolIsInterCompany,boolean boolIsSpecialOrder) {

     try {

       if (strStatusResult != "")
           salesOrder.setSvfpaStockStatus(strStatusResult);
        
       if (strActionResult != "")
          salesOrder.setSVFPAAction(strActionResult);
       
       salesOrder.setSvfpaInterCompany(boolIsInterCompany);
       salesOrder.setSvfpaSpecialOrder(boolIsSpecialOrder);
       OBDal.getInstance().save(salesOrder);
       OBDal.getInstance().flush();

     } catch (Exception e) {
       e.printStackTrace();       
       logger.log("error void setOrderStockStatus " + e);
     }

   }

}
