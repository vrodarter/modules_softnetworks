package com.spocsys.vigfurniture.productavailability.erpCommon.ad_callouts;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
// added by: jgomez
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.uom.UOM;
import org.openbravo.model.financialmgmt.tax.TaxRate;

import com.spocsys.vigfurniture.productavailability.ProductPriceWareHouse;

public class ProductAvailabilityExtend {
  private static final Logger log = Logger.getLogger(ProductAvailabilityExtend.class);

  public ProductAvailabilityExtend() {
    // TODO Auto-generated constructor stub
  }

  public static OrderLine getOrderLineParent(String ID) {
    OrderLine reg = null;

    try {

      OBContext.setAdminMode();

      try {

        final OBCriteria<OrderLine> regList = OBDal.getInstance().createCriteria(OrderLine.class);
        regList.add(Restrictions.eq(OrderLine.PROPERTY_SVFPAORDERLINEPARENT, ID));

        if ((regList != null) && (regList.list().size() > 0)) {
          reg = regList.list().get(0);
        }

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e);
        reg = null;
      }

    } finally {
      OBContext.restorePreviousMode();
    }

    return reg;
  }

  // updated by: jgomez
  public static OrderLine InsertLineInDocument(String strADOrgID, String strClientId,
      String strUserId, String strMWarehouseID, String strUOM, String strCOrderId,
      String strMProductID, BigDecimal QuantityAvailability, String strPriceStd,
      String strPriceList, String strCurrency, String strTax, String orderLineParent, Long nroLine) {

    OrderLine oLine = null;

    try {

      try {
        OBContext.setAdminMode(true);
        oLine = OBProvider.getInstance().get(OrderLine.class);
        final OBCriteria<Organization> orgList = OBDal.getInstance().createCriteria(
            Organization.class);
        orgList.add(Restrictions.eq(Organization.PROPERTY_ID, strADOrgID));

        final OBCriteria<Order> orderList = OBDal.getInstance().createCriteria(Order.class);
        orderList.add(Restrictions.eq(Order.PROPERTY_ID, strCOrderId));

        final OBCriteria<UOM> UOMList = OBDal.getInstance().createCriteria(UOM.class);
        UOMList.add(Restrictions.eq(UOM.PROPERTY_ID, strUOM));

        final OBCriteria<Product> ProductList = OBDal.getInstance().createCriteria(Product.class);
        ProductList.add(Restrictions.eq(Product.PROPERTY_ID, strMProductID));

        final OBCriteria<Warehouse> warehouseList = OBDal.getInstance().createCriteria(
            Warehouse.class);
        warehouseList.add(Restrictions.eq(Warehouse.PROPERTY_ID, strMWarehouseID));

        final OBCriteria<User> userList = OBDal.getInstance().createCriteria(User.class);
        userList.add(Restrictions.eq(User.PROPERTY_ID, strUserId));

        final OBCriteria<Client> clientList = OBDal.getInstance().createCriteria(Client.class);
        clientList.add(Restrictions.eq(Client.PROPERTY_ID, strClientId));

        final OBCriteria<Currency> currencyList = OBDal.getInstance()
            .createCriteria(Currency.class);
        currencyList.add(Restrictions.eq(Currency.PROPERTY_ID, strCurrency));

        final OBCriteria<TaxRate> taxList = OBDal.getInstance().createCriteria(TaxRate.class);
        taxList.add(Restrictions.eq(TaxRate.PROPERTY_ID, strTax));
        oLine.setNewOBObject(true);
        oLine.setSalesOrder(orderList.list().get(0));
        oLine.setOrderDate(orderList.list().get(0).getOrderDate());
        oLine.setWarehouse(warehouseList.list().get(0));
        oLine.setCurrency(currencyList.list().get(0));
        oLine.setActive(true);
        oLine.setCreationDate(new Date());
        oLine.setUpdated(new Date());
        oLine.setUpdatedBy(userList.list().get(0));
        oLine.setCreatedBy(userList.list().get(0));
        oLine.setClient(clientList.list().get(0));
        oLine.setOrganization(orgList.list().get(0));
        oLine.setProduct(ProductList.list().get(0));
        oLine.setUOM(UOMList.list().get(0));
        oLine.setOrderedQuantity(QuantityAvailability);
        oLine.setUnitPrice(new BigDecimal(strPriceStd));
        oLine.setListPrice(new BigDecimal(strPriceStd));
        oLine.setTax(taxList.list().get(0));
        oLine.setStandardPrice(new BigDecimal(strPriceStd));
        oLine.setDirectShipment(false);
        oLine.setReservedQuantity(BigDecimal.ZERO);
        oLine.setDeliveredQuantity(BigDecimal.ZERO);
        oLine.setInvoicedQuantity(BigDecimal.ZERO);
        oLine.setUnitPrice(new BigDecimal(strPriceStd));
        oLine.setPriceLimit(BigDecimal.ZERO);
        oLine.setLineNetAmount(new BigDecimal(strPriceStd));
        oLine.setFreightAmount(BigDecimal.ZERO);
        oLine.setDescriptionOnly(false);
        // Long NOorderLine = (orderList.list().get(0).getOrderLineList().size() + 1L) * 10L;
        oLine.setLineNo(nroLine);
        oLine.setSvfpaOrderlineParent(orderLineParent);

        // OBDal.getInstance().save(oLine);

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e);
        oLine = null;
      }

    } finally {
      OBContext.restorePreviousMode();
    }
    // Must return true if the line was inserted without mistakes.
    return oLine;

  }

  public static ProductPriceWareHouse getQuantityAvailabilityNew(String organizationId,
      String warehouseId, String productId, boolean TypeProduct, BigDecimal QtyToCheckout) {

    // BigDecimal qtyInStock = BigDecimal.ZERO;
    ProductPriceWareHouse regProductPriceWareHouse = null;
    //
    try {
      OBContext.setAdminMode();

      try {

        final StringBuilder queryExtend = new StringBuilder();
        queryExtend.append(" AS pw");
        queryExtend.append(" WHERE ");

        if (warehouseId != null) {
          queryExtend.append("(pw." + ProductPriceWareHouse.PROPERTY_WAREHOUSE + "."
              + Warehouse.PROPERTY_ID + " = '" + warehouseId + "') AND ");
        }

        queryExtend.append(" (pw." + ProductPriceWareHouse.PROPERTY_PRODUCT + "."
            + Product.PROPERTY_ID + " = '" + productId + "')");

        if (TypeProduct == true) {
          queryExtend.append(" ORDER BY pw." + ProductPriceWareHouse.PROPERTY_QTYINKIT + " DESC");
        } else {
          queryExtend.append(" ORDER BY pw." + ProductPriceWareHouse.PROPERTY_AVAILABLE + " DESC");
        }

        OBQuery<ProductPriceWareHouse> obq = OBDal.getInstance().createQuery(
            ProductPriceWareHouse.class, queryExtend.toString());
        obq.setMaxResult(1);
        // obq.setFilterOnReadableOrganization(false);
        System.out.println(queryExtend.toString());
        List<ProductPriceWareHouse> ListProductPriceWareHouse = obq.list();

        if ((ListProductPriceWareHouse != null) && (ListProductPriceWareHouse.size() > 0)) {
          regProductPriceWareHouse = ListProductPriceWareHouse.get(0);
          // if (ListProductPriceWareHouse.get(0).getProduct().isBillOfMaterials() == true) {
          // qtyInStock = ListProductPriceWareHouse.get(0).getQtyinkit();
          // } else {
          // qtyInStock = ListProductPriceWareHouse.get(0).getAvailable();
          // }

        }

      } catch (Exception e) {
        regProductPriceWareHouse = null;
        e.printStackTrace();
        log.error(e);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    // System.out.println("stock: " + qtyInStock.toString());
    // return qtyInStock;
    return regProductPriceWareHouse;

  }

}
