package com.spocsys.vigfurniture.productavailability.erpCommon.ad_callouts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.service.db.CallStoredProcedure;

import com.spocsys.vigfurniture.advanceproductmanagement.SubstituteProduct;
import com.spocsys.vigfurniture.incomingproduct.ProductTotalIncProdV;
import com.spocsys.vigfurniture.productavailability.ProductByWareHouse;
import com.spocsys.vigfurniture.productavailability.ProductPriceWareHouse;

class StockAvailable {

  Organization regOrganization = null;
  Warehouse regWareHouse = null;
  BigDecimal QtyAvailable = null;
  String Action = null;
  Order PurchaseOrder = null;
  Date Eta = null;

  public Organization getRegOrganization() {
    return regOrganization;
  }

  public void setRegOrganization(Organization regOrganization) {
    this.regOrganization = regOrganization;
  }

  public Warehouse getRegWareHouse() {
    return regWareHouse;
  }

  public void setRegWareHouse(Warehouse regWareHouse) {
    this.regWareHouse = regWareHouse;
  }

  public BigDecimal getQtyAvailable() {
    return QtyAvailable;
  }

  public void setQtyAvailable(BigDecimal qtyAvailable) {
    QtyAvailable = qtyAvailable;
  }

  public String getAction() {
    return Action;
  }

  public void setAction(String action) {
    Action = action;
  }

  public Order getPurchaseOrder() {
    return PurchaseOrder;
  }

  public void setPurchaseOrder(Order purchaseOrder) {
    PurchaseOrder = purchaseOrder;
  }

  public Date getEta() {
    return Eta;
  }

  public void setEta(Date eta) {
    Eta = eta;
  }

  public StockAvailable(Organization regOrganization, Warehouse regWareHouse,
      BigDecimal QtyAvailable, String Action, Order PurchaseOrder) {

    this.regOrganization = regOrganization;
    this.regWareHouse = regWareHouse;
    this.QtyAvailable = QtyAvailable;
    this.Action = Action;
    this.PurchaseOrder = PurchaseOrder;

  }

}

public class ProductAvailabilityProcess {

  JSONArray ROW_ADD = new JSONArray();
  JSONArray ROW_UPDATED = new JSONArray();

  String TYPE_SOURCE = "VIG FURNITURE";
  String TYPE_SOURCE_VALUE = "";
  String TYPE_SOURCE_VALUE_LOCATION = "";
  Organization OrgSalesOrder = null;
  Warehouse WareSalesOrder = null;
  Order SalesOrderGlobal = null;
  Warehouse StockLocationGlobal = null;
  Warehouse ShipFromGlobal = null;
  List<OrderLine> ListOrderLine = null;
  List<OrderLine> ListOrderLineNewest = null;
  List<StockAvailable> ListStockAvailableTemp = null;
  HashMap<String, List<StockAvailable>> HashMapProductsAvailable = new HashMap<String, List<StockAvailable>>();
  HashMap<String, List<StockAvailable>> HashMapProductsAvailablePO = new HashMap<String, List<StockAvailable>>();
  int LastNoLine = 0;

  // ---
  // ---
  private static final Logger log = Logger.getLogger(ProductAvailabilityProcess.class);

  public ProductAvailabilityProcess() {
    // TODO Auto-generated constructor stub
  }

  public BigDecimal getAvailablePO(ProductTotalIncProdV regIncProduct) {

    BigDecimal QuantityAvailable = BigDecimal.ZERO;
    if (regIncProduct != null) {

      QuantityAvailable = regIncProduct.getQuantity();

    }
    return QuantityAvailable;
  }

  public BigDecimal getAvailable(ProductByWareHouse regProductByWareHouse) {

    BigDecimal QuantityAvailable = BigDecimal.ZERO;
    if (regProductByWareHouse != null) {

      // if ((regProductByWareHouse.getProduct().isBillOfMaterials() == true)
      // && (regProductByWareHouse.getQuantityAvailable() != null)) {
      // QuantityAvailable = regProductByWareHouse.getQuantityAvailable();// QtyInkit
      //
      // } else {
      // if (regProductByWareHouse.getAvailable() != null) {
      // QuantityAvailable = regProductByWareHouse.getAvailable();
      // }
      // }
      if ((regProductByWareHouse.getQtyOnHand() != null)
          && (regProductByWareHouse.getCommitted() != null)) {
        QuantityAvailable = regProductByWareHouse.getQtyOnHand().subtract(
            regProductByWareHouse.getCommitted());// regProductByWareHouse.getTotalAvailable();
      }

    }
    return QuantityAvailable;
  }

  public static List<ProductByWareHouse> getStock(String clientId, String orgTransactionId,
      String warehouseId, String productId, boolean Tranfer, boolean intercompany) {
    List<ProductByWareHouse> ListProductByWareHouse = null;

    try {

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS pw");
      queryExtend.append(" WHERE ");

      if (intercompany == false) {
        queryExtend.append(" (ad_isorgincluded('" + orgTransactionId + "', pw.organization.id , '"
            + clientId + "')<>-1)");

        if (warehouseId != null) {

          if (Tranfer == false) {
            queryExtend.append(" AND (pw.warehouse.id = '" + warehouseId + "')");

          } else {
            queryExtend.append(" AND (pw.warehouse.id <> '" + warehouseId + "')");
          }
        }
      } else {
        queryExtend.append(" (ad_isorgincluded('" + orgTransactionId + "', pw.organization.id , '"
            + clientId + "')=-1)");
      }

      queryExtend.append(" AND (pw.product.id='" + productId + "')");
      OBQuery<ProductByWareHouse> obq = OBDal.getInstance().createQuery(ProductByWareHouse.class,
          queryExtend.toString());

      ListProductByWareHouse = obq.list();

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
      ListProductByWareHouse = null;
    }

    return ListProductByWareHouse;

  }

  private void setListInHashMap(String productId, String Action,
      List<ProductByWareHouse> ListProductByWareHouse) {

    List<StockAvailable> ListStockAvailable = new ArrayList<StockAvailable>();

    if (ListProductByWareHouse != null) {

      if (HashMapProductsAvailable.containsKey(productId) == true) {
        ListStockAvailable = HashMapProductsAvailable.get(productId);
      }

      for (ProductByWareHouse regProductByWareHouse : ListProductByWareHouse) {

        StockAvailable regStockAvailable = new StockAvailable(OrgSalesOrder,
            regProductByWareHouse.getWarehouse(), getAvailable(regProductByWareHouse), Action, null);
        ListStockAvailable.add(regStockAvailable);

      }
      HashMapProductsAvailable.put(productId, ListStockAvailable);
    }

  }

  private void setStockByStockAvailabilityAndAction(String productId) {
    try {
      if (HashMapProductsAvailable.containsKey(productId) != true) {
        OBContext.setAdminMode(true);
        // ReadyToPick

        setListInHashMap(
            productId,
            "RTP",
            getStock(OrgSalesOrder.getClient().getId(), OrgSalesOrder.getId(),
                WareSalesOrder.getId(), productId, false, false));

        // Transfer

        setListInHashMap(
            productId,
            "TRANSFER",
            getStock(OrgSalesOrder.getClient().getId(), OrgSalesOrder.getId(),
                WareSalesOrder.getId(), productId, true, false));

        // Intercompany

        setListInHashMap(
            productId,
            "INTERCOMPANY",
            getStock(OrgSalesOrder.getClient().getId(), OrgSalesOrder.getId(),
                WareSalesOrder.getId(), productId, true, true));

        // MostrarStocks();

      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
  }

  public boolean CheckAvailabiltyByLine(OrderLine regOrderLine, int IndexLine, boolean NewObject) {

    try {
      if (HashMapProductsAvailable.containsKey(regOrderLine.getProduct().getId()) == true) {

        if (HashMapProductsAvailable.get(regOrderLine.getProduct().getId()) != null) {

          ListStockAvailableTemp = HashMapProductsAvailable.get(regOrderLine.getProduct().getId());
          int Index = 0;
          for (StockAvailable regStockAvailable : ListStockAvailableTemp) {
            BigDecimal QtyOrdered = regOrderLine.getOrderedQuantity();
            if (regStockAvailable.getQtyAvailable().doubleValue() > 0) {

              if (regStockAvailable.getQtyAvailable().doubleValue() < regOrderLine
                  .getOrderedQuantity().doubleValue()) {

                BigDecimal NewQuantityOrdered = regOrderLine.getOrderedQuantity().subtract(
                    regStockAvailable.getQtyAvailable());

                String strADOrgID = regOrderLine.getOrganization().getId();
                String strClientId = regOrderLine.getClient().getId();
                String strUserId = regOrderLine.getCreatedBy().getId();
                String strMWarehouseID = regOrderLine.getWarehouse().getId();
                String strUOM = regOrderLine.getProduct().getUOM().getId();
                String strCOrderId = regOrderLine.getSalesOrder().getId();
                String strMProductID = regOrderLine.getProduct().getId();
                BigDecimal QuantityAvailability = NewQuantityOrdered;
                String strPriceStd = String.valueOf(regOrderLine.getUnitPrice());
                String strPriceList = regOrderLine.getSalesOrder().getPriceList().getId();
                String strCurrency = regOrderLine.getCurrency().getId();
                String strTax = regOrderLine.getTax().getId();
                // Long nroLine = Long.parseLong(String.valueOf(regOrderLine.getSalesOrder()
                // .getOrderLineList().size() + 10));
                LastNoLine = LastNoLine + 10;
                Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
                OrderLine regOrderLineNew = ProductAvailabilityExtend.InsertLineInDocument(
                    strADOrgID, strClientId, strUserId, strMWarehouseID, strUOM, strCOrderId,
                    strMProductID, QuantityAvailability, strPriceStd, strPriceList, strCurrency,
                    strTax, regOrderLine.getId(), nroLine);
                ListOrderLine.get(IndexLine)
                    .setOrderedQuantity(regStockAvailable.getQtyAvailable());
                ListOrderLineNewest.add(regOrderLineNew);

              }
              BigDecimal NewQuantityAvailable = regStockAvailable.getQtyAvailable().subtract(
                  QtyOrdered);

              ListOrderLine.get(IndexLine).setSvfpaStockavailability("INSTOCK");
              ListOrderLine.get(IndexLine).setSvfpaAction(getActionWhenisInStock(regOrderLine));
              ListStockAvailableTemp.get(Index).setQtyAvailable(BigDecimal.ZERO);

              if (NewQuantityAvailable.doubleValue() > 0) {
                ListStockAvailableTemp.get(Index).setQtyAvailable(NewQuantityAvailable);
              }

              HashMapProductsAvailable.put(regOrderLine.getProduct().getId(),
                  ListStockAvailableTemp);

              if (NewObject == false) {
                UpdatedStatus(ListOrderLine.get(IndexLine), "update");
              }

              OBDal.getInstance().save(regOrderLine);
              return true;
            }
            Index++;
          }

        }
      }

      ListOrderLine.get(IndexLine).setSvfpaStockavailability("OUTOFSTOCK");
      ListOrderLine.get(IndexLine).setSvfpaAction(null);

      if (OrgSalesOrder.getSearchKey().equalsIgnoreCase(TYPE_SOURCE) == true) {
        ListOrderLine.get(IndexLine).setSvfpaAction("SOURCE");
      } else {
        if (ListOrderLine.get(IndexLine).getProduct().isSvfpaIsvigproduct() == true) {
          ListOrderLine.get(IndexLine).setSvfpaAction("INTERCOMPANY");
        } else {
          ListOrderLine.get(IndexLine).setSvfpaAction("SOURCE");
        }
      }

      if (ListOrderLine.get(IndexLine).getProduct().isDiscontinued() == true) {
        ListOrderLine.get(IndexLine).setSvfpaAction("DISCONTINUED");
      }

      if (NewObject == false) {
        UpdatedStatus(ListOrderLine.get(IndexLine), "update");
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }

    return false;
  }

  public void IterationProcess(List<OrderLine> ListOrderLineParm, boolean NewObject) {
    try {
      ListOrderLine = ListOrderLineParm;
      ListOrderLineNewest = new ArrayList<OrderLine>();

      if (ListOrderLine != null) {
        int Index = 0;
        for (OrderLine regOrderLine : ListOrderLine) {

          String productId = regOrderLine.getProduct().getId();
          setStockByStockAvailabilityAndAction(productId);
          CheckAvailabiltyByLine(regOrderLine, Index, NewObject);

          if (NewObject == true) {
            SalesOrderGlobal.getOrderLineList().add(regOrderLine);
            UpdatedStatus(regOrderLine, "add");

          }
          Index++;

        }
        OBDal.getInstance().save(SalesOrderGlobal);

        // System.out.println("STOCK FINAL");
        // MostrarStocks();

        if ((ListOrderLineNewest != null) && (ListOrderLineNewest.size() > 0)) {
          IterationProcess(ListOrderLineNewest, true);
        }

      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }

  }

  // ---------------------

  public List<ProductTotalIncProdV> getStockPO(String IDProduct, String IDWarehouse, Order regOrder) {

    List<ProductTotalIncProdV> ListIncProd = null;

    if ((IDProduct != null) && (IDWarehouse != null)) {
      try {

        String IDs = Utility.getInStrSet(OBContext.getOBContext()
            .getOrganizationStructureProvider().getNaturalTree(regOrder.getOrganization().getId()));

        final StringBuilder queryExtend = new StringBuilder();
        queryExtend.append(" AS incprod");
        queryExtend.append(" WHERE ");

        queryExtend.append("     incprod.product.id='" + IDProduct + "'");
        queryExtend.append(" AND incprod.warehouse.id='" + IDWarehouse + "'");
        queryExtend.append(" AND incprod.organization.id IN (" + IDs + ")");
        queryExtend.append(" AND NOT incprod.organization.id ='0'");

        OBQuery<ProductTotalIncProdV> obq = OBDal.getInstance().createQuery(
            ProductTotalIncProdV.class, queryExtend.toString());

        ListIncProd = obq.list();

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e);
        ListIncProd = null;
      }
    }
    return ListIncProd;
  }

  private void setListInHashMapPO(String productId, List<ProductTotalIncProdV> ListIncProd) {

    List<StockAvailable> ListStockAvailable = new ArrayList<StockAvailable>();

    if (ListIncProd != null) {

      if (HashMapProductsAvailablePO.containsKey(productId) == true) {
        ListStockAvailable = HashMapProductsAvailablePO.get(productId);
      }

      for (ProductTotalIncProdV regIncProduct : ListIncProd) {

        StockAvailable regStockAvailable = new StockAvailable(OrgSalesOrder,
            regIncProduct.getWarehouse(), getAvailablePO(regIncProduct), "",
            regIncProduct.getSalesOrder());
        regStockAvailable.setEta(regIncProduct.getETA());
        ListStockAvailable.add(regStockAvailable);

      }
      HashMapProductsAvailablePO.put(productId, ListStockAvailable);
    }

  }

  public List<OrderLine> getAllOrderLines(String orderId) {

    try {

      final OBCriteria<OrderLine> ListOrderLineNew = OBDal.getInstance().createCriteria(
          OrderLine.class);
      ListOrderLineNew.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER + "." + Order.PROPERTY_ID,
          orderId));
      ListOrderLineNew.add(Restrictions.eq(OrderLine.PROPERTY_SVFPAIGNORECHKPA, false));
      ListOrderLineNew.addOrder(org.hibernate.criterion.Order.asc(OrderLine.PROPERTY_LINENO));

      if ((ListOrderLineNew != null) && (ListOrderLineNew.list().size() > 0)) {
        LastNoLine = ListOrderLineNew.list().get(ListOrderLineNew.list().size() - 1).getLineNo()
            .intValue();
      }

      return ListOrderLineNew.list();

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }

    return null;

  }

  public static List<ProductPriceWareHouse> getAllStockProduct(String productId) {

    try {

      final OBCriteria<ProductPriceWareHouse> ListProductPriceWareHouse = OBDal.getInstance()
          .createCriteria(ProductPriceWareHouse.class);
      ListProductPriceWareHouse.add(Restrictions.eq(ProductPriceWareHouse.PROPERTY_PRODUCT + "."
          + Product.PROPERTY_ID, productId));

      return ListProductPriceWareHouse.list();

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }

    return null;

  }

  public static ProductPriceWareHouse getStockProduct(List<Organization> ListOrganization,
      String warehouseId, String productId, boolean intercompany) {

    try {
      final OBCriteria<ProductPriceWareHouse> ListProductPriceWareHouse = OBDal.getInstance()
          .createCriteria(ProductPriceWareHouse.class);

      if (intercompany == false) {
        ListProductPriceWareHouse.add(Restrictions.in(ProductPriceWareHouse.PROPERTY_ORGANIZATION,
            ListOrganization));

        if (warehouseId != null) {
          ListProductPriceWareHouse.add(Restrictions.eq(ProductPriceWareHouse.PROPERTY_WAREHOUSE
              + "." + Product.PROPERTY_ID, warehouseId));
        }
      }

      ListProductPriceWareHouse.add(Restrictions.eq(ProductPriceWareHouse.PROPERTY_PRODUCT + "."
          + Product.PROPERTY_ID, productId));

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }

    return null;

  }

  int getStockAvailableByType(List<StockAvailable> ListStockAvailableParam, String Type) {
    StockAvailable regStockAvailableMax = null;
    int IndexMax = -1;
    int Index = 0;
    String TypeNew = Type.equalsIgnoreCase("TRANSFER") == true ? "NOINTERCOMPANY" : Type;

    for (StockAvailable regStockAvailable : ListStockAvailableParam) {
      if ((regStockAvailable.getAction().equalsIgnoreCase(TypeNew) == true)
          && (regStockAvailable.getQtyAvailable().doubleValue() > 0)) {

        if (regStockAvailableMax == null) {
          regStockAvailableMax = regStockAvailable;
          IndexMax = Index;
        } else {
          if (regStockAvailableMax.getQtyAvailable().doubleValue() < regStockAvailable
              .getQtyAvailable().doubleValue()) {
            regStockAvailableMax = regStockAvailable;
            IndexMax = Index;
          }
        }
      }
      Index++;

    }

    return IndexMax;
  }

  int getStockAvailableMaxPO(List<StockAvailable> ListStockAvailableParam,
      Warehouse regStockLocation) {

    StockAvailable regStockAvailableMax = null;
    int IndexMax = -1;
    int Index = 0;

    if (regStockLocation != null) {

      for (StockAvailable regStockAvailable : ListStockAvailableParam) {
        if ((regStockAvailable.getRegWareHouse().getId().equalsIgnoreCase(regStockLocation.getId()))
            && (regStockAvailable.getQtyAvailable().doubleValue() > 0)) {

          if (regStockAvailableMax == null) {
            regStockAvailableMax = regStockAvailable;
            IndexMax = Index;
          } else {
            if ((regStockAvailableMax.getEta() == null) && (regStockAvailable.getEta() != null)) {
              regStockAvailableMax = regStockAvailable;
              IndexMax = Index;
            } else {
              if ((regStockAvailableMax.getEta() == null) && (regStockAvailable.getEta() == null)) {
                if (regStockAvailableMax.getQtyAvailable().doubleValue() < regStockAvailable
                    .getQtyAvailable().doubleValue()) {
                  regStockAvailableMax = regStockAvailable;
                  IndexMax = Index;

                }
              }// --
              else {
                if ((regStockAvailableMax.getEta() != null) && (regStockAvailable.getEta() != null)) {
                  if (regStockAvailableMax.getEta().after(regStockAvailable.getEta()) == true) {
                    regStockAvailableMax = regStockAvailable;
                    IndexMax = Index;
                  }
                }
              }
            }
          }

        }
        Index++;
      }

      return IndexMax;
    }

    return -1;
  }

  int getStockAvailable(List<StockAvailable> ListStockAvailableParam, Warehouse regStockLocation) {
    int Index = 0;

    if (regStockLocation != null) {

      for (StockAvailable regStockAvailable : ListStockAvailableParam) {
        if ((regStockAvailable.getRegWareHouse().getId().equalsIgnoreCase(regStockLocation.getId()))
            && (regStockAvailable.getQtyAvailable().doubleValue() > 0)) {
          return Index;
        }
        Index++;
      }
    }

    return -1;
  }

  Organization getOrgLegal(String OrgId) {

    Organization regOrganization = null;

    if ((OrgId != null) && (OrgId.trim().equals("") != true)) {
      String ID = getOrgParent(OrgId);

      if (ID != null) {
        try {
          regOrganization = OBDal.getInstance().get(Organization.class, ID);
        } catch (Exception e) {
          e.printStackTrace();
          log.error(e.getMessage());
          regOrganization = null;
        }
      }
    }
    return regOrganization;

  }

  String getOrgParent(String strOrgId) {
    String id_org = "";
    String strOrgIdNew = "0";

    if ((strOrgId != null) && (strOrgId.trim().equals("") != true)) {
      strOrgIdNew = strOrgId;
    }

    try {

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS org");
      queryExtend.append(" WHERE ");
      queryExtend.append(" (ad_isorgincluded('" + strOrgIdNew + "', org.id, org.client.id)<>-1)");
      queryExtend.append(" AND (org.organizationType='1')");

      OBQuery<Organization> obq = OBDal.getInstance().createQuery(Organization.class,
          queryExtend.toString());

      if ((obq != null) && (obq.list().size() > 0)) {
        id_org = obq.list().get(0).getId();
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return id_org;
  }

  String getOrgAccess(String strOrgId) {
    String ids_org = "";
    try {

      if (strOrgId.equalsIgnoreCase("") != true) {
        final CallStoredProcedure svfpa_getorgparent = new CallStoredProcedure();

        List<Object> Parametros = new ArrayList<Object>();
        Parametros.add(strOrgId);

        ids_org = svfpa_getorgparent.call("svfpa_getorgparent", Parametros, null, false).toString();
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return ids_org;
  }

  OBQuery<Organization> getOrgToCompare(Warehouse regStockLocation) {

    try {

      String strOrgParent = getOrgParent(SalesOrderGlobal.getOrganization().getId());
      String strOrgAccessIds = getOrgAccess(strOrgParent);
      String IDs = "'" + regStockLocation.getSvfpaOrgsource().getId() + "'";

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS org");
      queryExtend.append(" WHERE ");

      // queryExtend.append(" (ad_isorgincluded(svfpa_getorgparent('"
      // + regOrderLine.getSvfpaStockLocation().getSvfpaOrgsource().getId() + "'), org.id, '"
      // + regOrderLine.getClient().getId() + "')<>-1)");

      queryExtend.append(" (org.id IN (" + strOrgAccessIds + "))");
      queryExtend.append(" AND (org.id IN (" + IDs + "))");
      // System.out.println("----------------------------------------------------");
      // System.out.println(queryExtend.toString());
      OBQuery<Organization> obq = OBDal.getInstance().createQuery(Organization.class,
          queryExtend.toString());

      return obq;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  String getActionWhenisInStock(OrderLine regOrderLine) {
    String Action = null;

    try {

      if ((regOrderLine.getSvfpaStockLocation() != null)
          && (regOrderLine.getSvfpaShipFrom() != null)) {

        OBQuery<Organization> obq = getOrgToCompare(regOrderLine.getSvfpaStockLocation());
        OBQuery<Organization> obq1 = getOrgToCompare(regOrderLine.getSvfpaShipFrom());

        if ((obq != null) && (obq.list().size() > 0) && (obq1 != null) && (obq1.list().size() > 0)) {

          if (regOrderLine.getSvfpaStockLocation().getId()
              .equalsIgnoreCase(regOrderLine.getSvfpaShipFrom().getId()) == true) {
            Action = "RTP";
          } else {
            Action = "TRANSFER";
          }

        } else {
          Action = "INTERCOMPANY";
        }
        // System.out.println("Prod: " + regOrderLine.getProduct().getName());
        // System.out.println("Qty: " + regOrderLine.getOrderedQuantity().doubleValue());
        // System.out.println("Stock Location: " + regOrderLine.getSvfpaStockLocation().getName());
        // System.out.println("Ship From: " + regOrderLine.getSvfpaShipFrom().getName());
        // System.out.println(queryExtend.toString());

        // if (regOrderLine.getSvfpaStockLocation().getId()
        // .equalsIgnoreCase(regOrderLine.getSvfpaShipFrom().getId()) == true) {
        // Action = "RTP";
        // } else {
        //
        // String strOrgParent = getOrgParent(regOrderLine.getSvfpaStockLocation()
        // .getSvfpaOrgsource().getId());
        //
        // String strOrgAccessIds = getOrgAccess(strOrgParent);
        //
        // final StringBuilder queryExtend = new StringBuilder();
        // queryExtend.append(" AS org");
        // queryExtend.append(" WHERE ");
        //
        // // queryExtend.append(" (ad_isorgincluded(svfpa_getorgparent('"
        // // + regOrderLine.getSvfpaStockLocation().getSvfpaOrgsource().getId() + "'), org.id, '"
        // // + regOrderLine.getClient().getId() + "')<>-1)");
        //
        // queryExtend.append(" (org.id IN (" + strOrgAccessIds + "))");
        // queryExtend.append(" AND (org.id = '"
        // + regOrderLine.getSvfpaShipFrom().getSvfpaOrgsource().getId() + "')");
        //
        // OBQuery<Organization> obq = OBDal.getInstance().createQuery(Organization.class,
        // queryExtend.toString());
        //
        // if ((obq != null) && (obq.list().size() > 0)) {
        // Action = "TRANSFER";
        // } else {
        // Action = "INTERCOMPANY";
        // }
        // // System.out.println(queryExtend.toString());
        // }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
      Action = null;
    }

    return Action;
  }

  String getActionWhenisOutOfStock(OrderLine regOrderLine) {

    String Action = null;

    Organization regOrganization = getOrgLegal(regOrderLine.getSalesOrder().getOrganization()
        .getId());

    Organization regOrganizationStockLocation = getOrgLegal(regOrderLine.getSvfpaStockLocation()
        .getSvfpaOrgsource().getId());

    TYPE_SOURCE_VALUE = "";
    TYPE_SOURCE_VALUE_LOCATION = "";

    if (regOrganization != null) {
      TYPE_SOURCE_VALUE = regOrganization.getSearchKey();
    }

    if (regOrganizationStockLocation != null) {
      TYPE_SOURCE_VALUE_LOCATION = regOrganizationStockLocation.getSearchKey();
    }

    if ((TYPE_SOURCE_VALUE.equals("") != true) && (TYPE_SOURCE_VALUE_LOCATION.equals("") != true)) {

      if (TYPE_SOURCE_VALUE.equalsIgnoreCase(TYPE_SOURCE) == true) {

        if (TYPE_SOURCE_VALUE_LOCATION.equalsIgnoreCase(TYPE_SOURCE_VALUE) == true) {
          Action = "SOURCE";
        } else {
          if (regOrderLine.getProduct().isSvfpaIsvigproduct() == true) {
            Action = "SOURCE";
          } else {
            Action = "INTERCOMPANY";
          }
        }
      } else {
        if (TYPE_SOURCE_VALUE_LOCATION.equalsIgnoreCase(TYPE_SOURCE_VALUE) != true) {
          // if (regOrderLine.getProduct().isSvfpaIsvigproduct() == true) {
          Action = "INTERCOMPANY";
          // } else {
          // Action = "SOURCE";
          // }
        } else {
          if (regOrderLine.getProduct().isSvfpaIsvigproduct() == true) {
            Action = "INTERCOMPANY";
          } else {
            Action = "SOURCE";
          }
        }
      }
    }

    // if ((TYPE_SOURCE_VALUE.equalsIgnoreCase(TYPE_SOURCE) == true)
    // && (regOrderLine.getProduct().isSvfpaIsvigproduct() == true)) {
    // Action = "SOURCE";
    // } else {
    //
    // if (TYPE_SOURCE_VALUE.equalsIgnoreCase(TYPE_SOURCE) == true) {
    // Action = "SOURCE";
    // } else {
    // Action = "INTERCOMPANY";
    // }
    // // if (regOrderLine.getProduct().isSvfpaIsvigproduct() == true) {
    // // Action = "INTERCOMPANY";
    // // } else {
    // // Action = "SOURCE";
    // // }
    // }

    if ((regOrderLine.getProduct().getSvfsvProductCharact() != null)&&
    		!TYPE_SOURCE_VALUE.equals("")){
    	if ((regOrderLine.getProduct().getSvfsvProductCharact().equalsIgnoreCase("SO") == true)
    			&& (regOrderLine.getProduct().isSvfpaIsvigproduct() == true)
    			&& (TYPE_SOURCE_VALUE.equalsIgnoreCase(TYPE_SOURCE))) {
    		Action = "ISSUEPO";
    	}
    }
    //
    if (regOrderLine.getProduct().isDiscontinued() == true) {
      Action = "DISCONTINUED";
    }
    return Action;
  }

  void getActionNew(OrderLine regOrderLine) {
    String Action = null;

    if (regOrderLine.getSvfpaStockavailability() != null) {

      if (regOrderLine.getSvfpaStockavailability().equalsIgnoreCase("INSTOCK") == true) {

        if (regOrderLine.getSvfpaAction() != null) {

          if (regOrderLine.getSvfpaAction().equalsIgnoreCase("WAITINGFORPO") != true) {
            Action = getActionWhenisInStock(regOrderLine);
          }

        } else {
          Action = getActionWhenisInStock(regOrderLine);
        }

      } else {
        // if (regOrderLine.getSvfpaStockavailability().equalsIgnoreCase("OUTOFSTOCK") == true) {
        // Action = getActionWhenisOutOfStock(regOrderLine);
        // }
        if (regOrderLine.getSvfpaStockavailability().equalsIgnoreCase("OUTOFSTOCK") == true) {

          if (regOrderLine.getSvfpaAction() != null) {

            if (regOrderLine.getSvfpaAction().equalsIgnoreCase("WAITINGFORPO") != true) {
              Action = getActionWhenisOutOfStock(regOrderLine);
            }

          } else {
            Action = getActionWhenisOutOfStock(regOrderLine);
          }
        }
      }
    }
    if (Action != null) {
      regOrderLine.setSvfpaAction(Action);
      OBDal.getInstance().save(regOrderLine);
    }
  }

  BigDecimal getNumberWithoutDecimal(BigDecimal V) {

    BigDecimal R = BigDecimal.ZERO;
    try {
      String str = V.toString();
      int index = str.indexOf(".");
      if (index >= 0) {
        str = str.substring(0, index);
      }

      R = new BigDecimal(str);
    } catch (Exception e) {
    }
    return R;

  }

  void reCalculateQtyAvailableForItemByWarehouse(OrderLine regOrderLine, boolean isPO,
      Warehouse regWarehouse) {

    for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {

      if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
          .containsKey(regProductBOM.getBOMProduct().getId()) == true) {

        if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
            .get(regProductBOM.getBOMProduct().getId()) != null) {

          List<StockAvailable> ListStockAvailable = (isPO == false ? HashMapProductsAvailable
              : HashMapProductsAvailablePO).get(regProductBOM.getBOMProduct().getId());
          int Index = getStockAvailable(ListStockAvailable, regWarehouse);

          if (Index >= 0) {
            StockAvailable regStockAvailable = ListStockAvailable.get(Index);

            BigDecimal QtyToSubtract = regOrderLine.getOrderedQuantity().multiply(
                regProductBOM.getBOMQuantity());
            BigDecimal NewQtyAvailableForItem = regStockAvailable.getQtyAvailable().subtract(
                QtyToSubtract);

            if (NewQtyAvailableForItem.doubleValue() < 0) {
              NewQtyAvailableForItem = BigDecimal.ZERO;
            }
            ListStockAvailable.get(Index).setQtyAvailable(NewQtyAvailableForItem);
            (isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO).put(
                regProductBOM.getBOMProduct().getId(), ListStockAvailable);
          }

        }

      }
    }
  }

  void reCalculateQtyAvailableForItem(OrderLine regOrderLine, boolean isPO) {

    for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {

      if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
          .containsKey(regProductBOM.getBOMProduct().getId()) == true) {

        if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
            .get(regProductBOM.getBOMProduct().getId()) != null) {

          List<StockAvailable> ListStockAvailable = (isPO == false ? HashMapProductsAvailable
              : HashMapProductsAvailablePO).get(regProductBOM.getBOMProduct().getId());
          int Index = getStockAvailable(ListStockAvailable, regOrderLine.getSvfpaStockLocation());

          if (Index >= 0) {
            StockAvailable regStockAvailable = ListStockAvailable.get(Index);

            BigDecimal QtyToSubtract = regOrderLine.getOrderedQuantity().multiply(
                regProductBOM.getBOMQuantity());
            BigDecimal NewQtyAvailableForItem = regStockAvailable.getQtyAvailable().subtract(
                QtyToSubtract);

            if (NewQtyAvailableForItem.doubleValue() < 0) {
              NewQtyAvailableForItem = BigDecimal.ZERO;
            }
            ListStockAvailable.get(Index).setQtyAvailable(NewQtyAvailableForItem);
            (isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO).put(
                regProductBOM.getBOMProduct().getId(), ListStockAvailable);
          }

        }

      }
    }
  }

  void reCalculateQtyAvailableByWarehouse(OrderLine regOrderLine, boolean isPO,
      Warehouse regWarehouse) {
    BigDecimal QtyAvailableNew = null;
    BigDecimal QtyAvailableTemp = BigDecimal.ZERO;

    for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {

      if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
          .containsKey(regProductBOM.getBOMProduct().getId()) == true) {

        if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
            .get(regProductBOM.getBOMProduct().getId()) != null) {

          List<StockAvailable> ListStockAvailable = (isPO == false ? HashMapProductsAvailable
              : HashMapProductsAvailablePO).get(regProductBOM.getBOMProduct().getId());
          int Index = getStockAvailable(ListStockAvailable, regWarehouse);

          if (Index >= 0) {
            StockAvailable regStockAvailable = ListStockAvailable.get(Index);
            QtyAvailableTemp = regStockAvailable.getQtyAvailable().divide(
                regProductBOM.getBOMQuantity());

            if (QtyAvailableNew == null) {
              QtyAvailableNew = getNumberWithoutDecimal(QtyAvailableTemp);
            } else {

              if (QtyAvailableTemp.doubleValue() < QtyAvailableNew.doubleValue()) {
                QtyAvailableNew = getNumberWithoutDecimal(QtyAvailableTemp);
              }
            }
          }
        }
      }
    }

    if (QtyAvailableNew != null) {

      if (QtyAvailableNew.doubleValue() >= 0) {
        List<StockAvailable> ListStockAvailable = (isPO == false ? HashMapProductsAvailable
            : HashMapProductsAvailablePO).get(regOrderLine.getProduct().getId());
        int Index = getStockAvailable(ListStockAvailable, regWarehouse);
        if (Index >= 0) {
          ListStockAvailable.get(Index).setQtyAvailable(QtyAvailableNew);
          (isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO).put(regOrderLine
              .getProduct().getId(), ListStockAvailable);

        }

        if (QtyAvailableNew.doubleValue() > 0) {
          reCalculateQtyAvailableForItemByWarehouse(regOrderLine, isPO, regWarehouse);
        }

      }
    }
  }

  void reCalculateQtyAvailable(OrderLine regOrderLine, boolean isPO) {
    BigDecimal QtyAvailableNew = null;
    BigDecimal QtyAvailableTemp = BigDecimal.ZERO;

    for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {

      if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
          .containsKey(regProductBOM.getBOMProduct().getId()) == true) {

        if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
            .get(regProductBOM.getBOMProduct().getId()) != null) {

          List<StockAvailable> ListStockAvailable = (isPO == false ? HashMapProductsAvailable
              : HashMapProductsAvailablePO).get(regProductBOM.getBOMProduct().getId());
          int Index = getStockAvailable(ListStockAvailable, regOrderLine.getSvfpaStockLocation());

          if (Index >= 0) {
            StockAvailable regStockAvailable = ListStockAvailable.get(Index);
            QtyAvailableTemp = regStockAvailable.getQtyAvailable().divide(
                regProductBOM.getBOMQuantity());

            if (QtyAvailableNew == null) {
              QtyAvailableNew = getNumberWithoutDecimal(QtyAvailableTemp);
            } else {

              if (QtyAvailableTemp.doubleValue() < QtyAvailableNew.doubleValue()) {
                QtyAvailableNew = getNumberWithoutDecimal(QtyAvailableTemp);
              }
            }
          }
        }
      }
    }

    if (QtyAvailableNew != null) {

      if (QtyAvailableNew.doubleValue() >= 0) {
        List<StockAvailable> ListStockAvailable = (isPO == false ? HashMapProductsAvailable
            : HashMapProductsAvailablePO).get(regOrderLine.getProduct().getId());
        int Index = getStockAvailable(ListStockAvailable, regOrderLine.getSvfpaStockLocation());
        if (Index >= 0) {
          ListStockAvailable.get(Index).setQtyAvailable(QtyAvailableNew);
          (isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO).put(regOrderLine
              .getProduct().getId(), ListStockAvailable);

        }

        if (QtyAvailableNew.doubleValue() > 0) {
          reCalculateQtyAvailableForItem(regOrderLine, isPO);
        }

      }
    }
  }

  Product getMaxSubstitute(Product regProductOriginal, Warehouse regStockLocation, boolean isPO) {

    Product SubstituteProduct = null;
    StockAvailable regStockAvailableMax = null;

    try {
      for (SubstituteProduct regSubstituteProduct : regProductOriginal.getSVFADPMSubstituteList()) {

        List<StockAvailable> ListStockTemp = (isPO == false ? HashMapProductsAvailable
            : HashMapProductsAvailablePO).get(regSubstituteProduct.getSubstituteProduct().getId());

        int Index = getStockAvailable(ListStockTemp, regStockLocation);

        if (Index >= 0) {
          if (regStockAvailableMax == null) {
            regStockAvailableMax = ListStockTemp.get(Index);
            SubstituteProduct = regSubstituteProduct.getSubstituteProduct();
          } else {
            if (regStockAvailableMax.getQtyAvailable().doubleValue() < ListStockTemp.get(Index)
                .getQtyAvailable().doubleValue()) {
              regStockAvailableMax = ListStockTemp.get(Index);
              SubstituteProduct = regSubstituteProduct.getSubstituteProduct();
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }

    if ((regStockAvailableMax != null)
        && (regStockAvailableMax.getQtyAvailable().doubleValue() > 0)) {
      return SubstituteProduct;
    } else {
      return null;
    }

  }

  OrderLine InsertLine(OrderLine regOrderLine, Product regProduct, BigDecimal QtyNeed,
      String Status, Order regPurchaseOrder) {

    String strADOrgID = regOrderLine.getOrganization().getId();
    String strClientId = regOrderLine.getClient().getId();
    String strUserId = regOrderLine.getCreatedBy().getId();
    String strMWarehouseID = regOrderLine.getWarehouse().getId();
    String strUOM = regProduct.getUOM().getId();
    String strCOrderId = regOrderLine.getSalesOrder().getId();
    String strMProductID = regProduct.getId();
    BigDecimal QuantityAvailability = QtyNeed;
    String strPriceStd = String.valueOf(regOrderLine.getUnitPrice());
    String strPriceList = regOrderLine.getSalesOrder().getPriceList().getId();
    String strCurrency = regOrderLine.getCurrency().getId();
    String strTax = regOrderLine.getTax().getId();

    LastNoLine = LastNoLine + 10;
    Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
    OrderLine regOrderLineNew = ProductAvailabilityExtend.InsertLineInDocument(strADOrgID,
        strClientId, strUserId, strMWarehouseID, strUOM, strCOrderId, strMProductID,
        QuantityAvailability, strPriceStd, strPriceList, strCurrency, strTax, regOrderLine.getId(),
        nroLine);

    regOrderLineNew.setSvfpaStockLocation(StockLocationGlobal);
    regOrderLineNew.setSvfpaShipFrom(ShipFromGlobal);
    regOrderLineNew.setSvfpaStockavailability(Status);

    if (Status.equalsIgnoreCase("OUTOFSTOCK") == true) {
      regOrderLineNew.setSvfpaAction(getActionWhenisOutOfStock(regOrderLineNew));
    } else {
      regOrderLineNew.setSvfpaAction(getActionWhenisInStock(regOrderLineNew));

      // ---------------------------------------------------------------------
      if ((regPurchaseOrder != null)
          && (regPurchaseOrder.getDocumentNo().trim().equals("") != true)) {

        regOrderLineNew.setSvfpaStockavailability("OUTOFSTOCK");
        regOrderLineNew.setSvfpaAction("WAITINGFORPO");
        // regOrderLineNew.setSvfsvPoreference(regPurchaseOrder.getDocumentNo());
        regOrderLineNew.setSvfpaPurchaseorder(regPurchaseOrder);

      }
      // ---------------------------------------------------------------------
    }

    OBDal.getInstance().save(regOrderLineNew);
    UpdatedStatus(regOrderLineNew, "add");

    return regOrderLineNew;
  }

  BigDecimal actionToSubstitute(OrderLine regOrderLine, Product regProductOriginal,
      BigDecimal QtyNeed, String Action, boolean isPO) {
    BigDecimal QtyNeedNew = QtyNeed;

    Product SubstituteProduct = getMaxSubstitute(regProductOriginal,
        regOrderLine.getSvfpaStockLocation(), isPO);

    if (SubstituteProduct != null) {

      List<StockAvailable> ListStockAvailableTempNew = (isPO == false ? HashMapProductsAvailable
          : HashMapProductsAvailablePO).get(SubstituteProduct.getId());
      int Index = -1;// getStockAvailable(ListStockAvailableTempNew,
                     // regOrderLine.getSvfpaStockLocation());

      if (isPO == true) {
        Index = getStockAvailableMaxPO(ListStockAvailableTempNew,
            regOrderLine.getSvfpaStockLocation());
      } else {
        Index = getStockAvailable(ListStockAvailableTempNew, regOrderLine.getSvfpaStockLocation());
      }

      if (Index >= 0) {

        StockAvailable regStockAvailable = ListStockAvailableTempNew.get(Index);

        if (regStockAvailable != null) {

          if (regStockAvailable.getQtyAvailable().doubleValue() > 0/* QtyNeed.doubleValue() */) {

            if (Action.equalsIgnoreCase("NEWLINE") == true) {

              if (regStockAvailable.getQtyAvailable().doubleValue() > QtyNeed.doubleValue()) {
                InsertLine(regOrderLine, SubstituteProduct, QtyNeed, "INSTOCK",
                    regStockAvailable.getPurchaseOrder());
              } else {
                InsertLine(regOrderLine, SubstituteProduct, regStockAvailable.getQtyAvailable(),
                    "INSTOCK", regStockAvailable.getPurchaseOrder());
              }
            } else {

              regOrderLine.setProduct(SubstituteProduct);
              regOrderLine.setUOM(SubstituteProduct.getUOM());

              if (regStockAvailable.getQtyAvailable().doubleValue() > QtyNeed.doubleValue()) {
                regOrderLine.setOrderedQuantity(QtyNeed);
              } else {
                regOrderLine.setOrderedQuantity(regStockAvailable.getQtyAvailable());
              }
              regOrderLine.setSvfpaStockavailability("INSTOCK");
              // Here
              // regOrderLine.setSvfpaAction(getActionWhenisInStock(regOrderLine));

              // ------------------------------------------------------------------------------------

              if ((isPO == true)
                  && (regStockAvailable.getPurchaseOrder() != null)
                  && (regStockAvailable.getPurchaseOrder().getDocumentNo().trim().equals("") != true)) {

                regOrderLine.setSvfpaStockavailability("OUTOFSTOCK");
                regOrderLine.setSvfpaAction("WAITINGFORPO");
                regOrderLine.setSvfpaPurchaseorder(regStockAvailable.getPurchaseOrder());

              }
              // ------------------------------------------------------------------------------------

              UpdatedStatus(regOrderLine, "update");
              OBDal.getInstance().save(regOrderLine);

            }

            BigDecimal NewQuantityAvailable = regStockAvailable.getQtyAvailable().subtract(QtyNeed);
            QtyNeedNew = QtyNeed.subtract(regStockAvailable.getQtyAvailable());
            regOrderLine.setSvfpaPurchaseorder(regStockAvailable.getPurchaseOrder());

            ListStockAvailableTempNew.get(Index).setQtyAvailable(BigDecimal.ZERO);

            if (NewQuantityAvailable.doubleValue() > 0) {
              ListStockAvailableTempNew.get(Index).setQtyAvailable(NewQuantityAvailable);
            }

            (isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO).put(
                SubstituteProduct.getId(), ListStockAvailableTempNew);

            OBDal.getInstance().save(regOrderLine);

            if (QtyNeedNew.doubleValue() > 0) {
              return actionToSubstitute(regOrderLine, regProductOriginal, QtyNeedNew, "NEWLINE",
                  isPO);
            }

          }

        }
      }
    }
    if ((QtyNeedNew != null) && (QtyNeedNew.doubleValue() > 0)) {
      return QtyNeedNew;
    } else {
      return BigDecimal.ZERO;
    }

  }

  public BigDecimal CheckAvailabiltyAnotherWarehouse(OrderLine regOrderLine, BigDecimal QtyNeeded,
      String Type) {

    if (HashMapProductsAvailable.get(regOrderLine.getProduct().getId()) != null) {

      List<StockAvailable> ListStockAvailableTempNew = HashMapProductsAvailable.get(regOrderLine
          .getProduct().getId());

      int Index = getStockAvailableByType(ListStockAvailableTempNew, Type);

      if (Index >= 0) {
        StockAvailable regStockAvailable = ListStockAvailableTempNew.get(Index);

        if (regStockAvailable != null) {

          BigDecimal QtyOrdered = regOrderLine.getOrderedQuantity();
          if (regStockAvailable.getQtyAvailable().doubleValue() > 0) {

            if (regStockAvailable.getQtyAvailable().doubleValue() < regOrderLine
                .getOrderedQuantity().doubleValue()) {

              BigDecimal NewQuantityOrdered = regOrderLine.getOrderedQuantity().subtract(
                  regStockAvailable.getQtyAvailable());

              regOrderLine.setSvfpaStockavailability("INSTOCK");
              // Here
              // regOrderLine.setSvfpaAction(getActionWhenisInStock(regOrderLine));
              regOrderLine.setSvfpaStockLocation(regStockAvailable.getRegWareHouse());
              regOrderLine.setOrderedQuantity(regStockAvailable.getQtyAvailable());
              OBDal.getInstance().save(regOrderLine);
              UpdatedStatus(regOrderLine, "update");

              ListStockAvailableTempNew.get(Index).setQtyAvailable(BigDecimal.ZERO);

              if (regOrderLine.getProduct().isBillOfMaterials() == true) {
                reCalculateQtyAvailableByWarehouse(regOrderLine, false,
                    regStockAvailable.getRegWareHouse());
              }

              return NewQuantityOrdered;

            } else {

              BigDecimal NewQuantityAvailable = regStockAvailable.getQtyAvailable().subtract(
                  QtyOrdered);

              regOrderLine.setSvfpaStockavailability("INSTOCK");
              // Here
              // regOrderLine.setSvfpaAction(getActionWhenisInStock(regOrderLine));
              regOrderLine.setSvfpaStockLocation(regStockAvailable.getRegWareHouse());

              ListStockAvailableTempNew.get(Index).setQtyAvailable(BigDecimal.ZERO);

              if (NewQuantityAvailable.doubleValue() > 0) {
                ListStockAvailableTempNew.get(Index).setQtyAvailable(NewQuantityAvailable);
              }

              UpdatedStatus(regOrderLine, "update");
              OBDal.getInstance().save(regOrderLine);

              if (regOrderLine.getProduct().isBillOfMaterials() == true) {
                reCalculateQtyAvailableByWarehouse(regOrderLine, false,
                    regStockAvailable.getRegWareHouse());
              }

              return BigDecimal.ZERO;
            }

          }

        }
      }

    }

    return null;

  }

  OrderLine ProcessInOthersWarehouse(OrderLine regOrderLine, BigDecimal QtyNeeded, String Type) {

    BigDecimal QtyRest = CheckAvailabiltyAnotherWarehouse(regOrderLine, QtyNeeded, Type);
    OrderLine regOrderLineReturn = null;
    OrderLine regOrderLineNew = null;

    while ((QtyRest != null) && (QtyRest.doubleValue() > 0)) {

      regOrderLineNew = InsertLine(regOrderLine, regOrderLine.getProduct(), QtyRest, "OUTOFSTOCK",
          null);
      QtyRest = CheckAvailabiltyAnotherWarehouse(regOrderLineNew, QtyRest, Type);
    }

    if ((QtyRest == null) || (QtyRest.doubleValue() > 0)) {

      QtyRest = CheckAvailabiltyAnotherWarehouse(regOrderLineNew == null ? regOrderLine
          : regOrderLineNew, regOrderLineNew == null ? regOrderLine.getOrderedQuantity()
          : regOrderLineNew.getOrderedQuantity(), "INTERCOMPANY");

      while ((QtyRest != null) && (QtyRest.doubleValue() > 0)) {

        regOrderLineNew = InsertLine(regOrderLine, regOrderLine.getProduct(), QtyRest,
            "OUTOFSTOCK", null);
        QtyRest = CheckAvailabiltyAnotherWarehouse(regOrderLineNew, QtyRest, "INTERCOMPANY");
      }
    }

    if ((QtyRest != null) && (QtyRest.doubleValue() <= 0)) {
      return null;
    } else {

      regOrderLineReturn = regOrderLineNew;
      if (regOrderLineNew == null) {
        regOrderLineReturn = regOrderLine;
      }

      return regOrderLineReturn;
    }

  }

  public boolean CheckAvailabiltyByLineNew(OrderLine regOrderLine, boolean isPO) {

    try {
      BigDecimal QtyNeedOriginal = regOrderLine.getOrderedQuantity();
      Product regProductOriginal = regOrderLine.getProduct();

      if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
          .containsKey(regOrderLine.getProduct().getId()) == true) {

        if ((isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO)
            .get(regOrderLine.getProduct().getId()) != null) {

          if (regOrderLine.getProduct().isBillOfMaterials() == true) {
            reCalculateQtyAvailable(regOrderLine, isPO);
          }

          List<StockAvailable> ListStockAvailableTempNew = (isPO == false ? HashMapProductsAvailable
              : HashMapProductsAvailablePO).get(regOrderLine.getProduct().getId());

          int Index = -1;
          if (isPO == true) {
            Index = getStockAvailableMaxPO(ListStockAvailableTempNew,
                regOrderLine.getSvfpaStockLocation());
          } else {
            Index = getStockAvailable(ListStockAvailableTempNew,
                regOrderLine.getSvfpaStockLocation());
          }
          if (Index >= 0) {

            StockAvailable regStockAvailable = ListStockAvailableTempNew.get(Index);

            if (regStockAvailable != null) {
              BigDecimal QtyOrdered = regOrderLine.getOrderedQuantity();
              if (regStockAvailable.getQtyAvailable().doubleValue() > 0) {

                if (regStockAvailable.getQtyAvailable().doubleValue() < regOrderLine
                    .getOrderedQuantity().doubleValue()) {

                  BigDecimal NewQuantityOrdered = regOrderLine.getOrderedQuantity().subtract(
                      regStockAvailable.getQtyAvailable());

                  regOrderLine.setOrderedQuantity(regStockAvailable.getQtyAvailable());
                  regOrderLine.setSvfpaPurchaseorder(regStockAvailable.getPurchaseOrder());
                  regOrderLine.setSvfpaStockavailability("INSTOCK");
                  // Here
                  // regOrderLine.setSvfpaAction(getActionWhenisInStock(regOrderLine));
                  ListStockAvailableTempNew.get(Index).setQtyAvailable(BigDecimal.ZERO);

                  OBDal.getInstance().save(regOrderLine);

                  BigDecimal QtyRest = actionToSubstitute(regOrderLine, regProductOriginal,
                      NewQuantityOrdered, "NEWLINE", isPO);

                  if ((QtyRest.doubleValue() > 0)
                      && (QtyNeedOriginal.doubleValue() != QtyRest.doubleValue())) {

                    OrderLine regOrderLineNew = InsertLine(regOrderLine, regProductOriginal,
                        QtyRest, "OUTOFSTOCK", regStockAvailable.getPurchaseOrder());

                    if (isPO == false) {

                      // Checkout other warehouses
                      regOrderLineNew = ProcessInOthersWarehouse(regOrderLineNew, QtyRest,
                          "TRANSFER");
                      if (regOrderLineNew != null) {
                        CheckAvailabiltyByLineNew(regOrderLineNew, true);
                      }

                    }

                  } else {

                    if (isPO == false) {
                      // Checkout other warehouses
                      OrderLine regOrderLineNew = ProcessInOthersWarehouse(regOrderLine, QtyRest,
                          "TRANSFER");
                      if (regOrderLineNew != null) {
                        CheckAvailabiltyByLineNew(regOrderLineNew, true);
                      }

                    } else {
                      regOrderLine.setSvfpaStockavailability("OUTOFSTOCK");
                      // Here
                      // regOrderLine.setSvfpaAction(getActionWhenisOutOfStock(regOrderLine));
                      UpdatedStatus(regOrderLine, "update");
                      OBDal.getInstance().save(regOrderLine);

                    }

                  }

                }
                BigDecimal NewQuantityAvailable = regStockAvailable.getQtyAvailable().subtract(
                    QtyOrdered);

                regOrderLine.setSvfpaStockavailability("INSTOCK");
                // Here
                // regOrderLine.setSvfpaAction(getActionWhenisInStock(regOrderLine));

                // ------------------------------------------------------------------------------------

                if ((isPO == true)
                    && (regStockAvailable.getPurchaseOrder() != null)
                    && (regStockAvailable.getPurchaseOrder().getDocumentNo().trim().equals("") != true)) {

                  regOrderLine.setSvfpaStockavailability("OUTOFSTOCK");
                  regOrderLine.setSvfpaAction("WAITINGFORPO");
                  regOrderLine.setSvfpaPurchaseorder(regStockAvailable.getPurchaseOrder());
                }
                // ------------------------------------------------------------------------------------

                ListStockAvailableTempNew.get(Index).setQtyAvailable(BigDecimal.ZERO);

                if (NewQuantityAvailable.doubleValue() > 0) {
                  ListStockAvailableTempNew.get(Index).setQtyAvailable(NewQuantityAvailable);
                }

                (isPO == false ? HashMapProductsAvailable : HashMapProductsAvailablePO).put(
                    regOrderLine.getProduct().getId(), ListStockAvailableTempNew);

                UpdatedStatus(regOrderLine, "update");
                OBDal.getInstance().save(regOrderLine);
                return true;
              }

            }
          }

        }
      }
      BigDecimal QtyRest = actionToSubstitute(regOrderLine, regProductOriginal,
          regOrderLine.getOrderedQuantity(), "UPDATELINE", isPO);

      if ((QtyRest.doubleValue() > 0) && (QtyNeedOriginal.doubleValue() != QtyRest.doubleValue())) {

        OrderLine regOrderLineNew = InsertLine(regOrderLine, regProductOriginal, QtyRest,
            "OUTOFSTOCK", null);

        if (isPO == false) {
          // Checkout other warehouses
          regOrderLineNew = ProcessInOthersWarehouse(regOrderLineNew, QtyRest, "TRANSFER");

          if (regOrderLineNew != null) {
            CheckAvailabiltyByLineNew(regOrderLineNew, true);
          }
        }

      } else {

        if (QtyRest.doubleValue() > 0) {

          if (isPO == false) {

            // Checkout other warehouses
            OrderLine regOrderLineNew = ProcessInOthersWarehouse(regOrderLine, QtyRest, "TRANSFER");

            if (regOrderLineNew != null) {
              CheckAvailabiltyByLineNew(regOrderLineNew, true);
            }

          } else {
            regOrderLine.setSvfpaStockavailability("OUTOFSTOCK");
            // Here
            // regOrderLine.setSvfpaAction(getActionWhenisOutOfStock(regOrderLine));
            UpdatedStatus(regOrderLine, "update");
            OBDal.getInstance().save(regOrderLine);

          }
        } else {
          regOrderLine.setSvfpaStockavailability("INSTOCK");
          // Here
          // regOrderLine.setSvfpaAction(getActionWhenisInStock(regOrderLine));
          OBDal.getInstance().save(regOrderLine);

        }

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }

    return false;
  }

  public void MostrarStocks() {
    // Iterator iterador = HashMapProductsAvailable.entrySet().iterator();
    Iterator iterador = HashMapProductsAvailablePO.entrySet().iterator();
    // Iterator<Map.Entry<String, Float>> iterador = listaProductos.entrySet().iterator();

    Map.Entry ProductStock;

    while (iterador.hasNext()) {
      ProductStock = (Map.Entry) iterador.next();

      if (ProductStock.getValue() != null) {

        for (StockAvailable regStockAvailable : (List<StockAvailable>) ProductStock.getValue()) {
          System.out.println("----------------------------");
          System.out.println("product: " + ProductStock.getKey());
          // System.out.println("OrgTransaction: " +
          // regStockAvailable.getRegOrganization().getName());
          System.out.println("OrgWarehouse: "
              + regStockAvailable.getRegWareHouse().getSvfpaOrgsource().getName());
          System.out.println("Qty: " + regStockAvailable.getQtyAvailable());
          System.out.println("Warehouse: " + regStockAvailable.getRegWareHouse().getName());
          System.out
              .println("PO: " + regStockAvailable.getPurchaseOrder() != null ? regStockAvailable
                  .getPurchaseOrder().getDocumentNo() : "");

          if (regStockAvailable.getPurchaseOrder() != null) {
            if (regStockAvailable.getPurchaseOrder().getSvfpaEta() != null) {
              System.out.println("ETA: "
                  + (regStockAvailable.getPurchaseOrder().getSvfpaEta().toString()));
            }
          }

          System.out.println("Action: " + regStockAvailable.getAction());
          System.out.println("----------------------------");
        }
      }
    }
  }

  public void UpdatedStatus(OrderLine regOrderLine, String TypeOperation) {

    JSONObject ROW = new JSONObject();
    try {

      ROW.put("line_id", regOrderLine.getId());
      ROW.put("no_line", regOrderLine.getLineNo());
      ROW.put("product_id", regOrderLine.getProduct().getId());
      ROW.put("product_key", regOrderLine.getProduct().getSearchKey());
      ROW.put("product_name", regOrderLine.getProduct().getName());

      if (TypeOperation.equalsIgnoreCase("update")) {
        ROW_UPDATED.put(ROW);
      } else {
        ROW_ADD.put(ROW);
      }

    } catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      log.error(e);

    }

  }

  public static List<ProductByWareHouse> getStockNew(Product regProduct, String StockLocationOrgId,
      String ShipFromOrgId, String OrgIds, String Type) {
    List<ProductByWareHouse> ListProductByWareHouse = null;

    try {

      //

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS pw");
      queryExtend.append(" WHERE ");
      if ((OrgIds != null) && (OrgIds.isEmpty() == false)) {

        // if (Type.equalsIgnoreCase("RTP")) {
        // queryExtend.append(" pw.warehouse.svfpaOrgsource.id IN('" + StockLocationOrgId
        // + "') AND ");
        //
        // // --
        // queryExtend.append(" pw.warehouse.svfpaOrgsource.id IN('" + ShipFromOrgId + "') AND ");
        // queryExtend.append("'" + StockLocationOrgId + "'" + "=" + "'" + ShipFromOrgId + "'"
        // + " AND ");
        // queryExtend.append(" pw.warehouse.svfpaOrgsource.id IN (" + OrgIds + ") AND ");
        // // --
        //
        // } else {
        // if (Type.equalsIgnoreCase("TRANSFER") == true) {
        if (Type.equalsIgnoreCase("NOINTERCOMPANY") == true) {
          // queryExtend.append("NOT pw.warehouse.svfpaOrgsource='" + StockLocationOrgId
          // + "' AND pw.warehouse.svfpaOrgsource.id IN(" + OrgIds + ") AND ");
          //

          // queryExtend.append(" pw.warehouse.svfpaOrgsource.id IN('" + StockLocationOrgId
          // + "') AND ");

          // --
          // queryExtend.append(" pw.warehouse.svfpaOrgsource.id IN('" + ShipFromOrgId + "') AND ");
          // queryExtend.append("NOT '" + StockLocationOrgId + "'" + "=" + "'" + ShipFromOrgId + "'"
          // + " AND ");
          queryExtend.append(" pw.warehouse.svfpaOrgsource.id IN (" + OrgIds + ") AND ");
          // --

        } else {
          if (Type.equalsIgnoreCase("INTERCOMPANY") == true) {
            queryExtend.append(" NOT pw.warehouse.svfpaOrgsource.id IN(" + OrgIds + ") AND ");
          }
        }
        // }
      }
      queryExtend.append(" pw.product.id='" + regProduct.getId() + "' "
          + " ORDER BY pw.qtyOnHand DESC");

      // System.out.println(queryExtend.toString());
      OBQuery<ProductByWareHouse> obq = OBDal.getInstance().createQuery(ProductByWareHouse.class,
          queryExtend.toString());

      ListProductByWareHouse = obq.list();

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
      ListProductByWareHouse = null;
    }

    return ListProductByWareHouse;

  }

  private void setListInHashMapNew(String productId,
      List<ProductByWareHouse> ListProductByWareHouse, String Action) {

    List<StockAvailable> ListStockAvailable = new ArrayList<StockAvailable>();

    if (ListProductByWareHouse != null) {

      if (HashMapProductsAvailable.containsKey(productId) == true) {
        ListStockAvailable = HashMapProductsAvailable.get(productId);
      }

      for (ProductByWareHouse regProductByWareHouse : ListProductByWareHouse) {

        StockAvailable regStockAvailable = new StockAvailable(OrgSalesOrder,
            regProductByWareHouse.getWarehouse(), getAvailable(regProductByWareHouse), Action, null);

        ListStockAvailable.add(regStockAvailable);

      }
      HashMapProductsAvailable.put(productId, ListStockAvailable);
    }

  }

  void processStock(Product regProdut, OrderLine regOrderLine) {

    String StockLocationOrgId = regOrderLine.getSvfpaStockLocation().getSvfpaOrgsource().getId();
    String ShipFromOrgId = regOrderLine.getSvfpaShipFrom().getSvfpaOrgsource().getId();

    String strOrgParent = "0";
    try {
      strOrgParent = getOrgParent(SalesOrderGlobal.getOrganization().getId());
    } catch (Exception e) {
      strOrgParent = "0";
    }

    String strOrgAccessIds = getOrgAccess(strOrgParent);

    // get Stock in my Warehouse
    // setListInHashMapNew(regProdut.getId(),
    // getStockNew(regProdut, StockLocationOrgId, ShipFromOrgId, strOrgAccessIds, "RTP"), "RTP");

    // get Stock for Transfer
    // setListInHashMapNew(regProdut.getId(),
    // getStockNew(regProdut, StockLocationOrgId, ShipFromOrgId, strOrgAccessIds, "TRANSFER"),
    // "TRANSFER");

    setListInHashMapNew(
        regProdut.getId(),
        getStockNew(regProdut, StockLocationOrgId, ShipFromOrgId, strOrgAccessIds, "NOINTERCOMPANY"),
        "NOINTERCOMPANY");

    // get Stock for Intercompany
    setListInHashMapNew(regProdut.getId(),
        getStockNew(regProdut, StockLocationOrgId, ShipFromOrgId, strOrgAccessIds, "INTERCOMPANY"),
        "INTERCOMPANY");

  }

  private void setStockByStockAvailabilityAndActionNew(OrderLine regOrderLine) {
    try {

      if (HashMapProductsAvailable.containsKey(regOrderLine.getProduct().getId()) != true) {

        // Set the quantities of Stock
        // -----------------------------------------------------------------------------
        processStock(regOrderLine.getProduct(), regOrderLine);

        // Set the quantities of PO
        setListInHashMapPO(
            regOrderLine.getProduct().getId(),
            getStockPO(regOrderLine.getProduct().getId(), regOrderLine.getSvfpaStockLocation()
                .getId(), regOrderLine.getSalesOrder()));

        // Set the quantities for product has materials
        // -----------------------------------------------------------------------------
        if (regOrderLine.getProduct().isBillOfMaterials() == true) {
          for (ProductBOM regProductBOM : regOrderLine.getProduct().getProductBOMList()) {

            // Set the quantities of Stock
            processStock(regProductBOM.getBOMProduct(), regOrderLine);

            // Set the quantities of PO
            setListInHashMapPO(
                regProductBOM.getBOMProduct().getId(),
                getStockPO(regProductBOM.getBOMProduct().getId(), regOrderLine
                    .getSvfpaStockLocation().getId(), regOrderLine.getSalesOrder()));
          }
        }

        // Set the quantities for substitutes
        // -----------------------------------------------------------------------------
        for (SubstituteProduct regSubstituteProduct : regOrderLine.getProduct()
            .getSVFADPMSubstituteList()) {

          // Set the quantities of Stock
          processStock(regSubstituteProduct.getSubstituteProduct(), regOrderLine);

          // Set the quantities of PO
          setListInHashMapPO(
              regSubstituteProduct.getSubstituteProduct().getId(),
              getStockPO(regSubstituteProduct.getSubstituteProduct().getId(), regOrderLine
                  .getSvfpaStockLocation().getId(), regOrderLine.getSalesOrder()));
        }
        // ------------------------------------------------------------------------------

      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
  }

  void CheckOutActions(List<OrderLine> ListOrderLineParm) {

    if ((ListOrderLineParm != null) && (ListOrderLineParm.size() > 0)) {

      for (OrderLine regOrderLine : ListOrderLineParm) {
        getActionNew(regOrderLine);
      }
    }

  }

  public void IterationProcessNew(List<OrderLine> ListOrderLineParm) {
    try {

      ListOrderLine = ListOrderLineParm;

      if (ListOrderLine != null) {

        for (OrderLine regOrderLine : ListOrderLine) {

          regOrderLine.setSvfpaAction(null);
          regOrderLine.setSvfpaPurchaseorder(null);

          if (regOrderLine.getSvfpaStockLocation() != null) {
            StockLocationGlobal = regOrderLine.getSvfpaStockLocation();
          } else {
            StockLocationGlobal = regOrderLine.getSalesOrder().getSvfpaStockLocation();
          }

          if (regOrderLine.getSvfpaShipFrom() != null) {
            ShipFromGlobal = regOrderLine.getSvfpaShipFrom();
          } else {
            ShipFromGlobal = regOrderLine.getSalesOrder().getSvfpaShipFrom();
          }

          // Put the quantities of stock for this product in array
          setStockByStockAvailabilityAndActionNew(regOrderLine);
          // Begin the process to checkout availability for the product
          CheckAvailabiltyByLineNew(regOrderLine, false);

        }

        try {

          OBDal.getInstance().save(SalesOrderGlobal);
          OBDal.getInstance().commitAndClose();
          CheckOutActions(getAllOrderLines(SalesOrderGlobal.getId()));
        } catch (Exception e) {
          e.printStackTrace();
          log.error(e.getMessage());
        }
        // System.out.println("STOCK FINAL");
        // MostrarStocks();
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }

  }

  public JSONObject CheckAvailabilityProcess(Order regOrder) {
    try {

      OBContext.setAdminMode(true);
      try {

        if (regOrder.getOrganization().getOrganizationType().equals("1") == true) {
          TYPE_SOURCE_VALUE = regOrder.getOrganization().getSearchKey();
        } else {

          Organization regOrg = OBDal.getInstance().get(Organization.class,
              getOrgParent(regOrder.getOrganization().getId()));

          if (regOrg != null) {
            if (regOrg.getOrganizationType().equals("1") == true) {
              TYPE_SOURCE_VALUE = regOrg.getSearchKey();
            }
          }
        }

        SalesOrderGlobal = regOrder;
        OrgSalesOrder = regOrder.getOrganization();
        WareSalesOrder = regOrder.getWarehouse();

        ListOrderLine = new ArrayList<OrderLine>();
        ListOrderLineNewest = new ArrayList<OrderLine>();

        // IterationProcess(getAllOrderLines(regOrder.getId()), false);
        IterationProcessNew(getAllOrderLines(regOrder.getId()));

        JSONObject ROW_END = new JSONObject();

        int regProcess = 0;
        int regAdded = 0;
        int regUpdated = 0;

        if (ListOrderLine != null) {
          regProcess = ListOrderLine.size();
        }

        if (ROW_ADD != null) {
          ROW_END.put("row-added", ROW_ADD);
          regAdded = ROW_ADD.length();
        }

        if (ROW_UPDATED != null) {
          ROW_END.put("row-updated", ROW_UPDATED);
          regUpdated = ROW_UPDATED.length();
        }

        ROW_END.put("total", regProcess);
        ROW_END.put("total_added", regAdded);
        ROW_END.put("total_updated", regUpdated);

        return ROW_END;
      } catch (Exception e) {
        e.printStackTrace();
        log.error(e);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return null;
  }
}
