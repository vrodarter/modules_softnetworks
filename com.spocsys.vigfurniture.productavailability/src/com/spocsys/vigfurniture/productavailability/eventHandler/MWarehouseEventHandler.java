package com.spocsys.vigfurniture.productavailability.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;

public class MWarehouseEventHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Warehouse.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Warehouse regWarehouse = (Warehouse) event.getTargetInstance();

    if (regWarehouse != null) {

      Entity WarehouseEntity = ModelProvider.getInstance().getEntity(Warehouse.ENTITY_NAME);
      Property PropertyOrg = WarehouseEntity.getProperty(Warehouse.PROPERTY_ORGANIZATION);

      Organization regOrg = getOrg("0");

      if (regOrg != null) {
        event.setCurrentState(PropertyOrg, regOrg);
      }
    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Warehouse regWarehouse = (Warehouse) event.getTargetInstance();

    if (regWarehouse != null) {

      Entity WarehouseEntity = ModelProvider.getInstance().getEntity(Warehouse.ENTITY_NAME);
      Property PropertyOrg = WarehouseEntity.getProperty(Warehouse.PROPERTY_ORGANIZATION);

      Organization regOrg = getOrg("0");

      if (regOrg != null) {
        event.setCurrentState(PropertyOrg, regOrg);
      }
    }
  }

  Organization getOrg(String ID) {
    Organization regOrg = null;
    try {
      OBContext.setAdminMode(true);
      try {
        regOrg = OBDal.getInstance().get(Organization.class, ID);
      } catch (Exception e) {
        e.printStackTrace();
        logger.error(e);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return regOrg;
  }

}