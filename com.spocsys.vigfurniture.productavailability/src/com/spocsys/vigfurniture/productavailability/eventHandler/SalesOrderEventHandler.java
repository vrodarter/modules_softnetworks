package com.spocsys.vigfurniture.productavailability.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.model.common.order.Order;

public class SalesOrderEventHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Order.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Order regOrder = (Order) event.getTargetInstance();

    if (regOrder != null) {

      Entity OrderEntity = ModelProvider.getInstance().getEntity(Order.ENTITY_NAME);
      Property PropertyStockLocation = OrderEntity.getProperty(Order.PROPERTY_SVFPASTOCKLOCATION);
      Property PropertyShipFrom = OrderEntity.getProperty(Order.PROPERTY_SVFPASHIPFROM);
      
      
      if (regOrder.getSvfpaStockLocation() == null) {
        event.setCurrentState(PropertyStockLocation, regOrder.getOrganization()
            .getSVFPAStockLocation());
      }

      if (regOrder.getSvfpaShipFrom() == null) {
        event.setCurrentState(PropertyShipFrom, regOrder.getOrganization().getSVFPAShipFrom());
      }
    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Order regOrder = (Order) event.getTargetInstance();

    if (regOrder != null) {

      Entity OrderEntity = ModelProvider.getInstance().getEntity(Order.ENTITY_NAME);
      Property PropertyStockLocation = OrderEntity.getProperty(Order.PROPERTY_SVFPASTOCKLOCATION);
      Property PropertyShipFrom = OrderEntity.getProperty(Order.PROPERTY_SVFPASHIPFROM);
      Property PropertyStatus = OrderEntity.getProperty(Order.PROPERTY_SVFPASTATUS);
      
      //event.setCurrentState(PropertyStatus, "New");

      if (regOrder.getSvfpaStockLocation() == null) {
        event.setCurrentState(PropertyStockLocation, regOrder.getOrganization()
            .getSVFPAStockLocation());
      }

      if (regOrder.getSvfpaShipFrom() == null) {
        event.setCurrentState(PropertyShipFrom, regOrder.getOrganization().getSVFPAShipFrom());
      }
    }
  }

}