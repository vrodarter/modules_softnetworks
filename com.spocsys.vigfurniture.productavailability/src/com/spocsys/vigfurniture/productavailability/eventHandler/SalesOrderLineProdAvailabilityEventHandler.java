package com.spocsys.vigfurniture.productavailability.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityDeleteEvent;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.model.common.order.OrderLine;

public class SalesOrderLineProdAvailabilityEventHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(OrderLine.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final OrderLine regOrderLine = (OrderLine) event.getTargetInstance();

    if (regOrderLine != null) {

      if (regOrderLine.getSalesOrder().isSalesTransaction() == false) {
        if (regOrderLine.getProduct().isDiscontinued() == true) {
          throw new OBException("@SVFPA_ErrorDiscontinuedProductPO@");
        }
      }

    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final OrderLine regOrderLine = (OrderLine) event.getTargetInstance();

    if (regOrderLine != null) {

      if (regOrderLine.getSalesOrder().isSalesTransaction() == false) {
        if (regOrderLine.getProduct().isDiscontinued() == true) {
          throw new OBException("Error: trying to order a discontinued product");
        }
      } else {

        Entity OrderLineEntity = ModelProvider.getInstance().getEntity(OrderLine.ENTITY_NAME);
        Property StockLocationProperty = OrderLineEntity
            .getProperty(OrderLine.PROPERTY_SVFPASTOCKLOCATION);
        Property ShipFromProperty = OrderLineEntity.getProperty(OrderLine.PROPERTY_SVFPASHIPFROM);
        

       // if (regOrderLine.getSvfpaStockLocation() == null)
        //{
          event.setCurrentState(StockLocationProperty, regOrderLine.getSalesOrder()
              .getSvfpaStockLocation());
        //}

        //if (regOrderLine.getSvfpaShipFrom() == null) 
        //{
          event.setCurrentState(ShipFromProperty, regOrderLine.getSalesOrder().getSvfpaShipFrom());
        //}
      }

    }

  }

  public void onDelete(@Observes EntityDeleteEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

  }

}