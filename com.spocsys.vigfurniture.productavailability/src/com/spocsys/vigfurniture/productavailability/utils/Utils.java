package com.spocsys.vigfurniture.productavailability.utils;

import java.math.BigDecimal;
import java.util.List;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.enterprise.Organization;

public class Utils {

  public Utils() {
    // TODO Auto-generated constructor stub
  }

  public static BigDecimal getBiDecimal(String V) {
    try {
      return new BigDecimal(V);
    } catch (Exception e) {
      return null;
    }
  }

  public static String serializeToStringOrgIds(List<Organization> ListOrganization) {

    String Ids = "";
    if (ListOrganization != null) {
      int I = 0;
      for (Organization regOrganization : ListOrganization) {
        if (I > 0) {
          Ids = "'" + regOrganization.getId() + "'";
        } else {
          Ids += ", '" + regOrganization.getId() + "'";
        }

      }
    }

    return Ids;

  }

  public static List<Organization> getOrganizationIncluded(String orgParent) {

    String clientId = OBContext.getOBContext().getCurrentClient().getId();

    final StringBuilder queryExtend = new StringBuilder();
    queryExtend.append(" AS org");
    queryExtend.append(" WHERE ");
    queryExtend.append(" AD_ISORGINCLUDED('" + orgParent + "', org.id, '" + clientId + "')<>-1 ");

    OBQuery<Organization> obq = OBDal.getInstance().createQuery(Organization.class,
        queryExtend.toString());

    if ((obq != null) && (obq.list() != null)) {
      return obq.list();
    }

    return null;
  }
}
