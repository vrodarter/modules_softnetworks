isc.OBPopup.create
({
    ID: "SVFPA_WindowPopupProcess",
    title: "Product Availability",
    autoSize:true,
    autoCenter: true,
    showMinimizeButton: false,
    showMaximizeButton: false,
    isModal: true,
    showModalMask: true,
    autoDraw: false,
    height: "30%",
    width:"50%",
    closeClick: function () {WindowPopupIGALMStock.hide();},
    items:
    [
		
		isc.VStack.create
		({
			ID: "SVFPA_WindowPopupProcessCapaData",
			width:"100%",
			//visible: false,
			autoDraw: false,
			members:
		    [
		     
		    ]
		})
    ],
    begin: function()
    {
      try
      {
    	  var Height = (this.getHeight() - 28)+"px";
    	  OB.SVFPA.WindoLoadingImg = OB.SVFPA.WindoLoadingImg.replace(/000HEIGHT/g, Height);
    	  var SVFPA_WindoLoadingImgVar = isc.HTMLFlow.create
    	  ({
    	        ID: "SVFPAWindoLoadingImg",
    	        contents: OB.SVFPA.WindoLoadingImg,
    	        width:"100%",
    	        height:"100%"
    	  });
    	  SVFPA_WindowPopupProcessCapaData.removeMembers(SVFPA_WindowPopupProcessCapaData.getMembers());
    	  SVFPA_WindowPopupProcessCapaData.addMember(SVFPA_WindoLoadingImgVar);
       }
       catch(e)
       {
    	  this.hide();
    	  isc.warn("There was an error while it was executing the action");
    	  console.log(e); 
       }
    }
});



OB.SVFPA = OB.SVFPA || {};

OB.SVFPA.Process =
{
  viewLines: null,		
  grid: null,
  getTabLines: function(tabs)
  {
	 try
	 {
		  if((tabs!= null) && (tabs.length >0))
		  {
			for(var I=0; I< tabs.length; I++)
			{
			  if(tabs[I].pane.tabId=='187')
			  {
				 return tabs[I].pane;
			  }
			}
		  }
	 }
	 catch(e)
	 {
	   console.log(e.message);	 
	 }
	 return null;
  },
  callback: function (rpcResponse, data, rpcRequest)
  {
	// isc.SVFPA_FieldJustInitWidget.getButtonsSaveAndRefresh(OB.SVFPA.Process.viewLines.parentView.toolBar.leftMembers); 
	  
	 try
	 {
		 if((data!= null) && (data.result))
		 {
		   if(data.result=='nook')
		   {
			 isc.warn('There was an error while it was executing the action');
			 console.log(data.error);
		   }
		   else
		   {
//			   if(OB.SVFPA.Process.viewLines.isEditingGrid==false)
//               {
//				  isc.SVFPA_FieldJustInitWidget.ButtonRefresh.click();
//               }
//			   else
//			   {
//				  OB.SVFPA.Process.viewLines.viewGrid.refreshGrid();   
//			   }	   
			   //isc.SVFPA_FieldJustInitWidget.ButtonRefresh.click();
			   var MessageText = 'record added: ' + data.result.total_added;
			   MessageText+= ', record updated: ' + data.result.total_updated;
			   OB.SVFPA.Process.viewLines.messageBar.setMessage('success', null, MessageText);
			   OB.SVFPA.Process.viewLines.refresh();
		   }
		   //console.log(data.result);
		 }
	 }
	 catch(e)
	 {
	   console.log(e.message);	 
	 }
	 SVFPA_WindowPopupProcess.hide();
	 
  },
  beginProcess: function (OrderID, viewLines)
  {
	  OB.SVFPA.Process.viewLines= viewLines;
	  SVFPA_WindowPopupProcess.show();
	  SVFPA_WindowPopupProcess.begin();
	  OB.RemoteCallManager.call('com.spocsys.vigfurniture.productavailability.ad_process.CheckAvailabilityProcess', 
	  {
	     "OrderID": OrderID
	  }, {}, OB.SVFPA.Process.callback);
  },
  execute: function (params, view, viewLines)
  {
	try
	{
	  OB.SVFPA.Process.grid= params.button.contextView.viewGrid;
	  var RegistroView = OB.SVFPA.Process.grid.getSelectedRecords();
	  var OrderID = RegistroView[0].id;
	  OB.SVFPA.Process.beginProcess(OrderID, viewLines);
	}
	catch(e)
	{
	  console.log(e.message);	
	}
	  
  },

  CheckAvailability: function (params, view)
  {
	try
	{
	  var Tabs = params.button.contextView.children[1].tabs;
	  var TabLine = OB.SVFPA.Process.getTabLines(Tabs);
	  params.action = 'CheckAvailability';
      OB.SVFPA.Process.execute(params, view, TabLine);
	}
	catch(e)
	{
	  console.log(e.message);	
	}
  }
};
