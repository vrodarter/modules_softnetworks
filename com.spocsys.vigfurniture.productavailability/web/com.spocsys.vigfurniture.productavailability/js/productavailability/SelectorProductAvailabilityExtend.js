isc.defineClass('SVFPA_FieldJustInitWidget', isc.Label/*isc.OBGridFormButton*/);
isc.defineClass('SVFPA_Tab', isc.OBTabSetChild/*isc.OBGridFormButton*/);
isc.defineClass('SVFPA_POSItems', isc.OBGrid);
isc.defineClass('SVFPA_GridItems', isc.OBGrid);

isc.SVFPA_Tab.addProperties
(
  {
    height: "45%",
	width:"100%"    
  }		
);

isc.SVFPA_POSItems.addProperties
({
   showFilterEditor: true,
   dataSource: null,
   titleOrientation: 'top',
   editEvent: 'click',
   dataProperties:
   {
     useClientFiltering: false
   },
   gridFields:
   [
		 {
			name: 'idpo',
			title: 'PO ID',
			canFilter: false,
		    filterOnKeypress: false,
		    titleOrientation : 'top',
		    hidden:true
		 },
		 {
			name: 'idso',
			title: 'SO ID',
			canFilter: false,
			filterOnKeypress: false,
			titleOrientation : 'top',
			hidden:true
		 },
		 {
			name: 'product.id',
			title: 'Product ID',
			canFilter: false,
			filterOnKeypress: false,
			titleOrientation : 'top',
			hidden:true
		 },
         {
			name: 'documentnopo',
			type: '_id_10',
			title: 'PO number',
			canFilter: true,
		    filterOnKeypress: true,
		    titleOrientation : 'top',
		    align: 'center',
		 },
		 {
			name: 'orderedQuantity',
			type: '_id_29',
			title: 'Qty ordered',
			width: '100',
			canFilter: false,
			filterOnKeypress: false,
			titleOrientation : 'top'
			   	
		 },
		 {
		    name: 'qtyallocated',
		    type: '_id_29',
		    title: 'Qty allocated',
		    width: '100',
		    canFilter: false,
		    filterOnKeypress: false,
		    titleOrientation : 'top'
		   	
		 },
		 {
		    name: 'qtypo',
		    type: '_id_29',
		    title: 'Qty Available',
		    width: '100',
		    canFilter: false,
		    filterOnKeypress: false,
		    titleOrientation : 'top'
		 },
		 {
			name: 'documentnoso',
			type: '_id_10',
			title: 'Sales Order',
			canFilter: true,
			filterOnKeypress: true,
			titleOrientation : 'top',
			align: 'center',
			width: '300'
		 },
		 {
			name: 'customerReforder',
			type: '_id_10',
			title: 'Customer Reference Order',
			canFilter: true,
			filterOnKeypress: true,
			titleOrientation : 'top',
			align: 'center',
			width: '200'
		 },
		 {
			name: 'eTAEstimatedTimeOfArrival',
			title: 'ETA ( Estimated Time Of Arrival)',
			canFilter: true,
			editorType: 'OBDateTimeItem',
			filterEditorType: 'OBMiniDateRangeItem',
	        type: '_id_16',
	        align: 'right',
			filterOnKeypress: true,
			titleOrientation : 'top',
			width:'150'
		 }/*,
		 {
		     filterOnKeypress: false, 
		     name: 'SVFPA_quantityAvailability',
		     title: 'Quantity Needed',
		     type: '_id_29',
		     canEdit: true,
		     width: '100px',
		     change: function(a, item, value)
		     {
			    try
			    {
				   var V= value;
				  
				   if((V!=null) && (V.length > 0))
				   {	   
					   V= V.toString();
					   V = V.replace(/\,/g, '');
					   V = V.replace(/\./g, '');
					  
					   if ((V!= null) && (V!= '') && (V.length > 0))
					   {
						  var re=/^\d+(\.\d+)?$/;
						
						  if((V.length > 11) || (re.test(V)==false))
						  {
						    return false;  
						  }
					   } 
				   	   //---------------------------
				   }
				   
			       isc.SVFPA_FieldJustInitWidget.LastQuantityEntered= value;
			       isc.SVFPA_FieldJustInitWidget.executedSelector= true;
			       
			       isc.SVFPA_FieldJustInitWidget.WindowPopup = isc.SVFPA_GetID('WindowPopup');
			       var rowNum = isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.getSelectionObject().lastRow;
			       
			       isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.setEditValue(rowNum, 'SVFPA_quantityAvailability', value);
			       isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.data.localData[rowNum]['SVFPA_quantityAvailability']= value;
			       
			    }
			    catch(e)
			    {
				  console.log(e.message); 
			    }
		     }
	       }*/
	     
  ],
  resetAllCellOnQtyNeeded: function()
  {
	  var CountRow = 0;
	  
	  try
	  {
		CountRow = this.getData().getLength();
		for(var I= 0; I< CountRow; I++)
		{
		  this.setEditValue(I, 'SVFPA_quantityAvailability', null);
		}	
	  }
	  catch(e)
	  {
		console.log(e.message);  
	  }
  },
  
  cellClick: function (record, rowNum, colNum)
  {
     try
     {
		if(this.getFieldName(colNum)=='documentnopo')
	    {
	      OB.Utilities.openDirectTab('294', record.idpo,'GRID', 1, null);
	      SVFPA_FieldJustInitWidget.WindowPopup.hide();
	    }
		else
	    {
			if(this.getFieldName(colNum)=='documentnoso')
		    {
		      OB.Utilities.openDirectTab('186', record.idso,'GRID', 1, null);
		      SVFPA_FieldJustInitWidget.WindowPopup.hide();
		    }
	    }	
     }
	 catch(e)
	 {
	   console.log(e); 
	 }
  },
  cellContextClick: "this.sayCellEvent('Context-clicked', record, colNum); return false;",
  getCellCSSText: function (record, rowNum, colNum)
  {
		try
		{
			var Properties = 'color: #006600; text-decoration:underline;cursor:pointer;font-weight:bold;';
			
			if((this.getFieldName(colNum) == 'documentnopo') || (this.getFieldName(colNum) == 'documentnoso'))
			{
			  return Properties;							
			}
				
		}
		catch(e)
		{
		  console.log(e.message);	
		}
   },
   selectionUpdated: function(record, recordList)
   {
	  try
	  {
		  var rowNumSelected = this.getSelectionObject().lastRow;
		  var value= this.getEditValue(rowNumSelected, 'SVFPA_quantityAvailability');
		  
		   /*if((value != null) && (value.length > 0))
		   {	*/
			   isc.SVFPA_FieldJustInitWidget.LastQuantityEntered= value;
		       isc.SVFPA_FieldJustInitWidget.executedSelector= true;
		       
		       isc.SVFPA_FieldJustInitWidget.WindowPopup = isc.SVFPA_GetID('WindowPopup');
		       var rowNum = isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.getSelectionObject().lastRow;
		       
		       isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.setEditValue(rowNum, 'SVFPA_quantityAvailability', value);
		       isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.data.localData[rowNum]['SVFPA_quantityAvailability']= value;  
			   
		   //}
	  }
	  catch(e)
	  {
		console.log(e.message);  
	  }
	   
   },
   onFetchData: function(criteria, requestProperties)
   {
	  requestProperties = requestProperties || {};
	  requestProperties.params = requestProperties.params || {};
	  
	  var svfpaProductPriceWhV_ID= isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.svfpaProductPriceWhV_ID;
	  /*var PRODUCT_ID= isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.PRODUCT_ID;
	  var WAREHOUSE_ID= isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.WAREHOUSE_ID;*/
	  
      var recordSelected= null;
	  
	  try
	  {
		recordSelected= isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.getSelectedRecord();
	  }
	  catch(e)
	  {
		console.log(e);  
	  }
	  
	  if(svfpaProductPriceWhV_ID == null)
	  {
		svfpaProductPriceWhV_ID= '';
		
		if(recordSelected!= null)
		{
		  svfpaProductPriceWhV_ID= recordSelected.id;	
		}
	  }
	  
	  /*if(PRODUCT_ID == null)
	  {
		PRODUCT_ID= '';
		
		if(recordSelected!= null)
		{
		  PRODUCT_ID= recordSelected.product;	
		}
	  }
	  
	  if(WAREHOUSE_ID == null)
	  {
		WAREHOUSE_ID= '';
		
		if(recordSelected!= null)
		{
			WAREHOUSE_ID= recordSelected.warehouse;	
		}
	  } */
	 // requestProperties.params[OB.Constants.WHERE_PARAMETER] = "product.id='" +PRODUCT_ID+ "'";
	  requestProperties.params[OB.Constants.WHERE_PARAMETER] = "svfpaProductPriceWhV.id='" +svfpaProductPriceWhV_ID+ "'";
	  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.svfpaProductPriceWhV_ID= null;
	  
	  /*isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.PRODUCT_ID = null;
	  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.WAREHOUSE_ID = null;*/
	  
   },
   destroy: function ()
   {  
	  if(this.dataSource)
	  {
	    this.dataSource.destroy(); // needs to be done manually
	  }
	  this.Super('destroy', arguments);
   },
   setDataSource: function(ds)
   {
     // is called when the datasource is retrieved from the server
	 this.Super('setDataSource', [ds, this.gridFields]);
     this.refreshFields();
     //this.sort('name');
     this.fetchData();
   },
   /*searchAgain:function()
   {
	 //inicialiar  
	 OB.Datasource.get('SVFPA_Inprod_V');  
	 var DS= OB.Datasource.get('SVFPA_Inprod_V');
	 this.setDataSource(DS);
   },*/
   initWidget: function()
   {
	    //OB.Datasource.get('SVFPINC_Incoming_Prod', this, null, true);
	   OB.Datasource.get('SVFPA_Inprod_V', this, null, true);
	   this.Super('initWidget', arguments);
   }
});

//-------------------------------------------
isc.SVFPA_GridItems.addProperties
({
   showFilterEditor: true,
   dataSource: null,
   titleOrientation: "top",
   dataProperties:
   {
     useClientFiltering: false
   },
 
   gridFields:
   [
		 {
			name: 'idtab',
			title: 'Tab',
			canFilter: false,
		    filterOnKeypress: false,
		    titleOrientation : 'top',
		    hidden:true
		 },
		 {
			name: 'idline',
			title: 'Line',
			canFilter: false,
			filterOnKeypress: false,
			titleOrientation : 'top',
			hidden:true
		 },
         {
			name: 'documentNo',
			type: '_id_10',
			title: 'Document number',
			canFilter: true,
		    filterOnKeypress: true,
		    titleOrientation : 'top',
		    align: 'center',
		   	width: '300'
		 },
		 {
			name: 'movementDate',
			title: 'Movement date',
			canFilter: true,
			editorType: 'OBDateTimeItem',
			filterEditorType: 'OBMiniDateRangeItem',
            type: '_id_16',
            align: 'right',
		    filterOnKeypress: true,
		    titleOrientation : 'top',
		    width:'150'
		    
		 },
	     {
	        name: 'movementQuantity',
	        type: '_id_29',
	        title: 'Qty',
	        width: '100',
	        canFilter: false,
		    filterOnKeypress: false,
		    titleOrientation : 'top'
	     },
	     {
	    	 name: 'movementType',
	    	 type: '_id_189',
	    	 title: 'Type of Document',
	    	 canFilter: true,
			 filterOnKeypress: true,
			 titleOrientation : 'top',
			 align:'center'
	     },
	     
  ],
  
  cellClick: function (record, rowNum, colNum)
  {
     try
     {
		if(this.getFieldName(colNum)=='documentNo')
	    {
	      OB.Utilities.openDirectTab(record.idtab, record.idline,'GRID', 1, null);
	      SVFPA_FieldJustInitWidget.WindowPopup.hide();
	    }
     }
	 catch(e)
	 {
	   console.log(e); 
	 }
  },
  cellContextClick: "this.sayCellEvent('Context-clicked', record, colNum); return false;",
  getCellCSSText: function (record, rowNum, colNum)
  {
		try
		{
			var Properties = 'color: #006600; text-decoration:underline;cursor:pointer;font-weight:bold;';
			
			if(this.getFieldName(colNum) == 'documentNo')
			{
			  return Properties;							
			}
				
		}
		catch(e)
		{
		  console.log(e.message);	
		}
   },
   onFetchData: function(criteria, requestProperties)
   {
	  requestProperties = requestProperties || {};
	  requestProperties.params = requestProperties.params || {};
	  
	  var svfpaProductPriceWhV_ID= isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.svfpaProductPriceWhV_ID;
	  /*var PRODUCT_ID= isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.PRODUCT_ID;
	  var WAREHOUSE_ID= isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.WAREHOUSE_ID;*/
	  var recordSelected= null;
	  
	  try
	  {
		recordSelected= isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.getSelectedRecord();
	  }
	  catch(e)
	  {
		console.log(e);  
	  }
	  
	  if(svfpaProductPriceWhV_ID == null)
	  {
		  svfpaProductPriceWhV_ID= '';
		
		if(recordSelected!= null)
		{
			svfpaProductPriceWhV_ID= recordSelected.id;	
		}
	  }
	  
	  /*if(PRODUCT_ID == null)
	  {
		PRODUCT_ID= '';
		
		if(recordSelected!= null)
		{
		  PRODUCT_ID= recordSelected.product;	
		}
	  }
	  
	  if(WAREHOUSE_ID == null)
	  {
		WAREHOUSE_ID= '';
		
		if(recordSelected!= null)
		{
			WAREHOUSE_ID= recordSelected.warehouse;	
		}
	  }*/
	  
	  //requestProperties.params[OB.Constants.WHERE_PARAMETER] = "product.id='" +PRODUCT_ID+ "' AND warehouse.id ='"+WAREHOUSE_ID+"'";
	  requestProperties.params[OB.Constants.WHERE_PARAMETER] = "svfpaProductPriceWhV.id='" +svfpaProductPriceWhV_ID+ "'";
	  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.svfpaProductPriceWhV_ID = null;
	  /*isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.PRODUCT_ID = null;
	  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.WAREHOUSE_ID = null;*/
   },
   destroy: function ()
   {  
	  if(this.dataSource)
	  {
	    this.dataSource.destroy(); // needs to be done manually
	  }
	  this.Super('destroy', arguments);
   },
   setDataSource: function(ds)
   {
     // is called when the datasource is retrieved from the server
     this.Super('setDataSource', [ds, this.gridFields]);
     this.refreshFields();
     //this.sort('name');
     this.fetchData();
     
   },
   /*searchAgain:function()
   {
	 //inicialiar
	 console.log('searchAgain');
	 OB.Datasource.get('SVFPA_Product_Trans_V');  
	 var DS= OB.Datasource.get('SVFPA_Product_Trans_V');
	 this.setDataSource(DS);
   },*/
   initWidget: function()
   {
	    //OB.Datasource.get('SVFPINC_Incoming_Prod', this, null, true);
	   OB.Datasource.get('SVFPA_Product_Trans_V', this, null, true);
	   this.Super('initWidget', arguments);
   }
});

isc.SVFPA_FieldJustInitWidget.PrototypeWindowPopup= null;
isc.SVFPA_FieldJustInitWidget.initWidgetFunction= null;
isc.SVFPA_FieldJustInitWidget.WindowPopup= null;
isc.SVFPA_FieldJustInitWidget.LastQuantityEntered= null;
isc.SVFPA_FieldJustInitWidget.ButtonRefresh= null;
isc.SVFPA_FieldJustInitWidget.me= null;
isc.SVFPA_FieldJustInitWidget.ButtonSave= null;
isc.SVFPA_FieldJustInitWidget.ButtonSaveAndClose= null;
isc.SVFPA_FieldJustInitWidget.ButtonRefresh= null;
isc.SVFPA_FieldJustInitWidget.FunctionDataChange= null;
isc.SVFPA_FieldJustInitWidget.view= null;
isc.SVFPA_FieldJustInitWidget.SalesOrderID= null;
isc.SVFPA_FieldJustInitWidget.executedSelector= false;
isc.SVFPA_TabsIDS= new Array;

isc.SVFPA_SetID= function(entity, variable)
{
	try
	{
	  var TabSelected= OB.MainView.TabSet.getSelectedTab();
	  isc.SVFPA_TabsIDS[TabSelected.loadingTabId + '_' + variable] = entity;
	 
	}
	catch(e)
	{
	  console.log(e);	
	}
}

isc.SVFPA_GetID= function(variable)
{
    var entity= null;
    try
    {
	  var TabSelected= OB.MainView.TabSet.getSelectedTab();
	  entity= isc.SVFPA_TabsIDS[TabSelected.loadingTabId + '_' + variable];
    }
    catch(e)
    {
      console.log(e);
      entity= null;
    }
    return entity;
}

isc.SVFPA_FieldJustInitWidget.getFieldByNameColumnNew= function(Fields, V)
{
	  try
	  {
		  for(var I = 0; I< Fields.length; I++)
		  {
			 var r = Fields[I];
			 if(V != '__')
			 {	 
				 if(r.columnName == V)
				 {
				   return r;	 
				 }
			 }
			 else
			 {
				 if(r.name == V)
				 {
				   return r;	 
				 }
			 }	 
		  }
		  return null;
	  }
	  catch(e)
	  {
		console.log('Error function: getField: '+ V);   
		console.log(e.message);  
	  }
};

isc.SVFPA_FieldJustInitWidget.getButtonsSaveAndRefresh= function(Elements)
{
	try
	{
	  for(var I = 0; I< Elements.length; I++)
	  {
	 	  var r = Elements[I];
	      try
	      {
	 	 	  switch (r.buttonType)
		 	  {
			 	 case 'save':
			 		isc.SVFPA_FieldJustInitWidget.ButtonSave= r;
		 	     break;
			 	 case 'savecloseX':
			 		isc.SVFPA_FieldJustInitWidget.ButtonSaveAndClose= r;
		 	     break;
		 	     case 'refresh':
		 	    	isc.SVFPA_FieldJustInitWidget.ButtonRefresh= r;
		 	     break;
		 	  } 
	       }
	       catch(e)
	       {
	    	 console.log(e.message);
	       }
	    }
	 }
	 catch(e)
	 {
	    console.log('Error function: getFieldMProduct');   
	    console.log(e.message);  
	 }	
};

isc.SVFPA_FieldJustInitWidget.PrototypeWindowPopup= isc.OBSelectorPopupWindow.getPrototype();
isc.SVFPA_FieldJustInitWidget.PrototypeGrid= isc.OBGrid.getPrototype();
isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGrid= isc.SVFPA_FieldJustInitWidget.PrototypeGrid.initWidget;
isc.SVFPA_FieldJustInitWidget.FunctionDataChange= isc.SVFPA_FieldJustInitWidget.PrototypeGrid.dataChanged;
isc.SVFPA_FieldJustInitWidget.initWidgetFunctionSelector= isc.SVFPA_FieldJustInitWidget.PrototypeWindowPopup.initWidget;
isc.SVFPA_FieldJustInitWidget.WindowPopup= null;
isc.PRUEBA= null;

isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGridString =  isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGrid.toString();
isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGridStringNew = isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGridString.substring(isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGridString.indexOf('{'), isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGridString.length -1);
isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGridStringNew= isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGridStringNew + " this.setNewProperties(); }";

isc.OBGrid.addProperties
({	
	
	isTypeDocument: function(Type)
	{
		try
		{
		    var viewID= this.view.tabId; 
			  
			if(viewID == Type)
			{
			  return true;
			}	
			
		}
		catch(e)	
		{}
		return false;
	},
	setNewProperties:function()
	{
		if((this.isTypeDocument('187') == true) || (this.isTypeDocument('293') == true))
		{
		  isc.SVFPA_SetID(this, 'GridOrderLine');	
		}	
		if(this.isTypeDocument('187') == true)
		{	  
			  try
		      {
				 var ProductField= isc.SVFPA_FieldJustInitWidget.getFieldByNameColumnNew(this.fields, 'M_Product_ID');
		    	  
				 var NewFieldOut=
		         {
		            Name: "SVFPA_quantityAvailability",
		            formatType: "",
		            suffix: "_SVFPAQuantityEntered"
		         };
		        
		         Object.defineProperty(ProductField.outFields, 'SVFPA_quantityAvailability', {
					 value: NewFieldOut,
					 writable: true,
					 enumerable: true,
					 configurable: true
				 });
		         
		      }
		      catch(e)
		      {
		    	console.log(e.message);  
		      }
		 }     
	},
	setDataSourceChangedNew: function()
	{
		this.view.viewGrid.data.getDataSource().dataChanged= (function ()
		{
		   try
		   {
		      return function()
		      {
		       	try
		    	{
		    		isc.SVFPA_FieldJustInitWidget.me= this;
		    		isc.SVFPA_FieldJustInitWidget.getButtonsSaveAndRefresh(isc.SVFPA_FieldJustInitWidget.me.view.parentView.toolBar.leftMembers); 
		    		isc.SVFPA_FieldJustInitWidget.SalesOrderID = this.view.parentRecordId;
		    		this.Super("dataChanged", arguments);
							    	
				   	if(arguments[0].context.operationType != 'remove')
				   	{	
				    	sessionProperties = isc.SVFPA_FieldJustInitWidget.me.view.getContextInfo(true, true, false, true);
				        OB.RemoteCallManager.call('org.openbravo.client.application.window.FormInitializationComponent', sessionProperties,
				       	{
				           MODE: 'SETSESSION',
				           TAB_ID: isc.SVFPA_FieldJustInitWidget.me.view.tabId,
				           PARENT_ID: isc.SVFPA_FieldJustInitWidget.me.view.getParentId(),
				           ROW_ID: isc.SVFPA_FieldJustInitWidget.me.view.viewForm.values.id
				        },
					    function()
					    {
					       	try
					       	{
					       		if(isc.SVFPA_FieldJustInitWidget.executedSelector == true)
					       		{
					       			var SalesOrderID = isc.SVFPA_FieldJustInitWidget.SalesOrderID;
					       			OB.SVFPA.Process.beginProcess(SalesOrderID, isc.SVFPA_FieldJustInitWidget.me.view);
					       		}
					       		else
					       		{
					       			isc.SVFPA_FieldJustInitWidget.me.view.refresh();
					       		}	
	
					       	}
					      	catch(e)
					      	{
					       	  console.log(e.message);
					       	}
					      	isc.SVFPA_FieldJustInitWidget.executedSelector= false;
					       	isc.SVFPA_FieldJustInitWidget.LastQuantityEntered= null;
					    });
					}
				   	else
				   	{
				   		isc.SVFPA_FieldJustInitWidget.me.view.refresh();
				   	}	
						            
			}
			catch(e)
			{
			  console.log(e.message);
			} 
			   	        	
		 };
	  }
	  catch(e)
	  {
	  	console.log(e.message);
	  }
	 }());
	},
	getCellCSSText: function (record, rowNum, colNum)
    {
		try
		{
			var UrlSalesOrder= OB.Application.contextUrl + 'org.openbravo.service.datasource/Order';
			var UrlSalesOrderLine= OB.Application.contextUrl + 'org.openbravo.service.datasource/OrderLine';
			var UrlProductAvailable= OB.Application.contextUrl + 'org.openbravo.service.datasource/SVFPA_Product_Price_Wh_V';
		    var UrlDashBoard= OB.Application.contextUrl + 'org.openbravo.service.datasource/SVFSV_SalesOrder_Dashboard_V';
			var UrlTarget= this.dataSource.dataURL;
			var ColorGreen = 'background-color: #32CD32;';
			var ColorRed = 'background-color: #FF6347;';
			var ColorYellow = 'background-color: #CACC00;';
			
			if((UrlTarget == UrlSalesOrder) || (UrlTarget == UrlSalesOrderLine) || (UrlTarget == UrlDashBoard))
			{
				if (UrlTarget == UrlDashBoard)
				{
					if(this.getFieldName(colNum) == 'svfsvStockStatus')
					{
						if(record.svfsvStockStatus == 'OUTOFSTOCK')
						{
						  return ColorRed;
					    }
						else
						{
						   if(record.svfsvStockStatus == 'INSTOCK')
						   {
						  	 return ColorGreen;
						   }
						   else
						   {
							   if(record.svfsvStockStatus == 'PARTIAL')
							   {
							  	 return ColorYellow;
							   } 
						   }	   
					    }	
					}	 
				}
				else
				{
					var TabIDSalesOrder= '186';
				    var TabIDSalesOrderLine= '187';
				    var viewID= this.view.tabId; 
				  
					if(viewID == TabIDSalesOrderLine)
					{
						this.setDataSourceChangedNew();
						
						if(this.getFieldName(colNum) == 'svfpaStockavailability'/*'orderedQuantity'*/)
						{
							if(record.svfpaStockavailability == 'OUTOFSTOCK')
							{
							  return ColorRed;
							}
							else
							{
								if(record.svfpaStockavailability == 'INSTOCK')
								{
								  return ColorGreen;
								}	
							}	
								
						}
					}
					else
					{
					   if(viewID == TabIDSalesOrder)
					   {
						    if(this.getFieldName(colNum) == 'svfpaStockStatus'/*'orderedQuantity'*/)
							{
								if(record.svfpaStockStatus == 'OUTOFSTOCK')
								{
								  return ColorRed;
								}
								else
								{
									if(record.svfpaStockStatus == 'INSTOCK')
									{
									  return ColorGreen;
									}
									else
									{
										if(record.svfpaStockStatus == 'PARTIAL')
										{
										  return ColorYellow;
										}	
									}	
								 }	
							  } 
					      }
				      }
				  }	
			}
			else
			{
				if(UrlTarget == UrlProductAvailable)
				{
					if((this.getFieldName(colNum)== 'totalAvailable'/*'available'*/) && (record.totalAvailable > 0))
					{
					   return ColorGreen;
					}
					else
					{
					   if((this.getFieldName(colNum)== 'totalAvailable'/*'available'*/) && (record.totalAvailable <= 0))
					   {
						  return ColorRed;
					   }  
					}	
                 }   
			 }	
		}
		catch(e)
		{
		  console.log(e.message);	
		}
	},
	initWidget: new Function(isc.SVFPA_FieldJustInitWidget.initWidgetFunctionGridStringNew),
});

isc.OBSelectorPopupWindow.addProperties
(
   {
	  //"hidden": true, canHide: true} 
	  fieldsToShow: ['product$searchKey', 'product$name', 'warehouse', 'netListPrice', 'committed', 'qtyOnHand', 'totalAvailable', 'qtyOrderedPO', 'eTA', 'standardPrice', 'pONumber', 'product$discontinued', 'webAvailability', 'productPrice$priceListVersion'], 
	  listGridPOS: null, 
	  listGridASIS: null, 
	  isFieldToShow: function(V)
	  {
		try
		{
		  for(var I =0; I< this.fieldsToShow.length; I++)
		  {
			 if(this.fieldsToShow[I]==V)
			 {
			   return true;	 
			 }
		  }
		}
		catch(e)
		{
		  console.log(e.message);	
		}
		return false;
	  },
	  
	  setCantEditAllFields: function(fields, flag)
	  {
		  var FieldsNew = fields; 
		  
		  for(var I=0; I< FieldsNew.length; I++)
	      {
			  FieldsNew[I].canEdit= flag;
			  		  
			  try
			  {
			  
				  if(this.isFieldToShow(FieldsNew[I].name)== false)
				  {
					  FieldsNew[I].hidden= true;
					  FieldsNew[I].canHide= true;
				  }
			  }
			  catch(e)
			  {
				  
			  }
		  }
		  return FieldsNew;
		  
	  },
	  getParseProductName: function(str)
	  {
	    try
	    {
		    if(str.indexOf('-')!=-1)
			{
			  var Init = str.indexOf('-') + 2;
			  
			  if(Init <= str.length)
			  {
				str= str.substring(Init, str.length);  
			  }
			}
	    }
	    catch(e)
	    {
	      console.log(e.message);	
	    }
	    return str;
	  },
	  setFilterEditorCriteriaNew: function(_1)
	  {
		  
	          try
	  		  {
	  			  var Grid = isc.SVFPA_GetID('GridOrderLine');
	  			  var WindowPopup = isc.SVFPA_GetID('WindowPopup');
	  			  var Record = Grid.getSelectedRecord();
	  			  var ProductName= Record.product$_identifier;
	  			  var PriceListVersion = '';
	  			  
	  			  //ProductName = WindowPopup.getParseProductName(ProductName);
	  			  
	  			  try
	  			  {
	  				PriceListVersion= WindowPopup.defaultFilter.productPrice$priceListVersion;
	  			  }
	  			  catch(e){}
	  			  if((ProductName != null) && (ProductName != ''))
	  			  {
	  				  WindowPopup.selectorGrid.filterData
	  				  (
	  				     {
	  				    	 _constructor: 'AdvancedCriteria',
	  				    	 criteria:
	  				    	 [
	  				    		    {
	  				    		      'fieldName': 'product$searchKey',
	  				    		      'operator': 'iContains',
	  				    		      'value': ProductName
	  				    		    },
	  				    		    {
		  				    		   'fieldName': 'productPrice$priceListVersion',
		  				    		   'operator': 'iContains',
		  				    		   'value': PriceListVersion
		  				    		},
	  				    	  ],
	  				    	  operator: 'and'
	  				      }
	  				  );	  
	  			   }
	               else
	  	           {
	                   if(this.filterEditor)
	                   {
	                      this.setFilterValues(_1);
	                   }
	               }
	  		  }
	  		  catch(e)
	  		  {
	  			console.log(e.message);  
	  		  }
	  },
	  Svfpatab: null,
	  initsvfpaTab: function()
	  {  
		  try
		  {
			  
			    this.listGridPOS= isc.SVFPA_POSItems.create
			    ({
			    	PRODUCT_ID: null,
			    	WAREHOUSE_ID: null,
			    	svfpaProductPriceWhV_ID: null
			    });
			  
			    this.listGridASIS= isc.SVFPA_GridItems.create
			    ({
			    	PRODUCT_ID: null,
			    	WAREHOUSE_ID: null,
			    	svfpaProductPriceWhV_ID:null
			    });
			  
			    var newTabPO=
			    {  
			       title:"PO's",
			       pane: this.listGridPOS
			    };
			    var newTabASIS=
			    {  
			       title:"AS-IS",
			       pane: this.listGridASIS
			    };
			    
			    this.Svfpatab= SVFPA_Tab.create//isc.OBTabSetChild.create
				({
				    height: "45%",
				    width:"100%",
				    tabs:
				    [
	                  newTabPO,
	                  newTabASIS
				    ]
				});
				
				var LayoutVertical= isc.VStack.create
				({
				    height: "100%",
				    width: "100%",
				    layoutMargin: 0,
				    membersMargin: 0,
				    defaultLayoutAlign: "left",
				    members:
				    [
	                  this.items[0],
	                  this.Svfpatab
				    ] 
				 });
				var newItems= new Array();
				newItems.push(LayoutVertical);
				newItems.push(this.items[1]);
				
				this.removeItems(this.items);
				this.items= newItems;
				this.addItems(newItems);
				this.selectorGrid.setHeight("45%");
		  }
		  catch(e)
		  {
			console.log(e.message);  
		  }
	  },	
	 
	  setNewTabDetails: function()
	  {
		 this.Svfpatab.tabs.push
		 ({
			   title:"Item",
			   pane: isc.DynamicForm.create
			   ({
			        ID: "form0",
			        fields:
			        [
			           {name: "itemName", type:"text", title:"Item"},
				       {name: "description", type:"textArea", title:"Description"},
				       {name:"price", type:"float", title: "Price", defaultValue: "low"} 
				    ]
			   })
		  });
	  },
	  
	  setNewFieldInSelector: function()
	  {
		    this.selectorGrid.addProperties
		    (
		       {
		      	 canEdit: true,
		      	 editEvent: isc.EH.CLICK,
		         autoSaveEdits:true,
		         popup: this
		       }
		    );
		    isc.SVFPA_SetID(this, 'WindowPopup');
		    this.selectorGrid.setFilterEditorCriteria = this.setFilterEditorCriteriaNew;
		    this.initsvfpaTab();
		    		    
		    isc.SVFPA_FieldJustInitWidget.WindowPopup.selectorGrid.selectionUpdated= function(record, recordList)
	        {
		    	try
		    	{
		    	  if(record!= null)
		    	  {
		    		  /*isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.PRODUCT_ID = record.product$id;
		    		  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.WAREHOUSE_ID = record.warehouse;
		    		  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.PRODUCT_ID = record.product$id;
			    	  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.WAREHOUSE_ID = record.warehouse;*/
		    		  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.svfpaProductPriceWhV_ID = record.id;
		    		  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.svfpaProductPriceWhV_ID = record.id;
			    	  
			    	  //--------------------------------------------------------------------------------------
			    	  /*isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.searchAgain();
			    	  isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridASIS.searchAgain();*/
			    	  this.popup.listGridPOS.initWidget();
			    	  this.popup.listGridASIS.initWidget();
			    	  
			    	  //this.popup.listGridPOS.searchAgain();
			    	  //this.popup.listGridASIS.searchAgain();
			      }
		    	}
		    	catch(e)
		    	{
		    	  console.log(e.message);	
		    	}
	        };
	        
	        var FieldsdNewQuantity = null;
	       
	        try
	        {
	        	FieldsdNewQuantity= isc.SVFPA_FieldJustInitWidget.getFieldByNameColumnNew(this.selectorGrid.fields, 'SVFPA_quantityAvailability');
	        }
	        catch(e)
	        {
	        	FieldsdNewQuantity= null;
	        }
	        
	        /*this.selectorGrid.addProperties
		    (
		       {
		      	 canEdit: true,
		      	 editEvent: isc.EH.CLICK,
		         autoSaveEdits:true
		       }
		    );*/
	        
	        var FieldsdNew = this.setCantEditAllFields(this.selectorGrid.fields, false);
	        
	        if(FieldsdNewQuantity == null)
		    {	   
			    var NewFieldQuantity= 
			    {
				   filterOnKeypress: false, 
				   name: 'SVFPA_quantityAvailability',
				   title: 'Quantity Needed',
				   type: '_id_29',
				   canEdit: true,
				   width: '100px',
				   change: function(a, item, value)
				   {
					   try
					   {
						  var V= value.toString();
						  V = V.replace(/\,/g, '');
						  V = V.replace(/\./g, '');
						  
						  if ((V!= null) && (V!= '') && (V.length > 0))
						  {
							 var re=/^\d+(\.\d+)?$/;
							
							 if((V.length > 11) || (re.test(V)==false))
							 {
							   return false;  
							 }
						  } 
					   	  //---------------------------
					      
					      item.grid.setEditValue(item.rowNum, 'SVFPA_quantityAvailability', value);
					      isc.SVFPA_FieldJustInitWidget.LastQuantityEntered= value;
					      isc.SVFPA_FieldJustInitWidget.executedSelector= true;
					      isc.SVFPA_FieldJustInitWidget.WindowPopup = isc.SVFPA_GetID('WindowPopup');
					      //isc.SVFPA_FieldJustInitWidget.WindowPopup.listGridPOS.resetAllCellOnQtyNeeded();
					      item.grid.data.localData[item.rowNum]['SVFPA_quantityAvailability'] = value;
					   }
					   catch(e)
					   {
						  console.log(e.message); 
					   }
				      
				   }
			    }
			    FieldsdNew.push(NewFieldQuantity);
			 }
	         this.selectorGrid.fields= FieldsdNew;
	        
	  },
	  
	  initWidget: (function () {
		 try
	     {
	        return function()
	        {
	        	isc.SVFPA_FieldJustInitWidget.initWidgetFunctionSelector.apply(this, arguments);
	            
	            try
	            {
	               if(this.selectorGrid.dataSource.dataURL==OB.Application.contextUrl + 'org.openbravo.service.datasource/SVFPA_Product_Price_Wh_V')
	               {
	                 isc.SVFPA_FieldJustInitWidget.WindowPopup= this;
	                 this.setNewFieldInSelector();
	               }   
	               
	            }
	            catch(e)
	            {
	            	console.log(e.message);
	            }
	         };
	      }
          catch(e)
          {
        	  console.log(e.message);
          }
    }())
  }
);