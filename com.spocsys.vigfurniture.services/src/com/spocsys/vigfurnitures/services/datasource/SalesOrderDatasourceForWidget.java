package com.spocsys.vigfurnitures.services.datasource;

import static com.spocsys.vigfurnitures.services.utils.QueryParameter.with;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.ProductSubstitute;
import org.openbravo.service.datasource.ReadOnlyDataSourceService;
import org.openbravo.service.json.DataToJsonConverter;
import org.openbravo.service.json.JsonConstants;
import org.openbravo.service.json.JsonUtils;

import com.spocsys.vigfurnitures.services.utils.FinderGeneric;
import com.spocsys.vigfurnitures.services.utils.ResultMapCriteriaUtils;

public class SalesOrderDatasourceForWidget extends ReadOnlyDataSourceService {
	private String distinct;
	int count = 0;
	@Override
	protected int getCount(Map<String, String> parameters) {
		// TODO Auto-generated method stub
		return count;
	}


	@Override
	public String fetch(Map<String, String> parameters) {
		int startRow = 0;

		final List<JSONObject> jsonObjects = fetchJSONObject(parameters);

		final JSONObject jsonResult = new JSONObject();
		final JSONObject jsonResponse = new JSONObject();
		try {
			jsonResponse.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
			jsonResponse.put(JsonConstants.RESPONSE_STARTROW, startRow);
			jsonResponse.put(JsonConstants.RESPONSE_ENDROW, jsonObjects.size() - 1);
			jsonResponse.put(JsonConstants.RESPONSE_TOTALROWS, jsonObjects.size());
			jsonResponse.put(JsonConstants.RESPONSE_DATA, new JSONArray(jsonObjects));
			jsonResult.put(JsonConstants.RESPONSE_RESPONSE, jsonResponse);
		} catch (JSONException e) {
		}

		return jsonResult.toString();
	}



	private List<JSONObject> fetchJSONObject(Map<String, String> parameters) {
		final int startRow = Integer.parseInt(parameters.get(JsonConstants.STARTROW_PARAMETER));
		final int endRow = Integer.parseInt(parameters.get(JsonConstants.ENDROW_PARAMETER));
		final List<Map<String, Object>> data = getData(parameters, startRow, endRow);
		final DataToJsonConverter toJsonConverter = OBProvider.getInstance().get(
				DataToJsonConverter.class);
		toJsonConverter.setAdditionalProperties(JsonUtils.getAdditionalProperties(parameters));
		return toJsonConverter.convertToJsonObjects(data);
	}


	@Override
	protected List<Map<String, Object>> getData(Map<String, String> parameters,
			int startRow, int endRow) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		int rowNumbers=0;


		boolean showAll= false;
		if(parameters!=null&&!parameters.isEmpty()&&parameters.get("rowsNumber")!=null)
			rowNumbers= Integer.parseInt(parameters.get("rowsNumber"));

		if(parameters!=null&&!parameters.isEmpty()&&parameters.get("showAll")!=null)
			showAll= Boolean.parseBoolean(parameters.get("showAll"));

		if(showAll)
			rowNumbers=0;
		List<String> includedIds = new ArrayList<String>();
		distinct = parameters.get(JsonConstants.DISTINCT_PARAMETER);
		try{
			List<Order>	 list = FinderGeneric.find(Order.class,  with(Order.PROPERTY_SALESTRANSACTION, true).parameters(), false, false,rowNumbers,org.hibernate.criterion.Order.desc(Order.PROPERTY_ORDERDATE));
			
			if(list != null)
				for (Order order : list) {

					if(order.getOrderLineList()!=null && !order.getOrderLineList().isEmpty()){
						for (OrderLine line : order.getOrderLineList()) {


							//Generar linea a linea los productos
							Map<String, Object> row = new LinkedHashMap<String, Object>();
							row.put("id", order.getId());
							if(line.getProduct()!=null)
								row.put("productid",line.getProduct().getId()==null?"":line.getProduct().getId());
							row.put("organization", order.getOrganization());
							row.put("customerorder", 	order.getOrderReference()==null?"":order.getOrderReference());
							row.put("sonumber", 	order.getDocumentNo()==null?"":order.getDocumentNo());
							row.put("customername", 	order.getBusinessPartner());

							if(line.getProduct()!=null){
								row.put("item", line.getProduct().getSearchKey()==null?"":line.getProduct().getSearchKey());
								row.put("productname", line.getProduct());
							}
							String substituteitem="";
							if(line.getProduct()!=null && line.getProduct().getProductSubstituteList()!=null)

								for (ProductSubstitute psub : line.getProduct().getProductSubstituteList()) {
									substituteitem+=psub.getName()+", ";
								}


							row.put("substituteitem",substituteitem ==null?"":substituteitem);//tabla M_substitute

							if(order.getSvfpaStockLocation()!=null && order.getSvfpaStockLocation().getLocationAddress()!=null)
								row.put("currentlocation", order.getSvfpaStockLocation().getLocationAddress().getAddressLine1()==null?"":order.getSvfpaStockLocation().getLocationAddress().getAddressLine1());
							if(order.getSvfpaStockLocation()!=null && order.getSvfpaStockLocation().getLocationAddress()!=null)
								row.put("stocklocation", order.getSvfpaStockLocation().getLocationAddress().getAddressLine1()==null?"":order.getSvfpaStockLocation().getLocationAddress().getAddressLine1());

							if( order.getSvfpaShipFrom()!=null && order.getSvfpaShipFrom().getLocationAddress()!=null)
								row.put("shipfromlocation", order.getSvfpaShipFrom().getLocationAddress().getAddressLine1()==null?"":order.getSvfpaShipFrom().getLocationAddress().getAddressLine1());//lo toma del Sales Order es campo nuevo

							row.put("status", order.getSvfpaStatus()==null?"":order.getSvfpaStatus());
							row.put("svfsvStockStatus",order.getSvfpaStockStatus()==null?"OUTOFSTOCK":order.getSvfpaStockStatus());

							row.put("processed",order.isSvfpaProcessed()==null?false:order.isSvfpaProcessed());
							row.put("action",order.getSVFPAAction() ==null?"":order.getSVFPAAction());//nuevo campo de Sales order

							List<Invoice>	 listinvoice = FinderGeneric.find(Invoice.class, with(Invoice.PROPERTY_SALESTRANSACTION, false).and(Invoice.PROPERTY_SALESORDER, order).parameters(), false, false);
							if(listinvoice!=null&&!listinvoice.isEmpty()){
								row.put("ponumber", listinvoice.get(0).getDocumentNo()==null?"":listinvoice.get(0).getDocumentNo());// si esta asiciado se toma el No. de PI de la orden
								row.put("invoiceID", listinvoice.get(0).getId()==null?"": listinvoice.get(0).getId());
							}
							row.put("transfernumber","not yet");//Nuevo campo de Sales Order
							row.put("pickticketnumber", "not yet");//si hay un Pick ticket se toma la linea, si hay N numero de entradas se deben mostrar N nuemero de lineas
							row.put("eta", order.getSvfpaEta()==null?"":order.getSvfpaEta());//link
							row.put("specialorder", order.isSvfpaSpecialOrder()==null?false:order.isSvfpaSpecialOrder());//si tienes una orden, con 1 solo producto se debe maracar todos como speciales y es de Sales Order en cabecera


							if (StringUtils.isNotEmpty(parameters.get("criteria"))&&! parameters.get("criteria").contains("_dummy")) {


								boolean addValue = new ResultMapCriteriaUtils(row, parameters).applyFilter();
								if (!addValue) {
									continue;
								}
							}

							if (StringUtils.isNotEmpty(distinct)) {
								BaseOBObject obj = (BaseOBObject) row.get(distinct);
								if (includedIds.contains((String) obj.getId())) {
									continue;
								} else {
									includedIds.add((String) obj.getId());
								}
								Map<String, Object> distinctMap = new HashMap<String, Object>();
								distinctMap.put("id", obj.getId());
								distinctMap.put("_identifier", obj.getIdentifier());
								result.add(distinctMap);

							} else {
								result.add(row);
							}

						}

					}
					else{
						Map<String, Object> row = new LinkedHashMap<String, Object>();
						row.put("id", order.getId()==null?"":order.getId());

						row.put("organization", order.getOrganization().getName()==null?"":order.getOrganization().getName());
						row.put("customerorder", 	order.getOrderReference()==null?"":order.getOrderReference());
						row.put("sonumber", 	order.getDocumentNo()==null?"":order.getDocumentNo());
						row.put("customername", 	order.getBusinessPartner().getName()==null?"":order.getBusinessPartner().getName());

						if(order.getSvfpaStockLocation()!=null && order.getSvfpaStockLocation().getLocationAddress()!=null)
							row.put("currentlocation", order.getSvfpaStockLocation().getLocationAddress().getAddressLine1()==null?"":order.getSvfpaStockLocation().getLocationAddress().getAddressLine1());
						if(order.getSvfpaStockLocation()!=null && order.getSvfpaStockLocation().getLocationAddress()!=null)
							row.put("stocklocation", order.getSvfpaStockLocation().getLocationAddress().getAddressLine1()==null?"": order.getSvfpaStockLocation().getLocationAddress().getAddressLine1());
						if( order.getSvfpaShipFrom()!=null && order.getSvfpaShipFrom().getLocationAddress()!=null)
							row.put("shipfromlocation", order.getSvfpaShipFrom().getLocationAddress().getAddressLine1()==null?"": order.getSvfpaShipFrom().getLocationAddress().getAddressLine1());//lo toma del Sales Order es campo nuevo

						row.put("status", order.getSvfpaStatus()==null?"":order.getSvfpaStatus());
						row.put("svfsvStockStatus",order.getSvfpaStockStatus()==null?"OUTOFSTOCK":order.getSvfpaStockStatus());//se trae de Sales Order

						row.put("processed",order.isSvfpaProcessed()==null?false:order.isSvfpaProcessed());
						row.put("action",order.getSVFPAAction()==null?"":order.getSVFPAAction() );//nuevo campo de Sales order
						List<Invoice>	 listinvoice = FinderGeneric.find(Invoice.class, with(Invoice.PROPERTY_SALESTRANSACTION, false).and(Invoice.PROPERTY_SALESORDER, order).parameters(), false, false,rowNumbers);
						if(listinvoice!=null&&!listinvoice.isEmpty()){
							row.put("ponumber", listinvoice.get(0).getDocumentNo()==null?"":listinvoice.get(0).getDocumentNo());// si esta asiciado se toma el No. de PI de la orden
							row.put("invoiceID", listinvoice.get(0).getId()==null?"":listinvoice.get(0).getId());
						}
						row.put("transfernumber","not yet");//Nuevo campo de Sales Order
						row.put("pickticketnumber", "not yet");//si hay un Pick ticket se toma la linea, si hay N numero de entradas se deben mostrar N nuemero de lineas
						row.put("eta", order.getSvfpaEta()==null?"":order.getSvfpaEta());//link
						row.put("specialorder", order.isSvfpaSpecialOrder()==null?false:order.isSvfpaSpecialOrder());



						if (StringUtils.isNotEmpty(parameters.get("criteria"))&&! parameters.get("criteria").contains("_dummy")) {


							boolean addValue = new ResultMapCriteriaUtils(row, parameters).applyFilter();
							if (!addValue) {
								continue;
							}
						}

						if (StringUtils.isNotEmpty(distinct)) {
							BaseOBObject obj = (BaseOBObject) row.get(distinct);
							if (includedIds.contains((String) obj.getId())) {
								continue;
							} else {
								includedIds.add((String) obj.getId());
							}
							Map<String, Object> distinctMap = new HashMap<String, Object>();
							distinctMap.put("id", obj.getId());
							distinctMap.put("_identifier", obj.getIdentifier());
							result.add(distinctMap);

						} else {
							result.add(row);
						}
					}

				}	
			//Sorting 




			//fillfilters(result, parameters);
			//Sorting 

			if (result.isEmpty() || StringUtils.isNotEmpty(distinct)) {
				return result;
			}
			String sortBy = parameters.get(JsonConstants.SORTBY_PARAMETER);
			if (StringUtils.isEmpty(sortBy)) {
				sortBy = "documentNumber";
			}
			if (sortBy.endsWith("$_identifier")) {
				sortBy = sortBy.substring(0, sortBy.length() - 12);
			}
			Collections.sort(result, new ResultComparator(sortBy));

			OBContext.restorePreviousMode();
			count= result.size();
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return result;
	}



	private List<Map<String, Object>>   fillfilters(List<Map<String, Object>> result,Map<String, String> parameters ) {


		Iterator<Map<String, Object> > iterator = result.iterator();
		try {
			if(parameters!=null&&!parameters.isEmpty()&&parameters.get("criteria")!=null&&!parameters.get("criteria").isEmpty())

				if(result!=null&&!result.isEmpty()){

					String	criteriaJson=parameters.get("criteria");

					String[] criterias = criteriaJson.split("__;__");
					while ( iterator.hasNext()) {

						Map<String, Object> map = (Map<String, Object>) iterator.next();

						boolean eq =true;
						for (int i = 0; i < criterias.length&& eq; i++) {
							String string = criterias[i];
							JSONObject  org= new JSONObject(string);
							String fieldName= (String) org.get("fieldName");

							Object value;
							if(org.get("value") !=null&&org.get("value") instanceof Boolean)
								value=(Boolean) org.get("value");
							else
								value=(String) org.get("value");

							String operator= (String) org.get("operator");
							Object va;
							if( map.get(fieldName)!=null&&map.get(fieldName) instanceof Boolean)
								va=(Boolean) map.get(fieldName);
							else
								va= (String) map.get(fieldName);

							if((va!=null &&org.get("value") instanceof String)){
								va=((String)va).toLowerCase();
								value=((String)value).toLowerCase();
								if("iContains".equals(operator) && (org.get("value") instanceof String)){
									if(!((String) va).contains((CharSequence) value))
										eq=false;
								}
								if("iNotEqual".equals(operator)&& (org.get("value") instanceof String)){
									if(((String) va).equalsIgnoreCase((String) value))
										eq=false;
								}if("iEquals".equals(operator)&& (org.get("value") instanceof String)){
									if(!((String) va).equalsIgnoreCase((String) value))
										eq=false;
								}


							}else

								if((org.get("value") instanceof Boolean)){
									if(va!=org.get("value"))
										eq=false;
								}

							if(va==null ||((String) va).isEmpty() ){
								eq=false;
							}
							if(eq==false)
								iterator.remove();
						}



					}
				}



		} catch (JSONException e) {

			System.out.println(e.getMessage());

		}
		return  result;
	}
	private static class ResultComparator implements Comparator<Map<String, Object>> {
		private String compareBy = "convertedFreightAmount";
		boolean desc = false;

		public ResultComparator(String _compareBy) {
			compareBy = _compareBy;
			if (compareBy.startsWith("-")) {
				desc = true;
				compareBy = compareBy.substring(1);
			}
		}

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			Object obj1 = o1.get(compareBy);
			Object obj2 = o2.get(compareBy);
			if (obj2 == null) {
				return -1;
			} else if (obj1 == null) {
				return 1;
			}
			if (obj1 instanceof BigDecimal) {
				final BigDecimal v1 = (BigDecimal) o1.get(compareBy);
				final BigDecimal v2 = (BigDecimal) o2.get(compareBy);
				return desc ? v2.compareTo(v1) : v1.compareTo(v2);
			} else if (obj1 instanceof String) {
				final String v1 = (String) o1.get(compareBy);
				final String v2 = (String) o2.get(compareBy);
				return desc ? v2.compareTo(v1) : v1.compareTo(v2);
			} else if (obj1 instanceof Date) {
				final Date v1 = (Date) o1.get(compareBy);
				final Date v2 = (Date) o2.get(compareBy);
				return desc ? v2.compareTo(v1) : v1.compareTo(v2);
			} else if (obj1 instanceof BaseOBObject) {
				final String v1 = ((BaseOBObject) o1.get(compareBy)).getIdentifier();
				final String v2 = ((BaseOBObject) o2.get(compareBy)).getIdentifier();
				return desc ? v2.compareTo(v1) : v1.compareTo(v2);
			} else if (obj1 instanceof Boolean) {
				final boolean v1 = (Boolean) o1.get(compareBy);
				final boolean v2 = (Boolean) o2.get(compareBy);
				if (v1 == v2) {
					return 0;
				}
				if (v1) {
					return desc ? -1 : 1;
				} else {
					return desc ? 1 : -1;
				}
			} else {
				// unable to compare
				return 0;
			}
		}
	}
}
