package com.spocsys.vigfurnitures.services.events;

import static com.spocsys.vigfurnitures.services.utils.QueryParameter.with;

import java.util.Iterator;
import java.util.List;

import javax.enterprise.event.Observes;

import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;

import com.spocsys.vigfurnitures.services.utils.FinderGeneric;

public class POReferenceFieldEventHandler extends EntityPersistenceEventObserver{


	private static Entity[] entities = { ModelProvider.getInstance().getEntity(ShipmentInOut.ENTITY_NAME) ,
		ModelProvider.getInstance().getEntity(Order.ENTITY_NAME),	 ModelProvider.getInstance().getEntity(Invoice.ENTITY_NAME),
		ModelProvider.getInstance().getEntity(FIN_Payment.ENTITY_NAME)
	};
	@Override
	protected Entity[] getObservedEntities() {
		// TODO Auto-generated method stub
		return entities;
	}


	public void onUpdate(@Observes
			EntityUpdateEvent event) {
		if (!isValidEvent(event)) {
			return;
		}
		updatePoReference(event);
	}



	public void onSave(@Observes
			EntityNewEvent event) {
		if (!isValidEvent(event)) {
			return;
		}
		updatePoReference(event);
	}
	
	  protected void updatePoReference( final EntityPersistenceEvent event){
		  
			final BaseOBObject object = (BaseOBObject) event.getTargetInstance();
			if(object instanceof Order){
			
				checksField(((Order) object).getOrderReference(), ((Order) object).getOrderLineList());
			}else
				if(object instanceof ShipmentInOut){
					checksField(((ShipmentInOut) object).getOrderReference(), ((ShipmentInOut) object).getMaterialMgmtShipmentInOutLineList());
				}else
					if(object instanceof Invoice){
						checksField(((Invoice) object).getOrderReference(), ((Invoice) object).getInvoiceLineList());
					}else
						if(object instanceof FIN_Payment){
							List<FIN_PaymentScheduleDetail> details =  findPaymentDetails((FIN_Payment)object);
							if(details!=null && !details.isEmpty()){
							
							checksField(((FIN_Payment) object).getSvfsvPoreference(),details);
							for (FIN_PaymentScheduleDetail fin_PaymentScheduleDetail : details) {
								OBDal.getInstance().save(fin_PaymentScheduleDetail);
							}
							
					}}
			
			
	       
	  }
	  protected void checksField(String PoReferenceParent, List list){
		  
		   for (Object  baseOBObject : list) {
			  if(baseOBObject instanceof BaseOBObject){
			  String svfsvPoreference=  (String) ((BaseOBObject) baseOBObject).get("svfsvPoreference");
			   if(svfsvPoreference==null || svfsvPoreference.isEmpty()){
				   
				   ((BaseOBObject) baseOBObject).set("svfsvPoreference", PoReferenceParent);
			   }
		  }}
		  
			
			
	       
	  }
	  private boolean checkSameStatus(final Entity entity  ,String properties, final EntityUpdateEvent event) {
	
	    final String previousStatus = (String) event.getPreviousState(entity
	            .getProperty(properties));
	        final String currentStatus = (String) event.getCurrentState(entity
	            .getProperty(properties));
	        
	        if(previousStatus.equalsIgnoreCase(currentStatus))
	        	return true;
	        else
	        	return false;
	  }
	  
	  private List<FIN_PaymentScheduleDetail>  findPaymentDetails(FIN_Payment fin_Payment) {
	  Iterator<FIN_PaymentDetail> iterator = fin_Payment.getFINPaymentDetailList().iterator();
		
		 while (iterator.hasNext()) {
			FIN_PaymentDetail fin_PaymentDetail = (FIN_PaymentDetail) iterator
					.next();
			List<FIN_PaymentScheduleDetail> details = FinderGeneric.find(FIN_PaymentScheduleDetail.class, with(FIN_PaymentScheduleDetail.PROPERTY_PAYMENTDETAILS, fin_PaymentDetail).parameters(), false, false);
	            if( details!=null &&!details.isEmpty() )
			           return details;
		 }
		return null;
			
		}



	
}
		
		
		
