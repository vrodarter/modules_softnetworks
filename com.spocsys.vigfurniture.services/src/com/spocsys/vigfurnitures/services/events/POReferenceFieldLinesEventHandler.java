package com.spocsys.vigfurnitures.services.events;

import java.math.BigDecimal;

import javax.enterprise.event.Observes;

import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

public class POReferenceFieldLinesEventHandler extends EntityPersistenceEventObserver{

	private static Entity[] entities = { ModelProvider.getInstance().getEntity(OrderLine.ENTITY_NAME) ,
		ModelProvider.getInstance().getEntity(ShipmentInOutLine.ENTITY_NAME),	 ModelProvider.getInstance().getEntity(InvoiceLine.ENTITY_NAME),
		ModelProvider.getInstance().getEntity(FIN_PaymentScheduleDetail.ENTITY_NAME)};
	
	@Override
	protected Entity[] getObservedEntities() {
		return entities;
	}
	
	
	public void onUpdate(@Observes
			EntityUpdateEvent event) {
		if (!isValidEvent(event)) {
			return;
		}
		updatePoReference(event);
	}



	public void onSave(@Observes
			EntityNewEvent event) {
		if (!isValidEvent(event)) {
			return;
		}
		updatePoReference(event);
	}

	

	  protected void updatePoReference( final EntityPersistenceEvent event){
		  
			final BaseOBObject object = (BaseOBObject) event.getTargetInstance();
			if(object instanceof OrderLine){
				 OrderLine line = (OrderLine) object;
				Order parent = line.getSalesOrder();
			   
				if( parent.getOrderReference()!=null&&!parent.getOrderReference().isEmpty() ){
					
					checksField(parent.getOrderReference(), line,event);
					
				}
			}else
				if(object instanceof ShipmentInOutLine){
					ShipmentInOutLine inOutLine =(ShipmentInOutLine) object ;
					ShipmentInOut parent =  inOutLine.getShipmentReceipt();
					
					
					if( parent.getOrderReference()!=null&&!parent.getOrderReference().isEmpty() ){
						
					checksField(parent.getOrderReference(), inOutLine,event);
					}
				}else
					if(object instanceof InvoiceLine){
						Invoice parent =  ((InvoiceLine) object).getInvoice();
						if( parent.getOrderReference()!=null&&!parent.getOrderReference().isEmpty() ){
						checksField(parent.getOrderReference(), object,event);
						
						}
					}else
						if(object instanceof FIN_PaymentScheduleDetail){
							if(((FIN_PaymentScheduleDetail) object).getPaymentDetails()!=null){
							FIN_Payment parent =  ((FIN_PaymentScheduleDetail) object).getPaymentDetails().getFinPayment();
							parent = OBDal.getInstance().get(FIN_Payment.class, parent.getId());
							
							if( parent.getSvfsvPoreference()!=null&&!parent.getSvfsvPoreference().isEmpty() ){
							
							checksField( parent.getSvfsvPoreference(),object,event);
							}
							}
					}
			
			
	       
	  }
	  
	  
	  protected void checksField(String PoReferenceParent,Object  baseOBObject,final EntityPersistenceEvent event){
		  
		  
			  if(baseOBObject instanceof BaseOBObject){
			  String svfsvPoreference=  (String) ((BaseOBObject) baseOBObject).get("svfsvPoreference");
			   if(svfsvPoreference==null || svfsvPoreference.isEmpty()){
				   final Entity orderLineEntity = ModelProvider.getInstance().getEntity(((BaseOBObject)baseOBObject).getEntityName());
				      final Property orderLinePOReferenceProperty = orderLineEntity.getProperty("svfsvPoreference");
					event.setCurrentState(orderLinePOReferenceProperty,PoReferenceParent);
				   
				   
			   }
		  
			  }
	  
			 
	  }
	  
	
		  
}
