package com.spocsys.vigfurnitures.services.events;

import java.math.BigDecimal;

import javax.enterprise.event.Observes;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.domain.List;
import org.openbravo.model.ad.domain.Reference;
import org.openbravo.model.common.plm.Product;

public class ProductNameEventHandler extends EntityPersistenceEventObserver {
	private static Entity[] entities = { ModelProvider.getInstance().getEntity(Product.ENTITY_NAME) };
	
	@Override
	protected Entity[] getObservedEntities() {
		return entities;
	}

	
	public void onUpdate(@Observes
			EntityUpdateEvent event) {
		if (!isValidEvent(event)) {
			return;
		}
		updateProductName(event);
	}



	public void onSave(@Observes
			EntityNewEvent event) {
		if (!isValidEvent(event)) {
			return;
		}
		updateProductName(event);
	}
	
	  protected void updateProductName( final EntityPersistenceEvent event){
		  
		  final Product product = (Product) event.getTargetInstance();
		  final Entity productEntity = ModelProvider.getInstance().getEntity(product.getEntityName());
	      final Property name = productEntity.getProperty(Product.PROPERTY_NAME);
	      
	     String currentName= (String)event.getCurrentState(name);
	     if(StringUtils.isEmpty(currentName)){
	      //(If HK flag set HK as the prefix should be HK y Soft Kit the prefix is SK if special Order then prefix is SP )
	         
	     
	      StringBuffer fullName=new StringBuffer();
	      if(StringUtils.isNotEmpty(product.getSvfsvProductCharact()))
	      fullName.append(product.getSvfsvProductCharact());
	     
	     // (Box number/Total number of boxes) 
        BigDecimal boxValue=    product.getSvfsvBoxnumber();
        BigDecimal totalBoxValue=    product.getSvfsvTotalnumberofbox();
        BigDecimal ONE = new BigDecimal(1);
        if(boxValue!=null && totalBoxValue!=null)
        	if(boxValue.compareTo(ONE)!=0 ||totalBoxValue.compareTo(ONE)!=0  )
       	    	 fullName.append((StringUtils.isEmpty(fullName.toString())?"":"-")+boxValue.toString()+"/" +totalBoxValue.toString());
	    
	     
	     if(!StringUtils.isEmpty(product.getSvfsvModel()) )
	    	 fullName.append((StringUtils.isEmpty(fullName.toString())?"":"-")+product.getSvfsvModel());
	     if(!StringUtils.isEmpty(product.getSvfsvModelnumber()) )
	    	 fullName.append((StringUtils.isEmpty(fullName.toString())?"":"-")+product.getSvfsvModelnumber());
	     
	     if(!StringUtils.isEmpty(product.getSvfsvItemtype()) )
	    	 fullName.append((StringUtils.isEmpty(fullName.toString())?"":" ;")+product.getSvfsvItemtype());
	     if(!StringUtils.isEmpty(product.getSvfsvItemspecification()) )
	    	 fullName.append((StringUtils.isEmpty(fullName.toString())?"":" ;")+product.getSvfsvItemspecification());
	     
	    if(product.getProductCategory()!=null)
	    	if(!StringUtils.isEmpty(product.getProductCategory().getName()) ){
	    		String category = product.getProductCategory().getName();
	    		int slashpos = category.indexOf("/");
	    		if(slashpos!=-1 ){
	    			category =category.substring(slashpos+1);
	    			if(StringUtils.isNotEmpty(category))
	    			 fullName.append((StringUtils.isEmpty(fullName.toString())?"":" ;")+category);
	    		}
	    	}
	    	
	     if(!StringUtils.isEmpty(product.getSvfsvColor()) ){
	    	 OBCriteria<List> criteria = OBDal.getInstance().createCriteria(List.class);
	    	 criteria.add(Restrictions.eq(List.PROPERTY_SEARCHKEY,product.getSvfsvColor()));
	    	 criteria.add(Restrictions.eq(List.PROPERTY_REFERENCE,OBDal.getInstance().get(Reference.class, "8346AE1A87BB44AF9F941D488A458D66")));
	    	 if (criteria.list()!=null && !criteria.list().isEmpty()) {
	    		 fullName.append((StringUtils.isEmpty(fullName.toString())?"":" ;")+criteria.list().get(0).getName());
			}
	    	
	     }
	     if(!StringUtils.isEmpty(product.getSvfsvFinishMaterial()) ){
	    	 OBCriteria<List> criteria = OBDal.getInstance().createCriteria(List.class);
	    	 criteria.add(Restrictions.eq(List.PROPERTY_SEARCHKEY,product.getSvfsvFinishMaterial()) );
	    	 criteria.add(Restrictions.eq(List.PROPERTY_REFERENCE,OBDal.getInstance().get(Reference.class, "43218167922F4BAEAAD45461EB3BE988")));
	    	 if (criteria.list()!=null && !criteria.list().isEmpty()) {
	    		 fullName.append((StringUtils.isEmpty(fullName.toString())?"":" ;")+criteria.list().get(0).getName());
			}}
	    
	    
	     
	     
	    	 if(!StringUtils.isEmpty(product.getSearchKey()))
	    	 fullName.append(product.getSearchKey());
	      
		event.setCurrentState(name,StringUtils.isEmpty(fullName.toString())?"":fullName.toString());
	     }
	  }
}
