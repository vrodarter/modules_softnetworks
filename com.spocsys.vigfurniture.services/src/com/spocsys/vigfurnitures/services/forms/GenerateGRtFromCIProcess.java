package com.spocsys.vigfurnitures.services.forms;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.service.db.DbUtility;

import com.spocsys.vigfurnitures.services.utils.MaterialShipmentInOutService;

public class GenerateGRtFromCIProcess extends BaseActionHandler  {


	private Connection connection ;

	private DalConnectionProvider provider = new DalConnectionProvider();
	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {

		JSONObject jsonRequest = null;
	
		try {
			jsonRequest = new JSONObject(content);
			ShipmentInOut id=null;
			final JSONArray ciHIds = jsonRequest.getJSONArray("ciHeaders");
			for (int i = 0; i < ciHIds.length(); i++) {
				id=  generateGRProcess(ciHIds.getString(i));
			}
			JSONObject errorMessage = new JSONObject();
			errorMessage.put("severity", "TYPE_SUCCESS");
			errorMessage.put("title", OBMessageUtils.messageBD("Success"));
			errorMessage.put("text",  "Process finished successfully. Shipment No: "+ id.getDocumentNo()+" created");
			JSONArray responseActions = new JSONArray();

			JSONObject action2 = new JSONObject();
			JSONObject action2Params = new JSONObject();
			action2Params.put("tabId", "257");
			action2Params.put("recordId", id.getId());
			action2Params.put("wait", "true");
			action2.put("openDirectTab", action2Params);

			responseActions.put(action2);

			jsonRequest.put("responseActions", responseActions);

			jsonRequest.put("message", errorMessage);
			
			OBDal.getInstance().commitAndClose();
		}catch (Exception e) {


			OBDal.getInstance().rollbackAndClose();

			try {
				jsonRequest = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "TYPE_ERROR");
				errorMessage.put("text", message);
				jsonRequest.put("message", errorMessage);
			} catch (Exception e2) {

			}
		}
		return jsonRequest;
	}

	private ShipmentInOut  generateGRProcess(String commercial_invoice_id)   {
		connection =OBDal.getInstance().getConnection();
		provider.setConnection(connection); 

		ShipmentInOut invoice =null;
		try{
		Order ci=OBDal.getInstance().get(Order.class, commercial_invoice_id);
		ci=	(Order) OBDal.getInstance().getProxy(Order.ENTITY_NAME, commercial_invoice_id);
		ci.setSvfsvGenerategr(true);
		List<OrderLine>  lines = ci.getOrderLineList();
		 invoice = MaterialShipmentInOutService.createShipment(ci.getOrganization().getId(), ci.getClient().getId(), ci.getBusinessPartner().getId(),commercial_invoice_id);
		
		OBContext.setAdminMode();

		
		OBDal.getInstance().save(invoice);
		OBDal.getInstance().flush();
		OBDal.getInstance().refresh(invoice);
	
		OBContext.restorePreviousMode();

		MaterialShipmentInOutService.createShipmentLines(provider, invoice.getId(), lines);

		MaterialShipmentInOutService.updateSvfsvInoutCiLine(invoice, lines);
		
		invoice.setOrderReference(ci.getOrderReference());
		OBDal.getInstance().save(ci);
		OBDal.getInstance().flush();
		}catch (Exception e){
		 if(invoice!=null){
			 OBDal.getInstance().remove(invoice);
		 }
		 throw new OBException(e.getMessage());
			
			
		}
		return invoice;
	}









}
