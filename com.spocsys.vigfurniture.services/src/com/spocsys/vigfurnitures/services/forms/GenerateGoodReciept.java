/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.forms;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.vigfurnitures.services.utils.DateUtils;
import com.spocsys.vigfurnitures.services.utils.FormUtils;
import com.spocsys.vigfurnitures.services.utils.MaterialShipmentInOutService;

public class GenerateGoodReciept extends HttpSecureAppServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);
		OBError myMessage = null;

		if (vars.commandIn("DEFAULT")) {
			String strDateFrom = vars.getGlobalVariable("inpDateFrom",
					"GenerateGoodReciept|DateFrom", "");
			String strDateTo = vars.getGlobalVariable("inpDateTo", "GenerateGoodReciept|DateTo", "");
			String strC_BPartner_ID = vars.getGlobalVariable("inpcBpartnerId",
					"GenerateGoodReciept|C_BPartner_ID", "");
			String strAD_Org_ID = vars.getGlobalVariable("inpadOrgId",
					"GenerateGoodReciept|AD_Org_ID", vars.getOrg());
			vars.setSessionValue("GenerateGoodReciept|isSOTrx", "Y");
			printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strDateFrom, strDateTo,null,null,null);
		} else if (vars.commandIn("FIND")) {
			String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
					"GenerateGoodReciept|DateFrom");
			String strDateTo = vars.getRequestGlobalVariable("inpDateTo",
					"GenerateGoodReciept|DateTo");
			String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
					"GenerateGoodReciept|C_BPartner_ID");
			String strAD_Org_ID = vars.getGlobalVariable("inpadOrgId",
					"GenerateGoodReciept|AD_Org_ID");
			  String strcOrderIds = vars.getInStringParameter("inpcOrderId_IN", IsIDFilter.instance);
			printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strDateFrom, strDateTo,null,null,strcOrderIds);
		} else if ( vars.commandIn("FINDLINE") || vars.commandIn("FINDLINEALL")  ) {

			String strDateFrom = vars.getGlobalVariable("inpDateFrom",
					"GenerateGoodReciept|DateFrom", "");
			String strDateTo = vars.getGlobalVariable("inpDateTo", "GenerateGoodReciept|DateTo", "");
			String strC_BPartner_ID = vars.getGlobalVariable("inpcBpartnerId",
					"GenerateGoodReciept|C_BPartner_ID", "");
			String strAD_Org_ID = vars.getGlobalVariable("inpadOrgId",
					"GenerateGoodReciept|AD_Org_ID", vars.getOrg());
			vars.setSessionValue("GenerateGoodReciept|isSOTrx", "Y");
			String strSalesOrder = vars.getInStringParameter("inpInvoice", IsIDFilter.instance);
			String strSalesOrderLine = vars.getInStringParameter("inplineOrder", IsIDFilter.instance);
			  String strcOrderIds = vars.getInStringParameter("inpcOrderId_IN", IsIDFilter.instance);
			printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strDateFrom, strDateTo, strSalesOrder,strSalesOrderLine,strcOrderIds);
		}else
			if (vars.commandIn("GENERATE")) {
				myMessage = new OBError();
				myMessage.setTitle("");


				String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcVendorId", "GenerateGoodRecieptInvoice|C_Vendor_ID");
				String strSalesOrder = vars.getInStringParameter("inpInvoice", IsIDFilter.instance);
		    		String strAD_Org_ID = vars.getGlobalVariable("inpadOrgId","GenerateGoodRecieptInvoice|AD_Org_ID", vars.getOrg());
		    		String[] arr = (Utility.getContext(this, vars, "#User_Client", "FinancialReport")).split(",");
		    	    String adUserClient = arr[0];
		    	    
		    	    if(arr.length >1)
		    	    	adUserClient = arr[1];
		   		
		    		String strSalesOrderLine = vars.getRequiredInStringParameter("inplineOrder", IsIDFilter.instance);
		    		ArrayList<String> transactionsIds = new ArrayList<String>();
		    		
		        	if((!strSalesOrderLine.equalsIgnoreCase(""))&&(strSalesOrderLine != null)){
		    	    	transactionsIds = Utility.stringToArrayList(strSalesOrderLine.replaceAll("\\(|\\)|'", ""));
		    	    	if((!strC_BPartner_ID.equalsIgnoreCase(""))&&(strC_BPartner_ID != null)){
			    	    	try {
			    	    		
			    	    		ORgData[] oRgDatas = ORgData.select(this, strSalesOrder) ;
			    	    		CoReferenceData[] coReferenceDatas = CoReferenceData.select(this, strSalesOrderLine) ;
			    	    		SoOriginData[] originDatas = SoOriginData.selectInvoiceLineOriginOrder(this, strSalesOrderLine) ;
								if(!checkSameOrg(oRgDatas))
									new OBException("Selected Invoices must belong to the same organization");
								
								
			    	    		ShipmentInOut invoice = MaterialShipmentInOutService.createShipment(oRgDatas[0].adOrgId, adUserClient, strC_BPartner_ID,strSalesOrder);
			    	    		if(coReferenceDatas.length==1){
			    	    			invoice.setOrderReference(coReferenceDatas[0].emSvfsvPoreference);
								}
			    	    		
			    	    		
			    	    		if(originDatas.length==1){
			    	    			invoice.setSalesOrder(OBDal.getInstance().get(Order.class,originDatas[0].id)); 
								}
			    	    		MaterialShipmentInOutService.saveShipment(invoice);
			    	    		MaterialShipmentInOutService.createShipmentLines(this, invoice.getId(), transactionsIds, vars);
			    	    		MaterialShipmentInOutService.updateSvfsvInoutCiLine(invoice, transactionsIds, vars);
			    	    		myMessage.setType("Success");
								myMessage.setTitle("Process Done!!");
								myMessage.setMessage("Process finished successfully. Shipment No: " +"<a href=\"#\" onclick=\"openTab('296','"+invoice.getId()+"');return false;\" onmouseover=\"window.status='Purchase Order';return true;\" onmouseout=\"window.status='';return true;\" class=\"Link\" >"+ 
							               " <span id=\"fieldDocumentno\">" + invoice.getDocumentNo() +"</span></a> created");
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								myMessage.setType("Error");
								myMessage.setMessage("Error detected:"+e.getMessage());
								
							}
		    	    	}
		    	    	else{
		    	    		myMessage.setType("Error");
		    				myMessage.setMessage("Error detected: No vendor selected");
		    	    	}
		    	    	
		        	}else{
		        		myMessage.setType("Error");
						myMessage.setMessage("Error detected: No lines selected");
		        	}
		    		
			      if (log4j.isDebugEnabled())
			        log4j.debug(myMessage.getMessage());
			      // new message system
			      vars.setMessage("GenerateGoodReciept", myMessage);
			      
			      response.sendRedirect(strDireccion + request.getServletPath());
			} else
				pageError(response);
	}

	private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
			String strC_BPartner_ID, String strAD_Org_ID, String strDateFrom, String strDateTo, String invoiceID,String Lines,String strcOrderIds)
					throws IOException, ServletException {
		if (log4j.isDebugEnabled())
			log4j.debug("Output: dataSheet");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String discard[] = { "sectionDetail" };
		XmlDocument xmlDocument = null;
		
		GenerateGoodRecieptData[] data = null;
		GenerateGoodRecieptLineData[] dataLine = null;

		String strTreeOrg = GenerateProInvoiceData.treeOrg(this, vars.getClient());
		if (StringUtils.isEmpty(strC_BPartner_ID) && StringUtils.isEmpty(strAD_Org_ID) ) {
			xmlDocument = xmlEngine.readXmlTemplate(
					"com/spocsys/vigfurnitures/services/forms/GenerateGoodReciept", discard).createXmlDocument();
			data = GenerateGoodRecieptData.set();
			dataLine= GenerateGoodRecieptLineData.set();

		} else {

			xmlDocument = xmlEngine.readXmlTemplate(
					"com/spocsys/vigfurnitures/services/forms/GenerateGoodReciept").createXmlDocument();


			data = GenerateGoodRecieptData.select(this, 

					Utility.getContext(this, vars, "#User_Org", "GenerateGoodReciept"), strC_BPartner_ID,strcOrderIds,
					strDateFrom, DateUtils.nDaysAfter(this, vars, strDateTo, "1"),
					Tree.getMembers(this, strTreeOrg, strAD_Org_ID));

			  if(StringUtils.isNotEmpty(invoiceID)){
					BpData[] bpDatas = BpData.select(this, invoiceID) ; 
				if(checkSameBp(bpDatas)){
					xmlDocument.setParameter("paramVendorIdDescription",
					 OBDal.getInstance().get(BusinessPartner.class, bpDatas[0].cBpartnerId).getName());
					xmlDocument.setParameter("paramVendorId", bpDatas[0].cBpartnerId);
				}}

		}

		if(StringUtils.isNotEmpty(invoiceID)){


			dataLine=  GenerateGoodRecieptLineData.select(this, invoiceID);
		
		}

		ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "GenerateGoodReciept", false, "",
				"", "", false, "ad_forms", strReplaceWith, false, true);
		toolbar.prepareSimpleToolBarTemplate();
		xmlDocument.setParameter("toolbar", toolbar.toString());

		try {
			WindowTabs tabs = new WindowTabs(this, vars,
					"com.spocsys.vigfurnitures.services.forms.GenerateGoodReciept");
			xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
			xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
			xmlDocument.setParameter("childTabContainer", tabs.childTabs());
			xmlDocument.setParameter("theme", vars.getTheme());
			NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
					"GenerateGoodReciept.html", classInfo.id, classInfo.type, strReplaceWith,
					tabs.breadcrumb());
			xmlDocument.setParameter("navigationBar", nav.toString());
			LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "GenerateGoodReciept.html",
					strReplaceWith);
			xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
			if(StringUtils.isNotEmpty(strcOrderIds)){
				final String[] bps = FormUtils.splitString(strcOrderIds);
				
			    if (bps != null) {
			      final StringBuilder sb = new StringBuilder();
			      for (String bpId : bps) {
			    	  bpId=bpId.trim();
			        final Order bp = OBDal.getInstance().get(Order.class, bpId);
			        sb.append("<option value='" + bpId + "'>" + bp.getDocumentNo()+" - " +Utility.getDateFormatter(vars).format(bp.getOrderDate()) + "</option>\n");
			      }
			      xmlDocument.setParameter("sectionBusinessPartners", sb.toString());
			    }}
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		{
			OBError myMessage = vars.getMessage("GenerateGoodReciept");
			vars.removeMessage("GenerateGoodReciept");
			if (myMessage != null) {
				xmlDocument.setParameter("messageType", myMessage.getType());
				xmlDocument.setParameter("messageTitle", myMessage.getTitle());
				xmlDocument.setParameter("messageMessage", myMessage.getMessage());
			}
		}

		xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
		xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
		xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
		xmlDocument.setParameter("paramBPartnerId", strC_BPartner_ID);
		xmlDocument.setParameter("paramAdOrgId", strAD_Org_ID);
		xmlDocument.setParameter("dateFrom", strDateFrom);
		xmlDocument.setParameter("dateFromdisplayFormat", vars.getJavaDateFormat());
		xmlDocument.setParameter("dateFromsaveFormat", vars.getJavaDateFormat());
		xmlDocument.setParameter("dateTo", strDateTo);
		xmlDocument.setParameter("dateTodisplayFormat", vars.getJavaDateFormat());
		xmlDocument.setParameter("dateTosaveFormat", vars.getJavaDateFormat());
		if(StringUtils.isNotEmpty(strC_BPartner_ID))
		xmlDocument.setParameter("paramBPartnerDescription",
				 OBDal.getInstance().get(BusinessPartner.class, strC_BPartner_ID).getName());

		try {
			ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
					"AD_Org Security validation", Utility.getContext(this, vars, "#User_Org",
							"GenerateGoodReciept"), Utility.getContext(this, vars, "#User_Client",
									"GenerateGoodReciept"), 0);
			Utility.fillSQLParameters(this, vars, null, comboTableData, "GenerateGoodReciept",
					strAD_Org_ID);
			xmlDocument.setData("reportAD_Org_ID", "liststructure", comboTableData.select(false));
			comboTableData = null;
		} catch (Exception ex) {
			throw new ServletException(ex);
		}

		String print ="";
		xmlDocument.setData("structure1", data);
		if(StringUtils.isNotEmpty(Lines))
			invoiceID=	invoiceID.concat(","+ Lines);
		if(dataLine!=null &&  ( vars.commandIn("FINDLINE") || vars.commandIn("FINDLINEALL")  ) ){
			xmlDocument.setData("structure2", dataLine);
			print= xmlDocument.print();
			if(vars.commandIn("FINDLINEALL")  )
				print=	FormUtils.replaceCheckedAll(print);
			print=FormUtils.replaceChecked(print, invoiceID);
		}else

			print= xmlDocument.print();
		out.println(print);
		

		out.close();
	}

	public String getServletInfo() {
		return "GenerateGoodReciept Servlet. This Servlet was made by Wad constructor";
	}






	
	
	private boolean checkSameBp(BpData[] bpIds ) {
		boolean same= true;
		String idFirst =bpIds[0].cBpartnerId;
		int i=0;
		while (i<bpIds.length&&same) {
			
			if(!idFirst.equalsIgnoreCase(bpIds[i].cBpartnerId)){
				same=false;
			}
			i++;
		}
		
		
		return same;


	}

	private boolean checkSameOrg(ORgData[] oRgDatas ) {
		boolean same= true;
		String idFirst =oRgDatas[0].adOrgId;
		int i=0;
		while (i<oRgDatas.length&&same) {
			
			if(!idFirst.equalsIgnoreCase(oRgDatas[i].adOrgId)){
				same=false;
			}
			i++;
		}
		
		
		return same;


	}

	// end of getServletInfo() method
}
