/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.forms;

import static com.spocsys.vigfurnitures.services.utils.QueryParameter.with;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.vigfurnitures.services.SVFSV_Proforma_Orderline;
import com.spocsys.vigfurnitures.services.utils.CommercialInvoiceProcessClass;
import com.spocsys.vigfurnitures.services.utils.DateUtils;
import com.spocsys.vigfurnitures.services.utils.FinderGeneric;
import com.spocsys.vigfurnitures.services.utils.FormUtils;

public class GenerateProInvoice extends HttpSecureAppServlet {
	private static final long serialVersionUID = 1L;


	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);
		OBError myMessage = null;

		if (vars.commandIn("DEFAULT")) {
			removePageSessionVariables(vars);
			String strNameValue = vars.getGlobalVariable("inpKey", "SalesOrder.name","");
			strNameValue = strNameValue + "%";
			vars.setSessionValue("SalesOrder.name", strNameValue);
			String strDateFrom = vars.getGlobalVariable("inpDateFrom",
					"GenerateProInvoice|DateFrom", "");
			String strDateTo = vars.getGlobalVariable("inpDateTo", "GenerateProInvoice|DateTo", "");
			String strC_BPartner_ID = vars.getGlobalVariable("inpcBpartnerId",
					"GenerateProInvoice|C_BPartner_ID", "");
			String strAD_Org_ID = vars.getGlobalVariable("inpadOrgId",
					"GenerateProInvoice|AD_Org_ID", vars.getOrg());
			vars.setSessionValue("GenerateProInvoice|isSOTrx", "Y");
			printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strDateFrom, strDateTo,null,null,strNameValue,null);
		} else if (vars.commandIn("FIND")) {
			String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
					"GenerateProInvoice|DateFrom");
			String strDateTo = vars.getRequestGlobalVariable("inpDateTo",
					"GenerateProInvoice|DateTo");
			String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
					"GenerateProInvoice|C_BPartner_ID");
			String strAD_Org_ID = vars.getGlobalVariable("inpadOrgId",
					"GenerateProInvoice|AD_Org_ID");
			String strNameValue = vars.getGlobalVariable("inpKey", "SalesOrder.name");
			String strcOrderIds = vars.getInStringParameter("inpcOrderId_IN", IsIDFilter.instance);
			String strSalesOrder = vars.getInStringParameter("inpOrder", IsIDFilter.instance);
			String strSalesOrderLine = vars.getInStringParameter("inplineOrder", IsIDFilter.instance);
			strNameValue = strNameValue + "%";
			printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strDateFrom, strDateTo,strSalesOrder,strSalesOrderLine,strNameValue,strcOrderIds);
		} else if (vars.commandIn("FINDLINE")||vars.commandIn("FINDLINEALL") ) {

			String strDateFrom = vars.getGlobalVariable("inpDateFrom",
					"GenerateProInvoice|DateFrom", "");
			String strDateTo = vars.getGlobalVariable("inpDateTo", "GenerateProInvoice|DateTo", "");
			String strC_BPartner_ID = vars.getGlobalVariable("inpcBpartnerId",
					"GenerateProInvoice|C_BPartner_ID", "");
			String strAD_Org_ID = vars.getGlobalVariable("inpadOrgId",
					"GenerateProInvoice|AD_Org_ID", vars.getOrg());
			vars.setSessionValue("GenerateProInvoice|isSOTrx", "Y");
			String strNameValue = vars.getGlobalVariable("inpKey", "SalesOrder.name");
			strNameValue = strNameValue + "%";

			String strSalesOrder = vars.getInStringParameter("inpOrder", IsIDFilter.instance);
			String strSalesOrderLine = vars.getInStringParameter("inplineOrder", IsIDFilter.instance);
			String strcOrderIds = vars.getInStringParameter("inpcOrderId_IN", IsIDFilter.instance);
			printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strDateFrom, strDateTo,strSalesOrder,strSalesOrderLine,strNameValue,strcOrderIds);
		}else
			if (vars.commandIn("GENERATE")) {
				myMessage = new OBError();
				myMessage.setTitle("");
				try {
					String strC_Vendor_ID = vars.getRequestGlobalVariable("inpcVendorId",
							"GenerateProInvoice|C_Vendor_ID");
					String strSalesOrderLine = vars.getRequiredInStringParameter("inplineOrder", IsIDFilter.instance);
					String strSalesOrder = vars.getInStringParameter("inpOrder", IsIDFilter.instance);

					String[] lineIds = FormUtils.splitString(strSalesOrderLine);

					ORgData[] oRgDatas = ORgData.select(this, strSalesOrder) ;


					CoReferenceData[] coReferenceDatas = CoReferenceData.select(this, strSalesOrderLine) ;
					String[] arr = (Utility.getContext(this, vars, "#User_Client", "FinancialReport")).split(",");
					String adUserClient = arr[0];
					if(arr.length >1)
						adUserClient = arr[1];
					Order order = OBProvider.getInstance().get(Order.class);
					if(!checkSameOrg(oRgDatas))
						new OBException("Selected Orders must belong to the same organization");
					ArrayList<String> transactionsIds = new ArrayList<String>();
					if((!strSalesOrderLine.equalsIgnoreCase(""))&&(strSalesOrderLine != null)){
						transactionsIds = Utility.stringToArrayList(strSalesOrderLine.replaceAll("\\(|\\)|'", ""));

						String createCIcheck = vars.getRequestGlobalVariable("inpCreateCI",
								"GenerateProInvoice|CreateCI");
						if(createCIcheck.equalsIgnoreCase("Y")){

							Order invoice =  CommercialInvoiceProcessClass.createComercialInvoice(oRgDatas[0].adOrgId, adUserClient, strC_Vendor_ID);
							if(coReferenceDatas.length==1){
								invoice.setOrderReference(coReferenceDatas[0].emSvfsvPoreference);
							}
							OBDal.getInstance().save(invoice);
							OBDal.getInstance().flush();
							CommercialInvoiceProcessClass.createCommercialInvoiceLines(this, invoice.getId(), transactionsIds, vars);
							CommercialInvoiceProcessClass.updateSvfsvCiProformaLine(invoice,transactionsIds , vars);

							myMessage.setType("Success");
							myMessage.setTitle("Process Done!!");
							myMessage.setMessage("Process finished successfully. Invoice No: " +"<a href=\"#\" onclick=\"openTab('B4C3FD23F7B941C49DC05F3A07158E10','"+invoice.getId()+"');return false;\" onmouseover=\"window.status='Purchase Order';return true;\" onmouseout=\"window.status='';return true;\" class=\"Link\" >"+ 
									" <span id=\"fieldDocumentno\">"+invoice.getDocumentNo()+"</span></a>"+  " created");


						}

						else{



							BusinessPartner partner = OBDal.getInstance().get(BusinessPartner.class,strC_Vendor_ID);
							strC_Vendor_ID="";
							order.setBusinessPartner(partner);
							order.setPartnerAddress(partner.getBusinessPartnerLocationList().get(0));
							order.setOrganization(OBDal.getInstance().get(Organization.class, oRgDatas[0].adOrgId));
							order.setClient(OBContext.getOBContext().getCurrentClient());
							order.setDocumentStatus("DR");
							order.setOrderDate(new Date());
							order.setAccountingDate(new Date());
							order.setScheduledDeliveryDate(new Date());
							if(coReferenceDatas.length==1){
								order.setOrderReference(coReferenceDatas[0].emSvfsvPoreference);
							}
							User user = OBDal.getInstance().get(User.class, OBContext.getOBContext().getUser().getId());
							Warehouse warehouse =user.getDefaultWarehouse();


							if(warehouse!=null)	
								order.setWarehouse(warehouse);	
							else 
								throw new OBException("No Warehouse");

							order.setOrderDate(new Date());
							if(partner.getPurchasePricelist()!=null )
								order.setPriceList(partner.getPurchasePricelist());
							else
								if(partner.getPriceList()!=null )
									order.setPriceList(partner.getPriceList());

							if(order.getPriceList()==null)
								throw new OBException("Business Partner :" + partner.getName()+ " doesn't have Price List");


							if(partner.getPOPaymentMethod()!=null )
								order.setPaymentMethod(partner.getPOPaymentMethod());
							else
								if(partner.getPaymentMethod()!=null )
									order.setPaymentMethod(partner.getPaymentMethod());

							if(order.getPaymentMethod()==null)
								throw new OBException("Business Partner :" + partner.getName()+ " doesn't have Payment Method");

							if(partner.getPOPaymentTerms()!=null )
								order.setPaymentTerms(partner.getPOPaymentTerms());
							else
								if(partner.getPaymentTerms()!=null )
									order.setPaymentTerms(partner.getPaymentTerms());

							if(order.getPaymentTerms()==null)
								throw new OBException("Business Partner :" + partner.getName()+ " doesn't have Payment Terms");

							order.setSalesTransaction(false);
							order.setAccountingDate(new Date());

							if(partner.getCurrency()!=null )
								order.setCurrency(partner.getCurrency());
							else
								throw new OBException("Business Partner :" + partner.getName()+ " doesn't have Currency");

							List<DocumentType> documentTypes = FinderGeneric.find(DocumentType.class, with(DocumentType.PROPERTY_NAME, "Pro-Forma Invoices").parameters(), true, false);
							if(documentTypes == null ||documentTypes.isEmpty())
								throw new OBException("There is not default Document Type for Document Category : Pro-Forma Invoices");

							order.setDocumentType(documentTypes.get(0));
							order.setTransactionDocument(documentTypes.get(0));
							Long lineNo = 10L;
							OBDal.getInstance().save(order);
							OBDal.getInstance().flush();
							for (String id : lineIds) {
								id=id.trim();
								OrderLine line = copyLine(id);
								line.setSalesOrder(order);
								line.setLineNo(lineNo);
								updateLineOrderOrigin(vars,id, order,line );


								order.getOrderLineList().add(line);
								lineNo+=10L;


							}

							OBDal.getInstance().save(order);
							OBDal.getInstance().flush();


							myMessage.setType("Success");
							myMessage.setTitle("Process Done!!");
							myMessage.setMessage("Pro Forma Invoice Created with Document No. "+ "<a href=\"#\" onclick=\"openTab('33BC5FB7A05444DB83629B49876A2684','"+order.getId()+"');return false;\" onmouseover=\"window.status='Purchase Order';return true;\" onmouseout=\"window.status='';return true;\" class=\"Link\" >"+ 
									" <span id=\"fieldDocumentno\">"+order.getDocumentNo()+"</span></a>");
						}}} catch (Exception e) {
							myMessage = Utility.translateError(this, vars, vars.getLanguage(), e.getMessage());

							log4j.warn("Error");
							myMessage.setType("Error");

						}finally{
							OBDal.getInstance().commitAndClose();
						}

				vars.setMessage("GenerateProInvoice", myMessage);
				response.sendRedirect(strDireccion + request.getServletPath());
			} else
				pageError(response);
	}

	private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
			String strC_BPartner_ID, String strAD_Org_ID, String strDateFrom, String strDateTo, String SalesOrder, String Lines, String strNameValue,  String strcOrderIds)
					throws IOException, ServletException {
		if (log4j.isDebugEnabled())
			log4j.debug("Output: dataSheet");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String discard[] = { "sectionDetail","sectionDetailLine" };
		XmlDocument xmlDocument = null;

		GenerateProInvoiceData[] data = null;
		GenerateProInvoiceLineData[] dataLine = null;

		String strTreeOrg = GenerateProInvoiceData.treeOrg(this, vars.getClient());
		if (StringUtils.isEmpty(strC_BPartner_ID) && StringUtils.isEmpty(strAD_Org_ID) ) {
			xmlDocument = xmlEngine.readXmlTemplate(
					"com/spocsys/vigfurnitures/services/forms/GenerateProInvoice", discard).createXmlDocument();
			data = GenerateProInvoiceData.set();
			dataLine= GenerateProInvoiceLineData.set();

		} else 

		{

			xmlDocument = xmlEngine.readXmlTemplate(
					"com/spocsys/vigfurnitures/services/forms/GenerateProInvoice").createXmlDocument();

			data = GenerateProInvoiceData.select(this, 

					Utility.getContext(this, vars, "#User_Org", "GenerateProInvoice"), strC_BPartner_ID,
					strcOrderIds,strDateFrom, DateUtils.nDaysAfter(this, vars, strDateTo, "1"),
					Tree.getMembers(this, strTreeOrg, strAD_Org_ID));


			if(StringUtils.isNotEmpty(SalesOrder)){
				BpData[] bpDatas = BpData.select(this, SalesOrder) ;

				if(checkSameBp(bpDatas)){
					xmlDocument.setParameter("paramVendorIdDescription",
							OBDal.getInstance().get(BusinessPartner.class, bpDatas[0].cBpartnerId).getName());
					xmlDocument.setParameter("paramVendorId", bpDatas[0].cBpartnerId);
				}
			}}

		if(StringUtils.isNotEmpty(SalesOrder)){
			dataLine=  GenerateProInvoiceLineData.select(this, SalesOrder);

		}

		ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "GenerateProInvoice", false, "",
				"", "", false, "ad_forms", strReplaceWith, false, true);
		toolbar.prepareSimpleToolBarTemplate();
		xmlDocument.setParameter("toolbar", toolbar.toString());

		try {
			WindowTabs tabs = new WindowTabs(this, vars,
					"com.spocsys.vigfurnitures.services.forms.GenerateProInvoice");
			xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
			xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
			xmlDocument.setParameter("childTabContainer", tabs.childTabs());
			xmlDocument.setParameter("theme", vars.getTheme());
			NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
					"GenerateProInvoice.html", classInfo.id, classInfo.type, strReplaceWith,
					tabs.breadcrumb());
			xmlDocument.setParameter("navigationBar", nav.toString());
			LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "GenerateProInvoice.html",
					strReplaceWith);
			xmlDocument.setParameter("leftTabs", lBar.manualTemplate());

			if(StringUtils.isNotEmpty(strcOrderIds)){
				final String[] bps = FormUtils.splitString(strcOrderIds);

				if (bps != null) {
					final StringBuilder sb = new StringBuilder();
					for (String bpId : bps) {
						bpId=bpId.trim();
						final Order bp = OBDal.getInstance().get(Order.class, bpId);
						sb.append("<option value='" + bpId + "'>" + bp.getDocumentNo() + "</option>\n");
					}
					xmlDocument.setParameter("sectionBusinessPartners", sb.toString());
				}}
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		{
			OBError myMessage = vars.getMessage("GenerateProInvoice");
			vars.removeMessage("GenerateProInvoice");
			if (myMessage != null) {
				xmlDocument.setParameter("messageType", myMessage.getType());
				xmlDocument.setParameter("messageTitle", myMessage.getTitle());
				xmlDocument.setParameter("messageMessage", myMessage.getMessage());
			}
		}

		xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
		xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
		xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
		xmlDocument.setParameter("paramBPartnerId", strC_BPartner_ID);
		xmlDocument.setParameter("paramAdOrgId", strAD_Org_ID);
		xmlDocument.setParameter("dateFrom", strDateFrom);
		xmlDocument.setParameter("dateFromdisplayFormat", vars.getJavaDateFormat());
		xmlDocument.setParameter("dateFromsaveFormat",vars.getJavaDateFormat());
		xmlDocument.setParameter("dateTo", strDateTo);
		xmlDocument.setParameter("dateTodisplayFormat", vars.getJavaDateFormat());
		xmlDocument.setParameter("dateTosaveFormat", vars.getJavaDateFormat());
		xmlDocument.setParameter("key", strNameValue);
		if(StringUtils.isNotEmpty(strC_BPartner_ID))
			xmlDocument.setParameter("paramBPartnerDescription",
					OBDal.getInstance().get(BusinessPartner.class, strC_BPartner_ID).getName());

		try {
			ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
					"AD_Org Security validation", Utility.getContext(this, vars, "#User_Org",
							"GenerateProInvoice"), Utility.getContext(this, vars, "#User_Client",
									"GenerateProInvoice"), 0);
			Utility.fillSQLParameters(this, vars, null, comboTableData, "GenerateProInvoice",
					strAD_Org_ID);
			xmlDocument.setData("reportAD_Org_ID", "liststructure", comboTableData.select(false));
			comboTableData = null;

			String print ="";
			xmlDocument.setData("structure1", data);
			if(StringUtils.isNotEmpty(Lines))
				SalesOrder=	SalesOrder.concat(","+ Lines);

			if(dataLine!=null && ( vars.commandIn("FINDLINE") || vars.commandIn("FINDLINEALL")  )){
				xmlDocument.setData("structure2", dataLine);
				print= xmlDocument.print();
				if(vars.commandIn("FINDLINEALL")  )
					print=	FormUtils.replaceCheckedAll(print);
				print=FormUtils.replaceChecked(print,SalesOrder);

			}else{

				print= xmlDocument.print();

			}


			out.println(print);
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
		out.close();
	}

	public String getServletInfo() {
		return "GenerateProInvoice Servlet. This Servlet was made by Wad constructor";
	}




	private boolean checkSameBp(BpData[] bpIds ) {
		boolean same= true;
		String idFirst =bpIds[0].cBpartnerId;
		int i=0;
		while (i<bpIds.length&&same) {

			if(!idFirst.equalsIgnoreCase(bpIds[i].cBpartnerId)){
				same=false;
			}
			i++;
		}


		return same;


	}

	private boolean checkSameOrg(ORgData[] oRgDatas ) {
		boolean same= true;
		String idFirst =oRgDatas[0].adOrgId;
		int i=0;
		while (i<oRgDatas.length&&same) {

			if(!idFirst.equalsIgnoreCase(oRgDatas[i].adOrgId)){
				same=false;
			}
			i++;
		}


		return same;


	}




	private OrderLine copyLine(String lineId) {
		OrderLine current = OBDal.getInstance().get(OrderLine.class, lineId);

		Product product = OBDal.getInstance().get(Product.class, current.getProduct().getId());




		OrderLine newOrder = new OrderLine();

		newOrder.setProduct(current.getProduct());
		newOrder.setPartnerAddress(current.getPartnerAddress());
		newOrder.setCurrency(current.getCurrency());
		newOrder.setOrderDate(new Date());
		newOrder.setSvfsvPoreference(current.getSvfsvPoreference());
		newOrder.setWarehouse(current.getWarehouse());


		newOrder.setSvfpaShipFrom(current.getSvfpaShipFrom());
		newOrder.setSvfpaStockLocation(current.getSvfpaStockLocation());

		newOrder.setSvfpaSpecialOrder(current.isSvfpaSpecialOrder());

		newOrder.setAttributeSetValue(current.getAttributeSetValue());
		newOrder.setSvfsvOrigCOrderline(current);
		newOrder.setSvfsvOrigCOrder(current.getSalesOrder());
		newOrder.setListPrice(current.getListPrice());
		newOrder.setUnitPrice(current.getUnitPrice());
		newOrder.setPriceLimit(current.getPriceLimit());
		newOrder.setDiscount(current.getDiscount());
		newOrder.setStandardPrice(current.getStandardPrice());
		newOrder.setTaxableAmount(current.getTaxableAmount());
		/*if(product!=null && product.getTaxCategory()!=null){
			List<TaxRate> listTax=	FinderGeneric.find(TaxRate.class,  with(TaxRate.PROPERTY_TAXCATEGORY, product.getTaxCategory()).parameters(), false, false);
			if(listTax!=null && !listTax.isEmpty())	
				newOrder.setTax(listTax.get(0));




		}else
			throw new OBException("There is not Tax for product category: "+product==null?"":product.getTaxCategory().getName() );*/
		newOrder.setTax(current.getTax());
		newOrder.setUOM(product.getUOM());

		newOrder.setSvfsvPoreference(current.getSvfsvPoreference());

		newOrder.setOrganization(current.getOrganization());
		newOrder.setClient(current.getClient());

		return newOrder;

	}



	private void updateLineOrderOrigin(VariablesSecureApp vars,String id,Order order,OrderLine line ) throws ServletException {

		getSelectedBaseOBObjectAmount(vars, id, "inpPaymentAmount", order, line);



	}

	public static void  getSelectedBaseOBObjectAmount(
			VariablesSecureApp vars, String o, String htmlElementId,Order order,OrderLine line)
					throws ServletException {

		OrderLine current = OBDal.getInstance().get(OrderLine.class, o);
		BigDecimal decimal = new  BigDecimal(vars.getNumericParameter(htmlElementId + (String) o, ""));

		current.setSvfsvQtyallocatedGen(current.getSvfsvQtyallocatedGen()+decimal.longValue());
		SVFSV_Proforma_Orderline proformaOrderline = OBProvider.getInstance().get(SVFSV_Proforma_Orderline.class);
		line.setOrderedQuantity(decimal);


		proformaOrderline.setOrder(order);
		proformaOrderline.setOrderline(current);
		proformaOrderline.setQuantity(decimal.longValue());


		OBDal.getInstance().save(proformaOrderline);
		OBDal.getInstance().save(current);


	}
	// end of getServletInfo() method

	private void removePageSessionVariables(VariablesSecureApp vars) {
		vars.removeSessionValue("SalesOrder.name");
		vars.removeSessionValue("SalesOrder.bpartner");
		vars.removeSessionValue("SalesOrder.datefrom");
		vars.removeSessionValue("SalesOrder.dateto");
		vars.removeSessionValue("SalesOrder.description");
		vars.removeSessionValue("SalesOrder.grandtotalfrom");
		vars.removeSessionValue("SalesOrder.grandtotalto");
		vars.removeSessionValue("SalesOrder.order");
		vars.removeSessionValue("SalesOrder.adorgid");
		vars.removeSessionValue("SalesOrderInfo.currentPage");
	}


}
