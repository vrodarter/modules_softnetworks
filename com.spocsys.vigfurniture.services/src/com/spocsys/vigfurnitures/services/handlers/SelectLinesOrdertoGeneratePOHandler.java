package com.spocsys.vigfurnitures.services.handlers;

import static com.spocsys.vigfurnitures.services.utils.QueryParameter.with;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.ApprovedVendor;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.tax.TaxRate;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.model.pricing.pricelist.ProductPrice;

import com.spocsys.vigfurniture.incomingproduct.OrderReference;
import com.spocsys.vigfurnitures.services.SVFSV_Source;
import com.spocsys.vigfurnitures.services.utils.FinderGeneric;

public class SelectLinesOrdertoGeneratePOHandler extends
BaseProcessActionHandler {

	String dropshipInfoId= "";

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters,
			String content) {
		JSONObject msg = new JSONObject();
		JSONObject request =  new JSONObject();

		try {
			request =new JSONObject(content);
			JSONObject params = request.getJSONObject("_params");
			JSONObject message = params.getJSONObject("Generate PO ");	
			String c_bpartner_id = params.getString("c_bpartner_id");
			String isDefault = params.getString("em_svfsv_default");
			JSONArray selection = new JSONArray(message.getString(ApplicationConstants.SELECTION_PROPERTY));
			checkDropDhip( selection);
			if(isDefault.equalsIgnoreCase("true")&& StringUtils.isNotEmpty(c_bpartner_id)){
				JsonObjectByBP jsonObjectByBP  =  defaultBp(c_bpartner_id, selection);
				msg = createPO(jsonObjectByBP);
			}else{

				List<JsonObjectByBP> list=  checkBp( selection); 
				if(list!=null && !list.isEmpty())
					if(list.size()==1)
						if(list.get(0).bpartnerId.equalsIgnoreCase("null")){
							throw new OBException("No Vendor selected!!");

						}

				for (JsonObjectByBP jsonObjectByBP : list) {
					msg = createPO(jsonObjectByBP);

				}
			}
			JSONArray responseActions = new JSONArray();
			JSONObject refress = new JSONObject();
			JSONObject action2 = new JSONObject();
			JSONObject action2Params = new JSONObject();

			refress.put("refreshGrid", true);


			if(msg.has("record_id")){
				String record_id= msg.getString("record_id");
				action2Params.put("tabId", "294");
				action2Params.put("recordId", record_id);
				action2Params.put("wait", "true");
				action2.put("openDirectTab", action2Params);
				responseActions.put(action2);
			}

			responseActions.put(refress);

			request.put("message", msg);
			request.put("responseActions", responseActions);
			request.put("retryExecution", true);
			request.put("refreshGrid", true);
			OBDal.getInstance().commitAndClose();

		}catch(Exception e){
			OBDal.getInstance().rollbackAndClose();
			
			msg.remove("severity"); 
			msg.remove("text"); 
			try {
				msg.put("severity", "error");
				msg.put("text", e.getMessage());
				request.put("message", msg);
				request.put("retryExecution", true);
			} catch (JSONException e1) {

			}



		}

		return request;
	}

	private JSONObject createPO(JsonObjectByBP bp) throws Exception {
		JSONObject msg = new JSONObject();

	

			OBContext.getOBContext().setAdminMode(true);
			Order order = new Order();
			BusinessPartner partner = OBDal.getInstance().get(BusinessPartner.class, bp.getBpartnerId());
			order.setBusinessPartner(partner);
			order.setPartnerAddress((partner.getBusinessPartnerLocationList()!=null && !partner.getBusinessPartnerLocationList().isEmpty())?partner.getBusinessPartnerLocationList().get(0):null);
			if(order.getPartnerAddress()==null)
				throw new OBException("There is not Partner Address for "+ partner.getIdentifier());
			order.setOrganization(OBContext.getOBContext().getCurrentOrganization());
			order.setClient(OBContext.getOBContext().getCurrentClient());


			User user = OBDal.getInstance().get(User.class, OBContext.getOBContext().getUser().getId());
			Warehouse warehouse =user.getDefaultWarehouse();
			order.setScheduledDeliveryDate(new Date());

			if(warehouse!=null)	
				order.setWarehouse(warehouse);	
			else 
				throw new OBException("There is not Warehouse");
			order.setOrderDate(new Date());
			if(partner.getPurchasePricelist()!=null )
				order.setPriceList(partner.getPurchasePricelist());


			if(order.getPriceList()==null)
				throw new OBException("Business Partner :" + partner.getName()+ " don't have Purchase Price List");


			if(partner.getPOPaymentMethod()!=null )
				order.setPaymentMethod(partner.getPOPaymentMethod());
			else
				if(partner.getPaymentMethod()!=null )
					order.setPaymentMethod(partner.getPaymentMethod());

			if(order.getPaymentMethod()==null)
				throw new OBException("Business Partner :" + partner.getName()+ " don't have Payment Method");

			if(partner.getPOPaymentTerms()!=null )
				order.setPaymentTerms(partner.getPOPaymentTerms());
			else
				if(partner.getPaymentTerms()!=null )
					order.setPaymentTerms(partner.getPaymentTerms());

			if(order.getPaymentTerms()==null)
				throw new OBException("Business Partner :" + partner.getName()+ " don't have Payment Terms");


			order.setSalesTransaction(false);
			order.setAccountingDate(new Date());

			if(partner.getCurrency()!=null )
				order.setCurrency(partner.getCurrency());
			else
				throw new OBException("Business Partner :" + partner.getName()+ " don't have Currency");
			DocumentType documentType = getDocType(order.getOrganization());
			if(documentType == null)
				throw new OBException("There is not default Document Type for Document Category : Purchase Order");

			order.setDocumentType(documentType);
			order.setTransactionDocument(documentType);
			order.setOrderReference(partner.getOrderReference());
			if(StringUtils.isNotEmpty(dropshipInfoId)){
				Order salesOrderDropShip = OBDal.getInstance().get(Order.class, dropshipInfoId);

				order.setSvfdsDropship(true);
				order.setDropShipContact(salesOrderDropShip.getDropShipContact());
				order.setDropShipLocation(salesOrderDropShip.getDropShipLocation());
				order.setDropShipPartner(salesOrderDropShip.getDropShipPartner());
			}


			List<JSONObject> list = bp.getSelectedRows();
			Long line = 10L;
			for (JSONObject selectedOrderLineJSON : list) {

				String sourceID = selectedOrderLineJSON.getString("_identifier");
				SVFSV_Source source= OBDal.getInstance().get(SVFSV_Source.class, sourceID);

				if(source==null){
					throw new OBException("Please refresh process, this element was already processed previously");
				}
				String productID = selectedOrderLineJSON.getString("product");
				String poference = selectedOrderLineJSON.getString("orderReference");

				Product product = OBDal.getInstance().get(Product.class, productID);

				String  salesOrderId = selectedOrderLineJSON.getString("order");

				Order saleOrder = OBDal.getInstance().get(Order.class, salesOrderId);

				String customer_order_reference = selectedOrderLineJSON.getString("orderReference");
				BigDecimal qty = new BigDecimal(selectedOrderLineJSON.getDouble("qty"));

				OrderLine orderLine = new OrderLine();
				orderLine.setLineNo(line);
				orderLine.setBusinessPartner(partner);

				if(StringUtils.isNotEmpty(poference)){
					orderLine.setSvfsvPoreference(poference);
				}else
					orderLine.setSvfsvPoreference(order.getOrderReference());
				orderLine.setOrderDate(new Date());
				orderLine.setPartnerAddress(order.getPartnerAddress());
				orderLine.setProduct(product);
				orderLine.setOrderedQuantity(qty);
				orderLine.setWarehouse(order.getWarehouse());
				orderLine.setCurrency(order.getCurrency());
				ProductPrice price = getPOPriceList(product);
				if(price != null){
					orderLine.setListPrice(price.getListPrice());
					orderLine.setUnitPrice(price.getStandardPrice());
					orderLine.setStandardPrice(price.getStandardPrice());
				}else
					throw new OBException("No purchase price list for product: "+product.getName() );

				if(source.getOrderline()!=null &&source.getOrderline().getTax()!=null)
					orderLine.setTax(source.getOrderline().getTax());
				else{
				if(product!=null && product.getTaxCategory()!=null){
					List<TaxRate> listTax=	FinderGeneric.find(TaxRate.class,  with(TaxRate.PROPERTY_TAXCATEGORY, product.getTaxCategory()).parameters(), false, false);
					if(listTax!=null && !listTax.isEmpty())	
						orderLine.setTax(listTax.get(0));
					else
						throw new OBException("There is not Tax for product category: "+product.getTaxCategory().getName() );

					

				}else
					throw new OBException("There is not Tax for product category: "+product.getTaxCategory().getName() );
				}
				if(saleOrder!=null)
				order.setOrderReference(saleOrder.getOrderReference());
				orderLine.setUOM(product.getUOM());

				orderLine.setSvfsvPoreference(customer_order_reference);
				orderLine.setSalesOrder(order);
				orderLine.setAttributeSetValue(source.getOrderline().getAttributeSetValue());
				orderLine.setOrganization(OBContext.getOBContext().getCurrentOrganization());
				orderLine.setClient(OBContext.getOBContext().getCurrentClient());
				order.getOrderLineList().add(orderLine);


				line+=10L;
				if(source.getOrder() !=null){
				OrderReference orderReference = OBProvider.getInstance().get(OrderReference.class);
				orderReference.setSalesOrder(source.getOrder());
				orderReference.setOrganization(source.getOrganization());
				orderReference.setOrderlineref(source.getOrderline());
				orderReference.setSalesOrderLine(orderLine);
				orderReference.setQuantityAllocated(new BigDecimal(source.getQty()));
				OBDal.getInstance().save(orderReference);
				}
				OBDal.getInstance().save(order);
				OBDal.getInstance().save(orderLine);
		
				OBDal.getInstance().flush();

				OBDal.getInstance().remove(source);

			}



			OBDal.getInstance().save(order);
			OBDal.getInstance().flush();
			//Create Sales Order Reference


			msg.put("severity", "info");
			msg.put("text", "Purchase Order created: " +order.getIdentifier());
			msg.put("record_id", order.getId());
	
		// TODO Auto-generated method stub
		return msg;
	}

	public  DocumentType getDocType(Organization organization) throws ServletException {

		OBCriteria<DocumentType> criteria = OBDal.getInstance().createCriteria(DocumentType.class);
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_ORGANIZATION, organization));
        criteria.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, "POO"));
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, false));
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_TABLE, getTable()));

		List<DocumentType> listE = criteria.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}
	
	private  Table getTable() {
		Table table = null;
		OBCriteria<Table> criteria = OBDal.getInstance().createCriteria(Table.class);
		criteria.add(Restrictions.eq(Table.PROPERTY_DBTABLENAME, "C_Order"));
		List<Table> list= criteria.list();
	    if(list!=null && !list.isEmpty()){
	    	table=list.get(0);
	    }
	return table;
	}
	
	private ProductPrice getPOPriceList(Product product) {


		ProductPrice price = null;

		// get the movementLine
		StringBuilder hsqlScript= new StringBuilder();
		hsqlScript.append("select price from PricingProductPrice as price ");
		hsqlScript.append("left outer join price.priceListVersion listversion ");
		hsqlScript.append("left outer join listversion.priceList pricelist ");

		hsqlScript.append("where pricelist.");
		hsqlScript.append(PriceList.PROPERTY_SALESPRICELIST);

		hsqlScript.append(" = 'N'");
		hsqlScript.append(" and price.");
		hsqlScript.append(ProductPrice.PROPERTY_PRODUCT);
		hsqlScript.append(".id='");
		hsqlScript.append(product.getId());
		hsqlScript.append("'");
		final Session session = OBDal.getInstance().getSession();
		final Query query = session.createQuery(hsqlScript.toString());

		List<ProductPrice> list=   query.list();
		if(list!=null && !list.isEmpty())
			price=list.get(0);


		return price;


	}

	private List<JsonObjectByBP> checkBp(JSONArray selection) throws JSONException {
		List<JsonObjectByBP> result= new ArrayList<SelectLinesOrdertoGeneratePOHandler.JsonObjectByBP>();


		ArrayList<String> checked= new ArrayList<String>();


		for(int i = 0; i < selection.length(); i++){
			JSONObject selectedOrderLineJSON = selection.getJSONObject(i);
			String bpartnerid = selectedOrderLineJSON.getString("bpartner");
			if(!bpartnerid.equalsIgnoreCase("null")&&!checked.contains(bpartnerid)){
				checked.add(bpartnerid);
				JsonObjectByBP jsonObjectByBP = new JsonObjectByBP(bpartnerid);
				for(int j = 0; j < selection.length(); j++){

					JSONObject selected = selection.getJSONObject(j);
					String bpartnerid2 = selected.getString("bpartner");

					if(!selected.has("checked"))
						if(bpartnerid.equalsIgnoreCase(bpartnerid2)||bpartnerid2.equalsIgnoreCase("null")){
							jsonObjectByBP.add(selected);
							selected.put("checked", true);
						}
					if(bpartnerid2.equalsIgnoreCase("null")){

					}



				}

				result.add(jsonObjectByBP);
			}



		}


		return result;
	}

	private void checkDropDhip(JSONArray selection) throws JSONException {
		dropshipInfoId= "";
		boolean found =false;

		for(int i = 0; i < selection.length()& !found; i++){
			JSONObject selectedOrderLineJSON = selection.getJSONObject(i);
			String orderid = selectedOrderLineJSON.getString("order");
			Order salesOrder= OBDal.getInstance().get(Order.class, orderid);
			if(salesOrder!=null &&salesOrder.isSvfdsDropship()){
				found=true;
				dropshipInfoId=orderid;
			}
		}
	}

	private JsonObjectByBP defaultBp(String bpartner,JSONArray selection) throws JSONException {

		JsonObjectByBP jsonObjectByBP = new JsonObjectByBP(bpartner);

		for(int j = 0; j < selection.length(); j++){

			JSONObject selected = selection.getJSONObject(j);
			jsonObjectByBP.add(selected);

		}

		return jsonObjectByBP;
	}

	private boolean checkBPSupplier(String productId, String bpartnerId) {

		BusinessPartner businessPartner =OBDal.getInstance().get(BusinessPartner.class, bpartnerId);
		Product product = OBDal.getInstance().get(Product.class, productId);


		if(businessPartner!=null && product!=null)
			return checkBPSupplier(product, businessPartner) ;

		return false;
	}
	private boolean checkBPSupplier(Product product, BusinessPartner businessPartner) {

		if(businessPartner!=null && product!=null)
		{
			List<ApprovedVendor> list = FinderGeneric.find(ApprovedVendor.class, with(ApprovedVendor.PROPERTY_BUSINESSPARTNER, businessPartner).and(ApprovedVendor.PROPERTY_PRODUCT, product).parameters(), false, false);
			if(list!=null && !list.isEmpty())
				return true;
		}

		return false;
	}


	class JsonObjectByBP{

		private String bpartnerId;

		List<JSONObject> selectedRows;

		public JsonObjectByBP(String id) {

			super();
			this.bpartnerId=id;
			selectedRows= new ArrayList<JSONObject>();

		}

		public String getBpartnerId() {
			return bpartnerId;
		}

		public void setBpartnerId(String bpartnerId) {
			this.bpartnerId = bpartnerId;
		}

		public List<JSONObject> getSelectedRows() {
			return selectedRows;
		}

		public void setSelectedRows(List<JSONObject> selectedRows) {
			this.selectedRows = selectedRows;
		}

		public void add(JSONObject jsonObject){
			this.selectedRows.add(jsonObject);
		}


	}



}
