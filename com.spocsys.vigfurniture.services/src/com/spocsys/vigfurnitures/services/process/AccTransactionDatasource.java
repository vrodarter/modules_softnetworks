package com.spocsys.vigfurnitures.services.process;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class AccTransactionDatasource implements JRDataSource
{
    private ArrayList<String> transactionIds = new ArrayList<String>();
    
    
   private  VariablesSecureApp vars; 
   

public void setVars(VariablesSecureApp vars) {
	this.vars = vars;
}
	public void setTransactionIds(ArrayList<String> transactionIds) {
		this.transactionIds = transactionIds;
	}

	private int indice = -1;
    
    public AccTransactionDatasource(){
		super();
		transactionIds = new ArrayList<String>();
	}
@Override
    public Object getFieldValue(JRField jrf) throws JRException
    {
        String valor = null;
        
        FIN_FinaccTransaction finaccTransaction  = OBDal.getInstance().get(FIN_FinaccTransaction.class, transactionIds.get(indice));
        
       
        if ("description".equals(jrf.getName()))
        {
            valor = finaccTransaction.getDescription();
        }
        else if ("date".equals(jrf.getName()))
        {
        	SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
            valor = dateFormat.format(finaccTransaction.getCreationDate());
        }
        else if ("bp".equals(jrf.getName()))
        {
           valor = finaccTransaction.getBusinessPartner()==null?"":finaccTransaction.getBusinessPartner().getName();
        }else if ("gl".equals(jrf.getName()))
        {
            valor = finaccTransaction.getGLItem()==null?"":finaccTransaction.getGLItem().getName();
        }else if ("paymethod".equals(jrf.getName()))
        {
            valor = finaccTransaction.getFinPayment()==null?"": finaccTransaction.getFinPayment().getPaymentMethod().getName();
        }else if ("deposit".equals(jrf.getName()))
        {
         valor = finaccTransaction.getDepositAmount().toString();
        }else if ("amnt".equals(jrf.getName()))
        {
            try {
				valor = getAmount(vars, transactionIds.get(indice)).toString();
			} catch (ServletException e) {
		
			}
        }


        return valor;
    }

    public boolean next() throws JRException
    {
        return ++indice < transactionIds.size();
    }

    
    
    private BigDecimal getAmount(VariablesSecureApp vars,String id) throws ServletException {

		return getSelectedBaseOBObjectAmount(vars, id, "inpPaymentAmount");



	}

	public  static BigDecimal  getSelectedBaseOBObjectAmount(
			VariablesSecureApp vars, String o, String htmlElementId)
					throws ServletException {


		BigDecimal decimal = new  BigDecimal(vars.getRequiredNumericParameter(htmlElementId + (String) o, ""));


		return decimal;

	}
}