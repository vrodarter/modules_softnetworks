/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2012 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.process;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.hibernate.Hibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.ad_process.AcctServerProcess;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.utility.DateTimeData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.erpCommon.utility.reporting.ReportManager;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.system.ClientInformation;
import org.openbravo.model.ad.utility.Attachment;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.xmlEngine.XmlDocument;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.spocsys.vigfurniture.zebra.utils.ReportingUtils;
import com.spocsys.vigfurnitures.services.utils.DataSearchClass;
import com.spocsys.vigfurnitures.services.utils.DateUtils;
import com.spocsys.vigfurnitures.services.utils.PaymentDataSearchClass;
import com.sun.org.apache.bcel.internal.generic.ACONST_NULL;

public class AccountTransfer extends HttpSecureAppServlet {
	private static final long serialVersionUID = 1L;
	private Logger attachmentLogger= Logger.getLogger("Attachment Logger");
	
	 static  org.apache.log4j.Logger log4j = org.apache.log4j.Logger.getLogger(AccountTransfer.class);

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);

		if (vars.commandIn("DEFAULT")) {

			clearSessionValue(vars);
			String fFinancialAccountId = vars.getStringParameter("Fin_Financial_Account_ID");
			String strWindow = vars.getStringParameter("inpwindowId");
			String strTab = vars.getStringParameter("inpTabId");
			vars.setSessionValue("fFinancialAccountId", fFinancialAccountId);
			vars.setSessionValue("inpwindowId", strWindow);
			vars.setSessionValue("inpTabId", strTab);

			String strDateFrom = vars.getGlobalVariable("inpDateFrom",
					"AccountTransfer|DateFrom","");
			String strDateTo = vars.getGlobalVariable("inpDateTo",
					"AccountTransfer|DateTo","");

			String strC_BPartner_ID = vars.getGlobalVariable("inpcBpartnerIdParam",
					"AccountTransfer|C_BPartner_ID","");

			String strPaymentMethod_ID = vars.getGlobalVariable("inpPaymentMethodId",
					"AccountTransfer|FIN_PayementMethod_ID","");

			String acctDestiny = vars.getGlobalVariable("inpDestinyAccounId",
					"AccountTransfer|FIN_Account_ID","");

			String glDestiny = vars.getGlobalVariable("inpDestinyGlItemId",
					"AccountTransfer|GLItem_Destiny","");

			String glOrigin = vars.getGlobalVariable("inpOrigenGlItemId",
					"AccountTransfer|GLItem_Origin","");

			FIN_FinancialAccount origenAccount = OBDal.getInstance().get(FIN_FinancialAccount.class, fFinancialAccountId);
			String requiredPayMethod = vars.getGlobalVariable("inpRequiredPaymentMethodId",
					"AccountTransfer|RequiredPaymentMethod","");
			String selectedCashOrigen =  vars.getGlobalVariable("inpCashAccounId","AccountTransfer|FIN_Acc_Cash_ID","");
			String selectedCashDestiny =  vars.getGlobalVariable("inpCashDestinyAccounId","AccountTransfer|FIN_Acc_Cash_ID2","");
			//printPageDataSheet(response, vars);C_Order_ID
			printPage(request,response,vars,origenAccount,requiredPayMethod,strC_BPartner_ID,strPaymentMethod_ID,strDateFrom,strDateTo,selectedCashOrigen,selectedCashDestiny, acctDestiny,glDestiny,glOrigin,null);

		}else 
			if(vars.commandIn("FIND")){
				String fFinancialAccountId = vars.getSessionValue("fFinancialAccountId");
				String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
						"AccountTransfer|DateFrom");
				String strDateTo = vars.getRequestGlobalVariable("inpDateTo",
						"AccountTransfer|DateTo");

				String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerIdParam",
						"AccountTransfer|C_BPartner_ID");

				String strPaymentMethod_ID = vars.getRequestGlobalVariable("inpPaymentMethodId",
						"AccountTransfer|FIN_PayementMethod_ID");


				String acctDestiny = vars.getRequestGlobalVariable("inpDestinyAccounId",
						"AccountTransfer|FIN_Account_ID");

				String glDestiny = vars.getRequestGlobalVariable("inpDestinyGlItemId",
						"AccountTransfer|GLItem_Destiny");

				String glOrigin = vars.getRequestGlobalVariable("inpOrigenGlItemId",
						"AccountTransfer|GLItem_Origin");
				String requiredPayMethod = vars.getRequestGlobalVariable("inpRequiredPaymentMethodId",
						"AccountTransfer|RequiredPaymentMethod");

				String selectedCashOrigen =  vars.getRequestGlobalVariable("inpCashAccounId","AccountTransfer|FIN_Acc_Cash_ID");
				String selectedCashDestiny =  vars.getRequestGlobalVariable("inpCashDestinyAccounId","AccountTransfer|FIN_Acc_Cash_ID2");
				FIN_FinancialAccount origenAccount = OBDal.getInstance().get(FIN_FinancialAccount.class, fFinancialAccountId);
				String selectedQuotations = vars.getInStringParameter("inpDepositId", IsIDFilter.instance);

				printPage(request,response,vars,origenAccount,requiredPayMethod,strC_BPartner_ID,strPaymentMethod_ID,strDateFrom,strDateTo,selectedCashOrigen,selectedCashDestiny, acctDestiny,glDestiny,glOrigin,selectedQuotations);


			}else

				if (vars.commandIn("generate")) {


					String strDateFrom = vars.getRequestGlobalVariable("inpDateFrom",
							"AccountTransfer|DateFrom");
					String strDateTo = vars.getRequestGlobalVariable("inpDateTo",
							"AccountTransfer|DateTo");

					String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerIdParam",
							"AccountTransfer|C_BPartner_ID");

					String strPaymentMethod_ID = vars.getRequestGlobalVariable("inpPaymentMethodId",
							"AccountTransfer|FIN_PayementMethod_ID");
					String fFinancialAccountId = vars.getGlobalVariable("", "fFinancialAccountId");
					String strWindow = vars.getGlobalVariable("", "inpwindowId");
					String strTab = vars.getGlobalVariable("", "inpTabId");

					String selectedDAccount = vars.getStringParameter("inpDestinyAccounId");
					String selectedOGlitem = vars.getStringParameter("inpOrigenGlItemId");
					String selectedDGlitem = vars.getStringParameter("inpDestinyGlItemId");

					String requiredPayMethod = "-";//vars.getRequiredStringParameter("inpRequiredPaymentMethodId");



					String selectedCashOrigen =  vars.getStringParameter("inpCashAccounId");
					String selectedCashDestiny =  vars.getStringParameter("inpCashDestinyAccounId");
					String strWindowPath = Utility.getTabURL(strTab, "R", true);
					if (strWindowPath.equals(""))
						strWindowPath = strDefaultServlet;

					FIN_FinancialAccount origenAccount = OBDal.getInstance().get(FIN_FinancialAccount.class, fFinancialAccountId);
					FIN_FinancialAccount destinoAccount = OBDal.getInstance().get(FIN_FinancialAccount.class, selectedDAccount);


					FIN_FinancialAccount cashOrigin = OBDal.getInstance().get(FIN_FinancialAccount.class, selectedCashOrigen);
					FIN_FinancialAccount cashDestiny = OBDal.getInstance().get(FIN_FinancialAccount.class, selectedCashDestiny);

					GLItem origenGlitem = OBDal.getInstance().get(GLItem.class, selectedOGlitem);
					GLItem destinoGlitem = OBDal.getInstance().get(GLItem.class, selectedDGlitem);
					//printPageDataSheet(response, vars);C_Order_ID

					String selectedQuotations = vars.getRequiredInStringParameter("inpDepositId", IsIDFilter.instance);



					if((!selectedQuotations.equalsIgnoreCase(""))&&(selectedQuotations != null)){
						ArrayList<String> transactionsIds = Utility.stringToArrayList(selectedQuotations.replaceAll("\\(|\\)|'", ""));
						BigDecimal totalAmount = new BigDecimal(0);
						BigDecimal partialAmount = new BigDecimal(0);

						String result = "";
						OBError myError = new OBError();
						boolean exit = true;
						String descripcion = "";
						for (String string : transactionsIds) {
							BigDecimal bigDecimal = getAmount(vars, string);
							FIN_FinaccTransaction trans =  OBDal.getInstance().get(FIN_FinaccTransaction.class, string);
							String related = "";
							String paymentMethod ="";
							String partner = "Unknown";
							
							partialAmount= partialAmount.add(bigDecimal);
							BigDecimal depositAmount =  trans.getDepositAmount();
							
							totalAmount = totalAmount.add(depositAmount);
							if(trans.getGLItem() != null){
								related = trans.getGLItem().getName();
								paymentMethod = "Unknown";
							}else if(trans.getFinPayment() != null){
								related = trans.getFinPayment().getDocumentNo();
								paymentMethod = trans.getFinPayment().getPaymentMethod().getName();
							}
								
							if(trans.getBusinessPartner() != null)
								partner = trans.getBusinessPartner().getName();
							

							
							//payment,bparter,cant,el metodo
							descripcion += "-" + related + ";" + partner + ";" + bigDecimal.toString() + ";" + paymentMethod; 
							try {



							} catch (Exception e) {
								myError.setType("Error");
								result = " There was an error in the generation. Error:"+e.getMessage();
								e.printStackTrace();
							}
						}    	

						try {
							List<String> listTransactions  = new ArrayList<String>();
							if(origenAccount.getCurrentBalance().compareTo(partialAmount) > -1){
								if(totalAmount.compareTo(partialAmount)==1){
									
									FIN_FinaccTransaction auxTrans = createTransaction(cashDestiny, totalAmount.subtract(partialAmount), true,destinoGlitem,requiredPayMethod,null,descripcion);
									listTransactions.add(auxTrans.getId());
									updateBalance(cashDestiny, totalAmount.subtract(partialAmount), true);
									
								}else if(totalAmount.compareTo(partialAmount)==-1){
									
									FIN_FinaccTransaction auxTrans = createTransaction(cashOrigin, partialAmount.subtract(totalAmount), false,origenGlitem,requiredPayMethod,null,descripcion);
									listTransactions.add(auxTrans.getId());
									updateBalance(cashOrigin, partialAmount.subtract(totalAmount), false);
									
								}
								
								BigDecimal finalAmnt = totalAmount;
								if(totalAmount.compareTo(partialAmount)!=0){

									FIN_FinaccTransaction auxTrans = createTransaction(origenAccount, totalAmount, false,origenGlitem,requiredPayMethod,destinoAccount,descripcion);
									FIN_FinaccTransaction auxTrans1 = createTransaction(destinoAccount, partialAmount, true,destinoGlitem,requiredPayMethod,origenAccount,descripcion);
									listTransactions.add(auxTrans.getId());
									listTransactions.add(auxTrans1.getId());
									updateBalance(origenAccount, totalAmount, false);
									updateBalance(destinoAccount, partialAmount, true);

								}else{
									
									FIN_FinaccTransaction auxTrans = createTransaction(origenAccount, totalAmount, false,origenGlitem,requiredPayMethod, destinoAccount,descripcion);
									FIN_FinaccTransaction auxTrans1 = createTransaction(destinoAccount, totalAmount, true,destinoGlitem,requiredPayMethod,origenAccount,descripcion);
									listTransactions.add(auxTrans.getId());
									listTransactions.add(auxTrans1.getId());
									updateBalance(origenAccount, totalAmount, false);
									updateBalance(destinoAccount, totalAmount, true);

								}
								for (String string : transactionsIds) {
									FIN_FinaccTransaction trans =  OBDal.getInstance().get(FIN_FinaccTransaction.class, string);
									updateTransaction(trans);
								}   
								result = "Transaction successfully";
								myError.setType("Sucess");

								printTicket(vars, response, transactionsIds, origenAccount, destinoAccount, cashOrigin, cashDestiny, origenGlitem, destinoGlitem,strWindowPath,listTransactions);
							}
							else{
								myError.setType("Error");
								result = "The transaction have been cancelled. Error: Insuficient funds";
								exit=false;
							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							myError.setType("Error");
							result = " There was an error in the generation. Error:"+e.getMessage();
							e.printStackTrace();
							exit=false;
						}
						myError.setMessage(result);
						vars.setMessage(strTab, myError);
						//	if(exit)
						//printPageClosePopUp(response, vars, strWindowPath);
						if(!exit)
							printPageResponse(request,response,vars,origenAccount,selectedDAccount,selectedOGlitem,selectedDGlitem,strC_BPartner_ID,strPaymentMethod_ID,strDateFrom,strDateTo);

					}else{

						printPageResponse(request,response,vars,origenAccount,selectedDAccount,selectedOGlitem,selectedDGlitem,strC_BPartner_ID,strPaymentMethod_ID,strDateFrom,strDateTo);
					}


				} else
					pageError(response);
	}

	private void clearSessionValue(VariablesSecureApp vars) {
		vars.removeSessionValue("fFinancialAccountId");
		vars.removeSessionValue("inpwindowId");
		vars.removeSessionValue("inpTabId");
	}
	private void printPage(HttpServletRequest request, HttpServletResponse response,
			VariablesSecureApp vars,FIN_FinancialAccount account, String requiredPayMethod,String strC_BPartner_ID, String Payment,String strDateFrom,String strDateto,String selectedCashOrigen,String selectedCashDestiny,	String acctDestiny,String glDestiny,String glOrigin,String selectedQuotations ) throws IOException, ServletException {
		// Check for permissions to apply modules from application server.


		//String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurnitures/services/process/AccountTransfer").createXmlDocument();
		xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
		xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
		xmlDocument.setParameter("theme", vars.getTheme());
		{
			final OBError myMessage = vars.getMessage("AccountTransfer");
			vars.removeMessage("AccountTransfer");
			if (myMessage != null) {
				xmlDocument.setParameter("messageType", myMessage.getType());
				xmlDocument.setParameter("messageTitle", myMessage.getTitle());
				xmlDocument.setParameter("messageMessage", myMessage.getMessage());
			}
		}

		xmlDocument.setParameter("realtxt", account.getName());
		xmlDocument.setParameter("balance", String.valueOf(account.getCurrentBalance()));
		xmlDocument.setParameter("dateFrom", strDateFrom);
		xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateTo", strDateto);
		xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("paramBPartnerId", strC_BPartner_ID);
		xmlDocument.setParameter("idpaymentmethod", Payment);
		BusinessPartner businessPartner =OBDal.getInstance().get(BusinessPartner.class, strC_BPartner_ID);
		if(businessPartner!=null)
			xmlDocument.setParameter("paramBPartnerDescription",businessPartner.getName() );

		SVFSVAccTransData[] destAccountsArr = SVFSVAccTransData.select(this,  Utility.getReferenceableOrg(vars, account.getOrganization().getId()), account.getId());
		SVFSVGLItemData[] glItemsArr = SVFSVGLItemData.selectGLItem(this, Utility.getReferenceableOrg(vars, account.getOrganization().getId()));

		String selectedProvider =acctDestiny;
		if(destAccountsArr.length > 0){
			if(selectedProvider.equalsIgnoreCase(""))
				selectedProvider = destAccountsArr[0].getField("id");
			xmlDocument.setParameter("addestaccid", selectedProvider);
		}
		xmlDocument.setData("reportC_Account_Id", "liststructure", destAccountsArr);

		String selectedglOitem = glOrigin;
		if(glItemsArr.length > 0){
			if(selectedglOitem.equalsIgnoreCase(""))
				selectedglOitem = DataSearchClass.getGLItemsDefaultOut();
			if(selectedglOitem.equalsIgnoreCase(""))
				selectedglOitem = glItemsArr[0].getField("id");
			xmlDocument.setParameter("adoglitemid", selectedglOitem);
		}
		xmlDocument.setData("reportC_OrigenGlItem_Id", "liststructure", glItemsArr);

		String selectedDitem = glDestiny;
		if(glItemsArr.length > 0){
			if(selectedDitem.equalsIgnoreCase(""))
				selectedDitem = DataSearchClass.getGLItemsDefaultIn();
			if(selectedDitem.equalsIgnoreCase(""))
				selectedDitem = glItemsArr[0].getField("id");
			xmlDocument.setParameter("addglitemid", selectedDitem);
		}
		xmlDocument.setData("reportC_DestinyGlItem_Id", "liststructure", glItemsArr);

		AccountCashData[] cashData = AccountCashData.select(this, Utility.getReferenceableOrg(vars, account.getOrganization().getId()));

		//List<FIN_FinancialAccount> listAccount = PaymentDataSearchClass.getBranchAccountList(this,account.getOrganization().getId());
		//String orgs = Utility.getReferenceableOrg(vars, account.getOrganization().getId());
		String selectedChash = selectedCashOrigen;
		if(cashData.length > 0){

			if(selectedChash.equalsIgnoreCase(""))
				selectedChash = cashData[0].getField("id");
			xmlDocument.setParameter("idorigen", selectedChash);
		}

		String selectedChash2 = selectedCashDestiny;
		if(cashData.length > 0){

			if(selectedChash2.equalsIgnoreCase(""))
				selectedChash2 = cashData[0].getField("id");
			xmlDocument.setParameter("iddestiny", selectedChash2);

		}

		SVFSVPaymentMethodData[] methodDatas = SVFSVPaymentMethodData.select(this, Utility.getReferenceableOrg(vars, account.getOrganization().getId()));

		xmlDocument.setData("reportC_Cash_Id", "liststructure", cashData); 
		xmlDocument.setData("reportC_CashDestiny_Id", "liststructure", cashData);
		xmlDocument.setData("reportC_PaymentMethod_Id", "liststructure", methodDatas);
		xmlDocument.setData("reportC_RequiredPaymentMethod_Id", "liststructure", methodDatas);
		String selectedRequiredPayMethod =requiredPayMethod;
		if(methodDatas.length > 0){

			if(selectedRequiredPayMethod.equalsIgnoreCase(""))
				selectedRequiredPayMethod = methodDatas[0].getField("id");
			xmlDocument.setParameter("idrequiredpaymentmethod", selectedRequiredPayMethod);

		}

		SVFSVFINFinaccTransactionData[] finaccTransactionDatas=null;
		if(vars.commandIn("FIND")){
			finaccTransactionDatas= SVFSVFINFinaccTransactionData.select(this, strC_BPartner_ID,
					Payment,strDateFrom, DateUtils.nDaysAfter(this, vars, strDateto, "1"),account.getId()
					);
		}

		List<FIN_FinaccTransaction> list = DataSearchClass.loadDepositTransaction(account.getId(), toStringArray(finaccTransactionDatas),vars.commandIn("FIND"));
		String table = generateTable(list,vars);		
		xmlDocument.setParameter("desc", table);

		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();
		String print ="";
		if(list!=null&& !list.isEmpty() &&  vars.commandIn("FIND") ){

			print= xmlDocument.print();
			print=replaceChecked(print, selectedQuotations );
		}else

			print= xmlDocument.print();
		out.println(print);

		out.close();
	}

	private String  replaceChecked(String print,String SalesOrder) {
		String[] salesId = splitString(SalesOrder);

		for (String id : salesId) {
			id=id.trim();
			print =print.replaceFirst(id, id+"\"  checked="+"\"checked"); 
			print=print.replaceFirst("inpPaymentAmount"+id+"\" class="+"\"dojoValidateValid TextBox_btn_OneCell_width number TextBox required readonly", "inpPaymentAmount"+id+"\" class="+"\"dojoValidateValid TextBox_btn_OneCell_width number TextBox required" );
		}


		return print;


	}

	private String[] splitString(String value) {

		value=value.replaceAll("\\(", "");
		value=value.replaceAll("\\)", "");

		value=value.replaceAll("'", "");
		value=value.trim();
		String[] valueArray = value.split(",");

		return valueArray;
	}
	private Collection<String> toStringArray(SVFSVFINFinaccTransactionData[] finaccTransactionDatas) {
		ArrayList<String> arrayList = new ArrayList<String>();
		if(finaccTransactionDatas!=null)
			for (int i = 0; i < finaccTransactionDatas.length; i++) {
				SVFSVFINFinaccTransactionData svfsvfinFinaccTransactionData = finaccTransactionDatas[i];
				arrayList.add(svfsvfinFinaccTransactionData.finFinaccTransactionId);
			}

		return arrayList;

	}

	private void printPageResponse(HttpServletRequest request, HttpServletResponse response,
			VariablesSecureApp vars,FIN_FinancialAccount account,String selectedDAccount,String selectedOGlitem,String selectedDGlitem,String strC_BPartner_ID, String Payment,String strDateFrom,String strDateto) throws IOException, ServletException {
		// Check for permissions to apply modules from application server.


		final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurnitures/services/process/AccountTransfer").createXmlDocument();
		xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
		xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
		xmlDocument.setParameter("theme", vars.getTheme());
		{
			final OBError myMessage = vars.getMessage("AccountTransfer");
			vars.removeMessage("AccountTransfer");
			if (myMessage != null) {
				xmlDocument.setParameter("messageType", myMessage.getType());
				xmlDocument.setParameter("messageTitle", myMessage.getTitle());
				xmlDocument.setParameter("messageMessage", myMessage.getMessage());
			}
		}

		xmlDocument.setParameter("realtxt", account.getName());
		xmlDocument.setParameter("dateFrom", strDateFrom);
		xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateTo", strDateto);
		xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
		xmlDocument.setParameter("paramBPartnerId", strC_BPartner_ID);
		xmlDocument.setParameter("idpaymentmethod", Payment);
		BusinessPartner businessPartner =OBDal.getInstance().get(BusinessPartner.class, strC_BPartner_ID);
		if(businessPartner!=null)
			xmlDocument.setParameter("paramBPartnerDescription",businessPartner.getName() );
		SVFSVAccTransData[] destAccountsArr = SVFSVAccTransData.select(this,  Utility.getReferenceableOrg(vars, account.getOrganization().getId()), account.getId());
		SVFSVGLItemData[] glItemsArr = SVFSVGLItemData.selectGLItem(this, Utility.getReferenceableOrg(vars, account.getOrganization().getId()));

		xmlDocument.setParameter("addestaccid", selectedDAccount);
		xmlDocument.setData("reportC_Account_Id", "liststructure", destAccountsArr);

		xmlDocument.setParameter("adoglitemid", selectedOGlitem);
		xmlDocument.setData("reportC_OrigenGlItem_Id", "liststructure", glItemsArr);

		xmlDocument.setParameter("addglitemid", selectedDGlitem);
		xmlDocument.setData("reportC_DestinyGlItem_Id", "liststructure", glItemsArr);
		SVFSVFINFinaccTransactionData[] finaccTransactionDatas=null;
		if(vars.commandIn("FIND")){
			finaccTransactionDatas= SVFSVFINFinaccTransactionData.select(this, strC_BPartner_ID,
					Payment,strDateFrom, DateUtils.nDaysAfter(this, vars, strDateto, "1"),account.getId()
					);
		}

		List<FIN_FinaccTransaction> list = DataSearchClass.loadDepositTransaction(account.getId(), toStringArray(finaccTransactionDatas),vars.commandIn("FIND"));
		String table = generateTable(list,vars);		
		xmlDocument.setParameter("desc", table);

		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();



		out.println(xmlDocument.print());
		out.close();
	}


	private String generateTable(List<FIN_FinaccTransaction> list,VariablesSecureApp vars){

		String generalRows = new String();
		if(list.size() == 0){
			generalRows = " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_0\">                              "+
					" 	<td class=\"DataGrid_Body_LineNoCell\">                                                             "+
					" 		<span class=\"Checkbox_container_NOT_Focused\">                                                 "+
					" 			<input type=\"checkbox\" value=\"Unknown\" name=\"inpDepositId\"></input>   "+
					" 		</span>                                                                                       "+
					" 	</td>                                                                                             "+
					" 	<td class=\"DataGrid_Body_Cell\">There is not related data.</td>                                     "+
					" 	<td class=\"DataGrid_Body_Cell\"></td>                                                             "+
					" 	<td class=\"DataGrid_Body_Cell\"></td>                                           "+
					" </tr>                                                                                               ";
		}
		else{
			int pos = 0;
			for (FIN_FinaccTransaction transData : list) {
				String bp = "&nbsp";
				String glItem = "&nbsp";
				String paymentMethod = "Unknown";
				SimpleDateFormat dateFormat = (SimpleDateFormat) Utility.getDateFormatter(vars);

				if(transData.getFinPayment()!=null){
					if(transData.getFinPayment().getPaymentMethod()!= null){
						paymentMethod = transData.getFinPayment().getPaymentMethod().getName();
					}
				}
				if(transData.getBusinessPartner() != null)
					bp = transData.getBusinessPartner().getName();
				if(transData.getGLItem() != null)
					glItem = transData.getGLItem().getName(); 
				generalRows +=  " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_"+ String.valueOf(pos) +"\">                              "+
						" 	<td class=\"DataGrid_Body_LineNoCell\">                                                             "+
						" 		<span class=\"Checkbox_container_NOT_Focused\">                                                 "+
						" 			<input type=\"checkbox\" value=\"" + transData.getId() + "\" name=\"inpDepositId\" onclick=\"updateReadOnly2(this.value, this.checked);sum(this);return true;\"></input>   "+
						" 			<input type=\"hidden\" value=\"" + transData.getDepositAmount().toString() + "\" name=\"" + transData.getId() + "\" id=\"" + transData.getId() + "\"></input>   "+
						" 		</span>                                                                                       "+
						" 	</td>                                                                                             "+
						" 	<td class=\"DataGrid_Body_Cell\">" + transData.getTransactionType()+ "</td>                                     "+
						" 	<td class=\"DataGrid_Body_Cell\">" + transData.getDescription() + "</td>                                                             "+
						" 	<td class=\"DataGrid_Body_Cell\">" + dateFormat.format(transData.getCreationDate()) + "</td>                                           "+
						" 	<td class=\"DataGrid_Body_Cell\">" + bp + "</td>                                     "+
						" 	<td class=\"DataGrid_Body_Cell\">" + glItem + "</td>                                                             "+
						" 	<td class=\"DataGrid_Body_Cell\">" + paymentMethod+ "</td>                                           "+
						" 	<td class=\"DataGrid_Body_Cell\" style=\"text-align: right\">" + transData.getDepositAmount() + "</td>  "+
						"                                                                                                " + "<td class=\"DataGrid_Body_Cell_Amount\">" +
						" <input type=text style=\"width:100px\" onkeypress=\"return digits(this, event, true, false);\"  name=\"inpPaymentAmount"+transData.getId()+"\" class=\"dojoValidateValid TextBox_btn_OneCell_width number TextBox required readonly\" maxlength=\"32\" value=\""+transData.getDepositAmount()+"\" "+

		                  "id=\"inpPaymentAmount\""+

		                 "onfocus=\"numberInputEvent('onfocus', this);\" onblur=\"numberInputEvent('onblur', this);\" onkeydown=\"numberInputEvent('onkeydown', this, event);\" "+
		                 "onchange=\"numberInputEvent('onchange',this);\" /></td> </tr>"; 
				pos++;
			}
		}

		String generalTable = " <table class=\"DataGrid_Header_Table DataGrid_Body_Table\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">                                             "+
				"     <tbody>                                                                                                                                        "+
				"         <tr class=\"Popup_Client_Selector_DataGrid_HeaderRow\">                                                                                      "+
				"             <td class=\"DataGrid_Body_LineNoCell\" width=\"28\">                                                                                       "+
				"                <span class=\"Checkbox_container_NOT_Focused\">                                                                                      "+
				"                     <input type=\"checkbox\" onclick=\"markAll(document.frmMain.inpDepositId, this.checked);checkAll();return true;\" name=\"inpTodos\"></input>         "+
				"                 </span>                                                                                                                            "+
				"             </td>                                                                                                                                  "+
				"             <th class=\"DataGrid_Header_Cell\" width=\"40\">Type</th>                                                                             "+
				" 			<th class=\"DataGrid_Header_Cell\" width=\"220\">Description</th>                                                                                  "+
				"             <th class=\"DataGrid_Header_Cell\" width=\"70\">Date</th>                                                                                "+
				"             <th class=\"DataGrid_Header_Cell\" width=\"170\">Busisness Partner</th>                                                                             "+
				" 			<th class=\"DataGrid_Header_Cell\" width=\"110\">GlItem</th>                                                                                  "+
				" 			<th class=\"DataGrid_Header_Cell\" width=\"100\">Payment Method</th>                                                                                  "+
				"             <th class=\"DataGrid_Header_Cell\" width=\"100\" style=\"align:right\">Deposit</th>                                                    "+
				"<th class=\"DataGrid_Header_Cell\" width=\"100\" style=\"align:right\">Amount</th> "+
				"         </tr>                                                                                                                                      "+
				generalRows + 
				"     </tbody>                                                                                                                                       "+
				" </table>                                                                                                                                           ";
		return generalTable;

	}



	private static FIN_FinaccTransaction saveFinFinaccTransaction(FIN_FinaccTransaction order) throws Exception
	{
		OBContext.setAdminMode();

		try{

			OBDal.getInstance().save(order);
			OBDal.getInstance().flush();
			OBDal.getInstance().refresh(order);		   
		}
		catch(Exception ex){
			ex.printStackTrace();
			order = null;
		}
		finally{
			OBContext.restorePreviousMode();
		}

		return order;
	}

	private static FIN_FinancialAccount saveFinFinacialAccount(FIN_FinancialAccount order) throws Exception
	{
		OBContext.setAdminMode();

		try{

			OBDal.getInstance().save(order);
			OBDal.getInstance().flush();
			OBDal.getInstance().refresh(order);		   
		}
		catch(Exception ex){
			ex.printStackTrace();
			order = null;
		}
		finally{
			OBContext.restorePreviousMode();
		}

		return order;
	}

	private FIN_FinaccTransaction createTransaction(FIN_FinancialAccount account,BigDecimal amount,boolean deposit,GLItem objGlitem,String requiredPayMethod,FIN_FinancialAccount other,String description) throws Exception{


		FIN_PaymentMethod paymentMethod = OBDal.getInstance().get(FIN_PaymentMethod.class, requiredPayMethod);
		FIN_FinaccTransaction objFin_FinaccTransaction = new FIN_FinaccTransaction();
		objFin_FinaccTransaction.setAccount(account);
		objFin_FinaccTransaction.setActive(true);
		objFin_FinaccTransaction.setBusinessPartner(account.getBusinessPartner());
		objFin_FinaccTransaction.setCurrency(account.getCurrency());
		objFin_FinaccTransaction.setLineNo(DataSearchClass.getNextLine(this, account.getId()));
		objFin_FinaccTransaction.setDateAcct(new Date());
		objFin_FinaccTransaction.setStatus("RPPC");
		objFin_FinaccTransaction.setProcessed(true);
		objFin_FinaccTransaction.setProcessNow(false);
		objFin_FinaccTransaction.setPosted("Y");
		objFin_FinaccTransaction.setTransactionDate(new Date());
		objFin_FinaccTransaction.setCreatedByAlgorithm(false);
		objFin_FinaccTransaction.setGLItem(objGlitem);
		objFin_FinaccTransaction.setSvfsvActive(false);
		objFin_FinaccTransaction.setSvfsvTransactionlog(description);


		//Attachment attachment = OBProvider.getInstance().get(Attachment.class);

		if(deposit){
			objFin_FinaccTransaction.setDepositAmount(amount);
			//objFin_FinaccTransaction.setSvfsvActive(deposit);
			objFin_FinaccTransaction.setDescription("Deposit-Account:" + account.getName()+(other!=null? (" From: " + other.getIdentifier() +" Amount: " + amount):"") );
		}
		else{
			objFin_FinaccTransaction.setPaymentAmount(amount);
			//objFin_FinaccTransaction.setSvfsvActive(deposit);
			objFin_FinaccTransaction.setDescription("Payment-Account:" + account.getName() +(other!=null? (" To: "+ other.getIdentifier() +" Amount: " + amount):"" ));
		}

		return saveFinFinaccTransaction(objFin_FinaccTransaction);
	}

	private void updateBalance(FIN_FinancialAccount account,BigDecimal amount,boolean deposit) throws Exception{

		if(deposit){
			account.setCurrentBalance(account.getCurrentBalance().add(amount));;
		}
		else{
			account.setCurrentBalance(account.getCurrentBalance().subtract(amount));;
		}

		saveFinFinacialAccount(account);
	}

	private void updateTransaction(FIN_FinaccTransaction transaction) throws Exception{

		OBContext.setAdminMode();

		try{
			transaction.setSvfsvActive(false);
			OBDal.getInstance().save(transaction);
			OBDal.getInstance().flush();
			OBDal.getInstance().refresh(transaction);		   
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			OBContext.restorePreviousMode();
		}
	}

	private OBError handleException(String msg, VariablesSecureApp vars) {
		String title = Utility.messageBD(this, "Error", vars.getLanguage());
		String message = Utility.messageBD(this, msg, vars.getLanguage());
		if (message == null || message.isEmpty()) {
			message = Utility.messageBD(this, "ErrorInExtensionPoint", vars.getLanguage());
		}
		OBError err = new OBError();

		err.setTitle(title);
		err.setMessage(message);
		err.setType("ERROR");
		return err;
	}

	private OBError handleException(Exception e, VariablesSecureApp vars) {
		return handleException(e.getMessage(), vars);
	}

	public String getServletInfo() {
		return "GenerateInvoicesmanual Servlet. This Servlet was made by Pablo Sarobe";
	} // end of getServletInfo() method


	private BigDecimal getAmount(VariablesSecureApp vars,String id) throws ServletException {

		return getSelectedBaseOBObjectAmount(vars, id, "inpPaymentAmount");



	}

	public  static BigDecimal  getSelectedBaseOBObjectAmount(
			VariablesSecureApp vars, String o, String htmlElementId)
					throws ServletException {


		BigDecimal decimal = new  BigDecimal(vars.getRequiredNumericParameter(htmlElementId + (String) o, ""));


		return decimal;

	}


	@SuppressWarnings("deprecation")
	private void printTicket(VariablesSecureApp vars, HttpServletResponse response, ArrayList<String> transactionsIds,FIN_FinancialAccount origenAccount ,
			FIN_FinancialAccount destinoAccount,FIN_FinancialAccount cashOrigin,FIN_FinancialAccount cashDestiny,GLItem origenGlitem,GLItem destinoGlitem, String path,List<String> transactions) throws JRException, IOException, ServletException {
		

	
		String strAttach = globalParameters.strFTPDirectory;
		Map<String, String> map = totalDepositAndAmount(vars, transactionsIds);



		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("originaccount", origenAccount.getName());
		parametros.put("org", OBContext.getOBContext().getCurrentClient().getName());
		parametros.put("destinyaccount", destinoAccount.getName());
		parametros.put("origingl", origenGlitem.getName());
		parametros.put("destinygl", destinoGlitem.getName());
		parametros.put("origincash", (cashOrigin!=null? cashOrigin.getName():""));
		parametros.put("destinycash", (cashDestiny!=null ?cashDestiny.getName():""));
		parametros.put("balance", origenAccount.getCurrentBalance().toString());
		parametros.put("totaldeposit",map.get("total"));
		parametros.put("totalamnt", map.get("amount"));
		Client current = OBContext.getOBContext().getCurrentClient();
		if(current.getClientInformationList()!=null &&!current.getClientInformationList().isEmpty() && current.getClientInformationList().get(0)!=null ){

			ClientInformation clientInformation	= current.getClientInformationList().get(0);
			clientInformation = OBDal.getInstance().get(ClientInformation.class, clientInformation.getId());
			Hibernate.initialize(clientInformation);
			org.openbravo.model.ad.utility.Image log= clientInformation.getYourCompanyBigImage();
			if(log!=null){
				byte[]  imageInByte = log.getBindaryData();

				InputStream in = new ByteArrayInputStream(imageInByte);
				BufferedImage bImageFromConvert = ImageIO.read(in);

				parametros.put("image", bImageFromConvert);
			}

		}

		AccTransactionDatasource datasource = new AccTransactionDatasource();
		datasource.setTransactionIds(transactionsIds);
		datasource.setVars(vars);
		String reportName = Utility.getDateFormatter(vars).format(new Date())+"-vigaccounttransffer-to-"+destinoAccount.getName()+"-amount-"+map.get("amount")+".pdf";

		JasperReport reporte =(JasperReport) JRLoader.loadObjectFromFile(getServletContext().getRealPath("WEB-INF/classes/com/spocsys/vigfurnitures/services/process/Report_VigAccountTransfer.jasper"));
		String filePath = strAttach+File.separator+reportName; 
		JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, parametros, datasource);
		
		final ReportManager reportManager = new ReportManager(this, globalParameters.strFTPDirectory,
		        strReplaceWithFull, globalParameters.strBaseDesignPath,
		        globalParameters.strDefaultDesignPath, globalParameters.prefix, false);
		
		JRExporter exporter = new JRPdfExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition","attachment; filename=\""+reportName);
		File file =new File(filePath);

		exporter.setParameter(JRExporterParameter.OUTPUT_FILE,file);//exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
		exporter.exportReport();
		log4j.info("Luego de exportar");
		attachmentLogger.logp(Level.INFO, AccountTransfer.class.getName(), "printTicket", "Luego de exportar");
		
		if(file.exists()){
			log4j.info("Si el fichero existe");
			attachmentLogger.logp(Level.INFO, AccountTransfer.class.getName(), "printTicket", "Si el fichero existe");
			for (String string : transactions) {
				CreateAttachment(reportName, string);
			}
		}
		FileInputStream fileInputStream = new FileInputStream(filePath);

		OutputStream responseOutputStream = response.getOutputStream();
		int bytes;
		while ((bytes = fileInputStream.read()) != -1) {
			responseOutputStream.write(bytes);
		}

		responseOutputStream.close();
		response.flushBuffer();




	}
	
	private void CreateAttachment(String reportName,String transactionId){
		OBContext.getOBContext().setAdminMode(true);
		Attachment attachment = OBProvider.getInstance().get(Attachment.class);
		attachment.setName(reportName);
		attachment.setPath("/");
		log4j.info("Crea el attachment: " + reportName);
		attachmentLogger.logp(Level.INFO, AccountTransfer.class.getName(), "CreateAttachment", "Crea el attachment: " + reportName);
		attachment.setRecord(transactionId);
		log4j.info("Add record :" + transactionId);
		attachmentLogger.logp(Level.INFO, AccountTransfer.class.getName(), "CreateAttachment", "Add record :" + transactionId);
		OBCriteria<Table> criteria = OBDal.getInstance().createCriteria(Table.class);
		criteria.add(Restrictions.eq(Table.PROPERTY_NAME, "FIN_Finacc_Transaction"));
		Table table = (Table) criteria.uniqueResult();
		
		if(table!=null){
			log4j.info("Get Table : " + table.getName());
			attachmentLogger.logp(Level.INFO, AccountTransfer.class.getName(), "CreateAttachment", "Get Table : " + table.getName());
			attachment.setTable(table);
			OBCriteria<Attachment> criteriaAtt = OBDal.getInstance().createCriteria(Attachment.class);
			criteriaAtt.add(Restrictions.eq(Attachment.PROPERTY_RECORD, transactionId));
			criteriaAtt.addOrder(Order.desc(Attachment.PROPERTY_SEQUENCENUMBER));

			Long long1 = ( criteriaAtt.list()!=null&& !criteriaAtt.list().isEmpty()&& (Attachment)criteriaAtt.list().get(0) != null) ?((Attachment)criteriaAtt.list().get(0)).getSequenceNumber(): null ;
			if(long1==null || long1.compareTo(0L)==0)
				long1=10L;
			else
				long1=long1+10L;

			attachment.setSequenceNumber(long1);
			log4j.info("Attachment Creado : " + attachment.getIdentifier() + " camino : " + attachment.getPath());
			attachmentLogger.logp(Level.INFO, AccountTransfer.class.getName(), "CreateAttachment", "Attachment Creado : " + attachment.getIdentifier() + " camino : " + attachment.getPath());

			OBDal.getInstance().save(attachment);
			OBDal.getInstance().flush();
			OBContext.getOBContext().restorePreviousMode();
			 log4j.info( "Attachment Salvado : " + attachment.getIdentifier() + " camino : " + attachment.getPath());
			attachmentLogger.logp(Level.INFO, AccountTransfer.class.getName(), "CreateAttachment", "Attachment Salvado : " + attachment.getIdentifier() + " camino : " + attachment.getPath());
		}

	}

	private Map totalDepositAndAmount(VariablesSecureApp vars,ArrayList<String> transactionsIds) throws ServletException {
		BigDecimal total=new BigDecimal(0);
		BigDecimal amount=new BigDecimal(0);
		Map<String, String> map = new HashMap<String, String>();
		for (String string : transactionsIds) {
			FIN_FinaccTransaction finaccTransaction  = OBDal.getInstance().get(FIN_FinaccTransaction.class, string);

			total=total.add(finaccTransaction.getDepositAmount());
			amount=amount.add(getAmount( vars, string));
		}

		map.put("total", total.toString());
		map.put("amount", amount.toString());
		return map;
	}



}
