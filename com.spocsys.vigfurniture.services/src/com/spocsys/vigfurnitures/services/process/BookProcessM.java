/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2012 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.process.ProcessInstance;
import org.openbravo.model.common.order.Order;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.vigfurniture.productavailability.erpCommon.ad_callouts.ProductAvailabilityProcess;
import com.spocsys.vigfurnitures.services.utils.DataSearchClass;
import com.spocsys.vigfurnitures.services.utils.PDFieldProvider;

public class BookProcessM extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    
    if (vars.commandIn("DEFAULT")) {
    	
    	clearSessionValue(vars);
    	String cOrderId = vars.getStringParameter("C_Order_ID");
    	String processId = vars.getStringParameter("inpProcessId");
    	String strWindow = vars.getStringParameter("inpwindowId");
    	String strTab = vars.getStringParameter("inpTabId");
    	vars.setSessionValue("inpwindowId", strWindow);
    	vars.setSessionValue("inpTabId", strTab);
    	vars.setSessionValue("inpProcessId", processId);
    	vars.setSessionValue("cOrderId", cOrderId);
    	
   		printPage(request,response,vars,cOrderId);
    	
    }else if (vars.commandIn("generate")) {
    	
    	String strTab = vars.getGlobalVariable("", "inpTabId");
    	String cOrderId = vars.getGlobalVariable("", "cOrderId");
    	String processId = vars.getGlobalVariable("", "inpProcessId");
    	String selectedAction = vars.getStringParameter("inpAction");
    	String strWindowPath = Utility.getTabURL(strTab, "R", true);
    	if (strWindowPath.equals(""))
            strWindowPath = strDefaultServlet;

    	String userId = vars.getUser();
    	

    	String result = "";
    	OBError myError = new OBError();
    	myError.setType("Info");
		result = "Process finished";
		
    	try{
    	DataSearchClass.updateOrderAction(this,cOrderId, selectedAction);
    	Order order = OBDal.getInstance().get(Order.class, cOrderId);
    	
    	//Check availability process
    	try{
	    	if(selectedAction.equalsIgnoreCase("CO")){
				   ProductAvailabilityProcess PAvailability = new ProductAvailabilityProcess();
				     JSONObject Result = PAvailability.CheckAvailabilityProcess(order);
				      if(Result==null)
				         throw new OBException( "Error cheking availability");
				      
			}
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	
    	
    	
    	String pInstance = execute(order, processId,null,userId);
    	String pInstanceResult = DataSearchClass.getPinstanceData(this, pInstance);
    	
    	if(pInstanceResult != ""){
    		String resultCode = pInstanceResult.substring(0,pInstanceResult.indexOf("|"));
	    	if(resultCode.equalsIgnoreCase("0")){
	    		String resultMsg = pInstanceResult.substring(pInstanceResult.indexOf("|")+1);
	    		if(resultMsg.contains("ERROR=")){
	    			resultMsg = resultMsg.substring(1);
	    			String[] arr = resultMsg.split("\\@");
	    			String message = resultMsg;
	    			if(arr.length > 0)
	    				message = DataSearchClass.getMessage(this, arr[1]);
	    			if(!message.equalsIgnoreCase(""))
	    				result = message;
	    			else
	    				result = resultMsg;
	    		}
	    		myError.setType("Error");
	    	}
	    	/*else{
	    		Here was the check availability process until the client ask to be executed before the book process
	    	}*/
    	}
		
    	}catch(Exception ex){
    		myError.setType("Error");
    		result = "Error detected:"+ex.getMessage();
    		ex.printStackTrace();
    	}
    	myError.setMessage(result);
        vars.setMessage(strTab, myError);
    	printPageClosePopUp(response, vars, strWindowPath);

    } else
      pageError(response);
  }
  
  private void clearSessionValue(VariablesSecureApp vars) {
	    vars.removeSessionValue("inpwindowId");
	    vars.removeSessionValue("inpTabId");
	    vars.removeSessionValue("cOrderId");
	    vars.removeSessionValue("inpProcessId");
	  }
  
  private String getProcessId(VariablesSecureApp vars) throws ServletException {
	    String command = vars.getCommand();
	    if (command.equals("DEFAULT")) {
	      return vars.getRequiredStringParameter("inpadProcessId");
	    } else if (command.startsWith("BUTTON")) {
	      return command.substring("BUTTON".length());
	    } else if (command.startsWith("FRAMES")) {
	      return command.substring("FRAMES".length());
	    } else if (command.startsWith("SAVE_BUTTONActionButton")) {
	      return command.substring("SAVE_BUTTONActionButton".length());
	    }
	    return null;
	  }
  
  private String execute(Order order, String processId,String pInstance,String userId) throws Exception{	    	
	  if(pInstance == null){
		  String orderId = order.getId();
		  String adOrgId = DataSearchClass.getfield(this, orderId, "ad_org_id");
		  String adClientId = DataSearchClass.getfield(this, orderId, "ad_client_id");
		  if((adOrgId == null) ||(adOrgId.equalsIgnoreCase(""))){
			  throw new Exception("The Selected order has not selected organization"); 
		  }
		  if((adClientId == null) ||(adClientId.equalsIgnoreCase(""))){
			  throw new Exception("The Selected order has not selected client"); 
		  }
		  pInstance = DataSearchClass.saveProcessInstance(this, adOrgId,adClientId, processId,orderId, userId);
	  }
	  DataSearchClass.bookProcess(this, pInstance);
	  return pInstance;
  }
  
  private static void updateOrderAction(Order order,String docAction) throws Exception{
		OBContext.setAdminMode();
		try{
			//Salvando nuevo Price List Version
	        order.setDocumentAction(docAction);
	      
	        OBDal.getInstance().save(order);
	        OBDal.getInstance().flush();
	        OBDal.getInstance().refresh(order);
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
		    OBContext.restorePreviousMode();
		}
	}
  private void printPage(HttpServletRequest request, HttpServletResponse response,
	      VariablesSecureApp vars,String orderId) throws IOException, ServletException {
	    // Check for permissions to apply modules from application server.
	    
	    String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	    final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurnitures/services/process/BookProcessM").createXmlDocument();
	    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
	    xmlDocument.setParameter("theme", vars.getTheme());
	    {
	      final OBError myMessage = vars.getMessage("BookProcessM");
	      vars.removeMessage("AccountTransfer");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	    }
	    
	    Order order = OBDal.getInstance().get(Order.class, orderId); 
	    
	    PDFieldProvider[] actionsArr = getActions(order.getDocumentAction());
		    
	    String selectedAction = new String();
		if(actionsArr.length > 0){
			selectedAction = actionsArr[0].getField("id");
			xmlDocument.setParameter("addAction", selectedAction);
		}
		xmlDocument.setData("reportAction", "liststructure", actionsArr);
		
	    response.setContentType("text/html; charset=UTF-8");
	    final PrintWriter out = response.getWriter();

	    out.println(xmlDocument.print());
	    out.close();
	  }
  
  
  
  public static PDFieldProvider[] getActions(String status)
	{
		if(status.equalsIgnoreCase("CO")||status.equalsIgnoreCase("DR")){
			PDFieldProvider[] pdValues = new PDFieldProvider[1];
			pdValues[0] = new PDFieldProvider("NULL", "Book", "CO");
			return pdValues;
		}
		else{
			PDFieldProvider[] pdValues = new PDFieldProvider[2];
			pdValues[0] = new PDFieldProvider("NULL", "Close", "CL");
			pdValues[1] = new PDFieldProvider("NULL", "Reactivate", "RE");
			return pdValues;
		}
	}
  
  private OBError handleException(String msg, VariablesSecureApp vars) {
    String title = Utility.messageBD(this, "Error", vars.getLanguage());
    String message = Utility.messageBD(this, msg, vars.getLanguage());
    if (message == null || message.isEmpty()) {
      message = Utility.messageBD(this, "ErrorInExtensionPoint", vars.getLanguage());
    }
    OBError err = new OBError();

    err.setTitle(title);
    err.setMessage(message);
    err.setType("ERROR");
    return err;
  }

  private OBError handleException(Exception e, VariablesSecureApp vars) {
    return handleException(e.getMessage(), vars);
  }

  public String getServletInfo() {
    return "GenerateInvoicesmanual Servlet. This Servlet was made by Pablo Sarobe";
  } // end of getServletInfo() method
}
