/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2012-2013 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;


import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import com.spocsys.vigfurnitures.services.utils.LineData;


public class CheckBomProcess extends DalBaseProcess  {

  
 

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {
	  // TODO Auto-generated method stub
	  
	  	OBError myMessage = null;
		myMessage = new OBError();
		myMessage.setTitle("");
		
		// Create Sales Order
	    String cOrderId = (String) bundle.getParams().get("C_Order_ID");
	    Order order = OBDal.getInstance().get(Order.class, cOrderId);
	    List<ShipmentInOut> shipments = getAsociatedShipments(order);
	    List<LineData> listProducts = getProductsToProcess(order.getOrderLineList());
	    Long boxQty = order.getSvfsvBox();
	    
	    if(shipments.size() > 0){
	    	
	    	OBError msg = new OBError();
	        msg.setType("Error");
	        msg.setMessage("The selected order has associated shipments");
	        bundle.setResult(msg);
	        OBDal.getInstance().rollbackAndClose();
	        return;
	    }    
	    
	    /*if(boxQty <= 0){
	    	
	    	OBError msg = new OBError();
	        msg.setType("Error");
	        msg.setMessage("The selected order has cero box quantity set");
	        bundle.setResult(msg);
	        OBDal.getInstance().rollbackAndClose();
	        return;
	    }   */ 
	    
	    if(listProducts.size() == 0){
	    	
	    	OBError msg = new OBError();
	        msg.setType("Error");
	        msg.setMessage("The selected order has no special products");
	        bundle.setResult(msg);
	        OBDal.getInstance().rollbackAndClose();
	        return;
	    }    
	    
	     
	    try{
	    	
	    	String result = executeProcess(bundle.getConnection(),order,listProducts);
	    	if(result == "")
	    		result = "Proceso terminado exitosamente";
	    	OBError msg = new OBError();
	        msg.setType("info");;	        		     
	        msg.setMessage(result);
	        bundle.setResult(msg);
	    }
	    catch(Exception ex){
	    	OBError msg = new OBError();
	        msg.setType("Error");
	        msg.setMessage("Se ha detectado el error:"+ex.getMessage());
	        bundle.setResult(msg);
	        OBDal.getInstance().rollbackAndClose();
	        return;
		  }
  }
  
  private String executeProcess(org.openbravo.database.ConnectionProvider connectionProvider,Order order,List<LineData> listProducts) throws ServletException{
	  
	  //Long boxQty = order.getSvfsvBox();
	  String docNo = order.getDocumentNo();
	  String result = "";

	  for (LineData lineData : listProducts) {
		  BigDecimal cant = lineData.getBox();
		  Product prod = lineData.getProduct();
		  if(cant.longValue() > 0){ 
			  executeUpdate(connectionProvider,prod,cant.longValue(),docNo,lineData.getmAttibuteSetInstanceID());
		  	  result += "-Process executed for product:" + prod.getName() + " with id:" + prod.getId() + "<br/>";
		  	  prod.setSvfsvProductCharact("HK");
		  }else{
			  result += "-Process NOT executed for product:" + prod.getName() + " with id:" + prod.getId() + " line box quantity equals 0<br/>";
		  }
	  }
	  
	  return result;

  }
  
  private void executeUpdate(org.openbravo.database.ConnectionProvider connectionProvider,Product product,Long boxQty,String docNo,String lineMatributeSetInstanceID) throws ServletException{
	  
	  if(!product.isBillOfMaterials()){
		  product.setBillOfMaterials(true);
		  product = updateProduct(connectionProvider,product.getId()," isBOM = 'Y' ");
	  }
	  if(lineMatributeSetInstanceID != "")
		  product = updateProduct(connectionProvider,product.getId()," m_attributesetinstance_id = '" + lineMatributeSetInstanceID + "' ");
	  //Obtener listado de BOM relacionados
	  List<ProductBOM> listPB = product.getProductBOMList();
	  
	  if((listPB != null)||(listPB.size() > 0)){
		  //Si tiene listado de BOM y la cantidad es menor que la cant de BOX
		  if(listPB.size() < boxQty){
			  //Actualizo productBOM y product
			  for(int i = 0;i<listPB.size();i++){
				  long line = (i+1)*10;
				  ProductBOM pb = listPB.get(i);
				  Product auxProduct = pb.getBOMProduct();
				  String productName = createNewProductName(auxProduct.getName(),i,boxQty);
				  pb.setLineNo(line);
				  auxProduct.setName(productName);
				  pb = updateProductBOM(connectionProvider,pb.getId()," line = " + String.valueOf(line));
				  product = updateProduct(connectionProvider,product.getId()," name = '" + productName + "'");
			  }
			  //creo los que faltan
			  duplicateProcess(connectionProvider,product, listPB.size(), boxQty,docNo)  ;
		  }
	  }
	  
  }
  
  private String createNewProductName(String currentName,int index,long total){
	  String productName = currentName;
	  int sufixSize = currentName.lastIndexOf("-");
	  if(sufixSize > 0){
		  productName = currentName.substring(0, currentName.lastIndexOf("-"));
	  }
	  
	  productName = productName + "-" + String.valueOf(index+1) + String.valueOf(total);
	  return productName;
  }
  
  private void duplicateProcess(org.openbravo.database.ConnectionProvider connectionProvider,Product product, int startPoint, long cant,String docNo) throws ServletException{
	  for(int i = startPoint; i < cant ; i++){
		 String productName = createNewProductName(product.getName(),i,cant);
		 //Duplico el producto
		 String newProductId = duplicateProduct(connectionProvider,product.getId(),productName,docNo);
		 //Creo el Bom relacionado con el producto nuevo 
		 createProductBOM(connectionProvider, product, newProductId,(i+1)*10);
	  }
  }
  
  private List<LineData> getProductsToProcess(List<OrderLine> listLines){
	  List<LineData> listProduct = new ArrayList<LineData>();
	  for (OrderLine orderLine : listLines) {
		Product product = OBDal.getInstance().get(Product.class, orderLine.getProduct().getId());
		String mAttribuSetInstanceId = "";
		if(product.isSvfsvSpecialOrder()){
			LineData objData = new LineData();
			AttributeSetInstance attribute = orderLine.getAttributeSetValue();
			if(attribute != null)
				mAttribuSetInstanceId = attribute.getId();
			objData.setProduct(product);
			objData.setBox(orderLine.getSvfsvBox());
			objData.setmAttibuteSetInstanceID(mAttribuSetInstanceId);
			listProduct.add(objData);
		}
	  }
	  return listProduct;
  }
  
  private String createProductBOM(org.openbravo.database.ConnectionProvider connectionProvider,Product product, String productId,long line) throws ServletException{
	  
	  String newProductBomId = SequenceIdData.getUUID();
	  String strSql = " INSERT INTO m_product_bom(                                                          "+
			  "             m_product_bom_id, ad_client_id, ad_org_id, isactive, created,           "+
			  "             createdby, updated, updatedby, line, m_product_id, m_productbom_id,     "+
			  "             bomqty, description, bomtype)                                           "+
			  "     VALUES ('" + newProductBomId + "', '"+ product.getClient().getId() + "', '"+ product.getOrganization().getId() + "', 'Y', TO_DATE(NOW()),                                                          "+
			  "             '0', TO_DATE(NOW()), '0', "+ String.valueOf(line) +", '"+ product.getId() + "', '" + productId + "',                                                       "+
			  "             1, '', 'P');                                                               ";
		  
	  ResultSet result;
	    PreparedStatement st = null;

	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
 
	  return newProductBomId;
  }
  
  private Product updateProduct(org.openbravo.database.ConnectionProvider connectionProvider,String productId,String updateStr) throws ServletException{
	  
	  String strSql = " UPDATE m_product set " +  updateStr + " WHERE m_product_id = '" + productId + "' ;                                                             "; 
	  Product product = null;
	    ResultSet result;
	    PreparedStatement st = null;

	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    	product = OBDal.getInstance().get(Product.class, productId);
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	  
	  return product;
  }
  
  private ProductBOM updateProductBOM(org.openbravo.database.ConnectionProvider connectionProvider,String productBomId,String updateStr) throws ServletException{
	  String strSql = " UPDATE m_product_bom set " +  updateStr + " WHERE m_product_bom_id = '" + productBomId + "' ;                                                             "; 
	  
	  ProductBOM product = null;
	    ResultSet result;
	    PreparedStatement st = null;

	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    	product = OBDal.getInstance().get(ProductBOM.class, productBomId);
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	  return product;
  }
  
  public String getCurrentTimeStamp() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
  
  private String duplicateProduct(org.openbravo.database.ConnectionProvider connectionProvider, String productId,String productName,String docNo) throws ServletException{
	   String newProductId = SequenceIdData.getUUID();
	   String lastCharacters = newProductId.substring(newProductId.length()-5);
			
			String strSql = " INSERT INTO m_product(                                                                                     "+
					" 		m_product_id, ad_client_id, ad_org_id, isactive, created, createdby,                                 "+
					" 		updated, updatedby, value, name, description, documentnote, help,                                    "+
					" 		upc, sku, c_uom_id, salesrep_id, issummary, isstocked, ispurchased,                                  "+
					" 		issold, isbom, isinvoiceprintdetails, ispicklistprintdetails,                                        "+
					" 		isverified, m_product_category_id, classification, volume, weight,                                   "+
					" 		shelfwidth, shelfheight, shelfdepth, unitsperpallet, c_taxcategory_id,                               "+
					" 		s_resource_id, discontinued, discontinuedby, processing, s_expensetype_id,                           "+
					" 		producttype, imageurl, descriptionurl, guaranteedays, versionno,                                     "+
					" 		m_attributeset_id, m_attributesetinstance_id, downloadurl, m_freightcategory_id,                     "+
					" 		m_locator_id, ad_image_id, c_bpartner_id, ispriceprinted, name2,                                     "+
					" 		costtype, coststd, stock_min, enforce_attribute, calculated,                                         "+
					" 		ma_processplan_id, production, capacity, delaymin, mrp_planner_id,                                   "+
					" 		mrp_planningmethod_id, qtymax, qtymin, qtystd, qtytype, stockmin,                                    "+
					" 		attrsetvaluetype, isquantityvariable, isdeferredrevenue, revplantype,                                "+
					" 		periodnumber, isdeferredexpense, expplantype, periodnumber_exp,                                      "+
					" 		defaultperiod, defaultperiod_exp, bookusingpoprice, c_uom_weight_id,                                 "+
					" 		m_brand_id, isgeneric, generic_product_id, createvariants, characteristic_desc,                      "+
					" 		updateinvariants, managevariants, em_svfsv_special_order, em_svfsv_hard_kit,                         "+
					" 		em_svfsv_soft_kit, em_svfsv_substitute_product, em_svfsv_substitute_for)                             "+
					" Select  '" + newProductId + "', ad_client_id, ad_org_id, isactive, TO_DATE(NOW()), createdby,                      "+
					" 		TO_DATE(NOW()), updatedby, '"  + productName +  "-" + lastCharacters + "', '" + productName + "', description, documentnote, help,   "+
					" 		upc, sku, c_uom_id, salesrep_id, issummary, isstocked, ispurchased,                                  "+
					" 		issold, 'Y', isinvoiceprintdetails, ispicklistprintdetails,                                        "+
					" 		isverified, m_product_category_id, classification, volume, weight,                                   "+
					" 		shelfwidth, shelfheight, shelfdepth, unitsperpallet, c_taxcategory_id,                               "+
					" 		s_resource_id, discontinued, discontinuedby, processing, s_expensetype_id,                           "+
					" 		producttype, imageurl, descriptionurl, guaranteedays, versionno,                                     "+
					" 		m_attributeset_id, m_attributesetinstance_id, downloadurl, m_freightcategory_id,                     "+
					" 		m_locator_id, ad_image_id, c_bpartner_id, ispriceprinted, name2,                                     "+
					" 		costtype, coststd, stock_min, enforce_attribute, calculated,                                         "+
					" 		ma_processplan_id, production, capacity, delaymin, mrp_planner_id,                                   "+
					" 		mrp_planningmethod_id, qtymax, qtymin, qtystd, qtytype, stockmin,                                    "+
					" 		attrsetvaluetype, isquantityvariable, isdeferredrevenue, revplantype,                                "+
					" 		periodnumber, isdeferredexpense, expplantype, periodnumber_exp,                                      "+
					" 		defaultperiod, defaultperiod_exp, bookusingpoprice, c_uom_weight_id,                                 "+
					" 		m_brand_id, isgeneric, generic_product_id, createvariants, characteristic_desc,                      "+
					" 		updateinvariants, managevariants, em_svfsv_special_order, em_svfsv_hard_kit,                         "+
					" 		em_svfsv_soft_kit, em_svfsv_substitute_product, em_svfsv_substitute_for                              "+
					" FROM m_product                                                                                             "+
					" WHERE m_product_id = '" + productId + "' ;                                                             ";
		    
		    ResultSet result;
		    PreparedStatement st = null;

		    int iParameter = 0;
		    try {
		    	st = connectionProvider.getPreparedStatement(strSql);
		    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
		    	st.execute();
		    } catch(SQLException e){
		      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		    } catch(Exception ex){
		      throw new ServletException("@CODE=@" + ex.getMessage());
		    } finally {
		      try {
		        connectionProvider.releasePreparedStatement(st);
		      } catch(Exception ignore){
		        ignore.printStackTrace();
		      }
		    }
	   
		    duplicatePrice(connectionProvider, productId, newProductId);
		    duplicatePurchase(connectionProvider, productId, newProductId);
		    
		    
	   return newProductId;
  }
  
  private void duplicatePrice(org.openbravo.database.ConnectionProvider connectionProvider, String productId, String newProductId) throws ServletException{
			
			String strSql = "INSERT INTO m_productprice(m_productprice_id, m_pricelist_version_id, m_product_id, ad_client_id,                                             "+
					"ad_org_id, isactive, created, createdby, updated, updatedby, pricelist, pricestd, pricelimit, cost, algorithm)                                "+
					"Select  get_uuid(), m_pricelist_version_id, '" + newProductId + "', ad_client_id, ad_org_id, isactive, TO_DATE(NOW()), createdby, TO_DATE(NOW()),     "+
					"updatedby, pricelist, pricestd, pricelimit, cost, algorithm                                                                                   "+
					"From m_productprice                                                                                                                           "+
					"WHERE m_productprice.m_product_id = '" + productId + "' ;                                                                                 ";
		    
		    ResultSet result;
		    PreparedStatement st = null;

		    int iParameter = 0;
		    try {
		    	st = connectionProvider.getPreparedStatement(strSql);
		    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
		    	st.execute();
		    } catch(SQLException e){
		      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		    } catch(Exception ex){
		      throw new ServletException("@CODE=@" + ex.getMessage());
		    } finally {
		      try {
		        connectionProvider.releasePreparedStatement(st);
		      } catch(Exception ignore){
		        ignore.printStackTrace();
		      }
		    }
 }
  
  private void duplicatePurchase(org.openbravo.database.ConnectionProvider connectionProvider, String productId, String newProductId) throws ServletException{
		
		String strSql = "INSERT INTO m_product_po(m_product_po_id, m_product_id, c_bpartner_id, ad_client_id, ad_org_id,                                               "+
				"isactive, created, createdby, updated, updatedby, iscurrentvendor, c_uom_id, c_currency_id, pricelist, pricepo, priceeffective, pricelastpo,   "+
				"pricelastinv, vendorproductno, upc, vendorcategory, discontinued, discontinuedby, order_min, order_pack, costperorder, deliverytime_promised, "+ 
				"deliverytime_actual, qualityrating, royaltyamt, manufacturer, capacity, qtystd, qtytype) "+ 
				"select  get_uuid(), '" + newProductId + "', c_bpartner_id, ad_client_id, ad_org_id, isactive, TO_DATE(NOW()), createdby, TO_DATE(NOW()), updatedby,   "+
				" iscurrentvendor, c_uom_id, c_currency_id, pricelist, pricepo, priceeffective, pricelastpo, pricelastinv, vendorproductno, upc, " + 
				"vendorcategory, discontinued, discontinuedby, order_min, order_pack, costperorder, deliverytime_promised, deliverytime_actual, "+ 
				"qualityrating, royaltyamt, manufacturer, capacity, qtystd, qtytype "+ 
				"FROM m_product_po                                                                                                                             "+
				"WHERE m_product_po.m_product_id = '"+productId+"' ;                                                                                         ";
		
	    ResultSet result;
	    PreparedStatement st = null;

	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
}
  
  public static List<ShipmentInOut> getAsociatedShipments(Order order)    throws ServletException {
		
		 OBCriteria<ShipmentInOut> criteriaE = OBDal.getInstance().createCriteria(ShipmentInOut.class);
		 criteriaE.add(Restrictions.eq("salesOrder", order));
		 List<ShipmentInOut> listE = criteriaE.list();
		 return listE;
	  }
  
  
}
