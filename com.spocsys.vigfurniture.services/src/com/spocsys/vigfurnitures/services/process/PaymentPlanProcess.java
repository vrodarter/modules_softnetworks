/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2012 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.event.PaidStatusEventHandler;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetailV;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.vigfurnitures.services.utils.DataSearchClass;
import com.spocsys.vigfurnitures.services.utils.PDFieldProvider;
import com.spocsys.vigfurnitures.services.utils.PaymentPlanDataSearchClass;

public class PaymentPlanProcess extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    
    if (vars.commandIn("DEFAULT")) {
    	
    	clearSessionValue(vars);
    	String strWindow = vars.getStringParameter("inpwindowId");
    	String strTab = vars.getStringParameter("inpTabId");
    	String documentId = vars.getStringParameter("C_Order_ID");
    	if((documentId == null)||(documentId.equalsIgnoreCase(""))){
    		documentId = vars.getStringParameter("C_Invoice_ID");
    	}

    	vars.setSessionValue("inpwindowId", strWindow);
    	vars.setSessionValue("inpTabId", strTab);
    	vars.setSessionValue("documentId", documentId);
    	
        //printPageDataSheet(response, vars);C_Order_ID
   		printPage(request,response,vars,documentId);
    	
    }else if (vars.commandIn("generate")) {
    	
    	String documentId = vars.getGlobalVariable("", "documentId");
    	String strWindow = vars.getGlobalVariable("", "inpwindowId");
    	String strTab = vars.getGlobalVariable("", "inpTabId");
    	BigDecimal value = new BigDecimal(vars.getStringParameter("paramAmountFrom"));
    	String selectedMethod = vars.getStringParameter("inpPaymentMethodId");
    	
    	String strWindowPath = Utility.getTabURL(strTab, "R", true);
    	if (strWindowPath.equals(""))
            strWindowPath = strDefaultServlet;
    		String result = "Transaction successfully";
    		OBError myError = new OBError();
	    	try {
	    		generatePaymentSchedule(documentId,selectedMethod,value);
					myError.setType("Info");
	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				myError.setType("Error");
				result = " There was an error in the generation. Error:"+e.getMessage();
				e.printStackTrace();
			}
	        myError.setMessage(result);
	        vars.setMessage(strTab, myError);
	        printPageClosePopUp(response, vars, strWindowPath);
    }else
      pageError(response);
  }
  
  private void generatePaymentSchedule(String documentId,String paymentMethod,BigDecimal amount) throws Exception{
	  
	  Order order = null;
	    Invoice invoice = null;
	    Organization org = null;
	    Client client = null;
	    List<FIN_PaymentSchedule> list = new ArrayList<FIN_PaymentSchedule>();
	    
	    order = PaymentPlanDataSearchClass.getOrder(documentId);
	    if(order == null){
	    	invoice = PaymentPlanDataSearchClass.getInvoice(documentId);
	    	org = invoice.getOrganization();
	    	client = invoice.getClient();
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromInvoice(invoice); 
	    	Date date = list.get(0).getDueDate();
	    	Date expecteddate = list.get(0).getExpectedDate();
	    	FIN_PaymentSchedule paymentSchedule = PaymentPlanDataSearchClass.createFINPaymentScheduleInvoice(org.getId(), client.getId(), invoice.getBusinessPartner().getId(), invoice, paymentMethod, amount, date,expecteddate);
	    	PaymentPlanDataSearchClass.createFINPaymentScheduleDetailInvoice(this, org.getId(), client.getId(), invoice.getBusinessPartner().getId(), paymentSchedule, paymentMethod, amount, date,expecteddate,"0");
	    	List<FIN_PaymentScheduleDetail> listfinPayDetail = PaymentPlanDataSearchClass.getPaymentDetailInvoice(list.get(0)) ;
	    	if(list.get(0).getAmount().subtract(amount).compareTo(new BigDecimal(0))!=0){    		
	    		PaymentPlanDataSearchClass.updateFINPaymentSchedule(list.get(0), list.get(0).getAmount().subtract(amount));
	    		for (FIN_PaymentScheduleDetail fin_PaymentDetail : listfinPayDetail) {
	    			PaymentPlanDataSearchClass.updateFinPaymentScheduleDetail(this,fin_PaymentDetail.getId(),null, list.get(0).getAmount().subtract(amount).toString());
				}
	    	}else{
	    		for (FIN_PaymentScheduleDetail fin_PaymentDetail : listfinPayDetail) {
	    			PaymentPlanDataSearchClass.deletePlanDetail(fin_PaymentDetail);
				}
	    		PaymentPlanDataSearchClass.deletePlan(list.get(0));
	    	}
	    	
	    }else{
	    	org = order.getOrganization();
	    	client = order.getClient();
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromOrder(order); 
	    	Date date = list.get(0).getDueDate();
	    	Date expecteddate = list.get(0).getExpectedDate();
	    	FIN_PaymentSchedule paymentSchedule = PaymentPlanDataSearchClass.createFINPaymentScheduleOrder(org.getId(), client.getId(), order.getBusinessPartner().getId(), order, paymentMethod, amount, date,expecteddate);
	    	PaymentPlanDataSearchClass.createFINPaymentScheduleDetailOrder(this,org.getId(), client.getId(), order.getBusinessPartner().getId(), paymentSchedule, paymentMethod, amount, date,expecteddate,"0");
	    	List<FIN_PaymentScheduleDetail> listfinPayDetail = PaymentPlanDataSearchClass.getPaymentDetailOrder(list.get(0)) ;
	    	if(list.get(0).getAmount().subtract(amount).compareTo(new BigDecimal(0))!=0){
	    		PaymentPlanDataSearchClass.updateFINPaymentSchedule(list.get(0), list.get(0).getAmount().subtract(amount));
	    		for (FIN_PaymentScheduleDetail fin_PaymentDetail : listfinPayDetail) {
	    			PaymentPlanDataSearchClass.updateFinPaymentScheduleDetail(this,fin_PaymentDetail.getId(),null, list.get(0).getAmount().subtract(amount).toString());
				}
	    	}else{
	    		for (FIN_PaymentScheduleDetail fin_PaymentDetail : listfinPayDetail) {
	    			PaymentPlanDataSearchClass.deletePlanDetail(fin_PaymentDetail);
				}
	    		PaymentPlanDataSearchClass.deletePlan(list.get(0));
	    	}
	    }
  }
  
  private void clearSessionValue(VariablesSecureApp vars) {
	    vars.removeSessionValue("fFinancialAccountId");
	    vars.removeSessionValue("inpwindowId");
	    vars.removeSessionValue("inpTabId");
	  }
  private void printPage(HttpServletRequest request, HttpServletResponse response,
	      VariablesSecureApp vars,String documentId) throws IOException, ServletException {
	    // Check for permissions to apply modules from application server.
	    
	    String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	    final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurnitures/services/process/PaymentPlanProcess").createXmlDocument();
	    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
	    xmlDocument.setParameter("theme", vars.getTheme());
	    {
	      final OBError myMessage = vars.getMessage("PaymentPlanProcess");
	      vars.removeMessage("PaymentPlanProcess");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	    }
	    
	    Order order = null;
	    Invoice invoice = null;
	    Organization org = null;
	    Client client = null;
	    List<FIN_PaymentSchedule> list = new ArrayList<FIN_PaymentSchedule>();
	    BigDecimal totalPaid = new BigDecimal(0);
	    order = PaymentPlanDataSearchClass.getOrder(documentId);
	    if(order == null){
	    	invoice = PaymentPlanDataSearchClass.getInvoice(documentId);
	    	org = invoice.getOrganization();
	    	xmlDocument.setParameter("addDocumentNotxt", invoice.getDocumentNo());
	    	xmlDocument.setParameter("addDocumentTypetxt", "Invoice");
	    	client = invoice.getClient();
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromInvoice(invoice);
	    	totalPaid = getTotalAsignablePaid(list);  
	    	xmlDocument.setParameter("addDocumentTotalAmount", invoice.getGrandTotalAmount().toString());
	    	
	    }else{
	    	org = order.getOrganization();
	    	xmlDocument.setParameter("addDocumentNotxt", order.getDocumentNo());
	    	xmlDocument.setParameter("addDocumentTypetxt", "Order");
	    	client = order.getClient();
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromOrder(order);
	    	totalPaid = getTotalAsignablePaid(list);  
	    	xmlDocument.setParameter("addDocumentTotalAmount", order.getGrandTotalAmount().toString());
	 	    
	    }
	    xmlDocument.setParameter("addDocumentToPaid", totalPaid.toString());
		PDFieldProvider[] paymentMethodsArr = PaymentPlanDataSearchClass.getPaymentMethodsPDFields(client);
	    xmlDocument.setParameter("paramAmountFrom", "0");
	    String selected = new String();
		if(paymentMethodsArr.length > 0){
			selected = paymentMethodsArr[0].getField("id");
			xmlDocument.setParameter("addPaymentMethodId", selected);
		}
		xmlDocument.setData("reportPaymentMethod_Id", "liststructure", paymentMethodsArr);
		
		
		String table = generateTable(list,null);		
		xmlDocument.setParameter("desc", table);
		
	    response.setContentType("text/html; charset=UTF-8");
	    final PrintWriter out = response.getWriter();

	    out.println(xmlDocument.print());
	    out.close();
	  }
  
  
  private BigDecimal getTotalAsignablePaid(List<FIN_PaymentSchedule> list ){

	  return list.get(0).getAmount();
  }
  
  private void printPageResponse(HttpServletRequest request, HttpServletResponse response,
	      VariablesSecureApp vars,String documentId,String selectedId,String selectedMethodId) throws IOException, ServletException {
	    // Check for permissions to apply modules from application server.
	    
	  String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	    final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurnitures/services/process/AccountTransfer").createXmlDocument();
	    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
	    xmlDocument.setParameter("theme", vars.getTheme());
	    {
	      final OBError myMessage = vars.getMessage("AccountTransfer");
	      vars.removeMessage("AccountTransfer");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	    }
	    
	    Order order = null;
	    Invoice invoice = null;
	    Organization org = null;
	    Client client = null;
	    List<FIN_PaymentSchedule> list = new ArrayList<FIN_PaymentSchedule>();
	    
	    order = PaymentPlanDataSearchClass.getOrder(documentId);
	    if(order == null){
	    	invoice = PaymentPlanDataSearchClass.getInvoice(documentId);
	    	org = invoice.getOrganization();
	    	client = invoice.getClient();
	    	xmlDocument.setParameter("addDocumentNotxt", invoice.getDocumentNo());
	    	xmlDocument.setParameter("addDocumentTypetxt", "Invoice");
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromInvoice(invoice);
	    }else{
	    	org = order.getOrganization();
	    	client = order.getClient();
	    	xmlDocument.setParameter("addDocumentNotxt", order.getDocumentNo());
	    	xmlDocument.setParameter("addDocumentTypetxt", "Order");
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromOrder(order);
	    }
	   
	    
	    
	    PDFieldProvider[] paymentMethodsArr = PaymentPlanDataSearchClass.getPaymentMethodsPDFields(client);  
		xmlDocument.setParameter("addPaymentMethodId", selectedMethodId);
		xmlDocument.setData("reportPaymentMethod_Id", "liststructure", paymentMethodsArr);
		
		xmlDocument.setParameter("addSelectedId", selectedId);
	    
		String table = generateTable(list,selectedId);		
		xmlDocument.setParameter("desc", table);
		
	    response.setContentType("text/html; charset=UTF-8");
	    final PrintWriter out = response.getWriter();

	    out.println(xmlDocument.print());
	    out.close();
	  }
  
  
  private String generateTable(List<FIN_PaymentSchedule> list, String selectedId){
	   
	  String generalRows = new String();
	  if(list.size() == 0){
		  generalRows = " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_0\">                              "+
				  "     <td class=\"DataGrid_Body_Cell\">There is not related data.</td>                                     "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                                             "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                                             "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                           "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                           "+
				  " </tr>                                                                                               ";
	  }
	  else{
		  int pos = 0;
		  for (FIN_PaymentSchedule transData : list) {
			  String checked = "";
			  if((selectedId != null) && (selectedId.equalsIgnoreCase(transData.getId())))
				  checked = "checked";
				  
			String bp = "&nbsp";
			String document = "&nbsp";
			String documentNo = "";
			Invoice invoice = transData.getInvoice();
			Order order = transData.getOrder();
			if(invoice != null)
				documentNo = invoice.getDocumentNo();
			else
				documentNo = order.getDocumentNo();
			String paymentMethod = transData.getFinPaymentmethod().getName();
 		    generalRows +=  " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_"+ String.valueOf(pos) +"\">                              "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + documentNo + "</td>                                     "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + paymentMethod + "</td>                                     "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getAmount().toString() + "</td>                                                             "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getPaidAmount().toString() + "</td>                                           "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getOutstandingAmount().toString() + "</td>                                     "+
					  " </tr>                                                                                               ";
			  pos++;
		}
	  }
		  
	  String generalTable = " <table class=\"DataGrid_Header_Table DataGrid_Body_Table\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">                                             "+
			  "     <tbody>                                                                                                                                        "+
			  "         <tr class=\"Popup_Client_Selector_DataGrid_HeaderRow\">                                                                                      "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"100\">Document No</th>                                                                             "+
			  " 			<th class=\"DataGrid_Header_Cell\" width=\"220\">Payment Method</th>                                                                                  "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"70\" style=\"align:right\">Amoumt</th>                                                                                "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"170\" style=\"align:right\">Paid</th>                                                                             "+
			  " 			<th class=\"DataGrid_Header_Cell\" width=\"110\" style=\"align:right\">Outstanding</th>                                                                                  "+
			  "         </tr>                                                                                                                                      "+
			  generalRows + 
			  "     </tbody>                                                                                                                                       "+
			  " </table>                                                                                                                                           ";
	  return generalTable;
	
  }
  
 
  
 
  public String getServletInfo() {
    return "GenerateInvoicesmanual Servlet. This Servlet was made by Pablo Sarobe";
  } // end of getServletInfo() method
}
