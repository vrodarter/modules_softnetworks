/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2012 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.basic.BasicTextUI.BasicHighlighter;

import org.apache.axis.transport.jms.TopicConnector;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.info.Account;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.vigfurniture.financial.SVFFIN_Deposit;
import com.spocsys.vigfurniture.financial.SVFFIN_Deposit_Mngt;
import com.spocsys.vigfurnitures.services.utils.AccountDataClass;
import com.spocsys.vigfurnitures.services.utils.CreditDataClass;
import com.spocsys.vigfurnitures.services.utils.DataSearchClass;
import com.spocsys.vigfurnitures.services.utils.GlItemDataClass;
import com.spocsys.vigfurnitures.services.utils.NewPaymentDataClass;
import com.spocsys.vigfurnitures.services.utils.PDFieldProvider;
import com.spocsys.vigfurnitures.services.utils.PaymentDataSearchClass;
import com.spocsys.vigfurnitures.services.utils.PaymentPlanDataSearchClass;

public class PaymentProcess extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    
    if (vars.commandIn("DEFAULT")) {
    	
    	clearSessionValue(vars);
    	String strWindow = vars.getStringParameter("inpwindowId");
    	String strTab = vars.getStringParameter("inpTabId");
    	String documentId = vars.getStringParameter("C_Order_ID");
    	if((documentId == null)||(documentId.equalsIgnoreCase(""))){
    		documentId = vars.getStringParameter("C_Invoice_ID");
    	}

    	vars.setSessionValue("inpwindowId", strWindow);
    	vars.setSessionValue("inpTabId", strTab);
    	vars.setSessionValue("documentId", documentId);
    	
        //printPageDataSheet(response, vars);C_Order_ID
   		printPage(request,response,vars,documentId,strWindow);
    	
    }else if(vars.commandIn("generatePayFromDep")){
    	
      	String documentId = vars.getGlobalVariable("", "documentId");
    	String strWindow = vars.getGlobalVariable("", "inpwindowId");
    	String strTab = vars.getGlobalVariable("", "inpTabId");
    	String selectedMethod = vars.getStringParameter("inpPaymentMethodId");
    	String strWindowPath = Utility.getTabURL(strTab, "R", true);
    	vars.setSessionValue("inpwindowId", strWindow);
    	vars.setSessionValue("inpTabId", strTab);
    	vars.setSessionValue("documentId", documentId);
    	boolean error = false;
    	String errorMsg = "";
    	
    	ArrayList<String> selectedGlitemsIds = new ArrayList<String>();
    	//List<GlItemDataClass> listGlitems = new ArrayList<GlItemDataClass>();
    	
    	/*String selectedGlitems = vars.getInStringParameter("inpGlitemId", IsIDFilter.instance);
    	if((!selectedGlitems.equalsIgnoreCase(""))&&(selectedGlitems != null)){
	    	selectedGlitemsIds = Utility.stringToArrayList(selectedGlitems.replaceAll("\\(|\\)|'", ""));
	    	
	    	for (String idStr : selectedGlitemsIds) {
	    		GlItemDataClass glItemData = new GlItemDataClass();
				GLItem glitem = OBDal.getInstance().get(GLItem.class, idStr);
				String amount = vars.getStringParameter("inpAmount" + idStr);
				glItemData.setGlItem(glitem);
				glItemData.setAmount(new BigDecimal(amount));
				listGlitems.add(glItemData);
			}
    	}
    	*/
    	
    	
    	ArrayList<String> selectedPaymentsIds = new ArrayList<String>();
    	List<NewPaymentDataClass> listPayments = new ArrayList<NewPaymentDataClass>();
    	List<GlItemDataClass> listTotalGlitem = new ArrayList<GlItemDataClass>();
    	List<AccountDataClass> listTotalAccount = new ArrayList<AccountDataClass>();
    	List<CreditDataClass> listTotalCredit = new ArrayList<CreditDataClass>();
    	
    	String selectedPayments = vars.getInStringParameter("inpPayPlanId", IsIDFilter.instance);
    	if((!selectedPayments.equalsIgnoreCase(""))&&(selectedPayments != null)){
    		selectedPaymentsIds = Utility.stringToArrayList(selectedPayments.replaceAll("\\(|\\)|'", ""));
	    	for (String idStr : selectedPaymentsIds) {
	    		
	    		//Get account data from interface
	    		String accountId = vars.getStringParameter("idAccount" + idStr );
	    		FIN_PaymentSchedule paymentSchedule = OBDal.getInstance().get(FIN_PaymentSchedule.class, idStr);
	    		String accountAmountInputId = "idAccount" + idStr + "inpAmount";
	    		BigDecimal accountAmount = new BigDecimal(0);
	    		if(!accountId.equalsIgnoreCase("-1")){
	    			accountAmount = new BigDecimal(vars.getStringParameter(accountAmountInputId));
	    			listTotalAccount = AddAccountAmount(listTotalAccount,accountId,accountAmount);
	    		}
	    		
	    		//Get Glitem data from interface
	    		String idGlitem = vars.getStringParameter("idGlitem" + idStr);
	    		BigDecimal glItemAmount = new BigDecimal("0");
	    		GLItem glItem = null;
	    		if(!idGlitem.equalsIgnoreCase("-1")){
	    			glItemAmount = new BigDecimal(vars.getStringParameter("idGlitem" + idStr + "inpAmount"));
		    		glItem = OBDal.getInstance().get(GLItem.class, idGlitem);
		    		listTotalGlitem = AddGlitemAmount(listTotalGlitem,idGlitem,glItemAmount);
	    		}
	    		
	    		//Get credit data from interface
	    		String creditId = vars.getStringParameter("idCredit" + idStr );
	    		FIN_Payment credit = OBDal.getInstance().get(FIN_Payment.class, creditId);
	    		String creditAmountInputId = "idCredit" + idStr + "inpAmount";
	    		BigDecimal creditAmount = new BigDecimal(0);
	    		if(!accountId.equalsIgnoreCase("-1")){
	    			creditAmount = new BigDecimal(vars.getStringParameter(creditAmountInputId));
	    			listTotalCredit = AddCreditAmount(listTotalCredit,creditId,creditAmount);
	    		}
	    		
	    		
	    		
	    		if(!(accountAmount.compareTo(new BigDecimal(0))==0 && glItemAmount.compareTo(new BigDecimal(0))==0)){
	    			NewPaymentDataClass paymentData = new NewPaymentDataClass();
		    		paymentData.setAccountId(accountId);
		    		paymentData.setAmountAccount(accountAmount);
		    		paymentData.setGlItem(glItem);
		    		paymentData.setGlItemAmount(glItemAmount);
		    		paymentData.setPaymentMethodId(paymentSchedule.getFinPaymentmethod().getId());
		    		paymentData.setPayments(paymentSchedule);
		    		paymentData.setCredit(credit);
		    		paymentData.setCreditAmount(creditAmount);
		    		
		    		BigDecimal totalToPaid =  accountAmount.add(glItemAmount);
		    		if(totalToPaid.compareTo(paymentSchedule.getAmount().subtract(paymentSchedule.getPaidAmount()))>=0)
		    			paymentData.setCompleteAccount(true);
		    		else
		    			paymentData.setCompleteAccount(false);
		    		
					listPayments.add(paymentData);
	    		}
	    			
	    		
	    		
			}
    	}
    	
    	
    	for(int i=0;((i<listTotalAccount.size())&&(!error));i++){
    		AccountDataClass obj = listTotalAccount.get(i);
    		FIN_FinancialAccount account = OBDal.getInstance().get(FIN_FinancialAccount.class, obj.getAccountId());
    		if(account.getCurrentBalance().compareTo(obj.getAmount())<0){
    			error = true;
    			errorMsg= "The sum of amounts is bigger than the current balance for account "+ account.getName();
    		}
    	}
    	
    	for(int i=0;((i<listTotalCredit.size())&&(!error));i++){
    		CreditDataClass obj = listTotalCredit.get(i);
    		FIN_Payment credit = OBDal.getInstance().get(FIN_Payment.class, obj.getCreditId());
    		if(credit.getGeneratedCredit().compareTo(obj.getAmount())<0){
    			error = true;
    			errorMsg= "The sum of amount is bigger than the current balance for the selected credit ";
    		}
    	}
    	
    	for(int i=0;((i<listTotalGlitem.size())&&(!error));i++){
    		GlItemDataClass obj = listTotalGlitem.get(i);
    		GLItem gl = obj.getGlItem();
    		if(gl!= null){
	    		BigDecimal amount = new BigDecimal(PaymentDataSearchClass.getDepositBalance(gl.getId()).toString());
	    		if(amount.compareTo(obj.getAmount())<0){
	    			error = true;
	    			errorMsg= "The sum of amounts is bigger than the current Deposit for deposit "+ gl.getName();
	    		}
    		}
    	}
    	
    	if(!error){
    		if (strWindowPath.equals(""))
                strWindowPath = strDefaultServlet;
        		String result = "Transaction successfully";
        		OBError myError = new OBError();
    	    	try {
    	    		if(listPayments.size() > 0){
    	    			generatePaymentProcess(documentId, listPayments,strWindow);
    	    			myError.setType("Info");
    	    		}else{
    	    			error = true;
    	    			errorMsg= "Not valid payment data selected";
    	    			printPageResponse(request, response, vars, documentId, selectedMethod,selectedPaymentsIds,selectedGlitemsIds,strWindow,errorMsg);
    	    		}
    	    			
    	
    			} catch (Exception e) {
    				// TODO Auto-generated catch block
    				myError.setType("Error");
    				result = " There was an error in the generation. Error:"+e.getMessage();
    				e.printStackTrace();
    			}
    	        myError.setMessage(result);
    	        vars.setMessage(strTab, myError);
    	        printPageClosePopUp(response, vars, strWindowPath);
    	}
    	else{    		
    		printPageResponse(request, response, vars, documentId, selectedMethod,selectedPaymentsIds,selectedGlitemsIds,strWindow,errorMsg);
    	}
    	
    	
    	
    	
    }else if (vars.commandIn("generate")) {
    	
    	String documentId = vars.getGlobalVariable("", "documentId");

    	String strTab = vars.getGlobalVariable("", "inpTabId");
    	String strWindow = vars.getGlobalVariable("", "inpwindowId");
    	BigDecimal value = new BigDecimal(vars.getStringParameter("paramAmountFrom"));
    	String selectedMethod = vars.getStringParameter("inpPaymentMethodId");

    	ArrayList<String> selectedGlitemsIds = new ArrayList<String>();
    	    	
    	/*String selectedGlitems = vars.getInStringParameter("inpGlitemId", IsIDFilter.instance);
    	if((!selectedGlitems.equalsIgnoreCase(""))&&(selectedGlitems != null)){
	    	selectedGlitemsIds = Utility.stringToArrayList(selectedGlitems.replaceAll("\\(|\\)|'", ""));
    	}*/
    	
    	ArrayList<String> selectedPaymentsIds = new ArrayList<String>();
    	    	
    	String selectedPayments = vars.getInStringParameter("inpPayPlanId", IsIDFilter.instance);
    	if((!selectedPayments.equalsIgnoreCase(""))&&(selectedPayments != null)){
    		selectedPaymentsIds = Utility.stringToArrayList(selectedPayments.replaceAll("\\(|\\)|'", ""));
    	}
    	
    	String strWindowPath = Utility.getTabURL(strTab, "R", true);
    	if (strWindowPath.equals(""))
            strWindowPath = strDefaultServlet;
    		String result = "Transaction successfully";
    		OBError myError = new OBError();
    		
    		boolean error = false;
        	String errorMsg = "";
    		
	    	try {
	    		if(selectedPaymentsIds.size() > 0){
	    			String paySchId = selectedPaymentsIds.get(0);
	    			FIN_PaymentSchedule paymentSchedule = OBDal.getInstance().get(FIN_PaymentSchedule.class, paySchId);
	    			BigDecimal toPaid = paymentSchedule.getAmount().subtract(paymentSchedule.getPaidAmount());
	    			if(paymentSchedule.getPaidAmount().compareTo(new BigDecimal(0)) > 0){
	    				
	    				BigDecimal toPaidTotal = paymentSchedule.getAmount().subtract(paymentSchedule.getPaidAmount());
	    				if(value.compareTo(toPaidTotal)>0){
	    					error = true;
		    				errorMsg = "the new payment schedule value can't be bigger than the payment schedule pending amount";
		    				printPageResponse(request, response, vars, documentId, selectedMethod,selectedPaymentsIds,selectedGlitemsIds,strWindow,errorMsg);
	    				}
	    				else{
	    					generatePaymentSchedule(paySchId, documentId, selectedMethod, value);
							myError.setType("Info");
	    				}
	    					
	    					
	    				
	    				
	    				printPageResponse(request, response, vars, documentId, selectedMethod,selectedPaymentsIds,selectedGlitemsIds,strWindow,errorMsg);
	    			}else if(paymentSchedule.getAmount().compareTo(value) <= 0){
	    				error = true;
	    				errorMsg = "the new payment schedule value can't be equal or bigger than the payment schedule current value";
	    				printPageResponse(request, response, vars, documentId, selectedMethod,selectedPaymentsIds,selectedGlitemsIds,strWindow,errorMsg);
	    			}else{
	    				generatePaymentSchedule(paySchId, documentId, selectedMethod, value);
						myError.setType("Info");
	    			}
	    				
	    			
	    		}
	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				myError.setType("Error");
				result = " There was an error in the generation. Error:"+e.getMessage();
				e.printStackTrace();
			}
	        myError.setMessage(result);
	        vars.setMessage(strTab, myError);
	        printPageResponse(request, response, vars, documentId, selectedMethod,selectedPaymentsIds,selectedGlitemsIds,strWindow,"");
    }else{
    	String strTab = vars.getGlobalVariable("", "inpTabId");
		BigDecimal value = new BigDecimal(vars.getStringParameter("paramAmountFrom"));
		String selectedMethod = vars.getStringParameter("inpPaymentMethodId");
	
		
		String strWindowPath = Utility.getTabURL(strTab, "R", true);
    	printPageClosePopUp(response, vars, strWindowPath);
    }
  }
  
  private List<AccountDataClass> AddAccountAmount(List<AccountDataClass> list,String idAccount,BigDecimal amount){
	  boolean encontrado = false;
	  for (int i = 0;((i<list.size())&&(!encontrado));i++) {
		AccountDataClass accountDataClass = list.get(i);
		if(accountDataClass.getAccountId().equalsIgnoreCase(idAccount)){
			encontrado = true;
			accountDataClass.setAmount(accountDataClass.getAmount().add(amount));
			list.set(i, accountDataClass);
		}
	  }
	  
	  if(!encontrado && !idAccount.equalsIgnoreCase("-1")){
		  AccountDataClass accountDataClass = new AccountDataClass();
		  accountDataClass.setAccountId(idAccount);
		  accountDataClass.setAmount(amount);
		  list.add(accountDataClass);
	  }
	  return list;
  }
  
  private List<GlItemDataClass> AddGlitemAmount(List<GlItemDataClass> list,String idGlitem,BigDecimal amount){
	  boolean encontrado = false;
	  for (int i = 0;((i<list.size())&&(!encontrado));i++) {
		GlItemDataClass glitemDataClass = list.get(i);
		if(glitemDataClass.getGlItem().getId().equalsIgnoreCase(idGlitem)){
			encontrado = true;
			glitemDataClass.setAmount(glitemDataClass.getAmount().add(amount));
			list.set(i, glitemDataClass);
		}
	  }
	  
	  if(!encontrado){
		  GlItemDataClass glItemDataClass = new GlItemDataClass();
		  GLItem gl = OBDal.getInstance().get(GLItem.class, idGlitem);
		  glItemDataClass.setGlItem(gl);
		  glItemDataClass.setAmount(amount);
		  list.add(glItemDataClass);
	  }
	  return list;
  }
  
  private List<CreditDataClass> AddCreditAmount(List<CreditDataClass> list,String idCredit,BigDecimal amount){
	  boolean encontrado = false;
	  for (int i = 0;((i<list.size())&&(!encontrado));i++) {
		  CreditDataClass creditDataClass = list.get(i);
		if(creditDataClass.getCreditId().equalsIgnoreCase(idCredit)){
			encontrado = true;
			creditDataClass.setAmount(creditDataClass.getAmount().add(amount));
			list.set(i, creditDataClass);
		}
	  }
	  
	  if(!encontrado && !idCredit.equalsIgnoreCase("-1")){
		  CreditDataClass creditDataClass = new CreditDataClass();
		  creditDataClass.setCreditId(idCredit);
		  creditDataClass.setAmount(amount);
		  list.add(creditDataClass);
	  }
	  return list;
  }
  
  private void generatePaymentSchedule(String paySchedId, String documentId,String paymentMethod,BigDecimal amount) throws Exception{
	  
	  Order order = null;
	    Invoice invoice = null;
	    Organization org = null;
	    Client client = null;
	    //List<FIN_PaymentSchedule> list = new ArrayList<FIN_PaymentSchedule>();
	    FIN_PaymentSchedule schedule  = OBDal.getInstance().get(FIN_PaymentSchedule.class, paySchedId);
	    order = PaymentPlanDataSearchClass.getOrder(documentId);
	    if(order == null){
	    	invoice = PaymentPlanDataSearchClass.getInvoice(documentId);
	    	org = invoice.getOrganization();
	    	client = invoice.getClient();
	    	
	    	//list = PaymentPlanDataSearchClass.getPaymentPlanFromInvoice(invoice); 
	    	Date date = schedule.getDueDate();
	    	Date expecteddate = schedule.getExpectedDate();
	    	FIN_PaymentSchedule paymentSchedule = PaymentPlanDataSearchClass.createFINPaymentScheduleInvoice(org.getId(), client.getId(), invoice.getBusinessPartner().getId(), invoice, paymentMethod, amount, date,expecteddate);
	    	PaymentPlanDataSearchClass.createFINPaymentScheduleDetailInvoice(this, org.getId(), client.getId(), invoice.getBusinessPartner().getId(), paymentSchedule, paymentMethod, amount, date,expecteddate,"0");
	    	List<FIN_PaymentScheduleDetail> listfinPayDetail = PaymentPlanDataSearchClass.getPaymentDetailInvoice(schedule) ;
	    	if(schedule.getAmount().subtract(amount).compareTo(new BigDecimal(0))!=0){    		
	    		PaymentPlanDataSearchClass.updateFINPaymentSchedule(schedule, schedule.getAmount().subtract(amount));
	    		for (FIN_PaymentScheduleDetail fin_PaymentDetail : listfinPayDetail) {
	    			PaymentPlanDataSearchClass.updateFinPaymentScheduleDetail(this,fin_PaymentDetail.getId(),null, schedule.getAmount().subtract(amount).toString());
				}
	    	}else{
	    		for (FIN_PaymentScheduleDetail fin_PaymentDetail : listfinPayDetail) {
	    			PaymentPlanDataSearchClass.deletePlanDetail(fin_PaymentDetail);
				}
	    		PaymentPlanDataSearchClass.deletePlan(schedule);
	    	}
	    	
	    }else{
	    	org = order.getOrganization();
	    	client = order.getClient();
	    	//list = PaymentPlanDataSearchClass.getPaymentPlanFromOrder(order); 
	    	Date date = schedule.getDueDate();
	    	Date expecteddate = schedule.getExpectedDate();
	    	FIN_PaymentSchedule paymentSchedule = PaymentPlanDataSearchClass.createFINPaymentScheduleOrder(org.getId(), client.getId(), order.getBusinessPartner().getId(), order, paymentMethod, amount, date,expecteddate);
	    	PaymentPlanDataSearchClass.createFINPaymentScheduleDetailOrder(this,org.getId(), client.getId(), order.getBusinessPartner().getId(), paymentSchedule, paymentMethod, amount, date,expecteddate,"0");
	    	List<FIN_PaymentScheduleDetail> listfinPayDetail = PaymentPlanDataSearchClass.getPaymentDetailOrder(schedule) ;
	    	if(schedule.getAmount().subtract(amount).compareTo(new BigDecimal(0))!=0){
	    		PaymentPlanDataSearchClass.updateFINPaymentSchedule(schedule, schedule.getAmount().subtract(amount));
	    		for (FIN_PaymentScheduleDetail fin_PaymentDetail : listfinPayDetail) {
	    			PaymentPlanDataSearchClass.updateFinPaymentScheduleDetail(this,fin_PaymentDetail.getId(),null, schedule.getAmount().subtract(amount).toString());
				}
	    	}else{
	    		for (FIN_PaymentScheduleDetail fin_PaymentDetail : listfinPayDetail) {
	    			PaymentPlanDataSearchClass.deletePlanDetail(fin_PaymentDetail);
				}
	    		PaymentPlanDataSearchClass.deletePlan(schedule);
	    	}
	    }
  }
  
  private NewPaymentDataClass getPaymentData(String accountId,String methodId,List<NewPaymentDataClass> listPaymentData,boolean complete){
	  for (int i = 0; i < listPaymentData.size(); i++) {
		NewPaymentDataClass object = listPaymentData.get(i);
		if((object.getAccountId().equalsIgnoreCase(accountId))&&(object.getPaymentMethodId().equalsIgnoreCase(methodId))&&(object.isCompleteAccount() == complete))
		   return object;		
	  }
	  return null;
  }
  
  private void generatePaymentProcess(String documentId,List<NewPaymentDataClass> listPayments,String windowId) throws Exception{
	  
	    for (NewPaymentDataClass paymentDataClass : listPayments) {
	    	
	    	FIN_PaymentMethod objMethod = OBDal.getInstance().get(FIN_PaymentMethod.class, paymentDataClass.getPaymentMethodId());
	    	FIN_FinancialAccount account;
	    	if(!paymentDataClass.getAccountId().equalsIgnoreCase("-1"))
	    		account = OBDal.getInstance().get(FIN_FinancialAccount.class,paymentDataClass.getAccountId());
	    	else{
	    		FIN_PaymentSchedule schedule = paymentDataClass.getPayments();
	    		List<FIN_FinancialAccount> listAccount = PaymentDataSearchClass.getBranchAccountList(this,schedule.getOrganization().getId());
	    		account = listAccount.get(0);
	    	}
	    		
	    	List<FIN_PaymentSchedule> listSchedule = new ArrayList<FIN_PaymentSchedule>();
	    	listSchedule.add(paymentDataClass.getPayments());
	    	List<GlItemDataClass> listGlitems = new ArrayList<GlItemDataClass>();
	    	if(paymentDataClass.getGlItem() != null){
	    		GlItemDataClass glitemDataClass = new GlItemDataClass();
	    		glitemDataClass.setAmount(paymentDataClass.getGlItemAmount());
	    		glitemDataClass.setGlItem(paymentDataClass.getGlItem());
	    		listGlitems.add(glitemDataClass);
	    	}
	    	generatePayment(documentId,listSchedule,objMethod,account,listGlitems,paymentDataClass.isCompleteAccount(),windowId,paymentDataClass.getAmountAccount(),paymentDataClass.getCredit(),paymentDataClass.getCreditAmount());
		}

	    
	    
  }
  private void generatePaymentDirectProcess(VariablesSecureApp vars,String documentId,List<FIN_PaymentSchedule> listSchedule,String windowId) throws Exception{
	     	    
	    for (FIN_PaymentSchedule fin_PaymentSchedule : listSchedule){
	    	String accountId = vars.getStringParameter("idAccount" + fin_PaymentSchedule.getId() );
	    	FIN_FinancialAccount account = OBDal.getInstance().get(FIN_FinancialAccount.class, accountId);
	    	generateDirectPayment(documentId,fin_PaymentSchedule,account,windowId) ;
		}

}
  
  
  private List<String> PayGlitem(FIN_Payment payment, FIN_PaymentSchedule fin_PaymentSchedule,BigDecimal totalToPaid,Invoice invoice,Order order,FIN_PaymentMethod paymentMethod,GLItem glItem) throws Exception{
	  
	  List<String> listResult = new ArrayList<String>();
	  
	  fin_PaymentSchedule = PaymentPlanDataSearchClass.updateFINPaymentSchedulePaid(fin_PaymentSchedule, totalToPaid);
	  FIN_PaymentDetail paymentDetail = PaymentDataSearchClass.createFINPaymentDetailts(payment.getOrganization(), payment.getClient(), payment.getBusinessPartner(), payment, null, totalToPaid);
	  String paymentScheduleDetailId = "";
	  if(invoice != null)
		  paymentScheduleDetailId = PaymentPlanDataSearchClass.createFINPaymentScheduleDetailInvoice(this,invoice.getOrganization().getId(), invoice.getClient().getId(), invoice.getBusinessPartner().getId(), fin_PaymentSchedule, paymentMethod.getId() , totalToPaid, fin_PaymentSchedule.getDueDate(), fin_PaymentSchedule.getExpectedDate(), "0");
	  else
		  paymentScheduleDetailId = PaymentPlanDataSearchClass.createFINPaymentScheduleDetailOrder(this,order.getOrganization().getId(), order.getClient().getId(), order.getBusinessPartner().getId(), fin_PaymentSchedule, paymentMethod.getId(), totalToPaid, fin_PaymentSchedule.getDueDate(), fin_PaymentSchedule.getExpectedDate(), "0");
	  PaymentPlanDataSearchClass.updateFinPaymentScheduleDetail(this,paymentScheduleDetailId,paymentDetail.getId(), totalToPaid.toString());
	  
	  listResult.add(fin_PaymentSchedule.getId());
	  listResult.add(paymentScheduleDetailId);
	  
	  return listResult;
  }
  
  private List<GlItemDataClass>  generatePayment(String documentId,List<FIN_PaymentSchedule> listSchedule,FIN_PaymentMethod paymentMethod,FIN_FinancialAccount account,List<GlItemDataClass> listGlitems,boolean complete,String windowId,BigDecimal accountAmount,FIN_Payment credit,BigDecimal creditAmount) throws Exception{
	   
	  Order order = PaymentPlanDataSearchClass.getOrder(documentId);
	  
	  //Total to paid using glitem
	  BigDecimal amountGlitem = new BigDecimal(0);
	  for (GlItemDataClass glItemDataClass : listGlitems) {
		  amountGlitem = amountGlitem.add(glItemDataClass.getAmount());
	  }
	  BigDecimal auxAmountGlitem = amountGlitem;
	  Invoice invoice = null;
	  
	  //Total to paid 
	  BigDecimal totalToPaid = listSchedule.get(0).getAmount().subtract(listSchedule.get(0).getPaidAmount());	
	  
	  
	  if(totalToPaid.compareTo(amountGlitem.add(accountAmount.add(creditAmount))) >= 0){
		  totalToPaid = amountGlitem.add(accountAmount.add(creditAmount));
		  
	  }
	  
	  
	  
	  BigDecimal paymentNotGlitem = new BigDecimal(0);
	  if(amountGlitem.compareTo(totalToPaid)>=0)
		  amountGlitem = totalToPaid;
	  else
		  paymentNotGlitem = totalToPaid.subtract(amountGlitem);
	  
	  BigDecimal directPayment = paymentNotGlitem;
	  BigDecimal directAmountToPaid = new BigDecimal(0);
	  BigDecimal creditAmountToPaid = new BigDecimal(0);
	  
	  //calcular cuanto se va a opagar de la cuenta y cuanto del credito
	  if(paymentNotGlitem.compareTo(new BigDecimal(0))>0){
		  
		  if(creditAmount.compareTo(new BigDecimal(0))>0){
			  
			  if(paymentNotGlitem.compareTo(creditAmount)>=0){
				  creditAmountToPaid = creditAmount;
				  directAmountToPaid = paymentNotGlitem.subtract(creditAmount);
				  
				  if(directAmountToPaid.compareTo(accountAmount)>=0){
					  directAmountToPaid = directAmountToPaid.subtract(directAmountToPaid.subtract(accountAmount));
				  }
			  }
			  else
				  creditAmountToPaid = paymentNotGlitem;		  
		  }
		  else{
			  if(paymentNotGlitem.compareTo(accountAmount)>=0){
				  directAmountToPaid = paymentNotGlitem.subtract(paymentNotGlitem.subtract(accountAmount));
			  }
			  else
				  directAmountToPaid = paymentNotGlitem;	
		  }
		 
	  }
	 
		  
		  
		  
	 
  
	  	
	  Date date = new Date();
	  Organization org;
	  Client client;
	  BusinessPartner bp;
	  String documentType = "Order";
	  String documentNo = "";
	  String docName = "";
	  if(order == null){
		  invoice = PaymentPlanDataSearchClass.getInvoice(documentId);
		  documentType = "Invoice";
		  documentNo = invoice.getDocumentNo();
		  org = invoice.getOrganization();
		  client = invoice.getClient();
		  bp = invoice.getBusinessPartner();
		  docName = invoice.getDocumentType().getName();
	  }else{ 
		  documentNo = order.getDocumentNo();
		  org = order.getOrganization();
		  client = order.getClient();
		  bp = order.getBusinessPartner();
		  docName = order.getDocumentType().getName();
	  }
	  
	  FIN_Payment  payment  = PaymentDataSearchClass.createFINPayment(this,org, client, bp, paymentMethod, account, date, totalToPaid," " + documentType +":" +documentNo,PaymentDataSearchClass.isSaleDoc(windowId));
	  GLItem glItem = null;
	  FIN_PaymentDetail paymentDetailGl = null ;
	  BigDecimal TotalAmount = totalToPaid;
	  for (GlItemDataClass glItemData : listGlitems) {
		  if(TotalAmount.compareTo(new BigDecimal(0)) == 1){
			  
			  if(glItemData.getAmount().compareTo(new BigDecimal(0)) == 1){
				  
				  SVFFIN_Deposit deposit = PaymentDataSearchClass.geDeposits(glItemData.getGlItem());
				  BigDecimal amount = glItemData.getAmount();
				  if(TotalAmount.compareTo(amount) == -1){
					  glItemData.setAmount(amount.subtract(TotalAmount));
					  amount = TotalAmount;
				  }
				  else{
					  glItemData.setAmount(new BigDecimal(0));
				  }
				  paymentDetailGl = PaymentDataSearchClass.createFINPaymentDetailts(org, client, bp, payment, glItemData.getGlItem(), amount.negate());
				  PaymentPlanDataSearchClass.saveFinPaymentScheduleDetailGlItem(this, client.getId(), org.getId(), amount.negate().toString(), bp.getId(), "0", paymentDetailGl.getId());
				  
				  glItem =  OBDal.getInstance().get(GLItem.class, glItemData.getGlItem().getId());
				  PaymentDataSearchClass.createSVFFinPayment(this,org, client, payment, deposit,Boolean.FALSE,invoice,order,amount);
				  TotalAmount = TotalAmount.subtract(amount);
			  }
		  }
	  }
	  
	  for (FIN_PaymentSchedule fin_PaymentSchedule : listSchedule) { 
		  if(auxAmountGlitem.compareTo(new BigDecimal(0))>0){
			  
			  int caseCompare = totalToPaid.compareTo(auxAmountGlitem);
			  switch (caseCompare) {
				case 1:
					List<String> resultList = PayGlitem(payment, fin_PaymentSchedule,auxAmountGlitem,invoice,order,paymentMethod,glItem);
					 auxAmountGlitem = new BigDecimal(0);
					 if(directPayment.compareTo(new BigDecimal(0))>0){
						  generateDirectPaymentDetails(documentId,fin_PaymentSchedule,account,payment,directPayment,documentNo,documentType);
					  }
					break;
				case -1:
					 PayGlitem(payment, fin_PaymentSchedule,totalToPaid,invoice,order,paymentMethod,glItem);
					 auxAmountGlitem = auxAmountGlitem.subtract(totalToPaid);
					break;
	
				default:
					 PayGlitem(payment, fin_PaymentSchedule,auxAmountGlitem,invoice,order,paymentMethod,glItem);
					 auxAmountGlitem = new BigDecimal(0);
					break;
				}
			 
		  }else{
			  if(directPayment.compareTo(new BigDecimal(0))>0){
				  BigDecimal toPaid = directPayment;
				  if(directPayment.compareTo(totalToPaid)>0)
					  toPaid = directPayment.subtract(directPayment.subtract(totalToPaid));
				  generateDirectPaymentDetails(documentId,fin_PaymentSchedule,account,payment,directPayment,documentNo,documentType);
			  }
		  }

		  PaymentDataSearchClass.createFINPaymentTransaction(this,payment.getOrganization(), payment.getClient(), payment.getBusinessPartner(), null, account, payment, date, directAmountToPaid,  docName + " :"+documentNo,PaymentDataSearchClass.isSaleDoc(windowId),directPayment);
		  if(credit != null && creditAmountToPaid.compareTo(new BigDecimal(0))>0)
			  PaymentDataSearchClass.UpdateCredit(credit, creditAmountToPaid, payment.getDocumentNo());
	  }
		  	  
	  
	  PaymentPlanDataSearchClass.updateFinPayment(this, payment.getId());
	  if(invoice != null){
		  Boolean isPaid = false;
		  if(PaymentPlanDataSearchClass.getToPaidAmount(invoice).compareTo(new BigDecimal(0)) == 0)
			  isPaid = true;
		  PaymentDataSearchClass.updateInvoice(invoice.getId(), PaymentPlanDataSearchClass.getPaidAmount(invoice),PaymentPlanDataSearchClass.getToPaidAmount(invoice),isPaid);
	  }
	  return listGlitems;
  }
  
  private void generateDirectPayment(String documentId,FIN_PaymentSchedule schedule,FIN_FinancialAccount account,String windowsId) throws Exception{
	   
	  Order order = PaymentPlanDataSearchClass.getOrder(documentId);
	  Invoice invoice = null;
	  //BigDecimal toPaid = totalToPaid(documentId);
	  List<FIN_PaymentSchedule> listSchedule =  new ArrayList<FIN_PaymentSchedule>();
	  listSchedule.add(schedule);
	  BigDecimal totalToPaid = totalToPaid(listSchedule);
	  Date date = new Date();
	  Organization org;
	  Client client;
	  BusinessPartner bp;
	  String documentType = "Order";
	  String documentNo = "";
	  String docName = "";
	  if(order == null){
		  invoice = PaymentPlanDataSearchClass.getInvoice(documentId);
		  documentType = "Invoice";
		  documentNo = invoice.getDocumentNo();
		  org = invoice.getOrganization();
		  client = invoice.getClient();
		  bp = invoice.getBusinessPartner();
		  docName = invoice.getDocumentType().getName();
	  }else{ 
		  documentNo = order.getDocumentNo();
		  org = order.getOrganization();
		  client = order.getClient();
		  bp = order.getBusinessPartner();
		  docName = order.getDocumentType().getName();
	  }
	  
	  
	  FIN_Payment payment = PaymentDataSearchClass.createFINPayment(this,org, client, bp, schedule.getFinPaymentmethod(), account, date, totalToPaid," " + docName +":" +documentNo,PaymentDataSearchClass.isSaleDoc(windowsId));
	  FIN_FinaccTransaction transaction = PaymentDataSearchClass.createFINPaymentTransaction(this,org, client, bp, null, account, payment, date, totalToPaid, docName + " :"+documentNo,PaymentDataSearchClass.isSaleDoc(windowsId),totalToPaid);
	  for (FIN_PaymentSchedule fin_PaymentSchedule : listSchedule) {  
		  FIN_PaymentSchedule paymentSchedule = PaymentPlanDataSearchClass.updateFINPaymentSchedulePaid(fin_PaymentSchedule, totalToPaid);
		  FIN_PaymentDetail paymentDetail = PaymentDataSearchClass.createFINPaymentDetailts(org, client, bp, payment, null, totalToPaid);
		  FIN_PaymentScheduleDetail paymentScheduleDetail;
		  if(documentType.equalsIgnoreCase("Order")){
			  List<FIN_PaymentScheduleDetail> listDetailSchedule = PaymentPlanDataSearchClass.getPaymentDetailOrder(fin_PaymentSchedule);
			  paymentScheduleDetail = listDetailSchedule.get(0);
		  }else{
			  List<FIN_PaymentScheduleDetail> listDetailSchedule = PaymentPlanDataSearchClass.getPaymentDetailInvoice(fin_PaymentSchedule);
			  paymentScheduleDetail = listDetailSchedule.get(0);  
		  }
		  String paymentScheduleDetailId = PaymentPlanDataSearchClass.updateFinPaymentScheduleDetail(this, paymentScheduleDetail.getId(),paymentDetail.getId(), totalToPaid.toString());
	  }
	  PaymentPlanDataSearchClass.updateFinPayment(this, payment.getId());
	  
  }
  
  private void generateDirectPaymentDetails(String documentId,FIN_PaymentSchedule paymentSchedule,FIN_FinancialAccount account,FIN_Payment payment,BigDecimal totalToPaid,String documentNo,String documentType) throws Exception{
	  Date date = new Date();
	  paymentSchedule = PaymentPlanDataSearchClass.updateFINPaymentSchedulePaid(paymentSchedule, totalToPaid); 
	  FIN_PaymentDetail paymentDetail = PaymentDataSearchClass.createFINPaymentDetailts(payment.getOrganization(), payment.getClient(), payment.getBusinessPartner(), payment, null, totalToPaid);
	  String paymentScheduleDetailId = "";

	  if(documentType.equalsIgnoreCase("Order")){
		  paymentScheduleDetailId = PaymentPlanDataSearchClass.createFINPaymentScheduleDetailOrder(this,payment.getOrganization().getId(), payment.getClient().getId(), payment.getBusinessPartner().getId(), paymentSchedule, payment.getPaymentMethod().getId(), totalToPaid, paymentSchedule.getDueDate(), paymentSchedule.getExpectedDate(), "0");
	  }else{
		  paymentScheduleDetailId = PaymentPlanDataSearchClass.createFINPaymentScheduleDetailInvoice(this,payment.getOrganization().getId(), payment.getClient().getId(), payment.getBusinessPartner().getId(), paymentSchedule, payment.getPaymentMethod().getId(), totalToPaid, paymentSchedule.getDueDate(), paymentSchedule.getExpectedDate(), "0"); 
	  }

	  PaymentPlanDataSearchClass.updateFinPaymentScheduleDetail(this, paymentScheduleDetailId,paymentDetail.getId(), totalToPaid.toString());
  }
  
 
  
  private BigDecimal totalToPaid(List<FIN_PaymentSchedule> listSchedule){
	  
	  BigDecimal totalToPaid = new BigDecimal(0);

	  for (FIN_PaymentSchedule fin_PaymentSchedule : listSchedule) {
		  BigDecimal amount = fin_PaymentSchedule.getAmount().subtract(fin_PaymentSchedule.getPaidAmount());
		  totalToPaid = totalToPaid.add(amount);
	  }
	  
	  return totalToPaid;
  }
  
  private void clearSessionValue(VariablesSecureApp vars) {
	    vars.removeSessionValue("fFinancialAccountId");
	    vars.removeSessionValue("inpwindowId");
	    vars.removeSessionValue("inpTabId");
	  }
  
  public List<Order> AddToOrderList(List<Order> list,Order order)
  {
	  boolean exist = false;
	  for (Order orderItem : list) {
		if(orderItem.getId().equalsIgnoreCase(order.getId()))
				exist = true;
	  }
	  if(!exist)
		  list.add(order);
	  return list;
  }
 
  private List<Order> GetRelatedOrderByGoodReceipt(List<Order> list,Invoice invoice) throws ServletException{
		String mInOutId = invoice.getSvfsvMinout();
	  	if((mInOutId != null)&&(!mInOutId.equalsIgnoreCase(""))){
	  		ShipmentInOut inOut = OBDal.getInstance().get(ShipmentInOut.class, mInOutId);
	  		if(inOut != null){
	  			List<ShipmentInOutLine> lines = inOut.getMaterialMgmtShipmentInOutLineList();
	  			for (ShipmentInOutLine shipmentInOutLine : lines) {
	  				Order comercialInvoice = shipmentInOutLine.getSvfsvOrigCInvoice();
	  				if(comercialInvoice != null){
	  					List<Order> listOrderAux = PaymentDataSearchClass.getOrderRelatedProforma(this,comercialInvoice.getId());
	  					for (Order order : listOrderAux) {
	  						AddToOrderList(list,order);
						}
	  				}
				}
	  		}
	  		
	  	}
	  	return list;
}
  
  private void printPage(HttpServletRequest request, HttpServletResponse response,
	      VariablesSecureApp vars,String documentId,String windowId) throws IOException, ServletException {
	    // Check for permissions to apply modules from application server.
	    
	    String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	    final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurnitures/services/process/PaymentProcess").createXmlDocument();
	    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
	    xmlDocument.setParameter("theme", vars.getTheme());
	    {
	      final OBError myMessage = vars.getMessage("PaymentProcess");
	      vars.removeMessage("PaymentProcess");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	    }
	    
	    Order order = null;
	    Invoice invoice = null;
	    Organization org = null;
	    Client client = null;
	    List<FIN_PaymentSchedule> list = new ArrayList<FIN_PaymentSchedule>();
	    BigDecimal totalPaid = new BigDecimal(0);
	    order = PaymentPlanDataSearchClass.getOrder(documentId);
	    List<Order> listOrder = new  ArrayList<Order>();
	    List<FIN_Payment> creditList;
	    if(order == null){
	    	invoice = PaymentPlanDataSearchClass.getInvoice(documentId);
	    	listOrder = PaymentDataSearchClass.getInvoiceRelatedProforma(this,documentId);
	    	
	    	//get related comercial invoice if document was created from good receipt;
	    	listOrder = GetRelatedOrderByGoodReceipt(listOrder,invoice);
	    	
	    	org = invoice.getOrganization();
	    	xmlDocument.setParameter("addDocumentNotxt", invoice.getDocumentNo());
	    	xmlDocument.setParameter("addDocumentTypetxt", "Invoice");
	    	client = invoice.getClient();
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromInvoice(invoice);
	    	creditList = PaymentDataSearchClass.getCreditsSql(this, invoice.getBusinessPartner().getId());
	    	if(list.size() > 0) 
	    		totalPaid = getTotalAsignablePaid(list);  
	    	xmlDocument.setParameter("addDocumentTotalAmount", invoice.getGrandTotalAmount().toString());
	    	
	    }else{
	    	org = order.getOrganization();
	    	listOrder = PaymentDataSearchClass.getOrderRelatedProforma(this,documentId);
	    	listOrder.add(order);
	    	xmlDocument.setParameter("addDocumentNotxt", order.getDocumentNo());
	    	xmlDocument.setParameter("addDocumentTypetxt", "Order");
	    	client = order.getClient();
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromOrder(order);
	    	creditList = PaymentDataSearchClass.getCreditsSql(this, order.getBusinessPartner().getId());
	    	if(list.size() > 0) 
	    		totalPaid = getTotalAsignablePaid(list);  
	    	xmlDocument.setParameter("addDocumentTotalAmount", order.getGrandTotalAmount().toString());
	 	    
	    }
	    xmlDocument.setParameter("addDocumentToPaid", totalPaid.toString());
		
	    //Payment method for payment plan
	    PDFieldProvider[] paymentMethodsArr = PaymentPlanDataSearchClass.getPaymentMethodsPDFields(client);
	    xmlDocument.setParameter("paramAmountFrom", "0");
	    String selected = new String();
		if(paymentMethodsArr.length > 0){
			selected = paymentMethodsArr[0].getField("id");
			xmlDocument.setParameter("addPaymentMethodId", selected);
		}
		xmlDocument.setData("reportPaymentMethod_Id", "liststructure", paymentMethodsArr);
 
		List<FIN_FinancialAccount> listAccount = PaymentDataSearchClass.getBranchAccountList(this,org.getId());
		List<GLItem> glitems = PaymentDataSearchClass.loadGLItemByOrder(listOrder);
		String tableGlItems = generateTableGlitems(glitems, new ArrayList<String>());
		
		String defaultDocId = "";
		if(PaymentDataSearchClass.isSaleDoc(windowId)){
			for (FIN_FinancialAccount account : listAccount) {
				if(account.getName().equalsIgnoreCase("Undeposited Funds"))
					defaultDocId = account.getId();
			}

		}
		String table = generateTable(list,listAccount, new ArrayList<String>(),defaultDocId,glitems,creditList);		
		xmlDocument.setParameter("desc", table);
		xmlDocument.setParameter("descGlitems", tableGlItems);
		
	    response.setContentType("text/html; charset=UTF-8");
	    final PrintWriter out = response.getWriter();

	    out.println(xmlDocument.print());
	    out.close();
	  }
  
  
  private void printPageResponse(HttpServletRequest request, HttpServletResponse response,VariablesSecureApp vars,String documentId,String selectedPaymentMethod, List<String> selectedPayments,List<String> selectedGlitems,String windowId,String errorMessage) throws IOException, ServletException {
	    // Check for permissions to apply modules from application server.
	    
	    String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	    final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurnitures/services/process/PaymentProcess").createXmlDocument();
	    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
	    xmlDocument.setParameter("theme", vars.getTheme());
	    {
	      final OBError myMessage = vars.getMessage("PaymentProcess");
	      vars.removeMessage("PaymentProcess");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	    }
	    
	    Order order = null;
	    Invoice invoice = null;
	    Organization org = null;
	    Client client = null;
	    List<FIN_PaymentSchedule> list = new ArrayList<FIN_PaymentSchedule>();
	    BigDecimal totalPaid = new BigDecimal(0);
	    order = PaymentPlanDataSearchClass.getOrder(documentId);
	    List<Order> listOrder = new  ArrayList<Order>();
	    List<FIN_Payment> creditList;
	    if(order == null){
	    	invoice = PaymentPlanDataSearchClass.getInvoice(documentId);
	    	listOrder = PaymentDataSearchClass.getInvoiceRelatedProforma(this,documentId);
	    	
	    	//get related comercial invoice if document was created from good receipt;
	    	listOrder = GetRelatedOrderByGoodReceipt(listOrder,invoice);
	    	
	    	org = invoice.getOrganization();
	    	xmlDocument.setParameter("addDocumentNotxt", invoice.getDocumentNo());
	    	xmlDocument.setParameter("addDocumentTypetxt", "Invoice");
	    	client = invoice.getClient();
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromInvoice(invoice);
	    	creditList = PaymentDataSearchClass.getCreditsSql(this, invoice.getBusinessPartner().getId());
	    	if(list.size() > 0) 
	    		totalPaid = getTotalAsignablePaid(list);  
	    	xmlDocument.setParameter("addDocumentTotalAmount", invoice.getGrandTotalAmount().toString());
	    	
	    }else{
	    	org = order.getOrganization();
	    	listOrder = PaymentDataSearchClass.getOrderRelatedProforma(this,documentId);
	    	listOrder.add(order);
	    	xmlDocument.setParameter("addDocumentNotxt", order.getDocumentNo());
	    	xmlDocument.setParameter("addDocumentTypetxt", "Order");
	    	client = order.getClient();
	    	list = PaymentPlanDataSearchClass.getPaymentPlanFromOrder(order);
	    	creditList = PaymentDataSearchClass.getCreditsSql(this, order.getBusinessPartner().getId());
	    	if(list.size() > 0) 
	    		totalPaid = getTotalAsignablePaid(list);  
	    	xmlDocument.setParameter("addDocumentTotalAmount", order.getGrandTotalAmount().toString());
	 	    
	    }
	    xmlDocument.setParameter("addDocumentToPaid", totalPaid.toString());
		xmlDocument.setParameter("paramAmountFrom", "0");
	    
	    PDFieldProvider[] paymentMethodsArr = PaymentPlanDataSearchClass.getPaymentMethodsPDFields(client);
		
	    //Payment method for payment plan
		xmlDocument.setData("reportPaymentMethod_Id", "liststructure", paymentMethodsArr);
		xmlDocument.setParameter("addPaymentMethodId", selectedPaymentMethod);
		
		List<FIN_FinancialAccount> listAccount = PaymentDataSearchClass.getBranchAccountList(this,org.getId());
		List<GLItem> glitems = PaymentDataSearchClass.loadGLItemByOrder(listOrder);
		String tableGlItems = generateTableGlitems(glitems, new ArrayList<String>());
		
		String defaultDocId = "";
		if(PaymentDataSearchClass.isSaleDoc(windowId)){
			for (FIN_FinancialAccount account : listAccount) {
				if(account.getName().equalsIgnoreCase("Undeposited Funds"))
					defaultDocId = account.getId();
			}

		}
		String table = generateTable(list, listAccount,selectedPayments,defaultDocId,glitems,creditList);		
		xmlDocument.setParameter("desc", table);
		xmlDocument.setParameter("descGlitems", tableGlItems);
		
		xmlDocument.setParameter("errorMessageLbl", errorMessage);
		
	    response.setContentType("text/html; charset=UTF-8");
	    final PrintWriter out = response.getWriter();
	    
	    

	    out.println(xmlDocument.print());
	    out.close();
	  }
  
  
  private BigDecimal getTotalAsignablePaid(List<FIN_PaymentSchedule> list ){
	  return list.get(0).getAmount();
  }
  
  private boolean existInList(List<String> list,String id){

	  boolean founded = false;
	  for(int i=0;((i<list.size())&&(!founded));i++){
		  if(list.get(i).equalsIgnoreCase(id))
			  founded = true;
	  }
	  return founded;
  }
  
  private String generateTable(List<FIN_PaymentSchedule> list,List<FIN_FinancialAccount> listAccounts, List<String> selectedIds,String defaultDoc,List<GLItem> listGlitem,List<FIN_Payment> creditList){
	   
	  String generalRows = new String();
	  if(list.size() == 0){
		  generalRows = " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_0\">                              "+
				  " 	<td class=\"DataGrid_Body_LineNoCell\"></td>                                                            "+
				  "     <td class=\"DataGrid_Body_Cell\">There is not related data.</td>                                     "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                                             "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                                             "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                           "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                           "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                           "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                           "+
				  " </tr>                                                                                               ";
	  }
	  else{
		  int pos = 0;
		  for (FIN_PaymentSchedule transData : list) {
			  String checked = "";
			  if(existInList(selectedIds, transData.getId()))
				  checked = "checked";
				  
			String bp = "&nbsp";
			String document = "&nbsp";
			String documentNo = "";
			Invoice invoice = transData.getInvoice();
			Order order = transData.getOrder();
			String bPartnerId = "";
			if(invoice != null){
				bPartnerId = invoice.getBusinessPartner().getId();
				documentNo = invoice.getDocumentNo();
			}
			else{
				bPartnerId = order.getBusinessPartner().getId();				
				documentNo = order.getDocumentNo();
			}
			String paymentMethod = transData.getFinPaymentmethod().getName();
			BigDecimal toPaid = transData.getAmount().subtract(transData.getPaidAmount());
 		    generalRows +=  " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_"+ String.valueOf(pos) +"\">                              "+
 		    		" 	<td class=\"DataGrid_Body_LineNoCell\">                                                             ";
 		    		if(transData.getAmount().subtract(transData.getPaidAmount()).compareTo(new BigDecimal(0)) > 0){
 		    			generalRows +=  " 		<span class=\"Checkbox_container_NOT_Focused\">                                                 "+
					  " 			<input type=\"checkbox\" value=\"" + transData.getId() + "\" name=\"inpPayPlanId\"  id=\"inpPayPlanId\" " + checked + " ></input>   "+
					  " 			<input type=\"hidden\" value=\"" + toPaid.toString() + "\" name=\"" + transData.getId() + "\" id=\"" + transData.getId() + "\"></input>   "+
					  " 		</span>                                                                                       ";
 		    		}else{
 		    			generalRows +=  "&nbsp";
 		    		}
 		    		generalRows +=  " 	</td>                                                                                             "+  
		    		  " 	<td class=\"DataGrid_Body_Cell\">" + documentNo + "</td>                                     "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + paymentMethod + "</td>                                     "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getAmount().toString() + "</td>                                                             "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getPaidAmount().toString() + "</td>                                           "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getOutstandingAmount().toString() + "</td>                                     ";
 		    		if(transData.getAmount().subtract(transData.getPaidAmount()).compareTo(new BigDecimal(0)) > 0){
 		    			generalRows +=  dropFinancialAccount(listAccounts,transData.getId(),null,defaultDoc) +"      ";
 		    			generalRows +=  dropCredit(creditList,transData.getId(),defaultDoc) + "       " ;
 		    			generalRows +=  dropGlitem(listGlitem, transData.getId(), transData, defaultDoc) +" </tr>     "  ;
 		    		}else{
 		    			generalRows +=  dropFinancialAccount(listAccounts,transData.getId(),transData,defaultDoc) +"      ";;
 		    			generalRows +=  " <tr><td></td><td></td></tr>     "  ;
 		    		}      
			  pos++;
		}
	  }
		  
	  String generalTable = " <table class=\"DataGrid_Header_Table DataGrid_Body_Table\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">                                             "+
			  "     <tbody>                                                                                                                                        "+
			  "         <tr class=\"Popup_Client_Selector_DataGrid_HeaderRow\">                                                                                      "+
			  "         <td class=\"DataGrid_Body_LineNoCell\" width=\"28\">                                                                                       "+
			  "                <span class=\"Checkbox_container_NOT_Focused\">                                                                                      "+
			  "                     <input type=\"checkbox\" onclick=\"markAll(document.frmMain.inpPayPlanId, this.checked);return true;\" name=\"inpPayTodos\"></input>         "+
			  "                 </span>                                                                                                                            "+
			  "             </td>                                                                                                                                  "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"50\">Doc No</th>                                                                             "+
			  " 			<th class=\"DataGrid_Header_Cell\" width=\"80\">P.Method</th>                                                                                  "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"50\" style=\"align:right\">Amount</th>                                                                                "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"50\" style=\"align:right\">Paid</th>                                                                             "+
			  " 			<th class=\"DataGrid_Header_Cell\" width=\"70\" style=\"align:right\">Outstanding</th>                                                                                  "+
			  " 			<th class=\"DataGrid_Header_Cell\" width=\"160\" style=\"align:right\">Account/Balance</th>                                                                                  "+
			  " 			<th class=\"DataGrid_Header_Cell\" width=\"160\" style=\"align:right\">Credit/Amount</th>                                                                                  "+
			  " 			<th class=\"DataGrid_Header_Cell\" width=\"160\" style=\"align:right\">Glitem/Amount</th>                                                                                  "+
			  //" 			<th class=\"DataGrid_Header_Cell\" width=\"100\" style=\"align:right\">Glitem Amount</th>                                                                                  "+
			  "         </tr>                                                                                                                                      "+
			  generalRows + 
			  "     </tbody>                                                                                                                                       "+
			  " </table>       "
			  + hdFinancialAccountAmounts(listAccounts)
			  + hdGlitemsAmounts(listGlitem)
	  		  + hdCreditsAmounts(creditList);
	  		  
	  return generalTable;
	
  }
  
 private String dropFinancialAccount(List<FIN_FinancialAccount> list,String idPayment,FIN_PaymentSchedule schedule,String defaultId) {
	 	 String select = "";
	 	 
	 	 String amount = "0";
	 	 String amountHds = "";
	 	 int pos =0;
	 	 if(schedule == null){
	 		select = "<td class=\"DataGrid_Body_Cell\"><table><tr><td class=\"DataGrid_Body_Cell\">"
		 	 		+ "<select name=\"idAccount" + idPayment + "\" onchange=\"updateBalance(this);return true;\" >";
	 		if(defaultId == null || defaultId.equalsIgnoreCase(""))
	 			select += "<option value=\"-1\" selected>-No account selected-</option>";
		 	for (FIN_FinancialAccount fin_FinancialAccount : list) {
		 		String selectedStr = "";
		 		if(defaultId.equalsIgnoreCase(fin_FinancialAccount.getId())){
		 			selectedStr = "selected";
		 			amount = fin_FinancialAccount.getCurrentBalance().toString();
		 		}
		 		 select += "<option value=\""+ fin_FinancialAccount.getId() +"\" "+ selectedStr +">"+ fin_FinancialAccount.getName() +"</option>";
		 		 
			}
		 	select += "</select></td></tr>";
		 	String inputValue = "<tr><td class=\"DataGrid_Body_Cell\"><input type=\"text\" id=\"idAccount" + idPayment + "inpValue\" name=\"idAccount" + idPayment + "inpAmount\" value=\"" + amount.toString() +"\"></td></tr></table></td>";
		 	select += inputValue; 
	 	 }
	 	 else{
	 		select = "";
	 		List<FIN_PaymentScheduleDetail> listDetails =  schedule.getFINPaymentScheduleDetailInvoicePaymentScheduleList();
	 		if(listDetails.size() == 0){
	 			select = "<td class=\"DataGrid_Body_Cell\" colspan=\"3\">Paid by GlItem</td>"; 
	 		}
	 		else{
	 			select = "<td class=\"DataGrid_Body_Cell\" colspan=\"3\">"; 
	 			for (FIN_PaymentScheduleDetail fin_PaymentScheduleDetail : listDetails) {
	 				if(fin_PaymentScheduleDetail.getPaymentDetails() != null)
	 					select += fin_PaymentScheduleDetail.getPaymentDetails().getFinPayment().getAccount().getName() + ",";
	 				else
	 					select += "Paid by glItem,";
				}
	 			select = select.substring(0,select.length()-1) + "</td>";
	 		}  
	 	 }
	 	
	 	return select;
 }
 
 private String dropGlitem(List<GLItem> list,String idPayment,FIN_PaymentSchedule schedule,String defaultId) {
 	 String select = "";
 	 
 	 String amount = "0";
 	 String amountHds = "";
 	 int pos =0;

 		select = "<td class=\"DataGrid_Body_Cell\"><table><tr><td class=\"DataGrid_Body_Cell\">"
	 	 		+ "<select name=\"idGlitem" + idPayment + "\"  onchange=\"updateDeposit(this);return true;\">";
 		if(defaultId == null || defaultId.equalsIgnoreCase(""))
 			select += "<option value=\"-1\" selected>-No deposit selected-</option>";
	 	for (GLItem item : list) {
	 		String selectedStr = "";
	 		/*if(defaultId.equalsIgnoreCase(item.getId())){
	 			selectedStr = "selected";
	 			amount = item.getCurrentBalance().toString();
	 		}*/
	 		 select += "<option value=\""+ item.getId() +"\" "+ selectedStr +">"+ item.getName() +"</option>";
	 		 
		}
	 	select += "</select></td></tr>";
	 	String inputValue = "<tr><td class=\"DataGrid_Body_Cell\"><input type=\"text\" id=\"idGlitem" + idPayment + "inpValue\" name=\"idGlitem" + idPayment + "inpAmount\" value=\"0\"></td></tr></table></td>";
	 	select += inputValue; 
 
 	
 	return select;
}
 
 private String dropCredit(List<FIN_Payment> list,String idPayment,String defaultId) {
 	 String select = "";
 	 
 	 String amount = "0";
 	 String amountHds = "";
 	 int pos =0;

 		select = "<td class=\"DataGrid_Body_Cell\"><table><tr><td class=\"DataGrid_Body_Cell\">"
	 	 		+ "<select name=\"idCredit" + idPayment + "\"  onchange=\"updateDeposit(this);return true;\">";
 		if(defaultId == null || defaultId.equalsIgnoreCase(""))
 			select += "<option value=\"-1\" selected>-No Credit selected-</option>";
 		
	 	for (FIN_Payment item : list) {
	 		String selectedStr = "";
	 		/*if(defaultId.equalsIgnoreCase(item.getId())){
	 			selectedStr = "selected";
	 			amount = item.getCurrentBalance().toString();
	 		}*/
	 		 select += "<option value=\""+ item.getId() +"\" "+ selectedStr +">"+ pos +"-" + item.getBusinessPartner().getName() +"(" + item.getGeneratedCredit() + ")"+"</option>";
	 		 
		}
	 	select += "</select></td></tr>";
	 	String inputValue = "<tr><td class=\"DataGrid_Body_Cell\"><input type=\"text\" id=\"idCredit" + idPayment + "inpValue\" name=\"idCredit" + idPayment + "inpAmount\" value=\"0\"></td></tr></table></td>";
	 	select += inputValue; 
 
 	
 	return select;
}
 
 private String hdFinancialAccountAmounts(List<FIN_FinancialAccount> list) {
 	 String hdAmount = "";
 	 BigDecimal amount = new BigDecimal(0);
 	 String amountHds = "";
 	 int pos =0;
 	 for (FIN_FinancialAccount fin_FinancialAccount : list) {
 		hdAmount += "<input type=\"hidden\" value=\"" + fin_FinancialAccount.getCurrentBalance().toString() + "\" name=\"idAmount"+ fin_FinancialAccount.getId() + "\" id=\"idAmount"+ fin_FinancialAccount.getId() +"\"></input>";;	 
	}
 	return hdAmount;
}
 
 private String hdGlitemsAmounts(List<GLItem> list) {
 	 String hdAmount = "";
 	 String amountHds = "";
 	 int pos =0;
 	
 	 for (GLItem gl : list) {
 		 String amount = PaymentDataSearchClass.getDepositBalance(gl.getId()).toString();
 		hdAmount += "<input type=\"hidden\" value=\"" + amount + "\" name=\"idAmountHd"+ gl.getId() + "\" id=\"idAmountHd"+ gl.getId() +"\"></input>";;	 
	}
 	return hdAmount;
}
  
 private String hdCreditsAmounts(List<FIN_Payment> list) {
 	 String hdAmount = "";
 	 String amountHds = "";
 	 int pos =0;
 	
 	 for (FIN_Payment payment : list) {
 		 String amount = payment.getGeneratedCredit().toString();
 		hdAmount += "<input type=\"hidden\" value=\"" + amount + "\" name=\"idAmountHd"+ payment.getId() + "\" id=\"idAmountHd"+ payment.getId() +"\"></input>";;	 
	}
 	return hdAmount;
}
 
  private String generateTableGlitems(List<GLItem> list, List<String> selectedIds){
	  String generalRows = new String();
	  if(list.size() == 0){
		  generalRows =   " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_0\">                              "+
						  "     <td class=\"DataGrid_Body_Cell\" colspan=\"3\">There is not related data.</td>                                     "+
						  " </tr>                                                                                               ";
	  }
	  else{
		  int pos = 1;
		  for (GLItem transData : list) {
			  String checked = "";
			  if(existInList(selectedIds, transData.getId()))
				  checked = "checked";
				  
			String bp = "&nbsp";
			String document = "&nbsp";
			String documentNo = "";
			String amount = PaymentDataSearchClass.getDepositBalance(transData.getId()).toString();
			
 		    generalRows +=  " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_"+ String.valueOf(pos) +"\">                              "+
 		    		 " 	<td class=\"DataGrid_Body_Cell\">" + transData.getName() + "</td>                                     "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getDescription() + "</td>                              "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + amount + "</td>                                                  "+
					  " </tr>                                                                                               ";
			  pos++;
		}
	  }
		  
	  String generalTable = " <table class=\"DataGrid_Header_Table DataGrid_Body_Table\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">                                             "+
			  "     <tbody>                                                                                                                                        "+
			  "         <tr class=\"Popup_Client_Selector_DataGrid_HeaderRow\">                                                                                      "+
			 "             <th class=\"DataGrid_Header_Cell\" width=\"100\">Name</th>                                                                             "+
			  " 			<th class=\"DataGrid_Header_Cell\" width=\"120\">Description</th>                                                                      "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"70\" style=\"align:right\">Amoumt</th>                                                      "+
			 "         </tr>                                                                                                                                      "+
			  generalRows + 
			  "     </tbody>                                                                                                                                       "+
			  " </table>                                                                                                                                           ";
	  return generalTable;
	
  }
  
 
  
 
  public String getServletInfo() {
    return "GenerateInvoicesmanual Servlet. This Servlet was made by Pablo Sarobe";
  } // end of getServletInfo() method
}
