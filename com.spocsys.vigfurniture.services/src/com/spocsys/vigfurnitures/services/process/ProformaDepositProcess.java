/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2012 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.formula.atp.DateParser;
import org.apache.poi.ss.formula.ptg.IntPtg;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.event.PaidStatusEventHandler;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetailV;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.vigfurniture.financial.SVFFIN_Deposit;
import com.spocsys.vigfurniture.financial.SVFFIN_Deposit_Mngt;
import com.spocsys.vigfurniture.financial.SVFFIN_Payments;
import com.spocsys.vigfurnitures.services.utils.DataSearchClass;
import com.spocsys.vigfurnitures.services.utils.PDFieldProvider;
import com.spocsys.vigfurnitures.services.utils.PaymentDataSearchClass;
import com.spocsys.vigfurnitures.services.utils.PaymentPlanDataSearchClass;

public class ProformaDepositProcess extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    
    if (vars.commandIn("DEFAULT")) {
    	
    	clearSessionValue(vars);
    	String strWindow = vars.getStringParameter("inpwindowId");
    	String strTab = vars.getStringParameter("inpTabId");
    	String documentId = vars.getStringParameter("C_Order_ID");
    	if((documentId == null)||(documentId.equalsIgnoreCase(""))){
    		documentId = vars.getStringParameter("C_Invoice_ID");
    	}

    	String strWindowPath = Utility.getTabURL(strTab, "R", true);
    	vars.setSessionValue("inpwindowId", strWindow);
    	vars.setSessionValue("inpTabId", strTab);
    	vars.setSessionValue("documentId", documentId);
    	
        //printPageDataSheet(response, vars);C_Order_ID
   		try {
			printPage(request,response,vars,documentId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			String result = "Transaction successfully";
    		OBError myError = new OBError();
			myError.setType("Error");
			result = " There was an error in the generation. Error:"+e.getMessage();
			printPageClosePopUp(response, vars, strWindowPath);
		}
    	
    }else if (vars.commandIn("generate")) {
    	
    	String documentId = vars.getGlobalVariable("", "documentId");
    	String strWindow = vars.getGlobalVariable("", "inpwindowId");
    	String strTab = vars.getGlobalVariable("", "inpTabId");
    	BigDecimal value = new BigDecimal(vars.getStringParameter("paramAmountFrom"));
    	String selectedMethod = vars.getStringParameter("inpPaymentMethodId");
    	String selectedAccount = vars.getStringParameter("inpFinancialAccountId");
    	String selectedDate = vars.getStringParameter("inpDateTo");
    	
    	
    	
    	String strWindowPath = Utility.getTabURL(strTab, "R", true);
    	if (strWindowPath.equals(""))
            strWindowPath = strDefaultServlet;
    		String result = "Transaction successfully";
    		OBError myError = new OBError();
	    	try {
	    			generatePayment(documentId,selectedMethod,selectedAccount,selectedDate,value);
					myError.setType("Info");
	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				myError.setType("Error");
				result = " There was an error in the generation. Error:"+e.getMessage();
				e.printStackTrace();
			}
	        myError.setMessage(result);
	        vars.setMessage(strTab, myError);
	        try {
				printPage(request,response,vars,documentId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				myError.setType("Error");
				result = " There was an error in the generation. Error:"+e.getMessage();
				printPageClosePopUp(response, vars, strWindowPath);
			}
    }else{
    	String strTab = vars.getGlobalVariable("", "inpTabId");
    	String strWindowPath = Utility.getTabURL(strTab, "R", true);
    	printPageClosePopUp(response, vars, strWindowPath);
    }
  }
  
  private void generatePayment(String documentId,String paymentMethodId, String financialAccountId, String strDate ,BigDecimal amount) throws Exception{
	  Order order = OBDal.getInstance().get(Order.class, documentId);
	  FIN_PaymentMethod paymentMethod = OBDal.getInstance().get(FIN_PaymentMethod.class, paymentMethodId);
	  FIN_FinancialAccount account = OBDal.getInstance().get(FIN_FinancialAccount.class, financialAccountId);
	  Date date = new Date();
	  if(strDate != null)
	  date = FIN_Utility.getDate(strDate);
	  
	  SVFFIN_Deposit_Mngt depositMng = PaymentDataSearchClass.getDepositMng(order.getOrganization(),order.getClient(), order.getBusinessPartner());
	  GLItem glItem = PaymentDataSearchClass.getGLItem(order.getOrganization(),order.getClient(), order.getBusinessPartner(), order.getId(), order.getDocumentNo());
	  SVFFIN_Deposit deposit = PaymentDataSearchClass.getSvffin_Deposit(order, depositMng, glItem);
	  
	  FIN_Payment payment = PaymentDataSearchClass.createFINPayment(this,order.getOrganization(), order.getClient(), order.getBusinessPartner(), paymentMethod, account, date, amount," Proforma invoice:"+order.getDocumentNo(),false);
	  
	  
	  FIN_FinaccTransaction transaction = PaymentDataSearchClass.createFINPaymentTransaction(this,order.getOrganization(), order.getClient(), order.getBusinessPartner(), glItem, account, payment, date, amount, " Proforma invoice:"+order.getDocumentNo(),false,amount);
	  String fin_paymentdetailId =  PaymentDataSearchClass.createFINPaymentDetailtsSQL(this, order.getClient().getId(),order.getOrganization().getId(), payment.getId(), glItem.getId(),amount.toString());
	  PaymentPlanDataSearchClass.updateFinPayment(this, payment.getId());
	  PaymentDataSearchClass.createSVFFinPayment(this,order.getOrganization(), order.getClient(), payment, deposit,Boolean.TRUE,null,order,payment.getAmount());
	  FIN_PaymentDetail payDetail = OBDal.getInstance().get(FIN_PaymentDetail.class, fin_paymentdetailId);
	  PaymentPlanDataSearchClass.saveFinPaymentScheduleDetailGlItem(this, order.getClient().getId(), order.getOrganization().getId(), amount.toString(), order.getBusinessPartner().getId(), "0", payDetail.getId()); 
  }
  
  private void clearSessionValue(VariablesSecureApp vars) {
	    vars.removeSessionValue("fFinancialAccountId");
	    vars.removeSessionValue("inpwindowId");
	    vars.removeSessionValue("inpTabId");
	  }
  private void printPage(HttpServletRequest request, HttpServletResponse response,
	      VariablesSecureApp vars,String documentId) throws Exception {
	    // Check for permissions to apply modules from application server.
	    
	    String fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	    final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurnitures/services/process/ProformaDepositProcess").createXmlDocument();
	    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
	    xmlDocument.setParameter("theme", vars.getTheme());
	    {
	      final OBError myMessage = vars.getMessage("ProformaDepositProcess");
	      vars.removeMessage("ProformaDepositProcess");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	    }
	    
	    xmlDocument.setParameter("dateTo", "");
	    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
	    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
	    
	    Order order = null;
	    Invoice invoice = null;
	    Organization org = null;
	    Client client = null;
	    order = PaymentPlanDataSearchClass.getOrder(documentId);

    	org = order.getOrganization();
    	String orgId = org.getId();
    	client = order.getClient();
    	xmlDocument.setParameter("addDocumentNotxt", order.getDocumentNo());
    	xmlDocument.setParameter("addDocumentTypetxt", "Order");
    	xmlDocument.setParameter("addclientName", order.getClient().getName());
    	xmlDocument.setParameter("addbpartnerName", order.getBusinessPartner().getName());
    	
    	SVFFIN_Deposit_Mngt depositMng = PaymentDataSearchClass.getDepositMng(org, client, order.getBusinessPartner());
    	GLItem glItem = PaymentDataSearchClass.getGLItem(org, client, order.getBusinessPartner(), order.getId(), order.getDocumentNo());
    	SVFFIN_Deposit deposit = PaymentDataSearchClass.getSvffin_Deposit(order, depositMng, glItem);
    	
    	xmlDocument.setParameter("adddepositsAmount", deposit.getQTYDeposited().toString());
    	
    	List<SVFFIN_Payments> list = PaymentDataSearchClass.getSVFFINPaymentMethods(deposit);
    	List<FIN_Payment> listPayment =  new ArrayList<FIN_Payment>();
    	for (SVFFIN_Payments svffin_Payment : list) {
    		FIN_Payment payment = OBDal.getInstance().get(FIN_Payment.class, svffin_Payment.getFINPayment().getId());
    		listPayment.add(payment);
		}
    	
    	xmlDocument.setParameter("adddocTotalAmount", order.getGrandTotalAmount().toString());
	 	    
		PDFieldProvider[] paymentMethodsArr = PaymentDataSearchClass.getPaymentMethodsPDFields(client);
	    xmlDocument.setParameter("paramAmountFrom", "0");
	    String selected = new String();
		if(paymentMethodsArr.length > 0){
			selected = paymentMethodsArr[0].getField("id");
			xmlDocument.setParameter("addPaymentMethodId", selected);
		}
		xmlDocument.setData("reportPayMethodId", "liststructure", paymentMethodsArr);
		
		PDFieldProvider[] financialAcountArr = PaymentDataSearchClass.getFinancialAccountPDFields(this,orgId);
	    String selectedFinAccount = new String();
		if(financialAcountArr.length > 0){
			selectedFinAccount = financialAcountArr[0].getField("id");
			xmlDocument.setParameter("addFinancialAccountId", selectedFinAccount);
		}
		xmlDocument.setData("reportFinancialAccount", "liststructure", financialAcountArr);
		
		String table = generateTable(listPayment);		
		xmlDocument.setParameter("desc", table);
		
	    response.setContentType("text/html; charset=UTF-8");
	    final PrintWriter out = response.getWriter();

	    out.println(xmlDocument.print());
	    out.close();
	  }
  
 
  private String generateTable(List<FIN_Payment> list){
	   
	  String generalRows = new String();
	  if(list.size() == 0){
		  generalRows = " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_0\">                              "+
				  "     <td class=\"DataGrid_Body_Cell\">There is not related data.</td>                                     "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                                             "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                                             "+
				  " 	<td class=\"DataGrid_Body_Cell\"></td>                                                             "+
				  " </tr>                                                                                               ";
	  }
	  else{
		  int pos = 0;
		  for (FIN_Payment transData : list) {
			  
			  SVFFIN_Payments paymentdetail = PaymentDataSearchClass.getDepositPayment(transData);
			  String entrada = "payment In";
			  if(!paymentdetail.isPaymentIn())
				  entrada = "payment Out";

 		    generalRows +=  " <tr id=\"funcEvenOddRow\" class=\"DataGrid_Body_Row DataGrid_Body_Row_"+ String.valueOf(pos) +"\">                              "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getDescription() + "</td>                                     "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getAccount().getName() + "</td>                                                             "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getPaymentMethod().getName() + "</td>                                                             "+
					  " 	<td class=\"DataGrid_Body_Cell\">" + transData.getAmount().toString() + "</td>                                                             "+
					  " </tr>                                                                                               ";
			  pos++;
		}
	  }
		  
	  String generalTable = " <table class=\"DataGrid_Header_Table DataGrid_Body_Table\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">                                             "+
			  "     <tbody>                                                                                                                                        "+
			  "         <tr class=\"Popup_Client_Selector_DataGrid_HeaderRow\">                                                                                      "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"130\">Descripción</th>                                                                             "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"100\" style=\"align:right\">Account</th>                                                                                "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"100\" style=\"align:right\">Payment method</th>                                                                                "+
			  "             <th class=\"DataGrid_Header_Cell\" width=\"70\" style=\"align:right\">Amount</th>                                                      "+
			  "         </tr>                                                                                                                                      "+
			  generalRows + 
			  "     </tbody>                                                                                                                                       "+
			  " </table>                                                                                                                                           ";
	  return generalTable;
	
  }
  
 
  
 
  public String getServletInfo() {
    return "GenerateInvoicesmanual Servlet. This Servlet was made by Pablo Sarobe";
  } // end of getServletInfo() method
}
