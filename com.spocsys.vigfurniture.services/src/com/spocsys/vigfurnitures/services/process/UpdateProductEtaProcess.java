package com.spocsys.vigfurnitures.services.process;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.plm.Product;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;
import org.quartz.JobExecutionException;

import com.spocsys.vigfurniture.incomingproduct.svfpincProductIncomingV;

public class UpdateProductEtaProcess  extends DalBaseProcess{

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {
		try{
			ConnectionProvider connectionProvider =bundle.getConnection();
			Connection conn = connectionProvider.getConnection();
			Client client = OBContext.getOBContext().getCurrentClient();
			ProductData.updateETA(conn, connectionProvider, client.getId());

			List<String> checkedProductIds= new ArrayList<String>();
			OBCriteria<svfpincProductIncomingV> criteria= OBDal.getInstance().createCriteria(svfpincProductIncomingV.class);
			//criteria.add(Restrictions.isNull(svfpincProductIncomingV.PROPERTY_ORDERSO));
			criteria.add(Restrictions.gt(svfpincProductIncomingV.PROPERTY_ETA, new Date()));
			
			criteria.addOrder(org.hibernate.criterion.Order.asc(svfpincProductIncomingV.PROPERTY_ETA));

			List<svfpincProductIncomingV> incomingVs = criteria.list();

			for (svfpincProductIncomingV svfpincProductIncomingV2 : incomingVs) {	
				if(!checkedProductIds.contains(svfpincProductIncomingV2.getProduct().getId())){
					checkedProductIds.add(svfpincProductIncomingV2.getProduct().getId());

					Product product = svfpincProductIncomingV2.getProduct();
					product= OBDal.getInstance().get(Product.class, product.getId());

					///SEt ETA VALUE To Product from svfpincProductIncomingV

					Date ETa = svfpincProductIncomingV2.getETA();
					if(ETa!=null){
						//SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd");

//						Calendar currentCalendar = Calendar.getInstance();
//						currentCalendar.setTime(new Date());
//						int currentYear=	currentCalendar.get(Calendar.YEAR);
//
//						
//						Calendar etaCalendar = Calendar.getInstance();
//						etaCalendar.setTime(ETa);
//						etaCalendar.set(Calendar.YEAR, currentYear);

						product.setSvfsvEta(ETa);

					}}


			}






		}catch (Exception e) {
			// catch any possible exception and throw it as a Quartz
			// JobExecutionException
			throw new JobExecutionException(e.getMessage(), e);
		}

	}

}
