/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2012-2013 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurnitures.services.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;






import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import com.spocsys.vigfurnitures.services.utils.LineData;
import com.spocsys.vigfurnitures.services.utils.PaymentDataSearchClass;
import com.spocsys.vigfurnitures.services.utils.PaymentPlanDataSearchClass;


public class UpdateStatusProcess extends DalBaseProcess  {

  
 

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {
	  // TODO Auto-generated method stub
	  
	  	OBError myMessage = null;
		myMessage = new OBError();
		myMessage.setTitle("");
		    
	    /*if(boxQty <= 0){
	    	
	    	OBError msg = new OBError();
	        msg.setType("Error");
	        msg.setMessage("The selected order has cero box quantity set");
	        bundle.setResult(msg);
	        OBDal.getInstance().rollbackAndClose();
	        return;
	    }   */ 	    
	     
	    try{
	    	
	    	String result = "Proceso terminado exitosamente";
	    	
	    	// Create Sales Order
		    String cInvoiceId = (String) bundle.getParams().get("C_Invoice_ID");
		    Invoice invoice = OBDal.getInstance().get(Invoice.class, cInvoiceId);
		    Boolean isPaid = false;
		    if(PaymentPlanDataSearchClass.getToPaidAmount(invoice).compareTo(new BigDecimal(0)) == 0)
				  isPaid = true;
			PaymentDataSearchClass.updateInvoice(invoice.getId(), PaymentPlanDataSearchClass.getPaidAmount(invoice),PaymentPlanDataSearchClass.getToPaidAmount(invoice),isPaid);
		    
	    	OBError msg = new OBError();
	        msg.setType("info");;	        		     
	        msg.setMessage(result);
	        bundle.setResult(msg);
	    }
	    catch(Exception ex){
	    	OBError msg = new OBError();
	        msg.setType("Error");
	        msg.setMessage("Se ha detectado el error:"+ex.getMessage());
	        bundle.setResult(msg);
	        OBDal.getInstance().rollbackAndClose();
	        return;
		  }
  	 }
  
}
