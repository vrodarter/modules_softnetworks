package com.spocsys.vigfurnitures.services.utils;

import java.math.BigDecimal;

public class AccountDataClass {
	
	private String AccountId;
	private BigDecimal amount;
	public String getAccountId() {
		return AccountId;
	}
	public void setAccountId(String accountId) {
		AccountId = accountId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public AccountDataClass() {
		super();
		// TODO Auto-generated constructor stub
		amount = new BigDecimal(0);
	}
	
	

}
