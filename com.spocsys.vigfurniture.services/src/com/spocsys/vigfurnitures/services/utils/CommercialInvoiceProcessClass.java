package com.spocsys.vigfurnitures.services.utils;

import static com.spocsys.vigfurnitures.services.utils.QueryParameter.with;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.database.SessionInfo;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.erpCommon.businessUtility.PriceAdjustment;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.financial.FinancialUtils;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.service.db.QueryTimeOutUtil;

import com.spocsys.vigfurnitures.services.SVFSVCiProformaLine;

public class CommercialInvoiceProcessClass {

	public CommercialInvoiceProcessClass() {
		// TODO Auto-generated constructor stub
	}

	private static Sequence updateSequence(Sequence seq) throws Exception
	{
		OBContext.setAdminMode();

		try{

			OBDal.getInstance().save(seq);
			OBDal.getInstance().flush();
			OBDal.getInstance().refresh(seq);		   
		}
		catch(Exception ex){
			ex.printStackTrace();
			seq = null;
		}
		finally{
			OBContext.restorePreviousMode();
		}

		return seq;
	}

	private static Order saveInvoice(Order order) throws Exception
	{
		OBContext.setAdminMode();

		try{
			OBDal.getInstance().save(order);
			OBDal.getInstance().flush();
			OBDal.getInstance().refresh(order);		
			OBDal.getInstance().commitAndClose();
		}
		catch(Exception ex){
			ex.printStackTrace();
			order = null;
		}
		finally{
			OBContext.restorePreviousMode();
		}

		return order;
	}


	public static DocumentType getDocType(Client client)    throws ServletException {

		OBCriteria<DocumentType> criteriaE = OBDal.getInstance().createCriteria(DocumentType.class);

		criteriaE.add(Restrictions.eq("name", "AP Invoice"));
		criteriaE.add(Restrictions.eq("active", true));

		//criteriaE.add(Restrictions.eq("client", client));

		List<DocumentType> listE = criteriaE.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}

	public static PaymentTerm getPaymentTerm(Client client)    throws ServletException {

		OBCriteria<PaymentTerm> criteriaE = OBDal.getInstance().createCriteria(PaymentTerm.class);

		criteriaE.add(Restrictions.eq("client", client));
		criteriaE.add(Restrictions.eq("active", true));

		List<PaymentTerm> listE = criteriaE.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}

	public static FIN_PaymentMethod getPaymentMethod(Client client)    throws ServletException {

		OBCriteria<FIN_PaymentMethod> criteriaE = OBDal.getInstance().createCriteria(FIN_PaymentMethod.class);

		criteriaE.add(Restrictions.eq("client", client));
		criteriaE.add(Restrictions.eq("active", true));

		List<FIN_PaymentMethod> listE = criteriaE.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}



	public static String saveLine(org.openbravo.database.ConnectionProvider connectionProvider,String lineId,String invoiceId,String lineNo) throws Exception
	{
		String newId = SequenceIdData.getUUID();
		String strSql = " INSERT INTO c_invoiceline(                                                                         "+         
				"             c_invoiceline_id, ad_client_id, ad_org_id, isactive, created,                          "+
				"             createdby, updated, updatedby, c_invoice_id, c_orderline_id,                           "+
				"             m_inoutline_id, line, description, financial_invoice_line, account_id,                 "+
				"             m_product_id, qtyinvoiced, pricelist, priceactual, pricelimit,                         "+
				"             linenetamt, c_charge_id, chargeamt, c_uom_id, c_tax_id, s_resourceassignment_id,       "+
				"             taxamt, m_attributesetinstance_id, isdescription, quantityorder,                       "+
				"             m_product_uom_id, c_invoice_discount_id, c_projectline_id, m_offer_id,                 "+
				"             pricestd, excludeforwithholding, iseditlinenetamt, taxbaseamt,                         "+
				"             line_gross_amount, gross_unit_price, c_bpartner_id, periodnumber,                      "+
				"             grosspricestd, a_asset_id, defplantype, grosspricelist, c_project_id,                  "+
				"             isdeferred, c_period_id, c_costcenter_id, user1_id, user2_id,                          "+
				"             explode, bom_parent_id, match_lccosts, em_svfsv_poreference)                           "+
				"		Select  '" + newId + "', ad_client_id, ad_org_id, isactive, TO_DATE(NOW()),                        "+
				"             createdby, TO_DATE(NOW()), updatedby, '" + invoiceId +"', c_orderline_id,                           "+
				"             m_inoutline_id, line, description, financial_invoice_line, account_id,                 "+
				"             m_product_id, qtyinvoiced, pricelist, priceactual, pricelimit,                         "+
				"             linenetamt, c_charge_id, chargeamt, c_uom_id, c_tax_id, s_resourceassignment_id,       "+
				"             taxbaseamt, m_attributesetinstance_id, isdescription, quantityorder,                       "+
				"             m_product_uom_id, c_invoice_discount_id, c_projectline_id, m_offer_id,                 "+
				"             pricestd, excludeforwithholding, iseditlinenetamt, taxbaseamt,                         "+
				"             line_gross_amount, gross_unit_price, c_bpartner_id, periodnumber,                      "+
				"             grosspricestd, a_asset_id, defplantype, grosspricelist, c_project_id,                  "+
				"             isdeferred, c_period_id, c_costcenter_id, user1_id, user2_id,                          "+
				"             explode, bom_parent_id, match_lccosts, em_svfsv_poreference                            "+
				"	     FROM c_invoiceline                                                                          "+
				"		 WHERE c_invoiceline_id = '" + lineId + "'                                                                 ";                                                 
		PreparedStatement st = null;
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			st.execute();
		} catch(SQLException e){
			throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
			throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
			try {
				connectionProvider.releasePreparedStatement(st);
			} catch(Exception ignore){
				ignore.printStackTrace();
			}
		}
		return newId;
	}

	public static String saveLineFromOrderLine(org.openbravo.database.ConnectionProvider connectionProvider,String lineId,String invoiceId,String lineNo, BigDecimal decimal ) throws Exception
	{
		String newId = SequenceIdData.getUUID();
		String strSql = " INSERT INTO c_invoiceline(                                                                         "+         
				"             c_invoiceline_id, ad_client_id, ad_org_id, isactive, created,                          "+
				"             createdby, updated, updatedby, c_invoice_id, c_orderline_id,                           "+
				"             m_inoutline_id, line, description, financial_invoice_line, account_id,                 "+
				"             m_product_id, qtyinvoiced, pricelist, priceactual, pricelimit,                         "+
				"              c_charge_id, chargeamt, c_uom_id, c_tax_id, s_resourceassignment_id,       "+
				"             taxamt, m_attributesetinstance_id, isdescription, quantityorder,                       "+
				"             m_product_uom_id, c_invoice_discount_id, c_projectline_id, m_offer_id,                 "+
				"             pricestd, excludeforwithholding, iseditlinenetamt, taxbaseamt,                         "+
				"              gross_unit_price, c_bpartner_id, periodnumber,                      "+
				"             grosspricestd, a_asset_id, defplantype, grosspricelist, c_project_id,                  "+
				"             isdeferred, c_period_id, c_costcenter_id, user1_id, user2_id,                          "+
				"             explode, bom_parent_id, match_lccosts, em_svfsv_poreference,                            "+
				"             em_svfsv_containernum,em_svfsv_ordline_id,em_svfsv_saleorder_id, "+
				"             em_svfsv_ordernum,em_svfsv_prof_inv_id,em_svfsv_prof_ordline_id,em_svfsv_eta )        "+
				"    Select  '" + newId + "', ad_client_id, ad_org_id, isactive, TO_DATE(NOW()),                    "+   
				"            createdby, TO_DATE(NOW()), updatedby, '" + invoiceId +"', c_orderline_id,             "+
				"            m_inoutline_id, "+lineNo+", description, 'N', null,            "+
				"            m_product_id, "+decimal+", pricelist, priceactual, pricelimit,                        "+
				"             c_charge_id, chargeamt, c_uom_id, c_tax_id, s_resourceassignment_id,      "+
				"            0, m_attributesetinstance_id, isdescription, quantityorder,                    "+
				"            m_product_uom_id, null, null, m_offer_id,            "+
				"            pricestd, null, iseditlinenetamt, taxbaseamt,                      "+
				"            gross_unit_price, c_bpartner_id, null,                   "+
				"            grosspricestd, a_asset_id, null, 0, c_project_id,               "+
				"            'N', null, c_costcenter_id, user1_id, user2_id,                     "+
				"            explode, bom_parent_id, null, em_svfsv_poreference,                         "+
				"            null,em_svfsv_orig_c_orderline_id,em_svfsv_orig_c_order_id,"+
				"            (select documentno from c_order where c_order_id=em_svfsv_orig_c_order_id),(select c_order_id from c_orderline where c_orderline_id='" + lineId + "'),'" + lineId + "',em_svfsv_eta "+
				"     FROM c_orderline                                                                             "+
				"		 WHERE c_orderline_id = '" + lineId + "'                                                                 ";                                                 
		PreparedStatement st = null;
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			st.execute();
		} catch(SQLException e){
			throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
			throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
			try {
				connectionProvider.releasePreparedStatement(st);
			} catch(Exception ignore){
				ignore.printStackTrace();
			}
		}
		return newId;
	}
	public static String saveLineFromOrderLine2(String lineId,String orderId,String lineNo, BigDecimal decimal ) throws Exception
	{
		Order order = OBDal.getInstance().get(Order.class, orderId);
		OrderLine current  = OBDal.getInstance().get(OrderLine.class, lineId);
		OrderLine orderLine = OBProvider.getInstance().get(OrderLine.class);
		String strPrecision = "0";
		int StdPrecision = Integer.valueOf(strPrecision).intValue();
		
		BigDecimal  lineNetAmt;
		orderLine.setOrganization(order.getOrganization());
		orderLine.setProduct(current.getProduct());
		orderLine.setWarehouse(current.getWarehouse());
		orderLine.setSvfsvPoreference(current.getSvfsvPoreference());
		//orderLine.setSvfsvOrdernum(current.getSvfsvOrigCOrder().getDocumentNo());
		orderLine.setSvfsvReferencePfi(current.getSalesOrder().getSvfsvReferencePfi());
		orderLine.setSvfpaSpecialOrder(current.isSvfpaSpecialOrder());
		orderLine.setSvfsvOrigCOrderline(current.getSvfsvOrigCOrderline());
		orderLine.setSvfsvProfOrdline(current);
		orderLine.setSvfsvProfOrd(current.getSalesOrder());
		orderLine.setSvfsvOrigCOrder(current.getSvfsvOrigCOrder());
		orderLine.setSalesOrder(order);
		orderLine.setSvfsvEta(current.getSvfsvEta());
		orderLine.setListPrice(current.getListPrice());
		orderLine.setUnitPrice(current.getUnitPrice());
		orderLine.setPriceLimit(current.getPriceLimit());
		orderLine.setUOM(current.getUOM());
		orderLine.setTax(current.getTax());
		orderLine.setSvfsvProfInvoiceRef(current.getSalesOrder().getSvfsvProfInvoiceRef());
		orderLine.setOrderUOM(current.getOrderUOM());
		orderLine.setLineNo(Long.valueOf(lineNo));
		orderLine.setProject(current.getProject());
		orderLine.setAsset(current.getAsset());
		orderLine.setStandardPrice(current.getStandardPrice());
		orderLine.setTaxableAmount(current.getTaxableAmount());
		orderLine.setAttributeSetValue(current.getProduct().getAttributeSetValue());
		orderLine.setOrderDate(new Date());
		orderLine.setOrderedQuantity(decimal);
		orderLine.setCurrency(current.getCurrency());
		orderLine.setUOM(current.getUOM());
		orderLine.setScheduledDeliveryDate(new Date());
		OBDal.getInstance().save(orderLine);
		OBDal.getInstance().flush();
		OBDal.getInstance().commitAndClose();

		return orderLine.getId();


	}





	public static Order createComercialInvoice(String orgId,String adClientId,String bPartnerId) throws Exception{

		Organization organization = OBDal.getInstance().get(Organization.class, orgId);
		adClientId = adClientId.replace("'", "");
		Client client = OBDal.getInstance().get(Client.class, adClientId);
		BusinessPartner bp = OBDal.getInstance().get(BusinessPartner.class, bPartnerId);


		List<DocumentType> documentTypes = FinderGeneric.find(DocumentType.class, with(DocumentType.PROPERTY_NAME, "Comercial Invoice").parameters(), true, false);
		if(documentTypes == null ||documentTypes.isEmpty())
			throw new OBException("There is not default Document Type for Document Category : Comercial Invoice");

		DocumentType docType = documentTypes.get(0);

		Order order = new Order();
		order.setScheduledDeliveryDate(new Date());
		order.setOrganization(organization);
		order.setClient(client);
		order.setSvfsvIsprocess(false);
		if(docType == null )
			throw new OBException("There is not default Document Type for Document Category : Comercial Invoice");

		order.setDocumentType(docType);
		order.setTransactionDocument(docType);

		order.setBusinessPartner(bp);
		order.setPartnerAddress(bp.getBusinessPartnerLocationList().get(0));
		order.setDocumentStatus("DR");
		order.setActive(true);
		order.setSalesTransaction(false);
		//order.setSvfsvIscommercial(true);
		User user = OBDal.getInstance().get(User.class, OBContext.getOBContext().getUser().getId());
		Warehouse warehouse =user.getDefaultWarehouse();
		order.setWarehouse(warehouse);
		if(bp.getPOPaymentTerms()!=null )
			order.setPaymentTerms(bp.getPOPaymentTerms());
		else
			if(bp.getPaymentTerms()!=null )
				order.setPaymentTerms(bp.getPaymentTerms());

		if(order.getPaymentTerms()==null)
			throw new OBException("Business Partner :" + bp.getName()+ " don't have Payment Terms");


		if(bp.getPOPaymentMethod()!=null )
			order.setPaymentMethod(bp.getPOPaymentMethod());
		else
			if(bp.getPaymentMethod()!=null )
				order.setPaymentMethod(bp.getPaymentMethod());

		if(order.getPaymentMethod()==null)
			throw new OBException("Business Partner :" + bp.getName()+ " don't have Payment Method");

		order.setOrderDate(new Date());
		order.setAccountingDate(new Date());

		if(bp.getCurrency()!=null )
			order.setCurrency(bp.getCurrency());
		else
			throw new OBException("Business Partner :" + bp.getName()+ " don't have Currency");

		if(bp.getPurchasePricelist()!=null )
			order.setPriceList(bp.getPurchasePricelist());
		else
			if(bp.getPriceList()!=null )
				order.setPriceList(bp.getPriceList());
		if(order.getPriceList()==null)
			throw new OBException("Business Partner :" + bp.getName()+ " don't have Price List");

		order = saveInvoice(order);
		//String accountId = bp.getAccount().getId();


		return order;
	}

	public static void  createCommercialInvoiceLines(org.openbravo.database.ConnectionProvider connectionProvider,String invoiceId, List<String> listLines,VariablesSecureApp vars) throws Exception{
		int lineNo = 10;
		for (String string : listLines) {
			BigDecimal decimal = getAmount(vars, string);
			saveLineFromOrderLine2( string, invoiceId,String.valueOf(lineNo),decimal);
			updateALLQty(connectionProvider, decimal, string);
			lineNo += 10;
		}
	}

	private static BigDecimal getAmount(VariablesSecureApp vars,String id) throws ServletException {

		return getSelectedBaseOBObjectAmount(vars, id, "inpPaymentAmount");



	}

	public  static BigDecimal  getSelectedBaseOBObjectAmount(
			VariablesSecureApp vars, String o, String htmlElementId)
					throws ServletException {


		BigDecimal decimal = new  BigDecimal(vars.getRequiredNumericParameter(htmlElementId + (String) o, ""));


		return decimal;

	}


	public static int updateALLQty(ConnectionProvider connectionProvider, BigDecimal decimal, String c_orderline_id)    throws ServletException {
		String strSql = "";
		strSql = strSql + 
				"         UPDATE c_orderline SET em_svfsv_qtyallocated_gen  = em_svfsv_qtyallocated_gen+?" +
				"      WHERE c_orderline_id =? ";

		int updateCount = 0;
		PreparedStatement st = null;

		int iParameter = 0;
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
			iParameter++;
			UtilSql.setValue(st, iParameter, 0, null, decimal.toString());
			iParameter++;
			UtilSql.setValue(st, iParameter, 12, null, c_orderline_id);

			updateCount = st.executeUpdate();
		} catch(SQLException e){

			throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
		} catch(Exception ex){

			throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
			try {
				connectionProvider.releasePreparedStatement(st);
			} catch(Exception ignore){
				ignore.printStackTrace();
			}
		}
		return(updateCount);
	}

	public static void updateSvfsvCiProformaLine(Order invoice, ArrayList<String> transactionsIds, VariablesSecureApp vars) throws ServletException {
		for (String id : transactionsIds) {
			OrderLine orderLine = OBDal.getInstance().get(OrderLine.class, id);
			BigDecimal quantity= getAmount(vars, id);
			SVFSVCiProformaLine svfsvCiProformaLine = OBProvider.getInstance().get(SVFSVCiProformaLine.class);
			svfsvCiProformaLine.setOrderline(orderLine);
			svfsvCiProformaLine.setOrder(invoice);
			svfsvCiProformaLine.setQuantity(quantity.longValue());
			OBDal.getInstance().save(svfsvCiProformaLine);
			OBDal.getInstance().flush();
		}
		OBDal.getInstance().commitAndClose();
	}

}
