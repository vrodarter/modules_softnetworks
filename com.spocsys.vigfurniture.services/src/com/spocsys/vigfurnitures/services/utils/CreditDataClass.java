package com.spocsys.vigfurnitures.services.utils;

import java.math.BigDecimal;

public class CreditDataClass {
	
	String creditId;
	BigDecimal amount;
	public String getCreditId() {
		return creditId;
	}
	public void setCreditId(String creditId) {
		this.creditId = creditId;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public CreditDataClass() {
		super();
		
		amount = new BigDecimal(0);
		// TODO Auto-generated constructor stub
	}
	
}
