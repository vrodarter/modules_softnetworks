package com.spocsys.vigfurnitures.services.utils;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.UtilSql;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.ad.module.ADOrgModule;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;


public class DataSearchClass {

	public static String getPinstanceData(org.openbravo.database.ConnectionProvider connectionProvider,String id)    throws ServletException {

		String strSql =  "SELECT  result,				"+
						" errormsg "+
						" FROM 								"+
						" public.ad_pinstance		"+
						" where ad_pinstance_id='" + id + "'"; 
	    
	    ResultSet result;
	    PreparedStatement st = null;

	    String value = "";
	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	result = st.executeQuery();
	    	int pos = 0;
	    	
	    	while (result.next()) {
	    		value = UtilSql.getValue(result, "result")+"|"+UtilSql.getValue(result, "errormsg");
	    	}
	    	result.close();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return value;
	  }
	
	public static String getfield(org.openbravo.database.ConnectionProvider connectionProvider,String id,String field)    throws ServletException {

		String strSql =  "SELECT  " + field + "				"+
						" FROM 								"+
						" public.c_order		"+
						" where c_order_id='" + id + "'"; 
	    
	    ResultSet result;
	    PreparedStatement st = null;

	    String value = "";
	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	result = st.executeQuery();
	    	int pos = 0;
	    	
	    	while (result.next()) {
	    		value = UtilSql.getValue(result, field);
	    	}
	    	result.close();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return value;
	  }

	

	public static String getMessage(org.openbravo.database.ConnectionProvider connectionProvider,String message)    throws ServletException {

		String strSql =  "SELECT  msgtext				"+
						" FROM 								"+
						" public.ad_message		"+
						" where value like '%" + message + "%' limit 1"; 
	    
	    ResultSet result;
	    PreparedStatement st = null;

	    String value = "";
	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	result = st.executeQuery();
	    	int pos = 0;
	    	
	    	while (result.next()) {
	    		value = UtilSql.getValue(result, "msgtext");
	    	}
	    	result.close();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return value;
	  }
	
	public static String saveProcessInstance(org.openbravo.database.ConnectionProvider connectionProvider,String orgId,String adClientId,String processId,String OrderId,String user) throws Exception
	  {
		String newProcessInstanceId = SequenceIdData.getUUID();
		String strSql = "insert into AD_PInstance (AD_Process_ID, IsProcessing, Created, Updated, AD_Client_ID, AD_Org_ID,record_id, CreatedBy, UpdatedBy,ad_user_id, IsActive, AD_PInstance_ID)  "+ 
						"                  values ('" + processId + "', 'N', TO_DATE(NOW()), TO_DATE(NOW()), '" + adClientId + "', '" + orgId + "', '" + OrderId + "', '" + user +"', '" + user +"', '" + user +"', 'Y', '" + newProcessInstanceId + "')";
		ResultSet result;
	    PreparedStatement st = null;

	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return newProcessInstanceId;
	  }
	
	    
	    public static void updateOrderAction(org.openbravo.database.ConnectionProvider connectionProvider,String orderId,String action) throws Exception
		  {
			String strSql = "update c_order set docaction = '" + action + "' where c_order_id='" + orderId + "'";
			ResultSet result;
		    PreparedStatement st = null;

		    int iParameter = 0;
		    try {
		    	st = connectionProvider.getPreparedStatement(strSql);
		    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
		    	st.execute();
		    } catch(SQLException e){
		      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		    } catch(Exception ex){
		      throw new ServletException("@CODE=@" + ex.getMessage());
		    } finally {
		      try {
		        connectionProvider.releasePreparedStatement(st);
		      } catch(Exception ignore){
		        ignore.printStackTrace();
		      }
		    }
   
	  }
	
	public static boolean bookProcess(org.openbravo.database.ConnectionProvider connectionProvider, String adPinstanceId)    throws ServletException {
		boolean result = true;
	    String strSql = "";
	    strSql = strSql + 
	      "        Select  svfsv_c_order_post(?)";

	    PreparedStatement st = null;
	    int iParameter = 0;
	    try {
	      st = connectionProvider.getPreparedStatement(strSql);
	      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adPinstanceId);

	      st.execute();
	    } catch(SQLException e){
	    	  result = false;
		      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		    } catch(Exception ex){
		      throw new ServletException("@CODE=@" + ex.getMessage());
		    } finally {
		      try {
		        connectionProvider.releasePreparedStatement(st);
		      } catch(Exception ignore){
		        ignore.printStackTrace();
		      }
		    }
	    return result;
	  }
	
	public static Long getNextLine(org.openbravo.database.ConnectionProvider connectionProvider,String finFinancialAccountId)    throws ServletException {

		String strSql =  "SELECT 				"+
						" max(fin_finacc_transaction.line) as line "+
						" FROM 								"+
						" public.fin_finacc_transaction		"+
						" where fin_financial_account_id='" + finFinancialAccountId + "'"; 
	    
	    ResultSet result;
	    PreparedStatement st = null;

	    Long line = new Long(0);
	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	result = st.executeQuery();
	    	int pos = 0;
	    	
	    	while (result.next()) {
	    		String value = UtilSql.getValue(result, "line");
	    		if(!value.trim().isEmpty())
	    			line = Long.parseLong(value);
	    	}
	    	line = line + 10;
	    	result.close();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return line;
	  }


	public static List<FIN_FinancialAccount> loadDestinyAccounts(String finFinancialAccountId, String context)    throws ServletException {
		
		//String[] orgIds = splitString(context);
		  
		 OBCriteria<FIN_FinancialAccount> criteriaE = OBDal.getInstance().createCriteria(FIN_FinancialAccount.class);

			criteriaE.add(Restrictions.not(Restrictions.eq("id", finFinancialAccountId)));
			//criteriaE.add(Restrictions.in("organization", objOrg));
			List<FIN_FinancialAccount> listE = criteriaE.list();
			return listE;
	  }
	
public static List<FIN_FinaccTransaction> loadDepositTransaction(String finFinancialAccountId, Collection<String> ids, boolean command)    throws ServletException {
		
	FIN_FinancialAccount account = OBDal.getInstance().get(FIN_FinancialAccount.class, finFinancialAccountId);
		 OBCriteria<FIN_FinaccTransaction> criteriaE = OBDal.getInstance().createCriteria(FIN_FinaccTransaction.class);
           if(command ) 
        		   if(ids!=null&&!ids.isEmpty())
        	   criteriaE.add(Restrictions.in(FIN_FinaccTransaction.PROPERTY_ID, ids));
        		   else{
        			   ids.add("");
        			   criteriaE.add(Restrictions.in(FIN_FinaccTransaction.PROPERTY_ID,ids));
        		   }
           criteriaE.add(Restrictions.eq("account", account));
			
			
			criteriaE.add(Restrictions.eq("active", true));
			criteriaE.add(Restrictions.or(Restrictions.eq("status", "RPPC"), Restrictions.eq("status", "RDNC")) );
			criteriaE.add(Restrictions.eq("svfsvActive", true));
			criteriaE.add(Restrictions.eq("paymentAmount", new BigDecimal(0)));
			List<FIN_FinaccTransaction> listE = criteriaE.list();
			return listE;
	  }

		


	
	public static List<GLItem> loadGLItem()    throws ServletException {
		
		 OBCriteria<GLItem> criteriaE = OBDal.getInstance().createCriteria(GLItem.class);
			List<GLItem> listE = criteriaE.list();
			return listE;
	  }
	
	public static List<GLItem> loadGLItemByDefaultStatus(String status)    throws ServletException {
		
		 OBCriteria<GLItem> criteriaE = OBDal.getInstance().createCriteria(GLItem.class);
		 criteriaE.add(Restrictions.eq("svfsvDefault", status));
			List<GLItem> listE = criteriaE.list();
			return listE;
	  }
	
	public static PDFieldProvider[] getGLItems()
	{
		try {
			List<GLItem> list = loadGLItem();
			PDFieldProvider[] pdValues = new PDFieldProvider[list.size()];
			for(int i=0;i<list.size();i++){
				GLItem data = list.get(i);
				pdValues[i] = new PDFieldProvider("NULL", data.getName(), data.getId());
			}
			
			return pdValues;
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static String getGLItemsDefaultIn()
	{
		String result = "";
		try {
			List<GLItem> list = loadGLItemByDefaultStatus("1");
			if(list.size() > 0){
				GLItem item = list.get(0); 
				result = item.getId();
			}
			return result;
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public static String getGLItemsDefaultOut()
	{
		String result = "";
		try {
			List<GLItem> list = loadGLItemByDefaultStatus("2");
			if(list.size() > 0){
				GLItem item = list.get(0); 
				result = item.getId();
			}
			return result;
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static PDFieldProvider[] getDestinyAccounts(String finFinancialAccountId,String context)
	{
		
	
		try {
			List<FIN_FinancialAccount> list = loadDestinyAccounts(finFinancialAccountId,context);
			PDFieldProvider[] pdValues = new PDFieldProvider[list.size()];
			for(int i=0;i<list.size();i++){
				FIN_FinancialAccount data = list.get(i);
				pdValues[i] = new PDFieldProvider("NULL", data.getName(), data.getId());
			}
			
			return pdValues;
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}


	
}
