package com.spocsys.vigfurnitures.services.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;

import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.database.SessionInfo;
import org.openbravo.service.db.QueryTimeOutUtil;

public class DateUtils {

	
	  public static String nDaysAfter(ConnectionProvider connectionProvider, VariablesSecureApp vars,String fecha, String dias)    throws ServletException {
		    String strSql = "";
		    strSql = strSql + 
		      "        SELECT TO_DATE(?) + to_number(?) as fecha FROM DUAL";

		    ResultSet result;
		    String dateReturn = null;
		    PreparedStatement st = null;

		    int iParameter = 0;
		    try {
		    st = connectionProvider.getPreparedStatement(strSql);
		      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
		      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fecha);
		      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dias);

		      result = st.executeQuery();
		      if(result.next()) {
		        dateReturn = UtilSql.getDateValue(result, "fecha", vars.getJavaDateFormat());
		      }
		      result.close();
		    } catch(SQLException e){
		     
		      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
		    } catch(Exception ex){
		      
		      throw new ServletException("@CODE=@" + ex.getMessage());
		    } finally {
		      try {
		        connectionProvider.releasePreparedStatement(st);
		      } catch(Exception ignore){
		        ignore.printStackTrace();
		      }
		    }
		    return(dateReturn);
		  }

}
