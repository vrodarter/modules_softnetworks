package com.spocsys.vigfurnitures.services.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;

public class FinderGeneric {
	public static final String EMPTY = "";
	public static final String SPACE = " ";
	public static final DateFormat GF_DATE_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");

	public static final DateFormat DEBUG_DATE_FORMAT = new SimpleDateFormat(
			"yyyy/MM/dd HH:mm:ss");
	public static final String DEBUG_SEPARTOR = " : * ";
	public static final String DEBUG_ENDLINE = " \n";
	public static final String ENTRY_ID = "id";
	
	
	public static <T extends BaseOBObject> List<T> find(Class<T> clazz, HashMap<String, Object> properties, boolean clientFilter, boolean organizationFilter, Order...orders ) {
	return find(clazz, properties, clientFilter, organizationFilter, 0,orders);
	}
	

	public static <T extends BaseOBObject> List<T> find(Class<T> clazz, HashMap<String, Object> properties, boolean clientFilter, boolean organizationFilter, int resultLimit,Order...orders ) {

		OBCriteria<T> criteria = OBDal.getInstance().createCriteria(clazz);

		if(resultLimit!=0)
		criteria.setMaxResults(resultLimit);
		
		if (clientFilter)

			criteria.add(Restrictions.eq("client", OBContext.getOBContext().getCurrentClient()));

		if (organizationFilter)

			criteria.add(Restrictions.eq("organization", OBContext.getOBContext().getCurrentOrganization()));

		Iterator<Entry<String, Object>> iterator = properties.entrySet().iterator();
		String where = "";
		while (iterator.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator .next();

			criteria.add(Restrictions.eq(entry.getKey(), entry.getValue()));

			where += entry.getKey() + " = '" + entry.getValue()+ "'";
		}
		
		if(orders!=null )
			for (int i = 0; i < orders.length; i++) {
				criteria.addOrder(orders[i]);
			}

		debug("Looking for " + clazz.getSimpleName() + " where " +where+  ((clientFilter) ? " in client " + OBContext.getOBContext().getCurrentClient().getName() : EMPTY)

				+ ((organizationFilter) ? " in org "

                               + OBContext.getOBContext().getCurrentOrganization().getName() : EMPTY));

		try {

			List<T> obj = criteria.list();
			if (!obj.isEmpty()) {
				return obj;
			}


		} catch (Exception e) {

			debug("Error:" + e.getMessage());

		} finally {

		}
		debug("No " + clazz.getSimpleName() + " found");

		return null;
	}
	public static <T extends BaseOBObject> List<T> findlike(Class<T> clazz, HashMap<String, Object> properties, boolean clientFilter, boolean organizationFilter) {
		return findlike(clazz, properties, clientFilter, organizationFilter, 0);
	}
	
	
	public static <T extends BaseOBObject> List<T> findlike(Class<T> clazz, HashMap<String, Object> properties, boolean clientFilter, boolean organizationFilter, int resultLimit) {

		OBCriteria<T> criteria = OBDal.getInstance().createCriteria(clazz);

		criteria.setMaxResults(resultLimit);
		
		if (clientFilter)

			criteria.add(Restrictions.eq("client", OBContext.getOBContext().getCurrentClient()));

		if (organizationFilter)

			criteria.add(Restrictions.eq("organization", OBContext.getOBContext().getCurrentOrganization()));

		Iterator<Entry<String, Object>> iterator = properties.entrySet().iterator();
		String where = "";
		while (iterator.hasNext()) {
			Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator .next();

			criteria.add(Restrictions.ilike(entry.getKey(), entry.getValue()));

			where += entry.getKey() + " = '" + entry.getValue()+ "'";
		}

		debug("Looking for " + clazz.getSimpleName() + " where " +where+  ((clientFilter) ? " in client " + OBContext.getOBContext().getCurrentClient().getName() : EMPTY)

				+ ((organizationFilter) ? " in org "

                               + OBContext.getOBContext().getCurrentOrganization().getName() : EMPTY));

		try {

			List<T> obj = criteria.list();
			if (!obj.isEmpty()) {
				return obj;
			}


		} catch (Exception e) {

			debug("Error:" + e.getMessage());

		} finally {

		}
		debug("No " + clazz.getSimpleName() + " found");

		return null;
	}



	public static void  debug(Object o) {
		System.out.print(SPACE+DEBUG_DATE_FORMAT.format(new Date()) + DEBUG_SEPARTOR
				+ o + DEBUG_ENDLINE);
	}
}
