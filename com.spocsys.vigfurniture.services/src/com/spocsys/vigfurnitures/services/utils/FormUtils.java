package com.spocsys.vigfurnitures.services.utils;

public class FormUtils {

	
	

	public  static String  replaceChecked(String print,String SalesOrder) {
		String[] salesId = splitString(SalesOrder);

		for (String id : salesId) {
			id=id.trim();
			print =print.replaceFirst(id+"\"", id+"\"  checked="+"\"checked\""); 
			print=print.replaceFirst("inpPaymentAmount"+id+"\" class="+"\"dojoValidateValid TextBox_btn_OneCell_width number TextBox required readonly", "inpPaymentAmount"+id+"\" class="+"\"dojoValidateValid TextBox_btn_OneCell_width number TextBox required" );
		}


		return print;


	}
	public  static String  replaceCheckedAll(String print) {

			print =print.replaceFirst("name=\"inpTodos\"", "name=\"inpTodos\"  checked=\"checked\""); 


		return print;


	}
	public static String[] splitString(String value) {

		value=value.replaceAll("\\(", "");
		value=value.replaceAll("\\)", "");

		value=value.replaceAll("'", "");
		value=value.trim();
		String[] valueArray = value.split(",");

		return valueArray;
	}
	
	
	
}
