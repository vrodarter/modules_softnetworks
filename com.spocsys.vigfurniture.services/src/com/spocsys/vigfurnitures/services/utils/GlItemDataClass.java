package com.spocsys.vigfurnitures.services.utils;

import java.math.BigDecimal;

import org.openbravo.model.financialmgmt.gl.GLItem;

public class GlItemDataClass {
	
	private GLItem glItem;
	private BigDecimal amount;

	public GlItemDataClass() {
		// TODO Auto-generated constructor stub
	}

	public GLItem getGlItem() {
		return glItem;
	}

	public void setGlItem(GLItem glItem) {
		this.glItem = glItem;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	

}
