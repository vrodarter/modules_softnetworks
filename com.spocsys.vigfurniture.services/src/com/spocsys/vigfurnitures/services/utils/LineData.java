package com.spocsys.vigfurnitures.services.utils;

import java.math.BigDecimal;

import org.openbravo.model.common.plm.Product;

public class LineData {
	private Product product;
	private BigDecimal box;
	private String mAttibuteSetInstanceID;
	
	
	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public BigDecimal getBox() {
		return box;
	}


	public void setBox(BigDecimal box) {
		this.box = box;
	}
	
	


	public String getmAttibuteSetInstanceID() {
		return mAttibuteSetInstanceID;
	}


	public void setmAttibuteSetInstanceID(String mAttibuteSetInstanceID) {
		this.mAttibuteSetInstanceID = mAttibuteSetInstanceID;
	}


	public LineData() {
		// TODO Auto-generated constructor stub
	}

}
