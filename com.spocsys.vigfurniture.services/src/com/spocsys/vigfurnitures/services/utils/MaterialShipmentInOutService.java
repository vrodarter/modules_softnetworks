package com.spocsys.vigfurnitures.services.utils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.database.SessionInfo;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.model.procurement.POInvoiceMatch;
import org.openbravo.service.db.QueryTimeOutUtil;

import com.spocsys.vigfurnitures.services.SVFSVInoutCiLine;

public class MaterialShipmentInOutService {

	public MaterialShipmentInOutService() {
		// TODO Auto-generated constructor stub
	}

	private static Sequence updateSequence(Sequence seq) throws Exception {
		OBContext.setAdminMode();

		try{
			OBDal.getInstance().save(seq);
			OBDal.getInstance().flush();
			OBDal.getInstance().refresh(seq);		   
		}catch(Exception ex){
			ex.printStackTrace();
			seq = null;
		}
		finally{
			OBContext.restorePreviousMode();
		}

		return seq;
	}

	public static ShipmentInOut saveShipment(ShipmentInOut shipment) throws Exception{
		OBContext.setAdminMode();

	
			OBDal.getInstance().save(shipment);
			OBDal.getInstance().flush();
			OBDal.getInstance().refresh(shipment);		
			OBDal.getInstance().commitAndClose();
		
			
		
			OBContext.restorePreviousMode();
		

		return shipment;
	}

	public static DocumentType getDocType() throws ServletException {

		OBCriteria<DocumentType> criteriaE = OBDal.getInstance().createCriteria(DocumentType.class);

		criteriaE.add(Restrictions.eq("name", "MM Receipt"));
		criteriaE.add(Restrictions.eq("active", true));
		criteriaE.add(Restrictions.eq("default", true));

		List<DocumentType> listE = criteriaE.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}
	
	public static DocumentType getDocType(Organization organization) throws ServletException {

		OBCriteria<DocumentType> criteria = OBDal.getInstance().createCriteria(DocumentType.class);
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_ORGANIZATION, organization));
//		criteria.add(Restrictions.eq(DocumentType.PROPERTY_DOCUMENTCATEGORY, "ARI"));
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_SALESTRANSACTION, false));
		criteria.add(Restrictions.eq(DocumentType.PROPERTY_TABLE, getinvoiceTable()));

		List<DocumentType> listE = criteria.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}
	
	private static Table getinvoiceTable() {
		Table table = null;
		OBCriteria<Table> criteria = OBDal.getInstance().createCriteria(Table.class);
		criteria.add(Restrictions.eq(Table.PROPERTY_DBTABLENAME, "M_InOut"));
		List<Table> list= criteria.list();
	    if(list!=null && !list.isEmpty()){
	    	table=list.get(0);
	    }
	return table;
	}

	public static PaymentTerm getPaymentTerm(Client client) throws ServletException {

		OBCriteria<PaymentTerm> criteriaE = OBDal.getInstance().createCriteria(PaymentTerm.class);

		criteriaE.add(Restrictions.eq("client", client));
		criteriaE.add(Restrictions.eq("active", true));

		List<PaymentTerm> listE = criteriaE.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}

	public static FIN_PaymentMethod getPaymentMethod(Client client) throws ServletException {

		OBCriteria<FIN_PaymentMethod> criteriaE = OBDal.getInstance().createCriteria(FIN_PaymentMethod.class);

		criteriaE.add(Restrictions.eq("client", client));
		criteriaE.add(Restrictions.eq("active", true));

		List<FIN_PaymentMethod> listE = criteriaE.list();
		if(listE.size() > 0)
			return listE.get(0);
		else
			return null;

	}


	public static String saveLineFromOrderLine(org.openbravo.database.ConnectionProvider connectionProvider,String lineId,String minoutId,String lineNo, BigDecimal decimal) throws Exception
	{
		String newId = SequenceIdData.getUUID();


		String sql = "INSERT INTO m_inoutline( " +
				"m_inoutline_id, ad_client_id , ad_org_id, isactive, created, createdby, " +
				"updated, updatedby, line, description, m_inout_id, c_orderline_id," + 
				"m_locator_id, m_product_id, c_uom_id, movementqty, isinvoiced," + 
				"m_attributesetinstance_id, isdescription, quantityorder, m_product_uom_id," + 
				"m_condition_goods_id, canceled_inoutline_id, a_asset_id, manage_prereservation," + 
				"user1_id, user2_id, c_project_id, c_costcenter_id, c_bpartner_id," + 
				"explode, bom_parent_id, em_svfsv_poreference, em_obwpl_pickinglist_id," + 
				"em_obwpl_editlines_pe, em_obwpl_removeline,em_svfsv_orig_c_invoiceline_id,em_svfsv_orig_c_invoice_id, em_svfsv_reference_pfi)" +
				"SELECT  '" + newId + "', ad_client_id , ad_org_id , isactive, created, createdby," + 
				"updated, updatedby, " + lineNo + ", description, '" + minoutId + "', c_orderline_id," + 
				"(SELECT m_locator.m_locator_id FROM m_locator INNER JOIN m_warehouse ON m_locator.m_warehouse_id = m_warehouse.m_warehouse_id INNER JOIN m_inout ON m_warehouse.m_warehouse_id=m_inout.m_warehouse_id WHERE  m_inout.m_inout_id ='"+minoutId+"' LIMIT 1)"+
				", m_product_id, c_uom_id, "+ decimal.toString()+", 'N'," + 
				"m_attributesetinstance_id, 'N', quantityorder, m_product_uom_id," + 
				"null, null, a_asset_id, null," + 
				" user1_id, user2_id, c_project_id, c_costcenter_id, c_bpartner_id," + 
				" explode, bom_parent_id, em_svfsv_poreference, null," + 
				" null, null,c_orderline_id,c_order_id, em_svfsv_reference_pfi" +
				" FROM    c_orderline  " +
				" WHERE   c_orderline_id = '" + lineId + "'";



		PreparedStatement st = null;
		try {
			st = connectionProvider.getPreparedStatement(sql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			st.execute();
		} catch(SQLException e){
			throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
			throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
			try {
				connectionProvider.releasePreparedStatement(st);
			} catch(Exception ignore){
				ignore.printStackTrace();
			}
		}
		return newId;
	}

	@SuppressWarnings("deprecation")
	public static ShipmentInOut createShipment(String orgId,String adClientId,String bPartnerId,String orderId) throws Exception{

		Organization organization = OBDal.getInstance().get(Organization.class, orgId);
		adClientId = adClientId.replace("'", "");
		Client client = OBDal.getInstance().get(Client.class, adClientId);
		BusinessPartner bp = OBDal.getInstance().get(BusinessPartner.class, bPartnerId);
		Date date = new Date();
		DocumentType docType = getDocType(organization)==null ?getDocType():getDocType(organization);
	    ArrayList<String> order = Utility.stringToArrayList(orderId.replaceAll("\\(|\\)|'", ""));

		ShipmentInOut inOut = new ShipmentInOut();

		inOut.setOrganization(organization);
		inOut.setClient(client);
		inOut.setDocumentType(docType);
		inOut.setMovementDate(date);
		//inOut.setSvfsvOrderid(orderId);
		
		User user = OBDal.getInstance().get(User.class, OBContext.getOBContext().getUser().getId());
		Warehouse warehouse =user.getDefaultWarehouse();
		//warehouse=OBDal.getInstance().get(Warehouse.class, "5848641D712545C7AE0FE9634A163648");

		if(warehouse!=null)	
			inOut.setWarehouse(warehouse);	
		else 
			throw new OBException("There is not Warehouse");
		
		//inOut.setTransactionDocument(docType);
		
		inOut.setBusinessPartner(bp);
		inOut.setPartnerAddress(bp.getBusinessPartnerLocationList().get(0));
		//inOut.setDocumentStatus("TEMP");
		inOut.setActive(true);
		inOut.setSalesTransaction(false);
		//inOut.setPaymentTerms(getPaymentTerm(client));
		//inOut.setPaymentMethod(getPaymentMethod(client));
		//inOut.setinOutDate(new java.sql.Date(date.getTime()));
		inOut.setAccountingDate(new java.sql.Date(date.getTime()));
		//inOut.setCurrency(bp.getCurrency());
		//inOut.setPriceList(bp.getPriceList());
		
		//String accountId = bp.getAccount().getId();


		return inOut;
	}

	@SuppressWarnings("deprecation")
	public static ShipmentInOut createShipment(ShipmentInOut inOut) throws Exception{
		saveShipment(inOut);
		
		return inOut;
	}
	public static void  createShipmentLines(org.openbravo.database.ConnectionProvider connectionProvider,String invoiceId, List<String> listLines, VariablesSecureApp vars) throws Exception{
		int lineNo = 10;
		
		for (String string : listLines) {
			BigDecimal decimal = getAmount(vars, string);
			String outline=saveLineFromOrderLine(connectionProvider, string, invoiceId,String.valueOf(lineNo),decimal);
			updateALLQty(connectionProvider, decimal, string);
			OrderLine orderLine = OBDal.getInstance().get(OrderLine.class,string);
			createPOInvoiceMatch(orderLine,outline);

			ShipmentInOutLine currentLine=OBDal.getInstance().get(ShipmentInOutLine.class, outline);
			insertAcctDimension(connectionProvider.getConnection(), connectionProvider, outline, currentLine.getClient().getId(), currentLine.getOrganization().getId(), string, string);
			lineNo += 10;
		}
	}
	
	public static void  createShipmentLines(org.openbravo.database.ConnectionProvider connectionProvider,String invoiceId, List<OrderLine> listLines) throws Exception{
		int lineNo = 10;
		
		for (OrderLine string : listLines) {
			string =OBDal.getInstance().get(OrderLine.class, string.getId());
			BigDecimal decimal = string.getOrderedQuantity();
			String outline =saveLineFromOrderLine(connectionProvider, string.getId(), invoiceId,String.valueOf(lineNo),decimal);
			updateALLQty(connectionProvider, decimal, string.getId());	
			
			ShipmentInOutLine currentLine=OBDal.getInstance().get(ShipmentInOutLine.class, outline);
			insertAcctDimension(connectionProvider.getConnection(), connectionProvider, outline, currentLine.getClient().getId(), currentLine.getOrganization().getId(), string.getId(), string.getId());
			createPOInvoiceMatch(string,outline);
			lineNo += 10;
		}
	}

	public static void updateSvfsvInoutCiLine(ShipmentInOut shipmentInOut, ArrayList<String> transactionsIds, VariablesSecureApp vars) throws ServletException {
		for (String id : transactionsIds) {
			OrderLine invoiceLine = OBDal.getInstance().get(OrderLine.class, id);
			BigDecimal quantity = getAmount(vars, id);
			SVFSVInoutCiLine svfsvInoutCiLine = OBProvider.getInstance().get(SVFSVInoutCiLine.class);
			svfsvInoutCiLine.setOrderline(invoiceLine);
			svfsvInoutCiLine.setInout(shipmentInOut);
			svfsvInoutCiLine.setQuantity(quantity.longValue());
			OBDal.getInstance().save(svfsvInoutCiLine);
			OBDal.getInstance().flush();
		}
		OBDal.getInstance().commitAndClose();
	}
	
	public static void updateSvfsvInoutCiLine(ShipmentInOut shipmentInOut, List<OrderLine> transactionsIds) throws ServletException {
		for (OrderLine id : transactionsIds) {
			id =OBDal.getInstance().get(OrderLine.class, id.getId());
			BigDecimal quantity = id.getInvoicedQuantity();
			SVFSVInoutCiLine svfsvInoutCiLine = OBProvider.getInstance().get(SVFSVInoutCiLine.class);
			svfsvInoutCiLine.setOrderline(id);
			svfsvInoutCiLine.setInout(shipmentInOut);
			svfsvInoutCiLine.setQuantity(quantity.longValue());
			OBDal.getInstance().save(svfsvInoutCiLine);
			OBDal.getInstance().flush();
		}
		OBDal.getInstance().commitAndClose();
	}


	public static BigDecimal getAmount(VariablesSecureApp vars,String id) throws ServletException {

		return getSelectedBaseOBObjectAmount(vars, id, "inpPaymentAmount");



	}

	public  static BigDecimal  getSelectedBaseOBObjectAmount(
			VariablesSecureApp vars, String o, String htmlElementId)
					throws ServletException {


		BigDecimal decimal = new  BigDecimal(vars.getRequiredNumericParameter(htmlElementId + (String) o, ""));


		return decimal;

	}
	
	public static int updateALLQty(ConnectionProvider connectionProvider, BigDecimal decimal, String c_orderline_id)    throws ServletException {
		String strSql = "";
		strSql = strSql + 
				"         UPDATE c_orderline SET em_svfsv_qtyallocated_gen  = em_svfsv_qtyallocated_gen+?" +
				"      WHERE c_orderline_id =?";

		int updateCount = 0;
		PreparedStatement st = null;

		int iParameter = 0;
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
			iParameter++;
			UtilSql.setValue(st, iParameter, 0, null, decimal.toString());
			iParameter++;
			UtilSql.setValue(st, iParameter, 12, null, c_orderline_id);

			updateCount = st.executeUpdate();
		} catch(SQLException e){

			throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
		} catch(Exception ex){

			throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
			try {
				connectionProvider.releasePreparedStatement(st);
			} catch(Exception ignore){
				ignore.printStackTrace();
			}
		}
		return(updateCount);
	}
	
	
public static  void createPOInvoiceMatch(OrderLine order, String inOutLineid ) {
    ShipmentInOutLine outLine = OBDal.getInstance().get(ShipmentInOutLine.class,inOutLineid);

	POInvoiceMatch match = OBProvider.getInstance().get(POInvoiceMatch.class);
	match.setOrganization(outLine.getOrganization());
	match.setTransactionDate(new Date());
	match.setProduct(outLine.getProduct());
	match.setGoodsShipmentLine(outLine);
	match.setQuantity(outLine.getMovementQuantity());
	match.setSalesOrderLine(order);
	
	OBDal.getInstance().save(match);
	OBDal.getInstance().flush();
	OrderLine linepfi = order.getSvfsvProfOrdline();
	if(linepfi!=null&& 	linepfi.getOrderedQuantity()!=null &&linepfi.getSvfsvQtyallocatedGen()!=null )
	if(linepfi.getOrderedQuantity().compareTo(new BigDecimal(linepfi.getSvfsvQtyallocatedGen()))==0){
	POInvoiceMatch matchPFI = OBProvider.getInstance().get(POInvoiceMatch.class);
	
	matchPFI.setOrganization(outLine.getOrganization());
	matchPFI.setTransactionDate(new Date());
	matchPFI.setProduct(outLine.getProduct());
	matchPFI.setGoodsShipmentLine(outLine);
	matchPFI.setQuantity(outLine.getMovementQuantity());
	matchPFI.setSalesOrderLine(linepfi);
	
	OBDal.getInstance().save(matchPFI);
	OBDal.getInstance().flush();
	}
	OrderLine linepo = order.getSvfsvOrigCOrderline();
	if(linepo!=null&& 	linepo.getOrderedQuantity()!=null &&linepo.getSvfsvQtyallocatedGen()!=null )
if(	linepo.getOrderedQuantity().compareTo(new BigDecimal(linepo.getSvfsvQtyallocatedGen()))==0){
	POInvoiceMatch matchPO = OBProvider.getInstance().get(POInvoiceMatch.class);
	
	matchPO.setOrganization(outLine.getOrganization());
	matchPO.setTransactionDate(new Date());
	matchPO.setProduct(outLine.getProduct());
	matchPO.setGoodsShipmentLine(outLine);
	matchPO.setQuantity(outLine.getMovementQuantity());
	matchPO.setSalesOrderLine(linepo);
	
	OBDal.getInstance().save(matchPO);
	OBDal.getInstance().flush();
	}
	OBDal.getInstance().commitAndClose();
	
	
	

}




public static int insertAcctDimension(Connection conn, ConnectionProvider connectionProvider, String mInoutlineId, String adClientId, String adOrgId, String adUserId, String cOrderlineId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      INSERT INTO M_INOUTLINE_ACCTDIMENSION" +
      "        (M_InOutLine_AcctDimension_ID,M_InOutLine_ID, AD_Client_ID,AD_Org_ID,IsActive," +
      "        Created,CreatedBy,Updated,UpdatedBy," +
      "        C_Project_ID, C_Campaign_ID, User1_ID," +
      "        User2_ID, C_Activity_ID, C_Costcenter_ID," +
      "        C_BPartner_ID, M_Product_ID, A_Asset_ID," +
      "        Quantity" +
      "        )" +
      "        (SELECT GET_UUID(), ?, ?, ?, 'Y'," +
      "         now(), ?, now(), ?, C_Project_ID, C_Campaign_ID," +
      "         User1_ID, User2_ID, C_Activity_ID, C_Costcenter_Id," +
      "         C_BPartner_ID, M_Product_ID, A_Asset_ID," +
      "         (Amt/(SELECT PriceActual FROM C_OrderLine WHERE C_OrderLine_ID = ?))" +
      "         FROM C_OrderLine_AcctDimension" +
      "         WHERE C_OrderLine_ID = ?" +
      "         )";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
	
}
