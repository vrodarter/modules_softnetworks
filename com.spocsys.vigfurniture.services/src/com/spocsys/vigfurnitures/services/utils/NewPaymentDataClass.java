package com.spocsys.vigfurnitures.services.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;

public class NewPaymentDataClass {
	
	private String accountId;
	private String PaymentMethodId;
	private boolean completeAccount;
	private FIN_PaymentSchedule payments;
	private BigDecimal amountAccount;
	private GLItem glItem;
	private BigDecimal glItemAmount;
	private FIN_Payment credit;
	private BigDecimal creditAmount = new BigDecimal(0);
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getPaymentMethodId() {
		return PaymentMethodId;
	}
	public void setPaymentMethodId(String paymentMethodId) {
		PaymentMethodId = paymentMethodId;
	}
	public boolean isCompleteAccount() {
		return completeAccount;
	}
	public void setCompleteAccount(boolean completeAccount) {
		this.completeAccount = completeAccount;
	}
	public FIN_PaymentSchedule getPayments() {
		return payments;
	}
	public void setPayments(FIN_PaymentSchedule payments) {
		this.payments = payments;
	}
	public BigDecimal getAmountAccount() {
		return amountAccount;
	}
	public void setAmountAccount(BigDecimal amountAccount) {
		this.amountAccount = amountAccount;
	}
	public GLItem getGlItem() {
		return glItem;
	}
	public void setGlItem(GLItem glItem) {
		this.glItem = glItem;
	}
	public BigDecimal getGlItemAmount() {
		return glItemAmount;
	}
	public void setGlItemAmount(BigDecimal glItemAmount) {
		this.glItemAmount = glItemAmount;
	}
	public FIN_Payment getCredit() {
		return credit;
	}
	public void setCredit(FIN_Payment credit) {
		this.credit = credit;
	}
	public BigDecimal getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}
	
	
	

	}
