package com.spocsys.vigfurnitures.services.utils;
import org.openbravo.data.FieldProvider;

public class PDFieldProvider implements FieldProvider {
	private String value = "";
	private String name = "";
	private String id = "";

	public PDFieldProvider(String value, String name, String id) {
		super();
		this.value = value;
		this.name = name;
		this.id = id;
	}

	@Override
	public String getField(String fieldName) {
		String retStr = null;

		if (fieldName.equals("value"))
			retStr = value;
		else if (fieldName.equals("name"))
			retStr = name;
		else if (fieldName.equals("id"))
			retStr = id;

		return retStr;
	}

}
