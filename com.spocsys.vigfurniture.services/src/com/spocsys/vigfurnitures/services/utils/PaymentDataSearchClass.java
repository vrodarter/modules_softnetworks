package com.spocsys.vigfurnitures.services.utils;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Restrictions;
import org.jdom.DocType;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.UtilSql;
import org.openbravo.erpCommon.info.Account;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.gl.GLCategory;
import org.openbravo.model.financialmgmt.gl.GLItem;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetailV;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.FinAccPaymentMethod;

import com.spocsys.vigfurniture.financial.SVFFIN_Deposit;
import com.spocsys.vigfurniture.financial.SVFFIN_Deposit_Mngt;
import com.spocsys.vigfurniture.financial.SVFFIN_Payments;

public class PaymentDataSearchClass {

	public PaymentDataSearchClass() {
		// TODO Auto-generated constructor stub
	}
	
	public static Order getOrder(String orderId){
		Order order = OBDal.getInstance().get(Order.class, orderId);
		return order;
	}
	
	public static Invoice getInvoice(String invoiceId){
		Invoice invoice = OBDal.getInstance().get(Invoice.class, invoiceId);
		return invoice;
	}
	
	public static Client getClient(String clientId){
		Client client = OBDal.getInstance().get(Client.class, clientId);
		return client;
	}
	
	public static Organization getOrganization(String organizationId){
		Organization organization = OBDal.getInstance().get(Organization.class, organizationId);
		return organization;
	}
	
	public static BigDecimal getDepositBalance(String glItemId){
		GLItem glItem = OBDal.getInstance().get(GLItem.class, glItemId);
		List<SVFFIN_Deposit> listE = new ArrayList<SVFFIN_Deposit>();
		if(glItem != null){
			OBCriteria<SVFFIN_Deposit> criteriaE = OBDal.getInstance().createCriteria(SVFFIN_Deposit.class);
	
			 criteriaE.add(Restrictions.eq("glitem", glItem));
	
			 listE = criteriaE.list();
		}
		 if(listE.size() > 0){
			SVFFIN_Deposit deposit = listE.get(0);
			return deposit.getBalance();
		 }else
			 return new BigDecimal(0);
	}
	
	public static List<FIN_FinancialAccount> getFinancialAccount(Organization org){
		
		 OBCriteria<FIN_FinancialAccount> criteriaE = OBDal.getInstance().createCriteria(FIN_FinancialAccount.class);

		 criteriaE.add(Restrictions.eq("organization", org));
		 criteriaE.addOrderBy("name", true);

		 List<FIN_FinancialAccount> listE = criteriaE.list();
		 
		 return listE;
	}
	
	public static Table getTable(){
		
		 OBCriteria<Table> criteriaE = OBDal.getInstance().createCriteria(Table.class);

		 criteriaE.add(Restrictions.eq("name", "FIN_Payment"));
		 criteriaE.addOrderBy("name", true);

		 List<Table> listE = criteriaE.list();
		 
		 if(listE.size() > 0)
			 return listE.get(0);
		 else
			 return null;
	}
	
	public static GLCategory getGLCategory(Organization org,boolean payment){
		OBCriteria<GLCategory> criteriaE = OBDal.getInstance().createCriteria(GLCategory.class);
		if(!payment)
			 criteriaE.add(Restrictions.eq("name", "AP Payment"));
		else
			criteriaE.add(Restrictions.eq("name", "AR Receipt"));
		 criteriaE.add(Restrictions.eq("organization", org));
		 criteriaE.addOrderBy("default", true);

		 List<GLCategory> listE = criteriaE.list();
		 
		 if(listE.size() > 0)
			 return listE.get(0);
		 else{
			 GLCategory doc = null;
			 if(!org.getId().equalsIgnoreCase("0"))
				 doc = getGLCategory(getOrganization("0"),payment);
			 return doc;
		 }
		 
	}
	
public static GLCategory getGLCategorySql(org.openbravo.database.ConnectionProvider connectionProvider,String orgId,boolean payment) throws ServletException{
		
		String glName = "AR Receipt";
		if(!payment)
			glName = "AP Payment";
		String strSql =  "SELECT gl_category_id FROM gl_category where ad_org_id = '" + orgId + "' and name like '%" + glName + "%' "; 

		ResultSet result;
		PreparedStatement st = null;
		
		String value = "";
		int iParameter = 0;
		List<GLCategory> GLCategoryList = new ArrayList<GLCategory>();
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			result = st.executeQuery();
			int pos = 0;
			
			while (result.next()) {
				GLCategory glCategory = OBDal.getInstance().get(GLCategory.class, UtilSql.getValue(result, "gl_category_id"));
				if(glCategory != null)
					GLCategoryList.add(glCategory);
			}
			result.close();
		} catch(SQLException e){
		  throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
		  throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
		  try {
		    connectionProvider.releasePreparedStatement(st);
		  } catch(Exception ignore){
		    ignore.printStackTrace();
		  }
		}
		GLCategory doc = null;
		if(GLCategoryList.size() > 0)
			doc = GLCategoryList.get(0);
		else{
		    if(!orgId.equalsIgnoreCase("0"))
			 doc = getGLCategorySql(connectionProvider, getOrganization("0").getId(), payment);
		}
		 return doc;
	}
	
	public static DocumentType getDocType1(org.openbravo.database.ConnectionProvider connectionProvider,Organization org, boolean payment) throws ServletException{
		OBCriteria<DocumentType> criteriaE = OBDal.getInstance().createCriteria(DocumentType.class);
		
		criteriaE.add(Restrictions.eq("gLCategory", getGLCategorySql(connectionProvider,org.getId(),payment)));
		 criteriaE.add(Restrictions.eq("organization", org));
		 criteriaE.addOrderBy("default", true);

		 List<DocumentType> listE = criteriaE.list();
		 
		 if(listE.size() > 0)
			 return listE.get(0);
		 else{
			 DocumentType doc = getDocTypeSql(connectionProvider,getOrganization("0"),payment);
			 if(doc != null)
				 return doc;
			 else
				 return null;
		 }
		 
	}
	
public static DocumentType getDocTypeSql(org.openbravo.database.ConnectionProvider connectionProvider,Organization org,String docTypeName) throws ServletException{
		
		
		String strSql =  "SELECT c_doctype_id FROM c_doctype where ad_org_id = '" + org.getId() + "' and name = '" + docTypeName + "' "; 

		ResultSet result;
		PreparedStatement st = null;
		
		String value = "";
		int iParameter = 0;
		List<DocumentType> GLCategoryList = new ArrayList<DocumentType>();
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			result = st.executeQuery();
			int pos = 0;
			
			while (result.next()) {
				DocumentType glCategory = OBDal.getInstance().get(DocumentType.class, UtilSql.getValue(result, "c_doctype_id"));
				if(glCategory != null)
					GLCategoryList.add(glCategory);
			}
			result.close();
		} catch(SQLException e){
		  throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
		  throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
		  try {
		    connectionProvider.releasePreparedStatement(st);
		  } catch(Exception ignore){
		    ignore.printStackTrace();
		  }
		}
		DocumentType doc = null;
		if(GLCategoryList.size() > 0)
			doc = GLCategoryList.get(0);
		 return doc;
	}

	public static List<FIN_Payment> getCreditsSql(org.openbravo.database.ConnectionProvider connectionProvider,String bParnerId) throws ServletException{
		
		
		String strSql =  "SELECT fin_payment_id FROM fin_payment where c_bpartner_id = '" + bParnerId + "' and generated_credit > 0 "; 
	
		ResultSet result;
		PreparedStatement st = null;
		
		String value = "";
		int iParameter = 0;
		List<FIN_Payment> creditList = new ArrayList<FIN_Payment>();
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			result = st.executeQuery();
			int pos = 0;
			
			while (result.next()) {
				FIN_Payment credit = OBDal.getInstance().get(FIN_Payment.class, UtilSql.getValue(result, "fin_payment_id"));
				if(credit != null)
					creditList.add(credit);
			}
			result.close();
		} catch(SQLException e){
		  throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
		  throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
		  try {
		    connectionProvider.releasePreparedStatement(st);
		  } catch(Exception ignore){
		    ignore.printStackTrace();
		  }
		}
		
		 return creditList;
	}

	public static DocumentType getDocTypeSql(org.openbravo.database.ConnectionProvider connectionProvider,Organization org,boolean payment) throws ServletException{
		
		GLCategory gl = getGLCategorySql(connectionProvider, org.getId(), payment);
		
		String glName = "AR Receipt";
		if(!payment)
			glName = "AP Payment";
		
		String strSql =  "SELECT c_doctype_id FROM c_doctype where ad_org_id = '" + org.getId() + "' and gl_category_id = '" + gl.getId() + "' "; 
	
		ResultSet result;
		PreparedStatement st = null;
		
		String value = "";
		int iParameter = 0;
		List<DocumentType> GLCategoryList = new ArrayList<DocumentType>();
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			result = st.executeQuery();
			int pos = 0;
			
			while (result.next()) {
				DocumentType glCategory = OBDal.getInstance().get(DocumentType.class, UtilSql.getValue(result, "c_doctype_id"));
				if(glCategory != null)
					GLCategoryList.add(glCategory);
			}
			result.close();
		} catch(SQLException e){
		  throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
		  throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
		  try {
		    connectionProvider.releasePreparedStatement(st);
		  } catch(Exception ignore){
		    ignore.printStackTrace();
		  }
		}
		DocumentType doc = null;
		if(GLCategoryList.size() > 0)
			doc = GLCategoryList.get(0);
		else{
		    if(!org.getId().equalsIgnoreCase("0"))
			 doc = getDocTypeSql(connectionProvider, getOrganization("0"), payment);
		}
		 return doc;
	}
	
	public static List<Order> getInvoiceRelatedProforma(org.openbravo.database.ConnectionProvider connectionProvider,String invoiceId) throws ServletException{
		//
		String strSql =  "Select Distinct em_svfsv_prof_inv_id from c_invoiceline where c_invoice_id = '" + invoiceId + "'"; 

		ResultSet result;
		PreparedStatement st = null;
		
		String value = "";
		int iParameter = 0;
		List<Order> orderList = new ArrayList<Order>();
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			result = st.executeQuery();
			int pos = 0;
			
			while (result.next()) {
				Order order = OBDal.getInstance().get(Order.class, UtilSql.getValue(result, "em_svfsv_prof_inv_id"));
				if(order != null)
				orderList.add(order);
			}
			result.close();
		} catch(SQLException e){
		  throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
		  throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
		  try {
		    connectionProvider.releasePreparedStatement(st);
		  } catch(Exception ignore){
		    ignore.printStackTrace();
		  }
		}
		
		return orderList;
	}
	
	public static List<Order> getOrderRelatedProforma(org.openbravo.database.ConnectionProvider connectionProvider,String orderId) throws ServletException{
		//
		String strSql =  "Select Distinct em_svfsv_prof_ord_id from c_orderline where c_order_id = '" + orderId + "'"; 

		ResultSet result;
		PreparedStatement st = null;
		
		String value = "";
		int iParameter = 0;
		List<Order> orderList = new ArrayList<Order>();
		try {
			st = connectionProvider.getPreparedStatement(strSql);
			//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
			result = st.executeQuery();
			int pos = 0;
			
			while (result.next()) {
				String orderIdStr = UtilSql.getValue(result, "em_svfsv_prof_ord_id");
				if((orderIdStr != null)&&(!orderIdStr.equalsIgnoreCase(""))){
					Order order = OBDal.getInstance().get(Order.class, orderIdStr);
					orderList.add(order);
				}
			}
			result.close();
		} catch(SQLException e){
		  throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		} catch(Exception ex){
		  throw new ServletException("@CODE=@" + ex.getMessage());
		} finally {
		  try {
		    connectionProvider.releasePreparedStatement(st);
		  } catch(Exception ignore){
		    ignore.printStackTrace();
		  }
		}
		
		return orderList;
	}
	
	public static GLItem loadGLItem(String documentId)    throws ServletException {
		
		 OBCriteria<GLItem> criteriaE = OBDal.getInstance().createCriteria(GLItem.class);
		 
		 criteriaE.add(Restrictions.eq("svfsvDepositdocid", documentId));
		 
		 List<GLItem> listE = criteriaE.list();
		 
		 if(listE.size() > 0)
			 return listE.get(0);
		 else
			 return null;
	  }
	
	public static List<GLItem> loadGLItemByOrder(List<Order> orderList)    throws ServletException {
		List<GLItem> allGlItems = new ArrayList<GLItem>();
		for (Order order : orderList) {
			OBCriteria<GLItem> criteriaE = OBDal.getInstance().createCriteria(GLItem.class);
	 		 
			 criteriaE.add(Restrictions.eq("svfsvDepositdocid", order.getId()));
			 
			 List<GLItem> listE = criteriaE.list();
			 
			 allGlItems.addAll(listE);
		}
		 
		 
		 return allGlItems;
	  }
	
	
	public static SVFFIN_Deposit geDeposits(GLItem glItem){
		
		 OBCriteria<SVFFIN_Deposit> criteriaE = OBDal.getInstance().createCriteria(SVFFIN_Deposit.class);

			criteriaE.add(Restrictions.eq("glitem", glItem));
			criteriaE.addOrderBy("creationDate", true);

			List<SVFFIN_Deposit> listE = criteriaE.list();
			
			if(listE.size() > 0)
				return listE.get(0);
			else
				return null;
	}
	
	public static String getDepositMngId(org.openbravo.database.ConnectionProvider connectionProvider,String id)    throws ServletException {

		String strSql =  "SELECT  svffin_deposit_mngt_id				"+
						" FROM 								"+
						" public.svffin_deposit		"+
						" where svffin_deposit_id='" + id + "' limit 1"; 
	    
	    ResultSet result;
	    PreparedStatement st = null;

	    String value = "";
	    int iParameter = 0;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	result = st.executeQuery();
	    	int pos = 0;
	    	
	    	while (result.next()) {
	    		value = UtilSql.getValue(result, "svffin_deposit_mngt_id");
	    	}
	    	result.close();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return value;
	  }
	
	public static SVFFIN_Deposit_Mngt getDepositMng(String depositMngId){
		SVFFIN_Deposit_Mngt depositMngt = OBDal.getInstance().get(SVFFIN_Deposit_Mngt.class, depositMngId);
		return depositMngt;
	}
	
	public static List<FIN_PaymentMethod> getPaymentMethods(Client client){
		
		 OBCriteria<FIN_PaymentMethod> criteriaE = OBDal.getInstance().createCriteria(FIN_PaymentMethod.class);

			criteriaE.add(Restrictions.eq("client", client));
			criteriaE.add(Restrictions.eq("payoutAllow", true));
			criteriaE.addOrderBy("name", true);

			List<FIN_PaymentMethod> listE = criteriaE.list();
			return listE;
	}
	
	public static List<FIN_PaymentMethod> getPaymentMethodsByAccount(String financialAccount){
		
		 OBCriteria<FinAccPaymentMethod> criteriaE = OBDal.getInstance().createCriteria(FinAccPaymentMethod.class);
		 FIN_FinancialAccount account = OBDal.getInstance().get(FIN_FinancialAccount.class, financialAccount);

			criteriaE.add(Restrictions.eq("account", account));
			criteriaE.addOrderBy("paymentMethod", true);

			List<FinAccPaymentMethod> listE = criteriaE.list();
			List<FIN_PaymentMethod> listPayMethod = new ArrayList<FIN_PaymentMethod>();
			for (FinAccPaymentMethod finAccPaymentMethod : listE) {
				listPayMethod.add(finAccPaymentMethod.getPaymentMethod());				
			}
			return listPayMethod;
	}
	
	public static List<SVFFIN_Payments> getSVFFINPaymentMethods(SVFFIN_Deposit deposit){
		
		 OBCriteria<SVFFIN_Payments> criteriaE = OBDal.getInstance().createCriteria(SVFFIN_Payments.class);

			criteriaE.add(Restrictions.eq("svffinDeposit", deposit));
			criteriaE.add(Restrictions.eq("paymentIn", true));
			criteriaE.addOrderBy("creationDate", true);

			List<SVFFIN_Payments> listE = criteriaE.list();
			return listE;
	}
	
	
	
	public static treeNodeData getNodeData(org.openbravo.database.ConnectionProvider connectionProvider,String nodeId) throws ServletException{
		
		 String strSql = 
				 		 "  SELECT                                                              "+
						 "    ad_tree.name,                                                     "+
						 "    ad_treenode.ad_treenode_id,                                       "+
						 "    ad_treenode.ad_tree_id,                                           "+
						 "    ad_treenode.node_id,                                              "+
						 "    ad_treenode.parent_id,                                            "+
						 "    ad_treenode.seqno                                                "+
						 "  FROM                                                                "+
						 "    public.ad_treenode,                                               "+
						 "    public.ad_tree                                                   "+
						 "  WHERE                                                               "+
						 "    ad_treenode.ad_tree_id = ad_tree.ad_tree_id AND                   "+
						 "	 ad_treenode.node_id = '" + nodeId + "'       "+
						 "  order by seqno ;                                                    ";
		 
		 ResultSet result;
		    PreparedStatement st = null;
		    
		    treeNodeData objdata = null;
		    int iParameter = 0;
		    try {
		    	st = connectionProvider.getPreparedStatement(strSql);
		    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
		    	result = st.executeQuery();
		    	int pos = 0;
		    	
		    	while (result.next()) {
		    		objdata = new treeNodeData();
		    		objdata.setAdTreeNode_Parent_Id(UtilSql.getValue(result, "parent_id"));
		    		objdata.setAdTreeNode_Id(UtilSql.getValue(result, "ad_treenode_id"));
		    		objdata.setAdTreeNode_NodeId(UtilSql.getValue(result, "node_id"));
		    	}
		    	result.close();
		    } catch(SQLException e){
		      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		    } catch(Exception ex){
		      throw new ServletException("@CODE=@" + ex.getMessage());
		    } finally {
		      try {
		        connectionProvider.releasePreparedStatement(st);
		      } catch(Exception ignore){
		        ignore.printStackTrace();
		      }
		    }
		   
		    return objdata;
		 
	}
	
	public static List<treeNodeData> getHierarchy(org.openbravo.database.ConnectionProvider connectionProvider,String parent) throws ServletException{
		 
		 String strSql = 
				 		 "  SELECT                                                              "+
						 "    ad_tree.name,                                                     "+
						 "    ad_treenode.ad_treenode_id,                                       "+
						 "    ad_treenode.ad_tree_id,                                           "+
						 "    ad_treenode.node_id,                                              "+
						 "    ad_treenode.parent_id,                                            "+
						 "    ad_treenode.seqno                                                "+
						 "  FROM                                                                "+
						 "    public.ad_treenode,                                               "+
						 "    public.ad_tree                                                   "+
						 "  WHERE                                                               "+
						 "    ad_treenode.ad_tree_id = ad_tree.ad_tree_id AND                   "+
						 "	 ad_treenode.parent_id='" + parent + "' AND                                     "+
						 "    ad_tree.isactive='Y' AND                                          "+
						 "	 ad_treenode.isactive='Y'                                       "+
						 "  order by seqno ;                                                    ";
		 
		 ResultSet result;
		    PreparedStatement st = null;
		    
		 List<treeNodeData> listData = new ArrayList<treeNodeData>();
		    int iParameter = 0;
		    try {
		    	st = connectionProvider.getPreparedStatement(strSql);
		    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
		    	result = st.executeQuery();
		    	int pos = 0;
		    	while (result.next()) {
		    		treeNodeData objdata = new treeNodeData();
		    		objdata.setAdTreeNode_Id(UtilSql.getValue(result, "ad_treenode_id"));
		    		objdata.setAdTreeNode_NodeId(UtilSql.getValue(result, "node_id"));
		    		listData.add(objdata);
		    	}
		    	result.close();
		    } catch(SQLException e){
		      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
		    } catch(Exception ex){
		      throw new ServletException("@CODE=@" + ex.getMessage());
		    } finally {
		      try {
		        connectionProvider.releasePreparedStatement(st);
		      } catch(Exception ignore){
		        ignore.printStackTrace();
		      }
		    }
		    for (treeNodeData objTreeNodeData : listData) {
		    	objTreeNodeData.setChilds(getHierarchy(connectionProvider,objTreeNodeData.getAdTreeNode_NodeId()));
			}
		    return listData;
		 
	}
	
	
	
	private static SVFFIN_Deposit_Mngt getDeposit_Mngt(Client client,Organization org,BusinessPartner bp){
		
		 OBCriteria<SVFFIN_Deposit_Mngt> criteriaE = OBDal.getInstance().createCriteria(SVFFIN_Deposit_Mngt.class);

			criteriaE.add(Restrictions.eq("client", client));
			criteriaE.add(Restrictions.eq("organization", org));
			criteriaE.add(Restrictions.eq("bpartner", bp));

			List<SVFFIN_Deposit_Mngt> listE = criteriaE.list();
			if(listE.size() >0){
				return listE.get(0);
			}
			else
				return null;
	}
	
	public static SVFFIN_Payments getDepositPayment(FIN_Payment payment){
		
		 OBCriteria<SVFFIN_Payments> criteriaE = OBDal.getInstance().createCriteria(SVFFIN_Payments.class);

			criteriaE.add(Restrictions.eq("fINPayment", payment));
			
			List<SVFFIN_Payments> listE = criteriaE.list();
			if(listE.size() >0){
				return listE.get(0);
			}
			else
				return null;
	}
	
		
	public static PDFieldProvider[] getPaymentMethodsPDFields(Client client)
	{
		List<FIN_PaymentMethod> list = getPaymentMethods(client);
		PDFieldProvider[] pdValues = new PDFieldProvider[list.size()];
		if(list.size() > 0){
			for(int i=0;i<list.size();i++){
				FIN_PaymentMethod data = list.get(i);
				pdValues[i] = new PDFieldProvider("NULL", data.getName(), data.getId());
			}
		}else{
			pdValues = new PDFieldProvider[1];
			pdValues[0] = new PDFieldProvider("NULL", "Unknown", "Unknown");
			
		}
		
		return pdValues;
	}
	
	public static PDFieldProvider[] getFinancialAccountPDFields(org.openbravo.database.ConnectionProvider connectionProvider,String org) throws ServletException
	{
		List<FIN_FinancialAccount> list = getBranchAccountList(connectionProvider, org);
		PDFieldProvider[] pdValues = new PDFieldProvider[list.size()];
		if(list.size() > 0){
			for(int i=0;i<list.size();i++){
				FIN_FinancialAccount data = list.get(i);
				pdValues[i] = new PDFieldProvider("NULL", data.getName(), data.getId());
			}
		}else{
			pdValues = new PDFieldProvider[1];
			pdValues[0] = new PDFieldProvider("NULL", "Unknown", "Unknown");
			
		}
		
		return pdValues;
	}
	
	public static PDFieldProvider[] getGlitemsByOrder(List<Order> orderList) throws ServletException
	{
		List<GLItem> list = loadGLItemByOrder(orderList) ;
		
		PDFieldProvider[] pdValues = new PDFieldProvider[list.size()];
		if(list.size() > 0){
			for(int i=0;i<list.size();i++){
				GLItem data = list.get(i);
				pdValues[i] = new PDFieldProvider("NULL", data.getName(), data.getId());
			}
		}else{
			pdValues = new PDFieldProvider[1];
			pdValues[0] = new PDFieldProvider("NULL", "Unknown", "Unknown");
			
		}
		
		return pdValues;
	}
	
	public static PDFieldProvider[] getFinancialAccountMethods(String financialAccount)
	{
		List<FIN_PaymentMethod> list = getPaymentMethodsByAccount(financialAccount);
		PDFieldProvider[] pdValues = new PDFieldProvider[list.size()];
		if(list.size() > 0){
			for(int i=0;i<list.size();i++){
				FIN_PaymentMethod data = list.get(i);
				pdValues[i] = new PDFieldProvider("NULL", data.getName(), data.getId());
			}
		}else{
			pdValues = new PDFieldProvider[1];
			pdValues[0] = new PDFieldProvider("NULL", "Unknown", "Unknown");
			
		}
		
		return pdValues;
	}
	
	public static boolean isSaleDoc(String documentId){
		
		org.openbravo.model.ad.ui.Window window = OBDal.getInstance().get(org.openbravo.model.ad.ui.Window.class, documentId);
		if(window.getName().equalsIgnoreCase("Sales Order") || window.getName().equalsIgnoreCase("Sales Invoice"))
			return true;
		else
			return false;
		
	}
	
	public static String DefaultFinancialAccountForSaleDoc(String orgId){
		
		String result = "";
		Organization org = OBDal.getInstance().get(Organization.class, orgId);
		OBCriteria<FIN_FinancialAccount> criteriaE = OBDal.getInstance().createCriteria(FIN_FinancialAccount.class);

		criteriaE.add(Restrictions.eq("name", "Undeposited Funds"));
		criteriaE.add(Restrictions.eq("organization", org));
		
		List<FIN_FinancialAccount> listE = criteriaE.list();
		if(listE.size() > 0)
			result = listE.get(0).getId();
		
		return result;
		
	}
	
	public static FIN_Payment UpdateCredit(FIN_Payment credit,BigDecimal amountPaid,String paymentNo) throws Exception{
		
		credit.setGeneratedCredit(credit.getGeneratedCredit().subtract(amountPaid));
		credit.setUsedCredit(credit.getUsedCredit().add(amountPaid));
		String description = credit.getDescription();
		String[] listData = description.split(Pattern.quote("|"));
		String newCreditPaid = "|" + amountPaid.toString() +" used-payment:" + paymentNo;
		
		if(listData.length == 1){
			description = "Amount left as credit:" + credit.getGeneratedCredit().toString() + newCreditPaid   ;
		}
		else{
			description = "Amount left as credit:" + credit.getGeneratedCredit().toString()    ;
			for (int i = 1; i < listData.length; i++) {
				description = description + "|" + listData[i];
			}
			description = description + newCreditPaid;
		}
		
		credit.setDescription(description);
	
	   OBContext.setAdminMode();

	   try{
		  OBDal.getInstance().save(credit);
		  OBDal.getInstance().flush();
		  OBDal.getInstance().refresh(credit);		
		  OBDal.getInstance().commitAndClose();
	   }
	   catch(Exception ex){
		  ex.printStackTrace();
		  credit = null;
	   }
	   finally{
		  OBContext.restorePreviousMode();
	   }
		  
	   return credit;
   }
	
	
	private static FIN_Payment savePayment(FIN_Payment payment) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(payment);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(payment);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  payment = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return payment;
	  }
	
	private static Invoice saveInvoice(Invoice payment) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(payment);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(payment);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  payment = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return payment;
	  }
	
	private static FIN_PaymentDetail savePaymentDetail(FIN_PaymentDetail payment) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(payment);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(payment);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  payment = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return payment;
	  }
	
	private static FIN_PaymentScheduleDetail savePaymentScheduleDetail(FIN_PaymentScheduleDetail paymentScheduleDetail, String paymentDetailId) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  FIN_PaymentDetail PaymentDetail = OBDal.getInstance().get(FIN_PaymentDetail.class, paymentDetailId);
			  paymentScheduleDetail.setPaymentDetails(PaymentDetail);
			  OBDal.getInstance().save(paymentScheduleDetail);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(paymentScheduleDetail);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  paymentScheduleDetail = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return paymentScheduleDetail;
	  }
	
	public static SVFFIN_Deposit_Mngt saveDepositMng(SVFFIN_Deposit_Mngt depositMng) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(depositMng);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(depositMng);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  depositMng = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return depositMng;
	  }
	
	public static SVFFIN_Deposit saveDeposit(SVFFIN_Deposit deposit) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(deposit);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(deposit);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  deposit = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return deposit;
	  }
	
	private static Sequence updateSequence(Sequence seq) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  
			  OBDal.getInstance().save(seq);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(seq);		   
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  seq = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return seq;
	  }
	
	public static SVFFIN_Payments saveDepositPayment(SVFFIN_Payments depositPayment) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(depositPayment);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(depositPayment);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  depositPayment = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return depositPayment;
	  }
	
	public static GLItem saveGlitem(GLItem glItem) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(glItem);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(glItem);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  glItem = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return glItem;
	  }	
	
	public static FIN_FinaccTransaction saveTransaction(FIN_FinaccTransaction trans) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(trans);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(trans);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  trans = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return trans;
	  }
	
	public static FIN_FinancialAccount updateAccount(FIN_FinancialAccount account) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(account);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(account);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  account = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return account;
	  }

	public static FIN_Payment createFINPayment(org.openbravo.database.ConnectionProvider connectionProvider,Organization org, Client client, BusinessPartner bp,FIN_PaymentMethod payMethod,FIN_FinancialAccount finAccount,Date date,BigDecimal newAmount,String descripcion,boolean isReceipt) throws Exception{
		
		FIN_Payment payment = new FIN_Payment();
			
		DocumentType doctype = getDocTypeSql(connectionProvider,org,isReceipt);
		Sequence sequence = doctype.getDocumentSequence();
		String docNo = (sequence.getPrefix()!=null?sequence.getPrefix():"")+sequence.getNextAssignedNumber().toString() + (sequence.getSuffix()!=null?sequence.getSuffix():"");
		
		payment.setClient(client);
		payment.setOrganization(org);
		payment.setDocumentType(doctype);
		payment.setActive(true);
		payment.setReceipt(isReceipt);
		payment.setBusinessPartner(bp);
		payment.setPaymentDate(date);
		payment.setDescription(descripcion);
		payment.setCurrency(bp.getCurrency());
		payment.setAmount(newAmount);
		payment.setWriteoffAmount(new BigDecimal(0));
		payment.setPaymentMethod(payMethod);
		payment.setDocumentNo(docNo);
		payment.setStatus("RPPC");
		payment.setProcessed(false);
		payment.setProcessNow(false);
		payment.setPosted("N");
		payment.setAccount(finAccount);
		payment.setCreatedByAlgorithm(false);  
		payment.setAPRMProcessPayment("RE");
		
		payment = savePayment(payment);
	
		//sequence.setNextAssignedNumber(sequence.getNextAssignedNumber()+1);
		//updateSequence(sequence);
		
	    return payment;
	}
	
public static Invoice updateInvoice(String invoiceId,BigDecimal totalPaid,BigDecimal totalToPaid,Boolean isPaid) throws Exception{
		
		Invoice inv = OBDal.getInstance().get(Invoice.class, invoiceId);
			
		inv.setPaymentComplete(isPaid);
		inv.setTotalPaid(totalPaid);
		inv.setOutstandingAmount(totalToPaid);
		inv = saveInvoice(inv);
	
		//sequence.setNextAssignedNumber(sequence.getNextAssignedNumber()+1);
		//updateSequence(sequence);
		
	    return inv;
	}

public static FIN_FinaccTransaction createFINPaymentTransaction(org.openbravo.database.ConnectionProvider connectionProvider,Organization org, Client client, BusinessPartner bp,GLItem gLItem,FIN_FinancialAccount account,FIN_Payment payment,Date date,BigDecimal newAmount,String descripcion,boolean isReceipt,BigDecimal directPayment) throws Exception{
		
	FIN_FinaccTransaction transaction = new FIN_FinaccTransaction();
			
		DocumentType doctype = getDocTypeSql(connectionProvider,org,isReceipt);
		Sequence sequence = doctype.getDocumentSequence();
		String docNo = (sequence.getPrefix()!=null?sequence.getPrefix():"")+sequence.getNextAssignedNumber().toString() + (sequence.getSuffix()!=null?sequence.getSuffix():"");
		
		transaction.setClient(client);
		transaction.setOrganization(org);
		transaction.setActive(true);
		transaction.setBusinessPartner(bp);
		transaction.setDescription(descripcion);
		transaction.setCurrency(bp.getCurrency());
		
		if(gLItem != null)
			transaction.setGLItem(gLItem);
		transaction.setAccount(account);
		transaction.setStatus("RPPC");
		transaction.setTransactionType("BF");
		transaction.setProcessed(true);
		transaction.setProcessNow(false);
		transaction.setPosted("N");
		transaction.setLineNo(new Long(10));
		transaction.setCreatedByAlgorithm(false);  
		transaction.setFinPayment(payment);	
		transaction.setDateAcct(new Date());
		transaction.setTransactionDate(new Date());
		transaction.setAPRMModify(false);
		transaction.setAprmProcessed("R");
		transaction.setSvfsvActive(true);
		
		
	
		sequence.setNextAssignedNumber(sequence.getNextAssignedNumber()+1);
		updateSequence(sequence);
		if(!isReceipt){
			
			account.setCurrentBalance(account.getCurrentBalance().subtract(directPayment));
			transaction.setPaymentAmount(newAmount);
			transaction.setDepositAmount(new BigDecimal(0));
		}
		else{
			account.setCurrentBalance(account.getCurrentBalance().add(directPayment));
			transaction.setPaymentAmount(new BigDecimal(0));
			transaction.setDepositAmount(newAmount);
		}
		
		transaction = saveTransaction(transaction);
		account = updateAccount(account);
		
		
	    return transaction;
	}
	
	
	
	
	
public static FIN_PaymentDetail createFINPaymentDetailts(Organization org, Client client, BusinessPartner bp,FIN_Payment payment,GLItem gLItem,BigDecimal newAmount) throws Exception{
		
		FIN_PaymentDetail paymentDetail = new FIN_PaymentDetail();
		
		paymentDetail.setClient(client);
		paymentDetail.setOrganization(org);
		paymentDetail.setFinPayment(payment);
		//paymentDetail.setAmount(newAmount);
		if(gLItem != null)
			paymentDetail.setGLItem(gLItem);
		paymentDetail.setActive(true);
		//paymentDetail.setPrepayment(false);
		
		paymentDetail = savePaymentDetail(paymentDetail);
	
	    return paymentDetail;
	}

public static String createFINPaymentDetailtsSQL(org.openbravo.database.ConnectionProvider connectionProvider,String clientId, String orgId,String Fin_Payment_Id,String glitem,String amount) throws Exception
{
	String glItemStr = ",null";
	if(glitem != null)
		glItemStr = ",'" + glitem + "'";
	String newId = SequenceIdData.getUUID();
	
	
	String strSql = "    INSERT INTO fin_payment_detail(                                                    "+
			"                fin_payment_detail_id, ad_client_id, ad_org_id, created, createdby,    "+
			"                updated, updatedby, fin_payment_id,isactive,          "+
			"                c_glitem_id,amount)                                               "+
			"        VALUES ('" + newId + "','" + clientId + "', '" + orgId + "', TO_DATE(NOW()), '100',                                                         "+
			"                 TO_DATE(NOW()), '100', '" + Fin_Payment_Id + "', 'Y'                                                      "+
			"                "+ glItemStr +","+amount+");                                                                    ";                                            
  PreparedStatement st = null;
  try {
  	st = connectionProvider.getPreparedStatement(strSql);
  	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
  	st.execute();
  } catch(SQLException e){
    throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
  } catch(Exception ex){
    throw new ServletException("@CODE=@" + ex.getMessage());
  } finally {
    try {
      connectionProvider.releasePreparedStatement(st);
    } catch(Exception ignore){
      ignore.printStackTrace();
    }
  }
  return newId;
}
	
	public static SVFFIN_Deposit getSvffin_Deposit(Order order,SVFFIN_Deposit_Mngt depositMng,GLItem glItem) throws Exception{
		
		SVFFIN_Deposit deposit = geDeposits(glItem);
		
		if(deposit == null){
			
			deposit = new SVFFIN_Deposit();
			
			deposit.setClient(depositMng.getClient());
			deposit.setOrganization(depositMng.getOrganization());
			deposit.setActive(true);
			deposit.setOrder(order);
			deposit.setInvoiceTotal(order.getGrandTotalAmount());
			deposit.setBalance(new BigDecimal(0));
			deposit.setGlitem(glItem);
			deposit.setQTYDeposited(new BigDecimal(0));
			deposit.setPayments(new BigDecimal(0));
			deposit.setSvffinDepositMngt(depositMng);
			deposit.setDepositDate(new Date());
			deposit.setReferencePfi(order.getSvfsvReferencePfi());
			
			deposit = saveDeposit(deposit);
		}

		deposit.setReferencePfi(order.getSvfsvReferencePfi());
		
		
	    return deposit;
	}
	
	public static SVFFIN_Deposit_Mngt getDepositMng(Organization org, Client client, BusinessPartner bp) throws Exception{
			
		SVFFIN_Deposit_Mngt depositMng = getDeposit_Mngt(client, org, bp);
		if(depositMng == null){
				
			depositMng = new SVFFIN_Deposit_Mngt();
		
			depositMng.setClient(client);
			depositMng.setOrganization(org);
			depositMng.setActive(true);
			depositMng.setBpartner(bp);
			depositMng.setTotalDeposits(new BigDecimal(0));
			depositMng.setTotalUsedDeposits(new BigDecimal(0));
			depositMng.setBalance(new BigDecimal(0));
	
			depositMng = saveDepositMng(depositMng);
		}
	    return depositMng;
	}
	
	public static SVFFIN_Payments createSVFFinPayment(org.openbravo.database.ConnectionProvider connectionProvider,Organization org, Client client, FIN_Payment finPayment,SVFFIN_Deposit svffinDeposit,Boolean inPayment,Invoice invoice,Order order,BigDecimal amount) throws Exception{
		
		
		SVFFIN_Payments payment = new SVFFIN_Payments();
		payment.setOrganization(org);
		payment.setClient(client);
		payment.setFINPayment(finPayment);
		payment.setPaymentDate(new Date());
		payment.setPaymentIn(inPayment);
		payment.setActive(true);
		payment.setSvffinDeposit(svffinDeposit);
		payment.setAmountUsed(amount);
		if(invoice != null)
			payment.setInvoice(invoice);
		if(order != null)
			payment.setOrder(order);		
		
		payment = saveDepositPayment(payment);
		String depositMngId = getDepositMngId(connectionProvider, svffinDeposit.getId());
		SVFFIN_Deposit_Mngt depositMng = getDepositMng(depositMngId);
		
		if(inPayment){
			BigDecimal newAmount = svffinDeposit.getQTYDeposited().add(amount);
			BigDecimal newBalance = newAmount.subtract(svffinDeposit.getPayments());
			svffinDeposit.setQTYDeposited(newAmount);
			svffinDeposit.setBalance(newBalance);
			
			BigDecimal depositMngTotal = depositMng.getTotalDeposits().add(amount);
			BigDecimal depositMngBalance = depositMngTotal.subtract(depositMng.getTotalUsedDeposits());
			depositMng.setTotalDeposits(depositMngTotal);
			depositMng.setBalance(depositMngBalance);
		}else{
			BigDecimal newAmount = svffinDeposit.getPayments().add(amount);
			BigDecimal newBalance = svffinDeposit.getQTYDeposited().subtract(newAmount);
			svffinDeposit.setPayments(newAmount);
			svffinDeposit.setBalance(newBalance);
			
			BigDecimal depositMngUsedTotal = depositMng.getTotalUsedDeposits().add(amount);
			BigDecimal depositMngBalance = depositMng.getTotalDeposits().subtract(depositMngUsedTotal);
			depositMng.setTotalUsedDeposits(depositMngUsedTotal);
			depositMng.setBalance(depositMngBalance);
		}
		
		
		saveDeposit(svffinDeposit);
		saveDepositMng(depositMng);
		
	    return payment;
	}
	
	public static GLItem getGLItem(Organization org, Client client, BusinessPartner bp,String documentId,String docNo) throws Exception{
		
		GLItem glItem = loadGLItem(documentId);
		if(glItem == null){
				
			glItem = new GLItem();
		
			glItem.setClient(client);
			glItem.setOrganization(org);
			glItem.setActive(true);
			glItem.setSvfsvDepositdocid(documentId);
			glItem.setName(docNo+" - Liability Deposit");
			glItem.setDescription("GLItem for:" + docNo + " - Liability Deposit");
			glItem.setEnableInCash(false);
			glItem.setEnableInFinancialInvoices(false);
	
			glItem = saveGlitem(glItem);
		}
	    return glItem;
	}
	
	
	
	public static List<treeNodeData> getParent(org.openbravo.database.ConnectionProvider connectionProvider,String nodeId,List<treeNodeData> list) throws ServletException{
	
		treeNodeData nodeData = getNodeData(connectionProvider, nodeId);
		list.add(nodeData);
		if(!nodeData.getAdTreeNode_Parent_Id().equalsIgnoreCase("0")){
			list = getParent(connectionProvider, nodeData.getAdTreeNode_Parent_Id(),list);
		}			
		return list;
	}

	public static boolean existInList(FIN_FinancialAccount account,List<FIN_FinancialAccount> list){
		boolean found = false;
		for (int i = 0; ((i < list.size())&&(!found)); i++) {
			if(list.get(i).getId().equalsIgnoreCase(account.getId()))
				found = true;
		}
		return found;
	}
	
	public static List<FIN_FinancialAccount> getBranchAccountList(org.openbravo.database.ConnectionProvider connectionProvider,String nodeId) throws ServletException{
		List<treeNodeData> listNodes = new ArrayList<treeNodeData>();
		listNodes = getParent(connectionProvider,nodeId,listNodes);
		List<treeNodeData> listOrgs = getHierarchy(connectionProvider, nodeId);
		listNodes.addAll(listOrgs);
		List<FIN_FinancialAccount> list = new ArrayList<FIN_FinancialAccount>();
		for (treeNodeData treeNodeData : listNodes) {
			Organization org = OBDal.getInstance().get(Organization.class, treeNodeData.getAdTreeNode_NodeId());
			List<FIN_FinancialAccount> accountFromOrg = getFinancialAccount(org);
			for (FIN_FinancialAccount fin_FinancialAccount : accountFromOrg) {
				if(!existInList(fin_FinancialAccount, list)){
					list.add(fin_FinancialAccount);
				}
			}
			
		}
		return list;
	}

	
}
