package com.spocsys.vigfurnitures.services.utils;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;

public class PaymentPlanDataSearchClass {

	public PaymentPlanDataSearchClass() {
		// TODO Auto-generated constructor stub
	}
	
	public static Order getOrder(String orderId){
		Order order = OBDal.getInstance().get(Order.class, orderId);
		return order;
	}
	
	public static Invoice getInvoice(String invoiceId){
		Invoice invoice = OBDal.getInstance().get(Invoice.class, invoiceId);
		return invoice;
	}
	
	public static FIN_PaymentSchedule getFinPaymentSchedule(String Id){
		FIN_PaymentSchedule finPaymentSchedule = OBDal.getInstance().get(FIN_PaymentSchedule.class, Id);
		return finPaymentSchedule;
	}
	
	public static Client getClient(String clientId){
		Client client = OBDal.getInstance().get(Client.class, clientId);
		return client;
	}
	
	public static Organization getOrganization(String organizationId){
		Organization organization = OBDal.getInstance().get(Organization.class, organizationId);
		return organization;
	}
	
	public static List<FIN_PaymentSchedule> getPaymentPlanFromOrder(Order order){
		
		 OBCriteria<FIN_PaymentSchedule> criteriaE = OBDal.getInstance().createCriteria(FIN_PaymentSchedule.class);

			criteriaE.add(Restrictions.eq("order", order));
			criteriaE.addOrderBy("creationDate", true);

			List<FIN_PaymentSchedule> listE = criteriaE.list();
			return listE;
	}
	
	public static List<FIN_PaymentSchedule> getPaymentPlanFromInvoice(Invoice invoice){
		
		 OBCriteria<FIN_PaymentSchedule> criteriaE = OBDal.getInstance().createCriteria(FIN_PaymentSchedule.class);

			criteriaE.add(Restrictions.eq("invoice", invoice));
			criteriaE.addOrderBy("creationDate", true);

			List<FIN_PaymentSchedule> listE = criteriaE.list();
			return listE;
	}
	
	public static List<FIN_PaymentMethod> getPaymentMethods(Client client){
		
		 OBCriteria<FIN_PaymentMethod> criteriaE = OBDal.getInstance().createCriteria(FIN_PaymentMethod.class);

			criteriaE.add(Restrictions.eq("client", client));
			criteriaE.add(Restrictions.eq("payoutAllow", true));
			criteriaE.addOrderBy("name", true);

			List<FIN_PaymentMethod> listE = criteriaE.list();
			return listE;
	}
	
	public static List<FIN_PaymentScheduleDetail> getPaymentDetailOrder(FIN_PaymentSchedule payment){
		
		 OBCriteria<FIN_PaymentScheduleDetail> criteriaE = OBDal.getInstance().createCriteria(FIN_PaymentScheduleDetail.class);

			criteriaE.add(Restrictions.eq("orderPaymentSchedule", payment));
			criteriaE.addOrderBy("creationDate", true);
			
			List<FIN_PaymentScheduleDetail> listE = criteriaE.list();
			return listE;
	}
	
	public static List<FIN_PaymentScheduleDetail> getPaymentDetailInvoice(FIN_PaymentSchedule payment){
		
		 OBCriteria<FIN_PaymentScheduleDetail> criteriaE = OBDal.getInstance().createCriteria(FIN_PaymentScheduleDetail.class);

			criteriaE.add(Restrictions.eq("invoicePaymentSchedule", payment));
			criteriaE.addOrderBy("creationDate", true);
			
			List<FIN_PaymentScheduleDetail> listE = criteriaE.list();
			return listE;
	}
	
	public static PDFieldProvider[] getPaymentMethodsPDFields(Client client)
	{
		List<FIN_PaymentMethod> list = getPaymentMethods(client);
		PDFieldProvider[] pdValues = new PDFieldProvider[list.size()];
		if(list.size() > 0){
			for(int i=0;i<list.size();i++){
				FIN_PaymentMethod data = list.get(i);
				pdValues[i] = new PDFieldProvider("NULL", data.getName(), data.getId());
			}
		}else{
			pdValues = new PDFieldProvider[1];
			pdValues[0] = new PDFieldProvider("NULL", "Unknown", "Unknown");
			
		}
		
		return pdValues;
	}
	
	private static FIN_PaymentSchedule savePlan(FIN_PaymentSchedule paymentPlan) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(paymentPlan);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(paymentPlan);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  paymentPlan = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return paymentPlan;
	  }
	
	private static FIN_PaymentScheduleDetail savePlanDetail(FIN_PaymentScheduleDetail paymentPlanDetail) throws Exception
	  {
		  OBContext.setAdminMode();

		  try{
			  OBDal.getInstance().save(paymentPlanDetail);
		      OBDal.getInstance().flush();
		      OBDal.getInstance().refresh(paymentPlanDetail);		
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  paymentPlanDetail = null;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return paymentPlanDetail;
	  }
	
	public static boolean deletePlan(FIN_PaymentSchedule paymentPlan) throws Exception
	  {
		  OBContext.setAdminMode();
		  boolean result = true;
		  try{
			  OBDal.getInstance().remove(paymentPlan);;
		      OBDal.getInstance().flush();
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  result = false;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return result;
	  }
	
	public static boolean deletePlanDetail(FIN_PaymentScheduleDetail paymentPlan) throws Exception
	  {
		  OBContext.setAdminMode();
		  boolean result = true;
		  try{
			  OBDal.getInstance().remove(paymentPlan);;
		      OBDal.getInstance().flush();
		      OBDal.getInstance().commitAndClose();
		  }
		  catch(Exception ex){
			  ex.printStackTrace();
			  result = false;
		  }
		  finally{
			  OBContext.restorePreviousMode();
		  }
		  
		  return result;
	  }
	
	

	public static FIN_PaymentSchedule updateFINPaymentSchedule(FIN_PaymentSchedule fin_PaymentSchedule,BigDecimal newAmount) throws Exception{
		
		FIN_PaymentSchedule paymentSchedule = OBDal.getInstance().get(FIN_PaymentSchedule.class, fin_PaymentSchedule.getId());
			
		paymentSchedule.setAmount(newAmount);
		paymentSchedule.setOutstandingAmount(newAmount);
		
		paymentSchedule = savePlan(paymentSchedule);
	
	    return paymentSchedule;
	}
	
	public static FIN_PaymentSchedule updateFINPaymentSchedulePaid(FIN_PaymentSchedule fin_PaymentSchedule,BigDecimal paid) throws Exception{
		
		FIN_PaymentSchedule paymentSchedule = OBDal.getInstance().get(FIN_PaymentSchedule.class, fin_PaymentSchedule.getId());
			
		paymentSchedule.setPaidAmount(paymentSchedule.getPaidAmount().add(paid));
		paymentSchedule.setOutstandingAmount(paymentSchedule.getAmount().subtract(paymentSchedule.getPaidAmount()));
		
		paymentSchedule = savePlan(paymentSchedule);
	
	    return paymentSchedule;
	}

	
	public static FIN_PaymentSchedule createFINPaymentScheduleInvoice(String orgId,String adClientId,String bPartnerId,Invoice invoice,String finPaymentMethodId,BigDecimal amount,Date duedate,Date expectedDate) throws Exception{
		
		Organization organization = OBDal.getInstance().get(Organization.class, orgId);
		adClientId = adClientId.replace("'", "");
		Client client = OBDal.getInstance().get(Client.class, adClientId);
		BusinessPartner bp = OBDal.getInstance().get(BusinessPartner.class, bPartnerId);
		FIN_PaymentMethod finpayMethod = OBDal.getInstance().get(FIN_PaymentMethod.class, finPaymentMethodId);
		Date date = new Date();

	
		FIN_PaymentSchedule paymentSchedule = new FIN_PaymentSchedule();
		
		paymentSchedule.setOrganization(organization);
		paymentSchedule.setClient(client);
		paymentSchedule.setInvoice(invoice);;
		paymentSchedule.setDueDate(duedate);
		paymentSchedule.setExpectedDate(expectedDate);
		paymentSchedule.setActive(true);
		paymentSchedule.setFinPaymentmethod(finpayMethod);
		paymentSchedule.setCurrency(invoice.getCurrency());
		paymentSchedule.setAmount(amount);
		paymentSchedule.setPaidAmount(new BigDecimal(0));
		paymentSchedule.setOutstandingAmount(amount);
		paymentSchedule.setAprmModifPaymentINPlan(false);
		paymentSchedule.setAprmModifPaymentOUTPlan(false);
		
		paymentSchedule = savePlan(paymentSchedule);
		//String accountId = bp.getAccount().getId();
		
		
	    return paymentSchedule;
	}

	public static FIN_PaymentSchedule createFINPaymentScheduleOrder(String orgId,String adClientId,String bPartnerId,Order order,String finPaymentMethodId,BigDecimal amount,Date duedate,Date expectedDate) throws Exception{
	
	Organization organization = OBDal.getInstance().get(Organization.class, orgId);
	adClientId = adClientId.replace("'", "");
	Client client = OBDal.getInstance().get(Client.class, adClientId);
	BusinessPartner bp = OBDal.getInstance().get(BusinessPartner.class, bPartnerId);
	FIN_PaymentMethod finpayMethod = OBDal.getInstance().get(FIN_PaymentMethod.class, finPaymentMethodId);
	Date date = new Date();


	FIN_PaymentSchedule paymentSchedule = new FIN_PaymentSchedule();
	
	paymentSchedule.setOrganization(organization);
	paymentSchedule.setClient(client);
	paymentSchedule.setOrder(order);;
	paymentSchedule.setDueDate(duedate);
	paymentSchedule.setExpectedDate(expectedDate);
	paymentSchedule.setActive(true);
	paymentSchedule.setFinPaymentmethod(finpayMethod);
	paymentSchedule.setCurrency(order.getCurrency());
	paymentSchedule.setAmount(amount);
	paymentSchedule.setPaidAmount(new BigDecimal(0));
	paymentSchedule.setOutstandingAmount(new BigDecimal(0));
	paymentSchedule.setAprmModifPaymentINPlan(false);
	paymentSchedule.setAprmModifPaymentOUTPlan(false);
	
	paymentSchedule = savePlan(paymentSchedule);
	//String accountId = bp.getAccount().getId();
	
	
    return paymentSchedule;
}
	
	public static String saveFinPaymentScheduleDetail(org.openbravo.database.ConnectionProvider connectionProvider,String clientId, String orgId,String paymentScheduleId,boolean order,String amount,String bPartnerId,String writeOff) throws Exception
	  {
		String ids = "null,'" + paymentScheduleId + "',";
		if(order)
			ids = "'" + paymentScheduleId + "',null,";
		String newId = SequenceIdData.getUUID();
		String strSql = "  INSERT INTO fin_payment_scheduledetail(                                                                                                   "+
				"              fin_payment_scheduledetail_id, ad_client_id, ad_org_id, created,                                                              "+
				"              createdby, updated, updatedby, fin_payment_detail_id, fin_payment_schedule_order,fin_payment_schedule_invoice,                                             "+
				"              amount, isactive, writeoffamt,                                                                  "+
				"              iscanceled, c_bpartner_id, c_activity_id, m_product_id, c_campaign_id,                                                        "+
				"              c_project_id, c_salesregion_id, c_costcenter_id, user1_id, user2_id,                                                          "+
				"              doubtfuldebt_amount, isinvoicepaid, em_svfsv_poreference)                                                                     "+
				"      VALUES ('" + newId + "', '" + clientId + "', '" + orgId + "', TO_DATE(NOW()),   "+
				"              '100', TO_DATE(NOW()), '100', null, "+ ids +
				"              " + amount +", 'Y', " + writeOff + ",                                                                                                            "+
				"              'N', '" + bPartnerId + "', null, null, null,                                                                    "+
				"              null, null, null, null, null,                                                                                                 "+
				"              0, 'N', null);                                                                                                                ";                                                 
	    PreparedStatement st = null;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return newId;
	  }
	
	public static String saveFinPaymentScheduleDetailGlItem(org.openbravo.database.ConnectionProvider connectionProvider,String clientId, String orgId,String amount,String bPartnerId,String writeOff,String Fin_PaymentDetailId) throws Exception
	  {
		String ids = "null,null,";
		String newId = SequenceIdData.getUUID();
		String strSql = "  INSERT INTO fin_payment_scheduledetail(                                                                                                   "+
				"              fin_payment_scheduledetail_id, ad_client_id, ad_org_id, created,                                                              "+
				"              createdby, updated, updatedby, fin_payment_detail_id, fin_payment_schedule_order,fin_payment_schedule_invoice,                                             "+
				"              amount, isactive, writeoffamt,                                                                  "+
				"              iscanceled, c_bpartner_id, c_activity_id, m_product_id, c_campaign_id,                                                        "+
				"              c_project_id, c_salesregion_id, c_costcenter_id, user1_id, user2_id,                                                          "+
				"              doubtfuldebt_amount, isinvoicepaid, em_svfsv_poreference)                                                                     "+
				"      VALUES ('" + newId + "', '" + clientId + "', '" + orgId + "', TO_DATE(NOW()),   "+
				"              '100', TO_DATE(NOW()), '100', '" + Fin_PaymentDetailId + "', "+ ids +
				"              " + amount +", 'Y', " + writeOff + ",                                                                                                            "+
				"              'N', '" + bPartnerId + "', null, null, null,                                                                    "+
				"              null, null, null, null, null,                                                                                                 "+
				"              0, 'N', null);                                                                                                                ";                                                 
	    PreparedStatement st = null;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return newId;
	  }
	
	public static String updateFinPaymentScheduleDetail(org.openbravo.database.ConnectionProvider connectionProvider,String paymentScheduleId, String paymentDetailId,String amount) throws Exception
	  {
		String where = " fin_payment_scheduledetail_id  = '" + paymentScheduleId + "'";
		String setPaymentDetail = "";
		if(paymentDetailId != null)
			setPaymentDetail = " , fin_payment_detail_id =  '" + paymentDetailId + "'";
		
		String newId = SequenceIdData.getUUID();
		String strSql = "  Update fin_payment_scheduledetail                                                                                                   "+
				"          SET amount = " + amount + setPaymentDetail  +
				"          where "	+ where;
		
	    PreparedStatement st = null;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return newId;
	  }
	
	public static String updateFinPaymentScheduleDetail(org.openbravo.database.ConnectionProvider connectionProvider,String paymentScheduleId, String paymentDetailId) throws Exception
	  {

		String newId = SequenceIdData.getUUID();
		String strSql = "  Update fin_payment_scheduledetail                                                                                                   "+
				"          set fin_payment_detail_id =  '" + paymentDetailId + "' "+
				"          where fin_payment_scheduledetail_id  = '" + paymentScheduleId + "'";
		
	    PreparedStatement st = null;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return newId;
	  }
	
	
	public static void updateFinPayment(org.openbravo.database.ConnectionProvider connectionProvider,String paymentId) throws Exception
	  {
		String strSql = "  Update fin_payment                                                                                                  "+
				"          SET processed = 'Y'"  +
				"          where fin_payment_id='"	+ paymentId +"'";
		
	    PreparedStatement st = null;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	  }
	
	public static String updateFinPaymentScheduleDetailFull(org.openbravo.database.ConnectionProvider connectionProvider,String paymentScheduleId,String amount,String writeOff) throws Exception
	  {
		String where = " fin_payment_scheduledetail_id  = '" + paymentScheduleId + "'";
		String newId = SequenceIdData.getUUID();
		String strSql = "  Update fin_payment_scheduledetail                                                                                                   "+
				"          SET amount = " + amount + ", writeoffamt = " + writeOff +" "+
				"          where "	+ where;
		
	    PreparedStatement st = null;
	    try {
	    	st = connectionProvider.getPreparedStatement(strSql);
	    	//iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
	    	st.execute();
	    } catch(SQLException e){
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + " @" + e.getMessage());
	    } catch(Exception ex){
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    return newId;
	  }
	
public static String createFINPaymentScheduleDetailInvoice(org.openbravo.database.ConnectionProvider connectionProvider,String orgId,String adClientId,String bPartnerId,FIN_PaymentSchedule finPaymentSchedule,String finPaymentMethodId,BigDecimal amount,Date duedate,Date expectedDate,String writeOff) throws Exception{
		
		adClientId = adClientId.replace("'", "");
		Client client = OBDal.getInstance().get(Client.class, adClientId);
		
		return saveFinPaymentScheduleDetail(connectionProvider, client.getId(), orgId, finPaymentSchedule.getId(), false, amount.toString(), bPartnerId,writeOff);
		//String accountId = bp.getAccount().getId();
		
	}

	

	public static String createFINPaymentScheduleDetailOrder(org.openbravo.database.ConnectionProvider connectionProvider,String orgId, String adClientId,String bPartnerId,FIN_PaymentSchedule finPaymentSchedule,String finPaymentMethodId,BigDecimal amount,Date duedate,Date expectedDate,String writeOff) throws Exception{
		
		adClientId = adClientId.replace("'", "");
		Client client = OBDal.getInstance().get(Client.class, adClientId);
	
		return saveFinPaymentScheduleDetail(connectionProvider, client.getId(), orgId, finPaymentSchedule.getId(), true, amount.toString(), bPartnerId,writeOff);
	}
	
	public static BigDecimal getToPaidAmount(Invoice invoice){
		List<FIN_PaymentSchedule> list = getPaymentPlanFromInvoice(invoice);
		BigDecimal total = new BigDecimal(0);
		
		for (FIN_PaymentSchedule fin_PaymentSchedule : list) {
			total  = total.add(fin_PaymentSchedule.getAmount().subtract(fin_PaymentSchedule.getPaidAmount()));
		}
		
		return total;
	}
	
	public static BigDecimal getPaidAmount(Invoice invoice){
		List<FIN_PaymentSchedule> list = getPaymentPlanFromInvoice(invoice);
		BigDecimal total = new BigDecimal(0);
		
		for (FIN_PaymentSchedule fin_PaymentSchedule : list) {
			total  = total.add(fin_PaymentSchedule.getPaidAmount());
		}
		
		return total;
	}
}
