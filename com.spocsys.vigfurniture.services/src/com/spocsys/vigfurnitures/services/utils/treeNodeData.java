package com.spocsys.vigfurnitures.services.utils;

import java.util.ArrayList;
import java.util.List;

public class treeNodeData {
	private String adTreeNode_Parent_Id;
	private String adTreeNode_Id;
	private String adTreeNode_NodeId;
	private String adTreeNode_name;
	
	private List<treeNodeData> childs;

	public treeNodeData() {
		super();
		childs = new ArrayList<treeNodeData>();
		// TODO Auto-generated constructor stub
	}
	
	

	public String getAdTreeNode_Parent_Id() {
		return adTreeNode_Parent_Id;
	}



	public void setAdTreeNode_Parent_Id(String adTreeNode_Parent_Id) {
		this.adTreeNode_Parent_Id = adTreeNode_Parent_Id;
	}



	public String getAdTreeNode_Id() {
		return adTreeNode_Id;
	}

	public void setAdTreeNode_Id(String adTreeNode_Id) {
		this.adTreeNode_Id = adTreeNode_Id;
	}

	public String getAdTreeNode_NodeId() {
		return adTreeNode_NodeId;
	}

	public void setAdTreeNode_NodeId(String adTreeNode_NodeId) {
		this.adTreeNode_NodeId = adTreeNode_NodeId;
	}

	public String getAdTreeNode_name() {
		return adTreeNode_name;
	}

	public void setAdTreeNode_name(String adTreeNode_name) {
		this.adTreeNode_name = adTreeNode_name;
	}

	public List<treeNodeData> getChilds() {
		return childs;
	}

	public void setChilds(List<treeNodeData> childs) {
		this.childs = childs;
	}
	
	
	
	
}
