/************************************************************************************
 * Copyright (C) 2012-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
OB.SVFSV = OB.SVFSV || {};
OB.SVFSV.Process = {

generateGR: function (params, view) {
    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
    ciHeaders = [],
        callback;
    callback = function (rpcResponse, data, rpcRequest) {
      var message = data.message;
      view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
      // close process to refresh current view
      params.button.closeProcessPopup();
    };
    for (i = 0; i < selection.length; i++) {
    	ciHeaders.push(selection[i].id);
    }
    isc.confirm(OB.I18N.getLabel('OBWPACK_CreateConfirm'), function (clickedOK) {
      if (clickedOK) {
        OB.RemoteCallManager.call('com.spocsys.vigfurnitures.services.forms.GenerateGRtFromCIProcess', {
          ciHeaders: ciHeaders,
          action: 'process'
        }, {}, callback);
      }
    });
  }
 

};