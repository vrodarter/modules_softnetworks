package com.spocsys.vigfurniture.transfer.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;

import com.spocsys.vigfurniture.transfer.Movement;
import com.spocsys.vigfurniture.transfer.Movementline;
import com.spocsys.vigfurniture.transfer.TableTransf;

public class CreateTransferDocument {
  private static final Logger log = Logger.getLogger(CreateTransferDocument.class);

  static Movement createHeader(TableTransf regTableTransf) {

    try {

      Movement regMovement = OBProvider.getInstance().get(Movement.class);

      // ----------------------------
      regMovement.setClient(regTableTransf.getClient());
      regMovement.setOrganization(regTableTransf.getOrganization());
      regMovement.setCreatedBy(OBContext.getOBContext().getUser());
      regMovement.setUpdatedBy(OBContext.getOBContext().getUser());
      // -----------------------------
      regMovement.setName(new Date().toString());
      regMovement.setMovementDate(new Date());
      // ------------------------------

      if (regTableTransf.getInitialWarehouse() != null) {
        regMovement.setInitialWarehouse(regTableTransf.getInitialWarehouse());
      }

      regMovement.setTransitWarehouse(null);

      if (regTableTransf.getFinalWarehouse() != null) {
        regMovement.setFinalWarehouse(regTableTransf.getFinalWarehouse());
      }
      regMovement.setOrder(regTableTransf.getSalesOrder());

      return regMovement;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  static Movementline createMovementLine(TableTransf regTableTransf, Movement regMovement,
      Product regProduct, BigDecimal Qty, long Line) {

    try {

      Movementline regMovementLine = OBProvider.getInstance().get(Movementline.class);
      // ---------------------------------------------

      regMovementLine.setClient(regTableTransf.getClient());
      regMovementLine.setOrganization(regTableTransf.getOrganization());
      regMovementLine.setCreatedBy(OBContext.getOBContext().getUser());
      regMovementLine.setUpdatedBy(OBContext.getOBContext().getUser());
      // ------------------------------
      regMovementLine.setMovement(regMovement);
      regMovementLine.setLineNo(Line);
      regMovementLine.setMovementQuantity(Qty);
      regMovementLine.setProduct(regProduct);
      regMovementLine.setAttributeSetValue(regTableTransf.getAttributeSetValue());
      regMovementLine.setUOM(regTableTransf.getUOM());
      regMovementLine.setOrderline(regTableTransf.getSalesOrderLine());
      // ------------------------------
      return regMovementLine;
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  public static Movement create(List<TableTransf> ListTableTransf) {

    List<Movementline> regMovementList = new ArrayList<Movementline>();
    Movement regMovement = null;
    if (ListTableTransf.size() > 0) {
      regMovement = createHeader(ListTableTransf.get(0));

      if (regMovement == null) {
        return null;
      }
      int LastNoLine = 0;
      Long nroLine = Long.parseLong(String.valueOf(LastNoLine));
      for (TableTransf regTableTransf : ListTableTransf) {

        LastNoLine = LastNoLine + 10;
        nroLine = Long.parseLong(String.valueOf(LastNoLine));
        Movementline regMovementLine = null;

        if (regTableTransf.getProduct().isBillOfMaterials() == true) {
          for (ProductBOM regProductBOM : regTableTransf.getProduct().getProductBOMList()) {

            nroLine = Long.parseLong(String.valueOf(LastNoLine));
            regMovementLine = createMovementLine(regTableTransf, regMovement,
                regProductBOM.getBOMProduct(),
                regProductBOM.getBOMQuantity().multiply(regTableTransf.getMovementQuantity()),
                nroLine);
            LastNoLine = LastNoLine + 10;

            if (regMovementLine == null) {
              return null;
            }
            regMovementList.add(regMovementLine);

          }

        } else {

          regMovementLine = createMovementLine(regTableTransf, regMovement,
              regTableTransf.getProduct(), regTableTransf.getMovementQuantity(), nroLine);
          if (regMovementLine == null) {
            return null;
          }
          regMovementList.add(regMovementLine);

        }

      }

      try {

        regMovement.setSVFTRAFMovementlineList(regMovementList);

        // OBDal.getInstance().save(regMovement);
        // OBDal.getInstance().flush();
        // OBDal.getInstance().commitAndClose();

      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return null;
      }
      return regMovement;

    }
    return null;

  }
}
