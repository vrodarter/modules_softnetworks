package com.spocsys.vigfurniture.transfer.erpCommon.ad_actionButton;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;

import com.spocsys.vigfurniture.transfer.Movement;
import com.spocsys.vigfurniture.transfer.TableTransf;
import com.spocsys.vigfurniture.transfer.Utils.CreateTransferDocument;

public class CreateTransferDocumentAction extends BaseActionHandler {

  private static final Logger log = Logger.getLogger(CreateTransferDocumentAction.class);

  protected JSONObject execute(Map<String, Object> parameters, String data) {
    try {

      final JSONObject JSONreg = new JSONObject(data);
      final JSONObject JSONResult = new JSONObject();
      final JSONObject JSONRegResult = new JSONObject();

      JSONRegResult.put("status", "nook");
      JSONRegResult.put("msg", "");

      String IDs = JSONreg.getString("LinesID");

      List<TableTransf> ListTableTransf = getTransferLines(IDs);

      if ((ListTableTransf != null) && (ListTableTransf.size() > 0)) {

        Movement regMovement = CreateTransferDocument.create(ListTableTransf);

        if (regMovement != null) {

          try {

            JSONRegResult.put("status", "ok");
            JSONRegResult.put("msg", "Transfer document: " + regMovement.getName()
                + " has been created");

            OBDal.getInstance().save(regMovement);

            for (TableTransf regTableTransf : ListTableTransf) {
              OBDal.getInstance().remove(regTableTransf);
            }

            OBDal.getInstance().flush();
            OBDal.getInstance().commitAndClose();

          } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            JSONRegResult.put("status", "nook");
            JSONRegResult.put("msg", e.getMessage());

          }

        }

      }
      JSONResult.put("result", JSONRegResult);
      return JSONResult;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      throw new OBException(e);
    }
  }

  List<TableTransf> getTransferLines(String LinesId) {

    try {

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS transf");
      queryExtend.append(" WHERE ");
      queryExtend.append(" transf.id IN (" + LinesId + ")");

      // final OBCriteria<TableTransf> ListTableTransf = OBDal.getInstance().createCriteria(
      // TableTransf.class, queryExtend.toString());

      OBQuery<TableTransf> obq = OBDal.getInstance().createQuery(TableTransf.class,
          queryExtend.toString());

      if ((obq != null) && (obq.list().size() > 0)) {
        return obq.list();
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e);
    }
    return null;

  }

}