package com.spocsys.vigfurniture.transfer.erpCommon.ad_actionButton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jfree.util.Log;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.service.json.JsonConstants;

import com.spocsys.vigfurniture.transfer.Movement;

public class PrintTransferDocumentAction extends BaseActionHandler {
  private String ID = JsonConstants.ID;
  private String IDENTIFIER = JsonConstants.IDENTIFIER;
  private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
  private SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
  private static final Logger log = Logger.getLogger(PrintTransferDocumentAction.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {
    JSONObject response = new JSONObject();
    JSONObject request = null;

    try {

      OBContext.setAdminMode();
      try {
        OBDal dal = OBDal.getInstance();
        request = new JSONObject(content);
        String action = request.getString("action");

        if (action.equals("printPDF")) {
          printPDF(dal, request, response);
        }
        response.put("result", "Success");
      } catch (Exception e) {
        System.out.println("ERROR: " + e.getMessage());
        try {
          response.put("result", "Error");
          response.put("message", e.getMessage());
        } catch (JSONException e1) {
        }
      } finally {
        OBContext.restorePreviousMode();
      }
    } catch (Exception e) {
      e.printStackTrace();
      Log.error(e);
    }
    return response;
  }

  String getPathAbsolute() {
    String webContentDir = OBPropertiesProvider.getInstance().getOpenbravoProperties()
        .getProperty("source.path");
    if (!webContentDir.endsWith("/"))
      webContentDir += "/";
    // webContentDir += "WebContent/src-loc/design/";
    webContentDir += "WebContent/web/";
    return webContentDir;

  }

  String getPathTemplateJrxml() {

    String templateDir = "com.spocsys.vigfurniture.transfer/resource/transfer/GoodMovement.jrxml";
    String jrxml = getPathAbsolute() + templateDir;

    return jrxml;
  }

  String getPathTemplateSubReportJrxml() {

    String templateDir = "com.spocsys.vigfurniture.transfer/resource/transfer/GoodMovementLine.jrxml";
    String jrxml = getPathAbsolute() + templateDir;

    return jrxml;
  }

  private void printPDF(OBDal dal, JSONObject request, JSONObject response) {

    try {
      JSONObject record = request.getJSONObject("record");
      Movement regTransfer = dal.get(Movement.class, record.getString("id"));
      String url = createPDF(regTransfer);
      response.put("url", url);
    } catch (Exception e) {
      System.out.println("reprintPDF Error: " + e.getMessage());
      e.printStackTrace();
      throw new OBException(e.getMessage());
    }

  }

  public Connection getConnection() {
    Connection con = null;
    try {

      String DB_DRIVER_CLASS = OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty("bbdd.driver");
      String DB_URL = OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty("bbdd.url")
          + "/"
          + OBPropertiesProvider.getInstance().getOpenbravoProperties().getProperty("bbdd.sid");
      String DB_USERNAME = OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty("bbdd.user");
      String DB_PASSWORD = OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty("bbdd.password");

      Class.forName(DB_DRIVER_CLASS);

      con = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);

    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return con;
  }

  public String createPDF(Movement regTransfer) throws Exception {
    String filename = "Transfer_" + regTransfer.getName() + "-.pdf";

    String jrxml = getPathTemplateJrxml();
    // String jrxmlSubreport = getPathTemplateSubReportJrxml();
    // JRBeanCollectionDataSource datasourceFields = null;
    // new JRBeanCollectionDataSource(
    // getDataGiftCard(regTransfer.getId()));

    // --------------------------------------------------------------------------------------------
    Map<String, Object> params = new HashMap<String, Object>();

    params.put("NUMBERFORMAT", NumberFormat.getCurrencyInstance());
    params.put("DOCUMENT_ID", regTransfer.getId());

    // // ---------SUBREPORT JASPER---------
    // JasperDesign jdSubReportLines = JRXmlLoader.load(jrxmlSubreport);
    // JasperReport jSubReportLines = JasperCompileManager.compileReport(jdSubReportLines);
    //
    // JasperPrint jprintSubreport = JasperFillManager.fillReport(jSubReportLines, params,
    // datasourceFields);

    JasperDesign jd = JRXmlLoader.load(jrxml);
    JasperReport jrc = JasperCompileManager.compileReport(jd);

    // JasperPrint jprint = JasperFillManager.fillReport(jrc, params, datasourceFields);
    JasperPrint jprint = JasperFillManager.fillReport(jrc, params, getConnection());

    List<Object> jpList = new ArrayList<Object>();
    jpList.add(jprint);
    JRPdfExporter jpdf = new JRPdfExporter();

    String attachDir = OBPropertiesProvider.getInstance().getOpenbravoProperties()
        .getProperty("attach.path");

    jpdf.setExporterInput(new SimpleExporterInput(jprint));
    jpdf.setExporterOutput(new SimpleOutputStreamExporterOutput(attachDir + "/" + filename));
    SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
    jpdf.setConfiguration(configuration);
    jpdf.exportReport();

    return "utility/DownloadReport.html?report=" + filename;
  }
}
