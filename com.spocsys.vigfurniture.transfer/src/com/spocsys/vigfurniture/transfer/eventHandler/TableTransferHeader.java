package com.spocsys.vigfurniture.transfer.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;

import com.spocsys.vigfurniture.transfer.TableTransf;

public class TableTransferHeader extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      TableTransf.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final TableTransf regTransferHeader = (TableTransf) event.getTargetInstance();

    if (regTransferHeader != null) {
      Validate(regTransferHeader);
    }

    if (isCustomerReference(regTransferHeader) == true) {

      Entity TransferEntity = ModelProvider.getInstance().getEntity(TableTransf.ENTITY_NAME);
      Property CustomerRefProperty = TransferEntity
          .getProperty(TableTransf.PROPERTY_CUSTOMERREFERENCENUMBER);

      event.setCurrentState(CustomerRefProperty, regTransferHeader.getSalesOrderLine()
          .getSvfsvPoreference());

    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final TableTransf regTransferHeader = (TableTransf) event.getTargetInstance();

    if (regTransferHeader != null) {
      Validate(regTransferHeader);
    }

    if (isCustomerReference(regTransferHeader) == true) {

      Entity TransferEntity = ModelProvider.getInstance().getEntity(TableTransf.ENTITY_NAME);
      Property CustomerRefProperty = TransferEntity
          .getProperty(TableTransf.PROPERTY_CUSTOMERREFERENCENUMBER);

      event.setCurrentState(CustomerRefProperty, regTransferHeader.getSalesOrderLine()
          .getSvfsvPoreference());

    }

  }

  void Validate(TableTransf regTransferHeader) {

    if (regTransferHeader.getMovementQuantity().doubleValue() <= 0) {
      throw new OBException("Movement Quantity is not a correct value");
    }

    if ((regTransferHeader.getInitialWarehouse() != null)
        && (regTransferHeader.getFinalWarehouse() != null)) {

      if (regTransferHeader.getInitialWarehouse().getId()
          .equalsIgnoreCase(regTransferHeader.getFinalWarehouse().getId()) == true) {

        throw new OBException(
            "Initial warehouse and Final warehouse can not have the same warehouse");
      }

    }

  }

  boolean isCustomerReference(TableTransf regTransferHeader) {

    if (regTransferHeader.getSalesOrderLine() != null) {
      if ((regTransferHeader.getCustomerReferenceNumber() == null)
          || (regTransferHeader.getCustomerReferenceNumber().trim().equals("") == true)) {
        return true;
      }
    }
    return false;

  }
}
