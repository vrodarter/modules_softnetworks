package com.spocsys.vigfurniture.transfer.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;

import com.spocsys.vigfurniture.transfer.Movement;

public class TransferHeader extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Movement.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Movement regTransferHeader = (Movement) event.getTargetInstance();

    if (regTransferHeader != null) {
      Validate(regTransferHeader);
    }

    if (isCustomerReference(regTransferHeader) == true) {

      Entity TransferEntity = ModelProvider.getInstance().getEntity(Movement.ENTITY_NAME);
      Property CustomerRefProperty = TransferEntity.getProperty(Movement.PROPERTY_CUSTOMERREFNO);

      event.setCurrentState(CustomerRefProperty, regTransferHeader.getOrder().getOrderReference());

    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Movement regTransferHeader = (Movement) event.getTargetInstance();

    if (regTransferHeader != null) {
      Validate(regTransferHeader);
    }

    if (isCustomerReference(regTransferHeader) == true) {

      Entity TransferEntity = ModelProvider.getInstance().getEntity(Movement.ENTITY_NAME);
      Property CustomerRefProperty = TransferEntity.getProperty(Movement.PROPERTY_CUSTOMERREFNO);

      event.setCurrentState(CustomerRefProperty, regTransferHeader.getOrder().getOrderReference());

    }

  }

  void Validate(Movement regTransferHeader) {

    if ((regTransferHeader.getInitialWarehouse() != null)
        && (regTransferHeader.getTransitWarehouse() != null)) {

      if (regTransferHeader.getInitialWarehouse().getId()
          .equalsIgnoreCase(regTransferHeader.getTransitWarehouse().getId()) == true) {

        throw new OBException(
            "Initial warehouse and Transit warehouse can not have the same warehouse");
      }

    }

    if ((regTransferHeader.getTransitWarehouse() != null)
        && (regTransferHeader.getFinalWarehouse() != null)) {

      if (regTransferHeader.getTransitWarehouse().getId()
          .equalsIgnoreCase(regTransferHeader.getFinalWarehouse().getId()) == true) {

        throw new OBException(
            "Transit warehouse and Final warehouse can not have the same warehouse");
      }

    }

    if ((regTransferHeader.getInitialWarehouse() != null)
        && (regTransferHeader.getFinalWarehouse() != null)) {

      if (regTransferHeader.getInitialWarehouse().getId()
          .equalsIgnoreCase(regTransferHeader.getFinalWarehouse().getId()) == true) {

        throw new OBException(
            "Initial warehouse and Final warehouse can not have the same warehouse");
      }

    }

  }

  boolean isCustomerReference(Movement regTransferHeader) {

    if (regTransferHeader.getOrder() != null) {
      if ((regTransferHeader.getCustomerRefNo() == null)
          || (regTransferHeader.getCustomerRefNo().trim().equals("") == true)) {
        return true;
      }
    }
    return false;

  }
}
