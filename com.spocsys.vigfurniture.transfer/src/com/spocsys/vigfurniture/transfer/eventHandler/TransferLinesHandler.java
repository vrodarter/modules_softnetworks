package com.spocsys.vigfurniture.transfer.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;

import com.spocsys.vigfurniture.transfer.Movementline;

public class TransferLinesHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      Movementline.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Movementline regTransferLine = (Movementline) event.getTargetInstance();

    if (regTransferLine != null) {

      if ((regTransferLine.getMovement().getState() != null)
          && ((regTransferLine.getMovement().getState().equalsIgnoreCase("receive_transfer") == true) || (regTransferLine
              .getMovement().getState().equalsIgnoreCase("completed_transfer") == true))) {
        throw new OBException("This document can't be changed, because the document was processed");
      }

      if (isCustomerReference(regTransferLine) == true) {

        Entity TransferLineEntity = ModelProvider.getInstance().getEntity(Movementline.ENTITY_NAME);
        Property CustomerRefProperty = TransferLineEntity
            .getProperty(Movementline.PROPERTY_CUSTOMERREFERENCENUMBER);

        event.setCurrentState(CustomerRefProperty, regTransferLine.getOrderline()
            .getSvfsvPoreference());

      }

    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final Movementline regTransferLine = (Movementline) event.getTargetInstance();

    if (regTransferLine != null) {

      if (isCustomerReference(regTransferLine) == true) {

        Entity TransferLineEntity = ModelProvider.getInstance().getEntity(Movementline.ENTITY_NAME);
        Property CustomerRefProperty = TransferLineEntity
            .getProperty(Movementline.PROPERTY_CUSTOMERREFERENCENUMBER);

        event.setCurrentState(CustomerRefProperty, regTransferLine.getOrderline()
            .getSvfsvPoreference());

      }
    }

  }

  boolean isCustomerReference(Movementline regMovementline) {

    if (regMovementline.getOrderline() != null) {
      if ((regMovementline.getCustomerReferenceNumber() == null)
          || (regMovementline.getCustomerReferenceNumber().trim().equals("") == true)) {
        return true;
      }
    }
    return false;

  }

}