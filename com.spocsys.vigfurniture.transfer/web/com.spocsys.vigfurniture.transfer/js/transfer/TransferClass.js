isc.OBPopup.create
({
    ID: "SVFTRAF_WindowPopupProcess",
    title: "Creating Transfer Document",
    autoSize:true,
    autoCenter: true,
    showMinimizeButton: false,
    showMaximizeButton: false,
    isModal: true,
    showModalMask: true,
    autoDraw: false,
    height: "30%",
    width:"50%",
    closeClick: function () {this.hide();},
    items:
    [
		
		isc.VStack.create
		({
			ID: "SVFTRAF_WindowPopupProcessCapaData",
			width:"100%",
			//visible: false,
			autoDraw: false,
			members:
		    [
		     
		    ]
		})
    ],
    begin: function()
    {
      try
      {
    	  var Height = (this.getHeight() - 28)+"px";
    	  OB.SVFTRAF.WindoLoadingImg = OB.SVFTRAF.WindoLoadingImg.replace(/000HEIGHT/g, Height);
    	  var SVFTRAF_WindoLoadingImgVar = isc.HTMLFlow.create
    	  ({
    	        ID: "SVFTRAFWindoLoadingImg",
    	        contents: OB.SVFTRAF.WindoLoadingImg,
    	        width:"100%",
    	        height:"100%"
    	  });
    	  SVFTRAF_WindowPopupProcessCapaData.removeMembers(SVFTRAF_WindowPopupProcessCapaData.getMembers());
    	  SVFTRAF_WindowPopupProcessCapaData.addMember(SVFTRAF_WindoLoadingImgVar);
       }
       catch(e)
       {
    	  this.hide();
    	  isc.warn("There was an error while it was executing the action");
    	  console.log(e); 
       }
    }
});


OB.SVFTRAF = OB.SVFTRAF || {};

OB.SVFTRAF.Process =
{ 
  VarLines:null,
  View: null,
  ViewLines: null,
  grid: null,
  callback: function (rpcResponse, data, rpcRequest)
  {
	 try
	 {console.log(data);
		 if((data!= null) && (data.result))
		 {
		   if(data.result.status=='nook')
		   {
			 isc.warn('There was an error while it was executing the action');
			 console.log(data.result.msg);
		   }
		   else
		   {
			 OB.SVFTRAF.Process.grid.deselectAllRecords();  
			 OB.SVFTRAF.Process.View.refresh(); 
			 OB.SVFTRAF.Process.View.messageBar.setMessage('success', null, data.result.msg);
		   }
		 }
	 }
	 catch(e)
	 {
	   console.log(e.message);	 
	 }
	 SVFTRAF_WindowPopupProcess.hide();
	 
  },
  isVerifyLines: function()
  {
	  var RegistroView = OB.SVFTRAF.Process.ViewLines;
	  var I= 0;
	  var Fin= RegistroView.getLength(); 
	  var LinesID = '';
	  var Error='';
	  
	  for(var I=0; I< Fin; I++)
	  {
		  for(var J=0; J< Fin; J++)
		  {
			if(RegistroView[I].organization != RegistroView[J].organization)
			{
				Error='The lines must have the same organization';
				break;
			}
			else
			{
				if(RegistroView[I].initialWarehouse != RegistroView[J].initialWarehouse)
				{
				  Error='The lines must have the same Initial warehouse';
				  break;
				}
				else
				{
					if(RegistroView[I].finalWarehouse != RegistroView[J].finalWarehouse)
					{
					  Error='The lines must have the same Final warehouse';
					  break;
					}
				}	
			 }	
		   }
		   if(Error != '')
		   {
			 break;  
		   }
		   if(I>0)
   		   {
			  LinesID+= ",";	
		   }
		   LinesID+= "'" + RegistroView[I].id + "'";
		   
	  }
	  console.log(Error);
	  if(Error!= '')
	  {
		SVFTRAF_WindowPopupProcess.hide();
		isc.warn(Error);
		return false;  
	  }
	  OB.SVFTRAF.Process.VarLines = LinesID;
	  return true;
  },
  
  beginProcess: function ()
  {
	  SVFTRAF_WindowPopupProcess.show();
	  SVFTRAF_WindowPopupProcess.begin();
	  
	  if(OB.SVFTRAF.Process.isVerifyLines()== true)
	  {	  
	      //-------------------------------------------
		  var Lines= OB.SVFTRAF.Process.VarLines;
		  //-------------------------------------------
		  OB.RemoteCallManager.call('com.spocsys.vigfurniture.transfer.erpCommon.ad_actionButton.CreateTransferDocumentAction', 
		  {
		     "LinesID": Lines
		  }, {}, OB.SVFTRAF.Process.callback);
	  }
  },
  execute: function (params, view)
  {
	try
	{
	  OB.SVFTRAF.Process.grid= params.button.contextView.viewGrid;
	  var RegistroView = OB.SVFTRAF.Process.grid.getSelectedRecords();
	  OB.SVFTRAF.Process.ViewLines = RegistroView;
	 
	  // ask for confirmation
      isc.ask('Do you want to do this action?', function(ok) {
          if (ok) {
        	  OB.SVFTRAF.Process.beginProcess();
          }
      });
	}
	catch(e)
	{
	  console.log(e.message);	
	}
	  
  },
  CreateTransfer: function (params, view)
  {
	try
	{
	  OB.SVFTRAF.Process.View= params.button.contextView;	
	  params.action = 'CreateTransfer';
      OB.SVFTRAF.Process.execute(params, view);
	}
	catch(e)
	{
	  console.log(e.message);	
	}
  }
};
