


(function() {
	
	var loadingMessage = isc.Dialog.create({
		title : "Loading...",
		message : "&nbsp;&nbsp;&nbsp;&nbsp; ${loadingImage} "
				+ "&nbsp;&nbsp; Please Wait...",
		messageStyle : "center",
		showCloseButton : false
	});
	
	var buttonProps = {
		

		printPDF : function(id) {
			var me = this;
			var params = {};
			params.action = "printPDF";
			params.record = {
				"id" : id
			};
			
			loadingMessage.show();
			var callback = function(rpcResponse, data, rpcRequest) {
				loadingMessage.hide();
				if (data.result === 'Success') {
					window.location.href = data.url;
				} else {
					isc.warn("Error while printing PDF");
					console.log(data.message);
				}
			};
			OB.RemoteCallManager
					.call(
							'com.spocsys.vigfurniture.transfer.erpCommon.ad_actionButton.PrintTransferDocumentAction',
							params, {}, callback);
		},
		action : function() {
			var selectedRecords = this.view.viewGrid.getSelectedRecords();
			console.log(selectedRecords);
			this.printPDF(selectedRecords[0].id);
		},
		buttonType : 'print',
		prompt : 'Print'/* OB.I18N.getLabel('OBEXAPP_SumData') */,
		updateState : function() {
			var view = this.view, form = view.viewForm, grid = view.viewGrid, selectedRecords = grid
					.getSelectedRecords();
			if (view.isShowingForm && form.isNew) {
				this.setDisabled(true);
			} else if (view.isEditingGrid && grid.getEditForm().isNew) {
				this.setDisabled(true);
			} else {
				this.setDisabled(selectedRecords.length === 0);
			}
		}
	};

	// register the button for the sales order tab
	// the first parameter is a unique identification so that one button can not
	// be registered multiple times.
	OB.ToolbarRegistry.registerButton(buttonProps.buttonType,
			isc.OBToolbarIconButton, buttonProps, 100,
			'38469B337A334360837AAD34D5AF0AA8');
}());