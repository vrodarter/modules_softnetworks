package com.spocsys.vigfurniture.webservices.Product;

import java.io.Writer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.service.web.WebService;

public class Product implements WebService {

  private static final Logger log = Logger.getLogger(Product.class);
  JSONObject JSONReturn = null;
  JSONArray JSONReturnArray = null;
  String Result = null;

  public void ProcessWebService(String path, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    try {

      JSONReturn = new JSONObject();
      JSONReturnArray = new JSONArray();

      // String OrgSearchKey = (String) request.getParameter("org-key");
      // String ProductSearchKey = (String) request.getParameter("product-key");
      //
      // if ((ProductSearchKey == null) || (ProductSearchKey.trim().equalsIgnoreCase("") == true)) {
      // JSONObject JSONreg = new JSONObject();
      // JSONreg.put("msg", "Organization SearchKey is not correct");
      // JSONReturnArray.put(JSONreg);
      // }
      //
      // if ((ProductSearchKey == null) || (ProductSearchKey.trim().equalsIgnoreCase("") == true)) {
      // JSONObject JSONreg = new JSONObject();
      // JSONreg.put("msg", "Product SearchKey is not correct");
      // JSONReturnArray.put(JSONreg);
      // }

      if ((JSONReturnArray == null) || (JSONReturnArray.length() <= 0)) {
        ProductService ObjService = new ProductService(request);
        Result = ObjService.Execute();
      } else {
        JSONReturn.put("error", JSONReturnArray);
        Result = JSONReturn.toString();
      }

    } catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      log.error(e.getMessage());
      Result = e.getMessage();
    }

    response.setContentType("application/json");
    response.setCharacterEncoding("utf-8");

    final Writer w = response.getWriter();
    w.write(Result);
    w.close();

  }

  @Override
  public void doGet(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    // TODO Auto-generated method stub
    ProcessWebService(path, request, response);
  }

  @Override
  public void doPost(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    // TODO Auto-generated method stub
    ProcessWebService(path, request, response);
  }

  @Override
  public void doDelete(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    // TODO Auto-generated method stub

  }

  @Override
  public void doPut(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    // TODO Auto-generated method stub

  }

}
