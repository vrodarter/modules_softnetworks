package com.spocsys.vigfurniture.webservices.Product;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.Warehouse;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.model.pricing.pricelist.ProductPrice;

import com.spocsys.vigfurniture.advanceproductmanagement.SubstituteProduct;
import com.spocsys.vigfurniture.incomingproduct.svfpincProductIncomingV;
import com.spocsys.vigfurniture.productavailability.ProductByWareHouse;

public class ProductService {

  private static final Logger log = Logger.getLogger(ProductService.class);

  String JSONOriginal = "";
  JSONObject JSONRequest = null, JSONHeader = null;
  JSONArray JSONBody = null;
  HashMap<String, Integer> TypeAvailable = new HashMap<String, Integer>();
  // ---------------------------
  private String OrgSearchKey = null;
  private JSONArray WarehousesSearchKey = null;
  private String CategorySearchKey = null;
  private JSONArray ProductsSearchKey = null;
  private String WarehouseSearchKey = null;
  private Organization regOrg = null;
  private ProductCategory regCategory = null;
  private Product regProduct = null;
  private Warehouse regWarehouse = null;
  private String WarehousesIDs = null;

  public String getOrgSearchKey() {
    return OrgSearchKey;
  }

  public void setOrgSearchKey(String orgSearchKey) {
    OrgSearchKey = orgSearchKey;
  }

  public Organization getRegOrg() {
    return regOrg;
  }

  public void setRegOrg(Organization regOrg) {
    this.regOrg = regOrg;
  }

  public String getWarehousesSearchKey() {
    String ListKeys = null;
    int J = 0;
    try {
      if ((WarehousesSearchKey != null) && (WarehousesSearchKey.length() > 0)) {
        ListKeys = "";

        for (int I = 0; I < WarehousesSearchKey.length(); I++) {
          try {
            Warehouse regWarehouseP = getDalWarehouse(WarehousesSearchKey.getString(I));

            if (regWarehouseP != null) {

              if (I > 0) {
                ListKeys += ",";
              }

              ListKeys += "'" + regWarehouseP.getId() + "'";
              J++;
            }
          } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.error(e.getMessage());
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    if (J <= 0) {
      ListKeys = null;
    }
    return ListKeys;
  }

  public void setWarehousesSearchKey(JSONArray warehousesSearchKey) {
    WarehousesSearchKey = warehousesSearchKey;
  }

  public String getCategorySearchKey() {
    return CategorySearchKey;
  }

  public void setCategorySearchKey(String categorySearchKey) {
    CategorySearchKey = categorySearchKey;
  }

  public ProductCategory getRegCategory() {
    return regCategory;
  }

  public void setRegCategory(ProductCategory regCategory) {
    this.regCategory = regCategory;
  }

  public String getProductsSearchKey() {
    String ListKeys = null;
    int J = 0;
    try {
      if ((ProductsSearchKey != null) && (ProductsSearchKey.length() > 0)) {
        ListKeys = "";

        for (int I = 0; I < ProductsSearchKey.length(); I++) {
          try {
            Product regProductP = getDalProduct(ProductsSearchKey.getString(I));

            if (regProductP != null) {

              if (I > 0) {
                ListKeys += ",";
              }

              ListKeys += "'" + regProductP.getId() + "'";
              J++;
            }
          } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.error(e.getMessage());
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    if (J <= 0) {
      ListKeys = null;
    }
    return ListKeys;
  }

  public void setProductsSearchKey(JSONArray productSearchKey) {
    ProductsSearchKey = productSearchKey;
  }

  public String getWarehouseSearchKey() {
    return WarehouseSearchKey;
  }

  public void setWarehouseSearchKey(String warehouseSearchKey) {
    WarehouseSearchKey = warehouseSearchKey;
  }

  public Product getRegProduct() {
    return regProduct;
  }

  public void setRegProduct(Product regProduct) {
    this.regProduct = regProduct;
  }

  public Warehouse getRegWarehouse() {
    return regWarehouse;
  }

  public void setRegWarehouse(Warehouse regWarehouse) {
    this.regWarehouse = regWarehouse;
  }

  public Organization getDalOrg(String key) {

    try {

      try {
        OBContext.setAdminMode();

        OBCriteria<Organization> ListOrganization = OBDal.getInstance().createCriteria(
            Organization.class);
        ListOrganization.add(Restrictions.eq(Organization.PROPERTY_SEARCHKEY, key));

        return ((Organization) ListOrganization.uniqueResult());
      } finally {
        OBContext.restorePreviousMode();
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  public ProductCategory getDalCategory(String key) {

    try {

      OBCriteria<ProductCategory> ListProductCategory = OBDal.getInstance().createCriteria(
          ProductCategory.class);
      ListProductCategory.add(Restrictions.eq(ProductCategory.PROPERTY_SEARCHKEY, key));

      return ((ProductCategory) ListProductCategory.uniqueResult());

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  public Product getDalProduct(String key) {

    try {

      OBCriteria<Product> ListProduct = OBDal.getInstance().createCriteria(Product.class);
      ListProduct.add(Restrictions.eq(Product.PROPERTY_SEARCHKEY, key));

      return ((Product) ListProduct.uniqueResult());

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  public Warehouse getDalWarehouse(String key) {

    try {

      OBCriteria<Warehouse> ListWarehouse = OBDal.getInstance().createCriteria(Warehouse.class);
      ListWarehouse.add(Restrictions.eq(Warehouse.PROPERTY_SEARCHKEY, key));

      return ((Warehouse) ListWarehouse.uniqueResult());

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  public ProductService(HttpServletRequest request) {
    // TODO Auto-generated constructor stub
    try {

      String OrgSearchKeyP = (String) request.getParameter("org_key");
      String WarehousesSearchKeyP = (String) request.getParameter("warehouses_key");
      String CategorySearchKeyP = (String) request.getParameter("category_key");
      String ProductSearchKeyP = (String) request.getParameter("products_key");

      if ((OrgSearchKeyP != null) && (OrgSearchKeyP.trim().equals("") != true)) {
        setOrgSearchKey(OrgSearchKeyP.trim());
        setRegOrg(getDalOrg(getOrgSearchKey()));
      }

      if ((WarehousesSearchKeyP != null) && (WarehousesSearchKeyP.trim().equals("") != true)) {

        try {

          JSONArray JSONWarehousesArray = new JSONArray(WarehousesSearchKeyP.trim());
          setWarehousesSearchKey(JSONWarehousesArray);
          WarehousesIDs = getWarehousesSearchKey();
        } catch (JSONException e) {
          e.printStackTrace();
          log.error(e.getMessage());
        }

      }

      if ((CategorySearchKeyP != null) && (CategorySearchKeyP.trim().equals("") != true)) {
        setCategorySearchKey(CategorySearchKeyP.trim());
        setRegCategory(getDalCategory(CategorySearchKeyP.trim()));
      }

      if ((ProductSearchKeyP != null) && (ProductSearchKeyP.trim().equals("") != true)) {

        try {

          JSONArray JSONProductsArray = new JSONArray(ProductSearchKeyP.trim());
          setProductsSearchKey(JSONProductsArray);

        } catch (JSONException e) {
          e.printStackTrace();
          log.error(e.getMessage());
        }

      }

      // -----------------------------------------------

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
  }

  public String Execute() {

    JSONObject JSONReturn = new JSONObject();
    JSONArray JSONProducts = getJSONProduct();

    try {
      JSONReturn.put("products", JSONProducts);
      return JSONReturn.toString();
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return "{\"products\": []}";

  }

  private JSONArray getQtyonPOandETA(Product regProductNew) {

    JSONArray JSONArrayQtyonPOandETA = new JSONArray();

    try {

      // OBCriteria<svfpincProductIncomingV> ListIncomingProd = OBDal.getInstance().createCriteria(
      // svfpincProductIncomingV.class);
      // ListIncomingProd
      // .add(Restrictions.eq(svfpincProductIncomingV.PROPERTY_PRODUCT, regProductNew));

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS prod");
      queryExtend.append(" WHERE (1=1)");
      queryExtend.append(" AND (prod.product.id = '" + regProductNew.getId() + "')");

      if ((WarehousesIDs != null) && (WarehousesIDs.trim().equals("") != true)) {
        queryExtend.append(" AND (prod.warehouse.id IN (" + WarehousesIDs + "))");
      }

      OBQuery<svfpincProductIncomingV> ListIncomingProd = OBDal.getInstance().createQuery(
          svfpincProductIncomingV.class, queryExtend.toString());

      if ((ListIncomingProd != null) && (ListIncomingProd.count() > 0)) {

        for (svfpincProductIncomingV regInprod : ListIncomingProd.list()) {

          if (regInprod.getQtypo().doubleValue() > 0) {

            JSONObject JSONQtyonPOandETA = new JSONObject();
            JSONQtyonPOandETA.put("purchaseOrderDocument", regInprod.getPoid().getDocumentNo());
            JSONQtyonPOandETA.put("ETA", regInprod.getETA() != null ? regInprod.getETA() : "");
            JSONQtyonPOandETA.put(svfpincProductIncomingV.PROPERTY_WAREHOUSE, regInprod
                .getWarehouse().getIdentifier());
            JSONQtyonPOandETA.put("qtyavailable", regInprod.getQtypo());
            JSONArrayQtyonPOandETA.put(JSONQtyonPOandETA);

          }

        }
        return JSONArrayQtyonPOandETA;

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  private JSONArray getNetAvailableQty(Product regProductNew) {

    JSONArray JSONArrayNetAvailableQty = new JSONArray();

    try {

      // OBCriteria<ProductByWareHouse> ListProductByWareHouse = OBDal.getInstance().createCriteria(
      // ProductByWareHouse.class);
      // ListProductByWareHouse.add(Restrictions
      // .eq(ProductByWareHouse.PROPERTY_PRODUCT, regProductNew));

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS prod");
      queryExtend.append(" WHERE (1=1)");
      queryExtend.append(" AND (prod.product.id = '" + regProductNew.getId() + "')");

      if ((WarehousesIDs != null) && (WarehousesIDs.trim().equals("") != true)) {
        queryExtend.append(" AND (prod.warehouse.id IN (" + WarehousesIDs + "))");
      }

      OBQuery<ProductByWareHouse> ListProductByWareHouse = OBDal.getInstance().createQuery(
          ProductByWareHouse.class, queryExtend.toString());

      if ((ListProductByWareHouse != null) && (ListProductByWareHouse.count() > 0)) {

        for (ProductByWareHouse regProductByWareHouse : ListProductByWareHouse.list()) {

          JSONObject JSONNetAvailableQty = new JSONObject();
          JSONNetAvailableQty.put(ProductByWareHouse.PROPERTY_WAREHOUSE, regProductByWareHouse
              .getWarehouse().getIdentifier());
          JSONNetAvailableQty.put(
              ProductByWareHouse.PROPERTY_WEBAVAILABILITY,
              regProductByWareHouse.getWebAvailability() == null ? "0" : regProductByWareHouse
                  .getWebAvailability());
          JSONArrayNetAvailableQty.put(JSONNetAvailableQty);

        }
        return JSONArrayNetAvailableQty;

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  private JSONArray getPriceProduct(Product regProductNew) {

    try {

      JSONArray JSONArrayPrice = new JSONArray();

      String PropertyVersionDate = ProductPrice.PROPERTY_PRICELISTVERSION + "."
          + PriceListVersion.PROPERTY_VALIDFROMDATE;
      String PropertyListSales = ProductPrice.PROPERTY_PRICELISTVERSION + "."
          + PriceListVersion.PROPERTY_PRICELIST + "." + PriceList.PROPERTY_SALESPRICELIST;

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS pp");
      queryExtend.append(" WHERE ");
      queryExtend.append(" pp." + ProductPrice.PROPERTY_PRODUCT + "." + "id='"
          + regProductNew.getId() + "'");
      queryExtend.append(" ORDER BY pp." + PropertyListSales + " DESC,");
      queryExtend.append(" pp." + PropertyVersionDate + " DESC");

      OBQuery<ProductPrice> obq = OBDal.getInstance().createQuery(ProductPrice.class,
          queryExtend.toString());

      // OBCriteria<ProductPrice> ListProductPrice = OBDal.getInstance().createCriteria(
      // ProductPrice.class);
      // ListProductPrice.add(Restrictions.eq(ProductPrice.PROPERTY_PRODUCT, regProductNew));
      // ListProductPrice.addOrder(Order.desc(PropertyListSales));
      // ListProductPrice.addOrder(Order.desc(PropertyVersionDate));

      if ((obq != null) && (obq.list() != null)) {

        for (ProductPrice regProductPrice : obq.list()) {

          JSONObject JSONProductPrice = new JSONObject();
          JSONProductPrice.put("list-pricename", regProductPrice.getPriceListVersion()
              .getPriceList().getName());
          JSONProductPrice.put(PriceList.PROPERTY_SALESPRICELIST, regProductPrice
              .getPriceListVersion().getPriceList().isSalesPriceList());
          JSONProductPrice.put("version-name", regProductPrice.getPriceListVersion().getName());
          JSONProductPrice.put(PriceListVersion.PROPERTY_VALIDFROMDATE, regProductPrice
              .getPriceListVersion().getValidFromDate());
          JSONProductPrice.put(ProductPrice.PROPERTY_STANDARDPRICE,
              regProductPrice.getStandardPrice());
          JSONProductPrice.put(ProductPrice.PROPERTY_LISTPRICE, regProductPrice.getListPrice());
          JSONProductPrice.put(ProductPrice.PROPERTY_PRICELIMIT, regProductPrice.getPriceLimit());
          JSONArrayPrice.put(JSONProductPrice);
        }

      }
      return JSONArrayPrice;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return null;
  }

  private JSONArray getJSONProductSubstitutes(Product regProductNew) {

    JSONArray JSONArraySubstitutes = new JSONArray();
    List<SubstituteProduct> ListSubstituteProduct = regProductNew.getSVFADPMSubstituteList();

    if (ListSubstituteProduct != null) {

      for (SubstituteProduct regSubstitute : ListSubstituteProduct) {
        JSONObject JSONSubstituteProduct = new JSONObject();

        try {
          JSONSubstituteProduct.put(Product.PROPERTY_SEARCHKEY, regSubstitute
              .getSubstituteProduct().getSearchKey());
          JSONSubstituteProduct.put(Product.PROPERTY_NAME, regSubstitute.getSubstituteProduct()
              .getName());
          JSONSubstituteProduct.put(Product.PROPERTY_DESCRIPTION, regSubstitute
              .getSubstituteProduct().getDescription() != null ? regSubstitute
              .getSubstituteProduct().getDescription() : "");
          JSONSubstituteProduct.put(Product.PROPERTY_DISCONTINUED, regSubstitute
              .getSubstituteProduct().isDiscontinued() != null ? regSubstitute
              .getSubstituteProduct().isDiscontinued() : false);
          JSONSubstituteProduct.put(Product.PROPERTY_ACTIVE, regSubstitute.getSubstituteProduct()
              .isActive());
          try {

            OBContext.setAdminMode();

            JSONArray JSONNetAvailableQty = getNetAvailableQty(regProductNew);
            JSONArray JSONQtyonPOandETA = getQtyonPOandETA(regProductNew);

            JSONSubstituteProduct.put("net-available-qty",
                ((JSONNetAvailableQty == null) || (JSONNetAvailableQty.length() <= 0)) ? "[]"
                    : JSONNetAvailableQty);
            JSONSubstituteProduct.put("qty-on-po-and-eta", (JSONQtyonPOandETA == null)
                || (JSONQtyonPOandETA.length() <= 0) ? "[]" : JSONQtyonPOandETA);

          } finally {
            OBContext.restorePreviousMode();
          }

          JSONArraySubstitutes.put(JSONSubstituteProduct);

        } catch (JSONException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          log.error(e.getMessage());
        }

      }

    }
    return JSONArraySubstitutes;

  }

  private JSONObject getJSONProduct(Product regProductNew) {

    JSONObject JSONProduct = new JSONObject();

    // ---------------------------------------------------------
    try {
      JSONProduct.put(Product.PROPERTY_NAME,
          regProductNew.getName() != null ? regProductNew.getName() : "");
      JSONProduct.put(Product.PROPERTY_DESCRIPTION,
          regProductNew.getDescription() != null ? regProductNew.getDescription() : "");
      JSONProduct.put(Product.PROPERTY_DISCONTINUED, regProductNew.isDiscontinued());
      JSONProduct.put(Product.PROPERTY_ACTIVE, regProductNew.isActive());
      // ---------------------------------------------------------
      try {
        OBContext.setAdminMode();

        JSONArray JSONNetAvailableQty = getNetAvailableQty(regProductNew);
        JSONArray JSONQtyonPOandETA = getQtyonPOandETA(regProductNew);

        JSONProduct.put("net-available-qty", ((JSONNetAvailableQty == null) || (JSONNetAvailableQty
            .length() <= 0)) ? "[]" : JSONNetAvailableQty);
        JSONProduct.put("qty-on-po-and-eta",
            (JSONQtyonPOandETA == null) || (JSONQtyonPOandETA.length() <= 0) ? "[]"
                : JSONQtyonPOandETA);
      } finally {
        OBContext.restorePreviousMode();
      }
      // ---------------------------------------------------------
      JSONProduct.put(Product.PROPERTY_SVFSVHARDKIT,
          regProductNew.isSvfsvHardKit() != null ? regProductNew.isSvfsvHardKit() : false);
      JSONProduct.put(Product.PROPERTY_SVFSVSOFTKIT,
          regProductNew.isSvfsvSoftKit() != null ? regProductNew.isSvfsvSoftKit() : false);
      JSONProduct
          .put(Product.PROPERTY_SVFSVSPECIALORDER,
              regProductNew.isSvfsvSpecialOrder() != null ? regProductNew.isSvfsvSpecialOrder()
                  : false);
      // ----------------------------------------------------------
      JSONProduct.put("prices", getPriceProduct(regProductNew));
      // ----------------------------------------------------------
      JSONProduct.put(Product.PROPERTY_PRODUCTCATEGORY, regProductNew.getProductCategory()
          .getName());
      JSONProduct.put(Product.PROPERTY_SVFSVMAGENTOSKU,
          regProductNew.getSvfsvMagentoSku() != null ? regProductNew.getSvfsvMagentoSku() : "");
      JSONProduct.put(Product.PROPERTY_ATTRIBUTESET,
          regProductNew.getAttributeSet() != null ? regProductNew.getAttributeSet().getName() : "");
      JSONProduct.put(Product.PROPERTY_ATTRIBUTESETVALUE,
          regProductNew.getAttributeSetValue() != null ? regProductNew.getAttributeSetValue()
              .getIdentifier() : "");
      // -----------------------------------------------------------
      JSONProduct.put(SubstituteProduct.PROPERTY_SUBSTITUTEPRODUCT,
          getJSONProductSubstitutes(regProductNew));
      // -----------------------------------------------------------
      JSONProduct.put(Product.PROPERTY_SVFSVITEMSPECIFICATION, regProductNew
          .getSvfsvItemspecification() != null ? regProductNew.getSvfsvItemspecification() : "");
      JSONProduct.put(Product.PROPERTY_SVFSVITEMTYPE,
          regProductNew.getSvfsvItemtype() != null ? regProductNew.getSvfsvItemtype() : "");

      JSONProduct.put(Product.PROPERTY_SVFSVMODEL,
          regProductNew.getSvfsvModel() != null ? regProductNew.getSvfsvModel() : "");
      JSONProduct.put(Product.PROPERTY_SVFSVCOMMENTS,
          regProductNew.getSvfsvComments() != null ? regProductNew.getSvfsvComments() : "");
      // -----------------------------------------------------------

    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      log.error(e.getMessage());
    }

    return JSONProduct;
  }

  private JSONArray getJSONProduct() {

    try {

      JSONArray JSONArrayProduct = new JSONArray();

      final StringBuilder queryExtend = new StringBuilder();
      queryExtend.append(" AS prod");
      queryExtend.append(" WHERE (1=1)");

      if (getRegOrg() != null) {
        queryExtend.append(" AND (prod.organization.id = '" + getRegOrg().getId() + "')");
      }

      if (getRegCategory() != null) {
        queryExtend.append(" AND (prod.productCategory.id = '" + getRegCategory().getId() + "')");
      }

      if (getProductsSearchKey() != null) {
        queryExtend.append(" AND (prod.id IN (" + getProductsSearchKey() + "))");
      }

      OBQuery<Product> ListProduct = OBDal.getInstance().createQuery(Product.class,
          queryExtend.toString());

      if ((ListProduct != null) && (ListProduct.count() > 0)) {

        for (Product regProductNew : ListProduct.list()) {
          JSONArrayProduct.put(getJSONProduct(regProductNew));
        }
      }
      return JSONArrayProduct;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }
}
