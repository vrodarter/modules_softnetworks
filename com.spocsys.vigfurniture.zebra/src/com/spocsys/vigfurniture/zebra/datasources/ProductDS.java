package com.spocsys.vigfurniture.zebra.datasources;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Criteria;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.service.datasource.ReadOnlyDataSourceService;
import org.openbravo.service.json.DataToJsonConverter;
import org.openbravo.service.json.JsonConstants;
import org.openbravo.service.json.JsonUtils;


public class ProductDS  extends ReadOnlyDataSourceService{

	private static final String AD_TABLE_ID = "260";

	int count = 0;
	private String distinct;
	@Override
	protected int getCount(Map<String, String> parameters) {
		// TODO Auto-generated method stub
		return count;
	}

	@Override
	public String fetch(Map<String, String> parameters) {
		int startRow = 0;

		final List<JSONObject> jsonObjects = fetchJSONObject(parameters);

		final JSONObject jsonResult = new JSONObject();
		final JSONObject jsonResponse = new JSONObject();
		try {
			jsonResponse.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
			jsonResponse.put(JsonConstants.RESPONSE_STARTROW, startRow);
			jsonResponse.put(JsonConstants.RESPONSE_ENDROW, jsonObjects.size() - 1);
			jsonResponse.put(JsonConstants.RESPONSE_TOTALROWS, jsonObjects.size());
			jsonResponse.put(JsonConstants.RESPONSE_DATA, new JSONArray(jsonObjects));
			jsonResult.put(JsonConstants.RESPONSE_RESPONSE, jsonResponse);
		} catch (JSONException e) {
		}

		return jsonResult.toString();
	}



	private List<JSONObject> fetchJSONObject(Map<String, String> parameters) {
		final int startRow = Integer.parseInt(parameters.get(JsonConstants.STARTROW_PARAMETER));
		final int endRow = Integer.parseInt(parameters.get(JsonConstants.ENDROW_PARAMETER));
		final List<Map<String, Object>> data = getData(parameters, startRow, endRow);
		final DataToJsonConverter toJsonConverter = OBProvider.getInstance().get(
				DataToJsonConverter.class);
		toJsonConverter.setAdditionalProperties(JsonUtils.getAdditionalProperties(parameters));
		return toJsonConverter.convertToJsonObjects(data);
	}
	@Override
	public Entity getEntity() {
		return ModelProvider.getInstance().getEntityByTableId(AD_TABLE_ID);
	}
	
	

	@Override
	protected List<Map<String, Object>> getData(Map<String, String> parameters,
			int startRow, int endRow) {
		OBContext.setAdminMode(true);
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		List<String> includedIds = new ArrayList<String>();
		distinct = parameters.get(JsonConstants.DISTINCT_PARAMETER);
		
	
		try{
			
			Criteria criteria = OBDal.getInstance().createCriteria(Product.class);
			List<Product > products = criteria.list();
			for (Product product : products) {
						Map<String, Object> row = new LinkedHashMap<String, Object>();
						row.put("id", product.getId());

						row.put("organization", OBDal.getInstance().get(Organization.class,product.getOrganization().getId()));
						row.put("client", product.getClient());
						row.put("creationDate",product.getCreationDate());
						row.put("createdBy", product.getCreatedBy());
						row.put("updated", product.getUpdated());
						row.put("updatedBy", product.getUpdatedBy());
						row.put("active", true);
						row.put("productValue", product.getSearchKey());
					
						row.put("product", product);
						row.put("qty", 0);
						row.put("obSelected", false);
						row.put("hasError", false);
						row.put("errorMsg", "");

						if (StringUtils.isNotEmpty(parameters.get("criteria"))&&! parameters.get("criteria").contains("_dummy")) {
							
							boolean addValue = new com.spocsys.vigfurniture.zebra.utils.ResultMapCriteriaUtils(row, parameters).applyFilter();
							if (!addValue) {
								continue;
							}
						}

						if (StringUtils.isNotEmpty(distinct)) {
							BaseOBObject obj = (BaseOBObject) row.get(distinct);
							if (includedIds.contains((String) obj.getId())) {
								continue;
							} else {
								includedIds.add((String) obj.getId());
							}
							Map<String, Object> distinctMap = new HashMap<String, Object>();
							distinctMap.put("id", obj.getId());
							distinctMap.put("_identifier", obj.getIdentifier());
							result.add(distinctMap);

						} else {
							result.add(row);
						}

					}
			


			//Sorting 

			if (result.isEmpty() || StringUtils.isNotEmpty(distinct)) {
				return result;
			}
			String sortBy = parameters.get(JsonConstants.SORTBY_PARAMETER);
			if (StringUtils.isEmpty(sortBy)) {
				sortBy = "documentNumber";
			}
			if (sortBy.endsWith("$_identifier")) {
				sortBy = sortBy.substring(0, sortBy.length() - 12);
			}
			Collections.sort(result, new ResultComparator(sortBy));

			OBContext.restorePreviousMode();
			count= result.size();
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return result;
	}

	


	private static class ResultComparator implements Comparator<Map<String, Object>> {
		private String compareBy = "convertedFreightAmount";
		boolean desc = false;

		public ResultComparator(String _compareBy) {
			compareBy = _compareBy;
			if (compareBy.startsWith("-")) {
				desc = true;
				compareBy = compareBy.substring(1);
			}
		}

		@Override
		public int compare(Map<String, Object> o1, Map<String, Object> o2) {
			Object obj1 = o1.get(compareBy);
			Object obj2 = o2.get(compareBy);
			if (obj2 == null) {
				return -1;
			} else if (obj1 == null) {
				return 1;
			}
			if (obj1 instanceof BigDecimal) {
				final BigDecimal v1 = (BigDecimal) o1.get(compareBy);
				final BigDecimal v2 = (BigDecimal) o2.get(compareBy);
				return desc ? v2.compareTo(v1) : v1.compareTo(v2);
			} else if (obj1 instanceof String) {
				final String v1 = (String) o1.get(compareBy);
				final String v2 = (String) o2.get(compareBy);
				return desc ? v2.compareTo(v1) : v1.compareTo(v2);
			} else if (obj1 instanceof Date) {
				final Date v1 = (Date) o1.get(compareBy);
				final Date v2 = (Date) o2.get(compareBy);
				return desc ? v2.compareTo(v1) : v1.compareTo(v2);
			} else if (obj1 instanceof BaseOBObject) {
				final String v1 = ((BaseOBObject) o1.get(compareBy)).getIdentifier();
				final String v2 = ((BaseOBObject) o2.get(compareBy)).getIdentifier();
				return desc ? v2.compareTo(v1) : v1.compareTo(v2);
			} else if (obj1 instanceof Boolean) {
				final boolean v1 = (Boolean) o1.get(compareBy);
				final boolean v2 = (Boolean) o2.get(compareBy);
				if (v1 == v2) {
					return 0;
				}
				if (v1) {
					return desc ? -1 : 1;
				} else {
					return desc ? 1 : -1;
				}
			} else {
				// unable to compare
				return 0;
			}
		}
	}}

