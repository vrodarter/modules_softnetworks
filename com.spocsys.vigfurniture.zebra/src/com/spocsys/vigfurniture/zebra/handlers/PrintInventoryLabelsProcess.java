package com.spocsys.vigfurniture.zebra.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.InventoryCountLine;
import org.openbravo.model.materialmgmt.transaction.InventoryCount;
import org.openbravo.service.db.DbUtility;

import com.spocsys.vigfurniture.zebra.model.Label;
import com.spocsys.vigfurniture.zebra.service.ZebraPrinterService;

import fr.w3blog.zpl.model.ZebraLabel;

public class PrintInventoryLabelsProcess extends BaseActionHandler  {



	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {

		JSONObject jsonRequest = null;
		
		System.out.println("Pasa por aqui...11");
		System.out.println(content);
		
		try {
			jsonRequest = new JSONObject(content);
			List<Label> labelsToPrint = new ArrayList<Label>();
			final JSONArray inventoryIds = jsonRequest.getJSONArray("shipmentLine");
			//for (int i = 0; i < shipmentsIds.length(); i++) {
			//	try {
			//		labelsToPrint.addAll(printShipmentLabelsByMovementQty(shipmentsIds.getString(i)));
			//	}catch (Exception e0) {
			//		System.out.println("Pasa por aqui...11.1");
			//		System.out.println(e0);
			//	}
					
			//}
			
			String inventoryid = new String();
			// Trae ID InOut
			for (int i = 0; i < inventoryIds.length(); i++) {
				inventoryid = inventoryIds.getString(i);
			}
			//ShipmentInOut inOut = OBDal.getInstance().get(ShipmentInOut.class, inventoryid);
			InventoryCount inventoryCount = OBDal.getInstance().get(InventoryCount.class, inventoryid);
			
			List<InventoryCountLine> listToPrint = inventoryCount.getMaterialMgmtInventoryCountLineList();
			System.out.println("Antes del listToPrint");
			System.out.println(listToPrint);
			//Itera todo el InOut
			for (int i = 0; i < listToPrint.size(); i++) {
				labelsToPrint.addAll(printShipmentLabelsByMovementQty(listToPrint.get(i).getId()));	
				System.out.println(listToPrint.get(i).getId());
			}
			//System.out.println("Pasa por aqui...12");

			if(labelsToPrint.isEmpty())
				throw new OBException("No Goods Shipment to Print");

			ZebraPrinterService.printLabelRestClient(labelsToPrint);

			JSONObject errorMessage = new JSONObject();
			errorMessage.put("severity", "TYPE_SUCCESS");
			errorMessage.put("title", OBMessageUtils.messageBD("Success"));
			jsonRequest.put("message", errorMessage);

		}catch (Exception e) {


			OBDal.getInstance().rollbackAndClose();
			System.out.println(e);
			System.out.println("Pasa por aqui...13");
			try {
				jsonRequest = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = ex.getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "TYPE_ERROR");
				errorMessage.put("text", message.concat(" Error en PrintProductLabelsProcess"));
				jsonRequest.put("message", errorMessage);
				System.out.println("Pasa por aqui...14");
			} catch (Exception e2) {
				System.out.println("Pasa por aqui...15");
				throw new OBException(e2); 
			}

		}
		return jsonRequest;
	}





	private List<Label> printShipmentLabelsByMovementQty(String lineId)  {
		System.out.println("Pasa por aqui...0200");
                System.out.println(lineId);
		List<Label> labelsToPrint = new ArrayList<Label>();
		int qty = 0;
		BigDecimal qty2 = BigDecimal.ZERO;
		try {
			//ShipmentInOutLine inOutLine = new ShipmentInOutLine();
			InventoryCountLine inventoryCountLine = new InventoryCountLine();
			
			try {
				inventoryCountLine = OBDal.getInstance().get(InventoryCountLine.class, lineId);
				System.out.println(inventoryCountLine);
			} catch (Exception e300){
				System.out.println("Pasa por aqui...200");
                                System.out.println(e300);
			}
			try {
				qty2 = new BigDecimal(inventoryCountLine.getSsgmsQtylabels());
			} catch (Exception e301) {
				System.out.println("Pasa por aqui...2001");
                                System.out.println(e301);
			}

			qty = qty2.intValue();


			Product product = new Product();
			try {
				product = (Product) OBDal.getInstance().getProxy(Product.ENTITY_NAME, inventoryCountLine.getProduct().getId());
			} catch (Exception e30){
				System.out.println("Pasa por aqui...20");
				System.out.println(e30);
			}
			try {
				if (qty > 0){
				  labelsToPrint.add(new Label(ZebraPrinterService.printserviceLabelary2(product,inventoryCountLine).getZplCode(),qty));
				}
			}catch (Exception e31){
				System.out.println("Pasa por aqui...201");
                                System.out.println(e31);
			}
		} catch (Exception e3){
		 	System.out.println("Pasa por aqui...21");
			//System.out.println(inOutLine);
			//System.out.println(lineId);
			//System.out.println(product);
			System.out.println(e3);
		}
		return labelsToPrint;
	}



}
