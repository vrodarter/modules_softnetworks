package com.spocsys.vigfurniture.zebra.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.service.db.DbUtility;

import com.spocsys.vigfurniture.zebra.model.Label;
import com.spocsys.vigfurniture.zebra.service.ZebraPrinterService;

import fr.w3blog.zpl.model.ZebraLabel;

public class PrintProduct extends BaseActionHandler  {



        @Override
        protected JSONObject execute(Map<String, Object> parameters, String content) {

                JSONObject jsonRequest = null;

                System.out.println("Pasa por aqui...11");
                System.out.println(content);

                try {
                        jsonRequest = new JSONObject(content);
                        List<Label> labelsToPrint = new ArrayList<Label>();
                        final JSONArray productIds = jsonRequest.getJSONArray("product");

                        String product_id = new String();
                        // Trae ID InOut
                        for (int i = 0; i < productIds.length(); i++) {
                                product_id = productIds.getString(i);
                        }
                        //ShipmentInOut inOut = OBDal.getInstance().get(ShipmentInOut.class, inoutid);
                        
                        //List<ShipmentInOutLine> listToPrint = inOut.getMaterialMgmtShipmentInOutLineList();
                        //System.out.println("Antes del listToPrint");
                        //System.out.println(listToPrint);
                        //Itera todo el InOut
                        //for (int i = 0; i < listToPrint.size(); i++) {
                                labelsToPrint.addAll(printShipmentLabelsByMovementQty(product_id));
                        //        System.out.println(listToPrint.get(i).getId());
                        //}
                        System.out.println("Pasa por aqui...12");

                        //printShipmentLabelsByMovementQty(product_id);

                        if(labelsToPrint.isEmpty())
                                throw new OBException("No Goods Shipment to Print");

                        ZebraPrinterService.printLabelRestClient(labelsToPrint);

                        JSONObject errorMessage = new JSONObject();
                        errorMessage.put("severity", "TYPE_SUCCESS");
                        errorMessage.put("title", OBMessageUtils.messageBD("Success"));
                        jsonRequest.put("message", errorMessage);

                }catch (Exception e) {


                        OBDal.getInstance().rollbackAndClose();
                        System.out.println(e);
                        System.out.println("Pasa por aqui...13");
                        try {
                                jsonRequest = new JSONObject();
                                Throwable ex = DbUtility.getUnderlyingSQLException(e);
                                String message = ex.getMessage();
                                JSONObject errorMessage = new JSONObject();
                                errorMessage.put("severity", "TYPE_ERROR");
                                errorMessage.put("text", message.concat(" Error en PrintProductLabelsProcess"));
                                jsonRequest.put("message", errorMessage);
                                System.out.println("Pasa por aqui...14");
                        } catch (Exception e2) {
                                System.out.println("Pasa por aqui...15");
                                throw new OBException(e2);
                        }

                }
                return jsonRequest;
        }

        private List<Label> printShipmentLabelsByMovementQty(String lineId)  {
                System.out.println("Pasa por aqui...0200");
                System.out.println(lineId);
                List<Label> labelsToPrint = new ArrayList<Label>();
                int qty = 0;
                BigDecimal qty2 = BigDecimal.ZERO;
                try {
                        //ShipmentInOutLine inOutLine = new ShipmentInOutLine();
                        Product product = new Product();
                        try {
                                //inOutLine = OBDal.getInstance().get(ShipmentInOutLine.class, lineId);
                                product = (Product) OBDal.getInstance().getProxy(Product.ENTITY_NAME, lineId);
                                System.out.println(product);
                        } catch (Exception e300){
                                System.out.println("Pasa por aqui...200");
                                System.out.println(e300);
                        }
                        
                        try {
                                labelsToPrint.add(new Label(ZebraPrinterService.printserviceLabelary3(product).getZplCode(),1));
                        }catch (Exception e31){
                                System.out.println("Pasa por aqui...201");
                                System.out.println(e31);
                        }
                } catch (Exception e3){
                        System.out.println("Pasa por aqui...21");
                        //System.out.println(inOutLine);
                        //System.out.println(lineId);
                        //System.out.println(product);
                        System.out.println(e3);
                }
                return labelsToPrint;
        }



}

