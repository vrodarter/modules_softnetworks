package com.spocsys.vigfurniture.zebra.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.service.db.DbUtility;
import org.openbravo.warehouse.packing.Packing;

import com.spocsys.vigfurniture.zebra.model.Label;
import com.spocsys.vigfurniture.zebra.service.ZebraPrinterService;

import fr.w3blog.zpl.model.ZebraLabel;

public class PrintShippingLabelsProcess extends BaseActionHandler  {



	@Override
	protected JSONObject execute(Map<String, Object> parameters, String content) {

		JSONObject jsonRequest = null;
		// JSONObject response = null;
		try {
			jsonRequest = new JSONObject(content);
			List<Label> labelsToPrint = new ArrayList<Label>();
			final JSONArray shipmentsIds = jsonRequest.getJSONArray("shipmentHeaders");
			for (int i = 0; i < shipmentsIds.length(); i++) {


				labelsToPrint.addAll(printShipmentLabelsByPacking(shipmentsIds.getString(i)));

				
			}
			
			if(labelsToPrint.isEmpty())
				throw new OBException("No Goods Shipment to Print");

			ZebraPrinterService.printLabelRestClient3(labelsToPrint);

			JSONObject errorMessage = new JSONObject();
			errorMessage.put("severity", "TYPE_SUCCESS");
			errorMessage.put("title", OBMessageUtils.messageBD("Success"));
			// errorMessage.put("text", OBMessageUtils.messageBD("Pack List Automation Process Done!!!"));
			jsonRequest.put("message", errorMessage);
			

		}catch (Exception e) {


			OBDal.getInstance().rollbackAndClose();

			try {
				jsonRequest = new JSONObject();
				Throwable ex = DbUtility.getUnderlyingSQLException(e);
				String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "TYPE_ERROR");
				errorMessage.put("text", message);
				jsonRequest.put("message", errorMessage);
			} catch (Exception e2) {

			}

		}
		return jsonRequest;
	}



	@SuppressWarnings("unused")
	private ZebraLabel printShipmentLabels(String shipmentid)  {
		ShipmentInOut inOut = OBDal.getInstance().get(ShipmentInOut.class, shipmentid);

		return	ZebraPrinterService.printShippingLabels(inOut,"");


	}

	private List<Label> printShipmentLabelsByPacking(String packingId)  {



		List<Label> labelsToPrint = new ArrayList<Label>();
		Packing packing =  OBDal.getInstance().get(Packing.class, packingId);
		int qtyBoxes =1;
		if(packing.getOBWPACKBoxList()!=null &&! packing.getOBWPACKBoxList().isEmpty())
		  qtyBoxes = packing.getOBWPACKBoxList().size();
		
		List<ShipmentInOut> inOuts = packing.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList();


		for (ShipmentInOut shipmentInOut : inOuts) {
			
			for (int i = 1; i <=qtyBoxes; i++) {
				labelsToPrint.add(new Label( ZebraPrinterService.printShippingLabels(shipmentInOut,i+"/"+qtyBoxes).getZplCode(),1));
			}
			
		}


		return labelsToPrint;
	}










}
