package com.spocsys.vigfurniture.zebra.handlers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.ApplicationConstants;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.plm.Product;

import com.spocsys.vigfurniture.zebra.model.Label;
import com.spocsys.vigfurniture.zebra.service.ZebraPrinterService;

import fr.w3blog.zpl.model.ZebraLabel;

public class SelectProductsForPrintLabelsHandler extends
BaseProcessActionHandler {

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters, String content) {
		JSONObject msg = new JSONObject();
		JSONObject request = null;
		try {		
			request=new JSONObject(content);

			JSONObject params = request.getJSONObject("_params");

			JSONObject message = params.getJSONObject("Select products for print labels");			

			List<Label> labelsToPrint = new ArrayList<Label>();

			JSONArray selection = new JSONArray(message.getString(ApplicationConstants.SELECTION_PROPERTY));

			msg.put("severity", "info");

			msg.put("text", "Sucess");

			request.put("message", msg);

			for(int i = 0; i < selection.length(); i++){
				JSONObject selectedProductJSON = selection.getJSONObject(i);
				String productID = selectedProductJSON.getString("id");	
				Integer  count = selectedProductJSON.getInt("qty");	
				Product product= OBDal.getInstance().get(Product.class, productID);
					if(product!=null){
							ZebraLabel zebraLabel = ZebraPrinterService.printserviceLabelary(product);
							Label label = new Label(zebraLabel.getZplCode(),count);
							labelsToPrint.add(label);
					}
			
			}
			ZebraPrinterService.printLabelRestClient(labelsToPrint);
			request.put("message", msg);
			request.put("retryExecution", true);
			return request;

		} catch (Exception e) {
		
			msg.remove("severity"); 
			msg.remove("text"); 
			
			try {
				msg.put("severity", "error");
				msg.put("text", e.getMessage());
				request.put("message", msg);
				request.put("retryExecution", true);
			} catch (JSONException e1) {
			}

		      return request;
		}



	}

}
