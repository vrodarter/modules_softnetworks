package com.spocsys.vigfurniture.zebra.model;


public class Label {

	private String label;
	
	private int numbersOfCopy;
	
	public Label() {
		// TODO Auto-generated constructor stub
	}

	public Label(String label, int numbersOfCopy) {
		super();
		this.label = label;
		this.numbersOfCopy = numbersOfCopy;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getNumbersOfCopy() {
		return numbersOfCopy;
	}

	public void setNumbersOfCopy(int numbersOfCopy) {
		this.numbersOfCopy = numbersOfCopy;
	}
	
}
