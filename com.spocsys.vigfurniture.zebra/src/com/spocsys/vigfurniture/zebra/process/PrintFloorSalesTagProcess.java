/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2012 Openbravo SLU
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.spocsys.vigfurniture.zebra.process;

import java.awt.Graphics2D;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.HttpBaseUtils;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.PrintJRData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.domain.Reference;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductBOM;
import org.openbravo.model.pricing.pricelist.ProductPrice;
import org.openbravo.utils.FileUtility;
import org.openbravo.utils.Replace;
import org.openbravo.xmlEngine.XmlDocument;

import com.spocsys.vigfurniture.zebra.utils.ReportingUtils;
import com.spocsys.vigfurniture.zebra.utils.ReportingUtils.ExportType;

public class PrintFloorSalesTagProcess extends HttpSecureAppServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
	ServletException {
		VariablesSecureApp vars = new VariablesSecureApp(request);

		if (vars.commandIn("DEFAULT")) {

			clearSessionValue(vars);
			String strWindow = vars.getStringParameter("inpwindowId");
			String strTab = vars.getStringParameter("inpTabId");
			String product = vars.getStringParameter("M_Product_ID");

			vars.setSessionValue("inpwindowId", strWindow);
			vars.setSessionValue("productid", product);
			vars.setSessionValue("inpTabId", strTab);


			printPage(request,response,vars,product);

		}else if (vars.commandIn("GENERATE")) {
			String productId = vars.getGlobalVariable("", "productid");
			OBError myMessage = new OBError();


			String format = vars.getRequestGlobalVariable("inpFormat",
					"PrintFloorSalesTagProcess|inpFormat");

			String strPriceList = vars.getRequestGlobalVariable("inpcPriceListId",
					"PrintFloorSalesTagProcess|M_PriceList_ID");

			String strOrg = vars.getRequestGlobalVariable("inpOrganizacion",
					"PrintFloorSalesTagProcess|AD_ORG_ID");

			myMessage =printPagePDF( request,  response, vars, format ,  strPriceList,  productId, strOrg);


			
			
			String strTab = vars.getGlobalVariable("", "inpTabId");
			String strWindowPath = Utility.getTabURL(strTab, "R", true);
			vars.setMessage(strTab, myMessage);
			
			if(myMessage.getType().equalsIgnoreCase("Error"))
			printPageClosePopUp(response, vars,strWindowPath);
		}
	}



	private void clearSessionValue(VariablesSecureApp vars) {
		vars.removeSessionValue("fFinancialAccountId");
		vars.removeSessionValue("inpwindowId");
		vars.removeSessionValue("inpTabId");
		vars.removeSessionValue("productid");
	}

	private void printPage(HttpServletRequest request, HttpServletResponse response,
			VariablesSecureApp vars, String product) throws IOException, ServletException {
		// Check for permissions to apply modules from application server.


		final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurniture/zebra/process/PrintFloorSalesTagProcess").createXmlDocument();
		xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
		xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
		xmlDocument.setParameter("theme", vars.getTheme());
		{
			final OBError myMessage = vars.getMessage("PrintFloorSalesTagProcess");
			vars.removeMessage("PrintFloorSalesTagProcess");
			if (myMessage != null) {
				xmlDocument.setParameter("messageType", myMessage.getType());
				xmlDocument.setParameter("messageTitle", myMessage.getTitle());
				xmlDocument.setParameter("messageMessage", myMessage.getMessage());
			}
		}

		xmlDocument.setData("reportC_Org_ID", "liststructure",PrintFloorSalesTagProcessData.selectOrg(this, Utility.getContext(this, vars, "#User_Org", "PrintFloorSalesTagProcess")) );


		xmlDocument.setParameter("pricelist", Utility.arrayDobleEntrada("arrPriceList",PrintFloorSalesTagProcessData.selectPriceList(this, Utility.getContext(this, vars, "#User_Org", "PrintFloorSalesTagProcess"), processPriceList(OBDal.getInstance().get(Product.class,product ))) ));

		//Payment method for payment plan


		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();

		out.println(xmlDocument.print());
		out.close();
	}


	private void printPageResponse(HttpServletRequest request, HttpServletResponse response,VariablesSecureApp vars,String documentId,String selectedPaymentMethod, List<String> selectedPayments,List<String> selectedGlitems) throws IOException, ServletException {
		// Check for permissions to apply modules from application server.


		final XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/spocsys/vigfurniture/zebra/process/PrintFloorSalesTagProcess").createXmlDocument();
		xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
		xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\r\n");
		xmlDocument.setParameter("theme", vars.getTheme());
		{
			final OBError myMessage = vars.getMessage("PrintFloorSalesTagProcess");
			vars.removeMessage("PrintFloorSalesTagProcess");
			if (myMessage != null) {
				xmlDocument.setParameter("messageType", myMessage.getType());
				xmlDocument.setParameter("messageTitle", myMessage.getTitle());
				xmlDocument.setParameter("messageMessage", myMessage.getMessage());
			}
		}


		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();

		out.println(xmlDocument.print());
		out.close();
	}

	private OBError printPagePDF(HttpServletRequest request, HttpServletResponse response,
			VariablesSecureApp vars,String format , String strPriceList, String productId,String strOrg) throws ServletException, IOException{


		Product product = OBDal.getInstance().get(Product.class, productId); 

		String strReportName = "@basedesign@/com/spocsys/vigfurniture/zebra/process/"+format+".jasper";
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		final OBError myMessage = new OBError();

		try{

			String productPrice = PrintFloorSalesTagProcessData.getProductPriceByProductListID(this,strPriceList,productId);
			ProductPrice  price = null;

			if(StringUtils.isNotEmpty(productPrice))
				price= OBDal.getInstance().get(ProductPrice.class,productPrice );


			OBContext.getOBContext().setAdminMode(true);
			BufferedImage bImageFromConvert =Utility.showImageLogo("yourcompanylegal", strOrg);
			if(bImageFromConvert!=null){
				parameters.put("image", bImageFromConvert);
			}
							

			


			parameters.put("productid",product.getSearchKey());
			parameters.put("vendormodel",StringUtils.isNotEmpty(product.getSvfsvModel())?product.getSvfsvModel():"");

			if(product.getBrand()!=null)
				parameters.put("brand",StringUtils.isNotEmpty(product.getBrand().getName())?product.getBrand().getName():"");
			else
				parameters.put("brand","");
			parameters.put("name",StringUtils.isNotEmpty(product.getName())?product.getName():"");

			String colormaterial= colorMaterialFormat(product);

			parameters.put("colormaterial",StringUtils.isNotEmpty(colormaterial)?colormaterial:"");

			//Price List
			if(price!=null){
				parameters.put("regularPrice","$ "+price.getListPrice().toString());
				parameters.put("salesPrice","$ "+price.getStandardPrice().toString());

			}
			else{
				parameters.put("regularPrice"," ");
				parameters.put("salesPrice"," ");

			}
			parameters.put("w",StringUtils.isNotEmpty(product.getSvfsvShippingWidth())?product.getSvfsvShippingWidth():"");

			parameters.put("d",StringUtils.isNotEmpty(product.getSvfsvShippingDepth())?product.getSvfsvShippingDepth():"");
			parameters.put("h",StringUtils.isNotEmpty(product.getSvfsvShippingHeight())?product.getSvfsvShippingHeight():"");

			parameters.put("keyfeature","");

			parameters.put("shippingW",(product.getWeight()!=null && StringUtils.isNotEmpty(product.getWeight().toString()))?"Weight: "+product.getWeight()+ (product.getUOMForWeight()!=null ?" "+product.getUOMForWeight().getName():"" ):"");

			parameters.put("shippingV",( product.getSvfsvVolume()!=null &&StringUtils.isNotEmpty(product.getSvfsvVolume().toString()))?"Volume: "+product.getSvfsvVolume()+" Cu.In":"");
					
			parameters.put("barcode",product.getSearchKey());

			ComponentsDatasource datasource =  new ComponentsDatasource();
			if(format.equalsIgnoreCase("006")&&product.getProductBOMList()!=null &&! product.getProductBOMList().isEmpty() )
				datasource.set(product.getProductBOMList());
			renderJRCustom(vars,response,strReportName,product.getSearchKey()+"-"+Utility.formatDate(new Date(), vars.getJavaDateFormat())+"-"+format, "pdf", parameters, datasource, null,false);

		}catch(Exception e){
			
			myMessage.setType("Error");
			
			myMessage.setMessage(e.getMessage()==null ||e.getMessage().isEmpty() ?"There was an error":e.getMessage());

		}finally{
			OBContext.getOBContext().restorePreviousMode();
		}
return myMessage;

	}


	private String processPriceList(Product product){
		StringBuilder priceIds = new StringBuilder();


		List<ProductPrice> productPrices =product.getPricingProductPriceList();
		int i = 0;
		for (ProductPrice productPrice : productPrices) {
			if (i == 0) {
				priceIds.append("('" + productPrice.getPriceListVersion().getPriceList().getId() + "'");
			} else {
				priceIds.append( ",'"+ productPrice.getPriceListVersion().getPriceList().getId() + "'");
			}
			i++;
		}

		if(!priceIds.toString().equalsIgnoreCase(""))
			priceIds.append(")");
		
		return  StringUtils.isNotEmpty(priceIds.toString())?priceIds.toString():"";

	}
	private String colorMaterialFormat(Product product){
		StringBuilder builder = new StringBuilder();
		

		if(StringUtils.isNotEmpty(product.getSvfsvColor()) ){
			OBCriteria<org.openbravo.model.ad.domain.List> criteria = OBDal.getInstance().createCriteria(org.openbravo.model.ad.domain.List.class);
			criteria.add(Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_SEARCHKEY,product.getSvfsvColor()));
			criteria.add(Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_REFERENCE,OBDal.getInstance().get(Reference.class, "8346AE1A87BB44AF9F941D488A458D66")));
			if (criteria.list()!=null && !criteria.list().isEmpty()) {
				builder.append(criteria.list().get(0).getName());
			}


		} if(StringUtils.isNotEmpty(product.getSvfsvColorCode()) ){
			builder.append((StringUtils.isEmpty(builder.toString())?"":"/")+product.getSvfsvColorCode());
		}

		if(StringUtils.isNotEmpty(product.getSvfsvFinishMaterial()) ){
			OBCriteria<org.openbravo.model.ad.domain.List> criteria = OBDal.getInstance().createCriteria(org.openbravo.model.ad.domain.List.class);
			criteria.add(Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_SEARCHKEY,product.getSvfsvFinishMaterial()) );
			criteria.add(Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_REFERENCE,OBDal.getInstance().get(Reference.class, "43218167922F4BAEAAD45461EB3BE988")));
			if (criteria.list()!=null && !criteria.list().isEmpty()) {
				builder.append((StringUtils.isEmpty(builder.toString())?"":" ,")+criteria.list().get(0).getName());
			}}

		if(StringUtils.isNotEmpty(product.getSvfsvFinishMaterialCode()) ){
			builder.append((StringUtils.isEmpty(builder.toString())?"":"/")+product.getSvfsvFinishMaterialCode());
		}



		return (builder.toString().equalsIgnoreCase("null")?"":builder.toString());
	}


	class ComponentsDatasource implements JRDataSource
	{
		private List<ProductBOM> boms = new ArrayList<ProductBOM>();

		public void set(List<ProductBOM> boms) {
			this.boms = boms;
		}

		private int indice = -1;

		public ComponentsDatasource() {
			// TODO Auto-generated constructor stub
		}
		@Override
		public Object getFieldValue(JRField jrf) throws JRException
		{
			String valor = null;

			ProductBOM bom = boms.get(indice);		  
			Product product = bom.getBOMProduct();

			if ("component".equals(jrf.getName()))
			{
				valor = bom.getBOMProduct().getSearchKey();
			}
			else if ("description".equals(jrf.getName()))
			{
				if(product!=null)
					valor = StringUtils.isNotEmpty(product.getName())?product.getName():"";
			}
			else if ("qty".equals(jrf.getName()))
			{
				valor = bom.getBOMQuantity()!=null?bom.getBOMQuantity().toString():"";

			}else if ("w".equals(jrf.getName()))
			{

				if(product!=null)
					valor = StringUtils.isNotEmpty(product.getSvfsvShippingWidth())?product.getSvfsvShippingWidth():"";
			}else if ("v".equals(jrf.getName()))
			{
				if(product!=null)
					valor = product.getSvfsvVolume()!=null?product.getSvfsvVolume().toString():"";

			}


			return valor;
		}
		@Override
		public boolean next() throws JRException {
			return ++indice < boms.size();
		}

	}

	private void renderJRCustom(VariablesSecureApp variables, HttpServletResponse response,
			String strReportName, String strFileName, String strOutputType,
			HashMap<String, Object> designParameters, JRDataSource data,
			Map<Object, Object> exportParameters, boolean forceRefresh) throws ServletException {
		if (strReportName == null || strReportName.equals(""))
			strReportName = PrintJRData.getReportName(this, classInfo.id);

		final String strAttach = globalParameters.strFTPDirectory + "/284-" + classInfo.id;

		final String strLanguage = variables.getLanguage();
		final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

		final String strBaseDesign = getBaseDesignPath(strLanguage);

		strReportName = Replace.replace(Replace.replace(strReportName, "@basedesign@", strBaseDesign),
				"@attach@", strAttach);
		if (strFileName == null) {
			strFileName = strReportName.substring(strReportName.lastIndexOf("/") + 1);
		}

		ServletOutputStream os = null;
		UUID reportId = null;
		try {
			if (designParameters == null)
				designParameters = new HashMap<String, Object>();

			designParameters.put("BASE_WEB", strReplaceWithFull);
			designParameters.put("BASE_DESIGN", strBaseDesign);
			designParameters.put("ATTACH", strAttach);
			designParameters.put("USER_CLIENT", Utility.getContext(this, variables, "#User_Client", ""));
			designParameters.put("USER_ORG", Utility.getContext(this, variables, "#User_Org", ""));
			designParameters.put("LANGUAGE", strLanguage);
			designParameters.put("LOCALE", locLocale);
			designParameters.put("REPORT_TITLE",
					PrintJRData.getReportTitle(this, variables.getLanguage(), classInfo.id));

			final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator(variables.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
			dfs.setGroupingSeparator(variables.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
			final DecimalFormat numberFormat = new DecimalFormat(
					variables.getSessionValue("#AD_ReportNumberFormat"), dfs);
			designParameters.put("NUMBERFORMAT", numberFormat);

			os = response.getOutputStream();
			if (exportParameters == null)
				exportParameters = new HashMap<Object, Object>();
			if (strOutputType == null || strOutputType.equals(""))
				strOutputType = "html";
			final ExportType expType = ExportType.getExportType(strOutputType.toUpperCase());

			if (strOutputType.equals("html")) {
				if (log4j.isDebugEnabled())
					log4j.debug("JR: Print HTML");
				response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "."
						+ strOutputType);
				HttpServletRequest request = RequestContext.get().getRequest();
				String localAddress = HttpBaseUtils.getLocalAddress(request);
				exportParameters.put(ReportingUtils.IMAGES_URI, localAddress + "/servlets/image?image={0}");
				ReportingUtils.exportJR(strReportName, expType, designParameters, os, false, this, data,
						exportParameters);
			} else if (strOutputType.equals("pdf") || strOutputType.equalsIgnoreCase("xls")
					|| strOutputType.equalsIgnoreCase("txt") || strOutputType.equalsIgnoreCase("csv")) {
				reportId = UUID.randomUUID();
				File outputFile = new File(globalParameters.strFTPDirectory + "/" + strFileName + "-"
						+ (reportId) + "." + strOutputType);
				ReportingUtils.exportJR(strReportName, expType, designParameters, outputFile, false, this,
						data, exportParameters);
				response.setContentType("text/html;charset=UTF-8");
				response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "-"
						+ (reportId) + ".html");
				if (forceRefresh) {
					printPagePopUpDownloadAndRefresh(response.getOutputStream(), strFileName + "-"
							+ (reportId) + "." + strOutputType);
				} else {
					printPagePopUpDownload(response.getOutputStream(), strFileName + "-" + (reportId) + "."
							+ strOutputType);
				}
			}

		} catch (IOException ioe) {
			try {
				FileUtility f = new FileUtility(globalParameters.strFTPDirectory, strFileName + "-"
						+ (reportId) + "." + strOutputType, false, true);
				if (f.exists())
					f.deleteFile();
			} catch (IOException ioex) {
				log4j.error("Error trying to delete temporary report file " + strFileName + "-"
						+ (reportId) + "." + strOutputType + " : " + ioex.getMessage());
			}
		} catch (final Exception e) {
			throw new ServletException(e.getMessage(), e);
		} finally {
			try {
				os.close();
			} catch (final Exception e) {
			}
		}
	}




}