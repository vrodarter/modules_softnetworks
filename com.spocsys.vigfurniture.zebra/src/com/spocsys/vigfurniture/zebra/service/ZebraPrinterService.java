package com.spocsys.vigfurniture.zebra.service;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Iterator;
import java.util.Date;
import java.util.Calendar;
import java.text.DateFormat;
import java.util.TimeZone;
import java.util.GregorianCalendar;

import javax.ws.rs.core.MediaType;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.system.ClientInformation;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Location;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.warehouse.packing.Packing;
import org.openbravo.model.materialmgmt.transaction.InventoryCountLine;
import org.openbravo.model.materialmgmt.transaction.InventoryCount;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.spocsys.vigfurniture.zebra.model.Label;
import com.spocsys.vigfurniture.zebra.utils.StringUtils;
import com.spocsys.softnet.prppool.SsppUserPrts;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import fr.w3blog.zpl.constant.ZebraFont;
import fr.w3blog.zpl.model.ZebraLabel;
import fr.w3blog.zpl.model.element.ZebraBarCode128;
import fr.w3blog.zpl.model.element.ZebraGraficBox;
import fr.w3blog.zpl.model.element.ZebraNativeZpl;
import fr.w3blog.zpl.model.element.ZebraText;
import fr.w3blog.zpl.utils.ZebraUtils;

public class ZebraPrinterService {



	public static ZebraLabel printserviceLabelary(Product product) {


		ZebraLabel zebraLabel = new ZebraLabel(900, 750);


		zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);

		//zebraLabel.addElement(new ZebraNativeZpl("^FO10,460^GFA,2002,2002,22,K01gPFE,K07gQF8,K0CgQ0C,K0CR01MFQ0C,J018R01LFEQ06,J018T07IFER06,J018T03IF8R06,J018T01IFS06,J018U0FFEK0FF001I06,J0181KFI03IF80FFCJ0JF03I06,J01803IF8J07FF00FFCI07JFE3I06,J01800FFEK01FC00FFC001FE003FFI06,J018007FEK01F001FFC003FJ07FI06,J018003FEK01F001FF800FEJ03FI06,J018001FEK01E001FF801F8K0F8006,J018001FFK03C001FF803FL078006,J018I0FFK03C001FF807FL038006,J018I0FF8J078003FF00FEL038006,J018I07F8J078003FF00FCL018006,J018I07FCJ07I03FF01FCL018006,J018I03FCJ0FI03FF03F8M08006,J018I03FEJ0EI03FF03F8P06,J018I01FEI01EI07FF07F8P06,J018I01FFI01CI07FE07FQ06,J018J0FFI03CI07FE07FQ06,J018J0FF80038I07FE07FQ06,J018J07F80078I07FE0FFQ06,J018J07FC007J0FFE0FFK07IFE06,J018J03FC00FJ0FFC0FFK03IFC06,J018J03FE00EJ0FFC0FFL07FE006,J018J01FE01EJ0FFC0FFL03FC006,J018J01FF01CJ0FFC0FFL01FC006,J018K0FF03CI01FFC0FFL01F8006,J018K0FF838I01FF80FFL01F8006,J018K07F878I01FF807FL01F8006,J018K07FC7J01FF807F8K01F8006,J018K03FCFJ01FF807F8K01F8006,J018K03FEEJ03FF803FCK01F8006,J018K01FFEJ03FF003FCK01F8006,J018K01FFEJ03FF001FEK01F8006,J018L0FFCJ03FFI0FFK01F8006,J018L0FFCJ03FFI07FK01F8006,J018L07F8J03FFI03F8J01F8006,J018L07F8J07FEI01FEJ01F8006,J018L03FK07FEJ0FFJ03F8006,J018L03FK07FEJ07FCI07F8006,J018L01EK07FEJ01FF803FEI06,J018L01EK0FFEK03KFJ06,J018M0CK0FFEL07IF8J06,J018R01FFEU06,J018R03IFU06,J018R0JF8T06,J018Q0LFES06,K0CP01LFES0C,K0CgQ0C,K07gP038,K03gQF,,::::::::::L07FF381C7FF0F038E7FF9C0F3FF87FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF,L0780381C783CFC38E0781C0F3C1E78,:L0780381C7838FE38E0781C0F3C1E78,L07FE381C7878EF38E0781C0F3C3C7FF,L07FE381C7FF0E738E0781C0F3FF87FF,L07FE381C7FF0E3B8E0781C0F3FFC7FF,L0780381C7878E3F8E0781C0E3C3C78,L0780381C7838E1F8E0781C0E3C1C78,L07803C3C7838E1F8E0781E0E3C1E78,L07803FFC783CE0F8E0781FFE3C1E7FF8,L07801FF8783CE078E0780FFC3C0E7FF8,L07I0FE0701CE078E07803F83C0E7FF8,,:::::::^FS"));

		//zebraLabel.addElement(new ZebraText(50, 65,product.getSearchKey(), 7));

		zebraLabel.addElement(new ZebraBarCode128(50, 75,product.getSearchKey(),60, 1,30));

		zebraLabel.addElement(new ZebraBarCode128(550, 75,product.getSearchKey(),60, 1,30));

		zebraLabel.addElement(new ZebraText(10, 110,"Prueba de Impresion", 5));


		//zebraLabel.addElement(new ZebraGraficBox(10, 120, 860, 1, 3, "B"));



		//zebraLabel.addElement(new ZebraText(10, 145, "Model Number: "+(product.getSvfsvModel()!=null?product.getSvfsvModel():"" ), 5));

		//zebraLabel.addElement(new ZebraText(10, 175, "Description: "+(product.getName()!=null?product.getName():""), 5));

		//zebraLabel.addElement(new ZebraText(550, 145, "S/O: "/*product.getSearchKey()*/, 5));

		//zebraLabel.addElement(new ZebraText(550, 175, "P/O:"/*product.getSearchKey()*/, 5));

		//zebraLabel.addElement(new ZebraGraficBox(10, 240, 860, 1, 3, "B"));



		//zebraLabel.addElement(new ZebraText(10, 265, "Additional Information: "+ (product.isSvfsvSpecialOrder()? product.getDescription():"") , 5));
	    //zebraLabel.addElement(new ZebraText(595, 300, (org.apache.commons.lang.StringUtils.isNotEmpty(product.getUPCEAN())?product.getUPCEAN():""), 5));

		//zebraLabel.addElement(new ZebraGraficBox(10, 330, 860, 1, 3, "B"));

		//zebraLabel.addElement(new ZebraText(10, 350, "Rsvd For:"+ ((product.isSvfsvSpecialOrder()&& product.getBusinessPartner()!=null)?product.getBusinessPartner().getName():""), 5));

		//zebraLabel.addElement(new ZebraText(10, 380, "Group: "+(product.getProductCategory()!=null?product.getProductCategory().getName():""), 5));

		//zebraLabel.addElement(new ZebraText(10, 410, "Description: "+(product.getName()!=null?product.getName():""), 5));



		//zebraLabel.addElement(new ZebraText(590, 420, product.getBrand()!=null?product.getBrand().getName():"", 8));
		//zebraLabel.addElement(new ZebraText(570, 450,(product.getProductCategory()!=null?product.getProductCategory().getName():""), 5));


		//zebraLabel.addElement(new ZebraBarCode128(280, 520,  product.getSearchKey(),60, 1,30));

		//zebraLabel.addElement(new ZebraText(600, 490, "Recieved in: "+ (product.getStorageBin()!=null?product.getStorageBin().getSearchKey():""), 5));
		//SimpleDateFormat  dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		//zebraLabel.addElement(new ZebraText(600, 520, "on: "+(product.getSvfsvEta()!=null?dateFormat.format(product.getSvfsvEta()):""), 5));
		String string = zebraLabel.getZplCode();



		System.out.println(string);
		return zebraLabel;


	}



	public static ZebraLabel printserviceLabelaryold2(Product product,ShipmentInOutLine inOutLine) {
		ZebraLabel zebraLabel = new ZebraLabel(812, 203);
		zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);
		zebraLabel.addElement(new ZebraText(10, 20,product.getName(), 5));
		try {
			zebraLabel.addElement(new ZebraText(300, 20,"Order:"+inOutLine.getShipmentReceipt().getSalesOrder().getDocumentNo(), 5));
		} catch(Exception e){
                        System.out.println(e);
                }
		try {
                        zebraLabel.addElement(new ZebraText(300, 40,"Loc:"+inOutLine.getStorageBin().getSearchKey(), 5));
                } catch(Exception e){
                        System.out.println(e);
                }
		try {
                        zebraLabel.addElement(new ZebraText(500, 20,"WH:"+inOutLine.getShipmentReceipt().getWarehouse().getName(), 5));
                } catch(Exception e){
                        System.out.println(e);
                }
		try {
                        zebraLabel.addElement(new ZebraText(500, 40,"Rep:"+inOutLine.getShipmentReceipt().getCreatedBy().getName(), 5));
                } catch(Exception e){
                        System.out.println(e);
                }
		zebraLabel.addElement(new ZebraBarCode128(50, 110,product.getSearchKey(),60, 1,30));
		try {
			zebraLabel.addElement(new ZebraBarCode128(500, 110,inOutLine.getAttributeSetValue().getDescription(),60, 1,30));
		} catch(Exception e){
			System.out.println(e);
		}
		//zebraLabel.addElement(new ZebraText(10, 140,"Prueba de Impresion", 5));
		String string = zebraLabel.getZplCode();
		System.out.println(string);
		return zebraLabel;

	}






	public static ZebraLabel printserviceLabelary2(Product product,ShipmentInOutLine inOutLine) {
		ZebraLabel zebraLabel = new ZebraLabel(812, 203);
		zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);
		String nameProduct = product.getName();
		//System.out.println("nuevo labelary 2.....cha");	
		if (product.getName().length() >= 29){
		  zebraLabel.addElement(new ZebraText(10, 30,product.getName().substring(0, 28), 5));
		  //System.out.println("nuevo labelary 3.....cha");
		} else {
		  try {
		    zebraLabel.addElement(new ZebraText(10, 30,product.getName().substring(0, product.getName().length()-1), 5));
		    //System.out.println("nuevo labelary 4.....cha");
		  } catch (Exception e) {
		    //System.out.println("nuevo labelary 5.....cha");
		    System.out.println(e);
		  }
		}
		try {
		  if (nameProduct.length() >= 29){
		    zebraLabel.addElement(new ZebraText(10, 50,nameProduct.substring(0,28), 5));
		  } else{
		    zebraLabel.addElement(new ZebraText(10, 50,nameProduct.substring(0,nameProduct.length()-1), 5));
		  }
		  //System.out.println("nuevo labelary 6.....cha");
		} catch (Exception e) {
		  //System.out.println("nuevo labelary 7.....cha");
		  System.out.println(e);
		}
		//System.out.println("nuevo labelary 8.....cha");
		//zebraLabel.addElement(new ZebraText(300, 30,"CONDITION:",5));
		//System.out.println("nuevo labelary 8.1.....cha");
		try {
		   //System.out.println("nuevo labelary 8.2....cha");
		   if (product.getSnmtCondition() != null) {
		     //System.out.println("nuevo labelary 8.3....cha");
		     zebraLabel.addElement(new ZebraText(370, 130,product.getSnmtCondition(),20));
		     //System.out.println("nuevo labelary 8.4....cha");
		   }
		} catch (Exception e) {
		   //System.out.println("nuevo labelary 9.....cha");
		   System.out.println(e);
		}
		try{
		  zebraLabel.addElement(new ZebraText(500, 30,"Loc:"+inOutLine.getStorageBin().getSearchKey(), 5));
		}catch (Exception e) {
		  //System.out.println("nuevo labelary 9.2....cha");
                  System.out.println(e);
		}
		zebraLabel.addElement(new ZebraText(500, 50,"WH:"+inOutLine.getShipmentReceipt().getWarehouse().getName(), 5));
        	try{
		  zebraLabel.addElement(new ZebraText(500, 70,"PT:"+inOutLine.getSalesOrderLine().getSalesOrder().getDocumentNo(), 5));
		} catch (Exception e){
		   //System.out.println("nuevo labelary 9.1....cha");
                   System.out.println(e);
	
		}	
        	zebraLabel.addElement(new ZebraBarCode128(40, 140,product.getSearchKey(),60,false, 1,30));
		zebraLabel.addElement(new ZebraText(50, 160,product.getSearchKey(), 5));
		try {
		   if (inOutLine.getAttributeSetValue().getDescription()==null){
		   	zebraLabel.addElement(new ZebraBarCode128(500, 140,"0000000000",60,false, 1,30));
		   }else {
			zebraLabel.addElement(new ZebraBarCode128(500, 140,inOutLine.getAttributeSetValue().getDescription().substring(1),60,false, 1,30));
			zebraLabel.addElement(new ZebraText(500, 160,inOutLine.getAttributeSetValue().getDescription().substring(1), 5));
		   }
		} catch (Exception e) {
		  //System.out.println("nuevo labelary 10.....cha");
		  zebraLabel.addElement(new ZebraBarCode128(500, 140,"0000000000",60,false, 1,30));
		  System.out.println(e);
		}
		//System.out.println("nuevo labelary 11.....cha");
		String string = zebraLabel.getZplCode();
		System.out.println(string);
		return zebraLabel;

	}

	//public static ZebraLabel printserviceLabelary2(Product product,ShipmentInOutLine inOutLine) { InventoryCountLine
	public static ZebraLabel printserviceLabelary2(Product product,InventoryCountLine inventoryCountLine) {
		ZebraLabel zebraLabel = new ZebraLabel(812, 203);
		zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);
		String nameProduct = product.getName();
		//System.out.println("nuevo labelary 2.....cha");	
		if (product.getName().length() >= 29){
		  zebraLabel.addElement(new ZebraText(10, 30,product.getName().substring(0, 28), 5));
		  //System.out.println("nuevo labelary 3.....cha");
		} else {
		  try {
		    zebraLabel.addElement(new ZebraText(10, 30,product.getName().substring(0, product.getName().length()-1), 5));
		    //System.out.println("nuevo labelary 4.....cha");
		  } catch (Exception e) {
		    //System.out.println("nuevo labelary 5.....cha");
		    System.out.println(e);
		  }
		}
		try {
		  if (nameProduct.length() >= 29){
		    zebraLabel.addElement(new ZebraText(10, 50,nameProduct.substring(0,28), 5));
		  } else{
		    zebraLabel.addElement(new ZebraText(10, 50,nameProduct.substring(0,nameProduct.length()-1), 5));
		  }
		  //System.out.println("nuevo labelary 6.....cha");
		} catch (Exception e) {
		  //System.out.println("nuevo labelary 7.....cha");
		  System.out.println(e);
		}
		//System.out.println("nuevo labelary 8.....cha");
		//zebraLabel.addElement(new ZebraText(300, 30,"CONDITION:",5));
		//System.out.println("nuevo labelary 8.1.....cha");
		try {
		   //System.out.println("nuevo labelary 8.2....cha");
		   if (product.getSnmtCondition() != null) {
		     //System.out.println("nuevo labelary 8.3....cha");
		     zebraLabel.addElement(new ZebraText(300, 130,product.getSnmtCondition(),20));
		     //System.out.println("nuevo labelary 8.4....cha");
		   }
		} catch (Exception e) {
		   //System.out.println("nuevo labelary 9.....cha");
		   System.out.println(e);
		}
		try{
		  zebraLabel.addElement(new ZebraText(500, 30,"Loc:"+inventoryCountLine.getStorageBin().getSearchKey(), 5));
		}catch (Exception e) {
		  //System.out.println("nuevo labelary 9.2....cha");
                  System.out.println(e);
		}
		zebraLabel.addElement(new ZebraText(500, 50,"WH:"+inventoryCountLine.getPhysInventory().getWarehouse().getName(), 5));
        
        /*try{
		  zebraLabel.addElement(new ZebraText(500, 70,"PT:"+inOutLine.getSalesOrderLine().getSalesOrder().getDocumentNo(), 5));
		} catch (Exception e){
		   //System.out.println("nuevo labelary 9.1....cha");
                   System.out.println(e);
		}*/	

        zebraLabel.addElement(new ZebraBarCode128(40, 140,product.getSearchKey(),60,false, 1,30));
		zebraLabel.addElement(new ZebraText(50, 160,product.getSearchKey(), 5));
		try {
		   if (inventoryCountLine.getAttributeSetValue().getDescription()==null){
		   	zebraLabel.addElement(new ZebraBarCode128(500, 140,"0000000000",60,false, 1,30));
		   }else {
			zebraLabel.addElement(new ZebraBarCode128(500, 140,inventoryCountLine.getAttributeSetValue().getDescription().substring(1),60,false, 1,30));
			zebraLabel.addElement(new ZebraText(500, 160,inventoryCountLine.getAttributeSetValue().getDescription().substring(1), 5));
		   }
		} catch (Exception e) {
		  //System.out.println("nuevo labelary 10.....cha");
		  zebraLabel.addElement(new ZebraBarCode128(500, 140,"0000000000",60,false, 1,30));
		  System.out.println(e);
		}
		//System.out.println("nuevo labelary 11.....cha");
		String string = zebraLabel.getZplCode();
		System.out.println(string);
		return zebraLabel;

	}




        public static ZebraLabel printserviceLabelary3(Product product) {
                ZebraLabel zebraLabel = new ZebraLabel(812, 203);
                zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);
                zebraLabel.addElement(new ZebraText(10, 20,product.getName(), 5));
                zebraLabel.addElement(new ZebraBarCode128(50, 110,product.getSearchKey(),60, 1,30));
                //zebraLabel.addElement(new ZebraText(10, 140,"Prueba de Impresion", 5));
                String string = zebraLabel.getZplCode();
                System.out.println(string);
                return zebraLabel;

        }



	public static String senddingImageToPrinter() throws Exception {
		String zplCode ="";
		Client current= OBContext.getOBContext().getCurrentClient();
		if(current.getClientInformationList()!=null &&!current.getClientInformationList().isEmpty() && current.getClientInformationList().get(0)!=null ){

			ClientInformation clientInformation	= current.getClientInformationList().get(0);
			clientInformation = OBDal.getInstance().get(ClientInformation.class, clientInformation.getId());
			Hibernate.initialize(clientInformation);
			org.openbravo.model.ad.utility.Image log= clientInformation.getYourCompanyBigImage();
			Hibernate.initialize(log);
			if(log!=null){


				byte[] image = log.getBindaryData();


				String string= StringUtils.getHexString(image);

				zplCode = "~DGR:LOGO.GRF,"+image.length+","+39+","+string;


				//zebraLabel.addElement(new ZebraNativeZpl("^KD0\n"));

			}
		}
		System.out.println(zplCode);
		return 	zplCode;
	}


	public static ZebraLabel printShippingLabels(ShipmentInOut inOut,String boxNumber) {

		ZebraLabel zebraLabel = new ZebraLabel(400,400 );

		zebraLabel.addElement(new ZebraNativeZpl("^FO1,410^GFA,2002,2002,22,K01gPFE,K07gQF8,K0CgQ0C,K0CR01MFQ0C,J018R01LFEQ06,J018T07IFER06,J018T03IF8R06,J018T01IFS06,J018U0FFEK0FF001I06,J0181KFI03IF80FFCJ0JF03I06,J01803IF8J07FF00FFCI07JFE3I06,J01800FFEK01FC00FFC001FE003FFI06,J018007FEK01F001FFC003FJ07FI06,J018003FEK01F001FF800FEJ03FI06,J018001FEK01E001FF801F8K0F8006,J018001FFK03C001FF803FL078006,J018I0FFK03C001FF807FL038006,J018I0FF8J078003FF00FEL038006,J018I07F8J078003FF00FCL018006,J018I07FCJ07I03FF01FCL018006,J018I03FCJ0FI03FF03F8M08006,J018I03FEJ0EI03FF03F8P06,J018I01FEI01EI07FF07F8P06,J018I01FFI01CI07FE07FQ06,J018J0FFI03CI07FE07FQ06,J018J0FF80038I07FE07FQ06,J018J07F80078I07FE0FFQ06,J018J07FC007J0FFE0FFK07IFE06,J018J03FC00FJ0FFC0FFK03IFC06,J018J03FE00EJ0FFC0FFL07FE006,J018J01FE01EJ0FFC0FFL03FC006,J018J01FF01CJ0FFC0FFL01FC006,J018K0FF03CI01FFC0FFL01F8006,J018K0FF838I01FF80FFL01F8006,J018K07F878I01FF807FL01F8006,J018K07FC7J01FF807F8K01F8006,J018K03FCFJ01FF807F8K01F8006,J018K03FEEJ03FF803FCK01F8006,J018K01FFEJ03FF003FCK01F8006,J018K01FFEJ03FF001FEK01F8006,J018L0FFCJ03FFI0FFK01F8006,J018L0FFCJ03FFI07FK01F8006,J018L07F8J03FFI03F8J01F8006,J018L07F8J07FEI01FEJ01F8006,J018L03FK07FEJ0FFJ03F8006,J018L03FK07FEJ07FCI07F8006,J018L01EK07FEJ01FF803FEI06,J018L01EK0FFEK03KFJ06,J018M0CK0FFEL07IF8J06,J018R01FFEU06,J018R03IFU06,J018R0JF8T06,J018Q0LFES06,K0CP01LFES0C,K0CgQ0C,K07gP038,K03gQF,,::::::::::L07FF381C7FF0F038E7FF9C0F3FF87FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF,L0780381C783CFC38E0781C0F3C1E78,:L0780381C7838FE38E0781C0F3C1E78,L07FE381C7878EF38E0781C0F3C3C7FF,L07FE381C7FF0E738E0781C0F3FF87FF,L07FE381C7FF0E3B8E0781C0F3FFC7FF,L0780381C7878E3F8E0781C0E3C3C78,L0780381C7838E1F8E0781C0E3C1C78,L07803C3C7838E1F8E0781E0E3C1E78,L07803FFC783CE0F8E0781FFE3C1E7FF8,L07801FF8783CE078E0780FFC3C0E7FF8,L07I0FE0701CE078E07803F83C0E7FF8,,:::::::^FS"));

		zebraLabel.addElement(new ZebraNativeZpl("^A2"));
		zebraLabel.addElement(new ZebraText(10, 65,inOut.getBusinessPartner().getName(), 15));

		zebraLabel.addElement(new ZebraGraficBox(10, 115, 280, 1, 3, "B"));
		zebraLabel.addElement(new ZebraNativeZpl("^A1"));
	    Location location=	inOut.getPartnerAddress().getLocationAddress();
	    location = (Location) OBDal.getInstance().getProxy(Location.ENTITY_NAME, location.getId());
		zebraLabel.addElement(new ZebraText(10, 165,"Address: "+location.getAddressLine1()+ (location.getCityName()!=null? ", "+location.getCityName():"") +(location.getRegion()!=null?", "+location.getRegion().getName():"") +(location.getPostalCode()!=null?", "+location.getPostalCode():"") +(location.getCountry()!=null?", "+location.getCountry().getName():""), 15));
		zebraLabel.addElement(new ZebraNativeZpl("^A1"));
		String contactName="";
		String orderReference="";
		String orderNumber="";
		if(inOut.getSalesOrder()!=null){
			Order order =inOut.getSalesOrder();
			if(order.getBusinessPartner()!=null && order.getBusinessPartner().getADUserList()!=null && !order.getBusinessPartner().getADUserList().isEmpty()){

				User user	=inOut.getSalesOrder().getBusinessPartner().getADUserList().get(0);

				StringBuilder builder = new  StringBuilder();
				builder.append(org.apache.commons.lang.StringUtils.isNotEmpty(user.getEmail())?"Email: "+user.getEmail() :null);
				builder.append(org.apache.commons.lang.StringUtils.isNotEmpty(user.getPhone())?"Phone: "+user.getPhone() :null);
				contactName=builder.toString().equalsIgnoreCase("null")?"":builder.toString();

			}

			if(org.apache.commons.lang.StringUtils.isNotEmpty(order.getDocumentNo()))	
				orderNumber=order.getDocumentNo();

			if(org.apache.commons.lang.StringUtils.isNotEmpty(order.getOrderReference()))	
				orderReference=order.getOrderReference();
		}


		zebraLabel.addElement(new ZebraText(10, 215,"Contact: "+contactName, 15));
		zebraLabel.addElement(new ZebraNativeZpl("^A1"));
		zebraLabel.addElement(new ZebraText(10, 265,"Sales Order Number: " +orderNumber, 15));

		zebraLabel.addElement(new ZebraNativeZpl("^A1"));
		zebraLabel.addElement(new ZebraText(10, 315,"Customer Reference: "+orderReference, 15));

	    zebraLabel.addElement(new ZebraNativeZpl("^A1"));
	    zebraLabel.addElement(new ZebraText(10, 365,"Box: "+boxNumber, 15));

		String string = zebraLabel.getZplCode();



		System.out.println(string);

		return zebraLabel;

	}





	public static ZebraLabel printPackingSlip_old(Packing packing,String boxNumber) {

		ZebraLabel zebraLabel = new ZebraLabel(400,400 );

		zebraLabel.addElement(new ZebraNativeZpl("^A2"));
		zebraLabel.addElement(new ZebraText(10, 65,packing.getBusinessPartner().getName(), 15));

		zebraLabel.addElement(new ZebraGraficBox(10, 115, 280, 1, 3, "B"));
		zebraLabel.addElement(new ZebraNativeZpl("^A1"));
	    	Location location=	packing.getPartnerAddress().getLocationAddress();
	    	location = (Location) OBDal.getInstance().getProxy(Location.ENTITY_NAME, location.getId());
		zebraLabel.addElement(new ZebraText(10, 165,"Address: "+
					location.getAddressLine1()+ 
					(location.getCityName()!=null? ", "+location.getCityName():"") +
					(location.getRegion()!=null?", "+location.getRegion().getName():"") +
					(location.getPostalCode()!=null?", "+location.getPostalCode():"") +
					(location.getCountry()!=null?", "+location.getCountry().getName():""), 15));
		zebraLabel.addElement(new ZebraNativeZpl("^A1"));
		String contactName="";
		String orderReference="";
		String orderNumber="";
		if(packing.getSvfpOrder()!=null){
			Order order = packing.getSvfpOrder();
			if(order.getBusinessPartner()!=null && order.getBusinessPartner().getADUserList()!=null && !order.getBusinessPartner().getADUserList().isEmpty()){

				User user	= packing.getSvfpOrder().getBusinessPartner().getADUserList().get(0);

				StringBuilder builder = new  StringBuilder();
				builder.append(org.apache.commons.lang.StringUtils.isNotEmpty(user.getEmail())?"Email: "+user.getEmail() :null);
				builder.append(org.apache.commons.lang.StringUtils.isNotEmpty(user.getPhone())?"Phone: "+user.getPhone() :null);
				contactName=builder.toString().equalsIgnoreCase("null")?"":builder.toString();

			}

			if(org.apache.commons.lang.StringUtils.isNotEmpty(order.getDocumentNo()))	
				orderNumber=order.getDocumentNo();

			if(org.apache.commons.lang.StringUtils.isNotEmpty(order.getOrderReference()))	
				orderReference=order.getOrderReference();
		}


		zebraLabel.addElement(new ZebraText(10, 215,"Contact: "+contactName, 15));
		zebraLabel.addElement(new ZebraNativeZpl("^A1"));
		zebraLabel.addElement(new ZebraText(10, 265,"Sales Order Number: " +orderNumber, 15));

		zebraLabel.addElement(new ZebraNativeZpl("^A1"));
		zebraLabel.addElement(new ZebraText(10, 315,"Customer Reference: "+orderReference, 15));

	    zebraLabel.addElement(new ZebraNativeZpl("^A1"));
	    zebraLabel.addElement(new ZebraText(10, 365,"Box: "+boxNumber, 15));

		String string = zebraLabel.getZplCode();



		System.out.println(string);

		return zebraLabel;

	}








	public static ZebraLabel printPackingSlip(Packing packing,String boxNumber) {

		ZebraLabel zebraLabel = new ZebraLabel(800,800);
		zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);
		String str_line1 = "";
		String str_line2 = "";
		String str_line3 = "";
		String str_city = "";
		String str_regionCode = "";
		String str_countryCode = "";
		String str_postalCode = "";
		String str_phone = "";
		String str_name = "";
		String str_shipname = "";
		String str_shiplastname = "";
		String str_shipmethod = "";
		try{str_line1 = packing.getSvfpOrder().getSnmtSalesChannel().getAddressLine1();}catch (Exception e){ System.out.println(e);}
		try{str_line2 = packing.getSvfpOrder().getSnmtSalesChannel().getAddressLine2();}catch (Exception e){ System.out.println(e);}
		try{str_line3 = packing.getSvfpOrder().getSnmtSalesChannel().getAddressLine3();}catch (Exception e){ System.out.println(e);}
		try{str_city = packing.getSvfpOrder().getSnmtSalesChannel().getCity();}catch (Exception e){ System.out.println(e);}
		try{str_regionCode = packing.getSvfpOrder().getSnmtSalesChannel().getRegionCode();}catch (Exception e){ System.out.println(e);}
		try{str_countryCode = packing.getSvfpOrder().getSnmtSalesChannel().getCountryCode();}catch (Exception e){ System.out.println(e);}
		try{str_postalCode = packing.getSvfpOrder().getSnmtSalesChannel().getPostalCode();}catch (Exception e){ System.out.println(e);}
		try{str_phone = packing.getSvfpOrder().getSnmtSalesChannel().getPhoneNumber();}catch (Exception e){ System.out.println(e);}
		try{str_name =  packing.getSvfpOrder().getSvfpaName();}catch (Exception e){ System.out.println(e);}
		try{str_shipname = packing.getSvfpOrder().getDlmgtoShippingFirstname();}catch (Exception e){ System.out.println(e);}
		try{str_shiplastname = packing.getSvfpOrder().getDlmgtoShippingLastname();}catch (Exception e){ System.out.println(e);}
		try{str_shipmethod = packing.getSvfpOrder().getDlmgtoShippingMethod();}catch (Exception e){ System.out.println(e);}
		
	    	Location location=	packing.getPartnerAddress().getLocationAddress();
	    	location = (Location) OBDal.getInstance().getProxy(Location.ENTITY_NAME, location.getId());
		
		Location location_bill=	packing.getSvfpOrder().getPartnerAddress().getLocationAddress();
    		location_bill = (Location) OBDal.getInstance().getProxy(Location.ENTITY_NAME, location_bill.getId());	
		
		String contactName="";
		String orderReference="";
		String orderNumber="";
		if(packing.getSvfpOrder()!=null){
			Order order = packing.getSvfpOrder();
			if(order.getBusinessPartner()!=null && order.getBusinessPartner().getADUserList()!=null && !order.getBusinessPartner().getADUserList().isEmpty()){

				User user	= packing.getSvfpOrder().getBusinessPartner().getADUserList().get(0);

				StringBuilder builder = new  StringBuilder();
				builder.append(org.apache.commons.lang.StringUtils.isNotEmpty(user.getEmail())?"Email: "+user.getEmail() :null);
				builder.append(org.apache.commons.lang.StringUtils.isNotEmpty(user.getPhone())?"Phone: "+user.getPhone() :null);
				contactName=builder.toString().equalsIgnoreCase("null")?"":builder.toString();

			}

			if(org.apache.commons.lang.StringUtils.isNotEmpty(order.getDocumentNo()))	
				orderNumber=order.getDocumentNo();

			if(org.apache.commons.lang.StringUtils.isNotEmpty(order.getOrderReference()))	
				orderReference=order.getOrderReference();
		}

	    zebraLabel.addElement(new ZebraNativeZpl("^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR4,4~SD15^JUS^LRN^CI0^XZ"));
	    zebraLabel.addElement(new ZebraNativeZpl("^XA"+
									"^MMT "+
									"^PW812 "+
									"^LL1218 "+
									"^LS0 "+
									"^FO324,20^GB41,1178,2^FS " +
									"^FO375,731^GB128,467,2^FS "+
									"^FO375,20^GB128,467,2^FS "+
									"^FO568,731^GB142,467,2^FS "+
									"^FO731,20^GB61,1177,4^FS "+
									"^FT750,426^A0R,42,55^FH\\^FDPacking Slip^FS "+
									"^FT676,63^A0R,34,38^FH\\^FDShipping Department^FS"));

		zebraLabel.addElement(new ZebraNativeZpl("^FT622,61^A0R,28,28^FH\\^FD"+str_line1+(str_line2!=null?str_line2:"")+(str_line3!=null?str_line3:"")+"^FS"));
		zebraLabel.addElement(new ZebraNativeZpl("^FT588,61^A0R,28,28^FH\\^FD"+str_city+","+str_regionCode+" "+str_postalCode+"^FS"));
	    zebraLabel.addElement(new ZebraNativeZpl("^FT554,61^A0R,28,28^FH\\^FD"+str_phone+"^FS"));
	    
	    Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		DateFormat formatter = new SimpleDateFormat("MMM dd yyyy");    
	    formatter.setTimeZone(TimeZone.getTimeZone("GMT-5")); 
	    String newYork = formatter.format(calendar.getTime());

		zebraLabel.addElement(new ZebraNativeZpl("^FT678,739^A0R,28,26^FH\\^FDOrder: "+orderNumber+"^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT644,739^A0R,28,26^FH\\^FDPlaced: "+newYork+"^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT610,739^A0R,28,26^FH\\^FDShip Method: "+str_shipmethod+"^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT576,739^A0R,28,26^FH\\^FDChannel ID:^FS"));

        zebraLabel.addElement(new ZebraNativeZpl("^FT480,41^A0R,28,28^FH\\^FD                   Ship To                  ^FS"));
        zebraLabel.addElement(new ZebraNativeZpl("^FT477,751^A0R,31,28^FH\\^FD                   Bill To                  ^FS"));
        zebraLabel.addElement(new ZebraNativeZpl("^FO466,732^GB0,463,1^FS"));
        zebraLabel.addElement(new ZebraNativeZpl("^FO466,20^GB0,468,1^FS"));



    	zebraLabel.addElement(new ZebraNativeZpl("^FT444,745^A0R,23,26^FH\\^FD"+(str_shipname!=null?str_shipname:"")+" "+(str_shiplastname!=null?str_shiplastname:"")+"^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT416,745^A0R,23,26^FH\\^FD"+location.getAddressLine1()+"^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT388,745^A0R,23,26^FH\\^FD"+(location.getCityName()!=null? ", "+location.getCityName():"") +
				(location.getRegion()!=null?", "+location.getRegion().getName():"") +
				(location.getPostalCode()!=null?", "+location.getPostalCode():"") +
				(location.getCountry()!=null?", "+location.getCountry().getName():"")+"^FS"));
				
    	
    	zebraLabel.addElement(new ZebraNativeZpl("^FT444,31^A0R,23,26^FH\\^FD"+(str_shipname!=null?str_shipname:"")+" "+(str_shiplastname!=null?str_shiplastname:"")+"^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT416,31^A0R,23,26^FH\\^FD"+location.getAddressLine1()+"^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT388,31^A0R,23,26^FH\\^FD"+(location.getCityName()!=null? ", "+location.getCityName():"") +
				(location.getRegion()!=null?", "+location.getRegion().getName():"") +
				(location.getPostalCode()!=null?", "+location.getPostalCode():"") +
				(location.getCountry()!=null?", "+location.getCountry().getName():"")+"^FS"));
	    
    	zebraLabel.addElement(new ZebraNativeZpl("^FT331,41^A0R,34,31^FH\\^FDItem SKU^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT331,183^A0R,34,31^FH\\^FDName^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT337,995^A0R,28,24^FH\\^FDQTY^FS"));
    	zebraLabel.addElement(new ZebraNativeZpl("^FT337,1096^A0R,28,28^FH\\^FDPrice^FS"));

	    int i = 275;
		for (ShipmentInOut shipmentInOut : packing.getMaterialMgmtShipmentInOutEMObwpackPackinghIDList()) {
			for (ShipmentInOutLine shipmentInOutLine : shipmentInOut.getMaterialMgmtShipmentInOutLineList()) {
				zebraLabel.addElement(new ZebraNativeZpl("^FT"+Integer.toString(i)+",20^A0R,33,19^FH\\^FD"+shipmentInOutLine.getProduct().getSearchKey()+"^FS"));
				zebraLabel.addElement(new ZebraNativeZpl("^FT"+Integer.toString(i)+",183^A0R,33,19^FH\\^FD"+shipmentInOutLine.getProduct().getName()+"^FS"));
				zebraLabel.addElement(new ZebraNativeZpl("^FT"+Integer.toString(i)+",995^A0R,32,31^FH\\^FD"+shipmentInOutLine.getMovementQuantity().toString()+"^FS"));
				try{
					zebraLabel.addElement(new ZebraNativeZpl("^FT"+Integer.toString(i)+",1096^A0R,32,28^FH\\^FD"+shipmentInOutLine.getSalesOrderLine().getUnitPrice().toString()+"^FS"));
				} catch (Exception e){
					zebraLabel.addElement(new ZebraNativeZpl("^FT"+Integer.toString(i)+",1096^A0R,32,28^FH\\^FD0^FS"));
				}
				i = i-20;
			}
		}   	    
	    
		String string = zebraLabel.getZplCode();
		
		System.out.println(string);

		return zebraLabel;

	}














	
	public static void printLabelRestClient(List<Label> labels) throws Exception {
		
		
		
		Organization organization = OBContext.getOBContext().getCurrentOrganization();
		organization = (Organization) OBDal.getInstance().getProxy(Organization.ENTITY_NAME, organization.getId());
		com.sun.jersey.api.client.Client client = com.sun.jersey.api.client.Client.create();



		// Era Product ssppPrtProd
		String hql = "SELECT f.url,f.name FROM SSPP_User_Prts e, SSPP_Printers f WHERE e.ssppPrtProd = f.id AND e.user = '" + OBContext.getOBContext().getUser().getId()+"'";

                List<String> userList = (List<String>) OBDal.getInstance().getSession().createQuery(hql).list();


                Iterator i = userList.iterator();

                String nombre = new String();
                String urlbase = new String();
                while(i.hasNext()){
                           Object[] obj = (Object[]) i.next();
                           //now you have one array of Object for each row
                           System.out.println("Primer 0");
                           System.out.println(String.valueOf(obj[0]));
                           urlbase = String.valueOf(obj[0]);
                           //Integer service = Integer.parseInt(String.valueOf(obj[1])); //SERVICE assumed as int
                           System.out.println("Segundo 0");
                           System.out.println(String.valueOf(obj[1]));
                           nombre = String.valueOf(obj[1]);
                           //same way for all obj[2], obj[3], obj[4]
                }


                System.out.println("Print ssppuserprts.....");


                System.out.println("Pasa por aqui...1.1");
                if( org.apache.commons.lang.StringUtils.isEmpty(urlbase))
                                throw new OBException(OBMessageUtils.messageBD("SVFZE_no_printer for User in ") +" " + organization.getName());
                //String url = organization.getSvfzePrinter2WsUrl()+"/ws/print_zebra_label/printLabel"+ (org.apache.commons.lang.StringUtils.isNotEmpty(organization.getSvfzePrinter2Name())?"/"+URLEncoder.encode(organization.getSvfzePrinter2Name(),"UTF-8"):"") ;
                //String url = urlbase+"/ws/print_zebra_label/printLabel"+ (org.apache.commons.lang.StringUtils.isNotEmpty(nombre)?"/"+URLEncoder.encode(nombre,"UTF-8"):"") ;
		String url = urlbase+ (org.apache.commons.lang.StringUtils.isNotEmpty(nombre)?""+URLEncoder.encode(nombre,"UTF-8"):"") ;
		System.out.println("Url.. largo..Rest1");
		System.out.println(url);
		System.out.println("Pasa por aqui...2");
		WebResource webResource = client.resource(url);
		Gson gson = new Gson();
        	String input = gson.toJson(labels,  new TypeToken<List<Label>>(){}.getType());

	

		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, input);

		if (response.getStatus() != 200) {
			System.out.println("Pasa por aqui...3");
			throw new OBException(response.getEntity(String.class));
		}
		
	}


        public static void printLabelRestClient2(List<Label> labels) throws Exception {



                Organization organization = OBContext.getOBContext().getCurrentOrganization();
                organization = (Organization) OBDal.getInstance().getProxy(Organization.ENTITY_NAME, organization.getId());

		//Era shipping ssppPrtShipp
		String hql = "SELECT f.url,f.name FROM SSPP_User_Prts e, SSPP_Printers f WHERE e.ssppPrtLabel = f.id AND e.user = '" + OBContext.getOBContext().getUser().getId()+"'";
                
		List<String> userList = (List<String>) OBDal.getInstance().getSession().createQuery(hql).list();
		
		
		Iterator i = userList.iterator();
                
		String nombre = new String();
		String urlbase = new String();
		while(i.hasNext()){
                	   Object[] obj = (Object[]) i.next();
                	   //now you have one array of Object for each row
			   System.out.println("Primer 0");
			   System.out.println(String.valueOf(obj[0]));
			   urlbase = String.valueOf(obj[0]);
			   //Integer service = Integer.parseInt(String.valueOf(obj[1])); //SERVICE assumed as int
			   System.out.println("Segundo 0");
			   System.out.println(String.valueOf(obj[1]));
			   nombre = String.valueOf(obj[1]);
                	   //same way for all obj[2], obj[3], obj[4]
                }

                 
		System.out.println("Print ssppuserprts.....");

                com.sun.jersey.api.client.Client client = com.sun.jersey.api.client.Client.create();

                System.out.println("Pasa por aqui...1.2");
                if( org.apache.commons.lang.StringUtils.isEmpty(urlbase))
                                throw new OBException(OBMessageUtils.messageBD("SVFZE_no_printer for User in ") +" " + organization.getName());
		//String url = organization.getSvfzePrinter2WsUrl()+"/ws/print_zebra_label/printLabel"+ (org.apache.commons.lang.StringUtils.isNotEmpty(organization.getSvfzePrinter2Name())?"/"+URLEncoder.encode(organization.getSvfzePrinter2Name(),"UTF-8"):"") ;
		//String url = urlbase+"/ws/print_zebra_label/printLabel"+ (org.apache.commons.lang.StringUtils.isNotEmpty(nombre)?"/"+URLEncoder.encode(nombre,"UTF-8"):"") ;
                String url = urlbase+ (org.apache.commons.lang.StringUtils.isNotEmpty(nombre)?""+URLEncoder.encode(nombre,"UTF-8"):"") ;
		System.out.println("El url largo .... rest 2");
		System.out.println(url);

                System.out.println("Pasa por aqui...2");
                WebResource webResource = client.resource(url);
                Gson gson = new Gson();
                String input = gson.toJson(labels,  new TypeToken<List<Label>>(){}.getType());



                ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
                                .post(ClientResponse.class, input);

                if (response.getStatus() != 200) {
                        System.out.println("Pasa por aqui...3");
                        throw new OBException(response.getEntity(String.class));
                }

        }



        public static void printLabelRestClient3(List<Label> labels) throws Exception {



                Organization organization = OBContext.getOBContext().getCurrentOrganization();
                organization = (Organization) OBDal.getInstance().getProxy(Organization.ENTITY_NAME, organization.getId());
		
		//era Packing ssppPrtLabel
                String hql = "SELECT f.url,f.name FROM SSPP_User_Prts e, SSPP_Printers f WHERE e.ssppPrtShipp = f.id AND e.user = '" + OBContext.getOBContext().getUser().getId()+"'";

                List<String> userList = (List<String>) OBDal.getInstance().getSession().createQuery(hql).list();


                Iterator i = userList.iterator();

                String nombre = new String();
                String urlbase = new String();
                while(i.hasNext()){
                           Object[] obj = (Object[]) i.next();
                           //now you have one array of Object for each row
                           System.out.println("Primer 0");
                           System.out.println(String.valueOf(obj[0]));
                           urlbase = String.valueOf(obj[0]);
                           //Integer service = Integer.parseInt(String.valueOf(obj[1])); //SERVICE assumed as int
                           System.out.println("Segundo 0");
                           System.out.println(String.valueOf(obj[1]));
                           nombre = String.valueOf(obj[1]);
                           //same way for all obj[2], obj[3], obj[4]
                }

                System.out.println("Print ssppuserprts.....");

                com.sun.jersey.api.client.Client client = com.sun.jersey.api.client.Client.create();

                System.out.println("Pasa por aqui...1.3");
                if( org.apache.commons.lang.StringUtils.isEmpty(urlbase))
                                throw new OBException(OBMessageUtils.messageBD("SVFZE_no_printer for User in ") +" " + organization.getName());
                //String url = organization.getSvfzePrinter2WsUrl()+"/ws/print_zebra_label/printLabel"+ (org.apache.commons.lang.StringUtils.isNotEmpty(organization.getSvfzePrinter2Name())?"/"+URLEncoder.encode(organization.getSvfzePrinter2Name(),"UTF-8"):"") ;
		//String url = urlbase+"/ws/print_zebra_label/printLabel"+ (org.apache.commons.lang.StringUtils.isNotEmpty(nombre)?"/"+URLEncoder.encode(nombre,"UTF-8"):"") ;
                String url = urlbase+ (org.apache.commons.lang.StringUtils.isNotEmpty(nombre)?""+URLEncoder.encode(nombre,"UTF-8"):"") ;
                System.out.println("El url largo....rest 3");
                System.out.println(url);

                System.out.println("Pasa por aqui...2");
                WebResource webResource = client.resource(url);
                Gson gson = new Gson();
                String input = gson.toJson(labels,  new TypeToken<List<Label>>(){}.getType());



                ClientResponse response = webResource.type(MediaType.APPLICATION_JSON)
                                .post(ClientResponse.class, input);

                if (response.getStatus() != 200) {
                        System.out.println("Pasa por aqui...3");
                        throw new OBException(response.getEntity(String.class));
                }

        }



}
