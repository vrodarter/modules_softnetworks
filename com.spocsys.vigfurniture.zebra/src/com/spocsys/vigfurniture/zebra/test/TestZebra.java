package com.spocsys.vigfurniture.zebra.test;

import org.openbravo.model.common.plm.Product;

import fr.w3blog.zpl.constant.ZebraFont;
import fr.w3blog.zpl.model.ZebraLabel;
import fr.w3blog.zpl.model.element.ZebraBarCode128;
import fr.w3blog.zpl.model.element.ZebraGraficBox;
import fr.w3blog.zpl.model.element.ZebraNativeZpl;
import fr.w3blog.zpl.model.element.ZebraText;

public class TestZebra {

	public static void main(String[] args) {
		printserviceLabelaryLabelDescriptionFile();
	}
public static ZebraLabel printserviceLabelary(Product product) {
		
	
		
		ZebraLabel zebraLabel = new ZebraLabel(900, 750);
	    zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);

	    zebraLabel.addElement(new ZebraText(50, 65,product.getSearchKey(), 12));
	    
	    zebraLabel.addElement(new ZebraBarCode128(550, 75,product.getSearchKey(),50, true, true));
	    
	    zebraLabel.addElement(new ZebraGraficBox(10, 100, 860, 1, 3, "B"));
	    
	    zebraLabel.addElement(new ZebraText(10, 125, "Model Number: "+product.getSvfsvModel(), 5));
	    
	    zebraLabel.addElement(new ZebraText(10, 145, "Description: "+product.getDescription(), 5));
	    
	    zebraLabel.addElement(new ZebraGraficBox(10, 200, 860, 1, 3, "B"));
	    
	    zebraLabel.addElement(new ZebraText(10, 225, "Additional Information:", 5));
	    //Sales Orders
	    zebraLabel.addElement(new ZebraText(550, 225, "Rsvd For:"/*product.getSearchKey()*/, 5));
	    
	    
	  
	    zebraLabel.addElement(new ZebraText(10, 245, "GRADE:"/*product.getSearchKey()*/, 5));
	    zebraLabel.addElement(new ZebraText(550, 245, "Rsvd Value"/*product.getSearchKey()*/, 5));
	    
	    zebraLabel.addElement(new ZebraText(10, 265, "Color:"/*product.getSearchKey()*/, 5));
	    zebraLabel.addElement(new ZebraText(10, 285, "Configuration:"/*product.getSearchKey()*/, 5));
	    
	    zebraLabel.addElement(new ZebraText(10, 305, "S/O: "/*product.getSearchKey()*/, 5));
	    
	    zebraLabel.addElement(new ZebraText(550, 305, "P/O:"/*product.getSearchKey()*/, 5));
	    
	    zebraLabel.addElement(new ZebraGraficBox(10, 335, 860, 1, 3, "B"));
	    if(product.getProductCategory()!=null)
	    zebraLabel.addElement(new ZebraText(10, 360, "Category: "+product.getProductCategory().getName(), 5));
	    if(product.getBrand()!=null)
	    zebraLabel.addElement(new ZebraText(575, 360, product.getBrand().getName(), 5));
       
	    zebraLabel.addElement(new ZebraText(10, 380, product.getSearchKey(), 5));
	   
	    zebraLabel.addElement(new ZebraBarCode128(550, 450,  product.getSearchKey(),50, true, true));
	    
	    
	    zebraLabel.addElement(new ZebraText(10, 420, "Description", 5));
	    
	    zebraLabel.addElement(new ZebraText(400, 470, "Divani Model"/*product.getSearchKey()*/, 5));
	    
	    zebraLabel.addElement(new ZebraText(335, 490, "Recieved in:"/*product.getSearchKey()*/, 5));
	    zebraLabel.addElement(new ZebraText(525, 490, "on:"/*product.getSearchKey()*/, 5));
	    String string = zebraLabel.getZplCode();
	    

	    
	    System.out.println(string);
		return zebraLabel;
	
	
	}
public static ZebraLabel printserviceZPLViewerFormatoSO(Product product) {
	
	
	
	ZebraLabel zebraLabel = new ZebraLabel(600, 450);
    zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);

    zebraLabel.addElement(new ZebraText(35, 15,product.getSearchKey(), 9));
    
    zebraLabel.addElement(new ZebraBarCode128(350, 15, product.getSearchKey(),50, true, true));
    
    zebraLabel.addElement(new ZebraGraficBox(10, 70, 560, 1, 3, "B"));
    
    zebraLabel.addElement(new ZebraText(10, 75, "Model Number: "+product.getSvfsvModel(), 5));
    
    zebraLabel.addElement(new ZebraText(10, 95, "Description: "+product.getDescription(), 5));
    
    zebraLabel.addElement(new ZebraGraficBox(10, 150, 560, 1, 3, "B"));
    
    zebraLabel.addElement(new ZebraText(10, 155, "Additional Information:", 5));
    //Sales Orders
    zebraLabel.addElement(new ZebraText(350, 155, "Rsvd For:"/*product.getSearchKey()*/, 5));
    
    
  
    zebraLabel.addElement(new ZebraText(10, 175, "GRADE:"/*product.getSearchKey()*/, 5));
    zebraLabel.addElement(new ZebraText(350, 175, "Rsvd Value"/*product.getSearchKey()*/, 5));
    
    zebraLabel.addElement(new ZebraText(10, 195, "Color:"/*product.getSearchKey()*/, 5));
    zebraLabel.addElement(new ZebraText(10, 215, "Configuration:"/*product.getSearchKey()*/, 5));
    
    zebraLabel.addElement(new ZebraText(10, 235, "S/O: "/*product.getSearchKey()*/, 5));
    
    zebraLabel.addElement(new ZebraText(350, 235, "P/O:"/*product.getSearchKey()*/, 5));
    
    zebraLabel.addElement(new ZebraGraficBox(10, 265, 560, 1, 3, "B"));
    
    if(product.getProductCategory()!=null)
    zebraLabel.addElement(new ZebraText(10, 270, "Category: "+product.getProductCategory().getName(), 5));
    if(product.getBrand()!=null)
    zebraLabel.addElement(new ZebraText(375, 270,/* "Living Room"*/product.getBrand().getName(), 5));
   
    zebraLabel.addElement(new ZebraText(10, 290, product.getSearchKey(), 5));
   
    zebraLabel.addElement(new ZebraBarCode128(350, 300,  product.getSearchKey(),50, true, true));
    
    
    zebraLabel.addElement(new ZebraText(10, 310, product.getDescription(), 5));
    
    zebraLabel.addElement(new ZebraText(225, 350, "Divani Model"/*product.getSearchKey()*/, 5));
    
    zebraLabel.addElement(new ZebraText(175, 370, "Recieved in:"/*product.getSearchKey()*/, 5));
    zebraLabel.addElement(new ZebraText(325, 370, "on:"/*product.getSearchKey()*/, 5));
    zebraLabel.addElement(new ZebraNativeZpl("^FO400,75^XGR:LOGO.GRF,1,1^FS"));
    String string = zebraLabel.getZplCode();
    

    
    System.out.println(string);
	return zebraLabel;


}

public static ZebraLabel printserviceLabelaryLabelDescriptionFile() {
	
	
	
	ZebraLabel zebraLabel = new ZebraLabel(900, 750);
    zebraLabel.setDefaultZebraFont(ZebraFont.ZEBRA_ZERO);
   zebraLabel.addElement(new ZebraNativeZpl("^FO10,460^GFA,2002,2002,22,K01gPFE,K07gQF8,K0CgQ0C,K0CR01MFQ0C,J018R01LFEQ06,J018T07IFER06,J018T03IF8R06,J018T01IFS06,J018U0FFEK0FF001I06,J0181KFI03IF80FFCJ0JF03I06,J01803IF8J07FF00FFCI07JFE3I06,J01800FFEK01FC00FFC001FE003FFI06,J018007FEK01F001FFC003FJ07FI06,J018003FEK01F001FF800FEJ03FI06,J018001FEK01E001FF801F8K0F8006,J018001FFK03C001FF803FL078006,J018I0FFK03C001FF807FL038006,J018I0FF8J078003FF00FEL038006,J018I07F8J078003FF00FCL018006,J018I07FCJ07I03FF01FCL018006,J018I03FCJ0FI03FF03F8M08006,J018I03FEJ0EI03FF03F8P06,J018I01FEI01EI07FF07F8P06,J018I01FFI01CI07FE07FQ06,J018J0FFI03CI07FE07FQ06,J018J0FF80038I07FE07FQ06,J018J07F80078I07FE0FFQ06,J018J07FC007J0FFE0FFK07IFE06,J018J03FC00FJ0FFC0FFK03IFC06,J018J03FE00EJ0FFC0FFL07FE006,J018J01FE01EJ0FFC0FFL03FC006,J018J01FF01CJ0FFC0FFL01FC006,J018K0FF03CI01FFC0FFL01F8006,J018K0FF838I01FF80FFL01F8006,J018K07F878I01FF807FL01F8006,J018K07FC7J01FF807F8K01F8006,J018K03FCFJ01FF807F8K01F8006,J018K03FEEJ03FF803FCK01F8006,J018K01FFEJ03FF003FCK01F8006,J018K01FFEJ03FF001FEK01F8006,J018L0FFCJ03FFI0FFK01F8006,J018L0FFCJ03FFI07FK01F8006,J018L07F8J03FFI03F8J01F8006,J018L07F8J07FEI01FEJ01F8006,J018L03FK07FEJ0FFJ03F8006,J018L03FK07FEJ07FCI07F8006,J018L01EK07FEJ01FF803FEI06,J018L01EK0FFEK03KFJ06,J018M0CK0FFEL07IF8J06,J018R01FFEU06,J018R03IFU06,J018R0JF8T06,J018Q0LFES06,K0CP01LFES0C,K0CgQ0C,K07gP038,K03gQF,,::::::::::L07FF381C7FF0F038E7FF9C0F3FF87FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF,L0780381C783CFC38E0781C0F3C1E78,:L0780381C7838FE38E0781C0F3C1E78,L07FE381C7878EF38E0781C0F3C3C7FF,L07FE381C7FF0E738E0781C0F3FF87FF,L07FE381C7FF0E3B8E0781C0F3FFC7FF,L0780381C7878E3F8E0781C0E3C3C78,L0780381C7838E1F8E0781C0E3C1C78,L07803C3C7838E1F8E0781E0E3C1E78,L07803FFC783CE0F8E0781FFE3C1E7FF8,L07801FF8783CE078E0780FFC3C0E7FF8,L07I0FE0701CE078E07803F83C0E7FF8,,:::::::^FS"));
    
    zebraLabel.addElement(new ZebraText(50, 65,"product.getSearchKey()", 13));
    
    zebraLabel.addElement(new ZebraBarCode128(550, 75,"product.getSearchKey()",60, 1,30));
    
    zebraLabel.addElement(new ZebraGraficBox(10, 120, 860, 1, 3, "B"));
    
    // A partir de aca el choriso
    
    zebraLabel.addElement(new ZebraText(10, 145, "Model Number: "+"product.getSvfsvModel()", 5));
    
    zebraLabel.addElement(new ZebraText(10, 175, "Description: "+"product.getDescription()", 5));
    
zebraLabel.addElement(new ZebraText(550, 145, "S/O: "/*product.getSearchKey()*/, 5));
    
    zebraLabel.addElement(new ZebraText(550, 175, "P/O:"/*product.getSearchKey()*/, 5));
    
    zebraLabel.addElement(new ZebraGraficBox(10, 240, 860, 1, 3, "B"));
    
    // Lo de arriba esta OK!!!!
    
    zebraLabel.addElement(new ZebraText(10, 265, "Additional Information:", 5));
    //Sales Orders
   
    zebraLabel.addElement(new ZebraText(595, 300, "UPC Code:", 5));
    
  
//    zebraLabel.addElement(new ZebraText(10, 245, "GRADE:"/*product.getSearchKey()*/, 5));
//    zebraLabel.addElement(new ZebraText(550, 245, "Rsvd Value"/*product.getSearchKey()*/, 5));
//    
//    zebraLabel.addElement(new ZebraText(10, 265, "Color:"/*product.getSearchKey()*/, 5));
//    zebraLabel.addElement(new ZebraText(10, 285, "Configuration:"/*product.getSearchKey()*/, 5));
    
    
    
    zebraLabel.addElement(new ZebraGraficBox(10, 330, 860, 1, 3, "B"));
    
    zebraLabel.addElement(new ZebraText(10, 350, "Rsvd For:"/*product.getSearchKey()*/, 5));

    zebraLabel.addElement(new ZebraText(10, 380, "Category: "+"product.getProductCategory().getName()", 5));
    
    zebraLabel.addElement(new ZebraText(10, 410, "Description: "+"product.getDescription()", 5));
    
    //Hasta aca
   
    zebraLabel.addElement(new ZebraText(590, 420, "product.getBrand().getName()", 8));
    zebraLabel.addElement(new ZebraText(570, 450, "product.getProductCategory().getName()", 5));
  //  zebraLabel.addElement(new ZebraText(10, 380, "product.getSearchKey()", 5));
    
    
   
    zebraLabel.addElement(new ZebraBarCode128(280, 520,  "product.getSearchKey()",60, 1,30));
    
    
   // zebraLabel.addElement(new ZebraText(10, 420, "Description", 5));
    
   // zebraLabel.addElement(new ZebraText(400, 470, "Divani Model"/*product.getSearchKey()*/, 5));
    
    zebraLabel.addElement(new ZebraText(600, 490, "Recieved in:"/*product.getSearchKey()*/, 5));
    zebraLabel.addElement(new ZebraText(600, 520, "on:"/*product.getSearchKey()*/, 5));
    String string = zebraLabel.getZplCode();
    

    
    System.out.println(string);
	return zebraLabel;


}

private static void printShippingLabels() {
	
	ZebraLabel zebraLabel = new ZebraLabel(400,400 );
	
	 zebraLabel.addElement(new ZebraNativeZpl("^FO1,415^GFA,2002,2002,22,K01gPFE,K07gQF8,K0CgQ0C,K0CR01MFQ0C,J018R01LFEQ06,J018T07IFER06,J018T03IF8R06,J018T01IFS06,J018U0FFEK0FF001I06,J0181KFI03IF80FFCJ0JF03I06,J01803IF8J07FF00FFCI07JFE3I06,J01800FFEK01FC00FFC001FE003FFI06,J018007FEK01F001FFC003FJ07FI06,J018003FEK01F001FF800FEJ03FI06,J018001FEK01E001FF801F8K0F8006,J018001FFK03C001FF803FL078006,J018I0FFK03C001FF807FL038006,J018I0FF8J078003FF00FEL038006,J018I07F8J078003FF00FCL018006,J018I07FCJ07I03FF01FCL018006,J018I03FCJ0FI03FF03F8M08006,J018I03FEJ0EI03FF03F8P06,J018I01FEI01EI07FF07F8P06,J018I01FFI01CI07FE07FQ06,J018J0FFI03CI07FE07FQ06,J018J0FF80038I07FE07FQ06,J018J07F80078I07FE0FFQ06,J018J07FC007J0FFE0FFK07IFE06,J018J03FC00FJ0FFC0FFK03IFC06,J018J03FE00EJ0FFC0FFL07FE006,J018J01FE01EJ0FFC0FFL03FC006,J018J01FF01CJ0FFC0FFL01FC006,J018K0FF03CI01FFC0FFL01F8006,J018K0FF838I01FF80FFL01F8006,J018K07F878I01FF807FL01F8006,J018K07FC7J01FF807F8K01F8006,J018K03FCFJ01FF807F8K01F8006,J018K03FEEJ03FF803FCK01F8006,J018K01FFEJ03FF003FCK01F8006,J018K01FFEJ03FF001FEK01F8006,J018L0FFCJ03FFI0FFK01F8006,J018L0FFCJ03FFI07FK01F8006,J018L07F8J03FFI03F8J01F8006,J018L07F8J07FEI01FEJ01F8006,J018L03FK07FEJ0FFJ03F8006,J018L03FK07FEJ07FCI07F8006,J018L01EK07FEJ01FF803FEI06,J018L01EK0FFEK03KFJ06,J018M0CK0FFEL07IF8J06,J018R01FFEU06,J018R03IFU06,J018R0JF8T06,J018Q0LFES06,K0CP01LFES0C,K0CgQ0C,K07gP038,K03gQF,,::::::::::L07FF381C7FF0F038E7FF9C0F3FF87FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF8,L07FF381C7FF8F838E7FF9C0F3FFC7FF,L0780381C783CFC38E0781C0F3C1E78,:L0780381C7838FE38E0781C0F3C1E78,L07FE381C7878EF38E0781C0F3C3C7FF,L07FE381C7FF0E738E0781C0F3FF87FF,L07FE381C7FF0E3B8E0781C0F3FFC7FF,L0780381C7878E3F8E0781C0E3C3C78,L0780381C7838E1F8E0781C0E3C1C78,L07803C3C7838E1F8E0781E0E3C1E78,L07803FFC783CE0F8E0781FFE3C1E7FF8,L07801FF8783CE078E0780FFC3C0E7FF8,L07I0FE0701CE078E07803F83C0E7FF8,,:::::::^FS"));
	 
	 zebraLabel.addElement(new ZebraNativeZpl("^A2"));
	zebraLabel.addElement(new ZebraText(10, 65,"Name of the customer", 15));
    
    zebraLabel.addElement(new ZebraGraficBox(10, 115, 280, 1, 3, "B"));
    zebraLabel.addElement(new ZebraNativeZpl("^A1"));
    zebraLabel.addElement(new ZebraText(10, 165,"Shipping address", 15));
    zebraLabel.addElement(new ZebraNativeZpl("^A1"));
    zebraLabel.addElement(new ZebraText(10, 215,"Contact name ", 15));
    zebraLabel.addElement(new ZebraNativeZpl("^A1"));
    zebraLabel.addElement(new ZebraText(10, 265,"Sales Order number", 15));
    
    
    zebraLabel.addElement(new ZebraNativeZpl("^A1"));
    zebraLabel.addElement(new ZebraText(10, 315,"Customer Reference number", 15));
    
    zebraLabel.addElement(new ZebraNativeZpl("^A1"));
    zebraLabel.addElement(new ZebraText(10, 365,"BOX: ", 15));
    
    String string = zebraLabel.getZplCode();
    

    
    System.out.println(string);

}
}
