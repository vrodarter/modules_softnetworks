/**
 * 
 */

//Define a class that extends OBPopup
isc.defineClass('SVFZE_PrintShipment', isc.OBPopup);

isc.SVFZE_PrintShipment.addProperties({
	width: 320,
	height: 250,
	title: null,
	showMinimizeButton: false,
	showMaximizeButton: false,

	view: null,
	params: null,
	actionHandler: null,
	orders: null,

	mainform: null,
	okButton: null,
	cancelButton: null,

	initWidget: function () {

		// Form that contains the parameters
		this.mainform = isc.DynamicForm.create({
			numCols: 1,
			fields: [{
				name: 'Quantity',
				title: OB.I18N.getLabel('SVFZE_ShipmentPrint.Qty_Title'),
				height: 20,
				width: 190,
				type: '_id_11' //Date reference
			}]
		});

		// OK Button
		this.okButton = isc.OBFormButton.create({
			title: OB.I18N.getLabel('SVFZE_ShipmentPrint.OK_BUTTON_TITLE'),
			popup: this,
			action: function () {
				callback = function (rpcResponse, data, rpcRequest) {
					var status = rpcResponse.status,
					context = rpcRequest.clientContext,
					view = context.originalView.getProcessOwnerView(context.popup.params.processId);
					if (data.message) {
						view.messageBar.setMessage(data.message.severity, data.message.title, data.message.text);

					}
					rpcRequest.clientContext.popup.closeClick();
					rpcRequest.clientContext.originalView.refresh(false, false);
				};

				OB.RemoteCallManager.call(this.popup.actionHandler, {
					shipmentHeaders: this.popup.shipmentHeaders,
					action: this.popup.params.action,
					qtyParam: this.popup.mainform.getField('Quantity').getValue(), //send the parameter to the server too
				}, {}, callback, {popup: this.popup}); 
			}
		});

		// Cancel Button
		this.cancelButton = isc.OBFormButton.create({
			title: OB.I18N.getLabel('SVFZE_ShipmentPrint.CANCEL_BUTTON_TITLE'),
			popup: this,
			action: function () {
				this.popup.closeClick();
			}
		}); 

		//Add the elements into a layout   
		this.items = [
		              isc.VLayout.create({
		            	  defaultLayoutAlign: "center",
		            	  align: "center",
		            	  width: "100%",
		            	  layoutMargin: 10,
		            	  membersMargin: 6,
		            	  members: [
		            	            isc.HLayout.create({
		            	            	defaultLayoutAlign: "center",
		            	            	align: "center",
		            	            	layoutMargin: 30,
		            	            	membersMargin: 6,
		            	            	members: this.mainform
		            	            }), 
		            	            isc.HLayout.create({
		            	            	defaultLayoutAlign: "center",
		            	            	align: "center",
		            	            	membersMargin: 10,
		            	            	members: [this.okButton, this.cancelButton]
		            	            })
		            	            ]
		              })
		              ];

		this.Super('initWidget', arguments);
	}

});