/************************************************************************************
 * Copyright (C) 2012-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
OB.SVFZE = OB.SVFZE || {};
OB.SVFZE.Process = {
  process: function (params, view) {
    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
        shipmentHeaders = [],
        callback;
    callback = function (rpcResponse, data, rpcRequest) {
      var message = data.message;
      view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
      // close process to refresh current view
      params.button.closeProcessPopup();
    };
    for (i = 0; i < selection.length; i++) {
    	shipmentHeaders.push(selection[i].id);
    }
    isc.confirm(OB.I18N.getLabel('SVFZE_Cofirm'), function (clickedOK) {
      if (clickedOK) {
        OB.RemoteCallManager.call('com.spocsys.vigfurniture.zebra.handlers.PrintShippingLabelsProcess', {
        	shipmentHeaders: shipmentHeaders,
          action: 'process'
        }, {}, callback);
      }
    });
  },
  process2: function (params, view) {
    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
        shipmentHeaders = [],
        callback;
    callback = function (rpcResponse, data, rpcRequest) {
      var message = data.message;
      view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
      // close process to refresh current view
      params.button.closeProcessPopup();
    };
    for (i = 0; i < selection.length; i++) {
        shipmentHeaders.push(selection[i].id);
    }
    isc.confirm(OB.I18N.getLabel('SVFZE_Cofirm'), function (clickedOK) {
      if (clickedOK) {
        OB.RemoteCallManager.call('com.spocsys.vigfurniture.zebra.handlers.PrintShippingLabelsProcess2', {
                shipmentHeaders: shipmentHeaders,
          action: 'process'
        }, {}, callback);
      }
    });
  },
  execute: function (params, view) {
	    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
	    shipmentHeaders = [],
	        callback;
	 
	    for (i = 0; i < selection.length; i++) {
	    	shipmentHeaders.push(selection[i][OB.Constants.ID]);
	    }
	    
	    // Create the PopUp
	    isc.SVFZE_PrintShipment.create({
	    	shipmentHeaders: shipmentHeaders,
	      view: view,
	      params: params,
	      actionHandler: 'com.spocsys.vigfurniture.zebra.handlers.PrintShippingLabelsProcess'
	    }).show();
	  },
	  
	  printFromGR: function (params, view) {
		    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
		    shipmentLine = [],
		        callback;
		    callback = function (rpcResponse, data, rpcRequest) {
		      var message = data.message;
		      view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
		      // close process to refresh current view
		      params.button.closeProcessPopup();
		    };
		    for (i = 0; i < selection.length; i++) {
		    	shipmentLine.push(selection[i].id);
		    }
		    isc.confirm(OB.I18N.getLabel('SVFZE_Cofirm'), function (clickedOK) {
		      if (clickedOK) {
		        OB.RemoteCallManager.call('com.spocsys.vigfurniture.zebra.handlers.PrintProductLabelsProcess', {
		        	shipmentLine: shipmentLine,
		          action: 'process'
		        }, {}, callback);
		      }
		    });
		  },

    printFromIn: function (params, view) {
        var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
        shipmentLine = [],
            callback;
        callback = function (rpcResponse, data, rpcRequest) {
          var message = data.message;
          view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
          // close process to refresh current view
          params.button.closeProcessPopup();
        };
        for (i = 0; i < selection.length; i++) {
          shipmentLine.push(selection[i].id);
        }
        isc.confirm(OB.I18N.getLabel('SVFZE_Cofirm'), function (clickedOK) {
          if (clickedOK) {
            OB.RemoteCallManager.call('com.spocsys.vigfurniture.zebra.handlers.PrintInventoryLabelsProcess', {
              shipmentLine: shipmentLine,
              action: 'process'
            }, {}, callback);
          }
        });
      },


          printFromGR2: function (params, view) {
                    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
                    shipmentLine = [],
                        callback;
                    callback = function (rpcResponse, data, rpcRequest) {
                      var message = data.message;
                      view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
                      // close process to refresh current view
                      params.button.closeProcessPopup();
                    };
                    for (i = 0; i < selection.length; i++) {
                        shipmentLine.push(selection[i].id);
                    }
                    isc.confirm(OB.I18N.getLabel('SVFZE_Cofirm'), function (clickedOK) {
                      if (clickedOK) {
                        OB.RemoteCallManager.call('com.spocsys.vigfurniture.zebra.handlers.PrintProductLabelsProcess2', {
                                shipmentLine: shipmentLine,
                          action: 'process'
                        }, {}, callback);
                      }
                    });
                  },


		   
	  printProduct: function (params, view) {
		    var i, selection = params.button.contextView.viewGrid.getSelectedRecords(),
		    product = [],
		        callback;
		    callback = function (rpcResponse, data, rpcRequest) {
		      var message = data.message;
		      view.activeView.messageBar.setMessage(isc.OBMessageBar[message.severity], message.title, message.text);
		      // close process to refresh current view
		      params.button.closeProcessPopup();
		    };
		    for (i = 0; i < selection.length; i++) {
		    	product.push(selection[i].id);
		    }
		    isc.confirm(OB.I18N.getLabel('SVFZE_Cofirm'), function (clickedOK) {
		      if (clickedOK) {
		        OB.RemoteCallManager.call('com.spocsys.vigfurniture.zebra.handlers.PrintProduct', {
		        	product: product,
		          action: 'process'
		        }, {}, callback);
		      }
		    });
		  }


 

};
