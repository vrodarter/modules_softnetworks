package com.spocsys.vigfurnitures.advpaymentmngt.ad_actionbutton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Restrictions;
import org.openbravo.bankstatement.importer.generic.csv.implementation.bean.GenericBankStatementLineBean;
import org.openbravo.bankstatement.importer.generic.csv.util.Utility;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.financialmgmt.payment.FIN_BankStatementLine;

import com.spocsys.vigfurnitures.advpaymentmngt.FileMappingField;

public class AutomaticImporterCSV extends GenericCSVImporter<GenericBankStatementLineBean> {

  List<FileMappingField> getMappingColumns(String FormatFileID) {
    OBCriteria<FileMappingField> ListFileMappingField = OBDal.getInstance().createCriteria(
        FileMappingField.class);

    try {
      if ((FormatFileID != null) && (FormatFileID.trim().equals("") != true)) {
        ListFileMappingField.add(Restrictions.eq(FileMappingField.PROPERTY_FILEFORMAT + ".id",
            FormatFileID));

        if ((ListFileMappingField != null) && (ListFileMappingField.count() > 0)) {
          return ListFileMappingField.list();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public Map<String, String> generateColumnMapping() {

    Map<String, String> map = new HashMap<String, String>();

    map.put("Transaction Date", GenericBankStatementLineBean.PROPERTY_TRANSACTIONDATESTRING);
    map.put("Reference No.", GenericBankStatementLineBean.PROPERTY_REFERENCENO);
    map.put("Business Partner Name", GenericBankStatementLineBean.PROPERTY_BPARTNERNAME);
    map.put("Amount OUT", GenericBankStatementLineBean.PROPERTY_DRAMOUNTSTRING);
    map.put("Amount IN", GenericBankStatementLineBean.PROPERTY_CRAMOUNTSTRING);
    map.put("Description", GenericBankStatementLineBean.PROPERTY_DESCRIPTION);

    return map;
  }

  @Override
  public Class<GenericBankStatementLineBean> configureBean() {
    return GenericBankStatementLineBean.class;
  }

  @Override
  public List<FIN_BankStatementLine> generateFIN_BankStatementLine() {
    List<FIN_BankStatementLine> bankStatementLines = new ArrayList<FIN_BankStatementLine>();
    for (final GenericBankStatementLineBean gbslb : getBankStatementLines()) {
      bankStatementLines.add(Utility.createFIN_BankStatementLine(gbslb, getTargetBankStatement(),
          getLineNo() + 10l));
    }

    return bankStatementLines;
  }

}
