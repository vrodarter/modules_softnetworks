package com.spocsys.vigfurnitures.advpaymentmngt.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;

import com.spocsys.vigfurnitures.advpaymentmngt.FileFormat;

public class FileFormatEventHandler extends EntityPersistenceEventObserver {

  private static Entity[] entities = { ModelProvider.getInstance()
      .getEntity(FileFormat.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final FileFormat regFileFormat = (FileFormat) event.getTargetInstance();

    if (regFileFormat != null) {
      valid(regFileFormat);
    }

  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    final FileFormat regFileFormat = (FileFormat) event.getTargetInstance();

    if (regFileFormat != null) {
      valid(regFileFormat);
    }
  }

  void valid(FileFormat regFileFormat) {

    if ((regFileFormat.getNlineignore() != null)
        && (regFileFormat.getNlineignore().doubleValue() <= 0)) {
      throw new OBException("Lines Nº to ignore can't be less than 1");

    }

    if ((regFileFormat.getInitcolumn() != null)
        && (regFileFormat.getInitcolumn().doubleValue() <= 0)) {
      throw new OBException("From column Nº can't be less than 1");

    }

    if ((regFileFormat.getEndcolumn() != null) && (regFileFormat.getEndcolumn().doubleValue() <= 0)) {
      throw new OBException("To column Nº can't be less than 1");

    }

    if ((regFileFormat.getInitcolumn() != null) && (regFileFormat.getEndcolumn() != null)) {

      if (regFileFormat.getEndcolumn().doubleValue() < regFileFormat.getInitcolumn()) {
        throw new OBException("To column Nº can't be less than From column Nº");
      }

      if (regFileFormat.getInitcolumn().doubleValue() > regFileFormat.getEndcolumn()) {
        throw new OBException("From column Nº can't be higher than To column Nº");
      }

    }

  }
}