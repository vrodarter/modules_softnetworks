package com.spocsys.vigfurnitures.advpaymentmngt.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletConfig;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.openbravo.base.ConfigParameters;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.utility.Image;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.spocsys.vigfurnitures.advpaymentmngt.FileFormat;
import com.spocsys.vigfurnitures.advpaymentmngt.FileMappingField;

public class Utils {

  private static final Logger log = Logger.getLogger(Utils.class);

  private static final char QUOTECHAR = '"';
  private static final String FILE_NAME = "CustomFile.csv";
  private static final String UPLOAD_DIRECTORY = "uploadCVSImporter";
  private static final Locale DECIMALSEPARATOR_COMMA = Locale.ITALY;
  private static final Locale DECIMALSEPARATOR_DOT = Locale.US;
  static final String DEFAULT_DATEFORMAT = "dd/MM/yyyy";
  static final String DEFAULT_DECIMALSEPARATOR = ",";
  public static final String DEFAULT_FIELDDELIMITER = ",";
  private static String ClassName = "com.spocsys.vigfurnitures.advpaymentmngt.ad_actionbutton.AutomaticImporterCSV";

  public static BigDecimal getNumber(String str, String decimalSeparator) {
    Locale locale;
    if (",".equals(decimalSeparator)) {
      locale = DECIMALSEPARATOR_COMMA;
    } else if (".".equals(decimalSeparator)) {
      locale = DECIMALSEPARATOR_DOT;
    } else {
      locale = Locale.getDefault();
    }

    DecimalFormat dF = (DecimalFormat) DecimalFormat.getInstance(locale);
    dF.setParseBigDecimal(true);
    try {
      return (BigDecimal) dF.parse(str);
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return BigDecimal.ZERO;
  }

  public static BigDecimal getNumber1(String str) {

    try {

      return new BigDecimal(str);

    } catch (Exception e) {

      e.printStackTrace();
      log.error(e.getMessage());
    }
    return BigDecimal.ZERO;
  }

  public static FileFormat getFileFormat(String ID) {
    try {

      FileFormat regFileFormat = OBDal.getInstance().get(FileFormat.class, ID);

      if (regFileFormat != null) {
        return regFileFormat;
      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  public static String SaveFile(ServletConfig serverConfig, VariablesSecureApp vars) {

    try {

      FileItem PropertyFile = getFileStream(vars);

      ConfigParameters confParam = ConfigParameters
          .retrieveFrom(RequestContext.getServletContext());

      String uploadPath = confParam.prefix + UPLOAD_DIRECTORY;

      // String uploadPath = serverConfig.getServletContext().getRealPath("") + File.separator
      // + UPLOAD_DIRECTORY;

      File uploadDir = new File(uploadPath);

      if (!uploadDir.exists()) {
        uploadDir.mkdir();
      }

      File file = new File(uploadDir, FILE_NAME);

      InputStream input = PropertyFile.getInputStream();
      Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);

      return (uploadPath + File.separator + FILE_NAME);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;

  }

  public static HashMap<String, Integer> getHeaderColumns(String[] record) {

    try {

      int I = 0;
      HashMap<String, Integer> Columns = new HashMap<String, Integer>();

      for (String str : record) {
        Columns.put(str, I);
        I++;
      }

      return Columns;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static boolean rowIsTheFinal(String[] record, String Key) {

    try {

      for (String str : record) {

        if ((str != null) && (str.trim().equalsIgnoreCase(Key) == true)) {
          return true;
        }

      }

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return false;
  }

  public static List<String[]> parseRows(List<FileMappingField> ListFileMappingField,
      HashMap<String, Integer> HeaderColumns, List<String[]> Lines,
      Map<String, String> ConfigParameters) {

    try {
      String decimalSeparator = ConfigParameters.get("decimalSeparator");
      // -----------------------
      int TRANSACTION_DATE = 0;
      int REFERENCE = 1;
      int BUSINESS_PARTNER = 2;
      int AMOUNT_OUT = 3;
      int AMOUNT_IN = 4;
      int DESCRIPTION = 5;

      // -----------------------

      int TransactionDatePos = -1;
      int ReferencePos = -1;
      int BusinessPartnerPos = -1;
      int AmountOutPos = -1;
      int AmountInPos = -1;
      int DescriptionPos = -1;
      FileMappingField regAmountOut = null;
      FileMappingField regAmountIn = null;

      for (FileMappingField regFileMappingField : ListFileMappingField) {

        String KeyColumn = regFileMappingField.getSearchKey();
        String KeyColumnMapping = regFileMappingField.getName();

        if (HeaderColumns.containsKey(KeyColumn) == true) {

          int Pos = HeaderColumns.get(KeyColumn);
          if (KeyColumnMapping.equalsIgnoreCase("TD") == true) {
            TransactionDatePos = Pos;
          } else {
            if (KeyColumnMapping.equalsIgnoreCase("RN") == true) {
              ReferencePos = Pos;
            } else {
              if (KeyColumnMapping.equalsIgnoreCase("BP") == true) {
                BusinessPartnerPos = Pos;
              } else {
                if (KeyColumnMapping.equalsIgnoreCase("AO") == true) {
                  AmountOutPos = Pos;
                  regAmountOut = regFileMappingField;

                } else {
                  if (KeyColumnMapping.equalsIgnoreCase("AI") == true) {
                    AmountInPos = Pos;
                    regAmountIn = regFileMappingField;
                  } else {
                    if (KeyColumnMapping.equalsIgnoreCase("DESC") == true) {
                      DescriptionPos = Pos;
                    }
                  }
                }
              }
            }
          }

        }

      }

      List<String[]> LinesNew = new ArrayList<String[]>();

      for (int I = 1; I < Lines.size(); I++) {
        String[] record = Lines.get(I);
        String[] recordNew = new String[6];

        if (TransactionDatePos >= 0) {
          recordNew[TRANSACTION_DATE] = record[TransactionDatePos];
        }

        if (ReferencePos >= 0) {
          recordNew[REFERENCE] = record[ReferencePos];
        }

        if (BusinessPartnerPos >= 0) {
          recordNew[BUSINESS_PARTNER] = record[BusinessPartnerPos];
        }

        if (AmountOutPos >= 0) {

          if ((regAmountOut != null) && (regAmountOut.getType() != null)) {

            if (regAmountOut.getType().equalsIgnoreCase("POSITIVE") == true) {

              BigDecimal N = getNumber(record[AmountOutPos], decimalSeparator);

              if (N.doubleValue() >= 0) {
                recordNew[AMOUNT_OUT] = record[AmountOutPos];
              } else {
                recordNew[AMOUNT_OUT] = "0";
              }

            } else {

              BigDecimal N = getNumber(record[AmountOutPos], decimalSeparator);
              if (N.doubleValue() < 0) {
                N = N.multiply(new BigDecimal("-1"));
                recordNew[AMOUNT_OUT] = N.toString();
              } else {
                recordNew[AMOUNT_OUT] = "0";
              }
            }
            // recordNew[AMOUNT_OUT] = record[AmountOutPos];
          } else {
            recordNew[AMOUNT_OUT] = record[AmountOutPos];
          }
        }

        if (AmountInPos >= 0) {

          if ((regAmountIn != null) && (regAmountIn.getType() != null)) {

            if (regAmountIn.getType().equalsIgnoreCase("POSITIVE")) {

              BigDecimal N = getNumber(record[AmountInPos], decimalSeparator);

              if (N.doubleValue() >= 0) {
                recordNew[AMOUNT_IN] = record[AmountInPos];
              } else {
                recordNew[AMOUNT_IN] = "0";
              }

            } else {

              BigDecimal N = getNumber(record[AmountInPos], decimalSeparator);

              if (N.doubleValue() < 0) {
                N = N.multiply(new BigDecimal("-1"));
                recordNew[AMOUNT_IN] = N.toString();
              } else {
                recordNew[AMOUNT_IN] = "0";
              }
            }
            // recordNew[AMOUNT_IN] = record[AmountInPos];
          } else {
            recordNew[AMOUNT_IN] = record[AmountInPos];
          }
        }

        if (DescriptionPos >= 0) {
          recordNew[DESCRIPTION] = record[DescriptionPos];
        }

        LinesNew.add(recordNew);

      }

      return LinesNew;

    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return null;
  }

  public static List<String[]> rebuildList(List<String[]> LinesParse) {

    // -----------------------
    int TRANSACTION_DATE = 0;
    int REFERENCE = 1;
    int BUSINESS_PARTNER = 2;
    int AMOUNT_OUT = 3;
    int AMOUNT_IN = 4;
    int DESCRIPTION = 5;

    // -----------------------
    List<String[]> LinesParseNew = new ArrayList<String[]>();
    // ------------------------
    int I = 1;
    for (String[] record : LinesParse) {

      if ((record[AMOUNT_OUT] == null) || (record[AMOUNT_OUT].trim().equals("") == true)) {
        record[AMOUNT_OUT] = "0";
      }

      if ((record[AMOUNT_IN] == null) || (record[AMOUNT_IN].trim().equals("") == true)) {
        record[AMOUNT_IN] = "0";
      }

      LinesParseNew.add(record);
      I++;

    }
    return LinesParseNew;
  }

  public static List<String[]> ParseAndGenericNewCSV(String FileName, int InitLine,
      String keyEndColumn, int initColumn, int endColumn,
      List<FileMappingField> ListFileMappingField, String FileFormatID) throws IOException {

    Map<String, String> ConfigParameters = Utils.getConfigParameters(FileFormatID);

    List<String[]> ListLines = new ArrayList<String[]>();
    HashMap<String, Integer> ColumnHeader = new HashMap<String, Integer>();
    boolean readColumnHeader = false;

    char Separator = ConfigParameters.get("fieldDelimiter").charAt(0);

    try {
      CSVReader reader = new CSVReader(new FileReader(FileName), Separator, QUOTECHAR);
      String[] record = null;
      // reader.readNext();
      int I = 1;

      while ((record = reader.readNext()) != null) {

        if ((InitLine == 0) || (I > InitLine)) {
          if ((keyEndColumn == null) || (keyEndColumn.trim().equalsIgnoreCase("") == true)
              || (rowIsTheFinal(record, keyEndColumn)) == false) {

            int LengthArray = 0;

            if ((endColumn == 0) && (initColumn > 0)) {
              LengthArray = (record.length - initColumn) + 1;
            } else {
              if ((initColumn == 0) && (endColumn > 0)) {
                LengthArray = (endColumn - initColumn) + 1;
              } else {
                LengthArray = record.length;
              }
            }

            String[] recordNew = new String[LengthArray];

            int NC = 0;
            for (int J = 0; J < record.length; J++) {

              boolean IC = false;
              boolean EC = false;

              if ((initColumn == 0) || (J + 1 >= initColumn)) {
                IC = true;
              }

              if ((endColumn == 0) || (J + 1 <= endColumn)) {
                EC = true;
              }

              if ((IC == true) && (EC == true)) {
                recordNew[NC] = record[J];
                NC++;
              }

              if ((endColumn > 0) && (J + 1 >= endColumn)) {
                break;
              }

            }

            if (recordNew.length > 0) {

              if (readColumnHeader == false) {
                ColumnHeader = getHeaderColumns(recordNew);
                readColumnHeader = true;
              }
              ListLines.add(recordNew);
            }
          }
        }
        I++;

      }
      reader.close();
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
      ListLines = null;
    }

    List<String[]> LinesParse = parseRows(ListFileMappingField, ColumnHeader, ListLines,
        ConfigParameters);

    LinesParse = rebuildList(LinesParse);

    return LinesParse;

  }

  public static void writeToCSV(String FileName, char Separator, List<String[]> ListLines)
      throws IOException {

    try {
      CSVWriter writer = new CSVWriter(new FileWriter(FileName), Separator, QUOTECHAR);
      String[] HeaderColumn = { "Transaction Date", "Reference No.", "Business Partner Name",
          "Amount OUT", "Amount IN", "Description" };

      writer.writeNext(HeaderColumn);
      writer.writeAll(ListLines);

      writer.close();
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
  }

  public static Image InsertFile(InputStream FileInputStream, String Name) {
    Image regImage = OBProvider.getInstance().get(Image.class);

    try {

      regImage.setClient(OBContext.getOBContext().getCurrentClient());
      regImage.setOrganization(OBContext.getOBContext().getCurrentOrganization());
      regImage.setCreatedBy(OBContext.getOBContext().getUser());
      regImage.setUpdatedBy(OBContext.getOBContext().getUser());

      regImage.setName(Name);
      byte[] FileBytes;
      try {
        FileBytes = IOUtils.toByteArray(FileInputStream);
        regImage.setBindaryData(FileBytes);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        regImage = null;
        e.printStackTrace();
        log.error(e.getMessage());
      }
    } catch (Exception e) {
      e.printStackTrace();
      log.error(e.getMessage());
    }
    return regImage;

  }

  public static FileItem getFileStream(VariablesSecureApp vars) throws IOException {
    FileItem fi = vars.getMultiFile("inpFile");

    if (fi == null) {
      throw new IOException("Invalid filename");
    }
    InputStream in = fi.getInputStream();

    if (in == null) {
      throw new IOException("Corrupted file");
    }
    return fi;
  }

  public static Map<String, String> getConfigParameters(String FileFormatID) {
    Map<String, String> configParams = new HashMap<String, String>(2);
    configParams.put("dateFormat", DEFAULT_DATEFORMAT);
    configParams.put("decimalSeparator", DEFAULT_DECIMALSEPARATOR);
    configParams.put("fieldDelimiter", DEFAULT_FIELDDELIMITER);

    try {

      try {

        OBContext.setAdminMode(true);

        FileFormat regFileFormat = OBDal.getInstance().get(FileFormat.class, FileFormatID);

        if (regFileFormat != null) {

          final String dateFormat = StringUtils.isEmpty(regFileFormat.getDateFormat()) ? DEFAULT_DATEFORMAT
              : StringUtils.trim(regFileFormat.getDateFormat());
          final String decimalSeparator = StringUtils.isEmpty(regFileFormat.getDecimalSeparator()) ? DEFAULT_DECIMALSEPARATOR
              : regFileFormat.getDecimalSeparator();
          final String fieldDelimiter = StringUtils.isEmpty(regFileFormat.getFieldDelimiter()) ? DEFAULT_FIELDDELIMITER
              : regFileFormat.getFieldDelimiter();
          configParams.put("dateFormat", dateFormat);
          configParams.put("decimalSeparator", decimalSeparator);
          configParams.put("fieldDelimiter", fieldDelimiter);

        }
      } catch (Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
      }
    } finally {
      OBContext.restorePreviousMode();
    }

    return configParams;
  }
}
