/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.activiti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.openbravo.database.ConnectionProvider;
import org.openbravo.modulescript.ModuleScript;

/**
 * Module scripts which creates the tables for Activiti. Note after upgrading activiti this
 * modulescript should be adapted to support database upgrades.
 * 
 * @author mtaal
 */
public class ActivitiModuleScript extends ModuleScript {

  private static String[] TABLE_TYPE = { "TABLE" };

  private Properties properties = new Properties();
  private boolean isPostgres = false;
  private boolean isDb2 = false;
  private String filePrefix = "activiti.";

  public void execute() {
    try {
      properties.load(new FileInputStream(getPropertiesFile()));

      if (properties.getProperty("bbdd.rdbms") != null) {
        isPostgres = properties.getProperty("bbdd.rdbms").equals("POSTGRE");
        isDb2 = properties.getProperty("bbdd.rdbms").equals("DB2");
      }
      if (isPostgres) {
        filePrefix += "postgres.";
      } else if (isDb2) {
        filePrefix += "db2.";
      } else {
        filePrefix += "oracle.";
      }

      filePrefix += "create.";

      doCreateTables();

    } catch (Exception e) {
      handleError(e);
    }
  }

  private void doCreateTables() throws Exception {
    if (!isDbTablePresent("ACT_RU_EXECUTION")) {
      createTables("engine");
    }
    if (!isDbTablePresent("ACT_HI_PROCINST")) {
      createTables("history");
    }
    if (!isDbTablePresent("ACT_ID_USER")) {
      createTables("identity");
    }
  }

  private void createTables(String part) throws Exception {
    final String sqlFileName = filePrefix + part + ".sql";
    final String sourcePath = properties.getProperty("source.path");
    if (sourcePath == null) {
      throw new Exception("No source path specified, can't create activiti tables");
    }
    final File sourceDir = new File(sourcePath);
    final File sqlDir = new File(sourceDir,
        "modules/org.openbravo.activiti/src-util/modulescript/src/org/openbravo/activiti");
    final File sqlFile = new File(sqlDir, sqlFileName);
    final BufferedReader reader = new BufferedReader(new FileReader(sqlFile));
    final StringBuilder sb = new StringBuilder();
    String line;
    while ((line = reader.readLine()) != null) {
      line = line.trim();
      if (line.startsWith("# ") || line.startsWith("-- ")) {
        continue;
      }
      if (line.startsWith("execute java")) {
        throw new Exception(
            "Upgrading with java execution not supported, update the module script to support this.");
      }
      sb.append(line + "\n");
    }
    final String[] statements = sb.toString().split(";");
    final Connection connection = getConnectionProvider().getConnection();
    for (String statement : statements) {
      if (statement.trim().length() == 0) {
        continue;
      }
      final Statement jdbcStatement = connection.createStatement();
      jdbcStatement.execute(statement);
      jdbcStatement.close();
    }

    // do special thing..
    if (part.equals("engine")) {
      final Statement jdbcStatement = connection.createStatement();
      jdbcStatement.execute("insert into ACT_GE_PROPERTY values ('historyLevel', '2', 1)");
      jdbcStatement.close();
    }
  }

  private boolean isDbTablePresent(String tableName) throws Exception {
    final ConnectionProvider cp = getConnectionProvider();
    final Connection connection = cp.getConnection();

    String localTableName = tableName;
    if (isPostgres) {
      localTableName = tableName.toLowerCase();
    }

    ResultSet tables = null;
    try {
      tables = connection.getMetaData().getTables(connection.getCatalog(), null, localTableName,
          TABLE_TYPE);
      return tables.next();
    } finally {
      tables.close();
    }
  }
}