/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.activiti;

/**
 * Constants used in activiti
 * 
 * @author mtaal
 */
public class ActivitiConstants {

  public static final String ACTIVITI_IDENTIFIER = "_Identifier";
  public static final String TARGET_USER_ID = "targetUserId";
  public static final String TARGET_TAB_ID = "targetTabId";
  public static final String TARGET_IDENTIFIER = "targetIdentifier";
  public static final String TARGET_ENTITY = "targetEntity";
  public static final String TARGET_RECORD_ID = "targetRecordId";
  public static final String TARGET_LINK_NAME = "link";

}
