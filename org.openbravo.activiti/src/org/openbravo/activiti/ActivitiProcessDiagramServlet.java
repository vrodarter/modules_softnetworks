/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2012 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.activiti;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.bpmn.diagram.ProcessDiagramGenerator;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.runtime.ProcessInstance;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.HttpSecureAppServlet;

/**
 * Returns a diagram of an activiti process with the active tasks highlighted.
 * 
 * @author mtaal
 */
public class ActivitiProcessDiagramServlet extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {

    try {
      final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
      final RuntimeService runtimeService = processEngine.getRuntimeService();
      final RepositoryService repositoryService = processEngine.getRepositoryService();
      final String pid = request.getParameter("pid");
      if (pid == null) {
        return;
      }
      final ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
          .processInstanceId(pid).singleResult();

      final ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
          .getDeployedProcessDefinition(processInstance.getProcessDefinitionId());

      final InputStream is = ProcessDiagramGenerator.generateDiagram(processDefinition, "png",
          runtimeService.getActiveActivityIds(processInstance.getId()));

      response.setContentType("image/png");

      byte[] bytes = new byte[10000];
      int length = 0;
      final OutputStream os = response.getOutputStream();
      while ((length = is.read(bytes)) != -1) {
        os.write(bytes, 0, length);
      }
      os.close();
      is.close();

    } catch (final Exception e) {
      throw new OBException(e);
    }
  }
}
