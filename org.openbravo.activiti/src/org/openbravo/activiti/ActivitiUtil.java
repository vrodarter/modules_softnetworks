/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.activiti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.delegate.JavaDelegate;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Property;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;

/**
 * Activiti util method.
 * 
 * @author mtaal
 */
public class ActivitiUtil {

  private static ThreadLocal<OBContext> cachedContext = new ThreadLocal<OBContext>();

  /**
   * Should be called as one of the first things in a execute method of a {@link JavaDelegate}
   * implementation. Normally after this call a try-finally block is used. In the finally block the
   * {@link #clearOBContext()} method should be called.
   * 
   * If the variables map contains a variable {@link ActivitiConstants#TARGET_USER_ID} variable then
   * the OBContext is created using that user id.
   * 
   * @see OBContext#setOBContext(String)
   * @param variables
   *          the variables from the execution scope.
   * @param adminMode
   *          if true admin mode is set to true, {@link OBContext#setAdminMode()}
   */
  public static void setOBContext(Map<String, Object> variables, boolean adminMode) {
    if (cachedContext.get() != null) {
      throw new OBException("Context already cached");
    }
    cachedContext.set(OBContext.getOBContext());

    if (variables.containsKey(ActivitiConstants.TARGET_USER_ID)) {
      OBContext.setOBContext((String) variables.get(ActivitiConstants.TARGET_USER_ID));
    }

    if (adminMode) {
      OBContext.setAdminMode(false);
    }
  }

  /**
   * Should be called in a finally block when the delegate execution finishes.
   * 
   * @see #setOBContext(Map, boolean)
   */
  public static void clearOBContext(boolean adminMode) {
    if (adminMode) {
      OBContext.restorePreviousMode();
    }
    OBContext.setOBContext(cachedContext.get());
    cachedContext.set(null);
  }

  /**
   * The Openbravo business object is not serializable as this could potentially result in un-wanted
   * read of the total database when serializing. For activiti however it makes sense to be able to
   * store some business object information. For this the business object is converted into a Map
   * with control over the level of 'serialization'.
   */
  public static Map<String, Object> toSerializableObject(BaseOBObject obObject, int level) {
    final Map<String, Object> result = new HashMap<String, Object>();
    boolean isDerivedReadable = false;
    if (!obObject.isAllowRead() && !OBContext.getOBContext().isInAdministratorMode()) {
      isDerivedReadable = OBContext.getOBContext().getEntityAccessChecker()
          .isDerivedReadable(obObject.getEntity());
    }

    result.put(ActivitiConstants.ACTIVITI_IDENTIFIER, obObject.getIdentifier());

    for (Property p : obObject.getEntity().getProperties()) {

      if (isDerivedReadable && !p.allowDerivedRead()) {
        continue;
      }

      if (level == 0 && !p.isPrimitive()) {
        continue;
      }

      if (obObject.get(p.getName()) == null) {
        continue;
      }

      if (p.isPrimitive()) {
        result.put(p.getName(), obObject.get(p.getName()));
      } else if (p.isOneToMany() && p.isChild()) {
        final List<Object> values = new ArrayList<Object>();
        for (Object value : (List<?>) obObject.get(p.getName())) {
          values.add(toSerializableObject((BaseOBObject) value, level - 1));
        }
        result.put(p.getName(), values);
      } else if (!p.isOneToMany()) {
        result.put(p.getName(),
            toSerializableObject((BaseOBObject) obObject.get(p.getName()), level - 1));
      }
    }

    return result;
  }
}
