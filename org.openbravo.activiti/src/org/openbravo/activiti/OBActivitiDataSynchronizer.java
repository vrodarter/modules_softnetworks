/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011-2012 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.activiti;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.access.UserRoles;

/**
 * Is responsible for data synchronization between the activiti process engine and OB.
 * 
 * @author mtaal
 */
@ApplicationScoped
public class OBActivitiDataSynchronizer {
  private static final Logger log = Logger.getLogger(OBActivitiDataSynchronizer.class);

  private static final String ASSIGNMENT_GROUP = "assignment";
  private static final String USER_GROUP = "user";
  private static final String ADMIN_GROUP = "admin";

  public void synchronizeData() {
    synchronizeRoles();
    synchronizeUsers();
    synchronizeWorkFlows();
    // commit as this method is called when the system starts and this is
    // done outside of a request which has auto-session/commit handling
    // note: commit should not be done inside the above methods as
    // these methods are called as part of business events which
    // have their own transaction handling
    OBDal.getInstance().commitAndClose();
  }

  public void synchronizeWorkFlows() {
    final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    final RepositoryService repositoryService = processEngine.getRepositoryService();
    OBContext.setAdminMode();
    try {
      final OBQuery<WorkFlow> obQuery = OBDal.getInstance().createQuery(WorkFlow.class, null);
      obQuery.setFilterOnReadableClients(false);
      obQuery.setFilterOnReadableOrganization(false);
      for (WorkFlow workFlow : obQuery.list()) {
        if (workFlow.getWorkflow() == null) {
          continue;
        }

        String deploymentName;
        if (workFlow.getModule().isInDevelopment()) {
          deploymentName = workFlow.getName() + "_" + System.currentTimeMillis();
        } else {
          deploymentName = workFlow.getName() + "_" + workFlow.getModule().getVersion();
        }
        deploymentName += ".bpmn20.xml";
        final Deployment deployment = repositoryService.createDeploymentQuery()
            .deploymentName(deploymentName).singleResult();
        if (deployment == null) {
          final DeploymentBuilder deploymentBuilder = repositoryService.createDeployment()
              .enableDuplicateFiltering().name(deploymentName);
          deploymentBuilder.addInputStream(deploymentName, new ByteArrayInputStream(workFlow
              .getWorkflow().getBytes("UTF-8")));
          deploymentBuilder.deploy();
        }
      }
    } catch (Exception e) {
      throw new OBException(e);
    } finally {
      OBDal.getInstance().flush();
      OBContext.restorePreviousMode();
    }
  }

  public void synchronizeRoles() {

    final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    final IdentityService identityService = processEngine.getIdentityService();
    final GroupQuery groupQuery = identityService.createGroupQuery();
    final List<Group> list = groupQuery.list();
    final Map<String, Group> groups = new HashMap<String, Group>();
    for (Group group : list) {
      groups.put(group.getId(), group);
    }
    OBContext.setAdminMode();
    try {
      final OBQuery<Role> obQuery = OBDal.getInstance().createQuery(Role.class, null);
      obQuery.setFilterOnActive(false);
      obQuery.setFilterOnReadableClients(false);
      obQuery.setFilterOnReadableOrganization(false);
      for (Role role : obQuery.list()) {
        Group group = groups.get(role.getId());
        if (group == null) {
          group = identityService.newGroup(role.getId());
        }
        group.setName(role.getName());
        group.setId(role.getId());
        group.setType(ASSIGNMENT_GROUP);
        identityService.saveGroup(group);
      }
    } finally {
      OBDal.getInstance().flush();
      OBContext.restorePreviousMode();
    }
  }

  public void synchronizeUsers() {
    final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    final IdentityService identityService = processEngine.getIdentityService();
    final UserQuery userQuery = identityService.createUserQuery();
    final List<User> list = userQuery.list();
    final Map<String, User> users = new HashMap<String, User>();
    for (User user : list) {
      users.put(user.getId(), user);
    }

    // due to this issue:
    // https://issues.openbravo.com/view.php?id=21358
    // caused by this activiti issue:
    // https://jira.codehaus.org/browse/ACT-970
    // first and last names should be unique (even between them it seems)
    // the code below makes them unique by appending a _
    // this is harmless the first and last name are only used for display
    // purposes
    List<String> names = new ArrayList<String>();

    OBContext.setAdminMode();
    try {
      final OBQuery<org.openbravo.model.ad.access.User> obQuery = OBDal.getInstance().createQuery(
          org.openbravo.model.ad.access.User.class, "password is not null");
      obQuery.setFilterOnActive(false);
      obQuery.setFilterOnReadableClients(false);
      obQuery.setFilterOnReadableOrganization(false);
      for (org.openbravo.model.ad.access.User obUser : obQuery.list()) {
        User user = users.get(obUser.getId());
        if (user == null) {
          user = identityService.newUser(obUser.getId());
        }
        user.setEmail(obUser.getEmail());

        // activiti needs a firstName
        if (obUser.getFirstName() != null) {
          user.setFirstName(makeNameSafe(obUser.getFirstName(), names));
        } else {
          user.setFirstName(makeNameSafe(obUser.getUsername(), names));
        }
        if (user.getFirstName() == null) {
          user.setFirstName(makeNameSafe(obUser.getName(), names));
        }
        user.setLastName(makeNameSafe(obUser.getLastName(), names));

        // use a value to prevent a null value to be displayed
        // in the activiti explorer
        if (user.getLastName() == null) {
          user.setLastName(makeNameSafe(user.getFirstName(), names));
        }
        user.setPassword(obUser.getPassword());
        user.setId(obUser.getId());
        identityService.saveUser(user);

        final List<String> groups = getGroups(identityService, user.getId());

        // and set membership
        boolean isSysAdmin = false;
        for (UserRoles userRoles : obUser.getADUserRolesList()) {
          final String roleId = (String) DalUtil.getId(userRoles.getRole());
          if (roleId.equals("0")) {
            isSysAdmin = true;
          }
          if (!groups.remove(roleId)) {
            identityService.createMembership(obUser.getId(), roleId);
          }
        }

        if (!groups.remove(USER_GROUP)) {
          identityService.createMembership(obUser.getId(), USER_GROUP);
        }

        // make sure that the sysadmin is part of the sysadmin group
        if (isSysAdmin && !groups.remove(ADMIN_GROUP)) {
          identityService.createMembership(obUser.getId(), ADMIN_GROUP);
        }

        // now remove any remaining memberships
        for (String groupId : groups) {
          identityService.deleteMembership(user.getId(), groupId);
        }
      }
    } finally {
      OBDal.getInstance().flush();
      OBContext.restorePreviousMode();
    }
  }

  // activiti can not handle start/end spaces in a name
  // it gives an error when first entering the work flow administration
  // StringIndexOutOfBoundsException
  // https://issues.openbravo.com/view.php?id=21358
  private String makeNameSafe(String name, List<String> names) {
    if (name == null) {
      return name;
    }
    String localName = name.trim();
    // replace all names with a double space to a single space
    localName = localName.replace("  ", " ");

    while (names.contains(localName)) {
      localName = localName + ".";
    }
    names.add(localName);
    return localName;
  }

  private List<String> getGroups(IdentityService identityService, String userId) {
    final List<String> result = new ArrayList<String>();
    final GroupQuery qry = identityService.createGroupQuery();
    qry.groupMember(userId);
    for (Group group : qry.list()) {
      result.add(group.getId());
    }
    return result;
  }
}
