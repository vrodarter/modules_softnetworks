/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.activiti;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.client.kernel.event.TransactionBeginEvent;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.access.UserRoles;

/**
 * This class observes modifications/insertions to users and roles. After the transaction is
 * completed users and roles are synchronized with Activiti.
 * 
 * @author mtaal
 */
public class OBActivitiEntityEventDataSynchronizer extends EntityPersistenceEventObserver {
  private static Entity[] entities = { ModelProvider.getInstance().getEntity(Role.ENTITY_NAME),
      ModelProvider.getInstance().getEntity(User.ENTITY_NAME),
      ModelProvider.getInstance().getEntity(WorkFlow.ENTITY_NAME),
      ModelProvider.getInstance().getEntity(UserRoles.ENTITY_NAME) };

  private static ThreadLocal<Boolean> userUpdated = new ThreadLocal<Boolean>();
  private static ThreadLocal<Boolean> roleUpdated = new ThreadLocal<Boolean>();
  private static ThreadLocal<Boolean> workFlowUpdated = new ThreadLocal<Boolean>();

  @Inject
  private OBActivitiDataSynchronizer dataSynchronizer;

  public void onTransactionBegin(@Observes TransactionBeginEvent event) {
    roleUpdated.set(null);
    userUpdated.set(null);
  }

  public void onTransactionCompleted(@Observes TransactionBeginEvent event) {

    // if rolledback don't sync
    if (event.getTransaction().wasRolledBack()) {
      roleUpdated.set(null);
      userUpdated.set(null);
      workFlowUpdated.set(null);
      return;
    }

    if (roleUpdated.get() != null && roleUpdated.get()) {
      roleUpdated.set(null);
      dataSynchronizer.synchronizeRoles();
    }
    if (userUpdated.get() != null && userUpdated.get()) {
      userUpdated.set(null);
      dataSynchronizer.synchronizeUsers();
    }
    if (workFlowUpdated.get() != null && workFlowUpdated.get()) {
      workFlowUpdated.set(null);
      dataSynchronizer.synchronizeWorkFlows();
    }
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    handleEvent(event);
  }

  public void onNew(@Observes EntityNewEvent event) {
    handleEvent(event);
  }

  private void handleEvent(EntityPersistenceEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    if (event.getTargetInstance() instanceof User) {
      userUpdated.set(true);
    }
    if (event.getTargetInstance() instanceof UserRoles) {
      userUpdated.set(true);
    }
    if (event.getTargetInstance() instanceof Role) {
      roleUpdated.set(true);
    }
    if (event.getTargetInstance() instanceof WorkFlow) {
      workFlowUpdated.set(true);
    }
  }

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }
}
