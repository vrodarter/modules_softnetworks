/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.activiti;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.explorer.identity.LoggedInUser;
import org.activiti.explorer.ui.login.DefaultLoginHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.portal.PortalAccessible;

/**
 * Override the login handler used by the explorer app to enable auto login.
 * 
 * @author mtaal
 */
public class OBActivitiLoginHandler extends DefaultLoginHandler implements PortalAccessible {

  public LoggedInUser authenticate(HttpServletRequest request, HttpServletResponse response) {
    return doLogin();
  }

  private LoggedInUser doLogin() {
    final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
    final IdentityService identityService = processEngine.getIdentityService();
    final UserQuery userQuery = identityService.createUserQuery();
    if (OBContext.getOBContext() == null) {
      // happens when logging out in activiti
      return null;
    }
    final User user = userQuery.userId(OBContext.getOBContext().getUser().getId()).singleResult();
    if (user != null) {
      return super.authenticate(user.getId(), user.getPassword());
    }
    return null;
  }
}
