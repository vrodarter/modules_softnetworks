/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.activiti;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.ThreadHandler;

/**
 * Sets the authenticated user in the activiti engine.
 * 
 * @author mtaal
 */

public class OBActivitiRequestFilter implements Filter {

  public void init(FilterConfig fConfig) throws ServletException {
  }

  public void destroy() {
  }

  public void doFilter(final ServletRequest request, final ServletResponse response,
      final FilterChain chain) throws IOException, ServletException {
    final ThreadHandler dth = new ThreadHandler() {

      @Override
      public void doBefore() {
        if (OBContext.getOBContext() != null && OBContext.getOBContext().isInitialized()) {
          final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
          if (processEngine != null) {
            final IdentityService identityService = processEngine.getIdentityService();
            identityService.setAuthenticatedUserId(OBContext.getOBContext().getUser().getId());
          }
        }
      }

      @Override
      protected void doAction() throws Exception {
        chain.doFilter(request, response);
      }

      @Override
      public void doFinal(boolean errorOccured) {
        final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        if (processEngine != null) {
          final IdentityService identityService = processEngine.getIdentityService();
          identityService.setAuthenticatedUserId(null);
        }
      }
    };

    dth.run();
  }
}