/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.activiti;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.portal.PortalAccessible;
import org.openbravo.service.json.JsonConstants;
import org.openbravo.service.json.JsonUtils;

/**
 * Action handler which checks how many tasks there are potentially assigned to the current user.
 * 
 * @author mtaal
 */
@ApplicationScoped
public class OBActivitiTaskActionHandler extends BaseActionHandler implements PortalAccessible {

  private static final String TASKID_PARAM = "taskId";
  private static final String ACTION_PARAM = "ACTION";
  private static final String TASK_VIEW_DATA = "taskData";
  private static final String TASK_SUMMARY = "taskSummary";
  private static final String CLAIM = "claim";
  private static final String COMPLETE = "complete";

  // the date format is stored application scoped, a format is not
  // threadsafe therefore the execute method is synchronized
  private SimpleDateFormat dateTimeFormat = JsonUtils.createDateTimeFormat();

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.openbravo.client.kernel.BaseActionHandler#execute(javax.servlet.http.HttpServletRequest,
   * javax.servlet.http.HttpServletResponse)
   */
  @Override
  public synchronized void execute() {
    // method is synchronized because of the global date time format above

    OBContext.setAdminMode();
    try {
      final HttpServletRequest request = RequestContext.get().getRequest();
      String actionParam = request.getParameter(ACTION_PARAM);

      final JSONObject result = new JSONObject();
      final ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
      final TaskService taskService = processEngine.getTaskService();
      final RuntimeService runtimeService = processEngine.getRuntimeService();

      if (CLAIM.equals(actionParam)) {
        // claim the task
        taskService.claim(request.getParameter(TASKID_PARAM), OBContext.getOBContext().getUser()
            .getId());

        // return the refreshed task view data again
        actionParam = TASK_VIEW_DATA;
      } else if (COMPLETE.equals(actionParam)) {
        // complete the task
        taskService.complete(request.getParameter(TASKID_PARAM));

        // return the refreshed task view data again
        actionParam = TASK_VIEW_DATA;
      }

      if (TASK_SUMMARY.equals(actionParam)) {
        int cnt = 0;
        {
          final TaskQuery taskQuery = taskService.createTaskQuery();
          taskQuery.taskAssignee(OBContext.getOBContext().getUser().getId());
          cnt += taskQuery.list().size();
        }
        {
          final TaskQuery taskQuery = taskService.createTaskQuery();
          taskQuery.taskCandidateUser(OBContext.getOBContext().getUser().getId());
          final List<Task> tasks = taskQuery.list();
          cnt += tasks.size();
        }
        result.put("cnt", cnt);
      } else if (TASK_VIEW_DATA.equals(actionParam)) {
        {
          final List<JSONObject> taskJsons = new ArrayList<JSONObject>();
          final TaskQuery taskQuery = taskService.createTaskQuery();
          taskQuery.taskAssignee(OBContext.getOBContext().getUser().getId());
          final List<Task> tasks = taskQuery.list();
          for (Task task : tasks) {
            final Map<String, Object> variables = (task.getProcessDefinitionId() != null ? runtimeService
                .getVariables(task.getExecutionId()) : new HashMap<String, Object>());
            taskJsons.add(taskToJson(task, variables));
          }
          result.put("assignedTasks", new JSONArray(taskJsons));
        }
        {
          final List<JSONObject> taskJsons = new ArrayList<JSONObject>();
          final TaskQuery taskQuery = taskService.createTaskQuery();
          taskQuery.taskCandidateUser(OBContext.getOBContext().getUser().getId());
          final List<Task> tasks = taskQuery.list();
          for (Task task : tasks) {
            final Map<String, Object> variables = (task.getProcessDefinitionId() != null ? runtimeService
                .getVariables(task.getExecutionId()) : new HashMap<String, Object>());
            taskJsons.add(taskToJson(task, variables));
          }
          result.put("candidateTasks", new JSONArray(taskJsons));
        }
      }
      result.put("result", "success");

      final HttpServletResponse response = RequestContext.get().getResponse();
      response.setContentType(JsonConstants.JSON_CONTENT_TYPE);
      response.setHeader("Content-Type", JsonConstants.JSON_CONTENT_TYPE);
      response.getWriter().write(result.toString());
    } catch (Exception e) {
      throw new IllegalStateException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private JSONObject taskToJson(Task task, Map<String, Object> variables) throws JSONException {
    final JSONObject taskJson = new JSONObject();
    taskJson.put("id", task.getId());
    taskJson.put("name", task.getName());
    taskJson.put("description", task.getDescription());
    taskJson.put("creationDate", dateTimeFormat.format(task.getCreateTime()));

    if (task.getAssignee() != null) {
      final User user = OBDal.getInstance().get(User.class, task.getAssignee());
      if (user != null) {
        taskJson.put("assignee", user.getIdentifier());
      }
    }

    int validLinkPartCount = 0;
    if (setTargetInfo(ActivitiConstants.TARGET_RECORD_ID, variables, taskJson)) {
      validLinkPartCount++;
    }
    if (setTargetInfo(ActivitiConstants.TARGET_TAB_ID, variables, taskJson)) {
      validLinkPartCount++;
    }
    // both target info's set, set the target link value to display it
    if (validLinkPartCount == 2) {
      // note showing a space on purpose as this 'tricks' smartclient in that there is content in
      // the cell showing the actual content, this is more practical for now than changing the logic
      // for link fields
      taskJson.put(ActivitiConstants.TARGET_LINK_NAME, " ");
    }

    setTargetInfo(ActivitiConstants.TARGET_IDENTIFIER, variables, taskJson);

    taskJson.put("processInstanceId", task.getProcessInstanceId());

    return taskJson;
  }

  private boolean setTargetInfo(String targetInfoKey, Map<String, Object> variables, JSONObject json)
      throws JSONException {
    if (variables.containsKey(targetInfoKey)) {
      json.put(targetInfoKey, (String) variables.get(targetInfoKey));
      return true;
    }
    return false;
  }

  protected JSONObject execute(Map<String, Object> parameters, String data) {
    throw new UnsupportedOperationException();
  }
}
