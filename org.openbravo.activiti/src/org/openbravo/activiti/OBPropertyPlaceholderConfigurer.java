/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.activiti;

import java.util.Properties;

import org.hibernate.cfg.Configuration;
import org.openbravo.base.session.SessionFactoryController;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * @author mtaal
 */
public class OBPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

  @Override
  protected String resolvePlaceholder(String placeholder, Properties props) {

    // use the hibernate properties to initialize the jdbc
    final Configuration cfg = SessionFactoryController.getInstance().getConfiguration();
    if (cfg.getProperty(placeholder) != null) {
      return cfg.getProperty(placeholder);
    }
    if (placeholder.equals("activiti.ui.environment")) {
      return "activiti";
    }
    return super.resolvePlaceholder(placeholder, props);
  }

}
