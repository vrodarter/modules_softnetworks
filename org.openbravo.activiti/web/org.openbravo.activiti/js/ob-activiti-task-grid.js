/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use. this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

isc.ClassFactory.defineClass('OBACTIV_ActivitiTaskGrid', isc.OBGrid);

isc.OBACTIV_ActivitiTaskGrid.addProperties({

  width: '100%',
  height: '100%',
  dataSource: null,
  canEdit: false,
  alternateRecordStyles: true,
  showFilterEditor: false,
  canReorderFields: false,
  canFreezeFields: false,
  canGroupBy: false,
  canAutoFitFields: false,
  selectionType: 'single',
  showCellContextMenus: false,

  dataProperties: {
    useClientFiltering: false
  },

  gridFields: [{
    name: 'name',
    width: '20%',
    title: OB.I18N.getLabel('OBACTIV_TaskName')
  }, {
    name: 'diagram',
    width: '5%',
    canSort: false,
    cellAlign: 'center',
    title: OB.I18N.getLabel('OBACTIV_TaskDiagram'),
    showHover: true,
    hoverText: OB.I18N.getLabel('OBACTIV_ShowDiagram'),
    hoverHTML: function (record, value, rowNum, colNum, grid) {
      return this.hoverText;
    },
    imageURL: OB.Styles.skinsPath + 'Default/org.openbravo.activiti/images/tasks/show-task-diagram.png',
    
    formatCellValue: function (value, record, rowNum, colNum, grid) {
      var src = this.imageURL,
          srcWithoutExt = src.substring(0, src.lastIndexOf('.')),
          srcExt = src.substring(src.lastIndexOf('.') + 1, src.length),
          onmouseover = 'this.src=\'' + srcWithoutExt + '_Over.' + srcExt + '\'',
          onmousedown = 'this.src=\'' + srcWithoutExt + '_Down.' + srcExt + '\'',
          onmouseout = 'this.src=\'' + src + '\'';
      return '<img style="cursor: pointer;" onmouseover="' + onmouseover + '" onmousedown="' + onmousedown + '" onmouseout="' + onmouseout + '" src="' + src + '" />';
    }
    
  }, {
    name: 'description',
    width: '50%',
    title: OB.I18N.getLabel('OBACTIV_TaskDescription'),
    showHover: true,
    hoverHTML: function (record, value, rowNum, colNum, grid) {
      return value;
    }
  }, {
    name: 'creationDate',
    width: '10%',
    title: OB.I18N.getLabel('OBACTIV_TaskCreationDate'),
    type: 'dateTime',
    formatCellValue: function (value, record, rowNum, colNum, grid) {
      if (!value) {
        return '';
      }

      return Date.parseSchemaDate(value).toShortDatetime();
    }
  }, {
    name: 'link',
    title: OB.I18N.getLabel('OBACTIV_TaskLink'),
    width: '20%',
    isLink: true,
    type: '_id_10',
    showHover: true,
    hoverHTML: function (record, value, rowNum, colNum, grid) {
      if (record.targetIdentifier) {
        return record.targetIdentifier;
      }
      return this.linkText;
    },
    linkText: OB.I18N.getLabel('OBACTIV_TaskLinkDetails')
  }],

  initWidget: function () {
    this.fields = this.gridFields;
    this.dataSource = isc.DataSource.create({
      fields: this.gridFields,
      clientOnly: true,
      testData: []
    });
    this.Super('initWidget', arguments);
  },

  formatLinkValue: function (record, field, colNum, rowNum, value) {
    var fld = this.getField(colNum);
    if (record.targetIdentifier) {
      return record.targetIdentifier;
    }
    return fld.linkText;
  },

  cellClick: function (record, rowNum, colNum) {
    var fld = this.getField(colNum),
        height = isc.Page.getHeight() - 100,
        width = isc.Page.getWidth() - 100;

    if (height < 500) {
      height = 500;
    }
    if (width < 600) {
      width = 600;
    }

    if (fld.isLink) {
      OB.Utilities.openDirectTab(record.targetTabId, record.targetRecordId);
    } else if (fld.name === 'diagram') {
      var imagePopup = isc.OBPopup.create({
        height: height,
        width: width,
        showMinimizeButton: false,
        showMaximizeButton: false
      });
      var image = isc.Img.create({
        height: height,
        width: width,
        src: "../activiti-process-diagram?pid=" + record.processInstanceId
      });
      image.setImageType('center');
      imagePopup.addItem(image);
      imagePopup.show();
    }
  },

  setTaskData: function (data) {
    this.invalidateCache();
    this.getDataSource().setCacheData(data, true);
    this.fetchData();
    // this extra call is needed to solve this issue:
    // https://issues.openbravo.com/view.php?id=17145
    //this.refreshFields();
  }
});