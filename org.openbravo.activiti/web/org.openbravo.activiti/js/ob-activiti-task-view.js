/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2012 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

// used to keep track of the navbar button
OB.OB_ACTIV = {};

isc.defineClass("OBACTIV_ActivitiTaskView", isc.VLayout).addProperties({

  width: '100%',
  height: '100%',
  overflow: 'visible',
  sections: {},
  grids: {},

  initWidget: function () {
    this.addMember(isc.OBToolbar.create({
      view: this,
      leftMembers: [isc.OBToolbarIconButton.create(isc.OBToolbar.REFRESH_BUTTON_PROPERTIES)],
      rightMembers: []
    }));

    this.sectionStack = isc.OBSectionStack.create();
    this.addMember(this.sectionStack);

    this.createSections();

    this.Super('initWidget', arguments);

    this.readData();
  },

  refresh: function () {
    this.readData();

    // update the navbar button
    if (OB.OB_ACTIV.navbarButton) {
      OB.OB_ACTIV.navbarButton.updateCount();
    }
  },

  tabSelected: function (tabNum, tabPane, ID, tab) {
    this.refresh();
  },

  readData: function () {
    var callBack;
    callBack = function (rpcResponse, data, rpcRequest) {
      var instance = rpcRequest.clientContext;

      instance.grids.Assigned.setTaskData(data.assignedTasks);
      instance.grids.Candidate.setTaskData(data.candidateTasks);
    };

    OB.RemoteCallManager.call('org.openbravo.activiti.OBActivitiTaskActionHandler', {}, {
      ACTION: 'taskData',
      IsAjaxCall: '1',
      ignoreForSessionTimeout: '1'
    }, callBack, this);
  },

  createSections: function () {
    var sectionControl;

    this.grids.Assigned = isc.OBACTIV_ActivitiTaskGrid.create({
      view: this
    });
    sectionControl = isc.OBACTIV_SectionTitleControl.create({
      actionName: 'complete',
      confirmText: OB.I18N.getLabel('OBACTIV_ConfirmCompleteSelected'),
      titleText: OB.I18N.getLabel('OBACTIV_CompleteSelected'),
      grid: this.grids.Assigned
    });
    this.sections.Assigned = {
      title: OB.I18N.getLabel('OBACTIV_AssignedTasks'),
      expanded: true,
      items: [this.grids.Assigned],
      controls: [sectionControl]
    };
    this.sectionStack.addSection(this.sections.Assigned);

    this.grids.Candidate = isc.OBACTIV_ActivitiTaskGrid.create({
      view: this
    });
    sectionControl = isc.OBACTIV_SectionTitleControl.create({
      actionName: 'claim',
      confirmText: OB.I18N.getLabel('OBACTIV_ConfirmClaimSelected'),
      titleText: OB.I18N.getLabel('OBACTIV_ClaimSelected'),
      grid: this.grids.Candidate
    });

    this.sections.Candidate = {
      title: OB.I18N.getLabel('OBACTIV_CandidateTasks'),
      expanded: true,
      items: [this.grids.Candidate],
      controls: [sectionControl]
    };
    this.sectionStack.addSection(this.sections.Candidate);
  },

  // always only open one
  isSameTab: function (viewId, params) {
    return viewId === this.getClassName();
  },

  getBookMarkParams: function () {
    var result = {};
    result.viewId = this.getClassName();
    result.tabTitle = this.tabTitle;
    return result;
  }
});

isc.ClassFactory.defineClass('OBACTIV_SectionTitleControl', isc.OBLinkButtonItem);
isc.OBACTIV_SectionTitleControl.addProperties({

  initWidget: function () {
    this.setTitle("[ " + this.titleText + " ]");
    this.Super('initWidget', arguments);
  },

  action: function (confirmed) {
    var callBack, me = this,
        record = this.grid.getSelectedRecord();
    if (!record) {
      isc.say(OB.I18N.getLabel('OBACTIV_PleaseSelectARecord'));
      return;
    }

    if (!confirmed) {
      callBack = function (ok) {
        if (ok) {
          me.action(true);
        }
      };
      isc.ask(this.confirmText, callBack);
      return;
    }

    callBack = function (rpcResponse, data, rpcRequest) {
      var instance = rpcRequest.clientContext;

      instance.grids.Assigned.setTaskData(data.assignedTasks);
      instance.grids.Candidate.setTaskData(data.candidateTasks);

      // update the navbar button
      if (OB.OB_ACTIV.navbarButton) {
        OB.OB_ACTIV.navbarButton.updateCount();
      }

    };

    OB.RemoteCallManager.call('org.openbravo.activiti.OBActivitiTaskActionHandler', {}, {
      ACTION: this.actionName,
      taskId: record.id,
      IsAjaxCall: '1',
      ignoreForSessionTimeout: '1'
    }, callBack, this.grid.view);
  }
});

isc.ClassFactory.defineClass('OBACTIV_NavBarIcon', isc.ImgButton);

isc.OBACTIV_NavBarIcon.addProperties({

  labelCode: 'OBACTIV_NavBar_Label',

  // TODO: move to styling
  baseStyle: 'OBNavBarTextButton',
  alertIcon: {
    src: OB.Styles.skinsPath + 'Default/org.openbravo.client.application/images/navbar/iconAlert.png'
  },
  iconOrientation: 'left',
  iconWidth: 11,
  iconHeight: 13,

  initWidget: function () {
    var instance = this;

    this.Super('initWidget', arguments);

    OB.I18N.getLabel(instance.labelCode, ['-'], instance, 'setTitle');

    OB.TestRegistry.register('org.openbravo.activiti.OBActivitiTaskActionHandler', this);

    // update the count
    this.updateCount();

    OB.OB_ACTIV.navbarButton = this;
  },

  updateCount: function () {

    var callBack;
    callBack = function (rpcResponse, data, rpcRequest) {
      var instance = rpcRequest.clientContext;

      if (data.cnt > 0) {
        OB.I18N.getLabel(instance.labelCode, [data.cnt], instance, 'setTitle');
        instance.setIcon(instance.alertIcon);
      } else {
        OB.I18N.getLabel(instance.labelCode, [0], instance, 'setTitle');
        instance.setIcon({});
      }
      instance.markForRedraw();

      var call;
      call = function () {
        OB.RemoteCallManager.call('org.openbravo.activiti.OBActivitiTaskActionHandler', {}, {
          ACTION: 'taskSummary',
          IsAjaxCall: '1',
          ignoreForSessionTimeout: '1'
        }, callBack, instance);
      };

      // re-use the alertmanager delay
      isc.Timer.setTimeout(call, OB.AlertManager.delay);
    };

    OB.RemoteCallManager.call('org.openbravo.activiti.OBActivitiTaskActionHandler', {}, {
      ACTION: 'taskSummary',
      IsAjaxCall: '1',
      ignoreForSessionTimeout: '1'
    }, callBack, this);
  },

  click: function () {
    var viewDefinition = {
      i18nTabTitle: 'OBACTIV_ViewTitle'
    };
    OB.Layout.ViewManager.openView('OBACTIV_ActivitiTaskView', viewDefinition);
  },

  autoFit: true,
  showTitle: true,
  src: '',
  overflow: 'visible'
});