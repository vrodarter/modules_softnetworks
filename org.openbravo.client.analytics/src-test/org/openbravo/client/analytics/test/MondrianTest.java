/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2012, 2013 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):
 *   Martin Taal <martin.taal@openbravo.com>,
 ************************************************************************
 */

package org.openbravo.client.analytics.test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import mondrian.olap.Connection;
import mondrian.olap.Cube;
import mondrian.olap.Dimension;
import mondrian.olap.Query;
import mondrian.olap.Result;
import mondrian.rolap.RolapConnectionProperties;

import org.junit.Test;
import org.olap4j.Axis;
import org.olap4j.CellSet;
import org.olap4j.OlapConnection;
import org.olap4j.OlapStatement;
import org.olap4j.Position;
import org.olap4j.metadata.Member;
import org.openbravo.client.analytics.MondrianProvider;
import org.openbravo.client.analytics.MondrianSchemaHandler;
import org.openbravo.client.analytics.TimeDimensionProvider;
import org.openbravo.test.base.BaseTest;

/**
 * Tests the integration with Mondrian.
 * 
 * @author mtaal
 */

public class MondrianTest extends BaseTest {

  @Test
  public void testAllSchemas() throws Exception {
    final String schema = MondrianSchemaHandler.getInstance().getSchema();
    System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    System.err.println(schema);
    System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

    final OlapConnection olapConnection = MondrianProvider.getInstance().getOlapConnection();
    olapConnection.close();
  }

  @Test
  public void testMDX() throws Exception {
    final OlapConnection olapConnection = MondrianProvider.getInstance().getOlapConnection();
    OlapStatement stmt = olapConnection.createStatement();

    String mdx = "SELECT NON EMPTY {[Measures].[credit]} ON COLUMNS, NON EMPTY {Hierarchize({{[account].[All accounts], [account].[All accounts].Children}, "
        + " {[account].[PL - Profit & Loss].Children}})} ON ROWS"
        + " FROM [_732913485BB040FFA4643FF06D1AA095] ";

    mdx = "SELECT {[Measures].[credit]} ON COLUMNS, {Hierarchize({[account].[All accounts]})} ON ROWS "
        + " FROM [_732913485BB040FFA4643FF06D1AA095]";

    mdx = "select NON EMPTY {[Measures].[debitAmount]} ON COLUMNS, NON EMPTY {Hierarchize({[account].[All accounts]})} ON ROWS from [TRIAL_F6488042ACD14B6A87EBF42DB13F9EFC]";

    CellSet cellSet = stmt.executeOlapQuery(mdx);
    for (Position axis_0 : cellSet.getAxes().get(Axis.COLUMNS.axisOrdinal()).getPositions()) {
      printMembers("COLUMNS", axis_0.getMembers());
      System.err.println(cellSet.getCell(axis_0).getValue());
      if (cellSet.getAxes().size() > 1) {
        for (Position axis_1 : cellSet.getAxes().get(Axis.ROWS.axisOrdinal()).getPositions()) {
          printMembers("ROWS", axis_1.getMembers());
        }
      }
    }
  }

  public void testSetTimeDimensionReferences() {
    int cnt = TimeDimensionProvider.getInstance().setTimeDimensionReferences(true);
    assertTrue(cnt > 0);
    System.err.println(cnt);
    cnt = TimeDimensionProvider.getInstance().setTimeDimensionReferences(false);
    assertTrue(cnt == 0);
  }

  public void testConnection() {
    final Properties props = new Properties();
    props.setProperty(RolapConnectionProperties.Catalog.name(),
        "res:org/openbravo/client/application/analytics/test/OBCube.xml");
    Connection connection = MondrianProvider.getInstance().getMondrianConnection(props);

    // Second, set up a valid query string
    String queryStr = "select " + "{[Measures].[Order Cost]} on columns, "
        + "{[Store].[All Stores]} on rows " + "from [OB]";

    // Fourth, generate a MDX Query object
    Query query = connection.parseQuery(queryStr);

    // Fifth, execute the query
    Result result = connection.execute(query);

    // Finally, print out the result
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    result.print(pw);
    pw.flush();
    System.out.println(sw.toString());
  }

  public void testOlapConnection() {
    final Properties props = new Properties();
    props.setProperty(RolapConnectionProperties.Catalog.name(),
        "res:org/openbravo/client/application/analytics/test/OBCube.xml");
    OlapConnection connection = MondrianProvider.getInstance().getOlapConnection(props);
    try {
      for (org.olap4j.metadata.Cube cube : connection.getOlapSchema().getCubes()) {
        for (org.olap4j.metadata.Dimension dimension : cube.getDimensions()) {
          System.err.println(dimension.getName());
        }
        for (org.olap4j.metadata.Hierarchy hierarchy : cube.getHierarchies()) {
          System.err.println(hierarchy.getLevels());
        }
      }
      System.err.println(connection.getOlapSchema().getCubes());

      // Second, set up a valid query string
      String queryStr = "select " + "{[Order Status]} on columns, "
          + "{[Time].[2012].CHILDREN} on rows " + "from [OB] where [Measures].[Order Cost]";

      // Fourth, generate a MDX Query object
      OlapStatement statement = connection.createStatement();

      // Fifth, execute the query
      ResultSet result = statement.executeOlapQuery(queryStr);

      // Finally, print out the result
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      while (result.next()) {
        final Object o1 = result.getObject(0);
        final Object o2 = result.getObject(1);
        final Object o3 = result.getObject(2);
        final Object o4 = result.getObject(3);
        System.err.println(o4);
      }

      sw.append(result.toString());
      pw.flush();
      System.out.println(sw.toString());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public void testMDX1() throws Exception {
    testMDXQuery("SELECT ([account].Members) ON COLUMNS,  ( [organization].Members) ON ROWS FROM FinancialMgmtAccountingFact WHERE [Measures].[credit]");
  }

  public void testMDX3() throws Exception {
    testMDXQuery("SELECT CrossJoin([account].Members, [organization].Members) ON COLUMNS FROM FinancialMgmtAccountingFact WHERE [Measures].[credit]");
  }

  public void testMDX4() throws Exception {
    testMDXQuery("SELECT CrossJoin(DESCENDANTS([account].Hierarchy), DESCENDANTS([organization].Hierarchy)) ON COLUMNS FROM FinancialMgmtAccountingFact");
  }

  public void testMDX5() throws Exception {
    testMDXQuery("SELECT DESCENDANTS([account].Hierarchy) ON ROWS, DESCENDANTS([organization].Hierarchy) ON COLUMNS FROM FinancialMgmtAccountingFact");
  }

  public void testMDX7() throws Exception {
    testMDXQuery("SELECT [account].Members ON ROWS, [organization].Members ON COLUMNS FROM FinancialMgmtAccountingFact WHERE [Measures].[credit]");
  }

  public void testMDX8() throws Exception {
    testMDXQuery("SELECT CrossJoin([account].Members, [organization].Members) ON COLUMNS,  [Measures].[credit] FROM FinancialMgmtAccountingFact WHERE [Measures].[credit]");
  }

  public void testMDX9() throws Exception {
    testMDXQuery("SELECT CrossJoin([account].Members, [organization].Members) ON COLUMNS FROM FinancialMgmtAccountingFact");
  }

  public void testMDX10() throws Exception {
    testMDXQuery("SELECT NON EMPTY CrossJoin([account].Members, [organization].Members) ON COLUMNS,  [Measures].[credit] on ROWS FROM FinancialMgmtAccountingFact ");
  }

  public void testMDX11() throws Exception {
    testMDXQuery("SELECT NON EMPTY CrossJoin({[account].[1110 - Checking Account], [account].[11100 - Petty Cash]}, [organization].Members) ON COLUMNS,  [Measures].[credit] on ROWS FROM FinancialMgmtAccountingFact ");
  }

  public void testMDX6() throws Exception {
    testMDXQuery("SELECT DESCENDANTS([account].Hierarchy) ON 0 FROM FinancialMgmtAccountingFact WHERE [Measures].[credit]");
  }

  public void testMDX2() throws Exception {
    testMDXQuery("SELECT ([account].Members) ON COLUMNS,  ( CrossJoin( [organization].Members, [currency].Members)) ON ROWS FROM FinancialMgmtAccountingFact WHERE [Measures].[credit]");
  }

  public void testMDX12() throws Exception {
    testMDXQuery("SELECT [account].Members ON COLUMNS FROM FinancialMgmtAccountingFact");
  }

  public void testMDX13() throws Exception {
    testMDXQuery("SELECT NON EMPTY [organization].Members ON COLUMNS FROM FinancialMgmtAccountingFact WHERE [Measures].[credit]");
  }

  public void testMDX14() throws Exception {
    testMDXQuery("SELECT NON EMPTY [organization].Members ON COLUMNS FROM FinancialMgmtAccountingFact WHERE [Measures].[credit]");
  }

  private void testMDXQuery(String qryStr) throws Exception {
    System.err.println(qryStr);
    final OlapConnection olapConnection = MondrianProvider.getInstance().getOlapConnection(
        new Properties());
    final OlapStatement statement = olapConnection.createStatement();
    CellSet cellSet = statement.executeOlapQuery(qryStr);
    for (Position axis_0 : cellSet.getAxes().get(Axis.COLUMNS.axisOrdinal()).getPositions()) {
      printMembers("COLUMNS", axis_0.getMembers());
      System.err.println(cellSet.getCell(axis_0).getValue());
      if (cellSet.getAxes().size() > 1) {
        for (Position axis_1 : cellSet.getAxes().get(Axis.ROWS.axisOrdinal()).getPositions()) {
          printMembers("ROWS", axis_1.getMembers());
        }
      }
    }

  }

  private void printMembers(String axisName, List<Member> members) {
    System.err.println("Axis: " + axisName);
    for (Member member : members) {
      System.err.println(member.getUniqueName() + " parent: "
          + (member.getParentMember() != null ? member.getParentMember().getUniqueName() : ""));
    }
  }

  public void testCubeGenerationAndConnection() {
    final Properties props = new Properties();
    OlapConnection connection2 = MondrianProvider.getInstance().getOlapConnection(props);

    Connection connection = MondrianProvider.getInstance().getMondrianConnection(props);
    try {
      for (Cube cube : connection.getSchema().getCubes()) {
        for (Dimension dimension : cube.getDimensions()) {
          System.err.println(dimension.getName());
        }
        for (Dimension dimension : cube.getDimensions()) {
          System.err.println(dimension.getName());
        }
      }

      // Second, set up a valid query string
      String queryStr = "select " + "{[organization]} on columns, " + "{[product]} on rows "
          + "from [FinancialMgmtAccountingFact] where [Measures].[credit]";

      // Fourth, generate a MDX Query object
      OlapStatement statement = connection2.createStatement();

      // Fifth, execute the query
      ResultSet result = statement.executeOlapQuery(queryStr);

      // Finally, print out the result
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      while (result.next()) {
        final Object o1 = result.getObject(0);
        final Object o2 = result.getObject(1);
        final Object o3 = result.getObject(2);
        final Object o4 = result.getObject(3);
        System.err.println(o4);
      }

      sw.append(result.toString());
      pw.flush();
      System.out.println(sw.toString());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
