/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;

/**
 * 
 * @author mtaal
 */
@ApplicationScoped
@ComponentProvider.Qualifier(AnalyticsComponentProvider.QUALIFIER)
public class AnalyticsComponentProvider extends BaseComponentProvider {
  public static final String QUALIFIER = "OBANALY_analyticsComponentProvider";

  /*
   * (non-Javadoc)
   * 
   * @see org.openbravo.client.kernel.ComponentProvider#getComponent(java.lang.String,
   * java.util.Map)
   */
  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    if (componentId.equals(SaikuSettingsComponent.COMPONENT_ID)) {
      return getComponent(SaikuSettingsComponent.class);
    }
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.openbravo.client.kernel.ComponentProvider#getGlobalResources()
   */
  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();

    globalResources.add(createStaticResource(
        "web/org.openbravo.client.analytics/js/ob-analytics-saiku-view.js", true));

    globalResources.add(createStaticResource(
        "web/org.openbravo.client.analytics/js/ob-analytics-try-saiku-query-view.js", true));

    globalResources.add(createStaticResource(
        "web/org.openbravo.client.analytics/js/ob-analytics-show-saiku-view.js", true));

    globalResources.add(createStaticResource(
        "web/org.openbravo.client.analytics/js/ob-analytics-widget.js", true));

    globalResources.add(createStaticResource(
        "web/org.openbravo.client.analytics/js/ob-analytics-show-saiku-report.js", true));

    // Styling
    // globalResources.add(createStyleSheetResource(
    // "web/org.openbravo.userinterface.smartclient/openbravo/skins/"
    // + KernelConstants.SKIN_PARAMETER
    // + "/org.openbravo.client.application/ob-application-menu-styles.css", false));

    return globalResources;
  }

  @Override
  public List<String> getTestResources() {
    final List<String> testResources = new ArrayList<String>();
    return testResources;
  }
}