/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.Process;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;

/**
 * A generic query process handler.
 * 
 * @author mtaal
 */
public class AnalyticsProcessReportHandler extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(AnalyticsProcessReportHandler.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    JSONObject result = new JSONObject();
    try {
      Process process = OBDal.getInstance().get(Process.class, parameters.get("processId"));

      JSONObject params = new JSONObject(content).getJSONObject("_params");

      JSONArray actions = new JSONArray();

      JSONObject recordInfo = new JSONObject();

      final AnalyticsQueryProcessor qryProcessor = new AnalyticsQueryProcessor();
      qryProcessor.resolveParameters(params, process.getOBUIAPPParameterList());
      final JSONArray queries = new JSONArray();

      for (ReportQuery reportQuery : process.getOBANALYReportQueryList()) {
        final JSONObject query = new JSONObject();
        query.put("name", reportQuery.getName());
        query.put("query", qryProcessor.process(reportQuery.getQuery()));
        queries.put(queries.length(), query);
      }
      recordInfo.put("queries", queries);

      JSONObject reportAction = new JSONObject();
      reportAction.put("openSaikuReport", recordInfo);
      actions.put(reportAction);

      result.put("responseActions", actions);
      result.put("retryExecution", true);
      result.put("showResultsInProcessView", true);

      return result;
    } catch (Exception e) {
      log.error("Error in process", e);
      return new JSONObject();
    }
  }
}
