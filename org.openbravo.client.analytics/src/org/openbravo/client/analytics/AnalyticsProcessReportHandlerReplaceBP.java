/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * A generic query process handler.
 * 
 */
public class AnalyticsProcessReportHandlerReplaceBP extends AnalyticsProcessReportHandler {
  private static final Logger log = Logger.getLogger(AnalyticsProcessReportHandlerReplaceBP.class);

  @SuppressWarnings("unchecked")
  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    try {
      JSONObject params = new JSONObject(content).getJSONObject("_params");
      JSONObject result = super.doExecute(parameters, content);

      String businessPartnerID = params.has("C_BusinessPartner_ID") ? params
          .getString("C_BusinessPartner_ID") : "null";
      String businessPartnerCategoryID = params.has("C_BPCategory_ID") ? params
          .getString("C_BPCategory_ID") : "null";
      final boolean bpSet = !"null".equals(businessPartnerID);
      final boolean bpCatSet = !"null".equals(businessPartnerCategoryID);

      SAXReader reader = new SAXReader();

      // Retrieve Saiku queries
      JSONArray originalQueries = (JSONArray) ((JSONObject) ((JSONArray) result
          .get("responseActions")).getJSONObject(0).get("openSaikuReport")).get("queries");

      final JSONArray queries = new JSONArray();

      for (int i = 0; i < originalQueries.length(); i++) {

        String xml = (String) originalQueries.getJSONObject(i).get("query");

        Document document = reader.read(new StringReader(xml));
        ArrayList<Element> axes = (ArrayList<Element>) document.selectSingleNode("Query")
            .selectSingleNode("QueryModel").selectSingleNode("Axes").selectNodes("Axis");
        for (Element axis : axes) {
          if (axis.selectSingleNode("Dimensions") == null) {
            continue;
          }
          Collection<Element> dimensions = (Collection<Element>) axis
              .selectSingleNode("Dimensions").selectNodes("Dimension");
          for (Element dimension : dimensions) {
            if (!bpSet && dimension.attributeValue("name").equals("businessPartner")) {
              // if there is no bp selected, remove the dimension
              axis.element("Dimensions").remove(dimension);
            } else if (!bpSet && !bpCatSet
                && dimension.attributeValue("name").equals("businessPartnerCategory")) {
              // if there is no bp and no bpcat selected, include all bpcats
              Element selection = dimension.element("Inclusions").element("Selection");
              selection.remove(selection.attribute("node"));
              selection.remove(selection.attribute("type"));
              selection.remove(selection.attribute("operator"));
              selection.addAttribute("node", "[businessPartnerCategory].[(All)]");
              selection.addAttribute("type", "level");
              selection.addAttribute("operator", "MEMBERS");
            }
          }
        }

        final JSONObject query = new JSONObject();
        query.put("name", originalQueries.getJSONObject(i).get("name"));
        query.put("query", document.asXML().toString());
        queries.put(i, query);
      }

      ((JSONObject) ((JSONArray) result.get("responseActions")).getJSONObject(0).get(
          "openSaikuReport")).put("queries", queries);

      return result;
    } catch (Exception e) {
      log.error("Error in process", e);
      return new JSONObject();
    }
  }
}
