/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.io.StringReader;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.openbravo.base.exception.OBException;

/**
 * Handles BP and BPCategory filtering in the columns section of the report. The report query is
 * expected to be in MDX.
 * 
 */
public class AnalyticsProcessReportHandlerReplaceBPMDX extends AnalyticsProcessReportHandler {
  private static final Logger log = Logger
      .getLogger(AnalyticsProcessReportHandlerReplaceBPMDX.class);

  private final static String BPCAT_CROSSJOIN = "CrossJoin({[businessPartnerCategory].[";
  private final static String BP_CROSSJOIN = "CrossJoin({[businessPartner].[";
  private final static String NON_EMPTY = "NON EMPTY ";
  private final static String ON_COLUMNS = "ON COLUMNS";
  private final static String ON_ROWS = "ON ROWS";

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    try {
      JSONObject params = new JSONObject(content).getJSONObject("_params");
      JSONObject result = super.doExecute(parameters, content);

      String businessPartnerID = params.has("C_BusinessPartner_ID") ? params
          .getString("C_BusinessPartner_ID") : "null";
      String businessPartnerCategoryID = params.has("C_BPCategory_ID") ? params
          .getString("C_BPCategory_ID") : "null";
      final boolean bpSet = !"null".equals(businessPartnerID);
      final boolean bpCatSet = !"null".equals(businessPartnerCategoryID);

      SAXReader reader = new SAXReader();

      // Retrieve Saiku queries
      JSONArray originalQueries = (JSONArray) ((JSONObject) ((JSONArray) result
          .get("responseActions")).getJSONObject(0).get("openSaikuReport")).get("queries");

      final JSONArray queries = new JSONArray();

      for (int i = 0; i < originalQueries.length(); i++) {
        String xml = (String) originalQueries.getJSONObject(i).get("query");

        Document document = reader.read(new StringReader(xml));
        Element mdxElement = (Element) document.selectSingleNode("Query").selectSingleNode("MDX");
        String mdx = mdxElement.getTextTrim();
        int toIndex = -1;
        if (isFilterOnColumns()) {
          toIndex = mdx.toUpperCase().indexOf(ON_COLUMNS);
        } else {
          toIndex = mdx.toUpperCase().indexOf(ON_ROWS);
        }
        if (toIndex == -1) {
          throw new OBException("Can't parse MDX " + mdx);
        }
        final String tempStr = mdx.toUpperCase().substring(0, toIndex);
        final int fromIndex = tempStr.lastIndexOf(NON_EMPTY) + NON_EMPTY.length();
        String prePart = mdx.substring(0, fromIndex);
        String middlePart = mdx.substring(fromIndex, toIndex);
        String postPart = mdx.substring(toIndex, mdx.length());
        if (bpSet) {
          middlePart = BP_CROSSJOIN + businessPartnerID + "]}, " + middlePart + ") ";
        } else if (bpCatSet) {
          middlePart = BPCAT_CROSSJOIN + businessPartnerCategoryID + "]}, " + middlePart + ") ";
        }
        mdxElement.setText(prePart + middlePart + postPart);

        final JSONObject query = new JSONObject();
        query.put("name", originalQueries.getJSONObject(i).get("name"));
        query.put("query", document.asXML().toString());
        queries.put(i, query);
      }

      ((JSONObject) ((JSONArray) result.get("responseActions")).getJSONObject(0).get(
          "openSaikuReport")).put("queries", queries);

      return result;
    } catch (Exception e) {
      log.error("Error in process", e);
      return new JSONObject();
    }
  }

  protected boolean isFilterOnColumns() {
    return true;
  }
}
