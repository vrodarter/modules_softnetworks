/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;


/**
 * Handles BP and BPCategory filtering in the rows section of the report. The report query is
 * expected to be in MDX.
 * 
 */
public class AnalyticsProcessReportHandlerReplaceBPMDXRows extends
    AnalyticsProcessReportHandlerReplaceBPMDX {

  protected boolean isFilterOnColumns() {
    return false;
  }
}
