/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Column;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.domaintype.DomainType;
import org.openbravo.base.model.domaintype.ForeignKeyDomainType;
import org.openbravo.base.model.domaintype.PrimitiveDomainType;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.client.application.Parameter;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.domain.Reference;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;

/**
 * Processes a Saiku xml and replaces parameters with actual values. Parameters are of the format
 * ${NAME} where NAME is a parameter name. The following default parameters are supported:
 * currentDate, currentDate.YEAR, currentDate.QUARTER, currentDate.MONTH, currentDate.DAY,
 * currentClient, currentOrganization, currentUser
 * 
 * The last two are the actual objects present in memory, so you can use currentClient.id and
 * currentOrganization.id.
 * 
 * @author mtaal
 */
public class AnalyticsQueryProcessor {

  private Map<String, Object> parameters = new HashMap<String, Object>();

  public String process(String analyticsQuery) {
    OBContext.setAdminMode(true);
    try {
      final Client client = OBDal.getInstance().get(Client.class,
          OBContext.getOBContext().getCurrentClient().getId());
      if (!parameters.containsKey("currentClient")) {
        parameters.put("currentClient", client);
      }

      final Organization org = OBDal.getInstance().get(Organization.class,
          OBContext.getOBContext().getCurrentOrganization().getId());
      if (!parameters.containsKey("currentOrganization")) {
        parameters.put("currentOrganization", org);
      }

      // set the default currency
      if (!parameters.containsKey("currency")) {
        if (org.getCurrency() != null) {
          parameters.put("currency", org.getCurrency());
        } else {
          parameters.put("currency", client.getCurrency());
        }
      }

      if (!parameters.containsKey("currentUser")) {
        parameters.put("currentUser",
            OBDal.getInstance().get(User.class, OBContext.getOBContext().getUser().getId()));
      }

      if (!parameters.containsKey("currentDate")) {
        parameters.put("currentDate", Calendar.getInstance());
      }

      boolean finished = false;
      String contents = analyticsQuery;
      int numOfLoops = 0;
      while (!finished) {
        // prevent and report infinite loops
        numOfLoops++;
        if (numOfLoops > 10000) {
          throw new OBException("Too many loops, can not process " + contents);
        }

        // get the next parameter
        final int startIndex = contents.indexOf("${");
        if (startIndex == -1) {
          finished = true;
          break;
        }
        final int endIndex = contents.indexOf("}", startIndex);
        String parameter = contents.substring(startIndex + 2, endIndex);

        String[] parameterArray;
        if (parameter.contains("|")) {
          parameterArray = parameter.split("\\|");
        } else {
          parameterArray = new String[1];
          parameterArray[0] = parameter;
        }

        // compute its value
        int i = 0;
        String value = null;
        while (value == null && i < parameterArray.length) {
          value = computeValue(parameterArray[i]);
          i++;
        }

        if (value == null) {
          value = "[VALUE " + value + " of parameter " + parameter + " can not be processed]";
        }

        // replace
        contents = contents.replace("${" + parameter + "}", value);
      }

      // queries with parameters should always have a unique name
      contents = OBAnalyticsUtils.makeQueryNameUnique(contents);

      return contents;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private String computeValue(String parameter) {
    // simple replacement
    if (parameters.containsKey(parameter)) {
      final Object value = parameters.get(parameter);
      if (value instanceof BaseOBObject) {
        return OBAnalyticsUtils.getMemberName((BaseOBObject) value);
      }
      return "" + parameters.get(parameter);
    }
    // be robust
    if (parameters.containsKey(parameter.toLowerCase())) {
      return "" + parameters.get(parameter);
    }
    final String[] path = parameter.split("\\.");
    Object value = parameters.get(path[0]);
    return processValue(parameter, value, path, 0);
  }

  private String processValue(String parameter, Object value, String[] path, int index) {
    // not found
    if (value == null) {
      return null;
    }
    if (value instanceof Date) {
      final Calendar calendar = Calendar.getInstance();
      calendar.setTime((Date) value);
      return processCalendarParameter(calendar, path, index + 1);
    } else if (value instanceof Calendar) {
      return processCalendarParameter((Calendar) value, path, index + 1);
    } else if (value instanceof BaseOBObject) {
      if (index == (path.length - 1)) {
        return OBAnalyticsUtils.getMemberName((BaseOBObject) value);
      } else {
        // a value within the object
        final Object nextValue = ((BaseOBObject) value).get(path[index + 1]);
        return processValue(parameter, nextValue, path, index + 1);
      }
    }

    // hope for the best...
    return value.toString();
  }

  private String processCalendarParameter(Calendar calendar, String[] path, int nextIndex) {
    if (path.length == nextIndex) {
      // just return a date...
      return calendar.get(Calendar.YEAR) + "-" + (1 + calendar.get(Calendar.MONTH)) + "-"
          + calendar.get(Calendar.DAY_OF_MONTH);
    } else if ("YEAR".equals(path[nextIndex])) {
      return "" + calendar.get(Calendar.YEAR);
    } else if ("MONTH".equals(path[nextIndex])) {
      return "" + (1 + calendar.get(Calendar.MONTH));
    } else if ("DAY".equals(path[nextIndex])) {
      return "" + calendar.get(Calendar.DAY_OF_MONTH);
    } else if ("QUARTER".equals(path[nextIndex])) {
      return "" + getQuarter(calendar.get(Calendar.MONTH));
    } else if ("DATE".equals(path[nextIndex])) {
      return calendar.get(Calendar.YEAR) + get2Digits((1 + calendar.get(Calendar.MONTH)))
          + get2Digits(calendar.get(Calendar.DAY_OF_MONTH));
    }
    return null;
  }

  private String get2Digits(int val) {
    if (val < 10) {
      return "0" + val;
    }
    return "" + val;
  }

  private int getQuarter(int month) {
    if (month > 8) {
      return 4;
    } else if (month > 5) {
      return 3;
    } else if (month > 2) {
      return 2;
    }
    return 1;
  }

  public Map<String, Object> getParameters() {
    return parameters;
  }

  public void setParameters(Map<String, Object> parameters) {
    this.parameters = parameters;
  }

  public void resolveParameters(JSONObject paramObject, List<Parameter> listOfParameters)
      throws JSONException {
    OBContext.setAdminMode(true);
    try {
      for (Parameter parameter : listOfParameters) {
        final String defaultValue = parameter.getDefaultValue();

        // no value for this on, go away
        String paramName = parameter.getDBColumnName();
        if (paramName == null) {
          paramName = parameter.getName();
        }
        if (defaultValue == null && !paramObject.has(paramName)) {
          continue;
        }

        // get it from the jsonobject, or use the default
        Object jsonValue = null;
        if (paramObject.has(paramName)) {
          jsonValue = paramObject.get(paramName);
          if (jsonValue == null || JSONObject.NULL.equals(jsonValue)) {
            jsonValue = defaultValue;
          }
        } else {
          jsonValue = defaultValue;
        }

        // no default and null
        if (jsonValue == null) {
          continue;
        }

        // use a quickroute to find the type of the parameter...
        Reference reference = parameter.getReferenceSearchKey();
        if (reference == null) {
          reference = parameter.getReference();
        }
        final Object value = resolveParameter(parameter, reference, jsonValue);
        if (value != null) {
          // be robust and store using both the column name as the parameter name
          // also as lowercase
          parameters.put(paramName, value);
          parameters.put(parameter.getName(), value);
          parameters.put(paramName.toLowerCase(), value);
          parameters.put(parameter.getName().toLowerCase(), value);
        }
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private Object resolveParameter(Parameter parameter, Reference reference, Object jsonValue)
      throws JSONException {
    if (jsonValue instanceof JSONArray) {
      final JSONArray jsonArray = (JSONArray) jsonValue;
      if (jsonArray.length() == 0) {
        return null;
      }
      final List<Object> result = new ArrayList<Object>();
      for (int i = 0; i < jsonArray.length(); i++) {
        result.add(resolveParameter(parameter, reference, jsonArray.get(i)));
      }
      return result;
    }

    final org.openbravo.base.model.Reference modelReference = ModelProvider.getInstance()
        .getReference(reference.getId());
    final DomainType domainType = modelReference.getDomainType();
    if (domainType instanceof PrimitiveDomainType) {
      if (jsonValue instanceof String) {
        Object value = ((PrimitiveDomainType) domainType).createFromString((String) jsonValue);
        if (value instanceof Date) {
          final Calendar calendar = Calendar.getInstance();
          calendar.setTime((Date) value);
          return calendar;
        }
        return value;
      }
      return jsonValue;
    }
    // a foreign key reference
    // TODO: support multiple values
    final ForeignKeyDomainType fkDomainType = (ForeignKeyDomainType) domainType;
    final Column referencedColumn = fkDomainType.getForeignKeyColumn(parameter.getDBColumnName());
    final String referencedTableName = referencedColumn.getTable().getTableName();
    final Entity entity = ModelProvider.getInstance().getEntityByTableName(referencedTableName);
    final String id = jsonValue.toString();
    return OBDal.getInstance().get(entity.getName(), id);
  }

}