/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.Parameter;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.client.kernel.KernelUtils;
import org.openbravo.client.myob.WidgetClass;
import org.openbravo.client.myob.WidgetInstance;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.portal.PortalAccessible;
import org.openbravo.service.json.JsonUtils;

/**
 * @author mtaal
 * 
 */
@ApplicationScoped
public class AnalyticsWidgetActionHandler extends BaseActionHandler implements PortalAccessible {
  private static final Logger log = Logger.getLogger(AnalyticsWidgetActionHandler.class);

  /*
   * (non-Javadoc)
   * 
   * @see org.openbravo.client.kernel.BaseActionHandler#execute(java.util.Map, java.lang.String)
   */
  @Override
  protected JSONObject execute(Map<String, Object> params, String content) {
    JSONObject result = new JSONObject();

    OBContext.setAdminMode(true);
    try {
      final JSONObject parameters = new JSONObject(content);
      final WidgetInstance widget = OBDal.getInstance().get(WidgetInstance.class,
          parameters.getString("id"));
      if (widget == null) {
        return result;
      }
      final WidgetClass widgetClass = widget.getWidgetClass();
      WidgetAnalytics widgetAnalytics = null;
      for (WidgetAnalytics wa : widgetClass.getOBANALYWidgetAnalyticsList()) {
        if (wa.isActive()) {
          widgetAnalytics = wa;
          break;
        }
      }

      if (widgetAnalytics != null) {
        // if the widget has a time restriction, check it before processing the query
        if (widgetAnalytics.isApplyTimeRestriction()) {
          Parameter initParameter = widgetAnalytics.getInitPeriodParameter();
          Parameter endParameter = widgetAnalytics.getEndPeriodParameter();
          String initParameterValue = parameters.getString(initParameter.getDBColumnName());
          if (initParameterValue.indexOf(" ") != -1) {
            initParameterValue = initParameterValue.substring(0, initParameterValue.indexOf(" "));
          }
          String endParameterValue = parameters.getString(endParameter.getDBColumnName());
          if (endParameterValue.indexOf(" ") != -1) {
            endParameterValue = endParameterValue.substring(0, endParameterValue.indexOf(" "));
          }
          SimpleDateFormat paramDateFormat = JsonUtils.createDateFormat();
          try {
            Date startDate = new Timestamp(paramDateFormat.parse(initParameterValue).getTime());
            Date endDate = new Timestamp(paramDateFormat.parse(endParameterValue).getTime());
            long intervalDays = (endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24);
            long maxIntervalDays = widgetAnalytics.getMaxTimeInterval();
            if (intervalDays > maxIntervalDays) {
              log.error("The time interval speficied for the widget "
                  + widgetAnalytics.getObkmoWidgetClass().getWidgetTitle()
                  + " is too large (limit is " + widgetAnalytics.getMaxTimeInterval() + " days");
              result.put(
                  "error",
                  KernelUtils.getInstance().getI18N("OBANALY_TimeIntervalTooLarge",
                      new String[] { Long.toString(maxIntervalDays) }));
              return result;
            }
          } catch (Exception e) {
            log.error("Error while checking the time restriction of the "
                + widgetAnalytics.getObkmoWidgetClass().getWidgetTitle()
                + " widget. Skipping the time restriction check.");
          }
        }
        final AnalyticsQueryProcessor analyticsQueryProcessor = new AnalyticsQueryProcessor();

        analyticsQueryProcessor.setParameters(params);

        analyticsQueryProcessor.resolveParameters(parameters,
            widgetClass.getOBUIAPPParameterEMObkmoWidgetClassIDList());

        final String query = analyticsQueryProcessor.process(widgetAnalytics.getQuery());
        result.put("query", query);
      }

    } catch (JSONException e) {
      log.error("Error executing action: " + e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }
}
