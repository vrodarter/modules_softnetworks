/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.NonUniqueResultException;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.myob.WidgetInstance;
import org.openbravo.client.myob.WidgetProvider;
import org.openbravo.dal.service.OBDao;

/**
 * Responsible for creating the Analytics Widgets.
 * 
 * @author mtaal
 */
public class AnalyticsWidgetProvider extends WidgetProvider {
  private static final Logger log = Logger.getLogger(AnalyticsWidgetProvider.class);
  private static final String ANALYTICSWIDGETCLASSNAME = "OBAnalyticsWidget";

  @Override
  public String generate() {
    throw new UnsupportedOperationException(
        "HTMLWidget definition should be pre-loaded on the client");
  }

  @Override
  public String getClientSideWidgetClassName() {
    return ANALYTICSWIDGETCLASSNAME;
  }

  public JSONObject getWidgetInstanceDefinition(WidgetInstance widgetInstance) {
    final JSONObject jsonObject = new JSONObject();
    try {
      addDefaultWidgetProperties(jsonObject, widgetInstance);
      final JSONObject parameters = jsonObject.getJSONObject(WidgetProvider.PARAMETERS);
      final WidgetAnalytics widgetAnalytics = (WidgetAnalytics) OBDao.getFilteredCriteria(
          WidgetAnalytics.class,
          Restrictions.eq(WidgetAnalytics.PROPERTY_OBKMOWIDGETCLASS, getWidgetClass()))
          .uniqueResult();
      if (widgetAnalytics != null) {
        parameters.put("query", widgetAnalytics.getQuery());
      } else {
        log.error("No url widget defined for widget class " + widgetInstance.getWidgetClass());
      }
    } catch (NonUniqueResultException e) {
      log.error("More than one active url defined for widget " + widgetInstance.getWidgetClass(), e);
    } catch (Exception e) {
      throw new OBException(e);
    }
    return jsonObject;
  }

  @Override
  public boolean validate() {
    if (getWidgetClass() != null && getWidgetClass().getOBANALYWidgetAnalyticsList().isEmpty()) {
      log.error("No analytics widget defined for widget class " + getWidgetClass().getIdentifier());
      return false;
    }
    return true;
  }
}
