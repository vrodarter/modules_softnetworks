/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.core.OBContext;

/**
 * Sets the hashtext property of a new cache definition, is used when the mdx to preload is entered
 * manually.
 * 
 * @author mtaal
 */
public class CubeCacheQueryEventHandler extends EntityPersistenceEventObserver {
  private static final Logger log = Logger.getLogger(CubeCacheQueryEventHandler.class);

  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      CubeCacheDefinition.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    handleEvent(event);
  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    handleEvent(event);
  }

  private void handleEvent(EntityPersistenceEvent event) {
    final CubeCacheDefinition cacheDef = (CubeCacheDefinition) event.getTargetInstance();
    if (cacheDef.getHashtext() == null) {
      final String md5 = OBAnalyticsUtils.getMd5ForCachedQuery(OBContext.getOBContext().getRole(),
          cacheDef.getMdx());
      final Property hashTextProp = entities[0].getProperty(CubeCacheDefinition.PROPERTY_HASHTEXT);
      event.setCurrentState(hashTextProp, md5);
    }
  }
}
