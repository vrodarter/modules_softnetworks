/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.Set;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.model.ad.system.Client;

public abstract class GenerateCubesFromTemplate {

  /**
   * Return a Set with the Id's of the Templates that are going to be used to generate the Cubes.
   */
  public abstract Set<String> getCubeTemplatesToGenerate();

  public abstract void processCube(JSONObject request, JSONObject params, CubeDefinition cube,
      CubeDefinition cubeTemplate, Client client) throws OBException;

}
