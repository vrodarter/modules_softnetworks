/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.HashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

public class GenerateXmlQueryForSaiku {

  /**
   * Generates the header of the Xml Document and the basic Nodes structure of it for Saiku
   * 
   * @param queryId
   *          . A unique ID to identify the Query.
   * @param acctSchemaName
   *          . Name of the AccountingSchema from which the data is retrieved
   * @param acctSchemaId
   *          . ID of the AccountingSchema
   */
  public Document initializeXmlFile(String queryId, String acctSchemaId) {

    final Document doc = DocumentHelper.createDocument();
    doc.setXMLEncoding("UTF-8");
    Element query = doc.addElement("Query");
    doc.appendContent(query);
    query.addAttribute("name", queryId);
    query.addAttribute("type", "QM");
    query.addAttribute("connection", MondrianConstants.CATALOG_SCHEMA_NAME);
    query.addAttribute("cube", acctSchemaId);
    query.addAttribute("catalog", MondrianConstants.CATALOG_SCHEMA_NAME);
    query.addAttribute("schema", MondrianConstants.CATALOG_SCHEMA_NAME);

    Element queryModel = query.addElement("QueryModel");
    Element axes = queryModel.addElement("Axes");

    Element axisRows = axes.addElement("Axis");
    axisRows.addAttribute("location", "ROWS");
    axisRows.addAttribute("nonEmpty", "true");

    axisRows.addElement("Dimensions");

    Element axisColumns = axes.addElement("Axis");
    axisColumns.addAttribute("location", "COLUMNS");
    axisColumns.addAttribute("nonEmpty", "true");

    axisColumns.addElement("Dimensions");

    Element axisWhereClause = axes.addElement("Axis");
    axisWhereClause.addAttribute("location", "FILTER");
    axisWhereClause.addAttribute("nonEmpty", "false");

    axisWhereClause.addElement("Dimensions");
    query.addElement("MDX");
    query.addElement("Properties");

    return doc;

  }

  /**
   * Insert a Dimension in the Xml file for Saiku.
   * 
   * @param doc
   *          Xml Document
   * @param level
   *          0 for Rows, 1 for Columns, 2 for Where Clause
   * @param dimensionName
   *          Name of the Dimension
   * @param nodes
   *          Nodes that are going to be shown. If the list is empty, all nodes will be shown.
   */
  protected Document addDimension(Document doc, int level, String dimensionName, List<String> nodes) {

    Element axis = (Element) doc.selectSingleNode("Query").selectSingleNode("QueryModel")
        .selectSingleNode("Axes").selectNodes("Axis").get(level);
    Element dimensions = (Element) axis.selectSingleNode("Dimensions");

    Element dimension = dimensions.addElement("Dimension");
    dimension.addAttribute("name", dimensionName);
    dimension.addAttribute("hierarchizeMode", "PRE");
    dimension.addAttribute("hierarchyConsistent", "true");

    Element inclusions = dimension.addElement("Inclusions");

    Element[] selection = new Element[nodes.size()];

    for (int i = 0; i < nodes.size(); i++) {
      selection[i] = inclusions.addElement("Selection");
      selection[i] = setAttributes(selection[i], dimensionName, "level", "[" + dimensionName + "]."
          + nodes.get(i), "MEMBERS");
    }

    dimension.addElement("Exclusions");

    return doc;

  }

  /**
   * Insert a Dimension in the Xml file for Saiku.
   * 
   * @param doc
   *          Xml Document
   * @param level
   *          0 for Rows, 1 for Columns, 2 for Where Clause
   * @param dimensionName
   *          Name of the Dimension
   * @param nodes
   *          Nodes that are going to be shown. If the list is empty, all nodes will be shown. Nodes
   *          is a List of HashMap<String, String> with the following Parameters. 1.name: Name of
   *          the Node. 2.type: level or member 3.operator: MEMBER, MEMBERS, or CHILDREN 4.include:
   *          true or false
   */
  protected Document addDimensionAdvanced(Document doc, int level, String dimensionName,
      List<HashMap<String, String>> nodes) {

    Element axis = (Element) doc.selectSingleNode("Query").selectSingleNode("QueryModel")
        .selectSingleNode("Axes").selectNodes("Axis").get(level);
    Element dimensions = (Element) axis.selectSingleNode("Dimensions");

    Element dimension = dimensions.addElement("Dimension");
    dimension.addAttribute("name", dimensionName);
    dimension.addAttribute("hierarchizeMode", "PRE");
    dimension.addAttribute("hierarchyConsistent", "true");

    Element inclusions = dimension.addElement("Inclusions");

    Element[] selection = new Element[nodes.size()];

    for (int i = 0; i < nodes.size(); i++) {
      if ("false".equals(nodes.get(i).get("exclude")) || nodes.get(i).get("exclude") == null) {
        selection[i] = inclusions.addElement("Selection");
        selection[i] = setAttributes(selection[i], dimensionName,
            nodes.get(i).get("type") == null ? "level" : nodes.get(i).get("type"), "["
                + dimensionName + "]." + nodes.get(i).get("name"),
            nodes.get(i).get("operator") == null ? "MEMBERS" : nodes.get(i).get("operator"));
      }
    }

    Element exlusions = dimension.addElement("Exclusions");

    selection = new Element[nodes.size()];

    for (int i = 0; i < nodes.size(); i++) {
      if ("true".equals(nodes.get(i).get("exclude"))) {
        selection[i] = exlusions.addElement("Selection");
        selection[i] = setAttributes(selection[i], dimensionName,
            nodes.get(i).get("type") == null ? "level" : nodes.get(i).get("type"), "["
                + dimensionName + "]." + nodes.get(i).get("name"),
            nodes.get(i).get("operator") == null ? "MEMBERS" : nodes.get(i).get("operator"));
      }
    }

    return doc;

  }

  /**
   * Insert a Filter in the Xml file for Saiku.
   * 
   * @param doc
   *          Xml Document
   * @param level
   *          0 for Rows, 1 for Columns, 2 for Where Clause
   * @param dimensionName
   *          Name of the Dimension
   * @param nodes
   *          Nodes that are going to be shown. If the list is empty, all nodes will be shown.
   */
  public Document addFilter(Document doc, int level, String dimensionName, List<String> nodes) {

    if (nodes.size() == 0) {
      return doc;
    }

    Element axis = (Element) doc.selectSingleNode("Query").selectSingleNode("QueryModel")
        .selectSingleNode("Axes").selectNodes("Axis").get(level);
    Element dimensions = (Element) axis.selectSingleNode("Dimensions");

    Element dimension = dimensions.addElement("Dimension");
    dimension.addAttribute("name", dimensionName);

    Element inclusions = dimension.addElement("Inclusions");

    Element[] selection = new Element[nodes.size()];

    for (int i = 0; i < nodes.size(); i++) {
      selection[i] = inclusions.addElement("Selection");
      selection[i] = setAttributes(selection[i], dimensionName, "member", "[" + dimensionName
          + "]." + nodes.get(i), "MEMBER");
    }

    dimension.addElement("Exclusions");

    return doc;

  }

  /**
   * Add a Filter Condition to an Axis.
   * 
   * @param doc
   *          Xml Document
   * @param level
   *          0 for Rows, 1 for Columns, 2 for Where Clause
   * @param filterCondition
   *          String with the MDX of the Filter Condition
   */
  protected Document addFilterCondition(Document doc, int level, String filterCondition) {

    Element axis = (Element) doc.selectSingleNode("Query").selectSingleNode("QueryModel")
        .selectSingleNode("Axes").selectNodes("Axis").get(level);
    axis.addAttribute("filterCondition", filterCondition);

    return doc;

  }

  /**
   * Set Properties in Xml file for Saiku
   */
  public String finalizeXmlFile(Document doc) {

    generateProperty(doc, "saiku.ui.render.mode", "table");
    generateProperty(doc, "org.saiku.query.explain", "true");
    generateProperty(doc, "saiku.olap.query.nonempty.columns", "true");
    generateProperty(doc, "saiku.olap.query.nonempty.rows", "true");
    generateProperty(doc, "org.saiku.connection.scenario", "false");
    generateProperty(doc, "saiku.ui.formatter", "flat");
    // generateProperty(doc, "saiku.ui.formatter", "flattened");
    generateProperty(doc, "saiku.olap.query.automatic_execution", "true");
    generateProperty(doc, "saiku.olap.query.drillthrough", "true");
    generateProperty(doc, "saiku.olap.query.filter", "true");
    generateProperty(doc, "saiku.olap.query.limit", "true");
    generateProperty(doc, "saiku.olap.query.nonempty", "true");

    return doc.asXML();

  }

  /**
   * Sets attributes for Selection Element for insert in Xml file for Saiku
   * 
   * @param selection
   *          Selection Element
   * @param dimension
   *          Dimension Attribute
   * @param type
   *          Type Attribute
   * @param node
   *          Node Attribute
   * @param operator
   *          Operator Attribute
   */
  protected Element setAttributes(Element selection, String dimension, String type, String node,
      String operator) {
    selection.addAttribute("dimension", dimension);
    selection.addAttribute("type", type);
    selection.addAttribute("node", node);
    selection.addAttribute("operator", operator);
    return selection;
  }

  /**
   * Generate Property Element for insert in Xml file for Saiku
   * 
   * @param doc
   *          Xml Document
   * @param name
   *          Property name
   * @param value
   *          Property value
   */
  protected Document generateProperty(Document doc, String name, String value) {

    Element properties = (Element) doc.selectSingleNode("Query").selectSingleNode("Properties");

    Element property = properties.addElement("Property");
    property.addAttribute("name", name);
    property.addAttribute("value", value);

    return doc;
  }

  public Document addDimensionToRows(Document doc, String dimensionName, List<String> nodes) {
    return addDimension(doc, 0, dimensionName, nodes);
  }

  public Document addAdvancedDimensionToRows(Document doc, String dimensionName,
      List<HashMap<String, String>> nodes) {
    return addDimensionAdvanced(doc, 0, dimensionName, nodes);
  }

  public Document addFilterToRows(Document doc, String dimensionName, List<String> nodes) {
    return addFilter(doc, 0, dimensionName, nodes);
  }

  public Document addFilterConditionToRows(Document doc, String filterCondition) {
    return addFilterCondition(doc, 0, filterCondition);
  }

  public Document addDimensionToColumns(Document doc, String dimensionName, List<String> nodes) {
    return addDimension(doc, 1, dimensionName, nodes);
  }

  public Document addFilterToColumns(Document doc, String dimensionName, List<String> nodes) {
    return addFilter(doc, 1, dimensionName, nodes);
  }

  public Document addFilterConditionToColumns(Document doc, String filterCondition) {
    return addFilterCondition(doc, 1, filterCondition);
  }

  public Document addDimensionToWhereClause(Document doc, String dimensionName, List<String> nodes) {
    return addDimension(doc, 2, dimensionName, nodes);
  }

  public Document addFilterToWhereClause(Document doc, String dimensionName, List<String> nodes) {
    return addFilter(doc, 2, dimensionName, nodes);
  }

  public Document addFilterConditionToWhereClause(Document doc, String filterCondition) {
    return addFilterCondition(doc, 2, filterCondition);
  }

}
