/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBSecurityException;
import org.openbravo.base.model.AccessLevel;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.model.domaintype.BigDecimalDomainType;
import org.openbravo.base.model.domaintype.DateDomainType;
import org.openbravo.base.model.domaintype.DatetimeDomainType;
import org.openbravo.base.model.domaintype.EnumerateDomainType;
import org.openbravo.base.model.domaintype.LongDomainType;
import org.openbravo.base.model.domaintype.PrimitiveDomainType;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.util.Check;
import org.openbravo.client.analytics.TimeDimensionProvider.TimeDimensionEntry;
import org.openbravo.client.application.window.OBViewUtil;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.security.EntityAccessChecker;
import org.openbravo.dal.security.OrganizationStructureProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.access.RoleOrganization;
import org.openbravo.model.ad.datamodel.Column;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.system.ClientInformation;
import org.openbravo.model.ad.utility.Tree;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.financialmgmt.accounting.UserDimension1;
import org.openbravo.model.financialmgmt.accounting.UserDimension2;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;
import org.openbravo.model.financialmgmt.assetmgmt.Asset;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.manufacturing.cost.CostCenter;
import org.openbravo.model.marketing.Campaign;
import org.openbravo.model.project.Project;
import org.openbravo.model.sales.SalesRegion;

/**
 * Generates a Mondrian schema for one or more {@link CubeDefinition}.
 * 
 * NOTE: when working on generating the Mondrian schema the order of the elements in the xml should
 * exactly follow the Mondrian schema definition. A wrong order means that elements are ignore, no
 * exception or warning is given.
 * 
 * TODO: - in MondrianSchemaGenerator changed getFirstProperty and getLastProperty to be robust on
 * properties which are not there. TODO: use cubeDimension name to set dimension name and caption
 * 
 * @author mtaal
 */
public class Mondrian4SchemaGenerator {

  public static final String ANNOTATION_ENTITY = "entity";
  public static final String ANNOTATION_LISTPROPERTY = "listProperty";

  protected static final Logger log = Logger.getLogger(Mondrian4SchemaGenerator.class);

  public static final boolean isOracle = OBPropertiesProvider.getInstance()
      .getOpenbravoProperties().getProperty("bbdd.rdbms").equals("ORACLE");

  private int aliasCnt = 0;

  private Role currentRole = null;

  private List<String> readableOrganizations = null;

  private String aliasPrefix = null;

  private final Map<Property, FKDimensionDefinition> fkDimensions = new HashMap<Property, FKDimensionDefinition>();
  private final Map<String, List<ColumnDef>> columnDefs = new HashMap<String, List<ColumnDef>>();
  private final List<Property> measureLinks = new ArrayList<Property>();

  public synchronized void generate(Document document, Role role) {

    final Element rootElement = document.addElement("Schema")
        .addAttribute("name", MondrianConstants.CATALOG_SCHEMA_NAME)
        .addAttribute("metamodelVersion", "4.0");

    fkDimensions.clear();
    columnDefs.clear();
    measureLinks.clear();

    Element physicalSchemaElement = rootElement.element("PhysicalSchema");
    if (physicalSchemaElement == null) {
      physicalSchemaElement = rootElement.addElement("PhysicalSchema");
    }

    Check.isNotNull(aliasPrefix, "AliasPrefix should be set");

    currentRole = role;
    EntityAccessChecker entityAccessChecker = OBContext.getOBContext().getEntityAccessChecker();
    if (currentRole != OBContext.getOBContext().getRole()) {
      entityAccessChecker = OBProvider.getInstance().get(EntityAccessChecker.class);
      entityAccessChecker.setRoleId((String) DalUtil.getId(currentRole));
      entityAccessChecker.setObContext(OBContext.getOBContext());
      entityAccessChecker.initialize();
    }

    OBContext.setAdminMode(true);
    try {
      final OBQuery<CubeDefinition> cubeQry = OBDal.getInstance().createQuery(CubeDefinition.class,
          null);
      for (CubeDefinition cubeDefinition : cubeQry.list()) {
        if (!cubeDefinition.isActive() || cubeDefinition.isTemplate()) {
          continue;
        }

        final Entity entity = ModelProvider.getInstance().getEntityByTableId(
            cubeDefinition.getTable().getId());
        try {
          // Don't check the entity readability with the admin mode activated
          OBContext.restorePreviousMode();
          entityAccessChecker.checkReadable(entity);
        } catch (OBSecurityException ignored) {
          continue;
        } finally {
          OBContext.setAdminMode(true);
        }

        if (cubeDefinition.getXmldefinition() != null) {
          // you can also only add physicalschema elements in the top
          // if there is a cube don't do the rest, if there is no cube
          // then continue with the dimensions.
          final boolean cubeXMLPresent = parseXmlAndAdd(rootElement, "Cube",
              cubeDefinition.getXmldefinition());
          readAddXMLPhysicalSchema(physicalSchemaElement, cubeDefinition.getXmldefinition());
          if (cubeXMLPresent) {
            continue;
          }
        }

        final Element cubeElement = rootElement.addElement("Cube")
            .addAttribute("name", cubeDefinition.getName())
            .addAttribute("caption", cubeDefinition.getName())
            .addAttribute("enableScenarios", "true");

        final Element dimensionsElement = cubeElement.addElement("Dimensions");

        final String alias = getAlias();
        addPhysicalElement(physicalSchemaElement, entity, alias, cubeDefinition.getSqlfilter());

        final List<Measure> measuresList = new ArrayList<Measure>();
        int cnt = 0;
        for (CubeDimension cubeDimension : getCubeDimensionsOrdered(cubeDefinition)) {
          if (!cubeDimension.isActive()) {
            continue;
          }
          final String modelElement = cubeDimension.getProperty();
          final Property property = getLastProperty(entity, modelElement);
          if (!isCubeableProperty(property)) {
            continue;
          }
          if (cubeDimension.isMeasure()) {
            Measure measure = new Measure();
            measure.setProperty(property);
            measure.setName(cubeDimension.getName());
            measure.setXmlDefintion(cubeDimension.getXmldefinition());
            measure.setMeasureExpression(cubeDimension.getMeasureExpression());
            measure.setIsCalculatedMember(cubeDimension.isCalculatedMember());
            measure.setFormula(cubeDimension.getFormula());
            measuresList.add(measure);
          } else if (cubeDimension.getXmldefinition() != null
              && !cubeDimension.getXmldefinition().equals("")) {
            parseXmlAndAdd(dimensionsElement, "Dimension", cubeDimension.getXmldefinition());
            readAddXMLPhysicalSchema(physicalSchemaElement, cubeDimension.getXmldefinition());
            cnt++;
          } else {
            cnt++;
            getCreateDimension(dimensionsElement, alias, cubeDimension.getSqlfilter(), entity,
                modelElement);
          }
        }

        // nothing to measure, remove again...
        if (cnt == 0 || measuresList.size() == 0) {
          rootElement.remove(cubeElement);
        } else {
          for (FKDimensionDefinition fkDefinition : fkDimensions.values()) {
            fkDefinition.generateMapping(dimensionsElement);
            fkDefinition.addPhysicalElementInformation(physicalSchemaElement);
          }
          setColumnDefs(physicalSchemaElement);
        }

        // needs to be done after handling all dimensions
        mapMeasurements(cubeElement, measuresList, alias);

        fkDimensions.clear();
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void setColumnDefs(Element physicalSchemaElement) {
    for (String searchForAlias : columnDefs.keySet()) {
      for (Object o : physicalSchemaElement.elements()) {
        final Element element = (Element) o;
        final String alias = element.attributeValue("alias");
        if (searchForAlias.equals(alias)) {
          Element columnDefsElement = element.element("ColumnDefs");
          if (columnDefsElement == null) {
            columnDefsElement = element.addElement("ColumnDefs");
          }
          for (ColumnDef columnDef : columnDefs.get(searchForAlias)) {
            if (columnDef.sql != null) {
              Element calculatedColumnDef = columnDefsElement.addElement("CalculatedColumnDef")
                  .addAttribute("name", columnDef.column);
              if (columnDef.type != null) {
                calculatedColumnDef.addAttribute("type", columnDef.type);
              }
              calculatedColumnDef.addElement("ExpressionView").addElement("SQL")
                  .setText(columnDef.sql);
            } else {
              columnDefsElement.addElement("ColumnDef").addAttribute("name", columnDef.column);
            }
          }
        }
      }
    }
    columnDefs.clear();
  }

  private void readAddXMLPhysicalSchema(Element physicalSchemaElement, String xml) {
    final Element tempElement = physicalSchemaElement.addElement("tempElement");
    parseXmlAndAdd(tempElement, "PhysicalSchema", xml);
    if (tempElement.element("PhysicalSchema") != null) {
      for (Object o : tempElement.element("PhysicalSchema").elements()) {
        physicalSchemaElement.add((Element) o);
      }
      physicalSchemaElement.remove(tempElement);
    }
  }

  private void getCreateDimension(Element parentElement, String alias, String sqlFilter,
      Entity sourceEntity, String propertyPath) {
    final List<Property> propertiesOnPath = getPropertiesOnPath(sourceEntity, propertyPath);

    final Property firstProperty = propertiesOnPath.get(0);

    if (firstProperty.getDomainType() instanceof EnumerateDomainType) {
      final Element dimensionElement = createDegenerateDimensionElement(parentElement, alias,
          sourceEntity, firstProperty);
      addListAttribute(dimensionElement.element("Attributes"), firstProperty, alias);
    } else if (TimeDimensionProvider.getInstance().isTimeDimensionProperty(firstProperty)) {
      final Element dimensionElement = createDegenerateDimensionElement(parentElement, alias,
          sourceEntity, firstProperty);
      addDateTimeDimensionReferenceAttribute(dimensionElement, alias, firstProperty);
    } else if (firstProperty.isPrimitive()) {
      if (firstProperty.getDomainType() instanceof DateDomainType
          || firstProperty.getDomainType() instanceof DatetimeDomainType) {
        final Element dimensionElement = createDegenerateDimensionElement(parentElement, alias,
            sourceEntity, firstProperty);
        addDateTimeComputedAttribute(dimensionElement, alias, firstProperty);
      } else {
        final Element dimensionElement = createDegenerateDimensionElement(parentElement, alias,
            sourceEntity, firstProperty);
        addDefaultAttribute(dimensionElement.element("Attributes"), firstProperty, alias);
      }
    } else {
      // a foreign key reference
      FKDimensionDefinition fkDefinition = fkDimensions.get(firstProperty);
      if (fkDefinition == null) {
        fkDefinition = new FKDimensionDefinition(firstProperty);
        fkDimensions.put(firstProperty, fkDefinition);
      }
      fkDefinition.addDimension(propertiesOnPath, 1, sqlFilter);
    }
  }

  private Element createDegenerateDimensionElement(Element parentElement, String table,
      Entity entity, Property property) {
    final Element dimensionElement = parentElement.addElement("Dimension");
    dimensionElement.addAttribute("name", getDimensionName(entity, property.getName()));
    dimensionElement.addAttribute("caption", getCaption(entity, property.getName()));
    dimensionElement.addAttribute("table", table);
    dimensionElement.addAttribute("key", property.getName());

    measureLinks.add(property);

    return dimensionElement;
  }

  private void addDefaultAttribute(Element attributesElement, Property property, String table) {
    final Element attributeElement = attributesElement.addElement("Attribute");
    attributeElement.addAttribute("name", property.getName()).addAttribute("table", table)
        .addAttribute("keyColumn", toUpperOrLowerCase(property.getColumnName()))
        .addAttribute("caption", getCaption(property)).addAttribute("hasHierarchy", "false");
    attributeElement.addElement("MemberFormatter").addAttribute("className",
        OBMondrianMemberFormatter.class.getName());
  }

  private void addListAttribute(Element attributesElement, Property property, String table) {
    final Element attributeElement = attributesElement.addElement("Attribute");

    attributeElement.addAttribute("name", property.getName()).addAttribute("table", table)
        .addAttribute("keyColumn", toUpperOrLowerCase(property.getColumnName()))
        .addAttribute("caption", getCaption(property)).addAttribute("hasHierarchy", "false");

    // annotate the level element so that the memberformatter can read the correct translated value
    attributeElement.addElement("Annotations").addElement("Annotation")
        .addAttribute("name", ANNOTATION_LISTPROPERTY)
        .addText(property.getEntity().getName() + "." + property.getName());

    attributeElement.addElement("MemberFormatter").addAttribute("className",
        OBMondrianMemberFormatter.class.getName());
  }

  private List<Property> getPropertiesOnPath(Entity entity, String propertyPath) {
    final String[] propertyNames = propertyPath.split("\\.");
    final List<Property> result = new ArrayList<Property>();
    Entity loopEntity = entity;
    for (String propertyName : propertyNames) {
      final Property property = loopEntity.getProperty(propertyName);
      if (property.getTargetEntity() != null) {
        loopEntity = property.getTargetEntity();
      }
      result.add(property);
    }
    return result;
  }

  private void addPhysicalElement(Element physicalSchemaElement, Entity entity, String alias,
      String sqlFilter) {

    final Element queryElement = physicalSchemaElement.addElement("Query")
        .addAttribute("alias", alias).addAttribute("keyColumn", getPKColumn(entity));

    final String firstPartOfViewSQL = "select * from " + toUpperOrLowerCase(entity.getTableName())
        + " " + alias + " where " + getClientOrgFilter(entity, alias);
    if (sqlFilter != null) {
      final Element sqlElement = queryElement.addElement("ExpressionView").addElement("SQL");
      sqlElement.addText(firstPartOfViewSQL + " and (" + sqlFilter.replace("[alias]", alias) + ")");
    } else {
      final Element sqlElement = queryElement.addElement("ExpressionView").addElement("SQL");
      sqlElement.addText(firstPartOfViewSQL);
    }
  }

  private Tree getTree(Entity entity) {

    for (ClientInformation clientInfo : currentRole.getClient().getClientInformationList()) {
      if (clientInfo.isActive()) {
        if (Organization.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryTreeOrganization();
        } else if (Project.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryTreeProject();
        } else if (ProductCategory.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryTreeProductCategory();
        } else if (Product.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryTreeProduct();
        } else if (CostCenter.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryTreeCostCenter();
        } else if (Category.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryTreeBPartner();
        } else if (SalesRegion.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryTreeSalesRegion();
        } else if (UserDimension1.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryUserDimension1();
        } else if (UserDimension2.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryUserDimension2();
        } else if (Campaign.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getTreeCampaign();
        } else if (Asset.ENTITY_NAME.equals(entity.getName())) {
          return clientInfo.getPrimaryTreeAsset();
        }
      }
    }

    String treeType = null;
    if (FIN_FinancialAccount.ENTITY_NAME.equals(entity.getName())) {
      treeType = "AR";
    } else if (ElementValue.ENTITY_NAME.equals(entity.getName())) {
      treeType = "EV";
    }
    if (treeType == null) {
      return null;
    }
    final String clientId = currentRole.getClient().getId();
    final List<Tree> trees = OBDal
        .getInstance()
        .createQuery(
            Tree.class,
            Tree.PROPERTY_TYPEAREA + "='" + treeType + "' and " + Tree.PROPERTY_CLIENT + "='"
                + clientId + "'").list();
    if (trees.isEmpty()) {
      return null;
    }
    if (trees.size() > 1) {
      log.warn("More than 1 tree for treeType " + treeType);
    }

    return trees.get(0);
  }

  protected void addDateTimeComputedAttribute(Element dimensionElement, String alias,
      Property property) {

    if (true) {
      return;
    }

    Element attributesElement = dimensionElement.element("Attributes");
    if (attributesElement == null) {
      attributesElement = dimensionElement.addElement("Attributes");
    }
    final String columnSnippet = "<Column name='" + toUpperOrLowerCase(property.getColumnName())
        + "'/>";

    final String yearColumnName = property.getName() + "_YEAR";
    addColumnDef(alias, yearColumnName, "extract(year from " + columnSnippet + ")", "Integer");
    attributesElement.addElement("Attribute").addAttribute("name", property.getName() + "-Year")
        .addAttribute("table", alias).addAttribute("keyColumn", yearColumnName)
        .addAttribute("orderByColumn", yearColumnName);

    {
      final String quarterColumnName = property.getName() + "_QUARTER";
      addColumnDef(alias, quarterColumnName, "extract(quarter from " + columnSnippet + ")",
          "Integer");
      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-Quarter").addAttribute("table", alias)
          .addAttribute("orderByColumn", quarterColumnName);
      final Element keyElement = attributeElement.addElement("Key");
      keyElement.addElement("Column").addAttribute("name", yearColumnName);
      keyElement.addElement("Column").addAttribute("name", quarterColumnName);
    }

    final String monthColumnName = property.getName() + "_MONTH";
    {
      addColumnDef(alias, monthColumnName, "extract(month from " + columnSnippet + ")", "Integer");
      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-Month").addAttribute("table", alias)
          .addAttribute("orderByColumn", monthColumnName);
      final Element keyElement = attributeElement.addElement("Key");
      keyElement.addElement("Column").addAttribute("name", yearColumnName);
      keyElement.addElement("Column").addAttribute("name", monthColumnName);
    }

    final String weekColumnName = property.getName() + "_WEEK";
    {
      addColumnDef(alias, monthColumnName, "extract(weekofyear from " + columnSnippet + ")",
          "Integer");
      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-Week").addAttribute("table", alias)
          .addAttribute("orderByColumn", weekColumnName);
      final Element keyElement = attributeElement.addElement("Key");
      keyElement.addElement("Column").addAttribute("name", yearColumnName);
      keyElement.addElement("Column").addAttribute("name", weekColumnName);
    }

    {
      final String dayColumnName = property.getName() + "_DAY";
      addColumnDef(alias, dayColumnName, "extract(day from " + columnSnippet + ")", "Integer");
      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-Day").addAttribute("table", alias)
          .addAttribute("orderByColumn", toUpperOrLowerCase(property.getColumnName()));
      final Element keyElement = attributeElement.addElement("Key");
      keyElement.addElement("Column").addAttribute("name",
          toUpperOrLowerCase(property.getColumnName()));
    }

    Element hierarchiesElement = dimensionElement.element("Hierarchies");
    if (hierarchiesElement == null) {
      hierarchiesElement = dimensionElement.addElement("Hierarchies");
    }

    {
      final Element hierarchyElement = hierarchiesElement.addElement("Hierarchy");
      hierarchyElement.addAttribute("name", "Yearly").addAttribute("hasAll", "false");
      hierarchyElement.addElement("Level").addAttribute("attribute", property.getName() + "-Year");
      hierarchyElement.addElement("Level").addAttribute("attribute",
          property.getName() + "-Quarter");
      hierarchyElement.addElement("Level").addAttribute("attribute", property.getName() + "-Month");
      hierarchyElement.addElement("Level").addAttribute("attribute", property.getName() + "-Day");
    }

    {
      final Element hierarchyElement = hierarchiesElement.addElement("Hierarchy");
      hierarchyElement.addAttribute("name", "Weekly").addAttribute("hasAll", "false");
      hierarchyElement.addElement("Level").addAttribute("attribute", property.getName() + "-Year");
      hierarchyElement.addElement("Level").addAttribute("attribute", property.getName() + "-Week");
      hierarchyElement.addElement("Level").addAttribute("attribute", property.getName() + "-Day");
    }
  }

  protected void addDateTimeDimensionReferenceAttribute(Element dimensionElement, String alias,
      Property property) {

    Element attributesElement = dimensionElement.element("Attributes");
    if (attributesElement == null) {
      attributesElement = dimensionElement.addElement("Attributes");
    }

    attributesElement.addElement("Attribute").addAttribute("name", property.getName() + "-Year")
        .addAttribute("table", alias)
        .addAttribute("keyColumn", getTimeColumnName(TimeDimension.PROPERTY_THEYEAR))
        .addAttribute("orderByColumn", getTimeColumnName(TimeDimension.PROPERTY_THEYEAR));

    {
      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-Quarter").addAttribute("table", alias)
          .addAttribute("orderByColumn", getTimeColumnName(TimeDimension.PROPERTY_QUARTER));
      final Element keyElement = attributeElement.addElement("Key");
      keyElement.addElement("Column").addAttribute("name",
          getTimeColumnName(TimeDimension.PROPERTY_THEYEAR));
      keyElement.addElement("Column").addAttribute("name",
          getTimeColumnName(TimeDimension.PROPERTY_QUARTER));
    }

    {
      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-Month").addAttribute("table", alias)
          .addAttribute("orderByColumn", getTimeColumnName(TimeDimension.PROPERTY_MONTHOFYEAR));
      final Element keyElement = attributeElement.addElement("Key");
      keyElement.addElement("Column").addAttribute("name",
          getTimeColumnName(TimeDimension.PROPERTY_THEYEAR));
      keyElement.addElement("Column").addAttribute("name",
          getTimeColumnName(TimeDimension.PROPERTY_MONTHOFYEAR));
    }

    {
      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-Week").addAttribute("table", alias)
          .addAttribute("orderByColumn", getTimeColumnName(TimeDimension.PROPERTY_WEEKOFYEAR));
      final Element keyElement = attributeElement.addElement("Key");
      keyElement.addElement("Column").addAttribute("name",
          getTimeColumnName(TimeDimension.PROPERTY_THEYEAR));
      keyElement.addElement("Column").addAttribute("name",
          getTimeColumnName(TimeDimension.PROPERTY_WEEKOFYEAR));
    }

    {
      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-Day").addAttribute("table", alias);
      final Element keyElement = attributeElement.addElement("Key");
      keyElement.addElement("Column").addAttribute(
          "name",
          getTimeColumnName(TimeDimensionProvider.TIMEDIMENSIONENTITY.getIdProperties().get(0)
              .getName()));
    }

    Element hierarchiesElement = dimensionElement.element("Hierarchies");
    if (hierarchiesElement == null) {
      hierarchiesElement = dimensionElement.addElement("Hierarchies");
    }

    {
      final Element hierarchyElement = hierarchiesElement.addElement("Hierarchy");
      hierarchyElement.addAttribute("name", "Yearly").addAttribute("hasAll", "false");
      hierarchyElement.addElement("Level").addAttribute("attribute", "Year");
      hierarchyElement.addElement("Level").addAttribute("attribute", "Quarter");
      hierarchyElement.addElement("Level").addAttribute("attribute", "Month");
      hierarchyElement.addElement("Level").addAttribute("attribute", "Day");
    }

    {
      final Element hierarchyElement = hierarchiesElement.addElement("Hierarchy");
      hierarchyElement.addAttribute("name", "Weekly").addAttribute("hasAll", "false");
      hierarchyElement.addElement("Level").addAttribute("attribute", "Year");
      hierarchyElement.addElement("Level").addAttribute("attribute", "Week");
      hierarchyElement.addElement("Level").addAttribute("attribute", "Day");
    }
  }

  private void addColumnDef(String alias, String column, String sql, String type) {
    final ColumnDef columnDef = new ColumnDef(column, sql, type);
    List<ColumnDef> columnDefList = columnDefs.get(alias);
    if (columnDefList == null) {
      columnDefList = new ArrayList<Mondrian4SchemaGenerator.ColumnDef>();
      columnDefs.put(alias, columnDefList);
    }
    columnDefList.add(columnDef);
  }

  private List<CubeDimension> getCubeDimensionsOrdered(CubeDefinition cubeDefinition) {
    OBCriteria<CubeDimension> obc = OBDal.getInstance().createCriteria(CubeDimension.class);
    obc.add(Restrictions.eq(CubeDimension.PROPERTY_OBANALYCUBEDEFINITION, cubeDefinition));
    obc.addOrder(Order.asc(CubeDimension.PROPERTY_LINENO));
    return obc.list();
  }

  protected boolean parseXmlAndAdd(Element parentElement, String selectNode, String xml) {
    Document auxDoc = null;
    try {
      auxDoc = DocumentHelper.parseText(xml);
      // silently ignore if not present, is expected that way by some code
      if (auxDoc.selectSingleNode("//" + selectNode) != null) {
        parentElement.add(auxDoc.selectSingleNode("//" + selectNode));
        return true;
      }
    } catch (DocumentException e) {
      log.error("MondrianSchemaGenerator.generate() parsing xml " + xml, e);
    }
    return false;
  }

  protected String getAlias() {
    aliasCnt++;
    return getAliasForOracleOrPostgres(getAliasPrefix() + aliasCnt);
  }

  protected String getClientOrgFilter(Entity targetEntity, String alias) {
    final AccessLevel accessLevel = targetEntity.getAccessLevel();
    switch (accessLevel) {
    case SYSTEM:
      return null;
    case SYSTEM_CLIENT:
      return "(" + alias + toUpperOrLowerCase(".ad_client_id='") + currentRole.getClient().getId()
          + "' or " + alias + toUpperOrLowerCase(".ad_client_id='0')");
    default:
      final StringBuilder sb = new StringBuilder();
      for (String orgId : getReadableOrganizations()) {
        if (sb.length() > 0) {
          sb.append(",");
        }
        sb.append("'" + orgId + "'");
      }

      return alias + toUpperOrLowerCase(".ad_org_id in (") + sb.toString() + ")";
    }
  }

  protected boolean isCubeableProperty(Property property) {
    if (property.isId()) {
      return false;
    }
    if (property.isAuditInfo()) {
      return false;
    }
    if (property.isEncrypted()) {
      return false;
    }
    if (property.isOneToMany()) {
      return false;
    }
    if (property.isInactive()) {
      return false;
    }
    if (property.isAuditInfo()) {
      return false;
    }
    if (property.isTransient()) {
      return false;
    }
    if (property.getSqlLogic() != null) {
      return false;
    }
    if (property.getTargetEntity() != null) {
      if (Client.ENTITY_NAME.equals(property.getTargetEntity().getName())) {
        return false;
      }
      if (AttributeSet.ENTITY_NAME.equals(property.getTargetEntity().getName())) {
        return false;
      }
      if (AttributeSetInstance.ENTITY_NAME.equals(property.getTargetEntity().getName())) {
        return false;
      }
      if (Location.ENTITY_NAME.equals(property.getTargetEntity().getName())) {
        return false;
      }
    }
    if (property.isDate() || property.isDatetime()) {
      return true;
    }
    // a measurement
    if (property.getDomainType() instanceof LongDomainType) {
      return true;
    }
    if (property.getDomainType() instanceof BigDecimalDomainType) {
      return true;
    }
    if (property.getDomainType() instanceof EnumerateDomainType) {
      return true;
    }
    return true;
  }

  protected Property getLastProperty(Entity entity, String propertyPath) {
    final String[] propertyNames = propertyPath.split("\\.");
    Entity loopEntity = entity;
    Property currentProperty = null;
    for (String propertyName : propertyNames) {
      currentProperty = loopEntity.getProperty(propertyName);
      if (currentProperty.getTargetEntity() != null) {
        loopEntity = currentProperty.getTargetEntity();
      }
    }
    return currentProperty;
  }

  protected String getPrimaryKeyTable(Property property) {
    if (property.getTargetEntity() == null) {
      return toUpperOrLowerCase(property.getEntity().getTableName());
    } else {
      return toUpperOrLowerCase(property.getTargetEntity().getTableName());
    }
  }

  protected Property getFirstProperty(Entity entity, String propertyPath) {
    final String[] propertyNames = propertyPath.split("\\.");
    return entity.getProperty(propertyNames[0]);
  }

  protected Property computeLastJoinProperty(Property[] properties) {
    Property lastJoinProperty = null;
    for (Property property : properties) {
      final TimeDimensionEntry timeDimensionEntry = TimeDimensionProvider.getInstance()
          .getTimeDimensionEntry(property);

      if (timeDimensionEntry != null || property.getTargetEntity() != null) {
        lastJoinProperty = property;
      }
    }
    return lastJoinProperty;
  }

  private Element setLevelName(Element levelElement, String dimensionName) {
    levelElement.addAttribute("name", dimensionName + "Level");
    return levelElement;
  }

  private String getTimeColumnName(String propertyName) {
    final Property prop = TimeDimensionProvider.TIMEDIMENSIONENTITY.getProperty(propertyName);
    return toUpperOrLowerCase(prop.getColumnName());
  }

  protected String getDimensionName(Entity entity, String propertyPath) {
    List<Property> properties = getPropertiesOnPath(entity, propertyPath);
    if (properties.size() > 1) {
      return properties.get(0).getName() + "-" + properties.get(properties.size() - 1).getName();
    }
    return properties.get(0).getName();
  }

  private String getPKColumn(Entity entity) {
    return toUpperOrLowerCase(entity.getIdProperties().get(0).getColumnName());
  }

  private void mapMeasurements(Element parentElement, List<Measure> measuresList, String alias) {

    final Element calculatedMembersElement = parentElement.addElement("CalculatedMembers");
    final Element measureGroupElement = parentElement.addElement("MeasureGroups")
        .addElement("MeasureGroup").addAttribute("table", alias);
    final Element measuresElement = measureGroupElement.addElement("Measures");
    final Element dimensionLinksElement = measureGroupElement.addElement("DimensionLinks");

    for (Measure measure : measuresList) {
      if (measure.getIsCalculatedMember()) {
        if (measure.getXmlDefintion() != null && !"".equals(measure.getXmlDefintion())) {
          parseXmlAndAdd(calculatedMembersElement, "CalculatedMember", measure.getXmlDefintion());
        } else {
          final Element measureElement = calculatedMembersElement.addElement("CalculatedMember");
          if (measure.getName() != null && !"".equals(measure.getName())) {
            measureElement.addAttribute("name", measure.getName());
          } else {
            measureElement.addAttribute("name", measure.getProperty().getName());
          }
          measureElement.addAttribute("dimension", "Measures");
          final Element formula = measureElement.addElement("Formula");
          formula.addText(measure.getFormula());
          addCellFormatter(measure, measureElement, measure.getProperty());
        }
      } else {
        if (measure.getXmlDefintion() != null && !"".equals(measure.getXmlDefintion())) {
          parseXmlAndAdd(measuresElement, "Measure", measure.getXmlDefintion());
        } else {
          final Element measureElement = measuresElement.addElement("Measure");
          if (measure.getName() != null && !"".equals(measure.getName())) {
            measureElement.addAttribute("name", measure.getName());
          } else {
            measureElement.addAttribute("name", measure.getProperty().getName());
          }
          if (measure.getMeasureExpression() != null && !"".equals(measure.getMeasureExpression())) {
            measureElement.addAttribute("aggregator", "sum");
            final Element measureExpressionElement = measureElement.addElement("MeasureExpression");
            final Element sqlElement = measureExpressionElement.addElement("SQL");
            sqlElement.addText(measure.getMeasureExpression());
          } else {
            measureElement.addAttribute("column", toUpperOrLowerCase(measure.getProperty()
                .getColumnName()));
            measureElement.addAttribute("aggregator", "sum");
          }
          addCellFormatter(measure, measureElement, measure.getProperty());
        }
      }
    }
    for (Property prop : measureLinks) {
      if (prop.getTargetEntity() != null) {
        dimensionLinksElement.addElement("ForeignKeyLink")
            .addAttribute("dimension", prop.getName())
            .addAttribute("foreignKeyColumn", toUpperOrLowerCase(prop.getColumnName()));
      } else {
        dimensionLinksElement.addElement("FactLink").addAttribute("dimension", prop.getName());
      }
    }
    measureLinks.clear();
  }

  protected String getCaption(Entity entity, String propertyPath) {
    final List<Property> properties = getPropertiesOnPath(entity, propertyPath);
    if (properties.size() > 1) {
      return getCaption(properties.get(0)) + " - "
          + getCaption(properties.get(properties.size() - 1));
    }
    return getCaption(properties.get(0));
  }

  protected String getCaption(Property property) {
    final Column column = OBDal.getInstance().get(Column.class, property.getColumnId());
    if (column.getApplicationElement() != null) {
      final org.openbravo.model.ad.ui.Element element = column.getApplicationElement();
      return OBViewUtil.getLabel(element, element.getADElementTrlList());
    }
    return property.getName();
  }

  protected void addCellFormatter(Measure measure, Element element, Property property) {
    // trick to format percentages...
    if (measure.getName() != null && measure.getName().contains("%")) {
      element.addElement("CellFormatter").addAttribute("className",
          MondrianCellFormatter.Percentage.class.getName());
      return;
    }
    if (!(property.getDomainType() instanceof PrimitiveDomainType)) {
      return;
    }
    final PrimitiveDomainType primitiveDomainType = (PrimitiveDomainType) property.getDomainType();
    if (primitiveDomainType.getFormatId() == null) {
      return;
    }
    final Class<?> cellFormatterClass = MondrianCellFormatter
        .getCellFormatterClass(primitiveDomainType.getFormatId());
    element.addElement("CellFormatter").addAttribute("className", cellFormatterClass.getName());
  }

  protected List<String> getReadableOrganizations() {
    if (readableOrganizations != null) {
      return readableOrganizations;
    }

    Check.isNotNull(currentRole, "currentRole must be set");
    final OrganizationStructureProvider orgProvider = OBProvider.getInstance().get(
        OrganizationStructureProvider.class);
    orgProvider.setClientId(currentRole.getClient().getId());

    final Set<String> readableOrgs = new HashSet<String>();

    final List<Organization> os = getOrganizationList();
    for (final Organization o : os) {
      readableOrgs.addAll(orgProvider.getNaturalTree(o.getId()));
      // if zero is an organization then add them all!
      if (o.getId().equals("0")) {
        for (final Organization org : getOrganizations(currentRole.getClient())) {
          readableOrgs.add(org.getId());
        }
      }
    }
    readableOrgs.add("0");

    readableOrganizations = new ArrayList<String>(readableOrgs);
    return readableOrganizations;
  }

  @SuppressWarnings("unchecked")
  private List<Organization> getOrganizationList() {
    final Query qry = SessionHandler.getInstance().createQuery(
        "select o from " + Organization.class.getName() + " o, " + RoleOrganization.class.getName()
            + " roa where o." + Organization.PROPERTY_ID + "=roa."
            + RoleOrganization.PROPERTY_ORGANIZATION + "." + Organization.PROPERTY_ID + " and roa."
            + RoleOrganization.PROPERTY_ROLE + "." + Organization.PROPERTY_ID + "='"
            + currentRole.getId() + "' and roa." + RoleOrganization.PROPERTY_ACTIVE + "='Y' and o."
            + Organization.PROPERTY_ACTIVE + "='Y'");
    return (List<Organization>) qry.list();
  }

  @SuppressWarnings("unchecked")
  private List<Organization> getOrganizations(Client client) {
    final Query qry = SessionHandler.getInstance().createQuery(
        "select o from " + Organization.class.getName() + " o where " + "o."
            + Organization.PROPERTY_CLIENT + "=? and o." + Organization.PROPERTY_ACTIVE + "='Y'");
    qry.setParameter(0, client);
    return qry.list();
  }

  public String toUpperOrLowerCase(String arg) {
    if (isOracle) {
      return arg.toUpperCase();
    } else {
      return arg.toLowerCase();
    }
  }

  public String getAliasForOracleOrPostgres(String alias) {
    if (isOracle) {
      return "\"" + alias + "\"";
    } else {
      return alias;
    }
  }

  public Role getCurrentRole() {
    return currentRole;
  }

  public void setCurrentRole(Role currentRole) {
    this.currentRole = currentRole;
  }

  /**
   * Prepend the role id as part of the cube name
   * 
   * @see #getCubeKeyParts
   */
  protected String createCubeKey(String cubeName) {
    return currentRole.getId() + "_" + cubeName;
  }

  public String getAliasPrefix() {
    return aliasPrefix;
  }

  public void setAliasPrefix(String aliasPrefix) {
    this.aliasPrefix = aliasPrefix;
  }

  /**
   * Makes sure that the name works correctly in Saiku, for example gets rid of /, this does not
   * work nicely with Saiku
   */
  protected String getSafeName(String name) {
    return name.replace("/", "_");
  }

  private class Measure {
    Property property;
    String name;
    String xmlDefintion;
    boolean isCalculatedMember;
    String measureExpression;
    String formula;

    public boolean getIsCalculatedMember() {
      return isCalculatedMember;
    }

    public void setIsCalculatedMember(boolean isCalculatedMember) {
      this.isCalculatedMember = isCalculatedMember;
    }

    public String getMeasureExpression() {
      return measureExpression;
    }

    public void setMeasureExpression(String measureExpression) {
      this.measureExpression = measureExpression;
    }

    public String getFormula() {
      return formula;
    }

    public void setFormula(String formula) {
      this.formula = formula;
    }

    public Property getProperty() {
      return property;
    }

    public void setProperty(Property property) {
      this.property = property;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getXmlDefintion() {
      return xmlDefintion;
    }

    public void setXmlDefintion(String xmlDefintion) {
      this.xmlDefintion = xmlDefintion;
    }
  }

  private class FKDimensionDefinition {
    private final Property property;
    private final String alias;
    private String sqlFilter;
    private final List<Property> primitiveProperties = new ArrayList<Property>();
    private final Map<Property, FKDimensionDefinition> joinedDimensions = new HashMap<Property, Mondrian4SchemaGenerator.FKDimensionDefinition>();
    private boolean mapReferenceAsAttribute = false;
    private final Map<String, Tree> treeAliases = new HashMap<String, Tree>();

    FKDimensionDefinition(Property property) {
      this.property = property;
      alias = getAlias();
    }

    private void addPhysicalElementInformation(Element physicalSchemaElement) {
      // the referenced entity directly
      addPhysicalElement(physicalSchemaElement, property.getTargetEntity(), alias, sqlFilter);

      // add joins to the next level
      for (FKDimensionDefinition fkDefinition : joinedDimensions.values()) {
        fkDefinition.addPhysicalElementInformation(physicalSchemaElement);
      }
      // now add the joins
      for (FKDimensionDefinition fkDefinition : joinedDimensions.values()) {
        final Property refProperty = fkDefinition.property;
        final Element linkElement = physicalSchemaElement.addElement("Link");
        linkElement.addAttribute("target", alias);
        linkElement.addAttribute("source", fkDefinition.alias);
        if (property.getReferencedProperty() != null && !property.getReferencedProperty().isId()) {
          linkElement.addAttribute("key", toUpperOrLowerCase(refProperty.getReferencedProperty()
              .getColumnName()));
          addColumnDef(fkDefinition.alias, toUpperOrLowerCase(refProperty.getReferencedProperty()
              .getColumnName()), null, null);
        }
        linkElement.addAttribute("foreignKeyColumn",
            toUpperOrLowerCase(refProperty.getColumnName()));

        addColumnDef(alias, toUpperOrLowerCase(refProperty.getColumnName()), null, null);
      }

      // add the closure trees
      for (String treeAlias : treeAliases.keySet()) {
        final Tree tree = treeAliases.get(treeAlias);
        addPhysicalElement(physicalSchemaElement,
            ModelProvider.getInstance().getEntity(TreeClosure.ENTITY_NAME), treeAlias, " "
                + treeAlias + ".ad_tree_id = '" + tree.getId() + "'");
      }
    }

    public void generateMapping(Element parentElement) {
      final Element dimensionElement = parentElement.addElement("Dimension");

      measureLinks.add(property);

      dimensionElement.addAttribute("name",
          getDimensionName(property.getEntity(), property.getName()));
      dimensionElement
          .addAttribute("caption", getCaption(property.getEntity(), property.getName()));
      dimensionElement.addAttribute("table", alias);
      dimensionElement.addAttribute("key", property.getName() + "-key");

      Element attributesElement = dimensionElement.element("Attributes");
      if (attributesElement == null) {
        attributesElement = dimensionElement.addElement("Attributes");
      }

      final Element attributeElement = attributesElement.addElement("Attribute")
          .addAttribute("name", property.getName() + "-key").addAttribute("visible", "false")
          .addAttribute("keyColumn", toUpperOrLowerCase(property.getColumnName()))
          .addAttribute("caption", getCaption(property)).addAttribute("hasHierarchy", "false");
      attributeElement.addElement("MemberFormatter").addAttribute("className",
          OBMondrianMemberFormatter.class.getName());

      mapSnowFlakeDimension(dimensionElement);
    }

    private void mapSnowFlakeDimension(Element dimensionElement) {

      if (mapReferenceAsAttribute) {
        addIdentifierAttribute(dimensionElement.element("Attributes"), property, alias);

        final Tree tree = getTree(property.getTargetEntity());
        if (tree != null) {
          Element hierarchiesElement = dimensionElement.element("Hierarchies");
          if (hierarchiesElement == null) {
            hierarchiesElement = dimensionElement.addElement("Hierarchies");
          }
          final String treeAlias = alias + "_" + tree.getTypeArea();
          treeAliases.put(treeAlias, tree);

          final Element levelElement = hierarchiesElement.addElement("Level").addAttribute(
              "attribute", property.getName());
          setLevelName(levelElement, dimensionElement.attributeValue("name"));
          levelElement.addElement("Closure").addAttribute("table", treeAlias)
              .addAttribute("parentColumn", toUpperOrLowerCase("parent_id"))
              .addAttribute("childColumn", toUpperOrLowerCase("child_id"))
              .addAttribute("distanceColumn", toUpperOrLowerCase("distance"));
          levelElement.addElement("MemberFormatter").addAttribute("className",
              OBMondrianMemberFormatter.class.getName());
        }
      }

      for (Property prop : primitiveProperties) {
        mapProperty(dimensionElement, prop);
      }

      for (FKDimensionDefinition fkDefinition : joinedDimensions.values()) {
        fkDefinition.mapSnowFlakeDimension(dimensionElement);
      }
    }

    private void mapProperty(Element dimensionElement, Property prop) {
      if (prop.getDomainType() instanceof EnumerateDomainType) {
        addListAttribute(dimensionElement.element("Attributes"), prop, alias);
      } else if (TimeDimensionProvider.getInstance().isTimeDimensionProperty(prop)) {
        addDateTimeDimensionReferenceAttribute(dimensionElement, alias, prop);
      } else if (prop.getDomainType() instanceof DateDomainType
          || prop.getDomainType() instanceof DatetimeDomainType) {
        addDateTimeComputedAttribute(dimensionElement, alias, prop);
      } else {
        addDefaultAttribute(dimensionElement.element("Attributes"), prop, alias);
      }
    }

    private void addIdentifierAttribute(Element attributesElement, Property prop, String table) {
      final Element attributeElement = attributesElement.addElement("Attribute");
      attributeElement.addAttribute("name", prop.getName()).addAttribute("table", table)
          .addAttribute("keyColumn", getPKColumn(prop.getTargetEntity()))
          .addAttribute("caption", getCaption(prop));
      attributeElement.addElement("MemberFormatter").addAttribute("className",
          OBMondrianMemberFormatter.class.getName());
    }

    public void addDimension(List<Property> properties, int index, String localSqlFilter) {
      if (index == properties.size()) {
        this.sqlFilter = localSqlFilter;
        mapReferenceAsAttribute = true;
        return;
      }
      final Property prop = properties.get(index);
      if (!isCubeableProperty(prop)) {
        return;
      }
      if (TimeDimensionProvider.getInstance().isTimeDimensionProperty(prop)) {
        primitiveProperties.add(prop);
      } else if (prop.getTargetEntity() == null) {
        primitiveProperties.add(prop);
      } else if (joinedDimensions.containsKey(prop)) {
        joinedDimensions.get(prop).addDimension(properties, index + 1, sqlFilter);
      } else {
        final FKDimensionDefinition fkDefinition = new FKDimensionDefinition(prop);
        joinedDimensions.put(prop, fkDefinition);
        fkDefinition.addDimension(properties, index + 1, sqlFilter);
      }
    }
  }

  private class ColumnDef {
    final String column;
    final String sql;
    final String type;

    ColumnDef(String column, String sql, String type) {
      this.column = column;
      this.sql = sql;
      this.type = type;
    }
  }

}