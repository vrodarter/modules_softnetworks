/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.openbravo.client.kernel.KernelUtils;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

/**
 * Preloads the Mondrian cache useing {@link CubeCacheDefinition} instances.
 * 
 * @author mtaal
 */
public class MondrianCachePreloadProcess extends DalBaseProcess {

  private static boolean isDoingCachePreloading = false;

  public static boolean isDoingCachePreloading() {
    return isDoingCachePreloading;
  }

  public void doExecute(ProcessBundle bundle) throws Exception {

    if (!MondrianProvider.getInstance().isMondrianServer()) {
      return;
    }

    // if we are already running go away
    if (isDoingCachePreloading()) {
      return;
    }

    try {
      isDoingCachePreloading = true;
      MondrianProvider.getInstance().flushCacheAndClearConnections();

      final OBQuery<CubeCacheDefinition> cacheDefs = OBDal.getInstance().createQuery(
          CubeCacheDefinition.class, CubeCacheDefinition.PROPERTY_USEINPRELOAD + "=true");
      int cnt = 0;
      for (CubeCacheDefinition cacheDef : cacheDefs.list()) {
        try {
          MondrianSchemaHandler.setCurrentRoleId(cacheDef.getRole().getId());
          try {
            cnt++;
            MondrianProvider.getInstance().executeMDX(
                MondrianProvider.getInstance().getOlapConnection(), cacheDef.getMdx());
            cacheDef.setLastpreloadresult(KernelUtils.getInstance()
                .getI18N("OBANALY_Success", null));
          } catch (Throwable t) {
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            if (sw.toString().length() > 2000) {
              cacheDef.setLastpreloadresult(KernelUtils.getInstance()
                  .getI18N("OBANALY_Error", null) + "\n" + sw.toString().substring(0, 2000));
            } else {
              cacheDef.setLastpreloadresult(KernelUtils.getInstance()
                  .getI18N("OBANALY_Error", null) + "\n" + sw.toString());
            }
          }
        } finally {
          MondrianSchemaHandler.setCurrentRoleId(null);
        }
      }

      // OBError is also used for successful results
      final OBError msg = new OBError();
      msg.setType("Success");
      msg.setTitle("Number of executed mdx: " + cnt);

      bundle.setResult(msg);

    } catch (final Throwable t) {
      t.printStackTrace(System.err);
      final OBError msg = new OBError();
      msg.setType("Error");
      msg.setMessage(t.getMessage());
      msg.setTitle("Error occurred");
      bundle.setResult(msg);
    } finally {
      isDoingCachePreloading = false;
    }
  }
}
