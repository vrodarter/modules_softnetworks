/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import mondrian.spi.CellFormatter;

import org.apache.log4j.Logger;
import org.openbravo.client.kernel.reference.UIDefinitionController;
import org.openbravo.client.kernel.reference.UIDefinitionController.FormatDefinition;

/**
 * Formats a cell value.
 * 
 * @author mtaal
 */
public abstract class MondrianCellFormatter implements CellFormatter {
  private static final Logger log = Logger.getLogger(MondrianCellFormatter.class);

  public static Class<? extends MondrianCellFormatter> getCellFormatterClass(String formatId) {
    if (formatId == "qty") {
      return Quantity.class;
    } else if (formatId == "generalQty") {
      return GeneralQuantity.class;
    } else if (formatId == "euro") {
      return Amount.class;
    } else if (formatId == "price") {
      return Price.class;
    } else if (formatId == "integer") {
      return Integer.class;
    } else {
      log.warn("No cellformatter defined for formatid: " + formatId
          + ", using standard euro format");
      return Amount.class;
    }
  }

  final DecimalFormat decimalFormat = DecimalFormat();

  protected DecimalFormat DecimalFormat() {
    final String formatId = getFormatId();
    final FormatDefinition shortFormat = UIDefinitionController.getInstance().getFormatDefinition(
        formatId, UIDefinitionController.SHORTFORMAT_QUALIFIER);

    final String decimalSymbol = shortFormat.getDecimalSymbol();
    final String groupSymbol = shortFormat.getGroupingSymbol();

    String format = shortFormat.getFormat();
    format = format.replace(",", "_gr_");
    format = format.replace(".", decimalSymbol);
    format = format.replace("_gr_", groupSymbol);

    final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
    symbols.setDecimalSeparator(decimalSymbol.trim().charAt(0));
    symbols.setGroupingSeparator(groupSymbol.trim().charAt(0));

    return new DecimalFormat(format, symbols);
  }

  public abstract String getFormatId();

  @Override
  public synchronized String formatCell(Object value) {
    return decimalFormat.format(value);
  }

  public static class Quantity extends MondrianCellFormatter {
    public String getFormatId() {
      return "qty";
    }
  }

  public static class Percentage extends MondrianCellFormatter {
    public String getFormatId() {
      return "price";
    }

    protected DecimalFormat DecimalFormat() {
      final String formatId = getFormatId();
      final FormatDefinition shortFormat = UIDefinitionController.getInstance()
          .getFormatDefinition(getFormatId(), UIDefinitionController.SHORTFORMAT_QUALIFIER);
      final String decimalSymbol = shortFormat.getDecimalSymbol();
      final String groupSymbol = shortFormat.getGroupingSymbol();
      final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
      symbols.setDecimalSeparator(decimalSymbol.trim().charAt(0));
      symbols.setGroupingSeparator(groupSymbol.trim().charAt(0));
      String format = "0.#%";
      return new DecimalFormat(format, symbols);
    }
  }

  public static class GeneralQuantity extends MondrianCellFormatter {
    public String getFormatId() {
      return "generalQty";
    }
  }

  public static class Amount extends MondrianCellFormatter {
    public String getFormatId() {
      return "euro";
    }
  }

  public static class Price extends MondrianCellFormatter {
    public String getFormatId() {
      return "price";
    }
  }

  public static class Integer extends MondrianCellFormatter {
    public String getFormatId() {
      return "integer";
    }
  }
}
