/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

/**
 * Contains the constants used by the OB-Mondrian code.
 * 
 * @author mtaal
 */
public class MondrianConstants {
  public static final String CATALOG_SCHEMA_NAME = "Openbravo";
  public static final String DAY_LEVEL_NAME = "Day";
  public static final String MONTH_LEVEL_NAME = "Month";
  public static final String QUARTER_LEVEL_NAME = "Quarter";
  public static final long MDX_CACHE_TRESHOLD = 60000;

  public static final String MDX_CACHE_TRESHOLD_PROP_NAME = "analytics.cache_treshold";
  public static final String MDX_PRELOAD_ENABLED_TRESHOLD_PROP_NAME = "analytics.preload_enabled_treshold";

  public static final int MDX_PRELOAD_ENABLED_TRESHOLD = 10;
  public static final String MONDRIAN_SERVER_PROPERTY = "mondrian.server";
  // random content, see the usage of this constant to understand its usage
  public static final String MONDRIAN_QRY_TAGGER = "__;;__";

}
