/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Properties;

import mondrian.olap.CacheControl;
import mondrian.olap.Connection;
import mondrian.olap.MondrianProperties;
import mondrian.olap.MondrianServer;
import mondrian.olap.Util;
import mondrian.rolap.RolapConnection;
import mondrian.rolap.RolapConnectionProperties;
import mondrian.server.RepositoryContentFinder;
import mondrian.spi.impl.CatalogLocatorImpl;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.olap4j.CellSet;
import org.olap4j.OlapConnection;
import org.olap4j.OlapStatement;
import org.olap4j.mdx.SelectNode;
import org.olap4j.mdx.parser.MdxParser;
import org.olap4j.mdx.parser.MdxParserFactory;
import org.olap4j.mdx.parser.MdxValidator;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;

/**
 * Provides access to a mondrian server.
 * 
 * TODO: add connection pooling
 * 
 * @author mtaal
 */
public class MondrianProvider {
  private static final Logger log = Logger.getLogger(MondrianProvider.class);

  private static boolean runningInWebContainer = true;
  private static MondrianProvider instance = new MondrianProvider();

  public static MondrianProvider getInstance() {
    return instance;
  }

  public static void setInstance(MondrianProvider instance) {
    MondrianProvider.instance = instance;
  }

  public static boolean isRunningInWebContainer() {
    return runningInWebContainer;
  }

  public static void setRunningInWebContainer(boolean runningInWebContainer) {
    MondrianProvider.runningInWebContainer = runningInWebContainer;
  }

  public static boolean allowFinanceCube() {
    final Entity elementValueEntity = ModelProvider.getInstance().getEntity(
        ElementValue.ENTITY_NAME);
    return OBContext.getOBContext().getEntityAccessChecker().getReadableEntities()
        .contains(elementValueEntity);
  }

  private Util.PropertyList mondrianProperties = null;
  private MondrianServer mondrianServer = null;

  public boolean isMondrianServer() {
    final String prop = OBPropertiesProvider.getInstance().getOpenbravoProperties()
        .getProperty(MondrianConstants.MONDRIAN_SERVER_PROPERTY);
    // property not set so as a default allow mondrian
    if (prop == null || prop.trim().length() == 0) {
      return true;
    }
    return OBPropertiesProvider.getInstance().getBooleanProperty(
        MondrianConstants.MONDRIAN_SERVER_PROPERTY);
  }

  public Connection getMondrianConnection() {
    return getMondrianConnection(null);
  }

  public void flushCacheAndClearConnections() {
    OBSaikuConnectionManager.getObSaikuConnectionManager().clearAllConnections();
    OBSaikuQueryService.clearCachedQueries();

    // no cached schemas, not yet used, nothing to clear flush
    if (!MondrianSchemaHandler.getInstance().hasCachedSchemas()) {
      return;
    }
    flushCache();
  }

  public void flushCache() {
    // no cached schemas, not yet used, nothing to clear flush
    if (!MondrianSchemaHandler.getInstance().hasCachedSchemas()) {
      return;
    }
    final Connection mondrianConnection = getMondrianConnection();
    final CacheControl cacheControl = mondrianConnection.getCacheControl(null);
    cacheControl.flushSchemaCache();
    MondrianSchemaHandler.getInstance().clearCache();
  }

  public OlapConnection getOlapConnection() {
    return getOlapConnection(null);
  }

  /**
   * Returns an olap connection, NOTE: close the connection in a finally block.
   * 
   * @param extraProps
   * @param catalog
   * @return an {@link OlapConnection} to use
   */
  public OlapConnection getOlapConnection(Properties extraProps) {

    initializeMondrianProperties();

    if (!isMondrianServer()) {
      throw new OBMondrianDifferentServerException();
    }

    try {
      Class.forName("mondrian.olap4j.MondrianOlap4jDriver");
      final Properties props = getOpenbravoProperties();
      props.setProperty("mondrian.rolap.ignoreInvalidMembersDuringQuery", "true");
      props.setProperty(RolapConnectionProperties.Catalog.name(),
          MondrianConstants.CATALOG_SCHEMA_NAME);
      props.put(RolapConnectionProperties.DynamicSchemaProcessor.name(),
          OpenbravoDynamicSchemaProcessor.class.getName());
      if (extraProps != null) {
        props.putAll(extraProps);
      }

      MondrianProperties.instance().setProperty("mondrian.rolap.ignoreInvalidMembersDuringQuery",
          "true");

      props.put(RolapConnectionProperties.UseContentChecksum.name(), "true");
      props.put(RolapConnectionProperties.JdbcConnectionUuid.name(), "Openbravo");

      java.sql.Connection connection = java.sql.DriverManager.getConnection("jdbc:mondrian:jdbc="
          + props.getProperty(RolapConnectionProperties.Jdbc.name()), props);

      // wrap the connection to measure MDX execution times and store MDX queries
      final OlapConnection olapConnection = connection.unwrap(OlapConnection.class);
      return olapConnection;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public CellSet executeMDX(OlapConnection olapConnection, String mdxQryStr) {
    try {
      OlapConnection validateOlapConnection = olapConnection;

      // validate the query
      MdxParserFactory pFactory = validateOlapConnection.getParserFactory();
      MdxParser parser = pFactory.createMdxParser(validateOlapConnection);
      MdxValidator validator = pFactory.createMdxValidator(validateOlapConnection);
      SelectNode parsedObject = parser.parseSelect(mdxQryStr);
      validator.validateSelect(parsedObject);

      // execute it
      final OlapStatement statement = olapConnection.createStatement();
      // not supported by Mondrian
      // statement.setFetchDirection(ResultSet.FETCH_FORWARD);
      // statement.setFetchSize(5000);
      final CellSet cellSet = statement.executeOlapQuery(mdxQryStr);
      return cellSet;
    } catch (Exception e) {
      throw new OBException("Exception when executing mdx " + mdxQryStr, e);
    }
  }

  public Connection getMondrianConnection(Properties props) {

    if (!isMondrianServer()) {
      throw new OBMondrianDifferentServerException();
    }

    final Util.PropertyList copyProps = getMondrianProperties().clone();
    if (props != null) {
      for (Object o : props.keySet()) {
        final String key = (String) o;
        copyProps.put(key, props.getProperty(key));
      }
    }
    final RolapConnection connection = new RolapConnection(getMondrianServer(), copyProps, null);
    getMondrianServer().addConnection(connection);
    return connection;
  }

  private Util.PropertyList getMondrianProperties() {
    // initialize stuff
    getMondrianServer();
    return mondrianProperties;
  }

  private synchronized void initializeMondrianProperties() {
    if (mondrianProperties != null) {
      return;
    }

    mondrianProperties = new Util.PropertyList();
    final Properties obProps = getOpenbravoProperties();
    for (Object object : obProps.keySet()) {
      mondrianProperties.put((String) object, obProps.getProperty((String) object));
    }
    mondrianProperties.put(RolapConnectionProperties.Provider.name(), "mondrian");
    mondrianProperties.put(RolapConnectionProperties.Catalog.name(),
        MondrianConstants.CATALOG_SCHEMA_NAME);
    mondrianProperties.put(RolapConnectionProperties.DynamicSchemaProcessor.name(),
        OpenbravoDynamicSchemaProcessor.class.getName());

    try {
      InputStream is = this.getClass().getResourceAsStream("/mondrian.properties");
      // if mondrian.properties exist leave it to Mondrian to read it
      if (is != null) {
        is.close();
        return;
      }
      // read the default
      is = this.getClass().getResourceAsStream("/mondrian.default.properties");

      MondrianProperties.instance().load(is);
      is.close();
    } catch (IOException e) {
      throw new OBException(e);
    }
  }

  public MondrianServer getMondrianServer() {
    if (mondrianServer == null) {
      mondrianServer = createMondrianServer();
    }
    return mondrianServer;
  }

  private synchronized MondrianServer createMondrianServer() {
    if (mondrianServer != null) {
      return mondrianServer;
    }
    initializeMondrianProperties();

    return MondrianServer.createWithRepository(new OBRepositoryFinder(), new CatalogLocatorImpl());
  }

  protected Properties getOpenbravoProperties() {
    final Properties props = new Properties();
    final Properties obProps = OBPropertiesProvider.getInstance().getOpenbravoProperties();
    if (obProps == null) {
      return new Properties();
    }

    if (obProps.getProperty("bbdd.rdbms") != null) {
      if (obProps.getProperty("bbdd.rdbms").equals("POSTGRE")) {
        return getPostgresHbProps(obProps);
      } else if (obProps.getProperty("bbdd.rdbms").equals("DB2")) {
        return getDB2HbProps(obProps);
      }

      return getOracleHbProps(obProps);
    }
    return props;
  }

  private Properties getPostgresHbProps(Properties obProps) {
    final Properties props = new Properties();
    if (isJNDIModeOn(obProps)) {
      setJNDI(obProps, props);
    } else {
      props.setProperty(RolapConnectionProperties.JdbcDrivers.name(), "org.postgresql.Driver");
      props.setProperty(RolapConnectionProperties.Jdbc.name(), obProps.getProperty("bbdd.url")
          + "/" + obProps.getProperty("bbdd.sid"));

      props
          .setProperty(RolapConnectionProperties.JdbcUser.name(), obProps.getProperty("bbdd.user"));
      props.setProperty(RolapConnectionProperties.JdbcPassword.name(),
          obProps.getProperty("bbdd.password"));
    }
    return props;
  }

  private Properties getOracleHbProps(Properties obProps) {
    final Properties props = new Properties();
    if (isJNDIModeOn(obProps)) {
      setJNDI(obProps, props);
    } else {
      props.setProperty(RolapConnectionProperties.JdbcDrivers.name(),
          "oracle.jdbc.driver.OracleDriver");
      props.setProperty(RolapConnectionProperties.Jdbc.name(), obProps.getProperty("bbdd.url"));
      props
          .setProperty(RolapConnectionProperties.JdbcUser.name(), obProps.getProperty("bbdd.user"));
      props.setProperty(RolapConnectionProperties.JdbcPassword.name(),
          obProps.getProperty("bbdd.password"));
    }
    return props;
  }

  // NOTE: DB2 support is not complete in Openbravo, keeping it here
  // anyway for anyone wanting to add it in the future
  private Properties getDB2HbProps(Properties obProps) {
    final Properties props = new Properties();
    if (isJNDIModeOn(obProps)) {
      setJNDI(obProps, props);
    } else {
      props.setProperty(RolapConnectionProperties.JdbcDrivers.name(), "com.ibm.db2.jcc.DB2Driver");
      props.setProperty(RolapConnectionProperties.Jdbc.name(), obProps.getProperty("bbdd.url"));
      props
          .setProperty(RolapConnectionProperties.JdbcUser.name(), obProps.getProperty("bbdd.user"));
      props.setProperty(RolapConnectionProperties.JdbcPassword.name(),
          obProps.getProperty("bbdd.password"));
    }
    return props;
  }

  private void setJNDI(Properties obProps, Properties hbProps) {
    hbProps.setProperty(RolapConnectionProperties.DataSource.name(),
        "java:/comp/env/" + obProps.getProperty("JNDI.resourceName"));
  }

  // jndi should only be used if the application is running in a webcontainer
  // in all other cases (ant etc.) jndi should not be used, but the direct
  // openbravo.properties
  // should be used.
  private boolean isJNDIModeOn(Properties obProps) {
    if (!isRunningInWebContainer()) {
      return false;
    }
    return ("yes".equals(obProps.getProperty("JNDI.usage")) ? true : false);
  }

  public String getDataSourceInfo() {
    final Properties props = getOpenbravoProperties();
    final StringBuilder dsInfo = new StringBuilder();
    dsInfo.append("Provider=mondrian;");
    dsInfo.append("Jdbc=" + props.getProperty(RolapConnectionProperties.Jdbc.name()) + ";");
    dsInfo.append("JdbcUser=" + props.getProperty(RolapConnectionProperties.JdbcUser.name()) + ";");
    dsInfo.append("JdbcPassword="
        + props.getProperty(RolapConnectionProperties.JdbcPassword.name()) + ";");
    dsInfo.append("JdbcDrivers=" + props.getProperty(RolapConnectionProperties.JdbcDrivers.name())
        + ";");
    dsInfo.append("DynamicSchemaProcessor=" + OpenbravoDynamicSchemaProcessor.class.getName());
    return dsInfo.toString();
  }

  public void checkAccessToCatalog(String roleId, String catalog) {
    // Map<String, String> schemas = MondrianSchemaHandler.getInstance().getSchemas(roleId);
    // if (!schemas.containsKey(catalog)) {
    // throw new OBException("Not authorized to access catalog " + catalog);
    // }
  }

  private class OBRepositoryFinder implements RepositoryContentFinder {

    @Override
    public String getContent() {
      return createDataSourceXML();
    }

    private String createDataSourceXML() {
      final Document document = DocumentHelper.createDocument();
      document.setXMLEncoding("UTF-8");
      final Element rootElement = document.addElement("DataSources");
      final Element dsElement = rootElement.addElement("DataSource");
      dsElement.addElement("DataSourceName").addText("Provider=Mondrian;DataSource=Openbravo;");
      dsElement.addElement("DataSourceDescription").addText(
          "The Openbravo XMLA/OLAP Data Warehouse");
      dsElement.addElement("URL").addText("http://localhost:8080/openbravo/xmla");
      dsElement.addElement("DataSourceInfo").addText(getDataSourceInfo());
      dsElement.addElement("ProviderName").addText("Mondrian");
      dsElement.addElement("ProviderType").addText("MDP");
      dsElement.addElement("AuthenticationMode").addText("Unauthenticated");

      final Element catalogsElement = dsElement.addElement("Catalogs");
      final Element catalogElement = catalogsElement.addElement("Catalog");
      catalogElement.addAttribute("name", MondrianConstants.CATALOG_SCHEMA_NAME)
          .addElement("DataSourceInfo").addText(getDataSourceInfo());
      catalogElement.addElement("Definition").addText("Openbravo");

      try {
        final StringWriter sw = new StringWriter();
        final OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter writer = new XMLWriter(sw, format);
        writer.write(document);
        writer.close();
        return sw.toString().trim();
      } catch (Exception e) {
        throw new OBException(e);
      }
    }

    @Override
    public void shutdown() {
    }
  }
}
