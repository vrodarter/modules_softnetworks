/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBSecurityException;
import org.openbravo.base.model.AccessLevel;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.model.domaintype.BigDecimalDomainType;
import org.openbravo.base.model.domaintype.EnumerateDomainType;
import org.openbravo.base.model.domaintype.ForeignKeyDomainType;
import org.openbravo.base.model.domaintype.LongDomainType;
import org.openbravo.base.model.domaintype.PrimitiveDomainType;
import org.openbravo.base.model.domaintype.StringDomainType;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.util.Check;
import org.openbravo.client.analytics.TimeDimensionProvider.TimeDimensionEntry;
import org.openbravo.client.application.window.OBViewUtil;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.security.EntityAccessChecker;
import org.openbravo.dal.security.OrganizationStructureProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.access.RoleOrganization;
import org.openbravo.model.ad.datamodel.Column;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Tree;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.project.Project;

/**
 * Generates a Mondrian schema for one or more {@link CubeDefinition}.
 * 
 * NOTE: when working on generating the Mondrian schema the order of the elements in the xml should
 * exactly follow the Mondrian schema definition. A wrong order means that elements are ignore, no
 * exception or warning is given.
 * 
 * @author mtaal
 */
public class MondrianSchemaGenerator {

  public static final String ANNOTATION_ENTITY = "entity";
  public static final String ANNOTATION_LISTPROPERTY = "listProperty";
  public static final String ANNOTATION_YESNOPROPERTY = "yesNoProperty";
  public static final String ANNOTATION_DATEFILTERMEMBER = "DateFilterMEMBER";

  private static String DIALECT = "generic"; // "postgresql";

  protected static final Logger log = Logger.getLogger(MondrianSchemaGenerator.class);

  public static final boolean isOracle = OBPropertiesProvider.getInstance()
      .getOpenbravoProperties().getProperty("bbdd.rdbms").equals("ORACLE");

  protected static final String CUBE_KEY_SEPARATOR = "||";

  private int aliasCnt = 0;

  private CubeDimension currentCubeDimension = null;

  private Role currentRole = null;

  private List<String> readableOrganizations = null;

  private String aliasPrefix = null;

  public synchronized void generate(Document document, Role role) {

    final Element rootElement = document.addElement("Schema")
        .addAttribute("name", MondrianConstants.CATALOG_SCHEMA_NAME)
        .addAttribute("metamodelVersion", "3.6");

    Check.isNotNull(aliasPrefix, "AliasPrefix should be set");

    currentRole = role;
    EntityAccessChecker entityAccessChecker = OBContext.getOBContext().getEntityAccessChecker();
    if (currentRole != OBContext.getOBContext().getRole()) {
      entityAccessChecker = OBProvider.getInstance().get(EntityAccessChecker.class);
      entityAccessChecker.setRoleId((String) DalUtil.getId(currentRole));
      entityAccessChecker.setObContext(OBContext.getOBContext());
      entityAccessChecker.initialize();
    }

    OBContext.setAdminMode(true);
    try {

      final OBQuery<CubeDefinition> cubeQry = OBDal.getInstance().createQuery(CubeDefinition.class,
          null);
      for (CubeDefinition cubeDefinition : cubeQry.list()) {
        if (!cubeDefinition.isActive() || cubeDefinition.isTemplate()) {
          continue;
        }

        final Entity entity = ModelProvider.getInstance().getEntityByTableId(
            cubeDefinition.getTable().getId());
        try {
          // Don't check the entity readability with the admin mode activated
          OBContext.restorePreviousMode();
          entityAccessChecker.checkReadable(entity);
        } catch (OBSecurityException ignored) {
          continue;
        } finally {
          OBContext.setAdminMode(true);
        }

        if (cubeDefinition.getXmldefinition() != null) {
          parseXmlAndAdd(rootElement, "Cube", cubeDefinition.getXmldefinition());
          continue;
        }
        final Element cubeElement = rootElement.addElement("Cube")
            .addAttribute("name", cubeDefinition.getId())
            .addAttribute("caption", cubeDefinition.getName());

        // use a View element because of this Mondrian issue:
        // http://jira.pentaho.com/browse/MONDRIAN-1073
        final String viewAlias = getAlias();
        final Element viewElement = cubeElement.addElement("View")
            .addAttribute("alias", getAlias());
        final String firstPartOfViewSQL = "select * from "
            + toUpperOrLowerCase(entity.getTableName()) + " " + viewAlias + " where "
            + getClientOrgFilter(entity, viewAlias);
        if (cubeDefinition.getSqlfilter() != null) {
          final Element sqlElement = viewElement.addElement("SQL").addAttribute("dialect", DIALECT);
          sqlElement.addText(firstPartOfViewSQL + " and ("
              + cubeDefinition.getSqlfilter().replace("[alias]", viewAlias) + ")");
        } else {
          final Element sqlElement = viewElement.addElement("SQL").addAttribute("dialect", DIALECT);
          sqlElement.addText(firstPartOfViewSQL);
        }

        final List<Measure> measuresList = new ArrayList<Measure>();
        int cnt = 0;
        for (CubeDimension cubeDimension : getCubeDimensionsOrdered(cubeDefinition)) {
          if (!cubeDimension.isActive()) {
            continue;
          }
          try {
            final String modelElement = cubeDimension.getProperty();
            final Property property = getLastProperty(entity, modelElement);
            if (property == null) {
              log.error("The dimension with model element " + modelElement + " from cube "
                  + cubeDefinition.getName()
                  + " does not resolve to a valid property and it will be ignored");
              continue;
            }
            if (!isCubeableProperty(property)) {
              log.error("The dimension with model element " + modelElement + " from cube "
                  + cubeDefinition.getName() + " is not cubeable and it will be ignored");
              continue;
            }
            currentCubeDimension = cubeDimension;
            if (cubeDimension.isMeasure()) {
              Measure measure = new Measure();
              measure.setProperty(property);
              measure.setName(cubeDimension.getName());
              measure.setXmlDefintion(cubeDimension.getXmldefinition());
              measure.setMeasureExpression(cubeDimension.getMeasureExpression());
              measure.setIsCalculatedMember(cubeDimension.isCalculatedMember());
              measure.setFormula(cubeDimension.getFormula());
              if (cubeDimension.getAggregatorFunction() != null) {
                measure.setAggregator(cubeDimension.getAggregatorFunction());
              }
              measuresList.add(measure);
            } else if (cubeDimension.getXmldefinition() != null
                && !cubeDimension.getXmldefinition().equals("")) {
              parseXmlAndAdd(cubeElement, "Dimension", cubeDimension.getXmldefinition());
              cnt++;
            } else if (TimeDimensionProvider.getInstance().isTimeDimensionProperty(property)
                && !TimeDimensionProvider.getInstance().hasTimeDimensionEntry(property)) {
              cnt++;

              // create a fake mapping
              final TimeDimensionEntry dummyEntry = new TimeDimensionEntry();
              dummyEntry.setOriginalTimeProperty(property);
              dummyEntry.setTimeDimensionProperty(property);
              createModeledDateTimeReferenceDimension(cubeElement, entity, modelElement,
                  dummyEntry, cubeDimension);
            } else if (property.isDate() || property.isDatetime()) {
              cnt++;
              createDateTimeReferenceDimension(cubeElement, entity, modelElement, cubeDimension);
            } else if (property.getDomainType() instanceof ForeignKeyDomainType) {
              cnt++;
              createReferenceDimension(cubeElement, entity, modelElement,
                  cubeDimension.getSqlfilter(), cubeDimension);
            } else if (property.isPrimitive()) {
              cnt++;
              createPrimitiveReferenceDimension(cubeElement, entity, modelElement,
                  cubeDimension.getSqlfilter(), cubeDimension);
            } else {
              throw new IllegalStateException("Property " + property + " not supported");
            }
          } catch (Throwable reportThrowable) {
            log.error(reportThrowable.getMessage(), reportThrowable);
          }
        }
        createMeasurements(cubeElement, measuresList);

        // nothing to measure, remove again...
        if (cnt == 0 || measuresList.size() == 0) {
          rootElement.remove(cubeElement);
        }
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private List<CubeDimension> getCubeDimensionsOrdered(CubeDefinition cubeDefinition) {
    OBCriteria<CubeDimension> obc = OBDal.getInstance().createCriteria(CubeDimension.class);
    obc.add(Restrictions.eq(CubeDimension.PROPERTY_OBANALYCUBEDEFINITION, cubeDefinition));
    obc.addOrder(Order.asc(CubeDimension.PROPERTY_LINENO));
    return obc.list();
  }

  protected void parseXmlAndAdd(Element parentElement, String selectNode, String xml) {
    Document auxDoc = null;
    try {
      auxDoc = DocumentHelper.parseText(xml);
      // silently ignore if not present, is expected that way by some code
      if (auxDoc.selectSingleNode(selectNode) != null) {
        parentElement.add(auxDoc.selectSingleNode(selectNode));
      }
    } catch (DocumentException e) {
      log.error("MondrianSchemaGenerator.generate() parsing xml " + xml, e);
    }
  }

  protected boolean hasCubeLevelsDefined() {
    if (currentCubeDimension == null || currentCubeDimension.getOBANALYCubeLevelList().isEmpty()) {
      return false;
    }
    return true;
  }

  protected CubeLevel getCubeLevel(String name) {
    if (currentCubeDimension == null || currentCubeDimension.getOBANALYCubeLevelList().isEmpty()) {
      return null;
    }
    for (CubeLevel cubeLevel : currentCubeDimension.getOBANALYCubeLevelList()) {
      if (cubeLevel.getName().equals(name)) {
        return cubeLevel;
      }
    }
    return null;
  }

  protected String getAlias() {
    aliasCnt++;
    return getAliasForOracleOrPostgres(getAliasPrefix() + aliasCnt);
  }

  protected String getClientOrgFilter(Entity targetEntity, String alias) {
    final AccessLevel accessLevel = targetEntity.getAccessLevel();
    switch (accessLevel) {
    case SYSTEM:
      return null;
    case SYSTEM_CLIENT:
      return alias + toUpperOrLowerCase(".ad_client_id='")
          + OBContext.getOBContext().getCurrentClient().getId() + "' or " + alias
          + toUpperOrLowerCase(".ad_client_id='0'");
    default:
      final StringBuilder sb = new StringBuilder();
      for (String orgId : getReadableOrganizations()) {
        if (sb.length() > 0) {
          sb.append(",");
        }
        sb.append("'" + orgId + "'");
      }

      return alias + toUpperOrLowerCase(".ad_org_id in (") + sb.toString() + ")";
    }
  }

  protected boolean isCubeableProperty(Property property) {
    if (property.isId()) {
      return false;
    }
    if (property.isBoolean()) {
      return true;
    }
    if (property.isAuditInfo()) {
      return false;
    }
    if (property.isEncrypted()) {
      return false;
    }
    if (property.isOneToMany()) {
      return false;
    }
    if (property.isInactive()) {
      return false;
    }
    if (property.isTransient()) {
      return false;
    }
    if (property.getSqlLogic() != null) {
      return false;
    }
    if (property.getTargetEntity() != null) {
      if (Client.ENTITY_NAME.equals(property.getTargetEntity().getName())) {
        return false;
      }
      if (AttributeSet.ENTITY_NAME.equals(property.getTargetEntity().getName())) {
        return false;
      }
      if (AttributeSetInstance.ENTITY_NAME.equals(property.getTargetEntity().getName())) {
        return false;
      }
      if (Location.ENTITY_NAME.equals(property.getTargetEntity().getName())) {
        return false;
      }
    }
    if (property.isDate() || property.isDatetime()) {
      return true;
    }
    // a measurement
    if (property.getDomainType() instanceof BigDecimalDomainType) {
      return true;
    }
    if (property.getDomainType() instanceof LongDomainType) {
      return true;
    }
    if (property.getDomainType() instanceof EnumerateDomainType) {
      return true;
    }
    if (property.getDomainType() instanceof StringDomainType) {
      return true;
    }
    // ignore all other primitives
    if (property.isPrimitive()) {
      return false;
    }
    return true;
  }

  protected Property getLastProperty(Entity entity, String propertyPath) {
    final String[] propertyNames = propertyPath.split("\\.");
    Entity currentEntity = entity;
    Property currentProperty = null;
    for (String propertyName : propertyNames) {
      if (!currentEntity.hasProperty(propertyName)) {
        log.error("Property path " + propertyPath + " from entity " + entity.getName()
            + " does not resolve to a valid property");
        return null;
      }
      currentProperty = currentEntity.getProperty(propertyName);
      if (currentProperty.getTargetEntity() != null) {
        currentEntity = currentProperty.getTargetEntity();
      }
    }
    return currentProperty;
  }

  protected String getPrimaryKeyTable(Property property) {
    if (property.getTargetEntity() == null) {
      return toUpperOrLowerCase(property.getEntity().getTableName());
    } else {
      return toUpperOrLowerCase(property.getTargetEntity().getTableName());
    }
  }

  protected Property getFirstProperty(Entity entity, String propertyPath) {
    final String[] propertyNames = propertyPath.split("\\.");
    if (!entity.hasProperty(propertyNames[0])) {
      log.warn("Property path " + propertyPath + " from entity " + entity.getName()
          + " does not have a valid first property " + propertyNames[0]);
      return null;
    }
    return entity.getProperty(propertyNames[0]);
  }

  // TODO: translate list reference level names
  protected void createPrimitiveReferenceDimension(Element parentElement, Entity entity,
      String propertyPath, String sqlFilter, CubeDimension cubeDimension) {
    final Property property = getLastProperty(entity, propertyPath);
    // be robust
    if (property == null) {
      return;
    }
    final Element dimensionElement = parentElement.addElement("Dimension").addAttribute("name",
        getDimensionName(entity, propertyPath, cubeDimension));
    dimensionElement.addAttribute("caption", getCaption(entity, propertyPath, cubeDimension));

    final Property[] properties = getPropertiesOnPath(entity, propertyPath);
    final Property firstProperty = properties[0];
    final boolean isJoin = propertyPath.contains(".");

    final Element hierarchyElement = dimensionElement.addElement("Hierarchy").addAttribute(
        "hasAll", "true");
    if (isJoin && firstProperty.getTargetEntity() != null) {
      addPrimaryAndForeigKeyToElements(dimensionElement, hierarchyElement, firstProperty);
    }

    // create joins and intermediate levels, if any
    String tableName = createTableJoinsAndLevels(hierarchyElement, entity, propertyPath, sqlFilter,
        null);
    final Element levelElement = hierarchyElement.addElement("Level")
        .addAttribute("name", property.getName()).addAttribute("table", tableName)
        .addAttribute("column", toUpperOrLowerCase(property.getColumnName()))
        .addAttribute("caption", getCaption(property))
        .addAttribute("uniqueMembers", Boolean.toString(!hasLevels(propertyPath)));

    // annotate the level element so that the memberformatter can read the correct translated value
    if (property.getDomainType() instanceof EnumerateDomainType) {
      levelElement.addElement("Annotations").addElement("Annotation")
          .addAttribute("name", ANNOTATION_LISTPROPERTY)
          .addText(property.getEntity().getName() + "." + property.getName());
    }
    if (property.isBoolean()) {
      levelElement.addElement("Annotations").addElement("Annotation")
          .addAttribute("name", ANNOTATION_YESNOPROPERTY)
          .addText(property.getEntity().getName() + "." + property.getName());
    }

    levelElement.addElement("MemberFormatter").addAttribute("className",
        OBMondrianMemberFormatter.class.getName());

    setLevelName(levelElement, property.getName());
    setHierarchyName(hierarchyElement, property.getName());
  }

  private boolean hasLevels(String propertyPath) {
    return propertyPath.indexOf("\\.") != -1;
  }

  protected String createTableJoinsAndLevels(Element hierarchyElement, Entity entity,
      String propertyPath, String sqlFilter, CubeDimension cubeDimension) {
    String lastTableName = null;
    Element currentParentElement = hierarchyElement;
    final Property[] props = getPropertiesOnPath(entity, propertyPath);
    final Property lastJoinProperty = computeLastJoinProperty(props);

    if (lastJoinProperty == null) {
      return null;
    }

    final boolean lastPropertyIsDateTimeOrDate = props[props.length - 1].isDatetime()
        || props[props.length - 1].isDate()
        || TimeDimensionProvider.getInstance().isTimeDimensionProperty(props[props.length - 1]);

    int index = 0;

    // is used to determine if a parent-child hierarchy can be used, this only
    // makes sense in the lowest level in the hierarchy
    boolean isLowestLevelInHierarchy = true;
    for (Property property : props) {
      final TimeDimensionEntry timeDimensionEntry = TimeDimensionProvider.getInstance()
          .getTimeDimensionEntry(property);

      if (timeDimensionEntry != null) {
        property = timeDimensionEntry.getTimeDimensionProperty();
      }

      if (property.isPrimitive()) {
        continue;
      }
      Property nextProperty = null;
      if (index < (props.length - 1)) {
        nextProperty = props[index + 1];
      }

      final String alias = getAlias();

      Element tableElement = null;

      if (currentParentElement.getName().equals("Join")) {
        currentParentElement.attributeValue("rightAlias", alias);
      }

      if (nextProperty != null) {
        TimeDimensionEntry nextTimeDimensionEntry = TimeDimensionProvider.getInstance()
            .getTimeDimensionEntry(nextProperty);
        if (nextTimeDimensionEntry != null) {
          nextProperty = nextTimeDimensionEntry.getTimeDimensionProperty();
        }
      }

      // add a join in these cases
      // 1) the nextProperty is not primitive
      // 2) the nextProperty is in reality a time dimension property
      boolean isLeftJoin = false;
      if (nextProperty != null && nextProperty.getTargetEntity() != null) {
        Property referencedProperty;
        if (nextProperty.getReferencedProperty() != null) {
          referencedProperty = nextProperty.getReferencedProperty();
        } else {
          referencedProperty = nextProperty.getTargetEntity().getIdProperties().get(0);
        }
        currentParentElement = currentParentElement.addElement("Join")
            .addAttribute("leftKey", toUpperOrLowerCase(nextProperty.getColumnName()))
            .addAttribute("rightKey", toUpperOrLowerCase(referencedProperty.getColumnName()));
        isLeftJoin = true;
      }
      final Entity targetEntity = property.getTargetEntity();

      final String tableName = toUpperOrLowerCase(targetEntity.getTableName());
      tableElement = currentParentElement.addElement("Table").addAttribute("name", tableName)
          .addAttribute("alias", alias);
      if (currentParentElement.getName().equals("Hierarchy")) {
        currentParentElement.addAttribute("primaryKeyTable", alias);
      }

      // client org filter depends on the accesslevel of the target entity
      // systemonly will return null. systemclient will only return 0 and the client
      final String clientOrgFilter = getClientOrgFilter(targetEntity, alias);
      if (clientOrgFilter == null) {
        if (property.equals(lastJoinProperty) && sqlFilter != null) {
          Element sql = tableElement.addElement("SQL").addAttribute("dialect", DIALECT);
          sql.setText(sqlFilter.replace("[alias]", alias));
        }
      } else if (property.equals(lastJoinProperty) && sqlFilter != null) {
        Element sql = tableElement.addElement("SQL").addAttribute("dialect", DIALECT);
        sql.setText(clientOrgFilter + " and " + sqlFilter.replace("[alias]", alias));
      } else {
        tableElement.addElement("SQL").addAttribute("dialect", DIALECT).setText(clientOrgFilter);
      }

      lastTableName = alias;
      if (currentParentElement.getName().equals("Join")) {
        if (isLeftJoin) {
          currentParentElement.addAttribute("leftAlias", alias);
          if (currentParentElement.getParent().getName().equals("Join")) {
            currentParentElement.getParent().addAttribute("rightAlias", alias);
          } else if (currentParentElement.getParent().getName().equals("Hierarchy")) {
            currentParentElement.getParent().addAttribute("primaryKeyTable", alias);
          }
        } else {
          currentParentElement.addAttribute("rightAlias", alias);
        }
      }

      index++;
      // don't add the level for timedimensions that's done in another method
      if (!lastPropertyIsDateTimeOrDate) {

        final boolean visible;
        if (hasCubeLevelsDefined() && null == getCubeLevel(property.getName())) {
          visible = false;
        } else {
          visible = true;
        }

        if (visible && cubeDimension != null) {
          addReferenceLevel(isLowestLevelInHierarchy, alias, property, hierarchyElement,
              tableElement, cubeDimension, visible);
          isLowestLevelInHierarchy = false;
        }
      }
    }

    return lastTableName;
  }

  protected void addPrimaryAndForeigKeyToElements(Element dimensionElement,
      Element hierarchyElement, Property firstProperty) {
    final Entity targetEntity = firstProperty.getTargetEntity();
    if (targetEntity == null) {
      return;
    }
    hierarchyElement.addAttribute(
        "primaryKey",
        firstProperty.getReferencedProperty() == null ? toUpperOrLowerCase(targetEntity
            .getIdProperties().get(0).getColumnName()) : toUpperOrLowerCase(firstProperty
            .getReferencedProperty().getColumnName())).addAttribute("primaryKeyTable",
        getPrimaryKeyTable(firstProperty));
    dimensionElement.addAttribute("foreignKey", toUpperOrLowerCase(firstProperty.getColumnName()));
  }

  @SuppressWarnings("unchecked")
  protected void addReferenceLevel(boolean isLowestLevelInHierarchy, String alias,
      Property property, Element hierarchyElement, Element tableElement,
      CubeDimension cubeDimension, boolean isVisible) {

    final Entity targetEntity = property.getTargetEntity();
    final String idColumnName = toUpperOrLowerCase(targetEntity.getIdProperties().get(0)
        .getColumnName());
    final Element levelElement = hierarchyElement.addElement("Level")
        .addAttribute("name", property.getName()).addAttribute("column", idColumnName)
        .addAttribute("caption", getCaption(property)).addAttribute("table", alias);

    if (!isVisible) {
      levelElement.addAttribute("visible", "false");
    }

    levelElement.addAttribute("uniqueMembers", "true");

    levelElement.addElement("Annotations").addElement("Annotation")
        .addAttribute("name", ANNOTATION_ENTITY).addText(targetEntity.getName());

    // NOTE: limitations...
    // - mondrian does not support expression elements in combination with closures
    // - captionColumn and captionExpression give a indexOutofboundsException in Mondrian
    // - mondrian does not support snowflake and parent child relations
    // - closure hierarchies are not supported in snowflakes because of this issue:
    // http://jira.pentaho.com/browse/MONDRIAN-661
    final boolean isSnowFlake = (levelElement.getParent().element("Join") != null);

    // determine if a closure element should be added
    // also controls were to add ordinals
    final String treeId = cubeDimension.isIgnoreTreeHierarchy() ? null : getTreeId(targetEntity);
    final boolean addParentChildHierarchy = treeId != null && isLowestLevelInHierarchy
        && !isSnowFlake;

    // .addAttribute("alias", alias);
    // move to the front
    levelElement.detach();
    hierarchyElement.elements("Level").add(0, levelElement);
    setLevelName(levelElement, property.getName());

    if (addParentChildHierarchy) {
      final Property ordinalProperty = OBOlapClosureTreeHandler
          .getTreeOrdinalProperty(targetEntity);
      // a tree structure/use the tree ordinal column
      if (ordinalProperty != null) {
        levelElement.addAttribute("ordinalColumn",
            toUpperOrLowerCase(ordinalProperty.getColumnName()));
      }
      final Property parentIdProperty = OBOlapClosureTreeHandler
          .getTreeParentProperty(targetEntity);
      if (parentIdProperty != null) {
        // addExpressionElement(levelElement, "Parent", getAlias(alias) + "."
        // + toUpperOrLowerCase(parentIdProperty.getColumnName()));
        levelElement.addAttribute("parentColumn",
            toUpperOrLowerCase(parentIdProperty.getColumnName()));
      } else {
        levelElement
            .addElement("ParentExpression")
            .addElement("SQL")
            .addAttribute("dialect", DIALECT)
            .addText(
                "(select parent_id from " + toUpperOrLowerCase(TreeClosure.TABLE_NAME)
                    + " where child_id="
                    + getAliasForOracleOrPostgres(tableElement.attributeValue("alias")) + "."
                    + idColumnName + " and ad_tree_id = '" + treeId + "' and distance=1)");
      }

      levelElement.addElement("MemberFormatter").addAttribute("className",
          OBMondrianMemberFormatter.class.getName());

      addClosureElement(levelElement, tableElement, targetEntity, treeId);
    } else {
      // no tree sorting add an ordinal column on the first identifier property
      Property identifierProperty = targetEntity.getIdentifierProperties().get(0);
      if (targetEntity.hasProperty("name") && targetEntity.getProperty("name").isIdentifier()) {
        identifierProperty = targetEntity.getProperty("name");
      }

      Property orderClause = null;
      CubeLevel level = getCubeLevelFromProperty(property, cubeDimension);
      if (level != null) {
        if (level.getOrderProperty() != null) {
          String[] orderProperty = level.getOrderProperty().split("\\.");
          if (targetEntity.hasProperty(orderProperty[orderProperty.length - 1])) {
            orderClause = targetEntity.getProperty(orderProperty[orderProperty.length - 1]);
          }
        }
      }
      if (orderClause != null) {
        levelElement.addAttribute("ordinalColumn", toUpperOrLowerCase(orderClause.getColumnName()));
      } else {
        levelElement.addAttribute("ordinalColumn",
            toUpperOrLowerCase(identifierProperty.getColumnName()));
      }

      levelElement.addElement("MemberFormatter").addAttribute("className",
          OBMondrianMemberFormatter.class.getName());
    }

  }

  private CubeLevel getCubeLevelFromProperty(Property property, CubeDimension cubeDimension) {
    OBCriteria<CubeLevel> obc = OBDal.getInstance().createCriteria(CubeLevel.class);
    obc.add(Restrictions.eq(CubeLevel.PROPERTY_OBANALYCUBEDIMENSION, cubeDimension));
    obc.add(Restrictions.eq(CubeLevel.PROPERTY_NAME, property.getName()));
    return (CubeLevel) obc.uniqueResult();
  }

  private Property[] getPropertiesOnPath(Entity entity, String propertyPath) {
    final String[] propertyNames = propertyPath.split("\\.");
    final Property[] result = new Property[propertyNames.length];
    Entity currentEntity = entity;
    int cnt = 0;
    for (String propertyName : propertyNames) {
      final Property property = currentEntity.getProperty(propertyName);
      if (property.getTargetEntity() != null) {
        currentEntity = property.getTargetEntity();
      }
      result[cnt++] = property;
    }
    return result;
  }

  protected Property computeLastJoinProperty(Property[] properties) {
    Property lastJoinProperty = null;
    for (Property property : properties) {
      final TimeDimensionEntry timeDimensionEntry = TimeDimensionProvider.getInstance()
          .getTimeDimensionEntry(property);

      if (timeDimensionEntry != null || property.getTargetEntity() != null) {
        lastJoinProperty = property;
      }
    }
    return lastJoinProperty;
  }

  private Element setHierarchyName(Element hierarchyElement, String dimensionName) {
    // hierarchyElement.addAttribute("name", dimensionName + "Hierarchy");
    return hierarchyElement;
  }

  private Element setLevelName(Element levelElement, String dimensionName) {
    levelElement.addAttribute("name", dimensionName + "Level");
    return levelElement;
  }

  protected void createDateTimeReferenceDimension(Element parentElement, Entity entity,
      String propertyPath, CubeDimension cubeDimension) {
    final Property property = getLastProperty(entity, propertyPath);
    // be robust...
    if (property == null) {
      return;
    }
    // ignore the time dimension properties themselves
    if (TimeDimensionProvider.getInstance().isTimeDimensionProperty(property)) {
      return;
    }
    final TimeDimensionEntry entry = TimeDimensionProvider.getInstance().getTimeDimensionEntry(
        property);
    if (entry == null) {
      // use the standard way
      createComputedDateTimeReferenceDimension(parentElement, entity, propertyPath, cubeDimension);
    } else {
      createModeledDateTimeReferenceDimension(parentElement, entity, propertyPath, entry,
          cubeDimension);
    }
  }

  protected void createModeledDateTimeReferenceDimension(Element parentElement, Entity entity,
      String propertyPath, TimeDimensionEntry entry, CubeDimension cubeDimension) {

    final Element dimensionElement = parentElement.addElement("Dimension")
        .addAttribute("name", getDimensionName(entity, propertyPath, cubeDimension))
        .addAttribute("type", "TimeDimension");
    dimensionElement.addAttribute("caption", getCaption(entity, propertyPath, cubeDimension));

    final Property property = getLastProperty(entity, propertyPath);
    // be robust
    if (property == null) {
      return;
    }

    createDefaultDateTimeHierarchyElement(dimensionElement, entity, property, entry, propertyPath);
    createFilterDateTimeHierarchyElement(dimensionElement, entity, property, entry, propertyPath);
  }

  protected void createDefaultDateTimeHierarchyElement(Element dimensionElement, Entity entity,
      Property property, TimeDimensionEntry entry, String propertyPath) {

    final Property timeDimensionIdProperty = TimeDimensionProvider.TIMEDIMENSIONENTITY
        .getIdProperties().get(0);

    final boolean isJoin = propertyPath.contains(".");

    // create the default hierarchyelement
    Element defaultHierarchyElement = dimensionElement.addElement("Hierarchy").addAttribute(
        "hasAll", "true");
    if (isJoin) {
      final Property[] properties = getPropertiesOnPath(entity, propertyPath);
      final Property firstProperty = properties[0];
      addPrimaryAndForeigKeyToElements(dimensionElement, defaultHierarchyElement, firstProperty);
    } else {
      defaultHierarchyElement.addAttribute("primaryKey",
          toUpperOrLowerCase(timeDimensionIdProperty.getColumnName()));
      defaultHierarchyElement.addAttribute("primaryKeyTable",
          toUpperOrLowerCase(TimeDimensionProvider.TIMEDIMENSIONENTITY.getTableName()));
      dimensionElement.addAttribute("foreignKey", toUpperOrLowerCase(entry
          .getTimeDimensionProperty().getColumnName()));
    }
    setHierarchyName(defaultHierarchyElement, property.getName());

    String tableName = createTableJoinsAndLevels(defaultHierarchyElement, entity, propertyPath,
        null, null);

    if (tableName == null) {
      tableName = toUpperOrLowerCase(TimeDimension.TABLE_NAME);
      defaultHierarchyElement.addElement("Table").addAttribute("name", tableName);
    }
    defaultHierarchyElement.addElement("Level").addAttribute("name", "Year")
        .addAttribute("table", tableName).addAttribute("column", toUpperOrLowerCase("THEYEAR"))
        .addAttribute("uniqueMembers", Boolean.toString(!hasLevels(propertyPath)))
        .addAttribute("levelType", "TimeYears").addAttribute("type", "Integer");
    defaultHierarchyElement.addElement("Level")
        .addAttribute("name", MondrianConstants.QUARTER_LEVEL_NAME)
        .addAttribute("table", tableName).addAttribute("column", toUpperOrLowerCase("QUARTER"))
        .addAttribute("uniqueMembers", "false").addAttribute("levelType", "TimeQuarters")
        .addAttribute("type", "Integer");
    defaultHierarchyElement.addElement("Level")
        .addAttribute("name", MondrianConstants.MONTH_LEVEL_NAME).addAttribute("table", tableName)
        .addAttribute("column", toUpperOrLowerCase("MONTH_OF_YEAR"))
        .addAttribute("uniqueMembers", "false").addAttribute("levelType", "TimeMonths")
        .addAttribute("type", "Integer");
    // hierarchyElement.addElement("Level").addAttribute("name", "Week")
    // .addAttribute("column", "week_of_month").addAttribute("uniqueMembers", "false")
    // .addAttribute("levelType", "TimeWeeks").addAttribute("type", "Numeric");
    defaultHierarchyElement.addElement("Level")
        .addAttribute("name", MondrianConstants.DAY_LEVEL_NAME).addAttribute("table", tableName)
        .addAttribute("column", toUpperOrLowerCase("DAY_OF_MONTH"))
        .addAttribute("ordinalColumn", toUpperOrLowerCase("yyyymmdd"))
        .addAttribute("uniqueMembers", "false").addAttribute("levelType", "TimeDays")
        .addAttribute("type", "Integer");
  }

  protected void createFilterDateTimeHierarchyElement(Element dimensionElement, Entity entity,
      Property property, TimeDimensionEntry entry, String propertyPath) {

    final Property timeDimensionIdProperty = TimeDimensionProvider.TIMEDIMENSIONENTITY
        .getIdProperties().get(0);

    final boolean isJoin = propertyPath.contains(".");

    Element hierarchyElement = dimensionElement.addElement("Hierarchy").addAttribute("hasAll",
        "true");
    if (isJoin) {
      final Property[] properties = getPropertiesOnPath(entity, propertyPath);
      final Property firstProperty = properties[0];
      addPrimaryAndForeigKeyToElements(dimensionElement, hierarchyElement, firstProperty);
    } else {
      hierarchyElement.addAttribute("primaryKey",
          toUpperOrLowerCase(timeDimensionIdProperty.getColumnName()));
      hierarchyElement.addAttribute("primaryKeyTable",
          toUpperOrLowerCase(TimeDimensionProvider.TIMEDIMENSIONENTITY.getTableName()));
      dimensionElement.addAttribute("foreignKey", toUpperOrLowerCase(entry
          .getTimeDimensionProperty().getColumnName()));
    }
    hierarchyElement.addAttribute("name", "DayFilter_" + property.getName());

    String tableName = createTableJoinsAndLevels(hierarchyElement, entity, propertyPath, null, null);

    if (tableName == null) {
      tableName = toUpperOrLowerCase(TimeDimension.TABLE_NAME);
      hierarchyElement.addElement("Table").addAttribute("name", tableName);
    }

    final Element levelElement = hierarchyElement.addElement("Level")
        .addAttribute("name", "DateValue").addAttribute("table", tableName)
        .addAttribute("column", toUpperOrLowerCase("datevalue"))
        .addAttribute("levelType", "TimeDays").addAttribute("uniqueMembers", "true")
        .addAttribute("type", "String");

    levelElement.addElement("Annotations").addElement("Annotation")
        .addAttribute("name", ANNOTATION_DATEFILTERMEMBER)
        .addText(property.getEntity().getName() + "." + property.getName());

    levelElement.addElement("MemberFormatter").addAttribute("className",
        OBMondrianMemberFormatter.class.getName());
  }

  protected void createComputedDateTimeReferenceDimension(Element parentElement, Entity entity,
      String propertyPath, CubeDimension cubeDimension) {
    final Property property = getLastProperty(entity, propertyPath);
    if (property == null) {
      return;
    }
    final Property firstProperty = getFirstProperty(entity, propertyPath);
    if (firstProperty == null) {
      return;
    }
    final Element dimensionElement = parentElement.addElement("Dimension")
        .addAttribute("name", getDimensionName(entity, propertyPath, cubeDimension))
        .addAttribute("type", "TimeDimension");
    dimensionElement.addAttribute("caption", getCaption(entity, propertyPath, cubeDimension));

    if (propertyPath.contains(".")) {
      dimensionElement
          .addAttribute("foreignKey", toUpperOrLowerCase(firstProperty.getColumnName()));
    }

    final Element hierarchyElement = dimensionElement.addElement("Hierarchy").addAttribute(
        "hasAll", "true");
    if (propertyPath.contains(".") && firstProperty.getTargetEntity() != null) {
      addPrimaryAndForeigKeyToElements(dimensionElement, hierarchyElement, firstProperty);
    }

    setHierarchyName(hierarchyElement, property.getName());

    String tableName = createTableJoinsAndLevels(hierarchyElement, entity, propertyPath, null, null);
    String tableNameExpression = "";
    if (tableName != null) {
      tableNameExpression = tableName + ".";
    }

    {
      final Element levelElement = hierarchyElement.addElement("Level");
      levelElement.addAttribute("name", "Year");
      levelElement.addAttribute("table", tableName);
      levelElement.addAttribute("column", toUpperOrLowerCase(property.getColumnName()));
      levelElement.addAttribute("uniqueMembers", Boolean.toString(!hasLevels(propertyPath)));
      levelElement.addAttribute("levelType", "TimeYears");
      levelElement.addAttribute("type", "Integer");
      levelElement
          .addElement("KeyExpression")
          .addElement("SQL")
          .addAttribute("dialect", DIALECT)
          .addText(
              "extract(year from " + tableNameExpression
                  + toUpperOrLowerCase(property.getColumnName()) + ")");
    }
    {
      final Element levelElement = hierarchyElement.addElement("Level");
      levelElement.addAttribute("name", toUpperOrLowerCase(MondrianConstants.QUARTER_LEVEL_NAME));
      levelElement.addAttribute("column", toUpperOrLowerCase(property.getColumnName()));
      levelElement.addAttribute("table", tableName);
      levelElement.addAttribute("uniqueMembers", "false");
      levelElement.addAttribute("levelType", "TimeQuarters");
      levelElement.addAttribute("type", "Integer");
      levelElement
          .addElement("KeyExpression")
          .addElement("SQL")
          .addAttribute("dialect", DIALECT)
          .addText(
              "extract(quarter from " + tableNameExpression
                  + toUpperOrLowerCase(property.getColumnName()) + ")");
    }
    {
      final Element levelElement = hierarchyElement.addElement("Level");
      levelElement.addAttribute("name", toUpperOrLowerCase(MondrianConstants.MONTH_LEVEL_NAME));
      levelElement.addAttribute("table", tableName);
      levelElement.addAttribute("column", toUpperOrLowerCase(property.getColumnName()));
      levelElement.addAttribute("uniqueMembers", "false");
      levelElement.addAttribute("type", "Integer");
      levelElement.addAttribute("levelType", "TimeMonths");
      levelElement
          .addElement("KeyExpression")
          .addElement("SQL")
          .addAttribute("dialect", DIALECT)
          .addText(
              "extract(month from " + tableNameExpression
                  + toUpperOrLowerCase(property.getColumnName()) + ")");
    }

    {
      final Element levelElement = hierarchyElement.addElement("Level");
      levelElement.addAttribute("name", toUpperOrLowerCase(MondrianConstants.DAY_LEVEL_NAME));
      levelElement.addAttribute("table", tableName);
      levelElement.addAttribute("column", toUpperOrLowerCase(property.getColumnName()));
      levelElement.addAttribute("ordinalColumn", toUpperOrLowerCase(property.getColumnName()));
      levelElement.addAttribute("uniqueMembers", "false");
      levelElement.addAttribute("type", "Integer");
      levelElement.addAttribute("levelType", "TimeDays");
      levelElement
          .addElement("KeyExpression")
          .addElement("SQL")
          .addAttribute("dialect", DIALECT)
          .addText(
              "extract(day from " + tableNameExpression
                  + toUpperOrLowerCase(property.getColumnName()) + ")");
    }
  }

  protected String getDimensionName(Entity entity, String propertyPath, CubeDimension cubeDimension) {
    Property[] properties = getPropertiesOnPath(entity, propertyPath);
    String localName = properties[0].getName();
    if (cubeDimension.getName() != null) {
      localName = getSafeDimensionName(cubeDimension.getName());
    }
    if (properties.length > 1) {
      return localName + "-" + properties[properties.length - 1].getName();
    }
    return localName;
  }

  protected Element createReferenceDimension(Element parentElement, Entity entity,
      String propertyPath, String sqlFilter, CubeDimension cubeDimension) {
    final Property[] properties = getPropertiesOnPath(entity, propertyPath);
    final Property firstProperty = properties[0];
    final Element dimensionElement = parentElement.addElement("Dimension")
        .addAttribute("name", getDimensionName(entity, propertyPath, cubeDimension))
        .addAttribute("foreignKey", toUpperOrLowerCase(firstProperty.getColumnName()));
    dimensionElement.addAttribute("caption", getCaption(entity, propertyPath, cubeDimension));

    final Element hierarchyElement = dimensionElement.addElement("Hierarchy").addAttribute(
        "hasAll", "true");

    addPrimaryAndForeigKeyToElements(dimensionElement, hierarchyElement, firstProperty);

    createTableJoinsAndLevels(hierarchyElement, entity, propertyPath, sqlFilter, cubeDimension);
    return dimensionElement;
  }

  private void addClosureElement(Element levelElement, Element tableElement, Entity targetEntity,
      String treeId) {

    // final String pkColumnName = toUpperOrLowerCase(targetEntity.getIdProperties().get(0)
    // .getColumnName());
    levelElement.addAttribute("uniqueMembers", "true");

    // NOTE: OrdinalExpression must be before ParentExpression
    // levelElement
    // .addElement("OrdinalExpression")
    // .addElement("SQL").addAttribute("dialect", DIALECT)
    // .addText(
    // "(select ordinal from " + toUpperOrLowerCase(ClosureOrdinal.TABLE_NAME)
    // + " where child_id=" + getAlias(tableElement.attributeValue("alias")) + "."
    // + toUpperOrLowerCase(pkColumnName) + " and ad_tree_id = '" + treeId + "')");

    // levelElement
    // .addElement("ParentExpression")
    // .addElement("SQL").addAttribute("dialect", DIALECT)
    // .addText(
    // "(select parent_id from " + toUpperOrLowerCase(TreeClosure.TABLE_NAME)
    // + " where child_id=" + getAlias(tableElement.attributeValue("alias")) + "."
    // + toUpperOrLowerCase(pkColumnName) + " and ad_tree_id = '" + treeId
    // + "' and distance=1)");

    // note: alias is needed otherwise Mondrian will give a NPE

    final Element closureTableElement = levelElement.addElement("Closure")
        .addAttribute("parentColumn", toUpperOrLowerCase("PARENT_ID"))
        .addAttribute("childColumn", toUpperOrLowerCase("CHILD_ID")).addElement("Table")
        .addAttribute("alias", toUpperOrLowerCase(TreeClosure.TABLE_NAME))
        .addAttribute("name", toUpperOrLowerCase(TreeClosure.TABLE_NAME));
    closureTableElement.addElement("SQL").addAttribute("dialect", DIALECT)
        .addText(toUpperOrLowerCase(TreeClosure.TABLE_NAME) + ".ad_tree_id = '" + treeId + "'");
  }

  private String getTreeId(Entity entity) {

    // do not support all trees for now, as this gives easily a mondrian exception
    // http://jira.pentaho.com/browse/MONDRIAN-1810
    // http://jira.pentaho.com/browse/MONDRIAN-1047
    // closure tables in general are tricky things
    boolean supportTree = Organization.ENTITY_NAME.equals(entity.getName())
        || ProductCategory.ENTITY_NAME.equals(entity.getName())
        || Project.ENTITY_NAME.equals(entity.getName())
        || FIN_FinancialAccount.ENTITY_NAME.equals(entity.getName())
        || ElementValue.ENTITY_NAME.equals(entity.getName());
    if (!supportTree) {
      return null;
    }
    final Tree tree = OBAnalyticsUtils.getTree(entity);
    if (tree == null) {
      return null;
    }
    return tree.getId();
  }

  private void createMeasurements(Element parentElement, List<Measure> measuresList) {

    // move the calculated members to the end
    Collections.sort(measuresList, new Comparator<Measure>() {
      @Override
      public int compare(Measure o1, Measure o2) {
        if (o1.getIsCalculatedMember() && !o2.getIsCalculatedMember()) {
          return 1;
        } else if (!o1.getIsCalculatedMember() && o2.getIsCalculatedMember()) {
          return -1;
        }
        return 0;
      }
    });

    for (Measure measure : measuresList)
      if (measure.getIsCalculatedMember()) {
        if (measure.getXmlDefintion() != null && !"".equals(measure.getXmlDefintion())) {
          parseXmlAndAdd(parentElement, "CalculatedMember", measure.getXmlDefintion());
        } else {
          final Element measureElement = parentElement.addElement("CalculatedMember");
          if (measure.getName() != null && !"".equals(measure.getName())) {
            measureElement.addAttribute("name", measure.getName());
            measureElement.addAttribute("caption", measure.getName());
          } else {
            measureElement.addAttribute("name", measure.getProperty().getName());
            measureElement.addAttribute("caption", getCaption(measure.getProperty()));
          }
          measureElement.addAttribute("dimension", "Measures");
          final Element formula = measureElement.addElement("Formula");
          formula.addText(measure.getFormula());
          addCellFormatter(measure, measureElement, measure.getProperty());
        }
      } else {
        if (measure.getXmlDefintion() != null && !"".equals(measure.getXmlDefintion())) {
          parseXmlAndAdd(parentElement, "Measure", measure.getXmlDefintion());
        } else {
          final Element measureElement = parentElement.addElement("Measure");
          if (measure.getName() != null && !"".equals(measure.getName())) {
            measureElement.addAttribute("name", measure.getName());
            measureElement.addAttribute("caption", measure.getName());
          } else {
            measureElement.addAttribute("name", measure.getProperty().getName());
            measureElement.addAttribute("caption", getCaption(measure.getProperty()));
          }
          if (measure.getMeasureExpression() != null && !"".equals(measure.getMeasureExpression())) {
            final Element measureExpressionElement = measureElement.addElement("MeasureExpression");
            final Element sqlElement = measureExpressionElement.addElement("SQL").addAttribute(
                "dialect", DIALECT);
            sqlElement.addText(measure.getMeasureExpression());
          } else {
            measureElement.addAttribute("column", toUpperOrLowerCase(measure.getProperty()
                .getColumnName()));
          }
          measureElement.addAttribute("aggregator", measure.getAggregator());
          addCellFormatter(measure, measureElement, measure.getProperty());
        }
      }
  }

  protected String getCaption(Entity entity, String propertyPath, CubeDimension cubeDimension) {
    Property[] properties = getPropertiesOnPath(entity, propertyPath);
    String localName = getCaption(properties[0]);
    if (cubeDimension.getName() != null) {
      localName = cubeDimension.getName();
    }
    if (properties.length > 1) {
      return localName + " - " + getCaption(properties[properties.length - 1]);
    }
    return localName;
  }

  protected String getCaption(Property property) {
    final Column column = OBDal.getInstance().get(Column.class, property.getColumnId());
    if (column.getApplicationElement() != null) {
      final org.openbravo.model.ad.ui.Element element = column.getApplicationElement();
      return OBViewUtil.getLabel(element, element.getADElementTrlList());
    }
    return property.getName();
  }

  protected void addCellFormatter(Measure measure, Element element, Property property) {
    // trick to format percentages...
    if (measure.getName() != null && measure.getName().contains("%")) {
      element.addElement("CellFormatter").addAttribute("className",
          MondrianCellFormatter.Percentage.class.getName());
      return;
    }
    if (!(property.getDomainType() instanceof PrimitiveDomainType)) {
      return;
    }
    final PrimitiveDomainType primitiveDomainType = (PrimitiveDomainType) property.getDomainType();
    if (primitiveDomainType.getFormatId() == null) {
      return;
    }
    final Class<?> cellFormatterClass = MondrianCellFormatter
        .getCellFormatterClass(primitiveDomainType.getFormatId());
    element.addElement("CellFormatter").addAttribute("className", cellFormatterClass.getName());
  }

  protected List<String> getReadableOrganizations() {
    if (readableOrganizations != null) {
      return readableOrganizations;
    }

    Check.isNotNull(currentRole, "currentRole must be set");
    final OrganizationStructureProvider orgProvider = OBProvider.getInstance().get(
        OrganizationStructureProvider.class);
    orgProvider.setClientId(currentRole.getClient().getId());

    final Set<String> readableOrgs = new HashSet<String>();

    final List<Organization> os = getOrganizationList();
    for (final Organization o : os) {
      readableOrgs.addAll(orgProvider.getNaturalTree(o.getId()));
      // if zero is an organization then add them all!
      if (o.getId().equals("0")) {
        for (final Organization org : getOrganizations(currentRole.getClient())) {
          readableOrgs.add(org.getId());
        }
      }
    }
    readableOrgs.add("0");

    readableOrganizations = new ArrayList<String>(readableOrgs);
    return readableOrganizations;
  }

  @SuppressWarnings("unchecked")
  private List<Organization> getOrganizationList() {
    final Query qry = SessionHandler.getInstance().createQuery(
        "select o from " + Organization.class.getName() + " o, " + RoleOrganization.class.getName()
            + " roa where o." + Organization.PROPERTY_ID + "=roa."
            + RoleOrganization.PROPERTY_ORGANIZATION + "." + Organization.PROPERTY_ID + " and roa."
            + RoleOrganization.PROPERTY_ROLE + "." + Organization.PROPERTY_ID + "='"
            + currentRole.getId() + "' and roa." + RoleOrganization.PROPERTY_ACTIVE + "='Y' and o."
            + Organization.PROPERTY_ACTIVE + "='Y'");
    return (List<Organization>) qry.list();
  }

  @SuppressWarnings("unchecked")
  private List<Organization> getOrganizations(Client client) {
    final Query qry = SessionHandler.getInstance().createQuery(
        "select o from " + Organization.class.getName() + " o where " + "o."
            + Organization.PROPERTY_CLIENT + "=? and o." + Organization.PROPERTY_ACTIVE + "='Y'");
    qry.setParameter(0, client);
    return qry.list();
  }

  public String toUpperOrLowerCase(String arg) {
    if (isOracle) {
      return arg.toUpperCase();
    } else {
      return arg.toLowerCase();
    }
  }

  public String getAliasForOracleOrPostgres(String alias) {
    if (isOracle) {
      return "\"" + alias + "\"";
    } else {
      return alias;
    }
  }

  public Role getCurrentRole() {
    return currentRole;
  }

  public void setCurrentRole(Role currentRole) {
    this.currentRole = currentRole;
  }

  /**
   * Prepend the role id as part of the cube name
   * 
   * @see #getCubeKeyParts
   */
  protected String createCubeKey(String cubeName) {
    return currentRole.getId() + "_" + cubeName;
  }

  public String getAliasPrefix() {
    return aliasPrefix;
  }

  public void setAliasPrefix(String aliasPrefix) {
    this.aliasPrefix = aliasPrefix;
  }

  /**
   * Makes sure that the name works correctly in Saiku, for example gets rid of /, this does not
   * work nicely with Saiku
   */
  protected String getSafeName(String name) {
    return name.replace("/", "_");
  }

  protected String getSafeDimensionName(String name) {
    return name.replace("/", "_").replace(" ", "");
  }

  private class Measure {
    Property property;
    String name;
    String xmlDefintion;
    boolean isCalculatedMember;
    String measureExpression;
    String formula;
    String aggregator = "sum";

    public boolean getIsCalculatedMember() {
      return isCalculatedMember;
    }

    public void setIsCalculatedMember(boolean isCalculatedMember) {
      this.isCalculatedMember = isCalculatedMember;
    }

    public String getMeasureExpression() {
      return measureExpression;
    }

    public void setMeasureExpression(String measureExpression) {
      this.measureExpression = measureExpression;
    }

    public String getFormula() {
      return formula;
    }

    public void setFormula(String formula) {
      this.formula = formula;
    }

    public Property getProperty() {
      return property;
    }

    public void setProperty(Property property) {
      this.property = property;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getXmlDefintion() {
      return xmlDefintion;
    }

    public void setXmlDefintion(String xmlDefintion) {
      this.xmlDefintion = xmlDefintion;
    }

    public String getAggregator() {
      return aggregator;
    }

    public void setAggregator(String aggregator) {
      this.aggregator = aggregator;
    }
  }

}