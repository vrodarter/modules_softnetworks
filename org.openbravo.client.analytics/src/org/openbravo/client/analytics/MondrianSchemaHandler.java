/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.client.kernel.event.EntityDeleteEvent;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchema;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchemaElement;

/**
 * Gets and caches the mondrian schema and clears the cache when something changes in a cube
 * definition.
 * 
 * @see Mondrian
 * 
 * @author mtaal
 * 
 */
public class MondrianSchemaHandler {
  private static final Logger log = Logger.getLogger(MondrianSchemaHandler.class);

  // current role id is to override the role when pre-loading the cache.
  private static ThreadLocal<String> currentRoleId = new ThreadLocal<String>();

  private static MondrianSchemaHandler instance = new MondrianSchemaHandler();

  public static void setCurrentRoleId(String roleId) {
    currentRoleId.set(roleId);
  }

  public static MondrianSchemaHandler getInstance() {
    return instance;
  }

  public static void setInstance(MondrianSchemaHandler instance) {
    MondrianSchemaHandler.instance = instance;
  }

  private Map<String, String> schemaCache = new HashMap<String, String>();

  public boolean hasCachedSchemas() {
    return !schemaCache.isEmpty();
  }

  public synchronized void clearCache() {
    schemaCache.clear();
  }

  private String getCurrentCacheKey() {
    if (currentRoleId.get() != null) {
      return currentRoleId.get();
    }
    return OBContext.getOBContext().getRole().getId() + "_"
        + OBContext.getOBContext().getCurrentOrganization().getId();
  }

  public synchronized String getSchema() {
    final String cacheKey = getCurrentCacheKey();
    String cachedSchema = schemaCache.get(cacheKey);
    if (cachedSchema != null) {
      return cachedSchema;
    }

    Role role = null;
    OBContext.setAdminMode(true);
    try {
      role = OBDal.getInstance().get(Role.class, OBContext.getOBContext().getRole().getId());
    } finally {
      OBContext.restorePreviousMode();
    }

    final Document document = DocumentHelper.createDocument();
    document.setXMLEncoding("UTF-8");

    int aliasCnt = 1;
    {
      MondrianSchemaGenerator generator = new MondrianSchemaGenerator();
      generator.setAliasPrefix("alias" + (aliasCnt++) + "_");
      generator.generate(document, role);
    }

    try {
      final StringWriter sw = new StringWriter();
      OutputFormat format = OutputFormat.createPrettyPrint();
      XMLWriter writer = new XMLWriter(sw, format);
      writer.write(document);
      writer.close();
      final String schema = sw.toString();
      schemaCache.put(cacheKey, schema);

      if (log.isDebugEnabled()) {
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        log.debug("SCHEMA");
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        log.debug(schema);
        log.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      }
      // System.err.println(schema);
      return schema;
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  public static class CubeDefinitionEventObserver extends EntityPersistenceEventObserver {

    private static Entity[] entities = {
        ModelProvider.getInstance().getEntity(AcctSchemaElement.ENTITY_NAME),
        ModelProvider.getInstance().getEntity(AcctSchema.ENTITY_NAME),
        ModelProvider.getInstance().getEntity(Client.ENTITY_NAME),
        ModelProvider.getInstance().getEntity(CubeDefinition.ENTITY_NAME),
        ModelProvider.getInstance().getEntity(CubeDimension.ENTITY_NAME) };

    public void onNew(@Observes EntityNewEvent event) {
      if (!isValidEvent(event)) {
        return;
      }
      MondrianSchemaHandler.getInstance().clearCache();
    }

    public void onUpdate(@Observes EntityUpdateEvent event) {
      if (!isValidEvent(event)) {
        return;
      }
      MondrianSchemaHandler.getInstance().clearCache();
    }

    public void onDelete(@Observes EntityDeleteEvent event) {
      if (!isValidEvent(event)) {
        return;
      }
      MondrianSchemaHandler.getInstance().clearCache();
    }

    @Override
    protected Entity[] getObservedEntities() {
      return entities;
    }
  }
}
