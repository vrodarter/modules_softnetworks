/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.invoice.InvoiceLineOffer;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;

/**
 * Updates the {@link AnalyticsFactDiscounts} table.
 * 
 * @author AugustoMauch
 */
public class OBAnalyticsFactDiscountsUpdater extends
    OBAnalyticsFactUpdater<InvoiceLineOffer, AnalyticsFactDiscounts> {

  @Override
  protected ScrollableResults getData() {
    final Date fromDate = getLastUpdateRunDate();
    final String qryStr = "select ilo from " + InvoiceLineOffer.ENTITY_NAME
        + " ilo where ilo.client.id = :client and ilo.active=true and ilo."
        + InvoiceLineOffer.PROPERTY_INVOICELINE + "." + InvoiceLine.PROPERTY_INVOICE
        + ".active=true and ilo." + InvoiceLineOffer.PROPERTY_INVOICELINE + "."
        + InvoiceLine.PROPERTY_INVOICE + "." + Invoice.PROPERTY_DOCUMENTSTATUS
        + " in ('CO', 'CL') and not exists(select iloFact.id from "
        + AnalyticsFactDiscounts.ENTITY_NAME + " iloFact where iloFact."
        + AnalyticsFactDiscounts.PROPERTY_INVOICELINEOFFER + ".id=ilo.id) and ilo."
        + InvoiceLineOffer.PROPERTY_INVOICELINE + "." + InvoiceLine.PROPERTY_SALESORDERLINE
        + " is not null and " + "ilo." + InvoiceLineOffer.PROPERTY_INVOICELINE + "."
        + InvoiceLine.PROPERTY_INVOICE + ".salesTransaction = true and ilo."
        + InvoiceLineOffer.PROPERTY_INVOICELINE + "." + InvoiceLine.PROPERTY_INVOICE + "."
        + Invoice.PROPERTY_DOCUMENTTYPE + "." + DocumentType.PROPERTY_REVERSAL + " = false";
    final Query qry = OBDal.getInstance().getSession().createQuery(qryStr);
    qry.setString("client", OBContext.getOBContext().getCurrentClient().getId());
    qry.setReadOnly(true);
    qry.setFetchSize(5000);
    return qry.scroll(ScrollMode.FORWARD_ONLY);
  }

  @Override
  protected Map<String, String> getPropertyMap() {
    final String invoiceLinePath = AnalyticsFactDiscounts.PROPERTY_INVOICELINE;
    final String orderPath = invoiceLinePath + "." + InvoiceLine.PROPERTY_SALESORDERLINE + "."
        + OrderLine.PROPERTY_SALESORDER;
    final String invoicePath = invoiceLinePath + "." + InvoiceLine.PROPERTY_INVOICE;
    final String bpPath = invoicePath + "." + Invoice.PROPERTY_BUSINESSPARTNER;

    final Map<String, String> result = new HashMap<String, String>();

    result.put(AnalyticsFactDiscounts.PROPERTY_CLIENT, AnalyticsFactDiscounts.PROPERTY_CLIENT);
    result.put(AnalyticsFactDiscounts.PROPERTY_ORGANIZATION,
        AnalyticsFactDiscounts.PROPERTY_ORGANIZATION);
    result.put(AnalyticsFactDiscounts.PROPERTY_BUSINESSPARTNER, bpPath);
    result.put(AnalyticsFactDiscounts.PROPERTY_PRODUCT, invoiceLinePath + "."
        + InvoiceLine.PROPERTY_PRODUCT);
    result.put(AnalyticsFactDiscounts.PROPERTY_PRODUCTCATEGORY, invoiceLinePath + "."
        + InvoiceLine.PROPERTY_PRODUCT + "." + Product.PROPERTY_PRODUCTCATEGORY);

    result.put(AnalyticsFactDiscounts.PROPERTY_BUSINESSPARTNERCATEGORY, bpPath + "."
        + BusinessPartner.PROPERTY_BUSINESSPARTNERCATEGORY);
    result.put(AnalyticsFactDiscounts.PROPERTY_CURRENCY, invoicePath + "."
        + Invoice.PROPERTY_CURRENCY);
    result.put(AnalyticsFactDiscounts.PROPERTY_PAYMENTMETHOD, invoicePath + "."
        + Invoice.PROPERTY_PAYMENTMETHOD);
    result.put(AnalyticsFactDiscounts.PROPERTY_DOCUMENTNO, invoicePath + "."
        + Invoice.PROPERTY_DOCUMENTNO);
    result.put(AnalyticsFactDiscounts.PROPERTY_ORDERDATE, orderPath + "."
        + Order.PROPERTY_ORDERDATE);
    result.put(AnalyticsFactDiscounts.PROPERTY_SALESREPRESENTATIVE, orderPath + "."
        + Order.PROPERTY_SALESREPRESENTATIVE);
    result.put(AnalyticsFactDiscounts.PROPERTY_TOTALAMOUNT, InvoiceLineOffer.PROPERTY_TOTALAMOUNT);
    result.put(AnalyticsFactDiscounts.PROPERTY_DISCOUNTLINE,
        InvoiceLineOffer.PROPERTY_PRICEADJUSTMENTAMT);
    result.put(AnalyticsFactDiscounts.PROPERTY_ORDERDATE, orderPath + "."
        + Order.PROPERTY_ORDERDATE);
    result.put(AnalyticsFactDiscounts.PROPERTY_DATEORDERED, orderPath + "."
        + Order.PROPERTY_ORDERDATE);

    result.put(AnalyticsFactDiscounts.PROPERTY_PROMOTIONDISCOUNT,
        InvoiceLineOffer.PROPERTY_PRICEADJUSTMENT);
    result.put(AnalyticsFactDiscounts.PROPERTY_DISCOUNTPROMOTIONTYPE,
        InvoiceLineOffer.PROPERTY_PRICEADJUSTMENT + "." + PriceAdjustment.PROPERTY_DISCOUNTTYPE);
    return result;
  }

  @Override
  protected void processObject(InvoiceLineOffer invoiceLineOffer,
      AnalyticsFactDiscounts discountsFact) {

    final InvoiceLine invoiceLine = invoiceLineOffer.getInvoiceLine();
    final Invoice salesInvoice = invoiceLine.getInvoice();
    final OrderLine orderLine = invoiceLine.getSalesOrderLine();
    if (orderLine != null) {
      final Order salesOrder = orderLine.getSalesOrder();
      discountsFact.setSalesOrder(salesOrder);
    }
    discountsFact.setInvoiceLineOffer(invoiceLineOffer);
    discountsFact.setSalesOrderLine(orderLine);
    discountsFact.setInvoiceLine(invoiceLine);
    discountsFact.setInvoice(salesInvoice);
    if (discountsFact.getCurrency() == null) {
      discountsFact.setCurrency(salesInvoice.getCurrency());
    }
    discountsFact.setBusinessPartner(salesInvoice.getBusinessPartner());
  }

  protected void purgeFacts() {
    // Note: no implicit joins are allowed in the where clause, an inner select is however allowed
    final String qryStr = "delete " + getFactEntityName()
        + " iloFact where iloFact.updated < (select ilo.updated from "
        + InvoiceLineOffer.ENTITY_NAME + " ilo where ilo.id = iloFact."
        + AnalyticsFactDiscounts.PROPERTY_INVOICELINEOFFER + ".id) and iloFact.client.id=:client";
    OBDal.getInstance().getSession().createQuery(qryStr)
        .setString("client", OBContext.getOBContext().getCurrentClient().getId()).executeUpdate();
  }

  protected String getConversionDatePropertyName() {
    return AnalyticsFactDiscounts.PROPERTY_ORDERDATE;
  }

  protected String getFactEntityName() {
    return AnalyticsFactDiscounts.ENTITY_NAME;
  }

}