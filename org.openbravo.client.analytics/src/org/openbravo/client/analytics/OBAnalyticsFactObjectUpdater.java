/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import org.openbravo.base.structure.BaseOBObject;

/**
 * Base class for updating a single target object on the basis of the source object. Can be
 * implemented by subclasses.
 * 
 * @author mtaal
 */
public abstract class OBAnalyticsFactObjectUpdater<S extends BaseOBObject, F extends BaseOBObject>
    implements Comparable<OBAnalyticsFactObjectUpdater<BaseOBObject, BaseOBObject>> {

  /**
   * Allows an implementor to add additional information to a {@link BaseOBObject}. Every
   * {@link OBAnalyticsFactObjectUpdater} is called for every object. So the implementor should
   * determine itself if it is capable of handling the fact object.
   * 
   * @param factUpdater
   * @param sourceBob
   * @param targetBob
   */
  public abstract void handleFact(OBAnalyticsFactUpdater<S, F> factUpdater, S sourceBob, F factBob);

  /**
   * Must be implemented by subclass, return true if the subclass can handle the source and fact and
   * updater classes.
   * 
   */
  public abstract boolean canHandleFact(OBAnalyticsFactUpdater<?, ?> factUpdater, Object sourceBob,
      Object factBob);

  /**
   * Can be used to control the order in which fact object updaters are executed. The default value
   * returned here is 100, this is the execution order number for the standard OB object updaters.
   * If you want to execute an updater before return a lower number than 100, or after then return a
   * higher number.
   */
  public int getExecutionOrder() {
    return 100;
  }

  @Override
  public int compareTo(OBAnalyticsFactObjectUpdater<BaseOBObject, BaseOBObject> other) {
    return getExecutionOrder() - other.getExecutionOrder();
  }
}