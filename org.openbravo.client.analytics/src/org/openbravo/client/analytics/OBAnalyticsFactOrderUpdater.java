/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.InvoiceLineTax;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.geography.Location;
import org.openbravo.model.common.geography.Region;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.invoice.InvoiceLineOffer;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.cost.TransactionCost;
import org.openbravo.model.materialmgmt.transaction.MaterialTransaction;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;

/**
 * Updates the {@link AnalyticsFactOrder} table.
 * 
 * @author mtaal
 */
public class OBAnalyticsFactOrderUpdater extends
    OBAnalyticsFactUpdater<InvoiceLine, AnalyticsFactOrder> {

  @Override
  protected ScrollableResults getData() {
    final Date fromDate = getLastUpdateRunDate();
    final String qryStr = "select il from " + InvoiceLine.ENTITY_NAME
        + " il where il.client.id = :clientId and il.active=true and il."
        + InvoiceLine.PROPERTY_INVOICE + ".active=true and il." + InvoiceLine.PROPERTY_INVOICE
        + "." + Invoice.PROPERTY_DOCUMENTSTATUS
        + " IN ('CO', 'CL') and not exists(select fact.id from " + AnalyticsFactOrder.ENTITY_NAME
        + " fact where fact." + AnalyticsFactOrder.PROPERTY_INVOICELINE + ".id=il.id) and il."
        + InvoiceLine.PROPERTY_SALESORDERLINE + " is not null and il."
        + InvoiceLine.PROPERTY_INVOICE + ".salesTransaction = true and il."
        + InvoiceLine.PROPERTY_INVOICE + "." + Invoice.PROPERTY_DOCUMENTTYPE + "."
        + DocumentType.PROPERTY_REVERSAL + " = false";
    final Query qry = OBDal.getInstance().getSession().createQuery(qryStr);
    qry.setString("clientId", OBContext.getOBContext().getCurrentClient().getId());
    qry.setReadOnly(true);
    qry.setFetchSize(5000);
    return qry.scroll(ScrollMode.FORWARD_ONLY);
  }

  @Override
  protected Map<String, String> getPropertyMap() {
    final String orderLinePath = InvoiceLine.PROPERTY_SALESORDERLINE;
    final String orderPath = InvoiceLine.PROPERTY_SALESORDERLINE + "."
        + OrderLine.PROPERTY_SALESORDER;
    final String invoicePath = InvoiceLine.PROPERTY_INVOICE;
    final String bpPath = invoicePath + "." + Invoice.PROPERTY_BUSINESSPARTNER;
    final String productPath = InvoiceLine.PROPERTY_PRODUCT;

    final Map<String, String> result = new HashMap<String, String>();
    result.put(AnalyticsFactOrder.PROPERTY_CLIENT, InvoiceLine.PROPERTY_CLIENT);
    result.put(AnalyticsFactOrder.PROPERTY_ORGANIZATION, InvoiceLine.PROPERTY_ORGANIZATION);
    result.put(AnalyticsFactOrder.PROPERTY_BUSINESSPARTNER, bpPath);
    result.put(AnalyticsFactOrder.PROPERTY_BUSINESSPARTNERCATEGORY, bpPath + "."
        + BusinessPartner.PROPERTY_BUSINESSPARTNERCATEGORY);
    result.put(AnalyticsFactOrder.PROPERTY_TAX, InvoiceLine.PROPERTY_TAX);
    result.put(AnalyticsFactOrder.PROPERTY_BUSINESSPARTNERTAXCATEGORY, bpPath + "."
        + BusinessPartner.PROPERTY_TAXCATEGORY);
    result.put(AnalyticsFactOrder.PROPERTY_LOCATIONADDRESS, orderPath + "."
        + Order.PROPERTY_PARTNERADDRESS + "."
        + org.openbravo.model.common.businesspartner.Location.PROPERTY_LOCATIONADDRESS);
    result.put(AnalyticsFactOrder.PROPERTY_PRODUCT, InvoiceLine.PROPERTY_PRODUCT);

    result.put(AnalyticsFactOrder.PROPERTY_PRODUCTCATEGORY, productPath + "."
        + Product.PROPERTY_PRODUCTCATEGORY);
    result.put(AnalyticsFactOrder.PROPERTY_TAXCATEGORY, productPath + "."
        + Product.PROPERTY_TAXCATEGORY);
    result.put(AnalyticsFactOrder.PROPERTY_GENERICPRODUCT, productPath + "."
        + Product.PROPERTY_GENERICPRODUCT);
    result.put(AnalyticsFactOrder.PROPERTY_GENERICPRODUCTCATEGORY, productPath + "."
        + Product.PROPERTY_GENERICPRODUCT + "." + Product.PROPERTY_PRODUCTCATEGORY);
    result.put(AnalyticsFactOrder.PROPERTY_WAREHOUSE, orderLinePath + "."
        + OrderLine.PROPERTY_WAREHOUSE);

    result.put(AnalyticsFactOrder.PROPERTY_UOM, InvoiceLine.PROPERTY_UOM);
    result.put(AnalyticsFactOrder.PROPERTY_CURRENCY, invoicePath + "." + Invoice.PROPERTY_CURRENCY);
    result.put(AnalyticsFactOrder.PROPERTY_RETURNREASON, orderLinePath + "."
        + Order.PROPERTY_RETURNREASON);
    result.put(AnalyticsFactOrder.PROPERTY_COSTCENTER, InvoiceLine.PROPERTY_COSTCENTER);
    result.put(AnalyticsFactOrder.PROPERTY_STDIMENSION, InvoiceLine.PROPERTY_STDIMENSION);
    result.put(AnalyticsFactOrder.PROPERTY_NDDIMENSION, InvoiceLine.PROPERTY_NDDIMENSION);
    result.put(AnalyticsFactOrder.PROPERTY_PROJECT, InvoiceLine.PROPERTY_PROJECT);

    result.put(AnalyticsFactOrder.PROPERTY_DOCUMENTTYPE, orderPath + "."
        + Order.PROPERTY_DOCUMENTTYPE);
    result.put(AnalyticsFactOrder.PROPERTY_TRANSACTIONDOCUMENT, orderPath + "."
        + Order.PROPERTY_TRANSACTIONDOCUMENT);
    result.put(AnalyticsFactOrder.PROPERTY_SALESREPRESENTATIVE, orderPath + "."
        + Order.PROPERTY_SALESREPRESENTATIVE);
    result.put(AnalyticsFactOrder.PROPERTY_PAYMENTTERMS, invoicePath + "."
        + Invoice.PROPERTY_PAYMENTTERMS);
    result.put(AnalyticsFactOrder.PROPERTY_SALESCAMPAIGN, invoicePath + "."
        + Invoice.PROPERTY_SALESCAMPAIGN);
    result.put(AnalyticsFactOrder.PROPERTY_PAYMENTMETHOD, invoicePath + "."
        + Invoice.PROPERTY_PAYMENTMETHOD);
    result.put(AnalyticsFactOrder.PROPERTY_DOCUMENTNO, orderPath + "." + Order.PROPERTY_DOCUMENTNO);

    result.put(AnalyticsFactOrder.PROPERTY_BILLTOLOCATION, orderPath + "."
        + Order.PROPERTY_INVOICEADDRESS + "."
        + org.openbravo.model.common.businesspartner.Location.PROPERTY_LOCATIONADDRESS);

    result.put(AnalyticsFactOrder.PROPERTY_REGION, orderPath + "." + Order.PROPERTY_INVOICEADDRESS
        + "." + org.openbravo.model.common.businesspartner.Location.PROPERTY_LOCATIONADDRESS + "."
        + Location.PROPERTY_REGION);
    result.put(AnalyticsFactOrder.PROPERTY_COUNTRY, orderPath + "." + Order.PROPERTY_INVOICEADDRESS
        + "." + org.openbravo.model.common.businesspartner.Location.PROPERTY_LOCATIONADDRESS + "."
        + Location.PROPERTY_REGION + "." + Region.PROPERTY_COUNTRY);

    result.put(AnalyticsFactOrder.PROPERTY_PRICELIST, invoicePath + "."
        + Invoice.PROPERTY_PRICELIST);

    result.put(AnalyticsFactOrder.PROPERTY_DATEORDERED, orderPath + "." + Order.PROPERTY_ORDERDATE);
    result.put(AnalyticsFactOrder.PROPERTY_ORDERDATE, orderPath + "." + Order.PROPERTY_ORDERDATE);
    result.put(AnalyticsFactOrder.PROPERTY_SCHEDULEDDELIVERYDATE, orderPath + "."
        + Order.PROPERTY_SCHEDULEDDELIVERYDATE);
    result.put(AnalyticsFactOrder.PROPERTY_DATEDELIVERED, orderLinePath + "."
        + OrderLine.PROPERTY_DATEDELIVERED);
    result.put(AnalyticsFactOrder.PROPERTY_DATEINVOICED, InvoiceLine.PROPERTY_INVOICE + "."
        + Invoice.PROPERTY_INVOICEDATE);
    result.put(AnalyticsFactOrder.PROPERTY_INVOICEDATE, InvoiceLine.PROPERTY_INVOICE + "."
        + Invoice.PROPERTY_INVOICEDATE);
    result.put(AnalyticsFactOrder.PROPERTY_CONVERSIONDATE, InvoiceLine.PROPERTY_INVOICE + "."
        + Invoice.PROPERTY_INVOICEDATE);

    result.put(AnalyticsFactOrder.PROPERTY_ORDEREDQUANTITY, orderLinePath + "."
        + OrderLine.PROPERTY_ORDEREDQUANTITY);
    result.put(AnalyticsFactOrder.PROPERTY_DELIVEREDQUANTITY, orderLinePath + "."
        + OrderLine.PROPERTY_DELIVEREDQUANTITY);
    result.put(AnalyticsFactOrder.PROPERTY_INVOICEDQUANTITY, InvoiceLine.PROPERTY_INVOICEDQUANTITY);
    result.put(AnalyticsFactOrder.PROPERTY_LISTPRICE, InvoiceLine.PROPERTY_LISTPRICE);
    result.put(AnalyticsFactOrder.PROPERTY_UNITPRICE, InvoiceLine.PROPERTY_UNITPRICE);
    result.put(AnalyticsFactOrder.PROPERTY_LINENETAMOUNT, InvoiceLine.PROPERTY_LINENETAMOUNT);
    result.put(AnalyticsFactOrder.PROPERTY_DISCOUNT, InvoiceLine.PROPERTY_INVOICEDISCOUNT);
    result.put(AnalyticsFactOrder.PROPERTY_CONVERSIONDATE, InvoiceLine.PROPERTY_INVOICE + "."
        + Invoice.PROPERTY_INVOICEDATE);
    result.put(AnalyticsFactOrder.PROPERTY_GOODSSHIPMENTLINE,
        InvoiceLine.PROPERTY_GOODSSHIPMENTLINE);
    result.put(AnalyticsFactOrder.PROPERTY_GRANDTOTALAMOUNT, orderPath + "."
        + Order.PROPERTY_GRANDTOTALAMOUNT);
    return result;
  }

  @Override
  protected void processObject(InvoiceLine invoiceLine, AnalyticsFactOrder orderFact) {

    final OrderLine orderLine = invoiceLine.getSalesOrderLine();
    final Order salesOrder = orderLine.getSalesOrder();
    orderFact.setSalesOrderLine(orderLine);
    orderFact.setSalesOrder(salesOrder);
    final Invoice salesInvoice = invoiceLine.getInvoice();
    final ShipmentInOutLine inoutLine = orderFact.getGoodsShipmentLine();

    orderFact.setInvoiceLine(invoiceLine);
    orderFact.setInvoice(salesInvoice);
    if (orderFact.getCurrency() == null) {
      orderFact.setCurrency(salesInvoice.getCurrency());
    }
    if (orderFact.getReturnReason() == null) {
      orderFact.setReturnReason(salesOrder.getReturnReason());
    }
    if (orderFact.getProject() == null) {
      orderFact.setProject(salesInvoice.getProject());
    }
    if (orderFact.getStDimension() == null) {
      orderFact.setStDimension(salesInvoice.getStDimension());
    }
    if (orderFact.getNdDimension() == null) {
      orderFact.setNdDimension(salesInvoice.getNdDimension());
    }

    final BigDecimal discountAmount = sumDiscounts(invoiceLine);
    orderFact.setLineNetDiscountAmount(discountAmount);
    final BigDecimal taxAmount = sumTaxes(invoiceLine);
    orderFact.setLineNetTaxAmount(taxAmount);
    BigDecimal cost = BigDecimal.ZERO;
    if (inoutLine != null) {
      cost = sumCosts(inoutLine, orderFact.getCurrency(), orderFact.getConversionDate());
    }
    if (orderFact.getInvoicedQuantity().compareTo(ZERO) < 0) {

      orderFact.setLineReturnedQuantity(orderFact.getInvoicedQuantity().negate());
      orderFact.setLineQuantity(ZERO);

      orderFact.setLineReturnAmount(orderFact.getLineNetAmount().negate());
      orderFact.setLineAmount(ZERO);

      orderFact.setLineReturnDiscountAmount(discountAmount.negate());
      orderFact.setLineDiscount(ZERO);

      orderFact.setLineReturnTaxAmount(taxAmount.negate());
      orderFact.setLineTaxAmount(ZERO);

      orderFact.setLineNetCost(cost.negate());
      orderFact.setLineReturnCost(cost);
      orderFact.setLineCost(ZERO);
    } else {
      orderFact.setLineQuantity(orderFact.getInvoicedQuantity());
      orderFact.setLineReturnedQuantity(ZERO);

      orderFact.setLineAmount(orderFact.getLineNetAmount());
      orderFact.setLineReturnAmount(ZERO);

      orderFact.setLineDiscount(discountAmount);
      orderFact.setLineReturnDiscountAmount(ZERO);

      orderFact.setLineTaxAmount(taxAmount);
      orderFact.setLineReturnTaxAmount(ZERO);

      orderFact.setLineNetCost(cost);
      orderFact.setLineReturnCost(ZERO);
      orderFact.setLineCost(cost);
    }

    // prevent divide by zero
    if (orderFact.getLineNetAmount().compareTo(ZERO) > 0
        || orderFact.getLineNetAmount().compareTo(ZERO) < 0) {
      orderFact.setGrossMargin(orderFact.getLineNetAmount().subtract(orderFact.getLineNetCost()));
    } else {
      orderFact.setGrossMargin(ZERO);
    }

    if (orderFact.getDiscount() != null && orderFact.getDiscount().compareTo(ZERO) > 0) {
      orderFact.setOverrideOriginalNetAmount((orderFact.getLineNetAmount().multiply(HUNDERD)
          .divide(HUNDERD.subtract(orderFact.getDiscount()), RoundingMode.HALF_UP)));
    }
    if (orderFact.getInvoicedQuantity() != BigDecimal.ZERO) {
      orderFact.setUnitPrice(orderFact.getLineNetAmount().divide(orderFact.getInvoicedQuantity(),
          orderFact.getCurrency().getPricePrecision().intValue(), RoundingMode.HALF_UP));
    }
  }

  private BigDecimal sumCosts(ShipmentInOutLine inoutLine, Currency currencyTo, Date conversionDate) {
    BigDecimal cost = BigDecimal.ZERO;
    final String hql = "select cost.currency, cost.cost from " + TransactionCost.ENTITY_NAME
        + " cost, " + MaterialTransaction.ENTITY_NAME + " transaction " + " where transaction."
        + MaterialTransaction.PROPERTY_GOODSSHIPMENTLINE + ".id=:inoutLineId and cost."
        + TransactionCost.PROPERTY_INVENTORYTRANSACTION + "=transaction";
    final Query qry = OBDal.getInstance().getSession().createQuery(hql);
    qry.setParameter("inoutLineId", DalUtil.getId(inoutLine));
    for (Object record : qry.list()) {
      final Object[] values = (Object[]) record;
      final Currency currency = (Currency) values[0];
      final BigDecimal transactionCost = (BigDecimal) values[1];
      cost = cost.add((BigDecimal) getConvertedAmount(transactionCost, currency, currencyTo,
          conversionDate));
    }
    return cost;
  }

  private BigDecimal sumDiscounts(InvoiceLine invoiceLine) {
    final String hql = "select sum(" + InvoiceLineOffer.PROPERTY_TOTALAMOUNT + ") from "
        + InvoiceLineOffer.ENTITY_NAME + " where " + InvoiceLineOffer.PROPERTY_INVOICELINE
        + "=:invoiceLine";
    final Query qry = OBDal.getInstance().getSession().createQuery(hql);
    qry.setParameter("invoiceLine", invoiceLine);
    final Object result = qry.uniqueResult();
    if (result == null) {
      return ZERO;
    }
    return (BigDecimal) result;
  }

  private BigDecimal sumTaxes(InvoiceLine invoiceLine) {
    final String hql = "select sum(" + InvoiceLineTax.PROPERTY_TAXAMOUNT + ") from "
        + InvoiceLineTax.ENTITY_NAME + " where " + InvoiceLineTax.PROPERTY_INVOICELINE
        + "=:invoiceLine";
    final Query qry = OBDal.getInstance().getSession().createQuery(hql);
    qry.setParameter("invoiceLine", invoiceLine);
    final Object result = qry.uniqueResult();
    if (result == null) {
      return ZERO;
    }
    return (BigDecimal) result;
  }

  protected void purgeFacts() {
    // TODO: maybe also check the product or other data...
    // Note: no implicit joins are allowed in the where clause, an inner select is however allowed
    final String qryStr = "delete " + getFactEntityName()
        + " olFact where (olFact.updated < (select il.updated from " + InvoiceLine.ENTITY_NAME
        + " as il where il.id = olFact." + AnalyticsFactOrder.PROPERTY_INVOICELINE
        + ".id) or olFact.updated < (select i.updated from " + Invoice.ENTITY_NAME
        + " as i where olFact." + AnalyticsFactOrder.PROPERTY_INVOICELINE + "."
        + InvoiceLine.PROPERTY_INVOICE + ".id = i.id)) and olFact.client.id=:client";
    OBDal.getInstance().getSession().createQuery(qryStr)
        .setString("client", OBContext.getOBContext().getCurrentClient().getId()).executeUpdate();
  }

  protected String getFactEntityName() {
    return AnalyticsFactOrder.ENTITY_NAME;
  }

}