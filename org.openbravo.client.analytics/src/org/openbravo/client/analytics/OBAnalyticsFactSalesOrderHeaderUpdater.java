/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.client.analytics;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.InvoiceLineTax;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.geography.Location;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.invoice.InvoiceLineOffer;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;

public class OBAnalyticsFactSalesOrderHeaderUpdater extends
    OBAnalyticsFactUpdater<Order, AnalyticsFactSalesOrderHeader> {

  @Override
  protected ScrollableResults getData() {
    final Date fromDate = getLastUpdateRunDate();
    String strQry = "select o from " + Order.ENTITY_NAME + " o";
    strQry += " where o.client.id = :client and o.active = true";
    strQry += "   and o." + Order.PROPERTY_SALESTRANSACTION + " is true";
    strQry += "   and o." + Order.PROPERTY_DOCUMENTSTATUS + " IN ('CO', 'CL')";
    strQry += "   and not exists(select fact.id from " + AnalyticsFactSalesOrderHeader.ENTITY_NAME
        + " fact where fact." + AnalyticsFactSalesOrderHeader.PROPERTY_SALESORDER + ".id=o.id)";
    strQry += "   and o." + Order.PROPERTY_DOCUMENTTYPE + "." + DocumentType.PROPERTY_REVERSAL
        + " = false";
    strQry += " and exists(select il.id from " + InvoiceLine.ENTITY_NAME + " il where il."
        + InvoiceLine.PROPERTY_INVOICE + ".active=true and il." + InvoiceLine.PROPERTY_INVOICE
        + "." + Invoice.PROPERTY_DOCUMENTSTATUS + " IN ('CO', 'CL') and il."
        + InvoiceLine.PROPERTY_SALESORDERLINE + "." + OrderLine.PROPERTY_SALESORDER + "=o and "
        + InvoiceLine.PROPERTY_INVOICE + "." + Invoice.PROPERTY_DOCUMENTTYPE + "."
        + DocumentType.PROPERTY_REVERSAL + " = false)";

    final Query qry = OBDal.getInstance().getSession().createQuery(strQry);
    qry.setString("client", OBContext.getOBContext().getCurrentClient().getId());
    qry.setReadOnly(true);
    qry.setFetchSize(5000);
    return qry.scroll(ScrollMode.FORWARD_ONLY);
  }

  @Override
  protected Map<String, String> getPropertyMap() {
    final String strBPLocLocation = org.openbravo.model.common.businesspartner.Location.PROPERTY_LOCATIONADDRESS;
    final String strBPLocRegion = strBPLocLocation + "." + Location.PROPERTY_REGION;
    final String strBPLocCountry = strBPLocLocation + "." + Location.PROPERTY_COUNTRY;
    final String strBPPath = Order.PROPERTY_BUSINESSPARTNER;
    final Map<String, String> result = new HashMap<String, String>();
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_CLIENT, Order.PROPERTY_CLIENT);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_ORGANIZATION, Order.PROPERTY_ORGANIZATION);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_BUSINESSPARTNER,
        Order.PROPERTY_BUSINESSPARTNER);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_BUSINESSPARTNERCATEGORY, strBPPath + "."
        + BusinessPartner.PROPERTY_BUSINESSPARTNERCATEGORY);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_BUSINESSPARTNERTAXCATEGORY, strBPPath + "."
        + BusinessPartner.PROPERTY_TAXCATEGORY);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_LOCATIONADDRESS,
        Order.PROPERTY_PARTNERADDRESS + "." + strBPLocLocation);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_REGION, Order.PROPERTY_PARTNERADDRESS + "."
        + strBPLocRegion);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_COUNTRY, Order.PROPERTY_PARTNERADDRESS + "."
        + strBPLocCountry);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_WAREHOUSE, Order.PROPERTY_WAREHOUSE);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_CURRENCY, Order.PROPERTY_CURRENCY);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_DOCUMENTTYPE, Order.PROPERTY_DOCUMENTTYPE);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_TRANSACTIONDOCUMENT,
        Order.PROPERTY_TRANSACTIONDOCUMENT);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_SALESREPRESENTATIVE,
        Order.PROPERTY_SALESREPRESENTATIVE);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_INVOICELOCATIONADDRESS,
        Order.PROPERTY_INVOICEADDRESS + "." + strBPLocLocation);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_INVOICEREGION, Order.PROPERTY_INVOICEADDRESS
        + "." + strBPLocRegion);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_INVOICECOUNTRY, Order.PROPERTY_INVOICEADDRESS
        + "." + strBPLocCountry);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_DELIVERYLOCATION,
        Order.PROPERTY_DELIVERYLOCATION + "." + strBPLocLocation);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_DELIVERYREGION,
        Order.PROPERTY_DELIVERYLOCATION + "." + strBPLocRegion);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_DELIVERYCOUNTRY,
        Order.PROPERTY_DELIVERYLOCATION + "." + strBPLocCountry);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_PAYMENTTERMS, Order.PROPERTY_PAYMENTTERMS);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_PRICELIST, Order.PROPERTY_PRICELIST);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_PAYMENTMETHOD, Order.PROPERTY_PAYMENTMETHOD);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_RETURNREASON, Order.PROPERTY_RETURNREASON);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_DOCUMENTNO, Order.PROPERTY_DOCUMENTNO);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_ORDERDATE, Order.PROPERTY_ORDERDATE);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_DATEORDERED, Order.PROPERTY_ORDERDATE);
    result.put(AnalyticsFactSalesOrderHeader.PROPERTY_SCHEDULEDDELIVERYDATE,
        Order.PROPERTY_SCHEDULEDDELIVERYDATE);
    return result;
  }

  @Override
  protected void processObject(Order order, AnalyticsFactSalesOrderHeader factSOH) {
    factSOH.setSalesOrder(order);
    BigDecimal qty = BigDecimal.ZERO;
    BigDecimal qtyNet = BigDecimal.ZERO;
    BigDecimal qtyRet = BigDecimal.ZERO;
    BigDecimal amt = BigDecimal.ZERO;
    BigDecimal amtNet = BigDecimal.ZERO;
    BigDecimal amtRet = BigDecimal.ZERO;
    BigDecimal disAmt = BigDecimal.ZERO;
    BigDecimal disAmtNet = BigDecimal.ZERO;
    BigDecimal disAmtRet = BigDecimal.ZERO;
    BigDecimal dirDisAmt = BigDecimal.ZERO;
    BigDecimal dirDisAmtNet = BigDecimal.ZERO;
    BigDecimal dirDisAmtRet = BigDecimal.ZERO;
    BigDecimal taxAmtNet = order.getGrandTotalAmount().subtract(order.getSummedLineAmount());
    BigDecimal taxAmt = BigDecimal.ZERO;
    BigDecimal taxAmtRet = BigDecimal.ZERO;

    final String invoiceLineQryStr = "select il from " + InvoiceLine.ENTITY_NAME + " il where il."
        + InvoiceLine.PROPERTY_SALESORDERLINE + "." + OrderLine.PROPERTY_SALESORDER
        + "=:order and il." + InvoiceLine.PROPERTY_INVOICE + "." + Invoice.PROPERTY_DOCUMENTSTATUS
        + " in ('CO', 'CL') and il." + InvoiceLine.PROPERTY_ACTIVE + "=true";
    final Query invoiceLineQry = OBDal.getInstance().getSession().createQuery(invoiceLineQryStr);
    invoiceLineQry.setEntity("order", order);
    int cnt = 0;
    for (Object o : invoiceLineQry.list()) {
      cnt++;
      final InvoiceLine invoiceLine = (InvoiceLine) o;
      BigDecimal lineNetAmt = invoiceLine.getLineNetAmount();
      BigDecimal lineQty = invoiceLine.getInvoicedQuantity();
      // Regular lines
      BigDecimal priceList = invoiceLine.getListPrice();
      BigDecimal lineDirDisAmt = lineQty.multiply(priceList).subtract(lineNetAmt)
          .setScale(lineNetAmt.scale(), BigDecimal.ROUND_HALF_UP);
      final BigDecimal discountAmount = sumDiscounts(invoiceLine);
      final BigDecimal taxAmount = getTaxAmt(invoiceLine);

      qtyNet = qtyNet.add(lineQty);
      dirDisAmtNet = dirDisAmtNet.add(lineDirDisAmt);
      amtNet = amtNet.add(lineNetAmt);
      disAmtNet = disAmtNet.add(discountAmount);
      taxAmtNet = taxAmtNet.add(taxAmount);

      if (lineQty.signum() == -1) {
        // Return line
        qtyRet = qtyRet.add(lineQty.negate());
        amtRet = amtRet.add(lineNetAmt.negate());
        dirDisAmtRet = dirDisAmtRet.add(lineDirDisAmt.negate());
        disAmtRet = disAmtRet.add(discountAmount.negate());
        taxAmtRet = taxAmtRet.add(taxAmount.negate());
      } else {
        qty = qty.add(lineQty);
        amt = amt.add(lineNetAmt);
        dirDisAmt = dirDisAmt.add(lineDirDisAmt);
        taxAmt = taxAmt.add(taxAmount);
        disAmt = disAmt.add(discountAmount);
      }
    }

    factSOH.setLineCount(new Long(cnt));
    factSOH.setQuantity(qty);
    factSOH.setQuantityNet(qtyNet);
    factSOH.setQuantityReturned(qtyRet);
    factSOH.setAmount(amt);
    factSOH.setAmountNet(amtNet);
    factSOH.setAmountReturn(amtRet);
    factSOH.setTaxAmount(taxAmt);
    factSOH.setTaxAmountNet(taxAmtNet);
    factSOH.setTaxAmountReturn(taxAmtRet);
    factSOH.setDiscountAmount(disAmt);
    factSOH.setDiscountAmountNet(disAmtNet);
    factSOH.setDiscountAmountReturn(disAmtRet);
    factSOH.setDirectDiscountAmount(dirDisAmt);
    factSOH.setDirectDiscountAmountNet(dirDisAmtNet);
    factSOH.setDirectDiscountAmountReturn(dirDisAmtRet);
  }

  protected String getConversionDatePropertyName() {
    return AnalyticsFactSalesOrderHeader.PROPERTY_ORDERDATE;
  }

  private BigDecimal getTaxAmt(InvoiceLine invoiceLine) {
    BigDecimal taxAmt = BigDecimal.ZERO;
    for (InvoiceLineTax invoiceLineTax : invoiceLine.getInvoiceLineTaxList()) {
      taxAmt.add(invoiceLineTax.getTaxAmount());
    }
    return taxAmt;
  }

  private BigDecimal sumDiscounts(InvoiceLine invoiceLine) {
    final String hql = "select sum(" + InvoiceLineOffer.PROPERTY_TOTALAMOUNT + ") from "
        + InvoiceLineOffer.ENTITY_NAME + " where " + InvoiceLineOffer.PROPERTY_INVOICELINE
        + "=:invoiceLine";
    final Query qry = OBDal.getInstance().getSession().createQuery(hql);
    qry.setParameter("invoiceLine", invoiceLine);
    final Object result = qry.uniqueResult();
    if (result == null) {
      return ZERO;
    }
    return (BigDecimal) result;
  }

  @Override
  protected void purgeFacts() {
    // Note: no implicit joins are allowed in the where clause, an inner select is however allowed
    String strQry = "delete " + getFactEntityName() + " soFact";
    strQry += " where soFact.client.id = :client and (soFact.updated < (";
    strQry += "           select o.updated from " + Order.ENTITY_NAME + " as o";
    strQry += "           where o.id = soFact." + AnalyticsFactSalesOrderHeader.PROPERTY_SALESORDER;
    strQry += "         ) or exists(select il.id from " + InvoiceLine.ENTITY_NAME + " il where "
        + " (il.updated >= soFact.updated  or il." + InvoiceLine.PROPERTY_INVOICE
        + ".updated >= soFact.updated) and il." + InvoiceLine.PROPERTY_SALESORDERLINE + "."
        + OrderLine.PROPERTY_SALESORDER + "= soFact."
        + AnalyticsFactSalesOrderHeader.PROPERTY_SALESORDER + "))";
    OBDal.getInstance().getSession().createQuery(strQry)
        .setString("client", OBContext.getOBContext().getCurrentClient().getId()).executeUpdate();
  }

  @Override
  protected String getFactEntityName() {
    return AnalyticsFactSalesOrderHeader.ENTITY_NAME;
  }

}
