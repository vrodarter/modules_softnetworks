/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.currency.ConversionRate;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchema;

/**
 * Base class for updating fact tables from transactional tables.
 * 
 * S is the source type/class, F is the target fact type.
 * 
 * @author mtaal
 */
public abstract class OBAnalyticsFactUpdater<S extends BaseOBObject, F extends BaseOBObject> {
  private static final Logger log4j = Logger.getLogger(OBAnalyticsFactUpdater.class);

  protected static final BigDecimal ZERO = new BigDecimal("0.0");
  protected static final BigDecimal ONE = new BigDecimal("1.0");
  protected static final BigDecimal HUNDERD = new BigDecimal("100.0");

  protected boolean needsCurrencyConversion = false;
  List<Currency> currencyList = new ArrayList<Currency>();
  List<Property> amountAndPricePropertyList = new ArrayList<Property>();

  protected static final String REFERENCEID_Price = "800008";
  protected static final String REFERENCEID_Amount = "12";
  protected static final String REFERENCEID_Currency = "141";
  private Map<String, List<CachedConversionRate>> conversionRates = new HashMap<String, List<CachedConversionRate>>();

  @Inject
  @Any
  private Instance<OBAnalyticsFactObjectUpdater<?, ?>> factObjectUpdaters;

  @SuppressWarnings({ "rawtypes", "unchecked" })
  public String executeUpdate() {
    boolean err = true;
    try {
      OBContext.setAdminMode(false);

      if (log4j.isDebugEnabled()) {
        log4j.debug("Started fact updating with fact updater: " + this.getClass().getName());
      }

      purgeFacts();

      if (log4j.isDebugEnabled()) {
        log4j.debug("Purged facts (" + this.getClass().getName() + ")");
      }

      initializeCurrencyConversionVariables();

      final Map<String, String> propertyMap = getPropertyMap();

      // support execution order
      final List<OBAnalyticsFactObjectUpdater<BaseOBObject, BaseOBObject>> sortedFactObjectUpdaters = new ArrayList<OBAnalyticsFactObjectUpdater<BaseOBObject, BaseOBObject>>();
      for (OBAnalyticsFactObjectUpdater factObjectUpdater : factObjectUpdaters) {
        sortedFactObjectUpdaters.add(factObjectUpdater);
      }
      Collections.sort(sortedFactObjectUpdaters);

      final long t = System.currentTimeMillis();
      int created = 0;
      final ScrollableResults scroller = getData();
      while (scroller.next()) {
        final S sourceBob = (S) scroller.get(0);

        final F factBob = getTargetFactObject(sourceBob);

        setMappedProperties(sourceBob, factBob, propertyMap);
        processObject(sourceBob, factBob);
        for (OBAnalyticsFactObjectUpdater factObjectUpdater : sortedFactObjectUpdaters) {
          if (factObjectUpdater.canHandleFact(this, sourceBob, factBob)) {
            factObjectUpdater.handleFact(this, sourceBob, factBob);
          }
        }

        doSave(factBob, sourceBob);
        created++;

        if ((created % 1000) == 0) {
          // clear the session every 1000 objects to prevent out of memory errors
          OBDal.getInstance().flush();
          OBDal.getInstance().getSession().clear();

          // System.err.println("Fact updater " + this.getClass().getName() + " flushed, handled "
          // + created + " source objects in " + (System.currentTimeMillis() - t)
          // + " milliseconds (" + this.getClass().getName() + ")");

          if (log4j.isDebugEnabled()) {
            log4j.debug("Fact update flushed, handled " + created + " source objects in "
                + (System.currentTimeMillis() - t) + " milliseconds (" + this.getClass().getName()
                + ")");
          }
        }
      }
      OBDal.getInstance().commitAndClose();
      err = false;
      return "(Re)created " + created + " fact records";
    } catch (Exception ex) {
      ex.printStackTrace();
      log4j.error(ex.getMessage(), ex);
      return "Error";
    } finally {
      if (err) {
        try {
          OBDal.getInstance().rollbackAndClose();
        } finally {
          OBContext.restorePreviousMode();
        }
      } else {
        OBContext.restorePreviousMode();
      }
    }
  }

  /**
   * Computes which properties stores amounts/price values, i.e. values which need to be converted
   * if there is a currency. Will automatically find all properties which use the references: Amount
   * or Price.
   * 
   * Can be overridden by subclass to add or remove properties explicitly.
   * 
   * Is called once at the start of the update for each fact updater implementation.
   * 
   * The actual currency conversion happens in the {@link #doSave(BaseOBObject, BaseOBObject)}
   * method.
   * 
   * @param factEntity
   *          the entity of the Fact table
   * @return the list of {@link Property} which are currency converted.
   */
  protected List<Property> computeAmountAndPriceProperties(Entity factEntity) {
    List<Property> propertyList = new ArrayList<Property>();
    String referenceId = "";
    for (Property property : factEntity.getProperties()) {
      referenceId = property.getDomainType().getReference().getId();
      if (isAmountOrPrice(referenceId)) {
        propertyList.add(property);
      }
    }
    return propertyList;
  }

  private Currency getDefaultCurrency() {
    if (null != OBContext.getOBContext().getCurrentOrganization().getCurrency()) {
      return OBContext.getOBContext().getCurrentOrganization().getCurrency();
    }
    return OBContext.getOBContext().getCurrentClient().getCurrency();
  }

  protected boolean existCurrencyConversionProperties() {
    final Entity factEntity = ModelProvider.getInstance().getEntity(getFactEntityName());
    return getCurrencyPropertyName() != null && getConversionDatePropertyName() != null
        && null != factEntity.getProperty(getCurrencyPropertyName(), false)
        && null != factEntity.getProperty(getConversionDatePropertyName(), false);
  }

  protected BigDecimal getConversionRate(Currency origCurrency, Currency currencyTo,
      Date conversionDate) {
    final String key = getConversionRateKey(origCurrency, currencyTo);
    final List<CachedConversionRate> rates = conversionRates.get(key);
    if (rates == null) {
      return ONE;
    }
    final long conversionMillis = conversionDate.getTime();
    for (CachedConversionRate rate : rates) {
      if (rate.from <= conversionMillis && conversionMillis <= rate.to) {
        return rate.multiplyBy;
      }
    }
    return ONE;
  }

  private Map<String, List<CachedConversionRate>> readConversionRates() {
    final Map<String, List<CachedConversionRate>> result = new HashMap<String, List<CachedConversionRate>>();
    OBContext.setAdminMode(true);
    try {
      OBCriteria<ConversionRate> obc = OBDal.getInstance().createCriteria(ConversionRate.class);
      obc.setFilterOnReadableOrganization(false);
      for (ConversionRate conversionRate : obc.list()) {
        final String key = getConversionRateKey(conversionRate.getCurrency(),
            conversionRate.getToCurrency());

        List<CachedConversionRate> rates = result.get(key);
        if (rates == null) {
          rates = new ArrayList<CachedConversionRate>();
          result.put(key, rates);
        }
        rates.add(new CachedConversionRate(conversionRate));
      }
      return result;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private String getConversionRateKey(Currency from, Currency to) {
    // read the id without loading the object
    return DalUtil.getId(from) + "_" + DalUtil.getId(to);
  }

  @SuppressWarnings("hiding")
  private List<Currency> getCurrencies() {
    List<Currency> currencyList = new ArrayList<Currency>();
    OBCriteria<AcctSchema> obc = OBDal.getInstance().createCriteria(AcctSchema.class);
    obc.setFilterOnReadableClients(true);
    obc.setFilterOnReadableOrganization(false);
    for (AcctSchema schema : obc.list()) {
      if (!currencyList.contains(schema.getCurrency())) {
        currencyList.add(schema.getCurrency());
      }
    }
    final Client client = OBDal.getInstance().get(Client.class,
        OBContext.getOBContext().getCurrentClient().getId());
    final Currency clientCurrency = OBDal.getInstance().get(Currency.class,
        client.getCurrency().getId());
    if (!currencyList.contains(clientCurrency)) {
      currencyList.add(clientCurrency);
    }
    final OBQuery<Organization> orgsQry = OBDal.getInstance().createQuery(Organization.class,
        "client=:client");
    orgsQry.setNamedParameter("client", client);
    for (Organization org : orgsQry.list()) {
      if (org.getCurrency() != null && !currencyList.contains(org.getCurrency())) {
        currencyList.add(org.getCurrency());
      }
    }
    return currencyList;
  }

  /**
   * Checks if currency conversion handling is needed, if so creates a separate fact for each
   * currency. Calls then the {@link #saveFact(BaseOBObject, BaseOBObject)} method to do the actual
   * save for each fact object.
   * 
   * If there are no currencies to convert then directly calls
   * {@link #saveFact(BaseOBObject, BaseOBObject)}
   */
  protected void doSave(F factBob, S sourceBob) {

    if (needsCurrencyConversion) {
      Currency origCurrency = getCurrency(sourceBob, factBob);
      Date conversionDate = getConversionDate(sourceBob, factBob);
      for (Currency currency : currencyList) {
        @SuppressWarnings("unchecked")
        F factBobCopy = (F) DalUtil.copy(factBob);
        if (!currency.getId().equals(origCurrency.getId())) {
          for (Property property : amountAndPricePropertyList) {
            BigDecimal amount = (BigDecimal) factBobCopy.get(property.getName());
            if (amount != null) {
              BigDecimal convertedAmount = getConvertedAmount(amount, origCurrency, currency,
                  conversionDate);
              factBobCopy.set(property.getName(), convertedAmount);
            }
          }
          setCurrency(factBobCopy, currency);
        }
        saveFact(factBobCopy, sourceBob);
      }
    } else {
      saveFact(factBob, sourceBob);
    }
  }

  /**
   * Does the actual save of the fact object, calls {@link OBDal#save(Object)}.
   */
  protected void saveFact(F factBob, S sourceBob) {
    OBDal.getInstance().save(factBob);
  }

  /**
   * @return the last updated date/time of the fact table
   */
  protected Date getLastUpdateRunDate() {
    final Query qry = OBDal
        .getInstance()
        .getSession()
        .createQuery("select max(updated) from " + getFactEntityName() + " where client.id=:client");
    qry.setString("client", OBContext.getOBContext().getCurrentClient().getId());
    qry.setMaxResults(1);
    final Date dt = (Date) qry.uniqueResult();
    if (dt == null) {
      return new Date(0);
    }
    return dt;
  }

  /**
   * Can be overridden to set extra features after the standard properties. Is called after the
   * properties defined in the {@link #getPropertyMap()} have been set, before the fact object is
   * saved.
   */
  protected void processObject(S sourceBob, F factBob) {

  }

  /**
   * Gets the property map from {@link #getPropertyMap()} and then for each property copies the
   * value from the source to the target.
   * 
   * @param sourceBob
   * @param targetBob
   */
  protected void setMappedProperties(S sourceBob, F factBob, Map<String, String> propertyMap) {
    for (String propertyName : propertyMap.keySet()) {
      final String fromProperty = propertyMap.get(propertyName);
      final Object value = getValueFromPropertyPath(sourceBob, fromProperty.split("\\."), 0);
      setValue(factBob, propertyName, value);
    }
  }

  /**
   * Sets the value in the fact record for the property name, translates a date value in a
   * {@link TimeDimension} instance/reference.
   */
  protected void setValue(F factBob, String propertyName, Object value) {
    if (!(value instanceof Date)) {
      factBob.setValue(propertyName, value);
      return;
    }

    // a date, check if it is really a time dimension entry
    // if so get the time dimension and use it.
    final Property prop = factBob.getEntity().getProperty(propertyName);
    if (prop.getTargetEntity() != null
        && prop.getTargetEntity().getName().equals(TimeDimension.ENTITY_NAME)) {
      final Date dtValue = (Date) value;
      factBob.setValue(propertyName, TimeDimensionProvider.getInstance().getTimeDimension(dtValue));
    } else {
      // not a time dimension, use the datevalue
      factBob.setValue(propertyName, value);
    }
  }

  /**
   * Is used to get a value from the source object, is normally not overridden, but can be used to
   * return a specific value if needed, instead of overriding this method consider to override the
   * {@link #setValue(BaseOBObject, String, Object)} method.
   */
  protected Object getValueFromPropertyPath(BaseOBObject sourceBob, String[] propertyPath, int index) {
    final String propertyName = propertyPath[index];
    final Object value = sourceBob.getValue(propertyName);
    if (index == (propertyPath.length - 1)) {
      return value;
    }
    if (value instanceof BaseOBObject) {
      return getValueFromPropertyPath((BaseOBObject) value, propertyPath, index + 1);
    }
    if (value == null) {
      return null;
    }
    // incorrect case
    log4j.error("PropertyPath " + toString(propertyPath) + " resolves midway (at index: " + index
        + ") to a non-BaseOBObject value " + value);
    return null;
  }

  private String toString(String[] propertyPath) {
    final StringBuilder sb = new StringBuilder();
    for (String propertyName : propertyPath) {
      if (sb.length() > 0) {
        sb.append(".");
      }
      sb.append(propertyName);
    }
    return sb.toString();
  }

  private boolean isAmountOrPrice(String referenceId) {
    return REFERENCEID_Price.equals(referenceId) || REFERENCEID_Amount.equals(referenceId);
  }

  private void initializeCurrencyConversionVariables() {
    final Entity factEntity = ModelProvider.getInstance().getEntity(getFactEntityName());
    if (existCurrencyConversionProperties()) {
      needsCurrencyConversion = true;
      currencyList = getCurrencies();
      amountAndPricePropertyList = computeAmountAndPriceProperties(factEntity);
      conversionRates = readConversionRates();
    } else {
      currencyList.add(getDefaultCurrency());
    }
  }

  protected BigDecimal getConvertedAmount(BigDecimal amount, Currency origCurrency,
      Currency currencyTo, Date conversionDate) {

    // check if conversion is needed
    final String fromCurrencyId = (String) DalUtil.getId(origCurrency);
    final String toCurrencyId = (String) DalUtil.getId(currencyTo);
    if (toCurrencyId == null || fromCurrencyId == null || fromCurrencyId.equals(toCurrencyId)) {
      // not needed, return the original
      return amount;
    }

    final BigDecimal multipleRateBy = getConversionRate(origCurrency, currencyTo, conversionDate);
    final BigDecimal convertedValue = amount.multiply(multipleRateBy);

    return convertedValue.setScale(currencyTo.getStandardPrecision().intValue(),
        RoundingMode.HALF_UP);
  }

  protected Currency getCurrency(S sourceBob, F factBob) {
    return (Currency) factBob.getValue(getCurrencyPropertyName());
  }

  protected void setCurrency(F factBob, Currency currency) {
    factBob.set(getCurrencyPropertyName(), currency);
  }

  protected Date getConversionDate(S sourceBob, F factBob) {
    final Object value = factBob.getValue(getConversionDatePropertyName());
    if (value instanceof TimeDimension) {
      return ((TimeDimension) value).getThetime();
    }
    return (Date) value;
  }

  protected String getCurrencyPropertyName() {
    return "currency";
  }

  protected String getConversionDatePropertyName() {
    return "conversiondate";
  }

  /**
   * Create an instance of F on the basis of an instance of S. As a default uses the
   * {@link #getFactEntityName()} to create an empty instance.
   * 
   * @param sourceBob
   *          the source object
   * @return an instance of the target object.
   */
  @SuppressWarnings("unchecked")
  protected F getTargetFactObject(S sourceBob) {
    return (F) OBProvider.getInstance().get(getFactEntityName());
  }

  /**
   * @return the transactional records to use as the basis of updating the fact table. The
   *         {@link ScrollableResults} should return return instances of the source BaseOBObject
   *         defined in the generic parameter S of this class.
   */
  protected abstract ScrollableResults getData();

  /**
   * @return a mapping of transactional properties from the core transactional table (generics
   *         parameter S) to the fact table properties, you can use a propertyPath
   *         (salesOrder.businessPartner.bpCategory) to denote the source object. Openbravo will
   *         automatically handle converting date objects to the Openbravo {@link TimeDimension}.
   */
  protected abstract Map<String, String> getPropertyMap();

  /**
   * A method which is responsible for deleting facts from the fact table which should be updated.
   * The Openbravo fact updater does in fact not update existing fact records, it removes stale
   * records and then re-creates them.
   */
  protected abstract void purgeFacts();

  public String executeClearFactData() {
    boolean err = true;
    try {
      OBContext.setAdminMode(false);

      if (log4j.isDebugEnabled()) {
        log4j.debug("Started fact data clearing with fact updater: " + this.getClass().getName());
      }
      OBDal.getInstance().flush();
      clearFactData();
      OBDal.getInstance().commitAndClose();
      err = false;
      return "success";
    } catch (Exception ex) {
      ex.printStackTrace();
      log4j.error(ex.getMessage(), ex);
      return "Error";
    } finally {
      if (err) {
        try {
          OBDal.getInstance().rollbackAndClose();
        } finally {
          OBContext.restorePreviousMode();
        }
      } else {
        OBContext.restorePreviousMode();
      }
    }
  }

  /**
   * Does the real delete of fact data using the statement DELETE entity. Can be overridden to
   * perform other actions
   */
  protected void clearFactData() {
    OBDal.getInstance().getSession().createQuery("DELETE " + getFactEntityName()).executeUpdate();
  }

  /**
   * @return the entity name corresponding to the Fact entity (the F generic parameter of this
   *         class)
   */
  protected abstract String getFactEntityName();

  private class CachedConversionRate {

    final public long from;
    final public long to;
    final public BigDecimal multiplyBy;

    CachedConversionRate(ConversionRate rate) {
      from = rate.getValidFromDate().getTime();
      to = rate.getValidToDate().getTime();
      multiplyBy = rate.getMultipleRateBy();
    }
  }

}