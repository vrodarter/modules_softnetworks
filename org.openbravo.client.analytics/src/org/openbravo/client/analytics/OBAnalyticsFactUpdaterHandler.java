/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

/**
 * Iterates over all fact updaters and runs them.
 * 
 * @author mtaal
 */
public class OBAnalyticsFactUpdaterHandler {

  @Inject
  @Any
  private Instance<OBAnalyticsFactUpdater<?, ?>> factUpdaters;

  public String executeUpdate() {
    final StringBuilder sb = new StringBuilder();
    for (OBAnalyticsFactUpdater<?, ?> factUpdater : factUpdaters) {
      sb.append(factUpdater.getClass().getSimpleName() + ": " + factUpdater.executeUpdate() + "\n");
    }

    // flush all the caches
    MondrianProvider.getInstance().flushCacheAndClearConnections();

    return sb.toString();
  }

  public String executeClearFactData() {
    final StringBuilder sb = new StringBuilder();
    for (OBAnalyticsFactUpdater<?, ?> factUpdater : factUpdaters) {
      sb.append(factUpdater.getClass().getSimpleName() + ": " + factUpdater.executeClearFactData()
          + "\n");
    }

    // flush all the caches
    MondrianProvider.getInstance().flushCacheAndClearConnections();

    return sb.toString();
  }
}