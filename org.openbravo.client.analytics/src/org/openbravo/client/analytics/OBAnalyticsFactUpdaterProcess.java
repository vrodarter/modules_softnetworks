/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import org.openbravo.base.weld.WeldUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

/**
 * The process class for updating all fact tables.
 * 
 * @author mtaal
 */
public class OBAnalyticsFactUpdaterProcess extends DalBaseProcess {

  public void doExecute(ProcessBundle bundle) throws Exception {

    if (!MondrianProvider.getInstance().isMondrianServer()) {
      return;
    }

    try {

      final String result = WeldUtils.getInstanceFromStaticBeanManager(
          OBAnalyticsFactUpdaterHandler.class).executeUpdate();

      // updates trees and the time dimension
      OBOlapClosureTreeHandler.getInstance().update();
      TimeDimensionProvider.getInstance().setTimeDimensionReferences(false);

      // OBError is also used for successful results
      final OBError msg = new OBError();
      msg.setType("Success");
      msg.setTitle(result);

      bundle.setResult(msg);

    } catch (final Exception e) {
      e.printStackTrace(System.err);
      final OBError msg = new OBError();
      msg.setType("Error");
      msg.setMessage(e.getMessage());
      msg.setTitle("Error occurred");
      bundle.setResult(msg);
    }
  }
}
