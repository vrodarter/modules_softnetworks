/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.utility.Tree;

/**
 * Set of utility methods for Analytics
 * 
 * @author mtaal
 */
public class OBAnalyticsUtils {
  private static final Logger log = Logger.getLogger(OBAnalyticsUtils.class);

  private final static String SAIKU_QRY_NAME_PART = "name=\"";

  /**
   * Return a md5 identifying a cached query
   */
  public static String getMd5ForCachedQuery(Role role, String mdx) {
    return DigestUtils.md5Hex(role.getId() + mdx);
  }

  /**
   * Make the queryname unique so it won't be cached, ie be removed directly after execution. See
   * the OBSaikuQueryService class.
   */
  public static String makeQueryNameUnique(String query) {
    final int firstIndex = query.indexOf(SAIKU_QRY_NAME_PART);
    if (firstIndex == -1) {
      log.error("The following Saiku query can not be made unique " + query);
      return query;
    }
    if (query.contains(MondrianConstants.MONDRIAN_QRY_TAGGER)) {
      // already unique bailing out
      return query;
    }
    final String firstPart = query.substring(0, firstIndex);
    final String lastPart = query.substring(firstIndex + SAIKU_QRY_NAME_PART.length());
    return firstPart + SAIKU_QRY_NAME_PART + System.currentTimeMillis()
        + OBContext.getOBContext().getUser().getId() + MondrianConstants.MONDRIAN_QRY_TAGGER
        + lastPart;
  }

  /**
   * Get the Member Name for Saiku, for example: [parent].[parent].[member]
   */
  public static String getMemberName(BaseOBObject baseOBObject) {
    Tree tree = getTree(baseOBObject.getEntity());
    String memberName = "";
    memberName = "[" + baseOBObject.getId() + "]";
    if (tree != null) {
      BaseOBObject parent = baseOBObject;
      while (parent != null) {
        parent = getParent(parent, tree);
        if (parent == null) {
          break;
        }
        memberName = "[" + parent.getId() + "]." + memberName.toString();
      }
    }
    return memberName;
  }

  public static Tree getTree(Entity entity) {
    final String treeType = ModelProvider.getInstance().getTable(entity.getTableName())
        .getTreeType();
    if (treeType == null) {
      return null;
    }
    final String clientId = OBContext.getOBContext().getCurrentClient().getId();
    final List<Tree> trees = OBDal
        .getInstance()
        .createQuery(
            Tree.class,
            Tree.PROPERTY_TYPEAREA + "='" + treeType + "' and " + Tree.PROPERTY_CLIENT + "='"
                + clientId + "'").list();
    if (trees.isEmpty()) {
      return null;
    }
    if (trees.size() > 1) {
      log.warn("More than 1 tree for treeType " + treeType);
    }

    return trees.get(0);
  }

  public static BaseOBObject getParent(BaseOBObject child, Tree tree) {
    OBContext.setAdminMode(true);
    try {
      final Property parentProperty = OBOlapClosureTreeHandler.getTreeParentProperty(child
          .getEntity());
      if (parentProperty != null) {
        final String parentId = (String) child.get(parentProperty.getName());
        if (parentId == null) {
          return null;
        }
        return OBDal.getInstance().get(child.getEntityName(), parentId);
      }
      final OBQuery<BaseOBObject> obq = OBDal.getInstance().createQuery(
          TreeClosure.ENTITY_NAME,
          TreeClosure.PROPERTY_TREE + "=:tree and " + TreeClosure.PROPERTY_CHILD + "=:child and "
              + TreeClosure.PROPERTY_DISTANCE + "=1");
      obq.setFilterOnReadableClients(false);
      obq.setFilterOnReadableOrganization(false);
      obq.setNamedParameter("tree", tree);
      obq.setNamedParameter("child", child.getId());
      return obq.uniqueResult();
    } finally {
      OBContext.restorePreviousMode();
    }
  }
}
