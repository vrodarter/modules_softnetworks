/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

/**
 * Exception thrown when a Mondrian query/connection is asked on a server not designated for
 * Mondrian.
 * 
 * @author mtaal
 * @see MondrianConstants#MONDRIAN_SERVER_PROPERTY
 * @see MondrianProvider#isMondrianServer()
 */
public class OBMondrianDifferentServerException extends RuntimeException {
  private static final long serialVersionUID = 1L;
}
