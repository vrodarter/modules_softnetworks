/*
 ************************************************************************************
 * Copyright (C) 2014-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import mondrian.olap.Annotation;
import mondrian.olap.Member;
import mondrian.spi.MemberFormatter;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.impl.SessionImpl;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.model.domaintype.DomainType;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.domain.List;
import org.openbravo.model.ad.domain.ListTrl;
import org.openbravo.model.ad.domain.Reference;

/**
 * Reads the identifier for members/enums.
 * 
 * @author mtaal
 */
public class OBMondrianMemberFormatter implements MemberFormatter {

  private String cachedLanguageId = null;
  private String cachedYesLabel = null;
  private String cachedNoLabel = null;

  private SimpleDateFormat dateFormat = null;

  @Override
  public String formatMember(Member member) {
    OBContext.setAdminMode(true);
    try {
      if (member.getLevel().getAnnotationMap()
          .containsKey(MondrianSchemaGenerator.ANNOTATION_ENTITY)) {
        final String identifierKey = "Identifier_" + member.getName();
        Annotation identifierAnnotation = member.getLevel().getAnnotationMap().get(identifierKey);
        if (identifierAnnotation != null) {
          return (String) identifierAnnotation.getValue();
        }

        final Annotation annotation = member.getLevel().getAnnotationMap()
            .get(MondrianSchemaGenerator.ANNOTATION_ENTITY);
        if (annotation != null) {
          final Entity entity = ModelProvider.getInstance().getEntity(
              (String) annotation.getValue());
          final String identifier = getIdentifier(entity, member.getName());

          // prevent the hb session cache of getting too large in one MDX query
          flushSession();

          if (identifier != null) {
            identifierAnnotation = new IdentifierAnnotation(identifierKey, identifier);
            member.getLevel().getAnnotationMap().put(identifierKey, identifierAnnotation);

            return identifier;
          }
        }
      }
      if (member.getLevel().getAnnotationMap()
          .containsKey(MondrianSchemaGenerator.ANNOTATION_DATEFILTERMEMBER)) {
        return formatDateValueMemberValue(member.getName());
      }
      if (member.getLevel().getAnnotationMap()
          .containsKey(MondrianSchemaGenerator.ANNOTATION_YESNOPROPERTY)) {
        return getYesNoLabel(member.getName());
      }
      if (member.getLevel().getAnnotationMap()
          .containsKey(MondrianSchemaGenerator.ANNOTATION_LISTPROPERTY)) {

        final Annotation annotation = member.getLevel().getAnnotationMap()
            .get(MondrianSchemaGenerator.ANNOTATION_LISTPROPERTY);
        if (annotation != null) {
          // concatenation of entity name and property name
          final String propertyLocator = (String) annotation.getValue();

          // cache using the user id
          final String identifierKey = propertyLocator + "_" + member.getName() + "_"
              + OBContext.getOBContext().getUser().getId();
          Annotation identifierAnnotation = member.getLevel().getAnnotationMap().get(identifierKey);
          if (identifierAnnotation != null) {
            return (String) identifierAnnotation.getValue();
          }

          final String[] parts = propertyLocator.split("\\.");
          final Entity entity = ModelProvider.getInstance().getEntity(parts[0]);
          final Property property = entity.getProperty(parts[1]);
          final DomainType domainType = property.getDomainType();
          // read the reference again as it may be cached in mem and then was read using another
          // session
          final Reference reference = OBDal.getInstance().get(Reference.class,
              domainType.getReference().getId());
          for (List list : reference.getADListList()) {
            if (list.getSearchKey().equals(member.getName())) {
              String name = list.getName();
              final String languageId = OBContext.getOBContext().getLanguage().getId();
              for (ListTrl listTrl : list.getADListTrlList()) {
                if (languageId.equals(DalUtil.getId(listTrl.getLanguage()))) {
                  name = listTrl.getName();
                  break;
                }
              }
              identifierAnnotation = new IdentifierAnnotation(identifierKey, name);
              member.getLevel().getAnnotationMap().put(identifierKey, identifierAnnotation);
              return name;
            }
          }
        }
      }
      return member.getName();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  // synchronized as date formats are not thread safe
  private synchronized String formatDateValueMemberValue(String strValue) {
    if (dateFormat == null) {
      dateFormat = new SimpleDateFormat(OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty("dateFormat.java"));
    }
    final int year = Integer.parseInt(strValue.substring(0, 4));
    final int month = Integer.parseInt(strValue.substring(4, 6));
    final int day = Integer.parseInt(strValue.substring(6, 8));
    final Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.YEAR, year);
    calendar.set(Calendar.MONTH, month - 1);
    calendar.set(Calendar.DAY_OF_MONTH, day);
    return dateFormat.format(calendar.getTime());
  }

  private void flushSession() {
    final Session session = OBDal.getInstance().getSession();
    if (((SessionImpl) session).getPersistenceContext().getEntityEntries().size() > 1000) {
      session.flush();
      session.clear();
    }
  }

  private String getIdentifier(Entity entity, Object id) {
    if (isSimpleIdentifier(entity)) {
      final String qryStr = "select " + entity.getIdentifierProperties().get(0).getName()
          + " from " + entity.getName() + " where id=:id";
      final Query qry = OBDal.getInstance().getSession().createQuery(qryStr);
      qry.setParameter("id", id);
      final Object identifier = qry.uniqueResult();
      if (identifier != null) {
        return identifier.toString();
      }
    }
    final BaseOBObject bob = OBDal.getInstance().get(entity.getName(), id);
    if (bob != null) {
      return bob.getIdentifier();
    }
    return null;
  }

  private boolean isSimpleIdentifier(Entity entity) {
    if (entity.getIdentifierProperties().size() != 1) {
      return false;
    }
    final Property identifierProperty = entity.getIdentifierProperties().get(0);
    return identifierProperty.isPrimitive() && !identifierProperty.isTranslatable();
  }

  private synchronized String getYesNoLabel(String name) {
    if (!OBContext.getOBContext().getLanguage().getId().equals(cachedLanguageId)) {
      cachedYesLabel = OBMessageUtils.messageBD("Yes");
      cachedNoLabel = OBMessageUtils.messageBD("No");
      cachedLanguageId = OBContext.getOBContext().getLanguage().getId();
    }
    if ("Y".equals(name)) {
      return cachedYesLabel;
    }
    return cachedNoLabel;
  }

  private static class IdentifierAnnotation implements Annotation {
    private final String value;
    private final String name;

    private IdentifierAnnotation(String name, String value) {
      this.name = name;
      this.value = value;
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public Object getValue() {
      return value;
    }
  }
}