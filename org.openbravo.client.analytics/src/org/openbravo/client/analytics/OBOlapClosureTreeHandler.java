/*
 ************************************************************************************
 * Copyright (C) 2014-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.model.ad.module.Module;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Tree;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.model.common.enterprise.Organization;

/**
 * Takes care of updating the closure and the ordinal tables.
 * 
 * See here: http://mondrian.pentaho.com/documentation/schema.php#Closure_tables
 * 
 * @author mtaal
 */
public class OBOlapClosureTreeHandler {
  private static final Logger log = Logger.getLogger(OBOlapClosureTreeHandler.class);

  private static final String ANALYTICS_MODULE_ID = "46C4143F1E17442B96E64C3E9934275E";

  private static OBOlapClosureTreeHandler instance = new OBOlapClosureTreeHandler();

  public static OBOlapClosureTreeHandler getInstance() {
    return instance;
  }

  public static void setInstance(OBOlapClosureTreeHandler instance) {
    OBOlapClosureTreeHandler.instance = instance;
  }

  public static BaseOBObject getParent(BaseOBObject child, Tree tree) {
    return OBAnalyticsUtils.getParent(child, tree);
  }

  public static Property getTreeParentProperty(Entity entity) {
    for (Property prop : entity.getProperties()) {
      if (prop.getName().toLowerCase()
          .endsWith(Organization.PROPERTY_OBANALYTREEPARENTID.toLowerCase())) {
        return prop;
      }
    }
    return null;
  }

  public static Property getTreeOrdinalProperty(Entity entity) {
    for (Property prop : entity.getProperties()) {
      if (prop.getName().toLowerCase()
          .endsWith(Organization.PROPERTY_OBANALYTREEORDINAL.toLowerCase())) {
        return prop;
      }
    }
    return null;
  }

  public synchronized void update() {
    if (!MondrianProvider.getInstance().isMondrianServer()) {
      return;
    }

    long execTime = System.currentTimeMillis();

    OBContext.setAdminMode(true);
    try {
      boolean fullUpdate = needFullUpdate();

      if (!fullUpdate && !needToUpdateTree(null)) {
        // not required to update any tree
        log.debug("No update of tree closure and analysis tree tables");
        return;
      }

      log.debug("Updating tree closure and analysis tree tables");

      // set the module version in the preference
      // on purpose do not do client/org check otherwise ad_preference saving fails
      OBContext.setAdminMode(false);
      try {
        final Module module = OBDal.getInstance().get(Module.class, ANALYTICS_MODULE_ID);
        Preferences.setPreferenceValue("OBANALY_UpdateVersion", module.getVersion(), true, OBDal
            .getInstance().get(Client.class, OBContext.getOBContext().getCurrentClient().getId()),
            OBDal.getInstance().get(Organization.class, "0"), null, null, null, null);
        OBDal.getInstance().flush();
      } finally {
        OBContext.restorePreviousMode();
      }

      if (fullUpdate) {
        purgeCurrentEntries(null);
        OBDal.getInstance().flush();
        log.debug("Done purging tree closure and analysis tree tables");
      }

      final List<String> treeTypes = new ArrayList<String>();
      for (Entity entity : ModelProvider.getInstance().getModel()) {
        if (entity.getTreeType() != null && !treeTypes.contains(entity.getTreeType())) {
          treeTypes.add(entity.getTreeType());
        }
      }
      for (String treeType : treeTypes) {
        buildClosureEntries(treeType, fullUpdate);
      }
      OBDal.getInstance().commitAndClose();
    } catch (Throwable t) {
      try {
        OBDal.getInstance().rollbackAndClose();
      } catch (Throwable ignore) {
      }
      throw new OBException(t);
    } finally {
      log.debug("Tree closure updated in " + (System.currentTimeMillis() - execTime) + " ms");
      OBContext.restorePreviousMode();
    }
  }

  /** A full update is required on each new Analytics module version */
  private boolean needFullUpdate() {
    try {
      String version = Preferences.getPreferenceValue("OBANALY_UpdateVersion", true, OBContext
          .getOBContext().getCurrentClient().getId(), "0", null, null, null);
      final Module module = OBDal.getInstance().get(Module.class, ANALYTICS_MODULE_ID);
      if (version == null) {
        return true;
      }
      if (!version.equals(module.getVersion())) {
        return true;
      }
    } catch (PropertyException e) {
      return true;
    }
    return false;
  }

  /**
   * A tree needs to be updated in case any of its nodes has been updated after update in the
   * closure table for that same tree.
   * 
   * @param tree
   *          tree to check, <code>null</code> represents any tree
   * @return if the tree requires to be updated, if tree is null the result indicates whether some
   *         tree require to be updated
   */
  private boolean needToUpdateTree(Tree tree) {
    long treeNodeUpdated = 0;
    final Date treeNodeUpdatedDate = getLastUpdated(TreeNode.ENTITY_NAME, tree);
    if (treeNodeUpdatedDate != null) {
      treeNodeUpdated = treeNodeUpdatedDate.getTime();
    } else {
      return false;
    }

    long listCnUpdated = -1;
    final Date listCnUpdatedDate = getLastUpdated(TreeClosure.ENTITY_NAME, tree);
    if (listCnUpdatedDate != null) {
      listCnUpdated = listCnUpdatedDate.getTime();
    } else {
      return true;
    }
    // the treenodes have been updated after the closure tables
    // do an update
    return listCnUpdated < treeNodeUpdated;
  }

  private Date getLastUpdated(String entityName, Tree tree) {
    String hql = "select max(updated) from " + entityName;
    if (tree != null) {
      hql += " where tree = :tree";
    }
    Query qry = OBDal.getInstance().getSession().createQuery(hql);
    if (tree != null) {
      qry.setParameter("tree", tree);
    }
    return (Date) qry.uniqueResult();
  }

  /**
   * Removes entries in closure table for a concrete tree, if tree is <code>null</code> removes all
   * entries for any tree
   */
  private void purgeCurrentEntries(Tree tree) {
    final Session session = OBDal.getInstance().getSession();
    int deletedEntities;

    if (tree == null) {
      deletedEntities = session.createQuery("delete " + TreeClosure.ENTITY_NAME).executeUpdate();
      log.debug("Deleted all TreeClosure entries: " + deletedEntities);
    } else {
      Query qDelete = session.createQuery("delete " + TreeClosure.ENTITY_NAME
          + " where tree = :tree");
      qDelete.setParameter("tree", tree);
      deletedEntities = qDelete.executeUpdate();
      if (deletedEntities > 0) {
        log.debug("Deleted " + deletedEntities + " entries for tree " + tree);
      }
    }
  }

  private void buildClosureEntries(String treeType, boolean fullUpdate) {
    final Session session = OBDal.getInstance().getSession();
    final Query treesQry = session.createQuery("select t from " + Tree.ENTITY_NAME + " t where "
        + Tree.PROPERTY_TYPEAREA + "=:treeType");
    treesQry.setParameter("treeType", treeType);
    for (Object o : treesQry.list()) {
      final Tree tree = (Tree) o;
      if (!fullUpdate && !needToUpdateTree(tree)) {
        continue;
      }

      log.debug("Building closure entries for tree " + tree);

      if (!fullUpdate) {
        // for full update all entities were previously purged
        purgeCurrentEntries(tree);
      }

      final Set<LocalTreeNode> roots = new HashSet<LocalTreeNode>();
      final Map<String, LocalTreeNode> allNodes = new HashMap<String, LocalTreeNode>();
      final Query treeNodesQry = session.createQuery("select tn from " + TreeNode.ENTITY_NAME
          + " tn where " + TreeNode.PROPERTY_TREE + ".id=:treeId order by sequenceNumber");
      treeNodesQry.setParameter("treeId", tree.getId());
      for (Object on : treeNodesQry.list()) {
        final TreeNode treeNode = (TreeNode) on;

        // ignore the virtual root
        if (treeNode.getNode().equals("0") && !treeType.equals("OO")) {
          continue;
        }

        // pointing to itself, ignore
        if (treeNode.getReportSet() != null && treeNode.getReportSet().equals(treeNode.getNode())) {
          continue;
        }

        LocalTreeNode child = allNodes.get(treeNode.getNode());
        if (child == null) {
          child = new LocalTreeNode();
          child.setTree(tree);
          child.setId(treeNode.getNode());
          allNodes.put(child.getId(), child);

          // add to the root, will be removed if the child
          // happens to have a parent
          roots.add(child);
        }

        // the ones pointing to a 0 parent are considered to be the root
        if (treeNode.getReportSet() == null
            || (!treeType.equals("OO") && treeNode.getReportSet().equals("0"))) {
          continue;
        }

        roots.remove(child);

        LocalTreeNode parent = allNodes.get(treeNode.getReportSet());
        if (parent == null) {
          parent = new LocalTreeNode();
          parent.setId(treeNode.getReportSet());
          parent.setTree(tree);
          allNodes.put(parent.getId(), parent);
          roots.add(parent);
        }
        parent.getChildren().add(child);
      }

      int ordinal = 1;
      for (LocalTreeNode root : roots) {
        ordinal = root.updateOrdinalColumn(ordinal);
        ordinal++;
      }

      final Organization organization = OBDal.getInstance().get(Organization.class, "0");
      for (LocalTreeNode root : roots) {
        root.createClosureEntries(organization, true);
      }
    }
  }

  private class LocalTreeNode {
    private String id;
    private Tree tree;
    private java.util.List<LocalTreeNode> children = new ArrayList<LocalTreeNode>();

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public java.util.List<LocalTreeNode> getChildren() {
      return children;
    }

    private void createClosureEntries(Organization organization, boolean isRoot) {

      // set our own parentId to null;
      if (isRoot) {
        updateParentIdColumn(null, id);
      }

      // create an entry for the record itself, is needed
      {
        final TreeClosure treeClosure = OBProvider.getInstance().get(TreeClosure.class);
        treeClosure.setTree(tree);
        treeClosure.setChild(id);
        treeClosure.setParent(id);
        treeClosure.setRoot(isRoot);
        treeClosure.setOrganization(organization);
        // distance is 0 to ourselves
        treeClosure.setDistance(new Long(0));
        OBDal.getInstance().save(treeClosure);
      }

      for (LocalTreeNode child : children) {
        final TreeClosure treeClosure = OBProvider.getInstance().get(TreeClosure.class);
        treeClosure.setTree(tree);
        treeClosure.setParent(id);
        treeClosure.setChild(child.getId());
        treeClosure.setOrganization(organization);
        treeClosure.setDistance(new Long(1));
        OBDal.getInstance().save(treeClosure);
        updateParentIdColumn(id, child.getId());
        child.createClosureEntries(organization, false);
        child.createClosureEntriesForParent(organization, id, 2);
      }
    }

    private void createClosureEntriesForParent(Organization organization, String parentId,
        long distance) {
      long localDistance = distance + 1;
      for (LocalTreeNode child : children) {
        final TreeClosure treeClosure = OBProvider.getInstance().get(TreeClosure.class);
        treeClosure.setTree(tree);
        treeClosure.setParent(parentId);
        treeClosure.setChild(child.getId());
        treeClosure.setOrganization(organization);
        treeClosure.setDistance(distance);
        OBDal.getInstance().save(treeClosure);
        child.createClosureEntriesForParent(organization, parentId, localDistance);
      }
    }

    private int updateOrdinalColumn(int ordinal) {
      final Entity entity = ModelProvider.getInstance().getEntityFromTreeType(tree.getTypeArea());
      final Property treeOrdinalProperty = getTreeOrdinalProperty(entity);
      if (entity == null || treeOrdinalProperty == null) {
        return ordinal;
      }

      final String dml = "update " + entity.getName() + " set " + treeOrdinalProperty.getName()
          + "=" + ordinal + " where id=:id";
      final Query qry = OBDal.getInstance().getSession().createQuery(dml);
      qry.setParameter("id", id);
      final int count = qry.executeUpdate();
      if (count != 1) {
        log.error("Unexpected update ordinal count " + count + " for treetype "
            + tree.getTypeArea() + " using dml " + dml);
      }

      int childOrdinal = ordinal + 1;
      for (LocalTreeNode child : children) {
        childOrdinal = child.updateOrdinalColumn(childOrdinal);
      }
      return childOrdinal;
    }

    private void updateParentIdColumn(String parentId, String childId) {
      final Entity entity = ModelProvider.getInstance().getEntityFromTreeType(tree.getTypeArea());
      final Property treeParentProperty = getTreeParentProperty(entity);
      if (entity == null || treeParentProperty == null) {
        return;
      }

      final String localParentId;
      if (parentId == null) {
        localParentId = "null";
      } else {
        localParentId = "'" + parentId + "'";
      }
      final String dml = "update " + entity.getName() + " set " + treeParentProperty.getName()
          + "=" + localParentId + " where id=:childId";
      final Query qry = OBDal.getInstance().getSession().createQuery(dml);
      qry.setParameter("childId", childId);
      final int count = qry.executeUpdate();
      if (count != 1) {
        log.error("Unexpected update parentId count " + count + " for treetype "
            + tree.getTypeArea() + " using dml " + dml);
      }
    }

    public void setTree(Tree tree) {
      this.tree = tree;
    }
  }

}
