/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

/**
 * Updates the entities which have a time dimension column so that their references to the time
 * dimension table are set.
 * 
 * In addition updates the tree close table.
 * 
 * @author mtaal
 */
public class OBOlapTimeDimensionTreeClosureUpdatingProcess extends DalBaseProcess {

  public void doExecute(ProcessBundle bundle) throws Exception {

    if (!MondrianProvider.getInstance().isMondrianServer()) {
      return;
    }

    try {
      final int updateCnt = TimeDimensionProvider.getInstance().setTimeDimensionReferences(false);

      // also do the tree closures here, to be on the safe side...
      OBOlapClosureTreeHandler.getInstance().update();

      // OBError is also used for successful results
      final OBError msg = new OBError();
      msg.setType("Success");
      msg.setTitle("Update done, " + updateCnt + " time dimension references set");

      bundle.setResult(msg);

    } catch (final Exception e) {
      e.printStackTrace(System.err);
      final OBError msg = new OBError();
      msg.setType("Error");
      msg.setMessage(e.getMessage());
      msg.setTitle("Error occurred");
      bundle.setResult(msg);
    }
  }
}
