/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.saiku.datasources.connection.AbstractConnectionManager;
import org.saiku.datasources.connection.ISaikuConnection;
import org.saiku.datasources.datasource.SaikuDatasource;

/**
 * Integration of OB and Saiku.
 * 
 * @author mtaal
 */

public class OBSaikuConnectionManager extends AbstractConnectionManager {

  private static OBSaikuConnectionManager singletonInstance = null;

  private static void setSingleton(OBSaikuConnectionManager instance) {
    if (singletonInstance != null) {
      throw new OBException();
    }
    singletonInstance = instance;
  }

  public static OBSaikuConnectionManager getObSaikuConnectionManager() {
    return singletonInstance;
  }

  private Map<String, ISaikuConnection> connections = new HashMap<String, ISaikuConnection>();

  public OBSaikuConnectionManager() {
    setSingleton(this);
  }

  @Override
  public void destroy() {
    if (connections != null && !connections.isEmpty()) {
      for (ISaikuConnection con : connections.values()) {
        try {
          Connection c = con.getConnection();
          if (!c.isClosed()) {
            c.close();
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    connections.clear();
  }

  @Override
  public void init() {
  }

  public void clearAllConnections() {
    try {
      for (ISaikuConnection conn : connections.values()) {
        conn.clearCache();
        if (conn instanceof OBSaikuOlapConnection) {
          ((OBSaikuOlapConnection) conn).close();
        }
      }
      connections.clear();
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  @Override
  protected ISaikuConnection getInternalConnection(String name, SaikuDatasource datasource) {
    final String connectionName = getConnectionName(name);
    ISaikuConnection con = null;
    if (!connections.containsKey(connectionName)) {
      con = connect(connectionName, datasource);
      connections.put(connectionName, con);
    } else {
      con = connections.get(connectionName);
    }
    return con;

  }

  private String getConnectionName(String name) {
    return name + "-" + OBContext.getOBContext().getUser().getUsername() + "-"
        + OBContext.getOBContext().getRole().getIdentifier();
  }

  @Override
  protected ISaikuConnection refreshInternalConnection(String name, SaikuDatasource datasource) {
    try {
      final String connectionName = getConnectionName(name);

      ISaikuConnection con = connections.remove(connectionName);
      if (con != null) {
        con.clearCache();
      }
      con = null;
      return getInternalConnection(name, datasource);
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  protected ISaikuConnection connect(String name, SaikuDatasource dataSource) {
    try {
      OBSaikuOlapConnection con = new OBSaikuOlapConnection();
      con.setName(dataSource.getName());
      con.setProperties(dataSource.getProperties());
      con.connect();
      if (con.initialized()) {
        return con;
      }
      return null;
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  @Override
  public void refreshAllConnections() {
    // flush all the caches
    MondrianProvider.getInstance().flushCache();

    super.refreshAllConnections();
  }
}