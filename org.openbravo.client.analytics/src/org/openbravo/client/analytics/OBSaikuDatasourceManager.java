/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.saiku.datasources.datasource.SaikuDatasource;
import org.saiku.datasources.datasource.SaikuDatasource.Type;
import org.saiku.service.datasource.IDatasourceManager;

/**
 * Integration of OB and Saiku.
 * 
 * @author mtaal
 */

public class OBSaikuDatasourceManager implements IDatasourceManager {

  public OBSaikuDatasourceManager() {
  }

  @Override
  public SaikuDatasource addDatasource(SaikuDatasource arg0) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<SaikuDatasource> addDatasources(List<SaikuDatasource> arg0) {
    throw new UnsupportedOperationException();
  }

  @Override
  public SaikuDatasource getDatasource(String arg0) {
    return getDatasources().get(arg0);
  }

  @Override
  public Map<String, SaikuDatasource> getDatasources() {
    final Map<String, SaikuDatasource> dataSources = new HashMap<String, SaikuDatasource>();

    SaikuDatasource dataSource = new SaikuDatasource(MondrianConstants.CATALOG_SCHEMA_NAME,
        Type.OLAP, new Properties());

    dataSources.put(dataSource.getName(), dataSource);

    return dataSources;
  }

  @Override
  public void load() {
  }

  @Override
  public boolean removeDatasource(String arg0) {
    return false;
  }

  @Override
  public SaikuDatasource setDatasource(SaikuDatasource arg0) {
    return getDatasources().get(arg0.getName());
  }

}