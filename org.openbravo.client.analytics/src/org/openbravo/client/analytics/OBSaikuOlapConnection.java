/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import mondrian.rolap.RolapConnection;

import org.olap4j.OlapConnection;
import org.saiku.datasources.connection.ISaikuConnection;

/**
 * Integration of OB and Saiku.
 * 
 * @author mtaal
 */

public class OBSaikuOlapConnection implements ISaikuConnection {

  private Properties properties = new Properties();
  private OlapConnection olapConnection;
  private String name;

  @Override
  public boolean clearCache() throws Exception {
    if (olapConnection != null && olapConnection.isWrapperFor(RolapConnection.class)) {
      final RolapConnection rcon = olapConnection.unwrap(RolapConnection.class);
      rcon.getCacheControl(null).flushSchemaCache();
    }
    return true;
  }

  @Override
  public boolean connect() throws Exception {
    olapConnection = MondrianProvider.getInstance().getOlapConnection(properties);
    return olapConnection != null;
  }

  @Override
  public boolean connect(Properties props) throws Exception {
    olapConnection = MondrianProvider.getInstance().getOlapConnection(props);
    return olapConnection != null;
  }

  @Override
  public Connection getConnection() {
    return olapConnection;
  }

  public void close() {
    try {
      if (olapConnection != null && !olapConnection.isClosed()) {
        olapConnection.close();
      }
    } catch (SQLException ignore) {

    }
  }

  @Override
  public String getDatasourceType() {
    return ISaikuConnection.OLAP_DATASOURCE;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public boolean initialized() {
    return olapConnection != null;
  }

  @Override
  public void setProperties(Properties props) {
    properties = props;
  }

  public void setName(String name) {
    this.name = name;
  }
}