/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import org.apache.log4j.Logger;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.common.enterprise.Organization;
import org.saiku.olap.dto.resultset.CellDataSet;
import org.saiku.olap.util.formatter.ICellSetFormatter;
import org.saiku.service.olap.OlapQueryService;

/**
 * Integration of OB and Saiku. Extend QueryService to measure query timings.
 * 
 * @author mtaal
 */

public class OBSaikuQueryService extends OlapQueryService {
  private static final long serialVersionUID = 1L;
  private static final Logger log = Logger.getLogger(OBSaikuQueryService.class);

  private static OBSaikuQueryService singleton = null;

  /**
   * Saiku caches queries in the query service and never removes them it seems.
   */
  public static void clearCachedQueries() {
    if (singleton != null) {
      singleton.getQueries().clear();
    }
  }

  private long cacheTreshold = MondrianConstants.MDX_CACHE_TRESHOLD;
  private long preloadEnabledTreshold = MondrianConstants.MDX_PRELOAD_ENABLED_TRESHOLD;
  private boolean didSetTresholds = false;

  public OBSaikuQueryService() {
    singleton = this;
  }

  public CellDataSet execute(String queryName, ICellSetFormatter formatter) {

    initTresholds();

    final CellDataSet result = super.execute(queryName, formatter);
    // we should not cache the mdx we execute for caching
    if (!MondrianCachePreloadProcess.isDoingCachePreloading() && result.runtime > cacheTreshold) {
      final String mdx = getMDXQuery(queryName);
      if (mdx != null) {
        try {
          storeMdx(mdx, result.runtime);
        } catch (Throwable t) {
          log.error("Error when storing mdx " + mdx + " " + t.getMessage(), t);
        }
      }
    }

    // remove the query which are being used for reports and such
    // as these queries are parameterised
    if (queryName.contains(MondrianConstants.MONDRIAN_QRY_TAGGER)) {
      closeQuery(queryName);
    }

    return result;
  }

  private void initTresholds() {
    if (didSetTresholds) {
      return;
    }

    try {
      final String val = OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty(MondrianConstants.MDX_CACHE_TRESHOLD_PROP_NAME);
      if (val != null) {
        cacheTreshold = Long.parseLong(val);
      }
    } catch (Throwable t) {
      log.error(t);
    }

    try {
      final String val = OBPropertiesProvider.getInstance().getOpenbravoProperties()
          .getProperty(MondrianConstants.MDX_PRELOAD_ENABLED_TRESHOLD_PROP_NAME);
      if (val != null) {
        preloadEnabledTreshold = Long.parseLong(val);
      }
    } catch (Throwable t) {
      log.error(t);
    }

    didSetTresholds = true;
  }

  private void storeMdx(String mdx, long timeLapsed) {
    OBContext.setAdminMode(true);
    try {
      final OBQuery<CubeCacheDefinition> cacheDefQry = OBDal.getInstance().createQuery(
          CubeCacheDefinition.class, CubeCacheDefinition.PROPERTY_HASHTEXT + "=:md5");
      cacheDefQry.setFilterOnReadableOrganization(false);
      final String md5 = OBAnalyticsUtils.getMd5ForCachedQuery(OBContext.getOBContext().getRole(),
          mdx);
      cacheDefQry.setNamedParameter("md5", md5);
      boolean found = false;
      for (CubeCacheDefinition cacheDef : cacheDefQry.list()) {
        // already there update it
        if (cacheDef.getRole() != null
            && cacheDef.getRole().getId().equals(OBContext.getOBContext().getRole().getId())) {
          Long numOfRuns = cacheDef.getNumberofruns() != null ? cacheDef.getNumberofruns() : 1;
          Long avgTime = cacheDef.getAveragetime() != null ? cacheDef.getAveragetime() : 0;

          final long newAvgTime = (numOfRuns * avgTime + timeLapsed) / (numOfRuns + 1);
          cacheDef.setNumberofruns(1 + numOfRuns);
          if (cacheDef.getNumberofruns() > preloadEnabledTreshold) {
            cacheDef.setUseinpreload(true);
          }
          cacheDef.setAveragetime(newAvgTime);
          found = true;
          break;
        }
      }
      // create a new one
      if (!found) {
        final CubeCacheDefinition cacheDef = OBProvider.getInstance()
            .get(CubeCacheDefinition.class);
        cacheDef.setOrganization(OBDal.getInstance().get(Organization.class, "0"));
        cacheDef.setMdx(mdx);
        cacheDef.setHashtext(md5);
        cacheDef.setNumberofruns(1l);
        cacheDef.setAveragetime(timeLapsed);
        cacheDef.setUseinpreload(false);
        cacheDef.setRole(OBDal.getInstance().get(Role.class,
            OBContext.getOBContext().getRole().getId()));
        OBDal.getInstance().save(cacheDef);
        OBDal.getInstance().flush();
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

}