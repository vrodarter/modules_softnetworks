/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.saiku.web.rest.objects.repository.IRepositoryObject;
import org.saiku.web.rest.resources.ISaikuRepository;

/**
 * Integration of OB and Saiku.
 * 
 * @author mtaal
 */

public class OBSaikuRepository implements ISaikuRepository {

  @Override
  public Response deleteResource(String arg0) {
    try {
      final AnalyticsQuery analyticsQuery = getAnalyticsQuery(arg0, false);
      if (analyticsQuery != null) {
        OBDal.getInstance().remove(analyticsQuery);
      }
      return Response.ok().build();
    } catch (Throwable t) {
      return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }
  }

  @Override
  public List<IRepositoryObject> getRepository(String path, String type) {
    final OBQuery<AnalyticsQuery> analyticsQueries = OBDal.getInstance().createQuery(
        AnalyticsQuery.class, "order by name");
    final List<IRepositoryObject> result = new ArrayList<IRepositoryObject>();
    for (AnalyticsQuery analyticsQuery : analyticsQueries.list()) {
      result.add(new OBRepositoryObject(analyticsQuery.getId(), analyticsQuery.getName()));
    }
    return result;
  }

  @Override
  public Response getResource(String arg0) {
    try {
      final AnalyticsQuery analyticsQuery = getAnalyticsQuery(arg0, false);
      if (analyticsQuery == null) {
        return Response.status(Status.NOT_FOUND).build();
      }
      byte[] doc = analyticsQuery.getQuery().getBytes("UTF-8");
      return Response.ok(doc, MediaType.TEXT_PLAIN).header("content-length", doc.length).build();
    } catch (Throwable t) {
      return Response.serverError().status(500).build();
    }
  }

  @Override
  public Response saveResource(String name, String query) {
    try {
      final AnalyticsQuery analyticsQuery = getAnalyticsQuery(name, true);
      analyticsQuery.setQuery(query);
      OBDal.getInstance().save(analyticsQuery);
      return Response.ok().build();
    } catch (Throwable t) {
      return Response.serverError().build();
    }
  }

  private AnalyticsQuery getAnalyticsQuery(String name, boolean createNew) {
    final OBQuery<AnalyticsQuery> analyticsQueries = OBDal.getInstance().createQuery(
        AnalyticsQuery.class, "name=:name");
    analyticsQueries.setNamedParameter("name", name);
    AnalyticsQuery analyticsQuery = analyticsQueries.uniqueResult();
    if (analyticsQuery == null) {
      analyticsQuery = OBProvider.getInstance().get(AnalyticsQuery.class);
      analyticsQuery.setName(name);
    }
    return analyticsQuery;
  }

  private static class OBRepositoryObject implements IRepositoryObject {

    private String id;
    private String name;

    OBRepositoryObject(String id, String name) {
      this.id = id;
      this.name = name;
    }

    @Override
    public String getId() {
      return id;
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public Type getType() {
      return Type.FILE;
    }

  }

}