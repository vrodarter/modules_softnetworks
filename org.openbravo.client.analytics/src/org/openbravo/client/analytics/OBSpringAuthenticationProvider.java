/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.ArrayList;
import java.util.Collection;

import org.openbravo.dal.core.OBContext;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Checks that the user authenticated by Spring is the user logged in in Openbravo.
 * 
 * @author mtaal
 */

public class OBSpringAuthenticationProvider implements AuthenticationProvider {

  public static UsernamePasswordAuthenticationToken createAuthentication() {
    final UserDetails user = createUserDetails();
    if (user == null) {
      return null;
    }

    return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
  }

  private static UserDetails createUserDetails() {
    OBContext.setAdminMode(true);
    try {
      if (OBContext.getOBContext() == null || !OBContext.getOBContext().isInitialized()
          || OBContext.getOBContext().isPortalRole()
          || OBContext.getOBContext().getUser().isLocked()
          || !OBContext.getOBContext().getUser().isActive()) {
        return null;
      }
      final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
      authorities.add(new GrantedAuthorityImpl(OBContext.getOBContext().getRole().getName()));
      if (OBContext.getOBContext().isInAdministratorMode()
          || OBContext.getOBContext().isAdminContext()
          || OBContext.getOBContext().getRole().isClientAdmin()) {
        authorities.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
      } else {
        authorities.add(new GrantedAuthorityImpl("ROLE_USER"));
      }
      return new org.springframework.security.core.userdetails.User(OBContext.getOBContext()
          .getUser().getUsername(), "", true, true, true, true, authorities);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @Override
  public Authentication authenticate(Authentication auth) throws AuthenticationException {
    if (OBContext.getOBContext() == null || !OBContext.getOBContext().isInitialized()
        || OBContext.getOBContext().isPortalRole() || OBContext.getOBContext().getUser().isLocked()
        || !OBContext.getOBContext().getUser().isActive()) {
      throw new InsufficientAuthenticationException("Invalid account");
    }

    if ("admin".equals(auth.getName())
        || OBContext.getOBContext().getUser().getUsername().equals(auth.getName())) {
      return createAuthentication();
    }
    throw new BadCredentialsException("Invalid user "
        + OBContext.getOBContext().getUser().getUsername());
  }

  @Override
  public boolean supports(Class<? extends Object> arg0) {
    return true;
  }

}