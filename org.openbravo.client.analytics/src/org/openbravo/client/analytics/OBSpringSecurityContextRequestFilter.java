/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.dal.core.ThreadHandler;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

/**
 * Makes sure that the user is shown as authenticated in Spring also. Set the
 * {@link SecurityContextHolder} with an authenticated user if the user is authenticated within
 * Openbravo also.
 * 
 * @author mtaal
 */

public class OBSpringSecurityContextRequestFilter implements Filter {

  public void init(FilterConfig fConfig) throws ServletException {
  }

  public void destroy() {
  }

  public void doFilter(final ServletRequest request, final ServletResponse response,
      final FilterChain chain) throws IOException, ServletException {
    final ThreadHandler dth = new ThreadHandler() {

      @Override
      public void doBefore() {
        attemptAuthentication((HttpServletRequest) request, (HttpServletResponse) response);
      }

      private Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse resp) {
        UsernamePasswordAuthenticationToken auth = OBSpringAuthenticationProvider
            .createAuthentication();
        if (auth != null && req.getSession(false) != null) {
          // Authenticate the user
          auth.setDetails(new WebAuthenticationDetails((HttpServletRequest) request));
          SecurityContextHolder.getContext().setAuthentication(auth);
          req.getSession().setAttribute(
              HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
              SecurityContextHolder.getContext());
        }
        return auth;
      }

      @Override
      protected void doAction() throws Exception {
        chain.doFilter(request, response);
      }

      @Override
      public void doFinal(boolean errorOccured) {
      }
    };

    dth.run();
  }

}