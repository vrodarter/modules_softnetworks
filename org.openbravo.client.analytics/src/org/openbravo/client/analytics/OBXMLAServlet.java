/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mondrian.olap.MondrianServer;
import mondrian.xmla.XmlaHandler;
import mondrian.xmla.XmlaHandler.ConnectionFactory;
import mondrian.xmla.impl.DefaultXmlaServlet;

import org.olap4j.OlapConnection;
import org.openbravo.authentication.AuthenticationException;
import org.openbravo.authentication.AuthenticationManager;
import org.openbravo.dal.core.OBContext;
import org.openbravo.service.web.UserContextCache;

/**
 * Extends the Mondrian XMLA servlet adding security and connecting to the correct Mondrian server.
 * 
 * @author mtaal
 */

public class OBXMLAServlet extends DefaultXmlaServlet {
  // private static final Logger log = Logger.getLogger(OBXMLAServlet.class);

  public static final String LOGIN_PARAM = "l";
  public static final String PASSWORD_PARAM = "p";

  private static final long serialVersionUID = 1L;

  protected SecureConnectionFactory server;

  @Override
  protected XmlaHandler.ConnectionFactory createConnectionFactory(ServletConfig servletConfig)
      throws ServletException {
    if (server == null) {
      server = new SecureConnectionFactory();
    }
    return server;
  }

  @Override
  public void destroy() {
    super.destroy();
    if (server != null) {
      MondrianProvider.getInstance().getMondrianServer().shutdown();
      server = null;
    }
  }

  @Override
  protected final void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // note: also checks max number of ws calls
    // https://issues.openbravo.com/view.php?id=0025176
    AuthenticationManager authManager = AuthenticationManager.getAuthenticationManager(this);

    String userId = null;
    try {
      userId = authManager.webServiceAuthenticate(request);
    } catch (AuthenticationException e) {
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      response.setContentType("text/plain;charset=UTF-8");
      final Writer w = response.getWriter();
      w.write(e.getMessage());
      w.close();
      return;
    }

    if (userId != null) {

      // export the schema
      if (request.getRequestURI().endsWith("/schema")) {
        response.setContentType("text/xml;charset=UTF-8");
        final Writer w = response.getWriter();
        w.write(MondrianSchemaHandler.getInstance().getSchema());
        w.close();
        return;
      }

      OBContext.setOBContext(UserContextCache.getInstance().getCreateOBContext(userId));
      OBContext.setOBContextInSession(request, OBContext.getOBContext());

      super.service(request, response);
    } else {
      // not logged in
      if (!"false".equals(request.getParameter("auth"))) {
        response.setHeader("WWW-Authenticate", "Basic realm=\"Openbravo\"");
      }
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
  }

  private class SecureConnectionFactory implements ConnectionFactory {
    private MondrianServer getServer() {
      return MondrianProvider.getInstance().getMondrianServer();
    }

    public OlapConnection getConnection(String catalog, String schema, String roleName,
        Properties props) throws SQLException {
      return MondrianProvider.getInstance().getMondrianServer().getConnection(null, null, null);
    }

    public Map<String, Object> getPreConfiguredDiscoverDatasourcesResponse() {
      return ((ConnectionFactory) getServer()).getPreConfiguredDiscoverDatasourcesResponse();
    }
  }
}