/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;

public class OpenStoredViews extends BaseProcessActionHandler {

  private static final Logger log = Logger.getLogger(OpenStoredViews.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    OBContext.setAdminMode(true);
    try {

      JSONObject request = new JSONObject(content);
      JSONObject params = request.getJSONObject("_params");

      // Retrieve parameters and create Objects for them
      // Organization, AcctSchema and View Name
      String report = params.getString("report");

      OBCriteria<AnalyticsQuery> obc = OBDal.getInstance().createCriteria(AnalyticsQuery.class);
      obc.add(Restrictions.eq(AnalyticsQuery.PROPERTY_ID, report));
      obc.add(Restrictions.eq(AnalyticsQuery.PROPERTY_ACTIVE, true));
      obc.add(Restrictions.eq(AnalyticsQuery.PROPERTY_PUBLISH, true));
      AnalyticsQuery query = (AnalyticsQuery) obc.uniqueResult();

      JSONObject result = new JSONObject();

      // Open new Tab when ending the Process
      JSONArray actions = new JSONArray();

      JSONObject recordInfo = new JSONObject();
      recordInfo.put("query", query.getQuery());
      recordInfo.put("tabName", query.getName());

      JSONObject openTabAction = new JSONObject();
      openTabAction.put("openSaikuView", recordInfo);

      actions.put(openTabAction);
      result.put("responseActions", actions);
      result.put("retryExecution", true);

      return result;
    } catch (JSONException e) {
      log.error("Error in process", e);
      return new JSONObject();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

}
