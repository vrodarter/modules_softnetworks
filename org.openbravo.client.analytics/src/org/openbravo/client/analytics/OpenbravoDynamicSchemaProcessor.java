/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import mondrian.olap.Util.PropertyList;
import mondrian.spi.DynamicSchemaProcessor;

/**
 * Generates an OLAP schema from the Openbravo Application Dictionary.
 * 
 * @author mtaal
 */
public class OpenbravoDynamicSchemaProcessor implements DynamicSchemaProcessor {

  @Override
  public synchronized String processSchema(String catalog, PropertyList propertyList)
      throws Exception {

    // MondrianProvider.getInstance().checkAccessToCatalog(catalog);
    return MondrianSchemaHandler.getInstance().getSchema();
  }
}
