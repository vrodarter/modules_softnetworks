/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.inject.spi.Bean;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.system.Client;

public class ProcessGenerateCubesFromTemplate extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(ProcessGenerateCubesFromTemplate.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    OBContext.setAdminMode(false);
    try {

      // Retrieve Parameters
      JSONObject request = new JSONObject(content);
      JSONObject params = (JSONObject) request.get("_params");
      JSONObject result = new JSONObject();
      JSONObject msg = new JSONObject();
      String errorMsg = "";
      JSONArray clientsArray = params.getJSONArray("AD_Client_ID");
      boolean delete = (Boolean) params.get("delete");
      String templateCubeId = (String) request.get("Obanaly_Cube_Definition_ID");
      CubeDefinition cubeTemplate = OBDal.getInstance().get(CubeDefinition.class, templateCubeId);

      if (cubeTemplate.getModule().isInDevelopment().equals(false)) {
        errorMsg = OBMessageUtils.messageBD("OBANALY_ModuleNotInDevelopment");
      } else {

        // Delete Previous Cubes
        if (delete) {
          for (int i = 0; i < clientsArray.length(); i++) {
            String strClientId = clientsArray.getString(i);
            Client client = OBDal.getInstance().get(Client.class, strClientId);
            deletePreviousCubes(cubeTemplate, client);
          }
        }

        // Retrieve Classes that extends GenerateCubesFromTemplate
        Set<Bean<?>> beansSet = WeldUtils.getStaticInstanceBeanManager().getBeans(
            GenerateCubesFromTemplate.class);
        List<Bean<?>> beansList = new ArrayList<Bean<?>>();
        beansList.addAll(beansSet);

        // Generate one Cube per Client based on the Template
        for (int i = 0; i < clientsArray.length(); i++) {
          String strClientId = clientsArray.getString(i);
          Client client = OBDal.getInstance().get(Client.class, strClientId);
          CubeDefinition cube = generateCube(cubeTemplate, client);

          // Call the Classes that extends GenerateCubesFromTemplate to realize modifications
          for (Bean<?> abstractBean : beansList) {
            GenerateCubesFromTemplate cubeHook = (GenerateCubesFromTemplate) WeldUtils
                .getStaticInstanceBeanManager().getReference(abstractBean,
                    GenerateCubesFromTemplate.class,
                    WeldUtils.getStaticInstanceBeanManager().createCreationalContext(abstractBean));
            // If the Class is going to modify the Cube generated with this Template
            if (cubeHook.getCubeTemplatesToGenerate() != null
                && cubeHook.getCubeTemplatesToGenerate().contains(cubeTemplate.getId())) {
              try {
                cubeHook.processCube(request, params, cube, cubeTemplate, client);
              } catch (OBException e) {
                log.error("Error in  ProcessGenerateCubesFromTemplate: ", e);
                errorMsg = errorMsg + e.getLocalizedMessage() + "\n";
              }
            }
            OBDal.getInstance().flush();
          }
        }
      }

      // Return message
      if ("".equals(errorMsg)) {
        msg.put("msgType", "success");
        msg.put("msgTitle", "Success");
        msg.put("msgText", OBMessageUtils.messageBD("OBANALY_CubesGeneratedFromTemplate"));
      } else {
        msg.put("msgType", "error");
        msg.put("msgTitle", "Error");
        msg.put("msgText", OBMessageUtils.messageBD("OBANALY_ErrorWhenGeneratingCubesFromTemplate")
            + errorMsg);
      }
      JSONArray actions = new JSONArray();
      JSONObject msgAction = new JSONObject();
      msgAction.put("showMsgInProcessView", msg);
      actions.put(msgAction);
      result.put("responseActions", actions);
      return result;
    } catch (JSONException e) {
      log.error("Error in process", e);
      return null;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Delete Cubes generated from the Template for a Client
   */
  private void deletePreviousCubes(CubeDefinition cubeTemplate, Client client) {
    OBCriteria<CubeDefinition> obc = OBDal.getInstance().createCriteria(CubeDefinition.class);
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATECUBE, cubeTemplate));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATE, false));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_CLIENT, client));
    obc.setFilterOnReadableClients(false);

    for (CubeDefinition cubeDefinition : obc.list()) {
      for (CubeReport cubeReport : cubeDefinition.getOBANALYCubeReportList()) {
        for (CubeReportDimension cubeReportDimension : cubeReport.getOBANALYCubeRepDimList()) {
          for (CubeReportDimensionElement cubeReportDimensionElement : cubeReportDimension
              .getOBANALYCubeRepDimElemList()) {
            OBDal.getInstance().remove(cubeReportDimensionElement);
          }
          cubeReportDimension.getOBANALYCubeRepDimElemList().clear();
          OBDal.getInstance().remove(cubeReportDimension);
        }
        cubeReport.getOBANALYCubeRepDimList().clear();
        OBDal.getInstance().remove(cubeReport);
      }
      cubeDefinition.getOBANALYCubeReportList().clear();

      for (CubeDimension cubeDimension : cubeDefinition.getOBANALYCubeDimensionList()) {
        for (CubeLevel cubeLevel : cubeDimension.getOBANALYCubeLevelList()) {
          OBDal.getInstance().remove(cubeLevel);
        }
        cubeDimension.getOBANALYCubeLevelList().clear();
        OBDal.getInstance().remove(cubeDimension);
      }
      cubeDefinition.getOBANALYCubeDimensionList().clear();
      OBDal.getInstance().remove(cubeDefinition);
    }
    OBDal.getInstance().flush();
  }

  /**
   * Generate a Cube based on the Template for the Client
   */
  private CubeDefinition generateCube(CubeDefinition cubeTemplate, Client client) {
    if (cubeAlreadyExists(client, cubeTemplate)) {
      return null;
    }
    CubeDefinition copy = (CubeDefinition) DalUtil.copy(cubeTemplate, true);
    copy.setName(copy.getName().replace(" Template", "") + " - "
        + client.getName().replace("/", "_"));
    copy.setDescription("");
    copy.setTemplate(false);
    if (copy.getSqlfilter() != null) {
      copy.setSqlfilter(copy.getSqlfilter().replace("@ad_client_id@", "'" + client.getId() + "'"));
    }
    copy.setClient(client);
    copy.setTemplateCube(cubeTemplate);
    OBDal.getInstance().save(copy);
    for (CubeDimension dimension : copy.getOBANALYCubeDimensionList()) {
      for (CubeLevel level : dimension.getOBANALYCubeLevelList()) {
        level.setClient(client);
        OBDal.getInstance().save(level);
      }
      dimension.setClient(client);
      if (dimension.getSqlfilter() != null) {
        dimension.setSqlfilter(dimension.getSqlfilter().replace("@ad_client_id@",
            "'" + client.getId() + "'"));
      }
      OBDal.getInstance().save(dimension);
    }

    for (CubeReport report : copy.getOBANALYCubeReportList()) {
      for (CubeReportDimension reportDimension : report.getOBANALYCubeRepDimList()) {
        for (CubeReportDimensionElement element : reportDimension.getOBANALYCubeRepDimElemList()) {
          element.setClient(client);
          OBDal.getInstance().save(element);
        }
        reportDimension.setClient(client);
        OBDal.getInstance().save(reportDimension);
      }
      report.setClient(client);
      OBDal.getInstance().save(report);
    }
    OBDal.getInstance().flush();
    return copy;
  }

  /**
   * Returns true if it exists at least one Cube that has been generated based on this Template for
   * the same Client.
   */
  private boolean cubeAlreadyExists(Client client, CubeDefinition cubeTemplate) {
    OBCriteria<CubeDefinition> obc = OBDal.getInstance().createCriteria(CubeDefinition.class);
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATECUBE, cubeTemplate));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_CLIENT, client));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATE, false));
    obc.setFilterOnReadableClients(false);
    obc.setFilterOnReadableOrganization(false);
    obc.setFilterOnActive(false);
    return obc.count() != 0;
  }

}
