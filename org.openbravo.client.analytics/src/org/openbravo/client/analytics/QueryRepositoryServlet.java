/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.service.json.JsonConstants;

/**
 * Stores saiku queries in the database.
 * 
 * @author mtaal
 */
public class QueryRepositoryServlet extends HttpSecureAppServlet {

  private static final long serialVersionUID = 1L;

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    doGet(request, response);
  }

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {

    try {
      if (null != request.getParameter("file") && null == request.getParameter("content")) {
        final OBQuery<AnalyticsQuery> analyticsQueries = OBDal.getInstance().createQuery(
            AnalyticsQuery.class, "name=:name");
        analyticsQueries.setNamedParameter("name", request.getParameter("file"));
        final List<AnalyticsQuery> list = analyticsQueries.list();
        if (!list.isEmpty()) {
          response.getWriter().write(list.get(0).getQuery());
        }
      } else if (null != request.getParameter("type")) {
        final JSONArray result = new JSONArray(); // (parameterMap, content);
        final OBQuery<AnalyticsQuery> analyticsQueries = OBDal.getInstance().createQuery(
            AnalyticsQuery.class, "");
        int i = 0;
        for (AnalyticsQuery analyticsQuery : analyticsQueries.list()) {

          final JSONObject fileObject = new JSONObject();
          fileObject.put("type", "FILE");
          fileObject.put("name", analyticsQuery.getName());
          fileObject.put("id", analyticsQuery.getName());
          fileObject.put("path", analyticsQuery.getName());
          fileObject.put("fileType", "saiku");
          result.put(i++, fileObject);
        }
        response.setContentType(JsonConstants.JSON_CONTENT_TYPE);
        response.setHeader("Content-Type", JsonConstants.JSON_CONTENT_TYPE);
        response.getWriter().write(result.toString());
      } else {
        final OBQuery<AnalyticsQuery> analyticsQueries = OBDal.getInstance().createQuery(
            AnalyticsQuery.class, "name=:name");
        analyticsQueries.setNamedParameter("name", request.getParameter("name"));
        final List<AnalyticsQuery> list = analyticsQueries.list();
        AnalyticsQuery analyticsQuery;
        if (!list.isEmpty()) {
          analyticsQuery = list.get(0);
        } else {
          analyticsQuery = OBProvider.getInstance().get(AnalyticsQuery.class);
          analyticsQuery.setName((String) request.getParameter("name"));
        }
        analyticsQuery.setOrganization(OBDal.getInstance().get(Organization.class, "0"));
        analyticsQuery.setQuery(request.getParameter("content"));
        OBDal.getInstance().save(analyticsQuery);
      }
    } catch (final Exception e) {
      throw new OBException(e);
    }
  }
}
