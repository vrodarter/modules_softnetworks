/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import org.openbravo.client.kernel.BaseTemplateComponent;
import org.openbravo.client.kernel.Template;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;

/**
 * Provides the settings js script which sets up the application data for saiku.
 * 
 * @author mtaal
 */
public class SaikuSettingsComponent extends BaseTemplateComponent {

  public static final String COMPONENT_ID = "SaikuSettings";

  private static final String TEMPLATE_ID = "468E1C56AD7E40AA8DB9805C82397572";

  @Override
  protected Template getComponentTemplate() {
    return OBDal.getInstance().get(Template.class, TEMPLATE_ID);
  }

  public String getWebApp() {
    String contextUrl = getContextUrl();
    if (contextUrl.endsWith("/")) {
      contextUrl = contextUrl.substring(0, contextUrl.length() - 1);
    }
    return contextUrl;
  }

  public String getUserName() {
    return OBContext.getOBContext().getUser().getUsername();
  }
}
