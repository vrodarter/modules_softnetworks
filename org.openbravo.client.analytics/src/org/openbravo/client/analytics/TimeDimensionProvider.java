/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.client.analytics;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.module.ModuleDBPrefix;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;

/**
 * Reads the model for properties refering to the {@link TimeDimension} entity.
 * 
 * @author mtaal
 */
public class TimeDimensionProvider {
  // start from 1990 until 60 years after 1970
  private static final long ONE_DAY = 24l * 3600l * 1000l;
  private static final long IGNORE_BEFORE_MILLIS = 20l * 365l * ONE_DAY;
  private static final long IGNORE_AFTER_MILLIS = 60l * 365l * ONE_DAY;
  private static final Logger log = Logger.getLogger(TimeDimensionProvider.class);
  private static TimeDimensionProvider instance = new TimeDimensionProvider();
  public static final Entity TIMEDIMENSIONENTITY = ModelProvider.getInstance().getEntity(
      TimeDimension.ENTITY_NAME);

  public static TimeDimensionProvider getInstance() {
    return instance;
  }

  public static void setInstance(TimeDimensionProvider instance) {
    TimeDimensionProvider.instance = instance;
  }

  private List<TimeDimensionEntry> entries = null;
  private List<String> dbPrefixes = new ArrayList<String>();
  private Map<Long, TimeDimension> cachedTimeDimensions = null;

  public void createTimeDimensionEntries(Date from, Date to) {
    // create entries for all dates from/to
  }

  public synchronized int setTimeDimensionReferences(boolean reset) {

    int totalUpdateCnt = 0;
    OBContext.setAdminMode(false);
    try {

      // do some default stuff
      setDefaultDataInTimeDimension();

      // so after setting the time dimension reference the original date may change
      for (Entity entity : getTimeDimensionEntities()) {
        if (entity.isView()) {
          continue;
        }

        log.debug("Updating time values for entity " + entity.getName());

        final List<TimeDimensionEntry> timeDimensionEntries = getTimeDimensionEntries(entity);

        for (TimeDimensionEntry entry : timeDimensionEntries) {
          int updateQryCnt = 0;
          final StringBuilder baseWhere = new StringBuilder();
          final StringBuilder updateWhere = new StringBuilder();
          final StringBuilder groupByWhere = new StringBuilder();
          if (!reset) {
            baseWhere.append(entry.getOriginalTimeProperty().getColumnName() + " is not null and ");
            baseWhere.append(entry.getTimeDimensionProperty().getColumnName() + " is null");
            updateWhere.append(" and (" + baseWhere + ")");
            groupByWhere.append(" where " + baseWhere);
          }

          final PreparedStatement updateStmt = OBDal.getInstance().getConnection()
              .prepareStatement(entry.getEntityUpdateSqlStatement() + updateWhere);
          final Statement groupByStmt = OBDal.getInstance().getConnection().createStatement();
          try {
            final String groupByQry = "select " + entry.getOriginalTimeProperty().getColumnName()
                + " from " + entity.getTableName() + groupByWhere + " group by "
                + entry.getOriginalTimeProperty().getColumnName();

            log.debug("Executing " + groupByQry);

            final ResultSet rs = groupByStmt.executeQuery(groupByQry);
            rs.setFetchDirection(ResultSet.FETCH_FORWARD);
            rs.setFetchSize(1000);
            int cnt = 0;
            final List<java.sql.Date> dts = new ArrayList<java.sql.Date>();
            while (rs.next()) {
              final java.sql.Date dt = rs.getDate(1);
              dts.add(dt);
            }
            rs.close();
            groupByStmt.close();
            for (java.sql.Date dt : dts) {
              final TimeDimension td = getTimeDimension(dt);
              if (td == null) {
                continue;
              }
              updateStmt.setString(1, td.getId());
              updateStmt.setDate(2, new java.sql.Date(dt.getTime()));
              cnt += updateStmt.executeUpdate();
              totalUpdateCnt += cnt;
              updateQryCnt++;
              if ((updateQryCnt % 100) == 0) {
                log.debug("Updated " + cnt + " time values with " + updateQryCnt
                    + " update queries ");
              }
            }
            log.debug("Done Executing " + groupByQry + " " + totalUpdateCnt);
          } finally {
            if (!updateStmt.isClosed()) {
              updateStmt.close();
            }
            if (!groupByStmt.isClosed()) {
              groupByStmt.close();
            }
          }
        }
        OBDal.getInstance().commitAndClose();
      }
    } catch (Exception e) {
      throw new OBException(e);
    } finally {
      OBContext.restorePreviousMode();
    }
    return totalUpdateCnt;
  }

  private void setDefaultDataInTimeDimension() {
    OBDal.getInstance().flush();
    final Query qry = OBDal
        .getInstance()
        .getSession()
        .createQuery(
            "select t from " + TimeDimension.ENTITY_NAME + " t where t."
                + TimeDimension.PROPERTY_DATEVALUE + "=null");
    final ScrollableResults scroller = qry.scroll();
    int cnt = 0;
    while (scroller.next()) {
      final TimeDimension td = (TimeDimension) scroller.get(0);
      td.setDateValue("" + td.getYyyymmdd());
      if ((cnt++ % 1000) == 0) {
        OBDal.getInstance().flush();
        OBDal.getInstance().getSession().clear();
      }
    }
    OBDal.getInstance().commitAndClose();
  }

  public TimeDimension getTimeDimension(Date dt) {
    if (dt == null) {
      return null;
    }
    // ignore these
    if (dt.getTime() < IGNORE_BEFORE_MILLIS || dt.getTime() > IGNORE_AFTER_MILLIS) {
      return null;
    }

    final Calendar calendar = Calendar.getInstance();
    calendar.setTime(dt);
    final TimeDimension td = getCachedTimeDimension(calendar);
    if (td == null) {
      createRange(dt);
      buildCache();
      return getTimeDimension(dt);
    }
    return td;
  }

  private synchronized TimeDimension getCachedTimeDimension(Calendar calendar) {
    final long value = toLong(calendar);
    if (cachedTimeDimensions == null) {
      buildCache();
    }
    return cachedTimeDimensions.get(value);
  }

  private synchronized void buildCache() {
    cachedTimeDimensions = getAllTimeDimensions();
  }

  private Map<Long, TimeDimension> getAllTimeDimensions() {
    final Map<Long, TimeDimension> allValues = new HashMap<Long, TimeDimension>();
    OBDal.getInstance().flush();
    final Query qry = OBDal.getInstance().getSession()
        .createQuery("from " + TimeDimension.ENTITY_NAME);
    for (Object o : qry.list()) {
      final TimeDimension td = (TimeDimension) o;
      allValues.put(td.getYyyymmdd(), td);
    }
    return allValues;
  }

  private synchronized void createRange(Date dt) {
    final Calendar cFrom = Calendar.getInstance();
    cFrom.setTime(dt);
    cFrom.add(Calendar.DAY_OF_MONTH, -3000);
    final Calendar cTo = Calendar.getInstance();
    cTo.setTime(dt);
    cTo.add(Calendar.DAY_OF_MONTH, 3000);
    createRange(cFrom.getTime(), cTo.getTime());
  }

  private void createRange(Date dateFrom, Date dateTo) {
    OBContext.setAdminMode(false);
    try {
      final TimeDimension minTimeDimension = getMinTimeDimension();
      final TimeDimension maxTimeDimension = getMaxTimeDimension();

      final Calendar calendarFrom = Calendar.getInstance();
      calendarFrom.setTime(dateFrom);
      final Calendar calendarTo = Calendar.getInstance();
      calendarTo.setTime(dateTo);

      Date localFromDate = dateFrom;
      Date localToDate = dateTo;
      // handle illegal dates
      if (calendarFrom.get(Calendar.YEAR) < 2100 && maxTimeDimension != null
          && dateFrom.getTime() > maxTimeDimension.getThetime().getTime()) {
        localFromDate = maxTimeDimension.getThetime();
      }
      if (calendarTo.get(Calendar.YEAR) > 1900 && minTimeDimension != null
          && dateTo.getTime() < minTimeDimension.getThetime().getTime()) {
        localToDate = minTimeDimension.getThetime();
      }
      final Map<Long, TimeDimension> allValues = getAllTimeDimensions();

      final Calendar calendar = Calendar.getInstance();
      calendar.setTime(localFromDate);
      while (calendar.getTimeInMillis() < localToDate.getTime()) {
        final long dtValue = toLong(calendar);
        if (!allValues.containsKey(dtValue)) {
          createTimeDimension(calendar);
        }
        calendar.add(Calendar.DAY_OF_MONTH, 1);
      }
      OBDal.getInstance().flush();
    } catch (Throwable t) {
      // clear the cache as it may have been violated
      cachedTimeDimensions = null;
      throw new OBException(t);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private long toLong(Calendar calendar) {
    final String str = calendar.get(Calendar.YEAR) + toString(1 + calendar.get(Calendar.MONTH))
        + toString(calendar.get(Calendar.DAY_OF_MONTH));
    return Long.parseLong(str);
  }

  private TimeDimension getMaxTimeDimension() {
    final Query qry = OBDal
        .getInstance()
        .getSession()
        .createQuery(
            "select e from " + TimeDimension.ENTITY_NAME + " e order by "
                + TimeDimension.PROPERTY_YYYYMMDD + " desc ");
    qry.setMaxResults(1);
    return (TimeDimension) qry.uniqueResult();
  }

  private TimeDimension getMinTimeDimension() {
    final Query qry = OBDal
        .getInstance()
        .getSession()
        .createQuery(
            "select e from " + TimeDimension.ENTITY_NAME + " e order by "
                + TimeDimension.PROPERTY_YYYYMMDD);
    qry.setMaxResults(1);
    return (TimeDimension) qry.uniqueResult();
  }

  private void createTimeDimension(Calendar calendar) {
    final TimeDimension timeDimension = OBProvider.getInstance().get(TimeDimension.class);
    timeDimension.setDAYOfMonth(new Long(calendar.get(Calendar.DAY_OF_MONTH)));
    timeDimension.setDAYOfWeek(new Long(calendar.get(Calendar.DAY_OF_WEEK)));
    int month = 1 + calendar.get(Calendar.MONTH);
    timeDimension.setMonthOfYear(new Long(month));
    final long quarter;
    if (month < 4) {
      quarter = 1;
    } else if (month < 7) {
      quarter = 2;
    } else if (month < 10) {
      quarter = 3;
    } else {
      quarter = 4;
    }
    timeDimension.setQuarter(quarter);
    timeDimension.setThetime(calendar.getTime());
    timeDimension.setTheyear(new Long(calendar.get(Calendar.YEAR)));
    timeDimension.setWeekOfYear(new Long(calendar.get(Calendar.WEEK_OF_YEAR)));
    timeDimension.setWeekOfMonth(new Long(calendar.get(Calendar.WEEK_OF_MONTH)));
    timeDimension.setYyyymmdd(toYYYYMMDD(timeDimension));
    timeDimension.setDateValue("" + timeDimension.getYyyymmdd());
    timeDimension.setClient(OBDal.getInstance().get(Client.class, "0"));
    timeDimension.setOrganization(OBDal.getInstance().get(Organization.class, "0"));
    OBDal.getInstance().save(timeDimension);
  }

  private long toYYYYMMDD(TimeDimension timeDimension) {
    return new Long(toString(timeDimension.getTheyear()) + toString(timeDimension.getMonthOfYear())
        + toString(timeDimension.getDAYOfMonth()));
  }

  private String toString(long value) {
    final String strValue = "" + value;
    if (strValue.length() < 2) {
      return "0" + strValue;
    }
    return strValue;
  }

  public List<Entity> getTimeDimensionEntities() {
    final Set<Entity> result = new HashSet<Entity>();
    for (TimeDimensionEntry entry : getEntries()) {
      result.add(entry.getTimeDimensionProperty().getEntity());
    }
    return new ArrayList<Entity>(result);
  }

  public List<TimeDimensionEntry> getTimeDimensionEntries(Entity entity) {
    final List<TimeDimensionEntry> result = new ArrayList<TimeDimensionEntry>();
    for (TimeDimensionEntry entry : getEntries()) {
      if (entry.getTimeDimensionProperty().getEntity() == entity) {
        result.add(entry);
      }
    }
    return result;
  }

  public boolean isTimeDimensionProperty(Property property) {
    return property.getTargetEntity() != null && property.getTargetEntity() == TIMEDIMENSIONENTITY;
  }

  public boolean hasTimeDimensionEntry(Property property) {
    for (TimeDimensionEntry entry : getEntries()) {
      if (entry.getTimeDimensionProperty() == property) {
        return true;
      }
    }
    return false;
  }

  public TimeDimensionEntry getTimeDimensionEntry(Property property) {
    for (TimeDimensionEntry entry : getEntries()) {
      if (entry.getOriginalTimeProperty() == property) {
        return entry;
      }
    }
    return null;
  }

  public List<TimeDimensionEntry> getEntries() {
    if (entries == null) {
      entries = computeEntries();
    }
    return entries;
  }

  private synchronized List<TimeDimensionEntry> computeEntries() {
    if (entries != null) {
      return entries;
    }

    final OBQuery<ModuleDBPrefix> prefixQry = OBDal.getInstance().createQuery(ModuleDBPrefix.class,
        "");
    prefixQry.setFilterOnReadableClients(false);
    prefixQry.setFilterOnReadableOrganization(false);
    for (ModuleDBPrefix prefix : prefixQry.list()) {
      dbPrefixes.add(prefix.getName().toLowerCase());
    }

    final List<TimeDimensionEntry> localEntries = new ArrayList<TimeDimensionEntry>();
    for (Entity entity : ModelProvider.getInstance().getModel()) {
      for (Property property : entity.getProperties()) {

        if (isTimeDimensionProperty(property)) {
          // got one
          final TimeDimensionEntry entry = new TimeDimensionEntry();
          entry.setTimeDimensionProperty(property);
          final String propertyName = property.getName();
          // only actually add the entry if there is an original property being
          // modeled
          String modeledPropertyName = getModeledPropertyName(propertyName);
          if (modeledPropertyName != null) {
            modeledPropertyName = modeledPropertyName.substring(0, 1).toLowerCase()
                + modeledPropertyName.substring(1);
            final Property originalTimeProperty = getPropertyIgnoreCase(entity, modeledPropertyName);
            if (originalTimeProperty != null) {
              entry.setOriginalTimeProperty(originalTimeProperty);
              localEntries.add(entry);
            }
          }
        }
      }
    }
    return localEntries;
  }

  private Property getPropertyIgnoreCase(Entity entity, String propertyName) {
    for (Property p : entity.getProperties()) {
      if (p.getName().equalsIgnoreCase(propertyName)) {
        return p;
      }
    }
    return null;
  }

  private String getModeledPropertyName(String tdPropertyName) {
    String localTdPropertyName = tdPropertyName.toLowerCase();
    String foundPrefix = null;
    for (String prefix : dbPrefixes) {
      if (localTdPropertyName.startsWith(prefix)
          && (foundPrefix == null || prefix.length() > foundPrefix.length())) {
        foundPrefix = prefix;
      }
    }
    if (foundPrefix != null) {
      return tdPropertyName.substring(foundPrefix.length());
    }
    return null;
  }

  public static class TimeDimensionEntry {
    private Property timeDimensionProperty;
    private Property originalTimeProperty;

    public Property getTimeDimensionProperty() {
      return timeDimensionProperty;
    }

    public void setTimeDimensionProperty(Property timeDimensionProperty) {
      this.timeDimensionProperty = timeDimensionProperty;
    }

    public Property getOriginalTimeProperty() {
      return originalTimeProperty;
    }

    public void setOriginalTimeProperty(Property originalTimeProperty) {
      this.originalTimeProperty = originalTimeProperty;
    }

    public String getEntityUpdateSqlStatement() {
      final Entity entity = timeDimensionProperty.getEntity();
      return "update " + entity.getTableName() + " set " + timeDimensionProperty.getColumnName()
          + "=? where " + originalTimeProperty.getColumnName() + "=?";
    }
  }
}
