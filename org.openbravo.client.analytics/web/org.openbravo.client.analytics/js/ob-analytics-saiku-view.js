/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

isc.defineClass("OBANALY_SaikuView", isc.HTMLPane).addProperties({

  width: '100%',
  height: '100%',
  overflow: 'visible',

  contentsType: 'page',
  contentsURL: 'web/org.openbravo.client.analytics/saiku/ob-analytics.html',

  // always only open one
  isSameTab: function (viewId, params) {
    return viewId === this.getClassName();
  },

  getBookMarkParams: function () {
    var result = {};
    result.viewId = this.getClassName();
    result.tabTitle = this.tabTitle;
    return result;
  }
});

//** {{{ showMsgInProcessView }}} **
//It shows a message in the view that invoked the process.
//Parameters:
//* {{{msgType}}}: The message type. It can be 'success', 'error', 'info' or 'warning'
//* {{{msgTitle}}}: The title of the message.
//* {{{msgText}}}: The text of the message.
OB.Utilities.Action.set('openSaikuView', function (paramObj) {
var viewDefinition = {
		tabTitle: paramObj.tabName == "null" ? 'Show Query' : paramObj.tabName,
  query: paramObj.query, showToolbars: true
};
OB.Layout.ViewManager.openView('OBANALY_ShowSaikuView', viewDefinition);
});