/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

OB.Utilities.Action.set('openSaikuReport', function(paramObj) {
  var i, queries = paramObj.queries,
      processView = paramObj._processView,
      mainLayout = processView.resultLayout,
      reportView, exportXLSButton, exportPDFButton, saveAsViewButton;

  reportView = isc.OBANALY_ShowSaikuReport.create({
    parameters: paramObj,
    queries: queries
  });
  // change to make it work with older version of the process window
  if (processView && processView.formContainerLayout) {
	    processView.formContainerLayout.setOverflow('visible');
	    processView.formContainerLayout.setHeight(1);
  }
  mainLayout.addMember(reportView);

  // add buttons to the process view toolbar
  if (false && !processView.toolBarLayout.analyticsButtonsAdded) {
    processView.toolBarLayout.analyticsButtonsAdded = true;

    exportXLSButton = isc.OBFormButton.create({
      title: OB.I18N.getLabel('OBANALY_ExportXLS'),
      realTitle: '',
      click: function() {
        console.log(reportView);
      }
    });
    processView.toolBarLayout.addLeftMembers([exportXLSButton], 0);

    exportPDFButton = isc.OBFormButton.create({
      title: OB.I18N.getLabel('OBANALY_ExportPDF'),
      realTitle: '',
      click: function() {
        console.log(reportView);
      }
    });
    processView.toolBarLayout.addLeftMembers([exportPDFButton], 0);

    saveAsViewButton = isc.OBFormButton.create({
      title: OB.I18N.getLabel('OBANALY_SaveAsView'),
      realTitle: '',
      click: function() {
        console.log(reportView);
      }
    });
    processView.toolBarLayout.addLeftMembers([saveAsViewButton], 0);
  }

});

isc.defineClass("OBANALY_ShowSaikuReport", isc.HLayout).addProperties({
  queries: [],
  width: '100%',
  height: '100%',
  layoutTopMargin: 10,

  initWidget: function() {
    var i, params;

    // create a more lean parameter object
    params = isc.clone(this.parameters);
    delete params.queries;
    delete params._processView;

    this.tabSet = isc.OBTabSet.create({
      width: '100%',
      height: '100%'
    }, OB.Styles.Personalization.TabSet);

    for (i = 0; i < this.queries.length; i++) {
      this.tabSet.addTab({
        title: this.queries[i].name,
        pane: isc.OBANALY_ShowSaikuView.create({
          query: this.queries[i].query,
          // show the side and toolbars
          targetParameters: {
            showToolbars: true,
            parameters: params
          },
          contentsURL: 'web/org.openbravo.client.analytics/saiku/ob-analytics-widget.html'
        })
      });
    }
    this.addMember(this.tabSet);
  }
});