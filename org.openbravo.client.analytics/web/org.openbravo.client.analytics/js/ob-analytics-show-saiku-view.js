/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

isc.defineClass("OBANALY_ShowSaikuView", isc.HTMLPane).addProperties({

  width: '100%',
  height: '100%',
  overflow: 'visible',
  httpMethod: 'POST',
  contentsType: 'page',
  contentsURL: 'web/org.openbravo.client.analytics/saiku/show-saiku.html',

  initWidget: function() {
    this.contentsURLParams = this.mdxParams;
    this.Super('initWidget', arguments);
  },
  
  onDraw: function() {
    var iframe = this.getIframeWindow();
    iframe.query = this.query;
    iframe.showToolbars = this.showToolbars;
    if (this.targetParameters) {
    	isc.addProperties(iframe, this.targetParameters);
    }
  },
  
  // always only open one
  isSameTab: function (viewId, params) {
    return viewId === this.getClassName();
  },

  getBookMarkParams: function () {
    var result = {};
    result.viewId = this.getClassName();
    result.tabTitle = this.tabTitle;
    return result;
  },
  
  getIframeWindow: function () {
    var container, iframes;

    container = this.getHandle();

    if (container && container.getElementsByTagName) {
      iframes = container.getElementsByTagName('iframe');
      if (iframes.length > 0) {
        return (iframes[0].contentWindow ? iframes[0].contentWindow : null);
      }
    }
    return null;
  }

});