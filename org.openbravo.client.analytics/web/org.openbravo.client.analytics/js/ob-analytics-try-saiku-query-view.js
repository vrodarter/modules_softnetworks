/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

isc.defineClass("OBANALY_TrySaikuQueryView", isc.OBBaseView).addProperties({

  labelContent: 'Label content should be passed in as a parameter',

  defaultQuery: '<?xml version="1.0" encoding="UTF-8"?>' + '<Query name="F077DC61-6EF0-6CA3-9F90-60282697AC0C" type="QM" connection="F&amp;B España, S.A. US_A_Euro" cube="[_F6488042ACD14B6A87EBF42DB13F9EFC]" catalog="F&amp;B España, S.A. US_A_Euro" schema="F&amp;B España, S.A. US_A_Euro">' + '<QueryModel>' + '<Axes>' + '<Axis location="ROWS" nonEmpty="true">' + '<Dimensions>' + '<Dimension name="accountingDate" hierarchizeMode="PRE" hierarchyConsistent="true">' + '<Inclusions>' + '<Selection dimension="accountingDate" type="level" node="[accountingDate].[Year]" operator="MEMBERS" />' + '</Inclusions>' + '<Exclusions />' + '</Dimension>' + '</Dimensions>' + '</Axis>' + '<Axis location="COLUMNS" nonEmpty="true">' + '<Dimensions>' + '<Dimension name="Measures">' + '<Inclusions>' + '<Selection dimension="Measures" type="member" node="[Measures].[debit]" operator="MEMBER" />' + '</Inclusions>' + '<Exclusions />' + '</Dimension>' + '</Dimensions>' + '</Axis>' + '<Axis location="FILTER" nonEmpty="false" />' + '</Axes>' + '</QueryModel>' + '<MDX>SELECT' + 'NON EMPTY {[Measures].[debit]} ON COLUMNS,' + 'NON EMPTY {Hierarchize({[accountingDate].[Year].Members})} ON ROWS' + 'FROM [_F6488042ACD14B6A87EBF42DB13F9EFC]</MDX>' + '<Properties>' + '<Property name="saiku.ui.render.mode" value="table" />' + '<Property name="org.saiku.query.explain" value="true" />' + '<Property name="saiku.olap.query.nonempty.columns" value="true" />' + '<Property name="saiku.olap.query.nonempty.rows" value="true" />' + '<Property name="org.saiku.connection.scenario" value="false" />' + '<Property name="saiku.olap.query.automatic_execution" value="true" />' + '<Property name="saiku.olap.query.drillthrough" value="true" />' + '<Property name="saiku.olap.query.filter" value="true" />' + '<Property name="saiku.olap.query.limit" value="true" />' + '<Property name="saiku.olap.query.nonempty" value="true" />' + '</Properties>' + '</Query>',

  width: '100%',
  height: '100%',

  align: 'center',
  defaultLayoutAlign: 'left',

  initWidget: function () {
    this.children = [this.createMainStructure()];
    this.Super("initWidget", arguments);
  },

  createMainStructure: function () {
    this.mainLayout = isc.VLayout.create({
      width: 600,
      height: 250,
      defaultLayoutAlign: 'right',
      layoutMargin: 10,
      overflow: 'auto'
    });
    this.form = isc.DynamicForm.create({
      width: 550,
      height: 175,
      fields: [{
        title: 'Query Definition - XML',
        name: 'query',
        type: "textArea",
        titleOrientation: 'top',
        value: this.defaultQuery,
        width: 550,
        height: 175
      }]
    });

    this.mainLayout.addMember(this.form);
    this.createCubeButton = isc.OBFormButton.create({
      view: this,
      title: 'Click to try Query',
      action: function () {
        var query = this.view.form.getValue('query'),
            viewDefinition = {
            tabTitle: 'Show Query',
            query: query
            };
        OB.Layout.ViewManager.openView('OBANALY_ShowSaikuView', viewDefinition);
      }
    });
    this.mainLayout.addMember(this.createCubeButton);
    return this.mainLayout;
  },

  getBookMarkParams: function () {
    var result = this.Super('getBookMarkParams', arguments);
    result.labelContent = this.labelContent;
    return result;
  }
});