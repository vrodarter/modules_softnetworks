/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

// = OB Analytics Widget =
//
// A widget which shows the contents of an analytics window.
//
isc.defineClass('OBAnalyticsWidget', isc.OBWidget).addProperties({
  contentSource: null,

  createWindowContents: function () {
    this.layout = isc.HLayout.create({
      width: '100%',
      height: '100%'
    });
    this.refresh();
    return this.layout;
  },

  // overriding maximize does not work for Portlets
  completeMaximize: function () {
    var res = this.Super('completeMaximize', arguments),
        me = this;
    this._isMaximized = true;
    this.fireOnPause('refresh', function () {
      me.refresh()
    });
    return res;
  },

  completeRestore: function () {
    var me = this;
    this.Super('completeRestore', arguments);
    this._isMaximized = false;
    this.fireOnPause('refresh', function () {
      me.refresh()
    });
  },

  refresh: function () {
    var me = this,
        params = isc.clone(this.parameters || {});
    params.id = this.dbInstanceId;

    OB.RemoteCallManager.call('org.openbravo.client.analytics.AnalyticsWidgetActionHandler', params, {}, function (rpcResponse, data, rpcRequest) {
      if (data.error) {
        me.handleError(data.error);
      } else {
        me.setAnalyticsContent(data.query);
      }
    });
  },

  handleError: function (error) {
    var messageBar = isc.OBMessageBar.create({});
    if (this.layout.getMember(0)) {
      this.layout.removeMember(this.layout.getMember(0));
    }
    messageBar = isc.OBMessageBar.create({});
    messageBar.setMessage('error', error);
    this.layout.addMember(messageBar);
  },

  setAnalyticsContent: function (query) {
    if (this.layout.getMember(0)) {
      this.layout.removeMember(this.layout.getMember(0));
    }

    // don't show anything!
    if (!query) {
      return;
    }

    this.layout.addMember(isc.OBANALY_ShowSaikuView.create({
      // use _isMaximized as maximized is sometimes set later
      targetParameters: {
        showToolbars: this._isMaximized
      },
      query: query,
      contentsURL: 'web/org.openbravo.client.analytics/saiku/ob-analytics-widget.html'
    }));
  }
});