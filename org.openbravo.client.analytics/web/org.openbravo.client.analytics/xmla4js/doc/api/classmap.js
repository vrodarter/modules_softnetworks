YAHOO.env.classMap = {
  "Xmla.Exception": "xmla",
  "Xmla": "xmla",
  "Xmla.Dataset": "xmla",
  "Xmla.Dataset.Cellset": "xmla",
  "Xmla.Dataset.Axis": "xmla",
  "Xmla.Rowset": "xmla"
};

YAHOO.env.resolveClass = function (className) {
  var a = className.split('.'),
      ns = YAHOO.env.classMap;

  for (var i = 0; i < a.length; i = i + 1) {
    if (ns[a[i]]) {
      ns = ns[a[i]];
    } else {
      return null;
    }
  }

  return ns;
};