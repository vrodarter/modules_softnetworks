/************************************************************************************
 * 2 * Copyright (C) 2012 Openbravo S.L.U. 3 * Licensed under the Openbravo Commercial License
 * version 1.0 4 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * 5
 ************************************************************************************/

package org.openbravo.finance.checkprinting;

import org.openbravo.database.ConnectionProvider;
import org.openbravo.modulescript.ModuleScript;

public class UpdateDueDateColumn extends ModuleScript {

  @Override
  // Updates new column values with their payment dates, or current date if null
  public void execute() {
    try {
      ConnectionProvider cp = getConnectionProvider();
      UpdateDueDateColumnData.updateDueDate(cp);
    } catch (Exception e) {
      handleError(e);
    }
  }
}
