/************************************************************************************ 
     2  * Copyright (C) 2012-2014 Openbravo S.L.U.
     3  * Licensed under the Openbravo Commercial License version 1.0 
     4  * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
     5  ************************************************************************************/

package org.openbravo.finance.checkprinting.erpCommon.ad_reports;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.finance.checkprinting.Checkprinting;
import org.openbravo.model.common.enterprise.DocumentTemplate;
import org.openbravo.numbertoword.erpCommon.utility.NumberToWord;

public class Rpt_Check extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  HashMap<String, String> words = new HashMap<String, String>();

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String checksId = vars.getSessionValue("CHECKPRINTING.INPOBFCHPCHECKPRINTINGID");
      if (log4j.isDebugEnabled())
        log4j.debug("+***********************: " + checksId);
      printPagePartePDF(response, vars, checksId);
    } else {
      pageError(response);
    }
  }

  private void printPagePartePDF(HttpServletResponse response, VariablesSecureApp vars,
      String checksId) throws IOException, ServletException {
    try {
      OBContext.setAdminMode();
      if (log4j.isDebugEnabled())
        log4j.debug("Output: pdf");
      String strBaseDesign = getBaseDesignPath(vars.getLanguage());
      final String strTabId = "94D91408A0454A66A933D25335060C8C";
      final int maxnum = 70;
      boolean moreThanOneDocTemplate = false;
      List<Checkprinting> checkList = getCheckList(checksId);
      DocumentTemplate checkDocTemplate = checkList.get(0).getDocumentType()
          .getDocumentTemplateList().get(0);
      for (Checkprinting check : checkList) {
        try {
          moreThanOneDocTemplate = !checkDocTemplate.equals(check.getDocumentType()
              .getDocumentTemplateList().get(0));
          if (moreThanOneDocTemplate) {
            throw new OBException(OBMessageUtils.messageBD("OBFCHP_MoreThanOneDocType"));
          }
          if ("VO".equals(check.getStatus())) {
            throw new OBException(OBMessageUtils.messageBD("OBFCHP_void"));
          }
          if (check.getPayment() == null) {
            throw new OBException(OBMessageUtils.messageBD("OBFCHP_nopayment"));
          }
          String javaclass = NumberToWord.getNumberToWord(check.getOrganization());
          String numbertoword = null;
          if (javaclass == null) {
            words.put(check.getId(), " ");
          } else {
            numbertoword = ((NumberToWord) Class.forName(javaclass).newInstance()).convert(check
                .getAmount());

            while (numbertoword.length() < maxnum) {
              numbertoword = numbertoword.concat("*");
            }
            words.put(check.getId(), numbertoword);
          }
        } catch (Exception e) {
          log4j.info(e);

          final OBError myMessage = OBMessageUtils.translateError(e.getMessage());
          vars.setMessage(strTabId, myMessage);
          printPageClosePopUpAndRefreshParent(response, vars);

        }

      }

      File templateFile = new File(strBaseDesign + checkDocTemplate.getTemplateLocation()
          + checkDocTemplate.getTemplateFilename());
      if (!templateFile.exists()) {
        throw new OBException(Utility.messageBD(this, "CreateFileError", OBContext.getOBContext()
            .getLanguage().getLanguage()));
      } else {

        HashMap<String, Object> parameters = new HashMap<String, Object>();
        response.setHeader("Content-disposition", "attach;");

        for (Checkprinting check : checkList) {
          check.setStatus("P");
          parameters.put("CHECK_IDS", checksId);
          parameters.put("ORGANIZATIONID", vars.getOrg());

          parameters.put("CHECK_AMOUNT_WORD", words);
          try {
            renderJR(vars, response, strBaseDesign + checkDocTemplate.getTemplateLocation()
                + checkDocTemplate.getTemplateFilename(), checkDocTemplate.getReportFilename(),
                "pdf", parameters, null, null, true);
          } catch (Exception e) {
            OBDal.getInstance().rollbackAndClose();
            throw new OBException(e);
          }
        }
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private List<Checkprinting> getCheckList(String checksId) {
    String modifiedChecksId = checksId.replaceAll("\\(|\\)|'", "");
    String[] arrayChecksId = modifiedChecksId.split(",");

    OBCriteria<Checkprinting> obCriteriaCheck = OBDal.getInstance().createCriteria(
        Checkprinting.class);
    obCriteriaCheck.add(Restrictions.in(Checkprinting.PROPERTY_ID, arrayChecksId));

    return obCriteriaCheck.list();
  }

}