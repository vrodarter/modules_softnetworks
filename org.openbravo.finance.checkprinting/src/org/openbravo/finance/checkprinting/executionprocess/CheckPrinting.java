/************************************************************************************ 
     2  * Copyright (C) 2012 Openbravo S.L.U. 
     3  * Licensed under the Openbravo Commercial License version 1.0 
     4  * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
     5  ************************************************************************************/

package org.openbravo.finance.checkprinting.executionprocess;

import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_PaymentExecutionProcess;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.finance.checkprinting.Checkprinting;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.PaymentRun;
import org.openbravo.model.financialmgmt.payment.PaymentRunParameter;
import org.openbravo.model.financialmgmt.payment.PaymentRunPayment;

public class CheckPrinting implements FIN_PaymentExecutionProcess {

  @Override
  public OBError execute(PaymentRun paymentRun) throws ServletException {
    long checkNumber;
    Boolean isSequence = false;

    PaymentRunParameter docTypeParameter = null;
    PaymentRunParameter checkNumberParameter = null;

    for (PaymentRunParameter prParameter : paymentRun.getFinancialMgmtPaymentRunParameterList()) {
      if (prParameter.getPaymentExecutionProcessParameter().getSearchKey().equals("C_DocType_ID")) {
        docTypeParameter = prParameter;
      }
      if (prParameter.getPaymentExecutionProcessParameter().getSearchKey().equals("CheckNumber")) {
        checkNumberParameter = prParameter;
      }
    }

    String strDocType = docTypeParameter.getValueOfTheTextParameter();
    DocumentType docType = OBDal.getInstance().get(DocumentType.class, strDocType);
    try {
      if (!"".equals(checkNumberParameter.getValueOfTheTextParameter())) {
        checkNumber = Long.parseLong(checkNumberParameter.getValueOfTheTextParameter());
        isSequence = false;
      } else {
        checkNumber = docType.getDocumentSequence().getNextAssignedNumber();
        isSequence = true;
      }
    } catch (Exception e) {
      OBError result = new OBError();
      result.setType("Error");
      result.setMessage("@APRM_NotValidNumber@");
      return result;
    }

    paymentRun.setStatus("PE");
    OBDal.getInstance().save(paymentRun);

    Boolean exist = false;
    for (PaymentRunPayment paymentRunPayment : paymentRun.getFinancialMgmtPaymentRunPaymentList()) {
      FIN_Payment payment = paymentRunPayment.getPayment();
      payment.setStatus(payment.isReceipt() ? "RPR" : "PPM");
      payment.setReferenceNo(String.valueOf(checkNumber));
      Checkprinting check = new Checkprinting();
      check.setDocumentType(docType);
      check.setCheckNumber(Long.toString(checkNumber));
      check.setCheckDate(new Date());
      check.setDueDate(payment.getPaymentDate());
      check.setPayment(payment);
      check.setOrganization(payment.getOrganization());
      if (payment.getBusinessPartner() != null) {
        check.setBusinessPartner(payment.getBusinessPartner());
      }
      check.setAmount(payment.getAmount());
      check.setStatus("NP");
      /* check if the CheckNumber exists in the ddbb */
      OBCriteria<Checkprinting> obCriteria = OBDal.getInstance()
          .createCriteria(Checkprinting.class);
      obCriteria
          .add(Restrictions.eq(Checkprinting.PROPERTY_CHECKNUMBER, Long.toString(checkNumber)));
      List<Checkprinting> bpgs = obCriteria.list();
      if (bpgs.size() >= 1) {
        exist = true;
      }
      OBDal.getInstance().save(check);

      if (isSequence == true) {
        checkNumber = docType.getDocumentSequence().getNextAssignedNumber()
            + docType.getDocumentSequence().getIncrementBy();
        docType.getDocumentSequence().setNextAssignedNumber(checkNumber);
        OBDal.getInstance().save(docType.getDocumentSequence());
        OBDal.getInstance().refresh(docType);
      } else {
        checkNumber += 1;
      }

      paymentRunPayment.setResult("S");
      OBDal.getInstance().save(payment);
      OBDal.getInstance().save(paymentRunPayment);
    }

    paymentRun.setStatus("E");
    OBError result = new OBError();
    OBDal.getInstance().save(paymentRun);
    OBDal.getInstance().flush();

    if (exist) {
      result.setType("Warning");
      result.setMessage("@OBFCHP_DUPLICATED@");
    } else {
      result.setType("Success");
      result.setMessage("@Success@");
    }
    return result;
  }
}
