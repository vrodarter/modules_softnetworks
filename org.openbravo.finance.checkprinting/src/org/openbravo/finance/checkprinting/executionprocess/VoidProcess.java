/************************************************************************************ 
 * Copyright (C) 2013 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 ************************************************************************************/
package org.openbravo.finance.checkprinting.executionprocess;

import org.openbravo.advpaymentmngt.process.FIN_AddPayment;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.finance.checkprinting.Checkprinting;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

public class VoidProcess extends DalBaseProcess {

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {
    try {
      OBError message = null;
      OBError msg = new OBError();
      final String checknumber = (String) bundle.getParams().get("checknumber");
      final String deletepayment = (String) bundle.getParams().get("deletepayment");
      final String check_printing_id = (String) bundle.getParams().get("Obfchp_Check_Printing_ID");

      Checkprinting check = OBDal.getInstance().get(Checkprinting.class, check_printing_id);
      check.setStatus("VO");
      FIN_Payment payment = check.getPayment();
      payment.setReferenceNo(checknumber);
      if (deletepayment.equals("Y")) {
        for (int i = 0; i < payment.getObfchpCheckPrintingList().size(); i++) {
          Checkprinting ck = payment.getObfchpCheckPrintingList().get(i);
          ck.setPayment(null);
          OBDal.getInstance().save(ck);
        }
        payment.getObfchpCheckPrintingList().clear();
        OBDal.getInstance().save(payment);
        message = FIN_AddPayment.processPayment(bundle.getContext().toVars(),
            bundle.getConnection(), "R", payment);
        if (message.getType().equals("Success")) {
          OBContext.setAdminMode(true);
          try {
            OBDal.getInstance().remove(payment);
          } finally {
            OBContext.restorePreviousMode();
          }
        } else {
          OBDal.getInstance().rollbackAndClose();
          bundle.setResult(message);
          return;
        }
      } else {
        Checkprinting newcheck = new Checkprinting();
        newcheck.setCheckNumber(checknumber);
        newcheck.setCheckDate(check.getCheckDate());
        newcheck.setDueDate(check.getDueDate());
        newcheck.setPayment(payment);
        newcheck.setOrganization(check.getOrganization());
        newcheck.setDocumentType(check.getDocumentType());
        if (check.getBusinessPartner() != null) {
          newcheck.setBusinessPartner(check.getBusinessPartner());
        }
        newcheck.setAmount(check.getAmount());
        newcheck.setStatus("NP");
        OBDal.getInstance().save(newcheck);
        msg.setType("Success");
        msg.setTitle("@Success@");
      }
      OBDal.getInstance().save(check);
      bundle.setResult(msg);
    } catch (final OBException e) {
      final OBError msg = new OBError();
      msg.setType("Error");
      msg.setMessage(e.getMessage());
      msg.setTitle("@Error@");
      bundle.setResult(msg);
    }
  }
}