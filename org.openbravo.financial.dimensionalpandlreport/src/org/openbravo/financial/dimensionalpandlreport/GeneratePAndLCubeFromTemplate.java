/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.financial.dimensionalpandlreport;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.analytics.CubeDefinition;
import org.openbravo.financial.dimensionalreports.GenerateFinancialCubesFromTemplate;
import org.openbravo.model.ad.system.Client;

public class GeneratePAndLCubeFromTemplate extends GenerateFinancialCubesFromTemplate {

  private static final Logger log = Logger.getLogger(GeneratePAndLCubeFromTemplate.class);

  @Override
  public Set<String> getCubeTemplatesToGenerate() {
    Set<String> set = new HashSet<String>();
    set.add("1E3B7EA7D12340618952D742E9B69EC2");
    return set;
  }

  @Override
  public void processCube(JSONObject request, JSONObject params, CubeDefinition cube,
      CubeDefinition cubeTemplate, Client client) throws OBException {
    super.processCube(request, params, cube, cubeTemplate, client);

  }

}
