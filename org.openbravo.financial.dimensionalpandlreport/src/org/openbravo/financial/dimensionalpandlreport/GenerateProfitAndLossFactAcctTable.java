/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.financial.dimensionalpandlreport;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.analytics.CubeReportDimensionElement;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.financial.dimensionalreports.InitializeAccountsTranslationTable;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Tree;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

public class GenerateProfitAndLossFactAcctTable extends DalBaseProcess {

  private static final Logger log = Logger.getLogger(GenerateProfitAndLossFactAcctTable.class);
  private static String DIMENSIONAL_PROFIT_AND_LOSS_TEMPLATE_ID = "1E3B7EA7D12340618952D742E9B69EC2";
  private static String DIMENSIONAL_PROFIT_AND_LOSS_PROCESS_ID = "4ACD547415AE45C6839E8EF63EA9ED08";
  private static InitializeAccountsTranslationTable generateAccountsTranslationTable;

  @Override
  protected void doExecute(ProcessBundle bundle) throws Exception {
    OBContext.setAdminMode(true);
    try {
      Client client = OBDal.getInstance().get(Client.class, bundle.getContext().getClient());
      // Initialize Account Translation Table
      generateAccountsTranslationTable = new InitializeAccountsTranslationTable();
      generateAccountsTranslationTable.initializeTranslateTable(client);
      populatePAndLFactAcctTable(client, bundle.getConnection());
    } catch (Throwable t) {
      try {
        OBDal.getInstance().rollbackAndClose();
      } catch (Throwable ignore) {
      }
      throw new OBException(t);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Initialize Profit And Loss Fact Acct Table
   */
  private void populatePAndLFactAcctTable(Client client, ConnectionProvider conn)
      throws SQLException, NoConnectionAvailableException {
    // Delete all previous entries
    deleteEntries(client);

    // Get all the Accounts needed from the definition of the Cube
    List<ElementValue> pAndLAccounts = getPAndLAccounts(client);
    // Insert records filtering by Accounts grouped in groups of 1000 Accounts
    int numberOfSplits = pAndLAccounts.size() / 1000;
    for (int i = 0; i <= numberOfSplits; i++) {
      int maxLenght = i * 1000;
      if (pAndLAccounts.size() >= (i + 1) * 1000) {
        maxLenght = (i + 1) * 1000;
      } else {
        maxLenght = pAndLAccounts.size() - (i * 1000);
      }
      // Translate into an expression that can be used in SQL: ('XXXX','YYY')
      String accountsFilter = translateToString(pAndLAccounts.subList(i * 1000, maxLenght));
      // Insert records
      insert(client, conn, accountsFilter);
    }
  }

  /**
   * Delete all previous entries
   */
  private void deleteEntries(Client client) {
    final Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery("delete from " + FactAcctProfitAndLoss.ENTITY_NAME
        + " where ad_client_id = ?");
    query.setParameter(0, client.getId());
    int deletedEntities = query.executeUpdate();
    log.debug("Deleted " + deletedEntities + " FactAcct Profit And Loss entries");
  }

  /**
   * Get all Accounts needed from the Cube Definition Window
   */
  private List<ElementValue> getPAndLAccounts(Client client) {
    List<ElementValue> pAndLAccounts = new ArrayList<ElementValue>();
    StringBuffer where = new StringBuffer();
    where.append(" as e");
    where.append(" left join e.obanalyCubeRepDim dim");
    where.append(" left join dim.obanalyCubeReport rep");
    where.append(" left join rep.oBANALYCubeDefinition cube");
    where.append(" where cube.template = false");
    where.append(" and cube.templateCube.id = :templateId");
    where.append(" and rep.processDefintion.id = :processId");
    where.append(" and e.client.id = :clientId");
    where.append(" and e.obfdrElementvalue is not null");
    final OBQuery<CubeReportDimensionElement> query = OBDal.getInstance().createQuery(
        CubeReportDimensionElement.class, where.toString());
    query.setFilterOnReadableOrganization(false);
    query.setNamedParameter("templateId", DIMENSIONAL_PROFIT_AND_LOSS_TEMPLATE_ID);
    query.setNamedParameter("processId", DIMENSIONAL_PROFIT_AND_LOSS_PROCESS_ID);
    query.setNamedParameter("clientId", client.getId());

    // Retrieve the Accounts defined and their children
    for (CubeReportDimensionElement elem : query.list()) {
      OBCriteria<Tree> treeQuery = OBDal.getInstance().createCriteria(Tree.class);
      treeQuery.add(Restrictions.eq(Tree.PROPERTY_TYPEAREA, "EV"));
      treeQuery.add(Restrictions.eq(Tree.PROPERTY_CLIENT, OBContext.getOBContext()
          .getCurrentClient()));
      Tree tree = (Tree) treeQuery.uniqueResult();

      pAndLAccounts.add(elem.getObfdrElementvalue());
      pAndLAccounts.addAll(getChildren(elem.getObfdrElementvalue(), tree));
    }

    return pAndLAccounts;
  }

  /**
   * Return a list of Accounts that are the children of the Account Parameter
   */
  private List<ElementValue> getChildren(ElementValue account, Tree tree) {
    List<ElementValue> pAndLAccounts = new ArrayList<ElementValue>();
    OBCriteria<TreeNode> obc = OBDal.getInstance().createCriteria(TreeNode.class);
    obc.add(Restrictions.eq(TreeNode.PROPERTY_TREE,
        OBDal.getInstance().get(Tree.class, tree.getId())));
    obc.add(Restrictions.eq(TreeNode.PROPERTY_REPORTSET, account.getId()));
    for (TreeNode tn : obc.list()) {
      pAndLAccounts.add(OBDal.getInstance().get(ElementValue.class, tn.getNode()));
      pAndLAccounts.addAll(getChildren(OBDal.getInstance().get(ElementValue.class, tn.getNode()),
          tree));
    }
    return pAndLAccounts;
  }

  /**
   * Translate a list into an expression that can be used in an SQL filter: ('XXX','YYY')
   */
  private String translateToString(List<ElementValue> accountsList) {
    String accountsFilter = "";
    for (ElementValue account : accountsList) {
      accountsFilter = accountsFilter + "'" + account.getId() + "', ";
    }
    if (!"".equals(accountsFilter)) {
      accountsFilter = "(" + accountsFilter.substring(0, accountsFilter.length() - 2) + ")";
    }
    return accountsFilter;
  }

  /**
   * Insert records into Profit And Loss FactAcct TAble
   */
  private void insert(Client client, ConnectionProvider conn, String accountsFilter)
	      throws SQLException, NoConnectionAvailableException {
	    StringBuffer insert = new StringBuffer();
	    insert.append(" INSERT INTO obdpl_fact_acct_pl");
	    insert.append(" (");
	    insert.append("   obdpl_fact_acct_pl_id,");
	    insert.append("   ad_client_id,");
	    insert.append("   ad_org_id,");
	    insert.append("   isactive,");
	    insert.append("   created,");
	    insert.append("   createdby,");
	    insert.append("   updated,");
	    insert.append("   updatedby,");
	    insert.append("   c_acctschema_id,");
	    insert.append("   account_id,");
	    insert.append("   m_locator_id,");
	    insert.append("   amtacctdr,");
	    insert.append("   amtacctcr,");
	    insert.append("   m_product_id,");
	    insert.append("   c_bpartner_id,");
	    insert.append("   ad_orgtrx_id,");
	    insert.append("   c_locfrom_id,");
	    insert.append("   c_locto_id,");
	    insert.append("   c_salesregion_id,");
	    insert.append("   c_project_id,");
	    insert.append("   c_campaign_id,");
	    insert.append("   c_activity_id,");
	    insert.append("   user1_id,");
	    insert.append("   user2_id,");
	    insert.append("   a_asset_id,");
	    insert.append("   c_costcenter_id,");
	    insert.append("   dateacct,");
	    insert.append("   obanaly_accountingdate,");
	    insert.append("   fact_acct_group_id,");
	    insert.append("   c_period_id,");
	    insert.append("   em_clapm_element_budget_id,");
	    insert.append("   em_clapm_budget,");
	    insert.append("   em_clapm_variation");
	    insert.append(" )");
	    insert.append(" SELECT get_uuid() AS fact_acct_id,");
	    insert.append("        fact_acct.ad_client_id,");
	    insert.append("        fact_acct.ad_org_id,");
	    insert.append("        'Y',");
	    insert.append("        now(),");
	    insert.append("        '0',");
	    insert.append("        now(),");
	    insert.append("        '0',");
	    insert.append("        fact_acct.c_acctschema_id,");
	    insert.append("        COALESCE(tr.parent_id, fact_acct.account_id) AS account_id,");
	    insert.append("        fact_acct.m_locator_id,");
	    insert.append("        SUM(CASE");
	    insert.append("             WHEN COALESCE(tr.acct_sign, 'Y') = 'Y'");
	    insert.append("              AND COALESCE(tr.isdebit, 'Y') = 'Y'");
	    insert.append("               OR COALESCE(tr.acct_sign, 'Y') = 'N'");
	    insert.append("              AND COALESCE(tr.isdebit, 'Y') = 'N'");
	    insert.append("             THEN fact_acct.amtacctdr");
	    insert.append("             ELSE (fact_acct.amtacctdr * (-1))");
	    insert.append("            END) AS amtacctdr,");
	    insert.append("        SUM(CASE");
	    insert.append("             WHEN COALESCE(tr.acct_sign, 'Y') = 'Y'");
	    insert.append("              AND COALESCE(tr.isdebit, 'Y') = 'Y'");
	    insert.append("               OR COALESCE(tr.acct_sign, 'Y') = 'N'");
	    insert.append("              AND COALESCE(tr.isdebit, 'Y') = 'N'");
	    insert.append("             THEN fact_acct.amtacctcr");
	    insert.append("             ELSE (fact_acct.amtacctcr * (-1))");
	    insert.append("            END) AS amtacctcr,");
	    insert.append("        fact_acct.m_product_id,");
	    insert.append("        fact_acct.c_bpartner_id,");
	    insert.append("        fact_acct.ad_orgtrx_id,");
	    insert.append("        fact_acct.c_locfrom_id,");
	    insert.append("        fact_acct.c_locto_id,");
	    insert.append("        fact_acct.c_salesregion_id,");
	    insert.append("        fact_acct.c_project_id,");
	    insert.append("        fact_acct.c_campaign_id,");
	    insert.append("        fact_acct.c_activity_id,");
	    insert.append("        fact_acct.user1_id,");
	    insert.append("        fact_acct.user2_id,");
	    insert.append("        fact_acct.a_asset_id,");
	    insert.append("        fact_acct.c_costcenter_id,");
	    insert.append("        fact_acct.dateacct,");
	    insert.append("        fact_acct.em_obanaly_accountingdate,");
	    insert.append("        fact_acct.fact_acct_group_id,");
	    insert.append("        fact_acct.c_period_id,");
	    insert.append("        clapm_element_budget.clapm_element_budget_id,");
	    insert.append("        clapm_element_budget.budget,");
	    insert.append("        CASE WHEN clapm_element_budget.clapm_element_budget_id is null THEN 0 ELSE (amtacctdr/clapm_element_budget.budget*100)-100 END AS variation");
	    insert.append(" FROM fact_acct");
	    insert.append("   LEFT JOIN obfdr_translate_accounts tr ON tr.node_id = fact_acct.account_id");
	    insert.append("     AND tr.ad_client_id = '").append(client.getId()).append("'");
	    insert.append("   LEFT JOIN clapm_element_budget ON fact_acct.account_id =clapm_element_budget.c_elementvalue_id");
	    insert.append("     AND clapm_element_budget.c_period_id=fact_acct.c_period_id");	   
	    insert.append(" WHERE fact_acct.ad_client_id = '").append(client.getId()).append("'");
	    if (!"".equals(accountsFilter)) {
	      insert.append("   AND COALESCE(tr.parent_id, fact_acct.account_id) IN ").append(
	          accountsFilter);
	    }
	    insert.append(" GROUP BY COALESCE(tr.parent_id,fact_acct.account_id),");
	    insert.append("          fact_acct.ad_client_id,");
	    insert.append("          fact_acct.c_acctschema_id,");
	    insert.append("          fact_acct.c_bpartner_id,");
	    insert.append("          fact_acct.m_product_id,");
	    insert.append("          fact_acct.c_project_id,");
	    insert.append("          fact_acct.user1_id,");
	    insert.append("          fact_acct.c_costcenter_id,");
	    insert.append("          fact_acct.user2_id,");
	    insert.append("          fact_acct.c_activity_id,");
	    insert.append("          fact_acct.c_campaign_id,");
	    insert.append("          fact_acct.ad_org_id,");
	    insert.append("          fact_acct.em_obanaly_accountingdate,");
	    insert.append("          fact_acct.ad_orgtrx_id,");
	    insert.append("          fact_acct.c_locfrom_id,");
	    insert.append("          fact_acct.c_locto_id,");
	    insert.append("          fact_acct.c_salesregion_id,");
	    insert.append("          fact_acct.a_asset_id,");
	    insert.append("          fact_acct.m_locator_id,");
	    insert.append("          fact_acct.dateacct,");
	    insert.append("          fact_acct.fact_acct_group_id,");
	    insert.append("          fact_acct.c_period_id,");
	    insert.append("          clapm_element_budget.clapm_element_budget_id,");
	    insert.append("          amtacctdr");
	  
	    
	    int insertedRecords = 0;
	    PreparedStatement st = null;

	    st = conn.getPreparedStatement(conn.getConnection(), insert.toString());
	    insertedRecords = st.executeUpdate();

	    log.debug("Inserted into Profit And Loss FactAcct Table: " + insertedRecords);
	  }
  
 /* private void insert(Client client, ConnectionProvider conn, String accountsFilter)
      throws SQLException, NoConnectionAvailableException {
    StringBuffer insert = new StringBuffer();
    insert.append(" INSERT INTO obdpl_fact_acct_pl");
    insert.append(" (");
    insert.append("   obdpl_fact_acct_pl_id,");
    insert.append("   ad_client_id,");
    insert.append("   ad_org_id,");
    insert.append("   isactive,");
    insert.append("   created,");
    insert.append("   createdby,");
    insert.append("   updated,");
    insert.append("   updatedby,");
    insert.append("   c_acctschema_id,");
    insert.append("   account_id,");
    insert.append("   m_locator_id,");
    insert.append("   amtacctdr,");
    insert.append("   amtacctcr,");
    insert.append("   m_product_id,");
    insert.append("   c_bpartner_id,");
    insert.append("   ad_orgtrx_id,");
    insert.append("   c_locfrom_id,");
    insert.append("   c_locto_id,");
    insert.append("   c_salesregion_id,");
    insert.append("   c_project_id,");
    insert.append("   c_campaign_id,");
    insert.append("   c_activity_id,");
    insert.append("   user1_id,");
    insert.append("   user2_id,");
    insert.append("   a_asset_id,");
    insert.append("   c_costcenter_id,");
    insert.append("   dateacct,");
    insert.append("   obanaly_accountingdate,");
    insert.append("   fact_acct_group_id,");
    insert.append("   c_period_id");
    insert.append(" )");
    insert.append(" SELECT get_uuid() AS fact_acct_id,");
    insert.append("        fact_acct.ad_client_id,");
    insert.append("        fact_acct.ad_org_id,");
    insert.append("        'Y',");
    insert.append("        now(),");
    insert.append("        '0',");
    insert.append("        now(),");
    insert.append("        '0',");
    insert.append("        fact_acct.c_acctschema_id,");
    insert.append("        COALESCE(tr.parent_id, fact_acct.account_id) AS account_id,");
    insert.append("        fact_acct.m_locator_id,");
    insert.append("        SUM(CASE");
    insert.append("             WHEN COALESCE(tr.acct_sign, 'Y') = 'Y'");
    insert.append("              AND COALESCE(tr.isdebit, 'Y') = 'Y'");
    insert.append("               OR COALESCE(tr.acct_sign, 'Y') = 'N'");
    insert.append("              AND COALESCE(tr.isdebit, 'Y') = 'N'");
    insert.append("             THEN fact_acct.amtacctdr");
    insert.append("             ELSE (fact_acct.amtacctdr * (-1))");
    insert.append("            END) AS amtacctdr,");
    insert.append("        SUM(CASE");
    insert.append("             WHEN COALESCE(tr.acct_sign, 'Y') = 'Y'");
    insert.append("              AND COALESCE(tr.isdebit, 'Y') = 'Y'");
    insert.append("               OR COALESCE(tr.acct_sign, 'Y') = 'N'");
    insert.append("              AND COALESCE(tr.isdebit, 'Y') = 'N'");
    insert.append("             THEN fact_acct.amtacctcr");
    insert.append("             ELSE (fact_acct.amtacctcr * (-1))");
    insert.append("            END) AS amtacctcr,");
    insert.append("        fact_acct.m_product_id,");
    insert.append("        fact_acct.c_bpartner_id,");
    insert.append("        fact_acct.ad_orgtrx_id,");
    insert.append("        fact_acct.c_locfrom_id,");
    insert.append("        fact_acct.c_locto_id,");
    insert.append("        fact_acct.c_salesregion_id,");
    insert.append("        fact_acct.c_project_id,");
    insert.append("        fact_acct.c_campaign_id,");
    insert.append("        fact_acct.c_activity_id,");
    insert.append("        fact_acct.user1_id,");
    insert.append("        fact_acct.user2_id,");
    insert.append("        fact_acct.a_asset_id,");
    insert.append("        fact_acct.c_costcenter_id,");
    insert.append("        fact_acct.dateacct,");
    insert.append("        fact_acct.em_obanaly_accountingdate,");
    insert.append("        fact_acct.fact_acct_group_id,");
    insert.append("        fact_acct.c_period_id");
    insert.append(" FROM fact_acct");
    insert.append("   LEFT JOIN obfdr_translate_accounts tr ON tr.node_id = fact_acct.account_id");
    insert.append("     AND tr.ad_client_id = '").append(client.getId()).append("'");
    insert.append(" WHERE fact_acct.ad_client_id = '").append(client.getId()).append("'");
    if (!"".equals(accountsFilter)) {
      insert.append("   AND COALESCE(tr.parent_id, fact_acct.account_id) IN ").append(
          accountsFilter);
    }
    insert.append(" GROUP BY COALESCE(tr.parent_id,fact_acct.account_id),");
    insert.append("          fact_acct.ad_client_id,");
    insert.append("          fact_acct.c_acctschema_id,");
    insert.append("          fact_acct.c_bpartner_id,");
    insert.append("          fact_acct.m_product_id,");
    insert.append("          fact_acct.c_project_id,");
    insert.append("          fact_acct.user1_id,");
    insert.append("          fact_acct.c_costcenter_id,");
    insert.append("          fact_acct.user2_id,");
    insert.append("          fact_acct.c_activity_id,");
    insert.append("          fact_acct.c_campaign_id,");
    insert.append("          fact_acct.ad_org_id,");
    insert.append("          fact_acct.em_obanaly_accountingdate,");
    insert.append("          fact_acct.ad_orgtrx_id,");
    insert.append("          fact_acct.c_locfrom_id,");
    insert.append("          fact_acct.c_locto_id,");
    insert.append("          fact_acct.c_salesregion_id,");
    insert.append("          fact_acct.a_asset_id,");
    insert.append("          fact_acct.m_locator_id,");
    insert.append("          fact_acct.dateacct,");
    insert.append("          fact_acct.fact_acct_group_id,");
    insert.append("          fact_acct.c_period_id");

    int insertedRecords = 0;
    PreparedStatement st = null;

    st = conn.getPreparedStatement(conn.getConnection(), insert.toString());
    insertedRecords = st.executeUpdate();

    log.debug("Inserted into Profit And Loss FactAcct Table: " + insertedRecords);
  }*/
}
