/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.financial.dimensionalpandlreport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.openbravo.client.analytics.CubeDefinition;
import org.openbravo.client.analytics.CubeReportDimensionElement;
import org.openbravo.client.analytics.GenerateXmlQueryForSaiku;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.financial.dimensionalreports.DimensionalReportsUtils;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.financialmgmt.accounting.Costcenter;
import org.openbravo.model.financialmgmt.accounting.UserDimension1;
import org.openbravo.model.financialmgmt.accounting.UserDimension2;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchema;
import org.openbravo.model.financialmgmt.calendar.Period;
import org.openbravo.model.financialmgmt.calendar.Year;
import org.openbravo.model.project.Project;

public class ProfitAndLossActionHandler extends BaseProcessActionHandler {
  private static final Logger log = Logger.getLogger(ProfitAndLossActionHandler.class);
  private static DimensionalReportsUtils utils = new DimensionalReportsUtils();
  private static GenerateXmlQueryForSaiku xmlGenerator = new GenerateXmlQueryForSaiku();
  private static String DIMENSIONAL_PROFIT_AND_LOSS_TEMPLATE_ID = "1E3B7EA7D12340618952D742E9B69EC2";

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {

    JSONObject result = new JSONObject();
    try {

      JSONObject request = new JSONObject(content);
      JSONObject params = request.getJSONObject("_params");

      // Retrieve parameters and create Objects for them
      // Organization, AcctSchema and View Name
      String orgStr = params.getString("AD_Org_ID");
      Organization org = OBDal.getInstance().get(Organization.class, orgStr);
      String acctSchemaStr = params.getString("C_AcctSchema_ID");
      AcctSchema acctSchema = OBDal.getInstance().get(AcctSchema.class, acctSchemaStr);
      String viewNameStr = params.getString("ViewName");

      // Year, Dates and Periods
      String yearStr = params.getString("C_Year_ID");
      Year year = OBDal.getInstance().get(Year.class, yearStr);
      String periodFromStr = params.getString("C_PeriodFrom_ID");
      Period periodFrom = OBDal.getInstance().get(Period.class, periodFromStr);
      String periodToStr = params.getString("C_PeriodTo_ID");
      Period periodTo = OBDal.getInstance().get(Period.class, periodToStr);
      boolean useFiscalCalendar = "true".equals(params.getString("FiscalCalendar"));

      // Reference Year, Reference Dates and Reference Periods
      String refYearStr = params.getString("C_RefYear_ID");
      Year refYear = OBDal.getInstance().get(Year.class, refYearStr);
      String refPeriodFromStr = params.getString("C_PeriodFromRef_ID");
      Period refPeriodFrom = OBDal.getInstance().get(Period.class, refPeriodFromStr);
      String refPeriodToStr = params.getString("C_PeriodToRef_ID");
      Period refPeriodTo = OBDal.getInstance().get(Period.class, refPeriodToStr);

      // Dimensional Filters
      JSONArray bpGroupStrArray = params.getJSONArray("C_BP_Group_ID");
      List<Category> bpGroupList = new ArrayList<Category>();
      for (int i = 0; i < bpGroupStrArray.length(); i++) {
        String bpGroupStr = bpGroupStrArray.getString(i);
        bpGroupList.add(OBDal.getInstance().get(Category.class, bpGroupStr));
      }
      JSONArray bPartnerStrArray = params.getJSONArray("C_BPartner_ID");
      List<BusinessPartner> bPartnerList = new ArrayList<BusinessPartner>();
      for (int i = 0; i < bPartnerStrArray.length(); i++) {
        String bPartnerStr = bPartnerStrArray.getString(i);
        bPartnerList.add(OBDal.getInstance().get(BusinessPartner.class, bPartnerStr));
      }
      JSONArray productCategoryStrArray = params.getJSONArray("M_Product_Category_ID");
      List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();
      for (int i = 0; i < productCategoryStrArray.length(); i++) {
        String productCategoryStr = productCategoryStrArray.getString(i);
        productCategoryList.add(OBDal.getInstance().get(ProductCategory.class, productCategoryStr));
      }
      JSONArray productStrArray = params.getJSONArray("M_Product_ID");
      List<Product> productList = new ArrayList<Product>();
      for (int i = 0; i < productStrArray.length(); i++) {
        String productStr = productStrArray.getString(i);
        productList.add(OBDal.getInstance().get(Product.class, productStr));
      }
      JSONArray projectStrArray = params.getJSONArray("C_Project_ID");
      List<Project> projectList = new ArrayList<Project>();
      for (int i = 0; i < projectStrArray.length(); i++) {
        String projectStr = projectStrArray.getString(i);
        projectList.add(OBDal.getInstance().get(Project.class, projectStr));
      }
      JSONArray costCenterStrArray = params.getJSONArray("C_CostCenter_ID");
      List<Costcenter> costCenterList = new ArrayList<Costcenter>();
      for (int i = 0; i < costCenterStrArray.length(); i++) {
        String costCenterStr = costCenterStrArray.getString(i);
        costCenterList.add(OBDal.getInstance().get(Costcenter.class, costCenterStr));
      }
      JSONArray user1StrArray = params.getJSONArray("User1_ID");
      List<UserDimension1> user1List = new ArrayList<UserDimension1>();
      for (int i = 0; i < user1StrArray.length(); i++) {
        String user1Str = user1StrArray.getString(i);
        user1List.add(OBDal.getInstance().get(UserDimension1.class, user1Str));
      }

      JSONArray user2StrArray = params.getJSONArray("User2_ID");
      List<UserDimension2> user2List = new ArrayList<UserDimension2>();
      for (int i = 0; i < user2StrArray.length(); i++) {
        String user2Str = user2StrArray.getString(i);
        user2List.add(OBDal.getInstance().get(UserDimension2.class, user2Str));
      }

      // Dimensions
      JSONArray dimensionsListsArray = params.getJSONArray("AcctDimensions");
      JSONArray dimensionsList1Array = dimensionsListsArray.getJSONArray(0);
      JSONArray dimensionsList2Array = dimensionsListsArray.getJSONArray(1);
      List<String> columnsDimensions = new ArrayList<String>();
      List<String> rowsDimensions = new ArrayList<String>();
      Set<String> remainingDimensions = utils.getDimensionsAvailableList();
      for (int i = 0; i < dimensionsList1Array.length(); i++) {
        String dimensionsList1 = dimensionsList1Array.getString(i);
        columnsDimensions.add(dimensionsList1);
        remainingDimensions.remove(dimensionsList1);
      }
      for (int i = 0; i < dimensionsList2Array.length(); i++) {
        String dimensionsList2 = dimensionsList2Array.getString(i);
        rowsDimensions.add(dimensionsList2);
        remainingDimensions.remove(dimensionsList2);
      }

      CubeDefinition cube = utils.getCube(DIMENSIONAL_PROFIT_AND_LOSS_TEMPLATE_ID, acctSchema);

      if (cube == null) {
        JSONObject msg = new JSONObject();
        msg.put("msgType", "error");
        msg.put("msgTitle", "Error");
        msg.put("msgText", OBMessageUtils.messageBD("OBFDR_CubeNotExists"));
        JSONArray actions = new JSONArray();
        JSONObject msgAction = new JSONObject();
        msgAction.put("showMsgInProcessView", msg);
        actions.put(msgAction);
        result.put("responseActions", actions);
        result.put("retryExecution", true);
        return result;
      }

      // Initialize Xml file for Saiku
      Document doc = xmlGenerator.initializeXmlFile(
          SequenceIdData.getUUID() + SequenceIdData.getUUID(), cube.getId());

      if (doc == null) {
        JSONObject msg = new JSONObject();
        msg.put("msgType", "error");
        msg.put("msgTitle", "Error");
        msg.put("msgText", OBMessageUtils.messageBD("OBANALY_ErrorQuery"));
        JSONArray actions = new JSONArray();
        JSONObject msgAction = new JSONObject();
        msgAction.put("showMsgInProcessView", msg);
        actions.put(msgAction);
        result.put("responseActions", actions);
        result.put("retryExecution", true);
        return result;
      }

      // Add Accounts Dimension to Rows
      List<CubeReportDimensionElement> elements = utils.getCubeReportDimensionElements(cube,
          parameters.get("processId").toString(), "account");
      List<HashMap<String, String>> acctNodes = utils.getAccountNodes(elements);
      if (acctNodes.isEmpty()) {
        JSONObject msg = new JSONObject();
        msg.put("msgType", "error");
        msg.put("msgTitle", "Error");
        msg.put("msgText", OBMessageUtils.messageBD("OBDPL_NoAccountsDefined"));
        JSONArray actions = new JSONArray();
        JSONObject msgAction = new JSONObject();
        msgAction.put("showMsgInProcessView", msg);
        actions.put(msgAction);
        result.put("responseActions", actions);
        result.put("retryExecution", true);
        return result;
      }
      doc = xmlGenerator.addAdvancedDimensionToRows(doc, "account", acctNodes);

      // Add dimensions to Rows
      for (String dimension : rowsDimensions) {
        List<String> nodes = new ArrayList<String>();
        if (utils.thereIsNoFilterForDimension(dimension, org, bpGroupList, bPartnerList,
            productCategoryList, productList, projectList, costCenterList, user1List, user2List)) {
          nodes.add(utils.getNodesName(dimension));
          xmlGenerator.addDimensionToRows(doc, utils.getDimensionName(dimension), nodes);
        } else {
          nodes = utils.createNodesForFilters(dimension, nodes, org, bpGroupList, bPartnerList,
              productCategoryList, productList, projectList, costCenterList, user1List, user2List);
          xmlGenerator.addFilterToRows(doc, utils.getDimensionName(dimension), nodes);
        }
      }

      // Add Date Dimension to Columns
      List<String> nodes = new ArrayList<String>();
      if (periodFrom == null && periodTo == null && refPeriodFrom == null && refPeriodTo == null) {
        // If Dates are not filtered
        if (useFiscalCalendar) {
          nodes.add("[" + year.getId() + "]");
        } else {
          nodes.add("[" + utils.getYearFromDate(utils.getFirstDayOfYear(year)) + "]");
          if (!utils.getYearFromDate(utils.getFirstDayOfYear(year)).equals(
              utils.getYearFromDate(utils.getLastDayOfYear(year)))) {
            nodes.add("[" + utils.getYearFromDate(utils.getLastDayOfYear(year)) + "]");
          }
        }
        if (refYear != null) {
          if (useFiscalCalendar) {
            nodes.add("[" + refYear.getId() + "]");
          } else {
            nodes.add("[" + utils.getYearFromDate(utils.getFirstDayOfYear(refYear)) + "]");
            if (!utils.getYearFromDate(utils.getFirstDayOfYear(refYear)).equals(
                utils.getYearFromDate(utils.getLastDayOfYear(refYear)))) {
              nodes.add("[" + utils.getYearFromDate(utils.getLastDayOfYear(refYear)) + "]");
            }
          }
        }
        if (useFiscalCalendar) {
          xmlGenerator.addFilterToColumns(doc, "period-year", nodes);
        } else {
          xmlGenerator.addFilterToColumns(doc, "accountingDate", nodes);
        }
      } else {
        // If dates are filtered with Perid From and Period To
        String filterCondition = "";
        if (useFiscalCalendar) {
          nodes.add("[yearLevel]");
          nodes.add("[periodLevel]");
          xmlGenerator.addDimensionToColumns(doc, "period-year", nodes);
          filterCondition = utils.getPeriodFilterNodes(nodes, year, refYear, periodFrom, periodTo,
              refPeriodFrom, refPeriodTo);
        } else {
          nodes.add("[Year]");
          nodes.add("[Quarter]");
          nodes.add("[Month]");
          xmlGenerator.addDimensionToColumns(doc, "accountingDate", nodes);
          filterCondition = utils.getDateFilterNodes(nodes, year, refYear, periodFrom, periodTo,
              refPeriodFrom, refPeriodTo);
        }
        xmlGenerator.addFilterConditionToColumns(doc, filterCondition);
      }

      // Add dimensions to Columns
      for (String dimension : columnsDimensions) {
        nodes = new ArrayList<String>();
        if (utils.thereIsNoFilterForDimension(dimension, org, bpGroupList, bPartnerList,
            productCategoryList, productList, projectList, costCenterList, user1List, user2List)) {
          nodes.add(utils.getNodesName(dimension));
          xmlGenerator.addDimensionToColumns(doc, utils.getDimensionName(dimension), nodes);
        } else {
          nodes = utils.createNodesForFilters(dimension, nodes, org, bpGroupList, bPartnerList,
              productCategoryList, productList, projectList, costCenterList, user1List, user2List);
          xmlGenerator.addFilterToColumns(doc, utils.getDimensionName(dimension), nodes);
        }
      }

      // Add Measures Dimension to filters
      nodes = new ArrayList<String>();
      nodes.add("[balance]");
      xmlGenerator.addFilterToWhereClause(doc, "Measures", nodes);

      // Add additional filters
      for (String dimension : remainingDimensions) {
        nodes = new ArrayList<String>();
        if (!utils.thereIsNoFilterForDimension(dimension, org, bpGroupList, bPartnerList,
            productCategoryList, productList, projectList, costCenterList, user1List, user2List)) {
          nodes = utils.createNodesForFilters(dimension, nodes, org, bpGroupList, bPartnerList,
              productCategoryList, productList, projectList, costCenterList, user1List, user2List);
          xmlGenerator.addFilterToWhereClause(doc, utils.getDimensionName(dimension), nodes);
        }
      }

      // Finalize Xml file for Saiku
      String xmlFile = xmlGenerator.finalizeXmlFile(doc).toString();

      // Save to Analytics Query
      String errorMessage = DimensionalReportsUtils.saveDimensionReportQuery(viewNameStr, xmlFile);
      if (errorMessage != null) {
        return DimensionalReportsUtils.createErrorProcessJson(errorMessage);
      }
      JSONObject recordInfo = new JSONObject();
      recordInfo.put("query", xmlFile);
      recordInfo.put("tabName", viewNameStr);

      JSONObject openTabAction = new JSONObject();
      openTabAction.put("openSaikuView", recordInfo);

      // Open new Tab when ending the Process
      JSONArray actions = new JSONArray();
      actions.put(openTabAction);
      result.put("responseActions", actions);
      result.put("retryExecution", true);

      return result;
    } catch (JSONException e) {
      log.error("Error in process", e);
      return new JSONObject();
    }
  }
}
