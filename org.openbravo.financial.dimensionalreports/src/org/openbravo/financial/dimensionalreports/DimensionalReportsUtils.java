/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2013 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */

package org.openbravo.financial.dimensionalreports;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.inject.spi.Bean;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.client.analytics.AnalyticsQuery;
import org.openbravo.client.analytics.CubeDefinition;
import org.openbravo.client.analytics.CubeReportDimensionElement;
import org.openbravo.client.analytics.OBOlapClosureTreeHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.ad.utility.Tree;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.common.plm.ProductCategory;
import org.openbravo.model.financialmgmt.accounting.AccountingFact;
import org.openbravo.model.financialmgmt.accounting.Costcenter;
import org.openbravo.model.financialmgmt.accounting.UserDimension1;
import org.openbravo.model.financialmgmt.accounting.UserDimension2;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchema;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchemaElement;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;
import org.openbravo.model.financialmgmt.calendar.Period;
import org.openbravo.model.financialmgmt.calendar.Year;
import org.openbravo.model.project.Project;

public class DimensionalReportsUtils {

  private static final String organizationDimension = "OO";
  private static final String businessPartnerDimension = "BP";
  private static final String productDimension = "PR";
  private static final String projectDimension = "PJ";
  private static final String costCenterDimension = "CC";
  private static final String user1Dimension = "U1";
  private static final String user2Dimension = "U2";

  /** Create a JSONobject which tells the process definition view to show a message */
  public static JSONObject createErrorProcessJson(String errorLabel) throws JSONException {
    final JSONObject msg = new JSONObject();
    msg.put("msgType", "error");
    msg.put("msgTitle", "Error");
    msg.put("msgText", OBMessageUtils.messageBD(errorLabel));
    JSONArray actions = new JSONArray();
    JSONObject msgAction = new JSONObject();
    msgAction.put("showMsgInProcessView", msg);
    actions.put(msgAction);
    final JSONObject result = new JSONObject();
    result.put("responseActions", actions);
    result.put("retryExecution", true);
    return result;
  }

  /**
   * Save the dimensional query using the viewNameStr, if the parameter is not set then nothing is
   * stored. If there is already a query with this name stored, then the error label is returned and
   * nothing is saved.
   */
  public static String saveDimensionReportQuery(String viewNameStr, String xmlFile) {
    if (viewNameStr != null && viewNameStr != "" && viewNameStr != "null") {
      OBQuery<AnalyticsQuery> checkQry = OBDal.getInstance().createQuery(AnalyticsQuery.class,
          "name=:name");
      checkQry.setFilterOnActive(false);
      checkQry.setFilterOnReadableOrganization(false);
      checkQry.setNamedParameter("name", viewNameStr);
      if (checkQry.count() > 0) {
        return "OBFDR_DimensionalReportExists";
      } else {
        AnalyticsQuery query = new AnalyticsQuery();
        query.setClient(OBContext.getOBContext().getCurrentClient());
        query.setOrganization(OBDal.getInstance().get(Organization.class, "0"));
        query.setQuery(xmlFile);
        query.setName(viewNameStr);
        query.setPublish(true);
        OBDal.getInstance().save(query);
      }
    }
    return null;
  }

  /**
   * Get all the distinct accounts for which exists an entry in the factAcct for the acctSchema
   * between the given years.
   */
  public List<ElementValue> getFactAcctAccounts(Year year, Year refYear, AcctSchema acctSchema) {
    List<ElementValue> accountsList = new ArrayList<ElementValue>();

    Date lastDay = null;

    if (refYear == null || getLastDayOfYear(year).after(getLastDayOfYear(refYear))) {
      lastDay = getLastDayOfYear(year);
    } else {
      lastDay = getLastDayOfYear(refYear);
    }

    String treeId = "";
    List<AcctSchemaElement> list = acctSchema.getFinancialMgmtAcctSchemaElementList();
    for (AcctSchemaElement element : list) {
      if (element.getType().equals("AC")) {
        treeId = element.getAccountingElement().getId();
        break;
      }
    }

    StringBuffer myQuery = new StringBuffer();

    myQuery.append(" select e");
    myQuery.append(" from ").append(ElementValue.ENTITY_NAME).append(" e");
    myQuery.append(" where exists");
    myQuery.append("             (select 1");
    myQuery.append("             from ").append(AccountingFact.ENTITY_NAME).append(" f");
    myQuery.append("             where f.account.id = e.id");
    myQuery.append("             and f.client.id = e.client.id");
    myQuery.append("             and f.accountingDate <= ? )");
    myQuery.append(" and e.").append(ElementValue.PROPERTY_ACCOUNTINGELEMENT).append(".id = '")
        .append(treeId).append("'");
    myQuery.append(" order by e.searchKey asc");

    final Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery(myQuery.toString());
    query.setParameter(0, lastDay);
    accountsList = (List<ElementValue>) query.list();

    return accountsList;
  }

  public Date getFirstDayOfYear(Year year) {
    OBCriteria<Period> obc = OBDal.getInstance().createCriteria(Period.class);
    obc.add(Restrictions.eq(Period.PROPERTY_YEAR, year));
    obc.addOrder(Order.asc(Period.PROPERTY_STARTINGDATE));
    obc.setMaxResults(1);
    return ((Period) obc.uniqueResult()).getStartingDate();
  }

  public Date getLastDayOfYear(Year year) {
    OBCriteria<Period> obc = OBDal.getInstance().createCriteria(Period.class);
    obc.add(Restrictions.eq(Period.PROPERTY_YEAR, year));
    obc.addOrder(Order.desc(Period.PROPERTY_ENDINGDATE));
    obc.setMaxResults(1);
    return ((Period) obc.uniqueResult()).getEndingDate();
  }

  /**
   * Return MDX expression for filter between dates.
   */
  public String getFilterBetweenDates(List<String> nodes, Date dateFrom, Date dateTo) {
    String filterCondition = "";
    filterCondition = "[accountingDate].[Year].[Quarter].[Month].[Day].CurrentMember in {[accountingDate].["
        + getYearFromDate(dateFrom)
        + "].["
        + getQuarterFromDate(dateFrom)
        + "].["
        + getMonthFromDate(dateFrom)
        + "].["
        + getDayFromDate(dateFrom)
        + "]:[accountingDate].["
        + getYearFromDate(dateTo)
        + "].["
        + getQuarterFromDate(dateTo)
        + "].["
        + getMonthFromDate(dateTo) + "].[" + getDayFromDate(dateTo) + "]}";
    return filterCondition;
  }

  public String getPeriodFilterNodes(List<String> nodes, Year year, Year refYear,
      Period periodFrom, Period periodTo, Period refPeriodFrom, Period refPeriodTo) {
    String filterConditionYear = "";
    String filterConditionPeriod = "";

    if (refYear != null) {
      filterConditionYear = "[period-year].CurrentMember in {[period-year].[" + year.getId()
          + "]:[period-year].[" + refYear.getId() + "]} ";
    } else {
      filterConditionYear = "[period-year].CurrentMember in {[period-year].[" + year.getId()
          + "]} ";
    }

    if (refYear != null) {
      String periodsInBetweenFrom = getPeriodsInBetween(year, periodFrom, periodTo);
      String periodsInBetweenTo = getPeriodsInBetween(refYear, refPeriodFrom, refPeriodTo);
      String periodsInBetween = "";
      if (!"".equals(periodsInBetweenFrom)) {
        periodsInBetween = "".equals(periodsInBetweenTo) ? periodsInBetweenFrom
            : periodsInBetweenFrom + " , " + periodsInBetweenTo;
      } else {
        periodsInBetween = "".equals(periodsInBetweenTo) ? "" : periodsInBetweenTo;
      }
      filterConditionPeriod = " or [period-year].[year].CurrentMember in {" + periodsInBetween
          + "}";
    } else {
      filterConditionPeriod = " or [period-year].[year].CurrentMember in {"
          + getPeriodsInBetween(year, periodFrom, periodTo) + "}";
    }

    String filterCondition = filterConditionYear + filterConditionPeriod;
    return filterCondition;
  }

  private String getPeriodsInBetween(Year year, Period periodFrom, Period periodTo) {
    String periodsInBetween = "";
    OBCriteria<Period> obc = OBDal.getInstance().createCriteria(Period.class);
    obc.add(Restrictions.eq(Period.PROPERTY_YEAR, year));
    if (periodFrom != null) {
      obc.add(Restrictions.ge(Period.PROPERTY_STARTINGDATE, periodFrom.getStartingDate()));
    }
    if (periodTo != null) {
      obc.add(Restrictions.le(Period.PROPERTY_ENDINGDATE, periodTo.getEndingDate()));
    }
    obc.addOrder(Order.asc(Period.PROPERTY_STARTINGDATE));
    for (Period period : obc.list()) {
      periodsInBetween = periodsInBetween + "[period-year].[" + year.getId() + "].["
          + period.getId() + "]" + ", ";
    }
    if (!"".equals(periodsInBetween)) {
      periodsInBetween = periodsInBetween.substring(0, periodsInBetween.length() - 2);
    }
    return periodsInBetween;
  }

  public String getDateFilterNodes(List<String> nodes, Year year, Year refYear, Period periodFrom,
      Period periodTo, Period refPeriodFrom, Period refPeriodTo) {
    String filterConditionYear = "";
    String filterConditionQuarter = "";
    String filterConditionMonth = "";

    if (refYear != null) {
      filterConditionYear = "[accountingDate].CurrentMember in {[accountingDate].["
          + getYearFromDate(getFirstDayOfYear(year)) + "]:[accountingDate].["
          + getYearFromDate(getLastDayOfYear(refYear)) + "]} ";
    } else {
      filterConditionYear = "[accountingDate].CurrentMember in {[accountingDate].["
          + getYearFromDate(getFirstDayOfYear(year)) + "]} ";
    }

    if (refYear != null) {
      filterConditionQuarter = " or [accountingDate].[Year].CurrentMember in {"
          + getQuarterFromPeriod(periodFrom, year, true) + ":"
          + getQuarterFromPeriod(periodTo, year, false) + ","
          + getQuarterFromPeriod(refPeriodFrom, refYear, true) + ":"
          + getQuarterFromPeriod(refPeriodTo, refYear, false) + "}";
    } else {
      filterConditionQuarter = " or [accountingDate].[Year].CurrentMember in {"
          + getQuarterFromPeriod(periodFrom, year, true) + ":"
          + getQuarterFromPeriod(periodTo, year, false) + "}";
    }

    if (refYear != null) {
      filterConditionMonth = " or [accountingDate].[Year].[Quarter].CurrentMember in {"
          + getMonthFromPeriod(periodFrom, year, true) + ":"
          + getMonthFromPeriod(periodTo, year, false) + ","
          + getMonthFromPeriod(refPeriodFrom, refYear, true) + ":"
          + getMonthFromPeriod(refPeriodTo, refYear, false) + "}";
    } else {
      filterConditionMonth = " or [accountingDate].[Year].[Quarter].CurrentMember in {"
          + getMonthFromPeriod(periodFrom, year, true) + ":"
          + getMonthFromPeriod(periodTo, year, false) + "}";
    }

    String filterCondition = filterConditionYear + filterConditionQuarter + filterConditionMonth;
    return filterCondition;
  }

  private String getQuarterFromPeriod(Period period, Year year, boolean isStartDate) {
    if (period == null) {
      if (isStartDate) {
        return "[accountingDate].[" + getYearFromDate(getFirstDayOfYear(year)) + "].[1]";
      } else {
        return "[accountingDate].[" + getYearFromDate(getLastDayOfYear(year)) + "].[4]";
      }
    } else {
      if (isStartDate) {
        return "[accountingDate].[" + getYearFromDate(period.getStartingDate()) + "].["
            + getQuarterFromDate(period.getStartingDate()) + "]";
      } else {
        return "[accountingDate].[" + getYearFromDate(period.getEndingDate()) + "].["
            + getQuarterFromDate(period.getEndingDate()) + "]";
      }
    }
  }

  private String getMonthFromPeriod(Period period, Year year, boolean isStartDate) {
    if (period == null) {
      if (isStartDate) {
        return "[accountingDate].[" + getYearFromDate(getFirstDayOfYear(year)) + "].[1].[1]";
      } else {
        return "[accountingDate].[" + getYearFromDate(getLastDayOfYear(year)) + "].[4].[12]";
      }
    } else {
      if (isStartDate) {
        return getQuarterFromPeriod(period, year, isStartDate) + ".["
            + getMonthFromDate(period.getStartingDate()) + "]";
      } else {
        return getQuarterFromPeriod(period, year, isStartDate) + ".["
            + getMonthFromDate(period.getStartingDate()) + "]";
      }
    }
  }

  public String getYearFromDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    Integer year = cal.get(Calendar.YEAR);
    return year.toString();

  }

  public String getQuarterFromDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    Integer quarter = (cal.get(Calendar.MONTH) / 3) + 1;
    return quarter.toString();

  }

  public String getMonthFromDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    Integer month = cal.get(Calendar.MONTH) + 1;
    return month.toString();
  }

  public String getDayFromDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    Integer day = cal.get(Calendar.DAY_OF_MONTH);
    return day.toString();

  }

  /**
   * Return the name of the Dimension
   */
  public String getDimensionName(String dimension) {
    if (dimension.equals(organizationDimension)) {
      return "organization";
    } else if (dimension.equals(businessPartnerDimension)) {
      return "businessPartner-businessPartnerCategory";
    } else if (dimension.equals(productDimension)) {
      return "product-productCategory";
    } else if (dimension.equals(projectDimension)) {
      return "project";
    } else if (dimension.equals(costCenterDimension)) {
      return "costcenter";
    } else if (dimension.equals(user1Dimension)) {
      return "stDimension";
    } else if (dimension.equals(user2Dimension)) {
      return "ndDimension";
    } else {
      Set<Bean<?>> beansSet = WeldUtils.getStaticInstanceBeanManager().getBeans(
          FinancialDimensionsHook.class);
      List<Bean<?>> beansList = new ArrayList<Bean<?>>();
      beansList.addAll(beansSet);

      for (Bean<?> abstractBean : beansList) {
        FinancialDimensionsHook hook = (FinancialDimensionsHook) WeldUtils
            .getStaticInstanceBeanManager().getReference(abstractBean,
                FinancialDimensionsHook.class,
                WeldUtils.getStaticInstanceBeanManager().createCreationalContext(abstractBean));
        if (hook.getHookDimensions().contains(dimension)) {
          return hook.getDimensionName(dimension);
        }
      }

      // TODO exeption
      return null;
    }
  }

  /**
   * Return the node name of the Dimension
   */
  public String getNodesName(String dimension) {
    if (dimension.equals(organizationDimension)) {
      return "[organizationLevel]";
    } else if (dimension.equals(businessPartnerDimension)) {
      return "[businessPartnerLevel]";
    } else if (dimension.equals(productDimension)) {
      return "[productLevel]";
    } else if (dimension.equals(projectDimension)) {
      return "[projectLevel]";
    } else if (dimension.equals(costCenterDimension)) {
      return "[costcenterLevel]";
    } else if (dimension.equals(user1Dimension)) {
      return "[stDimensionLevel]";
    } else if (dimension.equals(user2Dimension)) {
      return "[ndDimensionLevel]";
    } else {
      Set<Bean<?>> beansSet = WeldUtils.getStaticInstanceBeanManager().getBeans(
          FinancialDimensionsHook.class);
      List<Bean<?>> beansList = new ArrayList<Bean<?>>();
      beansList.addAll(beansSet);

      for (Bean<?> abstractBean : beansList) {
        FinancialDimensionsHook hook = (FinancialDimensionsHook) WeldUtils
            .getStaticInstanceBeanManager().getReference(abstractBean,
                FinancialDimensionsHook.class,
                WeldUtils.getStaticInstanceBeanManager().createCreationalContext(abstractBean));
        if (hook.getHookDimensions().contains(dimension)) {
          return hook.getNodeName(dimension);
        }
      }

      // TODO exeption
      return null;
    }
  }

  /**
   * Return a list with all the existing Dimensions
   */
  public Set<String> getDimensionsAvailableList() {
    Set<String> remainingDimensions = new HashSet<String>();
    remainingDimensions.add(organizationDimension);
    remainingDimensions.add(businessPartnerDimension);
    remainingDimensions.add(productDimension);
    remainingDimensions.add(projectDimension);
    remainingDimensions.add(costCenterDimension);
    remainingDimensions.add(user1Dimension);
    remainingDimensions.add(user2Dimension);
    return remainingDimensions;
  }

  /**
   * Returns true if the Dimension does not need to be filtered.
   */
  public boolean thereIsNoFilterForDimension(String dimension, Organization org,
      List<Category> bpGroupList, List<BusinessPartner> bPartnerList,
      List<ProductCategory> productCategoryList, List<Product> productList,
      List<Project> projectList, List<Costcenter> costCenterList, List<UserDimension1> user1List,
      List<UserDimension2> user2List) {
    if (dimension.equals(organizationDimension)) {
      return org.getId().equals("0");
    } else if (dimension.equals(businessPartnerDimension)) {
      return (bPartnerList.size() == 0 && bpGroupList.size() == 0);
    } else if (dimension.equals(productDimension)) {
      return (productList.size() == 0 && productCategoryList.size() == 0);
    } else if (dimension.equals(projectDimension)) {
      return projectList.size() == 0;
    } else if (dimension.equals(costCenterDimension)) {
      return costCenterList.size() == 0;
    } else if (dimension.equals(user1Dimension)) {
      return user1List.size() == 0;
    } else if (dimension.equals(user2Dimension)) {
      return user2List.size() == 0;
    } else {
      // TODO exeption
      return true;
    }
  }

  /**
   * Returns the nodes needed for the Xml file for Saiku
   */
  public List<String> createNodesForFilters(String dimension, List<String> nodes, Organization org,
      List<Category> bpGroupList, List<BusinessPartner> bPartnerList,
      List<ProductCategory> productCategoryList, List<Product> productList,
      List<Project> projectList, List<Costcenter> costCenterList, List<UserDimension1> user1List,
      List<UserDimension2> user2List) {

    if (dimension.equals(organizationDimension) && !"0".equals(org.getId())) {
      nodes.add(getOrgMemberName(org, dimension));
      return nodes;
    } else if (dimension.equals(businessPartnerDimension)) {
      for (BusinessPartner bPartner : bPartnerList) {
        // nodes.add(getMemberName(bPartner, dimension));
        nodes.add("[" + bPartner.getBusinessPartnerCategory().getId() + "].[" + bPartner.getId()
            + "]");
      }
      for (Category bpGroup : bpGroupList) {
        nodes.add("[" + bpGroup.getId() + "]");
      }
      return nodes;
    } else if (dimension.equals(productDimension)) {
      for (Product product : productList) {
        nodes.add(getMemberName(product.getProductCategory(), dimension) + ".[" + product.getId()
            + "]");
      }
      for (ProductCategory productCategory : productCategoryList) {
        nodes.add(getMemberName(productCategory, dimension));
      }
      return nodes;
    } else if (dimension.equals(projectDimension)) {
      for (Project project : projectList) {
        nodes.add(getMemberName(project, projectDimension));
      }
      return nodes;
    } else if (dimension.equals(costCenterDimension)) {
      for (Costcenter costCenter : costCenterList) {
        nodes.add(getMemberName(costCenter, dimension));
      }
      return nodes;
    } else if (dimension.equals(user1Dimension)) {
      for (UserDimension1 user1 : user1List) {
        nodes.add(getMemberName(user1, dimension));
      }
      return nodes;
    } else if (dimension.equals(user2Dimension)) {
      for (UserDimension2 user2 : user2List) {
        nodes.add(getMemberName(user2, dimension));
      }
      return nodes;
    } else {
      // TODO exeption
      return nodes;
    }
  }

  /**
   * Special case when creating the nodes. Only needed for Organization
   */
  private String getOrgMemberName(Organization organization, String dimension) {
    String memberName = getMemberName(
        OBDal.getInstance().get(Organization.class, organization.getId()), dimension);
    return memberName;
  }

  /**
   * Get the Member Name for Saiku, for example: [parent].[parent].[member]
   */
  public String getMemberName(BaseOBObject baseOBObject, String dimension) {
    Tree tree = getTree(dimension);
    String memberName = "";
    memberName = ".[" + baseOBObject.getId() + "]";
    if (tree != null) {
      BaseOBObject child = baseOBObject;
      while (child != null) {
        child = getParent(child, tree, baseOBObject.getEntityName());
        if (child == null) {
          break;
        }
        memberName = ".[" + child.getId() + "]" + memberName.toString();
      }
    }
    memberName = memberName.substring(1, memberName.length());
    return memberName;
  }

  /**
   * Get Parent Node of a Child within a Tree
   */
  private BaseOBObject getParent(BaseOBObject child, Tree tree, String entityName) {
    return OBOlapClosureTreeHandler.getParent(child, tree);
  }

  /**
   * Get Tree for a Dimension
   */
  private Tree getTree(String dimension) {
    OBContext.setAdminMode(true);
    try {
      OBCriteria<Tree> obc = OBDal.getInstance().createCriteria(Tree.class);
      if (productDimension.equals(dimension)) {
        obc.add(Restrictions.eq(Tree.PROPERTY_TYPEAREA, "PC"));
      } else {
        obc.add(Restrictions.eq(Tree.PROPERTY_TYPEAREA, dimension));
      }
      obc.add(Restrictions.eq(Tree.PROPERTY_CLIENT, OBContext.getOBContext().getCurrentClient()));
      return (Tree) obc.uniqueResult();
    } catch (Exception e) {
      return null;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private boolean datesAreNotInTheYear(Date dateFrom, Date dateTo, Year year) {

    OBCriteria<Period> obc = OBDal.getInstance().createCriteria(Period.class);
    obc.add(Restrictions.eq(Period.PROPERTY_YEAR, year));
    obc.addOrderBy(Period.PROPERTY_STARTINGDATE, true);
    List<Period> periodList = obc.list();
    Period startingPeriodOfYear = periodList.get(0);
    Period endingPeriodOfYear = periodList.get(periodList.size() - 1);
    if ((dateFrom != null && dateFrom.before(startingPeriodOfYear.getStartingDate()))
        || (dateTo != null && dateTo.after(endingPeriodOfYear.getEndingDate()))) {
      return true;
    }

    return false;
  }

  /**
   * Return the CubeReportDimensionElements for a given CubeDefinition, process and property
   */
  public List<CubeReportDimensionElement> getCubeReportDimensionElements(CubeDefinition cube,
      String processId, String property) {
    StringBuffer where = new StringBuffer();
    where.append(" as e");
    where.append(" join e.obanalyCubeRepDim as d");
    where.append(" join d.obanalyCubeReport as r");
    where.append(" join r.oBANALYCubeDefinition as c");
    where.append(" join e.obfdrElementvalue as a");
    where.append(" where c.name = :cubeName");
    where.append(" and r.processDefintion.id = :processId");
    where.append(" and d.obanalyCubeDimension.id in (select dim.id");
    where.append("                                  from OBANALY_Cube_Dimension dim");
    where.append("                                  where dim.property = :property)");
    where.append(" order by a.searchKey");
    OBQuery<CubeReportDimensionElement> query = OBDal.getInstance().createQuery(
        CubeReportDimensionElement.class, where.toString());
    query.setNamedParameter("cubeName", cube.getName());
    query.setNamedParameter("processId", processId);
    query.setNamedParameter("property", property);
    return query.list();
  }

  /**
   * Get MDX expression for setting Account nodes in MDX query
   */
  public List<HashMap<String, String>> getAccountNodes(List<CubeReportDimensionElement> elements) {
    List<HashMap<String, String>> acctNodes = new ArrayList<HashMap<String, String>>();
    for (CubeReportDimensionElement element : elements) {
      if (element.getShowType().equals("M") || element.getShowType().equals("B")) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", getMemberName(element.getObfdrElementvalue(), "EV"));
        map.put("type", "member");
        map.put("operator", "MEMBER");
        map.put("exclude", element.isExclude() ? "true" : "false");
        acctNodes.add(map);
      }
      if (element.getShowType().equals("C") || element.getShowType().equals("B")) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("name", getMemberName(element.getObfdrElementvalue(), "EV"));
        map.put("type", "member");
        map.put("operator", "CHILDREN");
        map.put("exclude", element.isExclude() ? "true" : "false");
        acctNodes.add(map);
      }
    }
    return acctNodes;
  }

  /**
   * Get Cube Definition for given Cube Template and Accounting Schema
   */
  public CubeDefinition getCube(String cubeTemplateID, AcctSchema acctSchema) {
    OBCriteria<CubeDefinition> obc = OBDal.getInstance().createCriteria(CubeDefinition.class);
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATECUBE + ".id", cubeTemplateID));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATE, false));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_CLIENT, acctSchema.getClient()));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_OBFDRACCTSCHEMA, acctSchema));
    obc.setFilterOnReadableOrganization(false);
    return (CubeDefinition) obc.uniqueResult();
  }
}
