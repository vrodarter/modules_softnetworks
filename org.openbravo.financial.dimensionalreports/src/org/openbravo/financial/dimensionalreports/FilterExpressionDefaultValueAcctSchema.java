/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.financial.dimensionalreports;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.criterion.Restrictions;
import org.openbravo.client.application.FilterExpression;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchema;

public class FilterExpressionDefaultValueAcctSchema implements FilterExpression {

  @Override
  public String getExpression(Map<String, String> requestMap) {
    org.openbravo.model.common.enterprise.Organization org = OBContext.getOBContext()
        .getCurrentOrganization();
    Set<String> orgParentTree = OBContext.getOBContext().getOrganizationStructureProvider()
        .getNaturalTree(org.getId());
    OBCriteria<AcctSchema> obCriteria = OBDal.getInstance().createCriteria(AcctSchema.class);
    obCriteria.createAlias(AcctSchema.PROPERTY_ORGANIZATION, "o", OBCriteria.LEFT_JOIN);
    obCriteria.add(Restrictions.in("o." + Organization.PROPERTY_ID, orgParentTree));
    List<AcctSchema> acctSchemaList = obCriteria.list();

    if (acctSchemaList.size() > 0) {
      return acctSchemaList.get(0).getId();
    } else {
      return null;
    }
  }

}
