/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.financial.dimensionalreports;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public abstract class FinancialDimensionsHook {

  public abstract List<String> getHookDimensions();

  public abstract String getDimensionName(String dimension);

  public abstract String getNodeName(String dimension);
}
