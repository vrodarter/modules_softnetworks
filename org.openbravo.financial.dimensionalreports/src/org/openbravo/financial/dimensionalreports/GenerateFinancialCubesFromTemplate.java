/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.financial.dimensionalreports;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.analytics.CubeDefinition;
import org.openbravo.client.analytics.CubeDimension;
import org.openbravo.client.analytics.CubeLevel;
import org.openbravo.client.analytics.CubeReport;
import org.openbravo.client.analytics.CubeReportDimension;
import org.openbravo.client.analytics.CubeReportDimensionElement;
import org.openbravo.client.analytics.GenerateCubesFromTemplate;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.financialmgmt.accounting.coa.AcctSchema;

public class GenerateFinancialCubesFromTemplate extends GenerateCubesFromTemplate {

  private static final Logger log = Logger.getLogger(GenerateFinancialCubesFromTemplate.class);
  private static final String templateID = "CD19E04DF4484905A308DE2A13CC278C";

  @Override
  /**
   * Generate Cubes from Financial Reports Template
   */
  public Set<String> getCubeTemplatesToGenerate() {
    Set<String> set = new HashSet<String>();
    set.add(templateID);
    return set;
  }

  @Override
  public void processCube(JSONObject request, JSONObject params, CubeDefinition cube,
      CubeDefinition cubeTemplate, Client client) throws OBException {
    OBContext.setAdminMode(false);
    try {
      removeGeneratedCubeWithoutAcctSchema(cube, cubeTemplate, client);
      List<AcctSchema> acctSchemaList = getAcctSchemaList(client);
      for (AcctSchema acctSchema : acctSchemaList) {
        generateCube(cubeTemplate, acctSchema, client);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Remove Cubes based on this Template for the same Client and without any AcctSchema.
   */
  private void removeGeneratedCubeWithoutAcctSchema(CubeDefinition cube,
      CubeDefinition cubeTemplate, Client client) {
    CubeDefinition previousCube = cube;
    if (previousCube == null) {
      previousCube = getCubeWithoutAcctSchema(cubeTemplate, client);
    }
    if (previousCube == null) {
      return;
    }
    for (CubeReport cubeReport : previousCube.getOBANALYCubeReportList()) {
      for (CubeReportDimension cubeReportDimension : cubeReport.getOBANALYCubeRepDimList()) {
        for (CubeReportDimensionElement cubeReportDimensionElement : cubeReportDimension
            .getOBANALYCubeRepDimElemList()) {
          OBDal.getInstance().remove(cubeReportDimensionElement);
        }
        cubeReportDimension.getOBANALYCubeRepDimElemList().clear();
        OBDal.getInstance().remove(cubeReportDimension);
      }
      cubeReport.getOBANALYCubeRepDimList().clear();
      OBDal.getInstance().remove(cubeReport);
    }
    previousCube.getOBANALYCubeReportList().clear();

    for (CubeDimension cubeDimension : previousCube.getOBANALYCubeDimensionList()) {
      for (CubeLevel cubeLevel : cubeDimension.getOBANALYCubeLevelList()) {
        OBDal.getInstance().remove(cubeLevel);
      }
      cubeDimension.getOBANALYCubeLevelList().clear();
      OBDal.getInstance().remove(cubeDimension);
    }
    previousCube.getOBANALYCubeDimensionList().clear();
    OBDal.getInstance().remove(previousCube);

    OBDal.getInstance().flush();
  }

  /**
   * Get Cubes created based on this Template, for the same Client and without an AcctSchema
   */
  private CubeDefinition getCubeWithoutAcctSchema(CubeDefinition cubeTemplate, Client client) {
    OBCriteria<CubeDefinition> obc = OBDal.getInstance().createCriteria(CubeDefinition.class);
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATECUBE, cubeTemplate));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_CLIENT, client));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATE, false));
    obc.add(Restrictions.isNull(CubeDefinition.PROPERTY_OBFDRACCTSCHEMA));
    obc.setFilterOnReadableClients(false);
    obc.setFilterOnReadableOrganization(false);
    obc.setFilterOnActive(false);
    return (CubeDefinition) obc.uniqueResult();
  }

  private void generateCube(CubeDefinition cubeTemplate, AcctSchema acctSchema, Client client) {
    if (cubeAlreadyExists(cubeTemplate, acctSchema, client)) {
      return;
    }

    CubeDefinition copy = (CubeDefinition) DalUtil.copy(cubeTemplate, true);
    copy.setName(copy.getName().replace(" Template", "") + " - "
        + acctSchema.getName().replace("/", "_"));
    copy.setDescription("");
    copy.setTemplate(false);
    copy.setSqlfilter(copy.getSqlfilter().replace("@c_acctschema_id@",
        "'" + acctSchema.getId() + "'"));
    copy.setClient(client);
    copy.setObfdrAcctschema(acctSchema);
    copy.setTemplateCube(cubeTemplate);
    copy.setModule(null);
    OBDal.getInstance().save(copy);
    for (CubeDimension dimension : copy.getOBANALYCubeDimensionList()) {
      for (CubeLevel level : dimension.getOBANALYCubeLevelList()) {
        level.setClient(client);
        level.setModule(null);
        OBDal.getInstance().save(level);
      }
      dimension.setClient(client);
      dimension.setModule(null);
      if (dimension.getSqlfilter() != null) {
        dimension.setSqlfilter(dimension.getSqlfilter().replace("@c_acctschema_id@",
            "'" + acctSchema.getId() + "'"));
      }
      OBDal.getInstance().save(dimension);
    }

    for (CubeReport report : copy.getOBANALYCubeReportList()) {
      for (CubeReportDimension reportDimension : report.getOBANALYCubeRepDimList()) {
        for (CubeReportDimensionElement element : reportDimension.getOBANALYCubeRepDimElemList()) {
          element.setClient(client);
          element.setModule(null);
          OBDal.getInstance().save(element);
        }
        reportDimension.setClient(client);
        reportDimension.setModule(null);
        OBDal.getInstance().save(reportDimension);
      }
      report.setClient(client);
      report.setModule(null);
      OBDal.getInstance().save(report);
    }

    OBDal.getInstance().flush();
  }

  /**
   * Return true if there is at least one Cube created based on this Template, for the same Client
   * and without an AcctSchema
   */
  private boolean cubeAlreadyExists(CubeDefinition cubeTemplate, AcctSchema acctSchema,
      Client client) {
    OBCriteria<CubeDefinition> obc = OBDal.getInstance().createCriteria(CubeDefinition.class);
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATECUBE, cubeTemplate));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_TEMPLATE, false));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_CLIENT, client));
    obc.add(Restrictions.eq(CubeDefinition.PROPERTY_OBFDRACCTSCHEMA, acctSchema));
    obc.setFilterOnActive(false);
    obc.setFilterOnReadableClients(false);
    obc.setFilterOnReadableOrganization(false);
    return obc.list().size() != 0;
  }

  /**
   * Return AcctSchemas of this Client
   */
  private List<AcctSchema> getAcctSchemaList(Client client) {
    OBCriteria<AcctSchema> obc = OBDal.getInstance().createCriteria(AcctSchema.class);
    obc.add(Restrictions.eq(AcctSchema.PROPERTY_CLIENT, client));
    obc.setFilterOnReadableClients(false);
    obc.setFilterOnReadableOrganization(false);
    return obc.list();
  }

}
