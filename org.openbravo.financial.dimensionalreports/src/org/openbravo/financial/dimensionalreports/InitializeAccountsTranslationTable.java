/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.financial.dimensionalreports;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Tree;
import org.openbravo.model.ad.utility.TreeNode;
import org.openbravo.model.financialmgmt.accounting.AccountingFact;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValue;
import org.openbravo.model.financialmgmt.accounting.coa.ElementValueOperand;

public class InitializeAccountsTranslationTable {

  private static final Logger log = Logger.getLogger(InitializeAccountsTranslationTable.class);

  /**
   * Initialize Account Translation Table. Used for translating Operands.
   */
  public void initializeTranslateTable(Client client) {
    if (noNeedToUpdateTranslateTable(client)) {
      log.debug("No update of account translation table needed");
      return;
    }
    log.debug("Updating account translation table");
    deleteCurrentEntries(client);
    OBDal.getInstance().flush();
    log.debug("Done deleting account translation table");
    insertIntoAccountTranslation(client);
    OBDal.getInstance().commitAndClose();
    log.debug("Done creating account translation table");
  }

  /**
   * Returns true if there is no need to update the Accounts Translation Table
   */
  private boolean noNeedToUpdateTranslateTable(Client client) {
    // Retrieve last updated time of TreeNode Table
    final Session session = OBDal.getInstance().getSession();
    final Query treeNodesQry = session.createQuery("select tn from " + TreeNode.ENTITY_NAME
        + " tn where ad_client_id = ? order by updated desc");
    treeNodesQry.setParameter(0, client.getId());
    treeNodesQry.setMaxResults(1);
    long treeNodeUpdated = 0;
    @SuppressWarnings("unchecked")
    final List<Object> listTn = treeNodesQry.list();
    if (listTn.size() > 0) {
      treeNodeUpdated = ((TreeNode) listTn.get(0)).getUpdated().getTime();
    }
    // Retrieve last updated time of Operands Table
    final Query operandsQry = session.createQuery("select o from "
        + ElementValueOperand.ENTITY_NAME + " o where ad_client_id = ? order by updated desc");
    operandsQry.setParameter(0, client.getId());
    operandsQry.setMaxResults(1);
    long opreandsUpdated = 0;
    @SuppressWarnings("unchecked")
    final List<Object> listOperands = operandsQry.list();
    if (listOperands.size() > 0) {
      opreandsUpdated = ((ElementValueOperand) listOperands.get(0)).getUpdated().getTime();
    }
    // If both tables are null, there is no need to update Accounts Translation Table
    if (listTn.size() <= 0 && listOperands.size() <= 0) {
      return true;
    }

    // Retrieve last updated time of Accounts Translation Table
    final Query translateQry = session.createQuery("select t from "
        + OBFDR_TranslateAccounts.ENTITY_NAME + " t where ad_client_id = ? order by updated");
    translateQry.setParameter(0, client.getId());
    translateQry.setMaxResults(1);
    @SuppressWarnings("unchecked")
    final List<Object> listTrans = translateQry.list();
    long listTransUpdated = -1;
    if (listTrans.size() > 0) {
      listTransUpdated = ((OBFDR_TranslateAccounts) listTrans.get(0)).getUpdated().getTime();
    } else {
      // If the Table is empty, it needs to be updated
      return false;
    }
    // If the updated time is previous to the updated time of the Operands or TreeNode Tables it
    // needs to be updated.
    return listTransUpdated > opreandsUpdated && listTransUpdated > treeNodeUpdated;
  }

  /**
   * Remove Current Entries of Account Translation Table
   */
  private void deleteCurrentEntries(Client client) {
    final Session session = OBDal.getInstance().getSession();
    Query query = session.createQuery("delete from " + OBFDR_TranslateAccounts.ENTITY_NAME
        + " where ad_client_id = ?");
    query.setParameter(0, client.getId());
    int deletedEntities = query.executeUpdate();
    log.debug("Deleted " + deletedEntities + " AccountTranslationTree entries");
  }

  /**
   * Insert records into Accounts Translation Table
   */
  private void insertIntoAccountTranslation(Client client) {
    // All the records of the Operands Table are inserted first
    insertOperandsIntoTranslationTable(client);

    OBDal.getInstance().flush();

    OBFDR_TranslateAccounts node = getChildWithOperandOrChildFromTranslate(client);
    while (node != null) {
      ElementValue parent = node.getReportSet();
      boolean sign = node.isAcctSign();

      // Remove entry from translate
      OBDal.getInstance().remove(node);

      // Add Children
      for (ElementValue ev : getChildren(node)) {
        insertTransalatedId(parent, ev, sign);
      }
      // Add Operands
      for (ElementValueOperand evo : getOperands(node)) {
        insertTransalatedId(parent, evo.getAccount(), sign && evo.getSign() == 1l ? true : (!sign
            && evo.getSign() != 1l ? true : false));
      }

      OBDal.getInstance().flush();

      node = getChildWithOperandOrChildFromTranslate(client);
    }

    // Add leaf accounts once more for not losing information
    correctTranslationTree(client);
    return;
  }

  /**
   * Insert nodes without Translating them
   */
  private void correctTranslationTree(Client client) {
    for (ElementValue node : getListOfNodesToAdd(client))
      insertTransalatedId(node, node, true);
  }

  /**
   * Return the List of Nodes that are present in both the fact_acct and obanaly_translation tables.
   */
  @SuppressWarnings("unchecked")
  private List<ElementValue> getListOfNodesToAdd(Client client) {
    List<ElementValue> nodesList = new ArrayList<ElementValue>();
    OBContext.setAdminMode();
    try {
      StringBuffer myQuery = new StringBuffer();
      myQuery.append(" select distinct e.node ");
      myQuery.append(" from ").append(OBFDR_TranslateAccounts.ENTITY_NAME).append(" e");
      myQuery.append(" where e.client.id=:clientId and e.")
          .append(OBFDR_TranslateAccounts.PROPERTY_NODE).append(" in ");
      myQuery.append("     (select distinct f.").append(AccountingFact.PROPERTY_ACCOUNT);
      myQuery.append("     from ").append(AccountingFact.ENTITY_NAME)
          .append(" f where f.client.id=:clientIdF)");

      final Session session = OBDal.getInstance().getSession();
      Query query = session.createQuery(myQuery.toString());
      query.setParameter("clientId", client.getId());
      query.setParameter("clientIdF", client.getId());
      nodesList = (List<ElementValue>) query.list();
      return nodesList;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Insert all operands into Translation table
   */
  private void insertOperandsIntoTranslationTable(Client client) {
    OBCriteria<ElementValueOperand> obc = OBDal.getInstance().createCriteria(
        ElementValueOperand.class);
    obc.add(Restrictions.eq(ElementValueOperand.PROPERTY_CLIENT, client));
    obc.setFilterOnReadableOrganization(false);
    for (ElementValueOperand operand : obc.list()) {
      insertTransalatedId(operand.getAccountElement(), operand.getAccount(),
          operand.getSign() == 1l);
    }
  }

  /**
   * Get the children of a node in the Accounts Tree
   */
  private List<ElementValue> getChildren(OBFDR_TranslateAccounts node) {
    List<ElementValue> list = new ArrayList<ElementValue>();
    OBCriteria<Tree> treeQuery = OBDal.getInstance().createCriteria(Tree.class);
    treeQuery.add(Restrictions.eq(Tree.PROPERTY_TYPEAREA, "EV"));
    treeQuery.add(Restrictions
        .eq(Tree.PROPERTY_CLIENT, OBContext.getOBContext().getCurrentClient()));
    Tree tree = (Tree) treeQuery.uniqueResult();
    OBCriteria<TreeNode> obc = OBDal.getInstance().createCriteria(TreeNode.class);
    obc.add(Restrictions.eq(TreeNode.PROPERTY_TREE,
        OBDal.getInstance().get(Tree.class, tree.getId())));
    obc.add(Restrictions.eq(TreeNode.PROPERTY_REPORTSET, node.getNode().getId()));
    for (TreeNode tn : obc.list()) {
      ElementValue child = OBDal.getInstance().get(ElementValue.class, tn.getNode());
      if (child != null) {
        list.add(child);
      }
    }
    return list;

  }

  /**
   * Get a Node from the Translation table that has either children or operands to explode.
   */
  private OBFDR_TranslateAccounts getChildWithOperandOrChildFromTranslate(Client client) {
    OBContext.setAdminMode();
    try {
      StringBuffer myQuery = new StringBuffer();
      myQuery.append("select e.id from ").append(OBFDR_TranslateAccounts.ENTITY_NAME);
      myQuery.append(" e where e.client.id=:clientId and (exists (select 1 from ")
          .append(ElementValueOperand.ENTITY_NAME)
          .append(" evo where evo.client.id=:evoClientId and evo.")
          .append(ElementValueOperand.PROPERTY_ACCOUNTELEMENT).append(" = e.")
          .append(OBFDR_TranslateAccounts.PROPERTY_NODE).append(")");
      myQuery.append(" or exists (select 1 from ").append(TreeNode.ENTITY_NAME)
          .append(" tn where tn.").append(TreeNode.PROPERTY_REPORTSET).append(" = e.")
          .append(OBFDR_TranslateAccounts.PROPERTY_NODE).append(".id))");
      final Session session = OBDal.getInstance().getSession();
      Query query = session.createQuery(myQuery.toString());
      query.setParameter("clientId", client.getId());
      query.setParameter("evoClientId", client.getId());
      query.setMaxResults(1);
      String id = (String) query.uniqueResult();
      return id == null ? null : OBDal.getInstance().get(OBFDR_TranslateAccounts.class, id);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Get the Operands used in a node.
   */
  private List<ElementValueOperand> getOperands(OBFDR_TranslateAccounts node) {
    OBCriteria<ElementValueOperand> obc = OBDal.getInstance().createCriteria(
        ElementValueOperand.class);
    obc.add(Restrictions.eq(ElementValueOperand.PROPERTY_ACCOUNTELEMENT, node.getNode()));
    return obc.list();
  }

  /**
   * Insert node into DB
   */
  private void insertTransalatedId(ElementValue node, ElementValue child, boolean sign) {
    OBFDR_TranslateAccounts newElement = OBProvider.getInstance()
        .get(OBFDR_TranslateAccounts.class);
    newElement.setNode(child);
    newElement.setReportSet(node);
    newElement.setOrganization(node.getOrganization());
    newElement.setAcctSign(sign);
    newElement.setDebit(!"C".equals(child.getAccountSign()));
    OBDal.getInstance().save(newElement);
  }

}
