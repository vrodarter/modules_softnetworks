/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * Mallikarjun M
 ************************************************************************************
 */

package org.openbravo.localization.us.checkprinting.ad_forms;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;

import org.hibernate.Query;
import org.openbravo.advpaymentmngt.dao.AdvPaymentMngtDao;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.JRFormatFactory;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.FIN_Payment_Credit;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.utils.FileUtility;
import org.openbravo.utils.Replace;
import org.openbravo.xmlEngine.XmlDocument;

public class CheckManagement extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT", "FIND")) {
    	log4j.info("Inside default........");
    	 String strCheckNoFromId = vars.getRequestGlobalVariable("inpmCheckNoFromId",
         "CheckManagement|inpmCheckNoFromId");

    	 String strCheckNoToId = vars.getRequestGlobalVariable("inpmCheckNoToId",
         "CheckManagement|inpmCheckNoToId");

    	 String strClient = vars.getRequestGlobalVariable("inpmClientId",
         "CheckManagement|inpmClientId");

    	 String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
         "CheckManagement|Ad_Org_ID");

    	 if(strAD_Org_ID==null || strAD_Org_ID.equals("")){
     		strAD_Org_ID="0";
     	}

    	 String strCPFromDate = vars.getRequestGlobalVariable("inpmCPFromDate",
         "CheckManagement|inpmCPFromDate");

    	 String strCPToDate = vars.getRequestGlobalVariable("inpmCPToDate",
         "CheckManagement|inpmCPToDate");

    	 String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
         "CheckManagement|inpmInvoiceNo");

    	 String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
            "CheckManagement|C_BPartner_ID");

    	 String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
        "CheckManagement|inpmBankAcc");

    	 String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
        "CheckPrintingCategory|inpmOrderNo");

    	 String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
        "CheckPrintingCategory|inpmPaymentNo");

    	 String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
        "CheckPrintingCategory|inpmProposalNo");

    	 reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

    } else if (vars.commandIn("SET")) {
            response.sendRedirect(strDireccion + request.getServletPath());
    } else if (vars.commandIn("PRINT")) {
    	log4j.info("Reprinting...");
    	 String strCheckNoFromId = vars.getRequestGlobalVariable("inpmCheckNoFromId",
         "CheckManagement|inpmCheckNoFromId");

    	 String strCheckNoToId = vars.getRequestGlobalVariable("inpmCheckNoToId",
         "CheckManagement|inpmCheckNoToId");

    	 String strClient = vars.getRequestGlobalVariable("inpmClientId",
         "CheckManagement|inpmClientId");

    	 String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
         "CheckManagement|Ad_Org_ID");

    	 String strCPFromDate = vars.getRequestGlobalVariable("inpmCPFromDate",
         "CheckManagement|inpmCPFromDate");

     	String strCPToDate = vars.getRequestGlobalVariable("inpmCPToDate",
         "CheckManagement|inpmCPToDate");

     	 String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
         "CheckManagement|inpmInvoiceNo");

        String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
            "CheckManagement|C_BPartner_ID");

        String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
        "CheckManagement|inpmBankAcc");

    	  String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);

    	  String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
          "CheckPrintingCategory|inpmOrderNo");

    	  String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
          "CheckPrintingCategory|inpmPaymentNo");

          String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
          "CheckPrintingCategory|inpmProposalNo");
          log4j.info("strCheckNoToId..."+strCheckNoToId);
        if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
        	//Rohit starts
        	String isvoid=isVoid(this,strSelectedId);
        	log4j.info("strSelectedId....."+strSelectedId);
        	log4j.info("isvoid....."+isvoid);
        	if(strSelectedId.equalsIgnoreCase("('')") || isvoid.equalsIgnoreCase("Y")){
        		String strMessage="One or more selected Checks is voided. Voided Checks cannot be re-printed.";
    			String strTitle="Error";
    		  	String strType="ERROR";
    		  	vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
    		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

        	}else{ //Rohit ends
    		  	String strPDFFileName=printChecks(this,vars,request,response,strSelectedId);
    		  	
//    		  	// Rohit -Eulen Starts
//    		  	String strTemp[]=strSelectedId.substring(1, strSelectedId.length()-1).split(",");
//    		  	String strStubIds="";
//
//    		    for(int i=0;i<strTemp.length;i++){
//    		    	
//    		  	  	String strSelId=strTemp[i];
//    		  	  	System.out.println("Inside for strSelId before substring......... "+strSelId);
//    		  	    //strSelId=strSelId.substring(1, strSelId.length()-1);
//    		  	    System.out.println("Inside for strSelId......... "+strSelId);
//    		  	    int stubCount=getStubCount(this,strSelId);
//    		  	    if (stubCount>14){
//    		  	    	System.out.println("Inside if stubCount......... "+stubCount);
//    		  	    	strStubIds=strStubIds+strSelId;
//    		  	    }
//     		    }
//    		    if(!strStubIds.equalsIgnoreCase("")){
//    		    	strStubIds="("+strStubIds+")";
//    		    String strStubPDFFileName=printStubs(this,vars,request,response,strStubIds);
//    		    strPDFFileName=strPDFFileName+strStubPDFFileName;
//    		    }
//    		    // Rohit -Eulen ends
     		  	
    		  	String strMessage="To Re-Print Check(s) or Stub(s) or Void any Check(s), click 'Check Management'. "
    		  					 +"To Print new check(s), click 'Check Printing'.";
    		  	String strTitle="Check(s) Printed Successfully.";
    		  	String strType="SUCCESS";
    		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
    		  	reprintedChecks(this,strSelectedId);
    		  	printedPageDataSheet(response,vars,strSelectedId,strPDFFileName);
    			request.getSession().setAttribute("PrintedRecords", strSelectedId);
    			request.getSession().setAttribute("strPDFFileName", strPDFFileName);
        	}

    	  }else{
    		    String strMessage="Please select atleast one Check to re-print";
    			String strTitle="Error";
    		  	String strType="ERROR";
    		    vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
    		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

    	  }

   }  // Eulen 10th may starts
    else if (vars.commandIn("STUB")) {
   	log4j.info("Reprinting...");
	 String strCheckNoFromId = vars.getRequestGlobalVariable("inpmCheckNoFromId",
    "CheckManagement|inpmCheckNoFromId");

	 String strCheckNoToId = vars.getRequestGlobalVariable("inpmCheckNoToId",
    "CheckManagement|inpmCheckNoToId");

	 String strClient = vars.getRequestGlobalVariable("inpmClientId",
    "CheckManagement|inpmClientId");

	 String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
    "CheckManagement|Ad_Org_ID");

	 String strCPFromDate = vars.getRequestGlobalVariable("inpmCPFromDate",
    "CheckManagement|inpmCPFromDate");

	String strCPToDate = vars.getRequestGlobalVariable("inpmCPToDate",
    "CheckManagement|inpmCPToDate");

	 String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
    "CheckManagement|inpmInvoiceNo");

   String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
       "CheckManagement|C_BPartner_ID");

   String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
   "CheckManagement|inpmBankAcc");

	  String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);

	  String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
     "CheckPrintingCategory|inpmOrderNo");

	  String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
     "CheckPrintingCategory|inpmPaymentNo");

     String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
     "CheckPrintingCategory|inpmProposalNo");
     log4j.info("strCheckNoToId..."+strCheckNoToId);
   if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
   	//Rohit starts
   	String isvoid=isVoid(this,strSelectedId);
   	log4j.info("strSelectedId....."+strSelectedId);
   	log4j.info("isvoid....."+isvoid);
   	if(strSelectedId.equalsIgnoreCase("('')") || isvoid.equalsIgnoreCase("Y")){
   		String strMessage="One or more selected Checks is voided. Stubs for Voided Checks cannot be (re) printed.";
			String strTitle="Error";
		  	String strType="ERROR";
		  	vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

   	}else{ //Rohit ends
		  	//String strPDFFileName=printChecks(this,vars,request,response,strSelectedId);
   			String strPDFFileName="";
		  	
		  	// Rohit -Eulen Starts
		  	String strTemp[]=strSelectedId.substring(1, strSelectedId.length()-1).split(",");
		  	String strStubIds="";
		  	//Boolean noStub=false;

		    for(int i=0;i<strTemp.length;i++){
		    	
		  	  	String strSelId=strTemp[i];
		  	  	//System.out.println("Inside for strSelId before substring......... "+strSelId);
		  	    //strSelId=strSelId.substring(1, strSelId.length()-1);
		  	    //System.out.println("Inside for strSelId......... "+strSelId);
		  	    //int stubCount=getStubCount(this,strSelId);
//		  	    if (stubCount>14){
//		  	    	System.out.println("Inside if stubCount......... "+stubCount);
		  	    	strStubIds=strStubIds+strSelId+",";
//		  	    	System.out.println("strStubIds............"+strStubIds);
//		  	    }else{
//		  	    	noStub=true;
//		  	    }
		    }
//		    if(noStub==true){
//		    	
//		    	String strMessage="One or more selected Checks doesn't have Stub.";
//				String strTitle="Error";
//			  	String strType="ERROR";
//			  	vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
//			    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);
//
//		    	
//		    }
//		    else
		    	if(!strStubIds.equalsIgnoreCase("")){
		    	strStubIds=strStubIds.substring(0,strStubIds.length()-1);
		    	strStubIds="("+strStubIds+")";
		    strPDFFileName=printStubs(this,vars,request,response,strStubIds);
		    }
		    // Rohit -Eulen ends
		  	
		  	String strMessage="To Re-Print Check(s) or Stub(s) or Void any Check(s), click 'Check Management'. "
		  					 +"To Print new check(s), click 'Check Printing'.";
		  	String strTitle="Check(s) Printed Successfully.";
		  	String strType="SUCCESS";
		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
		  	reprintedChecks(this,strSelectedId);
		  	printedPageDataSheet(response,vars,strSelectedId,strPDFFileName);
			request.getSession().setAttribute("PrintedRecords", strSelectedId);
			request.getSession().setAttribute("strPDFFileName", strPDFFileName);
   	}

	  }else{
		    String strMessage="Please select atleast one Check to re-print Stub";
			String strTitle="Error";
		  	String strType="ERROR";
		    vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

	  }
} // Eulen Ends 10 th may
    else if (vars.commandIn("PRINTED")) {
	   String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);
	   if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
		   String strPDFFileName=printedChecks(vars,request,response,strSelectedId);
		   String strMessage="To Re-Print or Void any check(s), click 'Check Management'. "
				 +"To Print new check(s), click 'Check Printing'.";
		  	String strTitle="Check(s) Printed Successfully.";
		  	String strType="SUCCESS";
		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
		  	reprintedChecks(this,strSelectedId);
		  	printedPageDataSheet(response,vars,strSelectedId,strPDFFileName);
 	    	request.getSession().setAttribute("PrintedRecords", strSelectedId);
 	    	request.getSession().setAttribute("strPDFFileName", strPDFFileName);

	   }else{
 		  	String strAlreadySelIds=(String)request.getSession().getAttribute("PrintedRecords");
 		  	String strMessage="Please select atleast one Check to re-print";
			String strTitle="Error";
		  	String strType="ERROR";
		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
		  	printedPageDataSheet(response,vars,strAlreadySelIds,null);
 	  }

   }else if (vars.commandIn("VOID")) {

     AdvPaymentMngtDao dao = new AdvPaymentMngtDao();
  	 String strCheckNoFromId = vars.getRequestGlobalVariable("inpmCheckNoFromId",
       "CheckManagement|inpmCheckNoFromId");

  	 String strCheckNoToId = vars.getRequestGlobalVariable("inpmCheckNoToId",
       "CheckManagement|inpmCheckNoToId");

  	 String strClient = vars.getRequestGlobalVariable("inpmClientId",
       "CheckManagement|inpmClientId");

  	 String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
       "CheckManagement|Ad_Org_ID");

  	 String strCPFromDate = vars.getRequestGlobalVariable("inpmCPFromDate",
       "CheckManagement|inpmCPFromDate");

   	 String strCPToDate = vars.getRequestGlobalVariable("inpmCPToDate",
       "CheckManagement|inpmCPToDate");

   	 String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
       "CheckManagement|inpmInvoiceNo");

      String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
          "CheckManagement|C_BPartner_ID");

      String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
      "CheckManagement|inpmBankAcc");

  	  String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);

  	  String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
     "CheckPrintingCategory|inpmOrderNo");

  	  String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
     "CheckPrintingCategory|inpmPaymentNo");

     String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
     "CheckPrintingCategory|inpmProposalNo");
     log4j.info("strInvoiceNo......"+strInvoiceNo);
     String isvoid="N";
     if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
     isvoid=isVoid(this,strSelectedId);
     }else{
    	 isvoid="N";
     }
 	log4j.info("voiding ...strSelectedId....."+strSelectedId);
 	log4j.info("voiding ...isvoid....."+isvoid);
 	if(strSelectedId.equalsIgnoreCase("('')") || isvoid.equalsIgnoreCase("Y")){
 			String strMessage="One or more selected Checks is already voided.";
			String strTitle="Error";
		  	String strType="ERROR";
		  	vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

 	}else if(!strSelectedId.equals(null) && !strSelectedId.equals("")){

  		  	String strPDFFileName=printChecks(this,vars,request,response,strSelectedId);
  		  	String strMessage="Check(s) Voided Successfully";
  		  	String strTitle="";
  		  	String strType="SUCCESS";
  		  	vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
  		  	//update the stauts to "payment pending" in both fin_payment and cp_checks table
  		  	voidedChecks(this,strSelectedId);
  		  	//Rohit begins
  		  	clearPayments(this,strSelectedId,vars,dao);
  		    //paymentProcessCancel(this,strSelectedId,"FIN_PAYMENT");
  		    

  		  	//Rohit ends
		    //UPDATE THE FIN_PAYMENT
	    	//voidedPaymentsChecks(this,strSelectedId);
  		  	reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);
  	    	//UPDATE THE CP-CHECKS

  			request.getSession().setAttribute("PrintedRecords", strSelectedId);
  			request.getSession().setAttribute("strPDFFileName", strPDFFileName);
  	  }else{
  		    String strMessage="Please select atleast one Check to void";
  		    String strTitle="Error";
		  	String strType="ERROR";
		    vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

  	  }


   }
//   else if(vars.commandIn("REISSUE")){
//	   String strCheckNoFromId = vars.getRequestGlobalVariable("inpmCheckNoFromId",
//       "CheckManagement|inpmCheckNoFromId");
//
//  	 String strCheckNoToId = vars.getRequestGlobalVariable("inpmCheckNoToId",
//       "CheckManagement|inpmCheckNoToId");
//
//  	 String strClient = vars.getRequestGlobalVariable("inpmClientId",
//       "CheckManagement|inpmClientId");
//
//  	 String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
//       "CheckManagement|Ad_Org_ID");
//
//  	 String strCPFromDate = vars.getRequestGlobalVariable("inpmCPFromDate",
//       "CheckManagement|inpmCPFromDate");
//
//   	 String strCPToDate = vars.getRequestGlobalVariable("inpmCPToDate",
//       "CheckManagement|inpmCPToDate");
//
//   	 String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
//       "CheckManagement|inpmInvoiceNo");
//
//      String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
//          "CheckManagement|C_BPartner_ID");
//
//      String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
//      "CheckManagement|inpmBankAcc");
//
//  	  String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);
//
//  	  String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
//     "CheckPrintingCategory|inpmOrderNo");
//
//  	  String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
//     "CheckPrintingCategory|inpmPaymentNo");
//
//     String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
//     "CheckPrintingCategory|inpmProposalNo");
//     String strPDFFileName="";
//     String strIntNewCheckNo="";
//	   if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
//
//		   boolean bVoidExist=CheckManagementData.getVoidExist(this,strSelectedId);
//		   if(bVoidExist){
//			   log4j.info("re issue a new check to this payment.strSelectedId.."+strSelectedId);
//			   strIntNewCheckNo=CheckPrintingCategoryData.CheckCount(this);
//			   int iNxtCheckNo=Integer.valueOf(strIntNewCheckNo)+1;
//			   strIntNewCheckNo=iNxtCheckNo+"";
//			    log4j.info("strIntNewCheckNo.."+strIntNewCheckNo);
//			    strPDFFileName=reIssueChecks(vars,request,response,strSelectedId,strIntNewCheckNo,strClient,strAD_Org_ID,strC_BPartner_ID);
//     		  	String strMessage="To reprint any check, select and click 'Ok'";
//     		  	String strTitle="Success";
//     		  	String strType="SUCCESS";
//     		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
//     			reprintedChecks(this,strSelectedId);
//    		  	printedPageDataSheet(response,vars,strSelectedId,strPDFFileName);
//    			request.getSession().setAttribute("PrintedRecords", strSelectedId);
//    			request.getSession().setAttribute("strPDFFileName", strPDFFileName);
//
//
//		   }else{
//	  		    String strMessage="Please select atlease one voided record to re issue";
//	  			String strTitle="Error";
//	  		  	String strType="ERROR";
//	  		    vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
//	  		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);
//		   }
//	   }else{
//  		    String strMessage="Please select atlease one voided record to re issue";
//  			String strTitle="Error";
//  		  	String strType="ERROR";
//  		    vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
//  		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);
//
//  	  }
//
//   }
   else
      pageError(response);
  }

  private String  printedChecks(VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFFileName="";
	  try{
		  data = CheckPrintingCategoryData.getPrintPaymentDetails(this,strSelectedIds);

		  String strSelId="";
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");

		    for(int i=0;i<strTemp.length;i++){
		  	  strSelId=strTemp[i];
		  	  strSelId=strSelId.substring(1, strSelId.length()-1);
		  	  break;
		    }

		    final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,strSelId);

		    String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		    String strTemplateName=finPayObj.getAccount().getUsloccpTempname();

		    log4j.info("strTemplateLocation...."+strTemplateLocation);
		    log4j.info("strTemplateName...."+strTemplateName);
		 
		    String strReportName=strTemplateLocation+"/"+strTemplateName;
		    for(int i=0;i<strTemp.length;i++){
		  	  	strSelId=strTemp[i];
		  	  strSelId="("+strSelId+")";

			  HashMap<String, Object> designParameters = new HashMap<String, Object>();
			  designParameters.put("strSelectedIds", strSelId);
			  int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelId);
			  designParameters.put("paymentsCount", payCount);
			  log4j.info("paymentsCount...."+payCount);

			  String renderFile=renderJRFile(variables, response,strReportName,"pdf", designParameters,data,null);
			  strPDFFileName=strPDFFileName+renderFile+",";
		    }

	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFFileName;

  }


  /**
   * Reinier Van Ommeren					Issue SPI-1103 				30 January 2012
   *    Changes - Multiple check bug to where a PDF is created for each check.. This module looped through each selectedId and 
   *     ran a report for each one.  Now the entire list is passed onto the renderJRFile(). 
   */
  private String printChecks(ConnectionProvider connectionProvider,VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFFileName="";
	  try{

		  data = CheckPrintingCategoryData.getPrintPaymentDetails(connectionProvider,strSelectedIds);

		  String strSelId="";
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");

		    for(int i=0;i<strTemp.length;i++){
		  	  	strSelId=strTemp[i];
		  	    strSelId=strSelId.substring(1, strSelId.length()-1);
		  	   break;
		    }

		    log4j.info("strSelId...."+strSelId);
		    final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,strSelId);

		    String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		    String strTemplateName=finPayObj.getAccount().getUsloccpTempname();

		    log4j.info("strTemplateLocation...."+strTemplateLocation);
		    log4j.info("strTemplateName...."+strTemplateName);


		 /* String strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/CheckPrinting.jrxml";

		  String srtRDBMS=connectionProvider.getRDBMS();

				if(srtRDBMS.equals("ORACLE")){
					strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/OracleCheckPrinting.jrxml";
				}else{
				    strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/PostGreCheckPrinting.jrxml";
				}*/
			  String strReportName=strTemplateLocation+"/"+strTemplateName;

	        log4j.info("strSelectedIds...." + strSelectedIds);
	  	  	HashMap<String, Object> designParameters = new HashMap<String, Object>();
	  	  	designParameters.put("strSelectedIds", strSelectedIds);
		  	  	
	  	  	int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelectedIds);
	  	  	designParameters.put("paymentsCount", payCount);
            log4j.info("paymentsCount...."+payCount);
			  
	  	  	String renderFile=renderJRFile(variables, response,strReportName,"pdf", designParameters,data,null);
	  	    log4j.info("renderFile...."+renderFile);
		  	strPDFFileName=strPDFFileName+renderFile+",";
		  	log4j.info("strPDFFileName...."+strPDFFileName);
			  

	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFFileName;

  }

  /**
   Rohit changed in may 4 2012 for Eulen
   */
  private String printStubs(ConnectionProvider connectionProvider,VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFFileName="";
	  try{

		  data = CheckPrintingCategoryData.getPrintPaymentDetails(connectionProvider,strSelectedIds);

		  String strSelId="";
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");

		    for(int i=0;i<strTemp.length;i++){
		  	  	strSelId=strTemp[i];
		  	    strSelId=strSelId.substring(1, strSelId.length()-1);
		  	   break;
		    }

		    log4j.info("strSelId...."+strSelId);
		    final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,strSelId);

		    String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		    String strTemplateName="Stub-Generic.jrxml";//finPayObj.getAccount().getUsloccpTempname();

		    log4j.info("strTemplateLocation...."+strTemplateLocation);
		    log4j.info("strTemplateName...."+strTemplateName);


		 /* String strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/CheckPrinting.jrxml";

		  String srtRDBMS=connectionProvider.getRDBMS();

				if(srtRDBMS.equals("ORACLE")){
					strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/OracleCheckPrinting.jrxml";
				}else{
				    strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/PostGreCheckPrinting.jrxml";
				}*/
			  String strReportName=strTemplateLocation+"/"+strTemplateName;

	        log4j.info("strSelectedIds...." + strSelectedIds);
	  	  	HashMap<String, Object> designParameters = new HashMap<String, Object>();
	  	  	designParameters.put("strSelectedIds", strSelectedIds);
		  	  	
	  	  	int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelectedIds);
	  	  	designParameters.put("paymentsCount", payCount);
            log4j.info("paymentsCount...."+payCount);
			  
	  	  	String renderFile=renderJRFile(variables, response,strReportName,"pdf", designParameters,data,null);
	  	    log4j.info("renderFile...."+renderFile);
		  	strPDFFileName=strPDFFileName+renderFile+",";
		  	log4j.info("strPDFFileName...."+strPDFFileName);
			  

	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFFileName;

  }


  private void reprintChecks(HttpServletResponse response, VariablesSecureApp vars,
		  String strCheckNoFromId,String  strCheckNoToId,String strClient,String strAD_Org_ID,String strCPFromDate,
		  String strCPToDate,String strInvoiceNo,String strC_BPartner_ID,String strBankAcc,String strOrderNo,String strPaymentNo,String strProposalNo) throws IOException, ServletException {

	  response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();

	    String discard[] = { };
	    XmlDocument xmlDocument = null;
	    CheckManagementData[] data = null;

      xmlDocument = xmlEngine.readXmlTemplate(
          "org/openbravo/localization/us/checkprinting/ad_forms/CheckManagement").createXmlDocument();
      data= CheckManagementData.reprintChecks(this,strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,
    		  strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

      // BEGIN generate and set UI elements

      ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CheckManagement", false, "",
          "", "", false, "ad_forms", strReplaceWith, false, true);
      toolbar.prepareSimpleToolBarTemplate();
      xmlDocument.setParameter("toolbar", toolbar.toString());
      try {
        WindowTabs tabs = new WindowTabs(this, vars,
            "org.openbravo.localization.us.checkprinting.ad_forms.CheckManagement");
        xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
        xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
        xmlDocument.setParameter("childTabContainer", tabs.childTabs());
        xmlDocument.setParameter("theme", vars.getTheme());
        NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
            "CheckManagement.html", classInfo.id, classInfo.type, strReplaceWith, tabs
                .breadcrumb());
        xmlDocument.setParameter("navigationBar", nav.toString());
        LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CheckManagement.html",
            strReplaceWith);
        xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
      xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
    // END generate and set UI elements

    // BEGIN check for any pending message to be shown and clear it from the session
      OBError myMessage = vars.getMessage("CheckManagement");
      vars.removeMessage("CheckManagement");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    // END check for any pending message to be shown and clear it from the session

    // BEGIN Organization dropdown - using tabledir reference
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
            "AD_Org Security validation", Utility.getContext(this, vars, "#User_Org",
                "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                "CheckManagement"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
        xmlDocument.setData("NAME_reportOrganizationDropdown", "liststructure", comboTableData
            .select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }


      try {

          ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
              "ad_client_id", "", "", Utility.getContext(this, vars, "#User_Org",
                  "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                  "CheckManagement"), 0);
          Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
          xmlDocument.setData("NAME_reportClient", "liststructure",
              comboTableData.select(false));
          comboTableData = null;
        } catch (Exception ex) {
          throw new ServletException(ex);
        }

        try {
        	String strId = null;

        	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
        		strId = "7ACD326BEAE34F338343AD4FFE793F77";
        	}
        	else
        	{
        		strId = "8511593E88D24C00991B382D0CF49B9E";
        	}
        		ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                "c_invoice_id", strId, "", Utility.getContext(this, vars, "#User_Org",
                    "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                    "CheckManagement"), 0);
            Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
            xmlDocument.setData("NAME_reportInvoiceNo", "liststructure",
                comboTableData.select(false));
            comboTableData = null;
          } catch (Exception ex) {
            throw new ServletException(ex);
          }


              try {
                  ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                      "FIN_FINANCIAL_ACCOUNT_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                          "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                          "CheckManagement"), 0);
                  Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
                  xmlDocument.setData("NAME_reportBankAcc", "liststructure",
                      comboTableData.select(false));
                  comboTableData = null;
                } catch (Exception ex) {
                  throw new ServletException(ex);
                }


                //this combo for order...
                try {
                	String strOrder_Id = null;

                	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
                		strOrder_Id = "FD620CDEFBF2438B932269EBC65D0825";
                	}
                	else
                	{
                		strOrder_Id = "2036A37E2A6C4BD2BCAACD3CCEB0F67C";
                	}

                    ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                        "C_Order_ID", strOrder_Id, "", Utility.getContext(this, vars, "#User_Org",
                            "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                            "CheckManagement"), 0);
                    Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
                    xmlDocument.setData("NAME_reportOrderNo", "liststructure",
                        comboTableData.select(false));
                    comboTableData = null;
                  } catch (Exception ex) {
                    throw new ServletException(ex);
                  }

                  try {
                  	String strPayment_Id = null;

                	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
                		strPayment_Id = "8A18129DBDF047AD9A93DC2C8AFFEFD4";
                	}
                	else
                	{
                		strPayment_Id = "C584536268CA4CF683F4761B57EBC67E";
                	}
                      ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                          "documentno", strPayment_Id, "", Utility.getContext(this, vars, "#User_Org",
                              "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                              "CheckPrintingCategory"), 0);
                      Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                      xmlDocument.setData("NAME_reportPaymentNo", "liststructure",
                          comboTableData.select(false));
                      comboTableData = null;

                      

//                      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
//                          "FIN_PAYMENT_ID", "", "", Utility.getContext(this, vars, "#User_Org",
//                              "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
//                              "CheckManagement"), 0);
//                      Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
//                      xmlDocument.setData("NAME_reportPaymentNo", "liststructure",
//                          comboTableData.select(false));
//                      comboTableData = null;

                      //

                    } catch (Exception ex) {
                      throw new ServletException(ex);
                    }

                    try {
                      	String strProposal_Id = null;

                    	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
                    		strProposal_Id = "00D5D95618EE4C70B9488BDA9696BECD";
                    	}
                    	else
                    	{
                    		strProposal_Id = "417E0A623BE04FBC95B73333655C5C3E";
                    	}
                        ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                            "documentno", strProposal_Id, "", Utility.getContext(this, vars, "#User_Org",
                                "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                                "CheckManagement"), 0);
                        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
                        xmlDocument.setData("NAME_reportProposalNo", "liststructure",
                            comboTableData.select(false));
                        comboTableData = null;
                      } catch (Exception ex) {
                        throw new ServletException(ex);
                      }



    // BEGIN put parameter values back into the form so that they remain entered on reload

	        xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
			xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
			xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
			xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));

			xmlDocument.setParameter("NAME_paramBPartnerId", strC_BPartner_ID);

		  final BusinessPartner objBPartner = OBDal.getInstance().get(BusinessPartner.class,
		  			strC_BPartner_ID);
		      String bPartnerDescription="";
		      if(objBPartner!=null){
		    	  bPartnerDescription=objBPartner.getName();
		      }
		      xmlDocument.setParameter("NAME_paramBPartnerDescription",bPartnerDescription);

		      xmlDocument.setParameter("NAME_paramOrganizationId", strAD_Org_ID);

			  xmlDocument.setParameter("NAME_paramClient", strClient);
			  xmlDocument.setParameter("NAME_paramInvoiceNo", strInvoiceNo);
			  xmlDocument.setParameter("NAME_paramBankAcc", strBankAcc);
			  xmlDocument.setParameter("NAME_paramCheckNoFrom", strCheckNoFromId);
			  xmlDocument.setParameter("NAME_paramCheckNoTo", strCheckNoToId);

			  xmlDocument.setParameter("NAME_paramCheckDateFrom", strCPFromDate);
			  xmlDocument.setParameter("NAME_paramCheckDateTo", strCPToDate);

			  xmlDocument.setParameter("NAME_paramOrderNo", strOrderNo);
			  xmlDocument.setParameter("NAME_paramPaymentNo", strPaymentNo);
			  xmlDocument.setParameter("NAME_paramProposalNo", strProposalNo);

			  xmlDocument.setData("structure1", data);

    // BEGIN render the output and close it so that it is returned to the user
    out.println(xmlDocument.print());
    out.close();
    // END render the output and close it so that it is returned to the user
  }




  public String getServletInfo() {
    return "CheckManagement Servlet. This Servlet was made by Mallikarjun ";
  }


  private void printedPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
	      String strSelectedId,String strPDFFileName ) throws IOException, ServletException {


	    log4j.debug("CheckPrintedCategory: Call to printedPageDataSheet");

	    // BEGIN initialize response
	      response.setContentType("text/html; charset=UTF-8");
	      PrintWriter out = response.getWriter();
	    // END initialize response

	    String discard[] = { };
	    XmlDocument xmlDocument = null;
	    CheckPrintingCategoryData[] data = null;

	      xmlDocument = xmlEngine.readXmlTemplate(
	          "org/openbravo/localization/us/checkprinting/ad_forms/CheckPrintedCategory").createXmlDocument();
	      data= CheckPrintingCategoryData.customPrintedSelect(this,strSelectedId);

	    // BEGIN generate and set UI elements
	    /*  ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CheckPrintedCategory", false, "",
				"", "", false, "ad_forms", strReplaceWith, false, true);
	      toolbar.prepareSimpleToolBarTemplate();
	      xmlDocument.setParameter("toolbar", toolbar.toString());*/
	      try {
	        WindowTabs tabs = new WindowTabs(this, vars,
	            "org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintedCategory");
	        xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
	        xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
	        xmlDocument.setParameter("childTabContainer", tabs.childTabs());
	        xmlDocument.setParameter("theme", vars.getTheme());
	        NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
	            "CheckPrintedCategory.html", classInfo.id, classInfo.type, strReplaceWith, tabs
	                .breadcrumb());
	        xmlDocument.setParameter("navigationBar", nav.toString());
	        LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CheckPrintedCategory.html",
	            strReplaceWith);
	        xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
	      } catch (Exception ex) {
	        throw new ServletException(ex);
	      }
	      xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
	      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
	      xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
	    // END generate and set UI elements

	    // BEGIN check for any pending message to be shown and clear it from the session
	      OBError myMessage = vars.getMessage("CheckPrintedCategory");
	      vars.removeMessage("CheckPrintedCategory");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }

	     if (strPDFFileName!=null){
	    	  String href="";
	    	  String strTempFileNames[]=strPDFFileName.split(",");

			    for(int i=0;i<strTempFileNames.length;i++){
			    	href=href+strDireccion + "/utility/DownloadReport.html?report=" + strTempFileNames[i]+",";
			    }

			    href=href.substring(0,href.length()-1);
			    log4j.info("pdf href. in check management screen...."+href);

			    xmlDocument.setParameter("href", href);
	    } else
	    	  xmlDocument.setParameter("href", "");

	      xmlDocument.setData("structure1", data);
	    // BEGIN render the output and close it so that it is returned to the user
	    out.println(xmlDocument.print());
	    out.close();
	    // END render the output and close it so that it is returned to the user
	  }



  private OBError CustomSuccessMsg(HttpServletResponse response, VariablesSecureApp vars,String strMessage,String strTitle,String strType) {

	    OBError myMessage = new OBError();
	    myMessage.setTitle(strTitle);
	    myMessage.setType(strType);
	    myMessage.setMessage(strMessage);
	    return myMessage;
	  }


  /**
   * Reinier Van Ommeren					30 January 2012
   *    Changes - when multipile strSelectedIds are passed in loop through and run each as a separate report.  Then accumulate the page
   *        and merge them into on common JasperPrint object.
   */
  protected String renderJRFile(VariablesSecureApp variables, HttpServletResponse response,
  	      String strReportName, String strOutputType, HashMap<String, Object> designParameters,
  	      FieldProvider[] data, Map<JRExporterParameter, Object> exportParameters) throws ServletException 
  {
       String strClassInfoID=variables.getSessionValue("classInfoId");
       String strPDFFileName=null;
       String strSelectedIds = ((String)designParameters.get("strSelectedIds"));
       Boolean multiPage = (designParameters != null  &&  designParameters.containsKey("MULTI_PAGE")  &&  ((String)designParameters.get("MULTI_PAGE")).equalsIgnoreCase("true"));

       if (strReportName == null || strReportName.equals(""))
       {
          strReportName = CheckManagementData.getReportName(this, strClassInfoID);
       }

  	    final String strAttach = globalParameters.strFTPDirectory + "/284-" + strClassInfoID;
  	    final String strLanguage = variables.getLanguage();
  	    final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

       final String strBaseDesign = getBaseDesignPath(strLanguage);
       strReportName = Replace.replace(Replace.replace(strReportName, "@basedesign@", strBaseDesign), "@attach@", strAttach);
       log4j.info("strReportName = " + strReportName);
       final String strFileName = strReportName.substring(strReportName.lastIndexOf("/") + 1);
       ServletOutputStream os = null;
       UUID reportId = null;

       try
       {
           final JasperReport jasperReport = Utility.getTranslatedJasperReport(this, strReportName,
                                                 strLanguage, strBaseDesign);

           if (designParameters == null)
           {
               designParameters = new HashMap<String, Object>();
           }

  	       Boolean pagination = true;

           if (strOutputType.equals("pdf"))
           {
                pagination = false;
           }

           designParameters.put("IS_IGNORE_PAGINATION", pagination);
           designParameters.put("BASE_WEB", strReplaceWithFull);
           designParameters.put("BASE_DESIGN", strBaseDesign);
           log4j.info("renderJRFile....strBaseDesign...."+strBaseDesign);
           designParameters.put("ATTACH", strAttach);
           designParameters.put("USER_CLIENT", Utility.getContext(this, variables, "#User_Client", ""));
           designParameters.put("USER_ORG", Utility.getContext(this, variables, "#User_Org", ""));
           designParameters.put("LANGUAGE", strLanguage);
           designParameters.put("LOCALE", locLocale);
           designParameters.put("REPORT_TITLE", "Check Printing");
  
           final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
           dfs.setDecimalSeparator(variables.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
           dfs.setGroupingSeparator(variables.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
           final DecimalFormat numberFormat = new DecimalFormat(variables.getSessionValue("#AD_ReportNumberFormat"), dfs);
 
           designParameters.put("NUMBERFORMAT", numberFormat);
 
           if (log4j.isDebugEnabled())
              log4j.debug("creating the format factory: " + variables.getJavaDateFormat());

           final JRFormatFactory jrFormatFactory = new JRFormatFactory();
           jrFormatFactory.setDatePattern(variables.getJavaDateFormat());
           designParameters.put(JRParameter.REPORT_FORMAT_FACTORY, jrFormatFactory);

           JasperPrint jasperPrint = new JasperPrint();
           Connection con = null;

           try 
           {
               con = getTransactionConnection();
               //mallik new changes begin..
               designParameters.put("REPORT_CONNECTION", con);
               designParameters.put("SUBREPORT_DIR", strBaseDesign+"//org//openbravo//localization//us//checkprinting//erpReports//");

               /* Removed by Reinier this was hard coded to pass along the sql script for report, but not needed anymore and
                * was not allowing a different .jrxml file to be defined in the UI.
                String strsubFileName = strBaseDesign+"//org//openbravo//localization//us//checkprinting//erpReports//Access-Sub.jrxml";
                JasperReport objJSubReport = JasperCompileManager.compileReport(strsubFileName);
                designParameters.put("Access-Sub", objJSubReport);
                //mallik ends
                */
               
     		  String strSelId="";
    		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
    		  
    		  /* loop through all id's and print each one, then put them together in same jasperPrint object */
              for(int i=0;i<strTemp.length;i++)
              {
    			  strSelId=strTemp[i];
    			  strSelId="("+strSelId+")";
    			  designParameters.put("strSelectedIds", strSelId);

                  if (i == 0) //first time around create jasperPrint...
                  {
                      jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters, con);
                  }
                  else //just append the results to jasperPrint obj...
                  {
                      JasperPrint jasperPrint2 = JasperFillManager.fillReport(jasperReport, designParameters, con);

                      /* loop through all pages and append them to the jasperPrint obj... */
                      for (int j=0; j < jasperPrint2.getPages().size(); j++)
                      {
                          jasperPrint.addPage((JRPrintPage)jasperPrint2.getPages().get(j));
                      }
    	          }
              } 
           }
           catch (final Exception e)
           {
              e.printStackTrace();
              throw new ServletException(e.getMessage(), e);
           }
           finally
           {
              releaseRollbackConnection(con);
           }

           if (exportParameters == null)
              exportParameters = new HashMap<JRExporterParameter, Object>();

           if (strOutputType == null || strOutputType.equals(""))
              strOutputType = "html";
           if (strOutputType.equals("html")) 
           {
              os = response.getOutputStream();
               
              if (log4j.isDebugEnabled())
                 log4j.debug("JR: Print HTML");

              response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "."
                                + strOutputType);
              final JRHtmlExporter exporter = new JRHtmlExporter();
              exportParameters.put(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
              exportParameters.put(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
              exportParameters.put(JRHtmlExporterParameter.SIZE_UNIT,
	       JRHtmlExporterParameter.SIZE_UNIT_POINT);
              exportParameters.put(JRHtmlExporterParameter.OUTPUT_STREAM, os);
              exporter.setParameters(exportParameters);
              exporter.exportReport();
              os.close();
           }
           else if (strOutputType.equals("pdf") || strOutputType.equalsIgnoreCase("xls"))
           {
              reportId = UUID.randomUUID();
              strPDFFileName=strFileName + "-" + (reportId) + "."+ strOutputType;
              saveReport(variables, jasperPrint, exportParameters, strFileName + "-" + (reportId) + "."
                          + strOutputType);
              response.setContentType("text/html;charset=UTF-8");
              response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "-"
                          + (reportId) + ".html");
           } 
           else
           {
               throw new ServletException("Output format no supported");
           }

  	    //JasperViewer.viewReport(jasperPrint,false);
  	      
  	    //  JasperPrintManager.printReport(jasperPrint, false);

  	      /*
  	    final JRPrintServiceExporter exporter = new JRPrintServiceExporter();
	  	exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	  	exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
	  	exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.TRUE);
	  	exporter.exportReport();*/

        } 
        catch (final JRException e) 
        {
           log4j.error("JR: Error: ", e);
           throw new ServletException(e.getMessage(), e);
         } 
         catch (IOException ioe)
         {
             try 
             {
                 FileUtility f = new FileUtility(globalParameters.strFTPDirectory, strFileName + "-"
                      + (reportId) + "." + strOutputType, false, true);
                 if (f.exists())
                   f.deleteFile();
             }
             catch (IOException ioex) 
             {
               log4j.error("Error trying to delete temporary report file " + strFileName + "-"
                   + (reportId) + "." + strOutputType + " : " + ioex.getMessage());
             }
         }
         catch (final Exception e)
         {
            e.printStackTrace();
           throw new ServletException(e.getMessage(), e);
         } 
         finally 
         {
            try
            {
               // os.close();
            } 
            catch (final Exception e)
            {
            }
         }

  	    return strPDFFileName;
  	  }


  private void saveReport(VariablesSecureApp vars, JasperPrint jp,
		  Map<JRExporterParameter, Object> exportParameters, String fileName) throws JRException {

	  final String outputFile = globalParameters.strFTPDirectory + "/" + fileName;
	  final String reportType = fileName.substring(fileName.lastIndexOf(".") + 1);
  	    if (reportType.equalsIgnoreCase("pdf")) {
  	      JasperExportManager.exportReportToPdfFile(jp, outputFile);
  	    } else if (reportType.equalsIgnoreCase("xls")) {
  	      JExcelApiExporter exporter = new JExcelApiExporter();
  	      exportParameters.put(JExcelApiExporterParameter.JASPER_PRINT, jp);
  	      exportParameters.put(JExcelApiExporterParameter.OUTPUT_FILE_NAME, outputFile);
  	      exportParameters.put(JExcelApiExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
  	      exportParameters.put(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
  	          Boolean.TRUE);
  	      exportParameters.put(JExcelApiExporterParameter.IS_DETECT_CELL_TYPE, true);
  	      exporter.setParameters(exportParameters);
  	      exporter.exportReport();
  	    } else {
  	      throw new JRException("Report type not supported");
  	    }

  	  }

  public int voidedChecks(ConnectionProvider connectionProvider,String selectedIds)  throws ServletException{

	    String strSql = "";
	    strSql = strSql +
	      "        UPDATE USLOCCP_CHECKS" +
	      "        SET isvoid = 'Y' , VOID_DATE=NOW() ,UPDATED=NOW() " + // fin_payment_id added by rohit
	      "        WHERE FIN_PAYMENT_ID IN "+selectedIds;
	    int updateCount = 0;
	    OBContext.setAdminMode();
	    log4j.info("voidedChecks sql......"+strSql);
	   
	    try {
		    updateCount= SessionHandler.getInstance().getSession()
		            .createSQLQuery(strSql)
		            .executeUpdate();
	    	
	    } catch(Exception e){
			 log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
		      e.printStackTrace();
		}finally{
		      OBContext.restorePreviousMode();
		}
	    return(updateCount);

  }

  public int voidedPaymentsChecks(ConnectionProvider connectionProvider,String selectedIds)  throws ServletException, SQLException{
	    String strSql = "";
	    strSql = strSql +
	      "UPDATE fin_payment " +
	      //"        SET status='PPPE' " + //commented by rohit and added the new line
	      "SET processed='Y' WHERE FIN_PAYMENT_ID in "+selectedIds;

	    log4j.info("voidedPaymentsChecks...processing...."+strSql);
	    int updateCount = 0;
	    try {
	    updateCount = SessionHandler.getInstance().getSession()
        .createSQLQuery(strSql)
        .executeUpdate();
	    } catch(Exception e){
	      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
	    }
	    return(updateCount);

}

//Rohit
  public int paymentTracProcessCancel(ConnectionProvider connectionProvider,String id,String tableName)  throws ServletException{

	    String strSql = "";
	    strSql = strSql +
	      "        UPDATE "+tableName+"	" +
	      "        SET processed='N' " +
	      "        WHERE "+tableName+"_ID = '"+id+"'";

	    log4j.info("paymentTracProcessCancel... sql..."+strSql);

	    int updateCount = 0;
	    try {
	    updateCount =  SessionHandler.getInstance().getSession()
	            .createSQLQuery(strSql)
	            .executeUpdate();
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	    }
	    return(updateCount);

}

//Rohit
  public int paymentProcessCancel(ConnectionProvider connectionProvider,String ids,String tableName)  throws ServletException{

	    String strSql = "";
	    strSql = strSql +
	      "        UPDATE "+tableName+"	" +
	      "        SET processed='N' " +
	      "        WHERE "+tableName+"_ID in "+ids;

	    log4j.info("paymentProcessCancel... sql..."+strSql);

	    int updateCount = 0;
	    try {
	    updateCount = SessionHandler.getInstance().getSession()
	            .createSQLQuery(strSql)
	            .executeUpdate();
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	    }
	    return(updateCount);

}
  /* Rohit
   */
  public int reprintedChecks(ConnectionProvider connectionProvider,String selectedIds)  throws ServletException{

	    String strSql = "";
	    strSql = strSql +
	      " UPDATE USLOCCP_CHECKS " +
	      " SET REPRINTED = 'Y' , REPRINTED_ID=CAST(REPRINTED_ID as integer) +1 ,UPDATED=NOW() ,DATE_LAST_REPRINT=NOW()" +
	      " WHERE FIN_PAYMENT_ID IN "+selectedIds;

	    int updateCount = 0;
	    try {
	    updateCount =  SessionHandler.getInstance().getSession()
	            .createSQLQuery(strSql)
	            .executeUpdate();
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	    return(updateCount);

}
  /* Rohit
   */
  
  public String isVoid(ConnectionProvider connectionProvider, String selectedIds)    throws ServletException {
	    String strSql = "";
	    strSql = strSql +
	      "        SELECT isvoid FROM USLOCCP_CHECKS WHERE FIN_PAYMENT_ID IN "+ selectedIds;

	    String strReturn = "";
	    char cVoid;
	    log4j.info("isVoid... sql..."+strSql);
	    try {
	    	Query q= SessionHandler.getInstance().getSession()
	            .createSQLQuery(strSql);
		    	  if (q.list().size() > 0) {
		    		  cVoid =(Character) q.list().get(0);
		    		  strReturn=String.valueOf(cVoid);
		    		   if(strReturn.equalsIgnoreCase("Y")){
		   	        	return(strReturn);
		    		   }
		    	  }
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	    return(strReturn);
	  }


  public String reIssueChecks(VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds,String strIntCheckNo,String strClient,String strAD_Org_ID,String strC_BPartner_ID){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFFileName="";
	  try{
		  //update the fin payment table for selected ids set the document num to the check number..
		  String strSelId="";
		  String strPayID="";
		  int strCheckNo=0;
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
		  strCheckNo=Integer.valueOf(strIntCheckNo);
		  String strSesICheck=(String) request.getSession().getAttribute("INVOICE_ON_CHECKS");
		  for(int i=0;i<strTemp.length;i++){
			  strSelId=strTemp[i];
			  strPayID=strTemp[0];
			  deleteVoidedChecks(this,strSelId);
			  CheckPrintingCategory.insertCheckDetails(this,strSelId,strCheckNo, strClient, strAD_Org_ID, strC_BPartner_ID,"N");
			  strCheckNo++;
		}

		  strPayID=strPayID.substring(1, strPayID.length()-1);
		  log4j.info("strPayID...."+strPayID);
		  final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,
				  strPayID);
		  log4j.info("finPayObj...."+finPayObj);
		  String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		  String strTemplateName=finPayObj.getAccount().getUsloccpTempname();

		  log4j.info("strTemplateLocation...."+strTemplateLocation);
		  log4j.info("strTemplateName...."+strTemplateName);

		  data = CheckPrintingCategoryData.getPrintPaymentDetails(this,strSelectedIds);

		  String strReportName=strTemplateLocation+"/"+strTemplateName;
		  HashMap<String, Object> designParameters = new HashMap<String, Object>();

		  designParameters.put("strIntCheckNo", strIntCheckNo);

		  log4j.info("printChecks strSelectedIds...."+strSelectedIds);

		  for(int i=0;i<strTemp.length;i++){
		  	  	strSelId=strTemp[i];
		  	  strSelId="("+strSelId+")";

			  designParameters.put("strSelectedIds", strSelId);
			  int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelId);
			  designParameters.put("paymentsCount", payCount);
			  log4j.info("paymentsCount...."+payCount);

			  String renderFile=renderJRFile(variables, response,strReportName,"pdf", designParameters,data,null);
			  strPDFFileName=strPDFFileName+renderFile+",";
			  log4j.info("printChecks strPDFFileName...."+strPDFFileName);
		    }

	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFFileName;

  }
  public int deleteVoidedChecks(ConnectionProvider connectionProvider,String selectedIds)  throws ServletException{

	    String strSql = "";
	    strSql = strSql +
	      "       delete from USLOCCP_CHECKS	" +
	      "        WHERE FIN_PAYMENT_ID = "+selectedIds;

	    int updateCount = 0;
	    try {
	    updateCount = SessionHandler.getInstance().getSession()
        .createSQLQuery(strSql)
        .executeUpdate();
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	    }
	    return(updateCount);

}
  //rohit
  public void clearPayments(ConnectionProvider connectionProvider,String selectedIds,VariablesSecureApp vars,AdvPaymentMngtDao dao)  throws ServletException {
	  String strSql = "";
	 strSql = strSql +

	 " select  " +
	 " i.c_invoice_id as invId, " +
	 " ps.fin_payment_schedule_id as fmsid, " +
	 " psd.fin_payment_scheduledetail_id as fisdid,  " +
	 " fpd.fin_payment_detail_id as fpdid, " +
	 " fft.fin_finacc_transaction_id as fftid, " +
	 " fpd.amount as amt, " +
	 " fp.writeoffamt as woamt, " +
	 " fp.fin_payment_id as finid,  " +
	 " fpp.fin_payment_proposal_id as fppid,   " +
	 " fppd.fin_payment_prop_detail_id as fppdid  " +
	 "    FROM fin_payment fp  " +
	 "    LEFT JOIN ad_client cl ON cl.ad_client_id = fp.ad_client_id " +
	 "    LEFT JOIN ad_org org ON org.ad_org_id = fp.ad_org_id " +
	 "    LEFT JOIN c_bpartner bp ON bp.c_bpartner_id = fp.c_bpartner_id " +
	 "    JOIN usloccp_checks cp ON cp.fin_payment_id = fp.fin_payment_id " +
	 "    JOIN ad_ref_list reflist ON reflist.value = fp.status " +
	 "    LEFT JOIN fin_financial_account fa ON fp.fin_financial_account_id = fa.fin_financial_account_id, fin_payment_detail fpd " +
	 "    JOIN fin_payment_scheduledetail psd ON fpd.fin_payment_detail_id = psd.fin_payment_detail_id " +
	 "    LEFT JOIN fin_payment_schedule ps ON psd.fin_payment_schedule_invoice = ps.fin_payment_schedule_id " +
	 "    LEFT JOIN c_invoice i ON ps.c_invoice_id = i.c_invoice_id " +
	 "    LEFT JOIN fin_payment_schedule pso ON psd.fin_payment_schedule_order = pso.fin_payment_schedule_id " +
	 "    LEFT JOIN c_order o ON pso.c_order_id = o.c_order_id " +
	 "    LEFT JOIN c_glitem gli ON fpd.c_glitem_id = gli.c_glitem_id " +
	 "    left join fin_finacc_transaction fft on fft.fin_payment_id=fpd.fin_payment_id  " +
	 "    left join fin_payment_prop_detail fppd on fppd.fin_payment_scheduledetail_id=psd.fin_payment_scheduledetail_id " + 
	 "    left join fin_payment_proposal fpp on fpp.fin_payment_proposal_id=fppd.fin_payment_proposal_id  " +
	 " WHERE fp.fin_payment_id = fpd.fin_payment_id and " +
	 " fp.fin_payment_id in "+selectedIds;

	 log4j.info("clearPayments SQL...."+strSql);

	    ResultSet result;
	    Statement st = null;
		String strfinid=null;

	    try {
	    	st = connectionProvider.getStatement();

	      result = st.executeQuery(strSql);

	      while(result.next()) {

	    	  String stramt=UtilSql.getValue(result, "amt").toString();
	    	  String strwoamt=UtilSql.getValue(result, "woamt").toString();
	    	  String strinvId=UtilSql.getValue(result, "invId");
	    	  strfinid=UtilSql.getValue(result, "finid");
	    	   log4j.info("strfinid...."+strfinid);
//
	    	  String strfisdid=UtilSql.getValue(result, "fisdid");
//	    	  //deletePayments(connectionProvider,strfisdid,"fin_payment_scheduledetail");
	    	  updateScheduleDetail(strfisdid,stramt,strwoamt);
//
//
	    	  String strfmsid=UtilSql.getValue(result, "fmsid");
//	    	  //deletePayments(connectionProvider,strfmsid,"fin_payment_schedule");
	    	  if(strfmsid!=null && !strfmsid.equalsIgnoreCase("")){
	    	  updateSchedule(strfmsid,stramt,strwoamt);
	    	  }
	    	  // rohit- changes to update order payment out details
	    	  if(strinvId!=null && !strinvId.equalsIgnoreCase("")){
	    		Invoice objInv = OBDal.getInstance().get(Invoice.class,
	    				strinvId);
	    		Order ObjOrdId=objInv.getSalesOrder();
	    		if(ObjOrdId==null){
	    			String strOrderLineId=getOrderLineFromInv(connectionProvider,strinvId);
	    			if(strOrderLineId!=null && !strOrderLineId.equalsIgnoreCase("")){
	    			OrderLine objOrderLine = OBDal.getInstance().get(OrderLine.class,
	    					strOrderLineId);
	    			ObjOrdId=objOrderLine.getSalesOrder();
	    			}
	    			
	    		}
	    		if(ObjOrdId!=null){
	    			String strOrdId=ObjOrdId.getId();
	    			log4j.info("ObjOrdId...."+ObjOrdId);
	    			String strOrdSchId=getScheduleId(connectionProvider,strOrdId);
	    			updateSchedule(strOrdSchId,stramt,strwoamt);
	    		}
	    	  
	    	  // rohit- changes to update order payment out details ends

	    	  updateInvoice(strinvId,stramt,strwoamt);
	    	  }

	    	  String strfppdid=UtilSql.getValue(result, "fppdid");
	    	  String strfppid=UtilSql.getValue(result, "fppid");
	    	  if(strfppdid!=null && !strfppdid.equalsIgnoreCase("")){
	    		  paymentTracProcessCancel(this,strfppid,"fin_payment_proposal");
	    		  deletePayments(connectionProvider,strfppdid,"fin_payment_prop_detail");
	    	  }

	    	  if(strfppid!=null && !strfppid.equalsIgnoreCase("")){

	    	  deletePayments(connectionProvider,strfppid,"fin_payment_proposal");
	    	  }
	    	  String strfftid=UtilSql.getValue(result, "fftid");
	    	  deleteFactReacords(connectionProvider,strfinid);
	    	  if(strfftid!=null && !strfftid.equalsIgnoreCase("")){
	    	  paymentTracProcessCancel(this,strfftid,"fin_finacc_transaction");
	    	  updateFinTranc(strfftid,stramt);
    		  
 	    	  deletePayments(connectionProvider,strfftid,"fin_finacc_transaction");
	    	  deleteFactReacords(connectionProvider,strfftid);
	    	  }
	    	  
	    	  //	updateScheduleDetail(strfisdid,"0",strwoamt);
	    	  //Update paid amt to zero in the fin_payment
	    	  //delete function




	      }
          //loop throgh finids..
	      //APRMVoid(connectionProvider,strfinid,vars);
	      log4j.info("selectedIds...."+selectedIds);
	      String subStringSelIds = selectedIds.substring(1, selectedIds.length()-1);
	      String selectedIdsWithoutQuotes = subStringSelIds.replace("'", "");
	      log4j.info("selectedIdsWithoutQuotes...."+selectedIdsWithoutQuotes);
	      //selectedIdsWithoutQuotes = selectedIdsWithoutQuotes.replaceAll(regex, replacement)
	      String selFinPayIds[] = selectedIdsWithoutQuotes.split(",");
    	  for(int k=0; k<selFinPayIds.length;k++)
    	  {
    		  log4j.info("Inside for loop for FinPayment APRM Void...."+selFinPayIds[k]);
    		  try{
    		  APRMVoid(connectionProvider,selFinPayIds[k].trim(),vars,dao);
    		  }catch (Exception e) {
    			log4j.error( "Exception:"+ e.getMessage());
				e.printStackTrace();
			}
    	  }
    	  for(int k=0; k<selFinPayIds.length;k++)
    	  {
    		  log4j.info("Inside for loop for FinPayment Canceled AMt to zero...."+selFinPayIds[k]);
    		  try{
    		  CanceledAmtToZero(connectionProvider,selFinPayIds[k].trim(),vars,dao);
    		  }catch (Exception e) {
  				e.printStackTrace();
  			}
    	  }
    	 

	      result.close();
	    } catch(SQLException e){
	      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releaseStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
  }

//Rohit
public void deletePayments(ConnectionProvider connectionProvider,String id,String tableName)  throws ServletException{

    String strSql = "";
    strSql = strSql +
      "       delete from "+ tableName +"" +
      "        WHERE "+tableName+"_ID = '"+id+"'";

    log4j.info("deletePayments in table "+tableName+"  SQL...."+strSql);

    try {
    SessionHandler.getInstance().getSession()
    .createSQLQuery(strSql)
    .executeUpdate();
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
    }


}

//Rohit
public void deleteFactReacords(ConnectionProvider connectionProvider,String id)  throws ServletException{

    String strSql = "";
    strSql = strSql +
      "       delete from fact_acct " +
      "        WHERE record_ID = '"+id+"'";

    log4j.info("deleteFactReacords in table "+id+"  SQL...."+strSql);

    try {
    SessionHandler.getInstance().getSession()
    .createSQLQuery(strSql)
    .executeUpdate();
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    }


}
//Rohit
public void updateScheduleDetail(String strScheduleDetailId,String stramt,String strwoamt){

    try {
    	OBContext.setAdminMode();

    	log4j.info("updateScheduleDetail strScheduleDetailId......"+strScheduleDetailId);
    	FIN_PaymentScheduleDetail objSD = OBDal.getInstance().get(FIN_PaymentScheduleDetail.class,
    			strScheduleDetailId);

    	BigDecimal bdamt= new BigDecimal (stramt);
    	BigDecimal bdwoamt= new BigDecimal (strwoamt);

    	log4j.info("bdamt...."+bdamt);
    	log4j.info("bdwoamt...."+bdwoamt);

    	objSD.setAmount(bdamt.add(bdwoamt));
    	objSD.setWriteoffAmount(objSD.getWriteoffAmount().subtract(bdwoamt));



		OBDal.getInstance().save(objSD);
		OBDal.getInstance().flush();
		OBDal.getInstance().getConnection().commit();


    } catch(Exception e){
      log4j.error("SQL error in query: Exception:"+ e);
    }finally{
    	OBContext.restorePreviousMode();
    }

}
//Rohit
public void updateSchedule(String strScheduleId,String stramt,String strwoamt){

    try {
    	OBContext.setAdminMode();
    	log4j.info("updateSchedule strScheduleId......"+strScheduleId);
    	FIN_PaymentSchedule objSchedule = OBDal.getInstance().get(FIN_PaymentSchedule.class,
    			strScheduleId);

	BigDecimal bdamt= new BigDecimal (stramt);
	BigDecimal bdwoamt= new BigDecimal (strwoamt);


	objSchedule.setPaidAmount(objSchedule.getPaidAmount().subtract(bdamt.add(bdwoamt)));
	objSchedule.setOutstandingAmount(objSchedule.getOutstandingAmount().add(bdamt.add(bdwoamt)));


	OBDal.getInstance().save(objSchedule);
	OBDal.getInstance().flush();
	OBDal.getInstance().getConnection().commit();

    } catch(Exception e){
      log4j.error("SQL error in query: Exception:"+ e);
    }finally{
    	OBContext.restorePreviousMode();
    }

}
//Rohit
public void updateInvoice(String strInvoiceId,String stramt,String strwoamt){

    try {
    	OBContext.setAdminMode();
    	log4j.info("updateInvoice strInvoiceId......"+strInvoiceId);
	Invoice objInv = OBDal.getInstance().get(Invoice.class,
			strInvoiceId);

	BigDecimal bdamt= new BigDecimal (stramt);
	BigDecimal bdwoamt= new BigDecimal (strwoamt);


	objInv.setPaymentComplete(false);
	objInv.setTotalPaid(objInv.getTotalPaid().subtract(bdamt.add(bdwoamt)));
	objInv.setOutstandingAmount(objInv.getOutstandingAmount().add(bdamt.add(bdwoamt)));
	objInv.setDueAmount(objInv.getDueAmount().add(bdamt.add(bdwoamt)));

	OBDal.getInstance().save(objInv);
	OBDal.getInstance().flush();
	OBDal.getInstance().getConnection().commit();

    } catch(Exception e){
      log4j.error("SQL error in query: Exception:"+ e);
    }finally{
    	OBContext.restorePreviousMode();
    }

}

//Rohit
public void updateFinTranc(String strFinTracId,String stramt){

    try {
    	OBContext.setAdminMode();
    	log4j.info("updateInvoice strFinTracId......"+strFinTracId);
    	FIN_FinaccTransaction objFinTranc = OBDal.getInstance().get(FIN_FinaccTransaction.class,
    			strFinTracId);
    	
    	
    	FIN_FinancialAccount objFinAcct = objFinTranc.getAccount();

	BigDecimal bdamt= new BigDecimal (stramt);

	objFinAcct.setCurrentBalance(objFinAcct.getCurrentBalance().add(bdamt));

	OBDal.getInstance().save(objFinAcct);
	OBDal.getInstance().flush();
	OBDal.getInstance().getConnection().commit();

    } catch(Exception e){
      log4j.error("SQL error in query: Exception:"+ e);
    }finally{
    	OBContext.restorePreviousMode();
    }

}
  
private void undoUsedCredit(FIN_Payment myPayment, VariablesSecureApp vars,
	      Set<String> invoiceDocNos) {
	    final List<FIN_Payment> payments = new ArrayList<FIN_Payment>();
	    for (final FIN_Payment_Credit pc : myPayment.getFINPaymentCreditList()) {
	      final FIN_Payment creditPaymentUsed = pc.getCreditPaymentUsed();
	      creditPaymentUsed.setUsedCredit(creditPaymentUsed.getUsedCredit().subtract(pc.getAmount()));
	      payments.add(creditPaymentUsed);
	    }

	    for (final FIN_Payment payment : payments) {
	      // Update Description
	      final String payDesc = payment.getDescription();
	      if (payDesc != null) {
	        final String invoiceDocNoMsg = Utility.messageBD(new DalConnectionProvider(),
	            "APRM_CreditUsedinInvoice", vars.getLanguage());
	        if (invoiceDocNoMsg != null) {
	          final StringBuffer newDesc = new StringBuffer();
	          for (final String line : payDesc.split("\n")) {
	            boolean include = true;
	            if (line.startsWith(invoiceDocNoMsg.substring(0, invoiceDocNoMsg.lastIndexOf("%s")))) {
	              for (final String docNo : invoiceDocNos) {
	                if (line.indexOf(docNo) > 0) {
	                  include = false;
	                  break;
	                }
	              }
	            }
	            if (include) {
	              newDesc.append(line);
	              if (!"".equals(line))
	                newDesc.append("\n");
	            }
	          }
	          payment.setDescription(newDesc.toString());
	        }
	      }
	    }
	  }
	  
public void APRMVoid(ConnectionProvider conProvider,String paymentId,VariablesSecureApp vars,AdvPaymentMngtDao dao){
    OBContext.setAdminMode();
    try {
    	log4j.info("Inside APRM Void paymentId 1...."+paymentId);
    	
    	final FIN_Payment payment = dao.getObject(FIN_Payment.class, paymentId);
    	//final VariablesSecureApp vars = bundle.getContext().toVars();
    	log4j.info("Inside APRM Void paymentId 2...."+paymentId);
    	
    	if (payment.isProcessed()) {
    		log4j.info("Inside APRM Void paymentId 3...."+paymentId);
       
        /*
         * Void the payment
         */
        payment.setStatus("RPVOID");

        /*
         * Cancel all payment schedule details related to the payment
         */
        final List<FIN_PaymentScheduleDetail> removedPDS = new ArrayList<FIN_PaymentScheduleDetail>();
        Set<String> invoiceDocNos = new HashSet<String>();
        for (final FIN_PaymentDetail paymentDetail : payment.getFINPaymentDetailList()) {
        	log4j.info("First for loop.... Fin Payment Detail Objects");
          for (final FIN_PaymentScheduleDetail paymentScheduleDetail : paymentDetail
              .getFINPaymentScheduleDetailList()) {
        	  log4j.info("Second for loop.... Fin Payment Schedule Detail Objects");
            BigDecimal outStandingAmt = BigDecimal.ZERO;
            log4j.info("paymentScheduleDetail ID...."+paymentScheduleDetail.getId());
            if (paymentScheduleDetail.getInvoicePaymentSchedule() != null) {
              // Related to invoices
            	log4j.info("Invoice Payment Schedule not null....");
              for (final FIN_PaymentScheduleDetail invScheDetail : paymentScheduleDetail
                  .getInvoicePaymentSchedule()
                  .getFINPaymentScheduleDetailInvoicePaymentScheduleList()) {
            	  log4j.info("FINPaymentScheduleDetailInvoicePaymentScheduleList....."+paymentScheduleDetail
                          .getInvoicePaymentSchedule()
                          .getFINPaymentScheduleDetailInvoicePaymentScheduleList().size());
                if (invScheDetail.getPaymentDetails() == null) {
                	log4j.info("outStandingAmt0...."+outStandingAmt);
                  outStandingAmt = outStandingAmt.add(invScheDetail.getAmount()).add(
                      invScheDetail.getWriteoffAmount());
                  removedPDS.add(invScheDetail);
                } else if (invScheDetail.equals(paymentScheduleDetail)) {
                  
                  outStandingAmt = outStandingAmt.add(invScheDetail.getAmount()).add(
                      invScheDetail.getWriteoffAmount());
                  log4j.info("outStandingAmt1...."+outStandingAmt);
                  paymentScheduleDetail.setCanceled(true);
                  //paymentScheduleDetail.setAmount(BigDecimal.ZERO);
                }
                invoiceDocNos.add(paymentScheduleDetail.getInvoicePaymentSchedule()
                    .getInvoice().getDocumentNo());
              }
              // Create merged Payment Schedule Detail with the pending to be paid amount
              if (outStandingAmt.compareTo(BigDecimal.ZERO) != 0) {
                final FIN_PaymentScheduleDetail mergedScheduleDetail = dao
                    .getNewPaymentScheduleDetail(payment.getOrganization(), outStandingAmt);
                mergedScheduleDetail.setInvoicePaymentSchedule(paymentScheduleDetail
                    .getInvoicePaymentSchedule());
                OBDal.getInstance().save(mergedScheduleDetail);
              }
            } else if (paymentScheduleDetail.getOrderPaymentSchedule() != null) {
            	log4j.info("Order Payment Schedule not null....");
              // Related to orders
              for (final FIN_PaymentScheduleDetail ordScheDetail : paymentScheduleDetail
                  .getOrderPaymentSchedule()
                  .getFINPaymentScheduleDetailOrderPaymentScheduleList()) {
                if (ordScheDetail.getPaymentDetails() == null) {
                  outStandingAmt = outStandingAmt.add(ordScheDetail.getAmount()).add(
                      ordScheDetail.getWriteoffAmount());
                  removedPDS.add(ordScheDetail);
                } else if (ordScheDetail.equals(paymentScheduleDetail)) {
                	
                  outStandingAmt = outStandingAmt.add(ordScheDetail.getAmount()).add(
                      ordScheDetail.getWriteoffAmount());
                  log4j.info("outStandingAmt2...."+outStandingAmt);
                  paymentScheduleDetail.setCanceled(true);
                  //paymentScheduleDetail.setAmount(BigDecimal.ZERO);
                }
              }
              // Create merged Payment Schedule Detail with the pending to be paid amount
              if (outStandingAmt.compareTo(BigDecimal.ZERO) != 0) {
            	  log4j.info("outStandingAmt3...."+outStandingAmt);
                final FIN_PaymentScheduleDetail mergedScheduleDetail = dao
                    .getNewPaymentScheduleDetail(payment.getOrganization(), outStandingAmt);
                mergedScheduleDetail.setOrderPaymentSchedule(paymentScheduleDetail
                    .getOrderPaymentSchedule());
                OBDal.getInstance().save(mergedScheduleDetail);
              }
            } else if (paymentDetail.getGLItem() != null) {
            	log4j.info("GL Item not null....");
              paymentScheduleDetail.setCanceled(true);
              log4j.info("BigDecimal.ZERO...."+BigDecimal.ZERO);
              paymentScheduleDetail.setAmount(BigDecimal.ZERO);
            } else if (paymentScheduleDetail.getOrderPaymentSchedule() == null
                && paymentScheduleDetail.getInvoicePaymentSchedule() == null) {
            	log4j.info("Credit Payment....");
              // Credit payment
              payment.setGeneratedCredit(payment.getGeneratedCredit().subtract(
                  paymentScheduleDetail.getAmount()));
              removedPDS.add(paymentScheduleDetail);
            }

            OBDal.getInstance().save(payment);
            OBDal.getInstance().flush();
          }
         // log4j.info("Second for loop ends.... Fin Payment Schedule Detail Objects");
          paymentDetail.getFINPaymentScheduleDetailList().removeAll(removedPDS);
          for (FIN_PaymentScheduleDetail removedPD : removedPDS) {
        	  log4j.info("For each Fin Payment Schedule Detail Objects removed");
            if (removedPD.getOrderPaymentSchedule() != null) {
              removedPD.getOrderPaymentSchedule()
                  .getFINPaymentScheduleDetailOrderPaymentScheduleList().remove(removedPD);
              OBDal.getInstance().save(removedPD.getOrderPaymentSchedule());
            }
            if (removedPD.getInvoicePaymentSchedule() != null) {
              removedPD.getInvoicePaymentSchedule()
                  .getFINPaymentScheduleDetailInvoicePaymentScheduleList().remove(removedPD);
              OBDal.getInstance().save(removedPD.getInvoicePaymentSchedule());
            }
            OBDal.getInstance().remove(removedPD);
          }
        }
        if (payment.getGeneratedCredit().compareTo(BigDecimal.ZERO) == 0
            && payment.getUsedCredit().compareTo(BigDecimal.ZERO) == 1) {
          undoUsedCredit(payment, vars, invoiceDocNos);
        }
        payment.getFINPaymentCreditList().clear();
        payment.setUsedCredit(BigDecimal.ZERO);
      }
      OBDal.getInstance().flush();
     
    } finally {
      OBContext.restorePreviousMode();
    }
}

public void CanceledAmtToZero(ConnectionProvider conProvider,String paymentId,VariablesSecureApp vars,AdvPaymentMngtDao dao){
	OBContext.setAdminMode();
    try {
    	log4j.info("Inside APRM Void paymentId...."+paymentId);
    	//AdvPaymentMngtDao dao = new AdvPaymentMngtDao();
    	final FIN_Payment payment = dao.getObject(FIN_Payment.class, paymentId);
    	//final VariablesSecureApp vars = bundle.getContext().toVars();
    	
    	
    	if (payment.isProcessed()) {
       
       
     
        for (final FIN_PaymentDetail paymentDetail : payment.getFINPaymentDetailList()) {
        	log4j.info("First for loop.... Fin Payment Detail Objects");
          for (final FIN_PaymentScheduleDetail paymentScheduleDetail : paymentDetail
              .getFINPaymentScheduleDetailList()) {
        	  log4j.info("Second for loop.... Fin Payment Schedule Detail Objects");
           // BigDecimal outStandingAmt = BigDecimal.ZERO;
            log4j.info("paymentScheduleDetail ID...."+paymentScheduleDetail.getId());
            if (paymentScheduleDetail.isCanceled() == true) {
              // Related to invoices
            	log4j.info("Invoice Payment Schedule not null...."+paymentScheduleDetail.getId());
            	paymentScheduleDetail.setAmount(BigDecimal.ZERO);
            	OBDal.getInstance().save(paymentScheduleDetail);
              // Create merged Payment Schedule Detail with the pending to be paid amount
              
                /*final FIN_PaymentScheduleDetail mergedScheduleDetail = dao
                    .getNewPaymentScheduleDetail(payment.getOrganization(), outStandingAmt);
                mergedScheduleDetail.setInvoicePaymentSchedule(paymentScheduleDetail
                    .getInvoicePaymentSchedule());
                OBDal.getInstance().save(mergedScheduleDetail);*/
              
            }  

            OBDal.getInstance().save(payment);
            OBDal.getInstance().flush();
          }
         // log4j.info("Second for loop ends.... Fin Payment Schedule Detail Objects");
         
         
        }
        
        
      }
      OBDal.getInstance().flush();
    } finally {
      OBContext.restorePreviousMode();
    }
}

/* Rohit
 */
public String getScheduleId(ConnectionProvider connectionProvider, String strOrderId)    throws ServletException {
	    String strSql = "";
	    strSql = strSql +
	      " select fin_payment_schedule_id from fin_payment_schedule where c_order_id='"+strOrderId+"' ";
	    log4j.info("getScheduleId...."+strSql);
	    String strReturn = "";
	 
	    try {
	    	Query q= SessionHandler.getInstance().getSession()
		            .createSQLQuery(strSql);
			    	  if (q.list().size() > 0) 
			    		  strReturn =(String) q.list().get(0);
			    		
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	    return(strReturn);
	  }

/* Rohit
 */
public String getOrderLineFromInv(ConnectionProvider connectionProvider, String strInvoiceId)    throws ServletException {
	    String strSql = "";
	    strSql = strSql +
	      " select c_orderLine_id from c_invoiceline where c_invoice_id='"+strInvoiceId+"' ";
	    log4j.info("getOrderLineFromInv...."+strSql);
	    String strReturn = null;
	    try {
	    	Query q= SessionHandler.getInstance().getSession()
		            .createSQLQuery(strSql);
			    	  if (q.list().size() > 0) 
			    		  strReturn =(String) q.list().get(0);
			    		
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	    return(strReturn);
	  }


}
