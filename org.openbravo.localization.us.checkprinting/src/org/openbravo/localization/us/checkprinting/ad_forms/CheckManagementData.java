/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * Mallikarjun M
 ************************************************************************************
 */
package org.openbravo.localization.us.checkprinting.ad_forms;

import java.sql.*;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import javax.servlet.ServletException;

import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;

import java.util.*;

class CheckManagementData implements FieldProvider {
static Logger log4j = Logger.getLogger(CheckManagementData.class);
  private String InitRecordNumber="0";
  public String mproductid;
  public String adorgname;
  public String searchkey;
  public String name;
  public String categoryname;
  public String bpartnername;
  public String producttype;
  public String purchased;
  public String sold;
  public String rownum;
  public String amount;
  public String dateplanned;
  public String paymentid;
  public String partnerid;
  public int iCheckNo;
  public String checknumber;
  public String checkdate;
  public String payee;
  public String status;
  public String checkamount;
  public String orgname;
  public String clientname;
  public String isvoid;


  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("MPRODUCTID"))
      return mproductid;
    else if (fieldName.equalsIgnoreCase("BPARTNERNAME"))
      return bpartnername;
    else if (fieldName.equals("rownum"))
      return rownum;
    else if (fieldName.equals("amount"))
        return amount;
    else if (fieldName.equals("dateplanned"))
        return dateplanned;
    else if (fieldName.equals("paymentid"))
        return paymentid;
    else if (fieldName.equals("partnerid"))
        return partnerid;
    else if (fieldName.equals("iCheckNo"))
        return String.valueOf(iCheckNo);
    else if (fieldName.equals("checknumber"))
        return String.valueOf(checknumber);
    else if (fieldName.equals("checkdate"))
        return String.valueOf(checkdate);
    else if (fieldName.equals("payee"))
        return String.valueOf(payee);
    else if (fieldName.equals("status"))
        return String.valueOf(status);
    else if (fieldName.equals("checkamount"))
        return String.valueOf(checkamount);
    else if (fieldName.equals("orgname"))
        return String.valueOf(orgname);
    else if (fieldName.equals("clientname"))
        return String.valueOf(clientname);
    else if (fieldName.equals("isvoid"))
        return String.valueOf(isvoid);
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }

 }

  public static CheckManagementData[] set()    throws ServletException {
	  CheckManagementData objectCheckManagementData[] = new CheckManagementData[1];
	  objectCheckManagementData[0] = new CheckManagementData();
	  objectCheckManagementData[0].adorgname = "";
	  objectCheckManagementData[0].name = "";
	  objectCheckManagementData[0].bpartnername = "";
	 return objectCheckManagementData;
  }

  public static CheckManagementData[] reprintChecks(ConnectionProvider connectionProvider,String strCheckNoFromId,String  strCheckNoToId,String strClient,String strAD_Org_ID,String strCPFromDate,
		  String strCPToDate,String strInvoiceNo,String strC_BPartner_ID,String strBankAcc,String strOrderNo,String strPaymentNo,String strProposalNo)  throws ServletException {
	    return reprintChecks(connectionProvider,strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,
	    		  strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc, 0, 0, strOrderNo, strPaymentNo, strProposalNo);
	  }

  /*
   * Reinier Van Ommeren						SPI-1119			31 January 2011
   *   changes 
   */
  public static CheckManagementData[] reprintChecks(ConnectionProvider connectionProvider,String strCheckNoFromId,String  strCheckNoToId,String strClient,String strAD_Org_ID,String strCPFromDate,
		  String strCPToDate,String strInvoiceNo,String strC_BPartner_ID,String strBankAcc,int firstRegister, int numberRegisters,String strOrderNo,String strPaymentNo,String strProposalNo)  throws ServletException {
	  log4j.debug("reprintChecks....................."+strAD_Org_ID+"!!!!!!");
	  
	  String dateFormat = OBPropertiesProvider.getInstance().getOpenbravoProperties()
	  .getProperty("dateFormat.java");
	  
	 
	  
	  if(strAD_Org_ID != null && strAD_Org_ID.equals("0")){
    	  strAD_Org_ID=null;
      }
	  String strSql = "";
	    strSql = strSql +
	      "  select distinct cast(checknumber as int)as sortcolumn,checknumber,id,checkdate, payee, status,checkamount,orgname,clientname,isvoid" +
	      "  from  usloccp_checksearch_v where 1=1 and checkamount!=0 and C_BPARTNER_ID is not null " ;
	   
	      if((strCheckNoFromId.equals("")) && (!strCheckNoToId.equals(""))){
	    	strSql = strSql + " AND CAST(checknumber AS integer) ='"+strCheckNoToId+"'";//checknumber

	    }else if((!strCheckNoFromId.equals("")) && (strCheckNoToId.equals(""))){
	    	strSql = strSql + " AND CAST(checknumber AS integer) ='"+strCheckNoFromId+"'";//checknumber

	    }else if((!strCheckNoFromId.equals("")) && (!strCheckNoToId.equals(""))){
	    	strSql = strSql + " AND CAST(checknumber AS integer)  between '"+strCheckNoFromId+"' and '"+strCheckNoToId+"'";//checknumber
	    	
	    }
	      log4j.info("strCheckNoFromId..."+strCheckNoFromId);
	      log4j.info("strCheckNoToId..."+strCheckNoToId);
	      log4j.info("strCPFromDate..."+strCPFromDate);
	      log4j.info("strCPToDate..."+strCPToDate);
	    if((strCPFromDate.equals("")) && (!strCPToDate.equals(""))){
	    	strSql = strSql +" AND checkdate=to_date('"+strCPToDate+"', '"+dateFormat+"')";//checkdate'dd/mm/yyyy'
	    }else if((!strCPFromDate.equals("")) && (strCPToDate.equals(""))){
	    	strSql = strSql +" AND checkdate=to_date('"+strCPFromDate+"', '"+dateFormat+"')";//checkdate
	    }else if((!strCPFromDate.equals("")) && (!strCPToDate.equals(""))){
	    	strSql = strSql +" AND checkdate between to_date('"+strCPFromDate+"', '"+dateFormat+"') and to_date('"+strCPToDate+"', '"+dateFormat+"')";//checkdate
	    }
	    log4j.info("strClient..."+strClient);
	    log4j.info("strAD_Org_ID..."+strAD_Org_ID);
	    log4j.info("strInvoiceNo..."+strInvoiceNo);
	    log4j.info("strC_BPartner_ID..."+strC_BPartner_ID);
	    log4j.info("strBankAcc..."+strBankAcc);
	   strSql = strSql + ((strClient==null || strClient.equals(""))?"":" AND AD_CLIENT_ID= ? ");
	   strSql = strSql + ((strAD_Org_ID==null || strAD_Org_ID.equals("0"))?"":" and ad_org_id= ? ");
	   strSql = strSql + ((strInvoiceNo==null || strInvoiceNo.equals(""))?"":" AND C_INVOICE_ID= ? ");
	   strSql = strSql + ((strC_BPartner_ID==null || strC_BPartner_ID.equals(""))?"":" AND C_BPARTNER_ID= ? ");
	   strSql = strSql + ((strBankAcc==null || strBankAcc.equals(""))?"":"  and FIN_FINANCIAL_ACCOUNT_ID= ? ");

	   log4j.info("strOrderNo..."+strOrderNo);
	   log4j.info("strPaymentNo..."+strPaymentNo);
	   log4j.info("strProposalNo..."+strProposalNo);
	   
	   strSql = strSql + ((strOrderNo==null || strOrderNo.equals(""))?"":" AND C_order_ID= ? ");
	   strSql = strSql + ((strPaymentNo==null || strPaymentNo.equals(""))?"":" AND id= ? ");
	   strSql = strSql + ((strProposalNo==null || strProposalNo.equals(""))?"":" AND fin_payment_proposal_id= ? ");
	   strSql = strSql + " order by sortcolumn desc ";//checknumber asc

	    ResultSet result;
	    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
	    PreparedStatement st = null;

	    int iParameter = 0;
	    try {
	    	log4j.info("strSql.in check mangement reprint method.."+strSql);
	    	log4j.debug("strSql.in check mangement reprint method.."+strSql);
	    st = connectionProvider.getPreparedStatement(strSql);
	    if (strClient != null && !(strClient.equals(""))) {
	          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strClient);
	        }
	    if (strAD_Org_ID != null && !(strAD_Org_ID.equals(""))) {
	          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strAD_Org_ID);
	          log4j.debug("strAD_Org_ID........."+strAD_Org_ID);
	        }
	      if (strInvoiceNo != null && !(strInvoiceNo.equals(""))) {
	          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strInvoiceNo);
	        }
	      if (strC_BPartner_ID != null && !(strC_BPartner_ID.equals(""))) {
	          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strC_BPartner_ID);
	        }
	      if (strBankAcc != null && !(strBankAcc.equals(""))) {
	          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strBankAcc);
	        }

	      if (strOrderNo != null && !(strOrderNo.equals(""))) {
	          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strOrderNo);
	        }
	      if (strPaymentNo != null && !(strPaymentNo.equals(""))) {
	          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strPaymentNo);
	        }
	      if (strProposalNo != null && !(strProposalNo.equals(""))) {
	          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strProposalNo);
	        }

	      result = st.executeQuery();
	      long countRecord = 0;
	      long countRecordSkip = 1;
	      boolean continueResult = true;
	      while(countRecordSkip < firstRegister && continueResult) {
	        continueResult = result.next();
	        countRecordSkip++;
	      }
	      while(continueResult && result.next()) {
	        countRecord++;
	        CheckManagementData objectCheckManagementData = new CheckManagementData();
	        objectCheckManagementData.paymentid = UtilSql.getValue(result, "id");
	        objectCheckManagementData.checknumber = UtilSql.getValue(result, "checknumber");
	        objectCheckManagementData.payee = UtilSql.getValue(result, "payee");
	        objectCheckManagementData.checkdate = UtilSql.getDateValue(result, "checkdate", "MM/dd/yyyy");
	        objectCheckManagementData.status = UtilSql.getValue(result, "status");
	        objectCheckManagementData.checkamount = UtilSql.getValue(result, "checkamount");
	        objectCheckManagementData.isvoid = UtilSql.getValue(result, "isvoid");
	       /* int strOrgCount=CheckPrintingCategoryData.getOrgName(connectionProvider,UtilSql.getValue(result, "id"));
	        if(strOrgCount==0)
	        	objectCheckManagementData.orgname = UtilSql.getValue(result, "orgname");
		        else
		        	objectCheckManagementData.orgname = "";
		        */
	        
	        objectCheckManagementData.orgname = UtilSql.getValue(result, "orgname");
	        objectCheckManagementData.clientname = UtilSql.getValue(result, "clientname");

	        objectCheckManagementData.rownum = Long.toString(countRecord);
	        objectCheckManagementData.InitRecordNumber = Integer.toString(firstRegister);
	        vector.addElement(objectCheckManagementData);
	        if (countRecord >= numberRegisters && numberRegisters != 0) {
	          continueResult = false;
	        }
	      }
	      result.close();
	    } catch(SQLException e){
	      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    CheckManagementData objectCheckManagementData[] = new CheckManagementData[vector.size()];
	    vector.copyInto(objectCheckManagementData);
	    return(objectCheckManagementData);
  }

  public static String getReportName(ConnectionProvider connectionProvider, String adProcessId){
	  String strReturn = null;
	  OBContext.setAdminMode();
		try {
			 
		     final org.openbravo.model.ad.ui.Process objProcess = OBDal.getInstance().get(org.openbravo.model.ad.ui.Process.class,
		    		 adProcessId);
		     if(objProcess!=null){
		    	 strReturn =  objProcess.getJRTemplateName();
		     }
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			OBContext.restorePreviousMode();
		}
	    return(strReturn);
 }
  
  public static boolean getVoidExist(ConnectionProvider connectionProvider,String strPaymentId) throws ServletException {
		 boolean bFlag=false;
		  String strSql = "";
	 	  strSql = strSql +
	 			   "  select payment.id from usloccpchecks " +
	 			     " WHERE payment.id in "+strPaymentId+" AND isvoid=true ";
	 	 OBContext.setAdminMode();
		    try {
		    	Query q = OBDal.getInstance().getSession().createQuery(strSql);
		 	    log4j.info("getCheckExist......"+strSql);
		 	    	if(q.list()!=null && q.list().size()>0)
		 	    		bFlag=true;
	 	    } catch(Exception e){
				 log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
			      e.printStackTrace();
			    }  finally {
			      OBContext.restorePreviousMode();
			    }
	 	   log4j.info("checkNumberExist..bFlag..."+bFlag);
		  return bFlag;
	}
 
}