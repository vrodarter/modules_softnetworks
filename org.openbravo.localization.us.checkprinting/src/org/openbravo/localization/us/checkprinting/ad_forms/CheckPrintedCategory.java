/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * Mallikarjun M
 ************************************************************************************
 */

package org.openbravo.localization.us.checkprinting.ad_forms;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.view.JasperViewer;

import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.JRFieldProviderDataSource;
import org.openbravo.erpCommon.utility.JRFormatFactory;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.utils.FileUtility;
import org.openbravo.utils.Replace;
import org.openbravo.xmlEngine.XmlDocument;

public class CheckPrintedCategory extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("FIND")) {

        String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
            "CheckPrintingCategory|C_BPartner_ID");
        String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
            "CheckPrintingCategory|Ad_Org_ID");
        if (strAD_Org_ID == null || strAD_Org_ID == "")
          strAD_Org_ID = "0";
        String strCategory_ID = vars.getRequestGlobalVariable("inpmProductCategoryId",
            "CheckPrintingCategory|inpmProductCategoryId");
        String strProductType = vars.getRequestGlobalVariable("inpmProductType",
            "CheckPrintingCategory|inpmProductType");
        String strPurchasedSold = vars.getStringParameter("inpPurchasedSold", "A");

        String strClient = vars.getRequestGlobalVariable("inpmClientId",
        "CheckPrintingCategory|inpmClientId");
        String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
        "CheckPrintingCategory|inpmInvoiceNo");
        String strBankStmtName = vars.getRequestGlobalVariable("inpmBankstmtName",
        "CheckPrintingCategory|inpmBankstmtName");
        String strCashJourName = vars.getRequestGlobalVariable("inpmCashJorName",
        "CheckPrintingCategory|inpmCashJorName");
        String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
        "CheckPrintingCategory|inpmBankAcc");
        String strCashBook = vars.getRequestGlobalVariable("inpmCashBook",
        "CheckPrintingCategory|inpmCashBook");

        String strIntCheckNo =  vars.getRequestGlobalVariable("inpmCheckNumber",
        "CheckPrintingCategory|inpmCheckNumber");
        String strInvoiceCheck = vars.getStringParameter("inpInvoiceCheck");
  	  
        String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
        "CheckPrintingCategory|inpmOrderNo");
        log4j.debug("strOrderNo.."+strOrderNo);
        String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
        "CheckPrintingCategory|inpmPaymentNo");
        log4j.info("strPaymentNo.."+strPaymentNo);

        String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
        "CheckPrintingCategory|inpmProposalNo");
        log4j.info("strProposalNo.."+strProposalNo);
   
        printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strCategory_ID,
          strProductType, strPurchasedSold,strClient,strInvoiceNo,strCashJourName,
          strBankStmtName,strBankAcc,strCashBook,strIntCheckNo,strInvoiceCheck,strOrderNo,strPaymentNo,strProposalNo);

    }  else if (vars.commandIn("PRINT")) {
   	 String strCheckNoFromId = vars.getRequestGlobalVariable("inpmCheckNoFromId",
     "CheckManagement|inpmCheckNoFromId");

	 String strCheckNoToId = vars.getRequestGlobalVariable("inpmCheckNoToId",
     "CheckManagement|inpmCheckNoToId");

	 String strClient = vars.getRequestGlobalVariable("inpmClientId",
     "CheckManagement|inpmClientId");

	 String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
     "CheckManagement|Ad_Org_ID");
	 if (strAD_Org_ID == null || strAD_Org_ID == "")
         strAD_Org_ID = "0";

	 String strCPFromDate = vars.getRequestGlobalVariable("inpmCPFromDate",
     "CheckManagement|inpmCPFromDate");

 	String strCPToDate = vars.getRequestGlobalVariable("inpmCPToDate",
     "CheckManagement|inpmCPToDate");

 	 String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
     "CheckManagement|inpmInvoiceNo");

    String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
        "CheckManagement|C_BPartner_ID");

    String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
    "CheckManagement|inpmBankAcc");

	  String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);

	  log4j.info("strSelectedId.in checkprinted category."+strSelectedId);
	  String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
	     "CheckPrintingCategory|inpmOrderNo");
	  log4j.info("strOrderNo.."+strOrderNo);
	     String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
	     "CheckPrintingCategory|inpmPaymentNo");
	     log4j.info("strPaymentNo.."+strPaymentNo);

	     String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
	     "CheckPrintingCategory|inpmProposalNo");
	     log4j.info("strProposalNo.."+strProposalNo);

    if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
    	log4j.info("CheckPrintedCategory.java PRINT Call if block");
		  	String strPDFFileName=printChecks(this,vars,request,response,strSelectedId);
		  	String strMessage="If you want to reprint any check, select and click ok";
		  	String strTitle="Success";
		  	String strType="SUCCESS";
		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
		  	printedPageDataSheet(response,vars,strSelectedId,strPDFFileName);
			request.getSession().setAttribute("PrintedRecords", strSelectedId);
			request.getSession().setAttribute("strPDFFileName", strPDFFileName);
	  }else{
		  log4j.info("CheckPrintedCategory.java PRINT Call else block");
		  	String strMessage="Please select atleast one check to print";
			String strTitle="Error";
		  	String strType="ERROR";
		  	vars.setMessage("CheckManagement", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
		    reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

	  }

}  else if (vars.commandIn("DEFAULT", "PRINTED")) {

	   String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);
	   if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
		   log4j.info("CheckPrintedCategory.java PRINTED Call if block");
			String strPDFFileName=printedChecks(vars,request,response,strSelectedId);
			String strMessage="If you want to reprint any check, select and click ok";
			String strTitle="Success";
			String strType="SUCCESS";
			vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
			log4j.debug("PRINTED...strSelectedId.."+strSelectedId);
			printedPageDataSheet(response,vars,strSelectedId,strPDFFileName);
			request.getSession().setAttribute("PrintedRecords", strSelectedId);
			request.getSession().setAttribute("strPDFFileName", strPDFFileName);

 	  }else{
 		 log4j.info("CheckPrintedCategory.java PRINTED Call else block");
 		  	String strAlreadySelIds=(String)request.getSession().getAttribute("PrintedRecords");
 		  	String strMessage="Please select atleast one check to print";
			String strTitle="Error";
		  	String strType="ERROR";
		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));

		  	printedPageDataSheet(response,vars,strAlreadySelIds,null);
 	  }

   } else
      pageError(response);
  }

  private String  printedChecks(VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFFileName="";
	  try{
		  data = CheckPrintingCategoryData.getPrintPaymentDetails(this,strSelectedIds);
		
		  String strSelId="";
		 
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
		      
		    for(int i=0;i<strTemp.length;i++){
		  	  strSelId=strTemp[i];
		  	   strSelId=strSelId.substring(1, strSelId.length()-1);
		  	   break;
		    }
		 
		    final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,strSelId);
		    
		    String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		    String strTemplateName=finPayObj.getAccount().getUsloccpTempname();

		    log4j.info("strTemplateLocation...."+strTemplateLocation);
		    log4j.info("strTemplateName...."+strTemplateName);

		    
			 /* String strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/CheckPrinting.jrxml";
				String srtRDBMS=this.getRDBMS();

				if(srtRDBMS.equals("ORACLE")){
					strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/OracleCheckPrinting.jrxml";
				}else{
				    strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/PostGreCheckPrinting.jrxml";
				}*/
		   String strReportName=strTemplateLocation+"/"+strTemplateName;
		    for(int i=0;i<strTemp.length;i++){
		  	  	strSelId=strTemp[i];
		  	  strSelId="("+strSelId+")";
		  
			  HashMap<String, Object> designParameters = new HashMap<String, Object>();
			  designParameters.put("strSelectedIds", strSelId);
			  int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelId);
			  designParameters.put("paymentsCount", payCount);
			  log4j.info("paymentsCount...."+payCount);
			  
			  String renderFile=renderJRFile(variables, response,strReportName,"pdf", designParameters,data,null);
			  strPDFFileName=strPDFFileName+renderFile+",";
		    }
		    

	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFFileName;

  }


  /**
   * Reinier Van Ommeren					Issue SPI-1103 				30 January 2012
   *    Changes - Multiple check bug to where a PDF is created for each check.. This module looped through each selectedId and 
   *     ran a report for each one.  Now the entire list is passed onto the renderJRFile(). 
   */
  private String printChecks(ConnectionProvider connectionProvider,VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFFileName="";
	  try{

		  data = CheckPrintingCategoryData.getPrintPaymentDetails(connectionProvider,strSelectedIds);
		  String strSelId="";
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
		      
		    for(int i=0;i<strTemp.length;i++){
		  	  strSelId=strTemp[i];
		  	  strSelId=strSelId.substring(1, strSelId.length()-1);
		  	  break;
		    }
		   
		    final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,strSelId);
		    
		    String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		    String strTemplateName=finPayObj.getAccount().getUsloccpTempname();

		    log4j.info("strTemplateLocation...."+strTemplateLocation);
		    log4j.info("strTemplateName...."+strTemplateName);
/*
			  String strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/CheckPrinting.jrxml";
				String srtRDBMS=this.getRDBMS();

				if(srtRDBMS.equals("ORACLE")){
					strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/OracleCheckPrinting.jrxml";
				}else{
				    strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/PostGreCheckPrinting.jrxml";
				}*/
		    
			String strReportName=strTemplateLocation+"/"+strTemplateName;
			  
	        log4j.info("strSelectedIds...." + strSelectedIds);
	  	  	HashMap<String, Object> designParameters = new HashMap<String, Object>();
	  	  	designParameters.put("strSelectedIds", strSelectedIds);
		  	  	
	  	  	int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelectedIds);
	  	  	designParameters.put("paymentsCount", payCount);
            log4j.info("paymentsCount...."+payCount);
			  
	  	  	String renderFile=renderJRFile(variables, response,strReportName,"pdf", designParameters,data,null);
	  	    log4j.info("renderFile...."+renderFile);
		  	strPDFFileName=strPDFFileName+renderFile+",";
		  	log4j.info("strPDFFileName...."+strPDFFileName);
			    
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFFileName;

  }


  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strC_BPartner_ID, String strAD_Org_ID, String strCategory_ID, String strProductType,
      String strPurchasedSold,String strClient,String strInvoiceNo,String strCashJourName,String strBankStmtName, String strBankAcc,String strCashBook, String strIntCheckNo,String strInvoiceCheck,String strOrderNo,String strPaymentNo,String strProposalNo ) throws IOException, ServletException {
    log4j.debug("CheckPrintingCategory: Call to printPagedataSheet");
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();

    String discard[] = { };
    XmlDocument xmlDocument = null;
    CheckPrintingCategoryData[] data = null;

    if (strAD_Org_ID.equals("")) {
      xmlDocument = xmlEngine.readXmlTemplate(
          "org/openbravo/localization/us/checkprinting/ad_forms/CheckPrintingCategory", discard).createXmlDocument();
      // retrieve empty data set
      data = CheckPrintingCategoryData.set();
    } else {
      xmlDocument = xmlEngine.readXmlTemplate(
          "org/openbravo/localization/us/checkprinting/ad_forms/CheckPrintingCategory").createXmlDocument();
      data= CheckPrintingCategoryData.customSelectForPrintCheck(vars,this,vars.getLanguage(), Utility.getContext(this,
              vars, "#User_Client", "CheckPrintingCategory"), Utility.getContext(this, vars,
                      "#User_Org", "CheckPrintingCategory"), strC_BPartner_ID,strAD_Org_ID,strCategory_ID,strProductType,
                      strPurchasedSold,strClient,strInvoiceNo,strCashJourName,strBankStmtName,strBankAcc,strCashBook,strOrderNo,strPaymentNo,strProposalNo);
    }

    // BEGIN generate and set UI elements
      /*ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CheckPrintingCategory", false, "",
          "", "", false, "ad_forms", strReplaceWith, false, true);
      toolbar.prepareSimpleToolBarTemplate();
      xmlDocument.setParameter("toolbar", toolbar.toString());*/
      try {
        WindowTabs tabs = new WindowTabs(this, vars,
            "org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintingCategory");
        xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
        xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
        xmlDocument.setParameter("childTabContainer", tabs.childTabs());
        xmlDocument.setParameter("theme", vars.getTheme());
        NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
            "CheckPrintingCategory.html", classInfo.id, classInfo.type, strReplaceWith, tabs
                .breadcrumb());
        xmlDocument.setParameter("navigationBar", nav.toString());
        LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CheckPrintingCategory.html",
            strReplaceWith);
        xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
      xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
    // END generate and set UI elements

    // BEGIN check for any pending message to be shown and clear it from the session
      OBError myMessage = vars.getMessage("CheckPrintingCategory");
      vars.removeMessage("CheckPrintingCategory");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    // END check for any pending message to be shown and clear it from the session
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
            "AD_Org Security validation", Utility.getContext(this, vars, "#User_Org",
                "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                "CheckPrintingCategory"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
        xmlDocument.setData("NAME_reportOrganizationDropdown", "liststructure", comboTableData
            .select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
            "M_Product_Category_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                "CheckPrintingCategory"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
        xmlDocument.setData("NAME_reportProductCategoryDropdown", "liststructure", comboTableData
            .select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
            "M_Product_Category_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                "CheckPrintingCategory"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
        xmlDocument.setData("NAME_reportProductCategoryToSetDropdown", "liststructure",
            comboTableData.select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
        try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "LIST",
            "M_Product_ProductType", "M_Product_ProductType", "", Utility.getContext(this, vars,
                "#User_Org", "CheckPrintingCategory"), Utility.getContext(this, vars,
                "#User_Client", "CheckPrintingCategory"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
        xmlDocument.setData("NAME_reportProductType", "liststructure", comboTableData.select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
  
      try {
          ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
              "ad_client_id", "", "", Utility.getContext(this, vars, "#User_Org",
                  "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                  "CheckPrintingCategory"), 0);
          Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
          xmlDocument.setData("NAME_reportClient", "liststructure",
              comboTableData.select(false));
          comboTableData = null;
        } catch (Exception ex) {
          throw new ServletException(ex);
        }

        try {
            ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                "c_invoice_id", "", "", Utility.getContext(this, vars, "#User_Org",
                    "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                    "CheckPrintingCategory"), 0);
            Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
            xmlDocument.setData("NAME_reportInvoiceNo", "liststructure",
                comboTableData.select(false));
            comboTableData = null;
          } catch (Exception ex) {
            throw new ServletException(ex);
          }


          try {
              ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                  "C_BANKSTATEMENT_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                      "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                      "CheckPrintingCategory"), 0);
              Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
              xmlDocument.setData("NAME_reportBankStmtName", "liststructure",
                  comboTableData.select(false));
              comboTableData = null;
            } catch (Exception ex) {
              throw new ServletException(ex);
            }


            try {
                ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                    "c_cash_id", "", "", Utility.getContext(this, vars, "#User_Org",
                        "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                        "CheckPrintingCategory"), 0);
                Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                xmlDocument.setData("NAME_reportCashJourName", "liststructure",
                    comboTableData.select(false));
                comboTableData = null;
              } catch (Exception ex) {
                throw new ServletException(ex);
              }

              try {
                  ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                      "C_BANKSTATEMENT_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                          "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                          "CheckPrintingCategory"), 0);
                  Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                  xmlDocument.setData("NAME_reportBankAcc", "liststructure",
                      comboTableData.select(false));
                  comboTableData = null;
                } catch (Exception ex) {
                  throw new ServletException(ex);
                }

                try {
                    ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                        "C_CASHBOOK_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                            "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                            "CheckPrintingCategory"), 0);
                    Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                    xmlDocument.setData("NAME_reportCashBook", "liststructure",
                        comboTableData.select(false));
                    comboTableData = null;
                  } catch (Exception ex) {
                    throw new ServletException(ex);
                  }

    // BEGIN put parameter values back into the form so that they remain entered on reload
      xmlDocument.setParameter("NAME_paramBPartnerId", strC_BPartner_ID);
      final BusinessPartner objBPartner = OBDal.getInstance().get(BusinessPartner.class,
    			strC_BPartner_ID);
        String bPartnerDescription="";
        if(objBPartner!=null){
      	  bPartnerDescription=objBPartner.getName();
        }
        xmlDocument.setParameter("NAME_paramBPartnerDescription",bPartnerDescription);
      xmlDocument.setParameter("NAME_paramOrganizationId", strAD_Org_ID);
      xmlDocument.setParameter("NAME_paramProductCategoryId", strCategory_ID);
      xmlDocument.setParameter("NAME_paramProductType", strProductType);
      xmlDocument.setParameter("NAME_paramPurchased", strPurchasedSold);
      xmlDocument.setParameter("NAME_paramSold", strPurchasedSold);
      xmlDocument.setParameter("NAME_paramAny", strPurchasedSold);

      xmlDocument.setParameter("NAME_paramClient", strClient);
	  xmlDocument.setParameter("NAME_paramInvoiceNo", strInvoiceNo);
	  xmlDocument.setParameter("NAME_paramCashJourName", strCashJourName);
	  xmlDocument.setParameter("NAME_paramBankStmtName", strBankStmtName);
	  xmlDocument.setParameter("NAME_paramBankAcc", strBankAcc);
	  xmlDocument.setParameter("NAME_paramCashBook", strCashBook);
	  String  strIntCheckCount = "";
	  if(strIntCheckNo==""){
		  strIntCheckCount = CheckPrintingCategoryData.CheckCount(this);
		  int iNxtCheckNo=Integer.valueOf(strIntCheckCount)+1;
		  String strNxtCheckNo=iNxtCheckNo+"";
		  xmlDocument.setParameter("NAME_inpmCheckNumber", strNxtCheckNo);
	  }else{
		  strIntCheckCount = strIntCheckNo;
		  xmlDocument.setParameter("NAME_inpmCheckNumber", strIntCheckCount);
	  }
	 // END put parameter values back into the form so that they remain entered on reload

    xmlDocument.setData("structure1", data);

    // BEGIN render the output and close it so that it is returned to the user
    out.println(xmlDocument.print());
    out.close();
    // END render the output and close it so that it is returned to the user
  }

  public String getServletInfo() {
    return "CheckPrintingCategory Servlet. This Servlet was made by Rok Lenardic";
  }

  private void printedPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
	      String strSelectedId,String strPDFFileName ) throws IOException, ServletException {


	  log4j.info("CheckPrintedCategory: Call to printedPageDataSheet");

	    // BEGIN initialize response
	      response.setContentType("text/html; charset=UTF-8");
	      PrintWriter out = response.getWriter();
	    // END initialize response

	    String discard[] = { };
	    XmlDocument xmlDocument = null;
	    CheckPrintingCategoryData[] data = null;

	      xmlDocument = xmlEngine.readXmlTemplate(
	          "org/openbravo/localization/us/checkprinting/ad_forms/CheckPrintedCategory").createXmlDocument();
	      data= CheckPrintingCategoryData.customPrintedSelect(this,strSelectedId);

	    // BEGIN generate and set UI elements
	      ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CheckPrintedCategory", false, "",
	          "", "", false, "ad_forms", strReplaceWith, false, true);
	      toolbar.prepareSimpleToolBarTemplate();
	      xmlDocument.setParameter("toolbar", toolbar.toString());
	      try {
	        WindowTabs tabs = new WindowTabs(this, vars,
	            "org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintedCategory");
	        xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
	        xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
	        xmlDocument.setParameter("childTabContainer", tabs.childTabs());
	        xmlDocument.setParameter("theme", vars.getTheme());
	        NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
	            "CheckPrintedCategory.html", classInfo.id, classInfo.type, strReplaceWith, tabs
	                .breadcrumb());
	        xmlDocument.setParameter("navigationBar", nav.toString());
	        LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CheckPrintedCategory.html",
	            strReplaceWith);
	        xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
	      } catch (Exception ex) {
	        throw new ServletException(ex);
	      }
	      xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
	      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
	      xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
	    // END generate and set UI elements

	      OBError myMessage = vars.getMessage("CheckPrintedCategory");
	      vars.removeMessage("CheckPrintedCategory");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	      if (strPDFFileName!=null){
	    	  String href="";
	    	  String strTempFileNames[]=strPDFFileName.split(",");

			    for(int i=0;i<strTempFileNames.length;i++){
			    	href=href+strDireccion + "/utility/DownloadReport.html?report=" + strTempFileNames[i]+",";
			    }
			    
			    href=href.substring(0,href.length()-1);
			    log4j.info("pdf href..check printed category..."+href);
		
			    xmlDocument.setParameter("href", href);
	    }else
	    	  xmlDocument.setParameter("href", "");

	      xmlDocument.setData("structure1", data);
	    // BEGIN render the output and close it so that it is returned to the user
	    out.println(xmlDocument.print());
	    out.close();
	    // END render the output and close it so that it is returned to the user
	  }

  private OBError CustomSuccessMsg(HttpServletResponse response, VariablesSecureApp vars,String strMessage,String strTitle,String strType) {

	    OBError myMessage = new OBError();
	    myMessage.setTitle(strTitle);
	    myMessage.setType(strType);
	    myMessage.setMessage(strMessage);
	    return myMessage;
	  }


  /**
   * Reinier Van Ommeren					30 January 2012
   *    Changes - when multipile strSelectedIds are passed in loop through and run each as a separate report.  Then accumulate the page
   *        and merge them into on common JasperPrint object.
   */
  protected String renderJRFile(VariablesSecureApp variables, HttpServletResponse response,
  	      String strReportName, String strOutputType, HashMap<String, Object> designParameters,
  	      FieldProvider[] data, Map<JRExporterParameter, Object> exportParameters) throws ServletException 
  {
       String strClassInfoID=variables.getSessionValue("classInfoId");
       String strPDFFileName=null;
       String strSelectedIds = ((String)designParameters.get("strSelectedIds"));
       Boolean multiPage = (designParameters != null  &&  designParameters.containsKey("MULTI_PAGE")  &&  ((String)designParameters.get("MULTI_PAGE")).equalsIgnoreCase("true"));

       if (strReportName == null || strReportName.equals(""))
       {
          strReportName = CheckManagementData.getReportName(this, strClassInfoID);
       }

       final String strAttach = globalParameters.strFTPDirectory + "/284-" + strClassInfoID;
       final String strLanguage = variables.getLanguage();
       final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

       final String strBaseDesign = getBaseDesignPath(strLanguage);
       strReportName = Replace.replace(Replace.replace(strReportName, "@basedesign@", strBaseDesign), "@attach@", strAttach);
       log4j.info("strReportName = " + strReportName);
       final String strFileName = strReportName.substring(strReportName.lastIndexOf("/") + 1);
       ServletOutputStream os = null;
       UUID reportId = null;

       try
       {
           final JasperReport jasperReport = Utility.getTranslatedJasperReport(this, strReportName,
                                                 strLanguage, strBaseDesign);

           if (designParameters == null)
           {
               designParameters = new HashMap<String, Object>();
           }

  	       Boolean pagination = true;

           if (strOutputType.equals("pdf"))
           {
                pagination = false;
           }

           designParameters.put("IS_IGNORE_PAGINATION", pagination);
           designParameters.put("BASE_WEB", strReplaceWithFull);
           designParameters.put("BASE_DESIGN", strBaseDesign);
           log4j.info("renderJRFile....strBaseDesign...."+strBaseDesign);
           designParameters.put("ATTACH", strAttach);
           designParameters.put("USER_CLIENT", Utility.getContext(this, variables, "#User_Client", ""));
           designParameters.put("USER_ORG", Utility.getContext(this, variables, "#User_Org", ""));
           designParameters.put("LANGUAGE", strLanguage);
           designParameters.put("LOCALE", locLocale);
           designParameters.put("REPORT_TITLE", "Check Printing");
  
           final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
           dfs.setDecimalSeparator(variables.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
           dfs.setGroupingSeparator(variables.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
           final DecimalFormat numberFormat = new DecimalFormat(variables.getSessionValue("#AD_ReportNumberFormat"), dfs);
 
           designParameters.put("NUMBERFORMAT", numberFormat);
 
           if (log4j.isDebugEnabled())
              log4j.debug("creating the format factory: " + variables.getJavaDateFormat());

           final JRFormatFactory jrFormatFactory = new JRFormatFactory();
           jrFormatFactory.setDatePattern(variables.getJavaDateFormat());
           designParameters.put(JRParameter.REPORT_FORMAT_FACTORY, jrFormatFactory);

           JasperPrint jasperPrint = new JasperPrint();
           Connection con = null;

           try 
           {
               con = getTransactionConnection();
               //mallik new changes begin..
               designParameters.put("REPORT_CONNECTION", con);
               designParameters.put("SUBREPORT_DIR", strBaseDesign+"//org//openbravo//localization//us//checkprinting//erpReports//");

               /* Removed by Reinier this was hard coded to pass along the sql script for report, but not needed anymore and
                * was not allowing a different .jrxml file to be defined in the UI.
                String strsubFileName = strBaseDesign+"//org//openbravo//localization//us//checkprinting//erpReports//Access-Sub.jrxml";
                JasperReport objJSubReport = JasperCompileManager.compileReport(strsubFileName);
                designParameters.put("Access-Sub", objJSubReport);
                //mallik ends
                */
               
     		  String strSelId="";
    		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
    		  
    		  /* loop through all id's and print each one, then put them together in same jasperPrint object */
              for(int i=0;i<strTemp.length;i++)
              {
    			  strSelId=strTemp[i];
    			  strSelId="("+strSelId+")";
    			  designParameters.put("strSelectedIds", strSelId);

                  if (i == 0) //first time around create jasperPrint...
                  {
                      jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters, con);
                  }
                  else //just append the results to jasperPrint obj...
                  {
                      JasperPrint jasperPrint2 = JasperFillManager.fillReport(jasperReport, designParameters, con);

                      /* loop through all pages and append them to the jasperPrint obj... */
                      for (int j=0; j < jasperPrint2.getPages().size(); j++)
                      {
                          jasperPrint.addPage((JRPrintPage)jasperPrint2.getPages().get(j));
                      }
    	          }
              } 
           }
           catch (final Exception e)
           {
              e.printStackTrace();
              throw new ServletException(e.getMessage(), e);
           }
           finally
           {
              releaseRollbackConnection(con);
           }

           if (exportParameters == null)
              exportParameters = new HashMap<JRExporterParameter, Object>();

           if (strOutputType == null || strOutputType.equals(""))
              strOutputType = "html";
           if (strOutputType.equals("html")) 
           {
              os = response.getOutputStream();
               
              if (log4j.isDebugEnabled())
                 log4j.debug("JR: Print HTML");

              response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "."
                                + strOutputType);
              final JRHtmlExporter exporter = new JRHtmlExporter();
              exportParameters.put(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
              exportParameters.put(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
              exportParameters.put(JRHtmlExporterParameter.SIZE_UNIT, JRHtmlExporterParameter.SIZE_UNIT_POINT);
              exportParameters.put(JRHtmlExporterParameter.OUTPUT_STREAM, os);
              exporter.setParameters(exportParameters);
              exporter.exportReport();
              os.close();
           }
           else if (strOutputType.equals("pdf") || strOutputType.equalsIgnoreCase("xls"))
           {
              reportId = UUID.randomUUID();
              strPDFFileName=strFileName + "-" + (reportId) + "."+ strOutputType;
              saveReport(variables, jasperPrint, exportParameters, strFileName + "-" + (reportId) + "."
                          + strOutputType);
              response.setContentType("text/html;charset=UTF-8");
              response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "-"
                          + (reportId) + ".html");
           } 
           else
           {
               throw new ServletException("Output format no supported");
           }

  	    //JasperViewer.viewReport(jasperPrint,false);
  	      
  	    //  JasperPrintManager.printReport(jasperPrint, false);

  	      /*
  	    final JRPrintServiceExporter exporter = new JRPrintServiceExporter();
	  	exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
	  	exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
	  	exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.TRUE);
	  	exporter.exportReport();*/

        } 
        catch (final JRException e) 
        {
           log4j.error("JR: Error: ", e);
           throw new ServletException(e.getMessage(), e);
         } 
         catch (IOException ioe)
         {
             try 
             {
                 FileUtility f = new FileUtility(globalParameters.strFTPDirectory, strFileName + "-"
                      + (reportId) + "." + strOutputType, false, true);
                 if (f.exists())
                   f.deleteFile();
             }
             catch (IOException ioex) 
             {
               log4j.error("Error trying to delete temporary report file " + strFileName + "-"
                   + (reportId) + "." + strOutputType + " : " + ioex.getMessage());
             }
         }
         catch (final Exception e)
         {
            e.printStackTrace();
           throw new ServletException(e.getMessage(), e);
         } 
         finally 
         {
            try
            {
               // os.close();
            } 
            catch (final Exception e)
            {
            }
         }

        return strPDFFileName;
    }


  private void saveReport(VariablesSecureApp vars, JasperPrint jp,
		  Map<JRExporterParameter, Object> exportParameters, String fileName) throws JRException {

	  final String outputFile = globalParameters.strFTPDirectory + "/" + fileName;
	  log4j.info("in checkprintedcategory file savereport method..."+outputFile);
  	    final String reportType = fileName.substring(fileName.lastIndexOf(".") + 1);
  	    if (reportType.equalsIgnoreCase("pdf")) {
  	      JasperExportManager.exportReportToPdfFile(jp, outputFile);
  	    } else if (reportType.equalsIgnoreCase("xls")) {
  	      JExcelApiExporter exporter = new JExcelApiExporter();
  	      exportParameters.put(JExcelApiExporterParameter.JASPER_PRINT, jp);
  	      exportParameters.put(JExcelApiExporterParameter.OUTPUT_FILE_NAME, outputFile);
  	      exportParameters.put(JExcelApiExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
  	      exportParameters.put(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
  	          Boolean.TRUE);
  	      exportParameters.put(JExcelApiExporterParameter.IS_DETECT_CELL_TYPE, true);
  	      exporter.setParameters(exportParameters);
  	      exporter.exportReport();
  	    } else {
  	      throw new JRException("Report type not supported");
  	    }

  	  }

  private void reprintChecks(HttpServletResponse response, VariablesSecureApp vars,
		  String strCheckNoFromId,String  strCheckNoToId,String strClient,String strAD_Org_ID,String strCPFromDate,
		  String strCPToDate,String strInvoiceNo,String strC_BPartner_ID,String strBankAcc,String strOrderNo,String strPaymentNo,String strProposalNo) throws IOException, ServletException {


	  log4j.info("CheckManagement: Call to reprintChecks strAD_Org_ID.."+strAD_Org_ID);
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();

    String discard[] = { };
    XmlDocument xmlDocument = null;
    CheckManagementData[] data = null;

    if (strAD_Org_ID.equals("0")) {
      xmlDocument = xmlEngine.readXmlTemplate(
          "org/openbravo/localization/us/checkprinting/ad_forms/CheckManagement", discard).createXmlDocument();
      // retrieve empty data set
      data = CheckManagementData.set();
    } else {
      xmlDocument = xmlEngine.readXmlTemplate(
          "org/openbravo/localization/us/checkprinting/ad_forms/CheckManagement").createXmlDocument();
      data= CheckManagementData.reprintChecks(this,strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,
    		  strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);
    }

    // BEGIN generate and set UI elements
      ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CheckManagement", false, "",
          "", "", false, "ad_forms", strReplaceWith, false, true);
      toolbar.prepareSimpleToolBarTemplate();
      xmlDocument.setParameter("toolbar", toolbar.toString());
      try {
        WindowTabs tabs = new WindowTabs(this, vars,
            "org.openbravo.localization.us.checkprinting.ad_forms.CheckManagement");
        xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
        xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
        xmlDocument.setParameter("childTabContainer", tabs.childTabs());
        xmlDocument.setParameter("theme", vars.getTheme());
        NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
            "CheckManagement.html", classInfo.id, classInfo.type, strReplaceWith, tabs
                .breadcrumb());
        xmlDocument.setParameter("navigationBar", nav.toString());
        LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CheckManagement.html",
            strReplaceWith);
        xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
      xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
    // END generate and set UI elements

    // BEGIN check for any pending message to be shown and clear it from the session
      OBError myMessage = vars.getMessage("CheckManagement");
      vars.removeMessage("CheckManagement");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    // END check for any pending message to be shown and clear it from the session

    // BEGIN Organization dropdown - using tabledir reference
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
            "AD_Org Security validation", Utility.getContext(this, vars, "#User_Org",
                "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                "CheckManagement"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
        xmlDocument.setData("NAME_reportOrganizationDropdown", "liststructure", comboTableData
            .select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }


      try {

          ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
              "ad_client_id", "", "", Utility.getContext(this, vars, "#User_Org",
                  "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                  "CheckManagement"), 0);
          Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
          xmlDocument.setData("NAME_reportClient", "liststructure",
              comboTableData.select(false));
          comboTableData = null;
        } catch (Exception ex) {
          throw new ServletException(ex);
        }

        try {
            ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                "c_invoice_id", "", "", Utility.getContext(this, vars, "#User_Org",
                    "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                    "CheckManagement"), 0);
            Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
            xmlDocument.setData("NAME_reportInvoiceNo", "liststructure",
                comboTableData.select(false));
            comboTableData = null;
          } catch (Exception ex) {
            throw new ServletException(ex);
          }


              try {
                  ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                      "C_BANK_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                          "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                          "CheckManagement"), 0);
                  Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
                  xmlDocument.setData("NAME_reportBankAcc", "liststructure",
                      comboTableData.select(false));
                  comboTableData = null;
                } catch (Exception ex) {
                  throw new ServletException(ex);
                }
      xmlDocument.setParameter("NAME_paramBPartnerId", strC_BPartner_ID);
      final BusinessPartner objBPartner = OBDal.getInstance().get(BusinessPartner.class,
    			strC_BPartner_ID);
        String bPartnerDescription="";
        if(objBPartner!=null){
      	  bPartnerDescription=objBPartner.getName();
        }
        xmlDocument.setParameter("NAME_paramBPartnerDescription",bPartnerDescription);
      xmlDocument.setParameter("NAME_paramOrganizationId", strAD_Org_ID);

      xmlDocument.setParameter("NAME_paramClient", strClient);
	  xmlDocument.setParameter("NAME_paramInvoiceNo", strInvoiceNo);
	  xmlDocument.setParameter("NAME_paramBankAcc", strBankAcc);
	  xmlDocument.setParameter("NAME_paramCheckNoFrom", strCheckNoFromId);
	  xmlDocument.setParameter("NAME_paramCheckNoTo", strCheckNoToId);
	  xmlDocument.setParameter("NAME_paramCheckDateFrom", strCPFromDate);
	  xmlDocument.setParameter("NAME_paramCheckDateTo", strCPToDate);


	  xmlDocument.setData("structure1", data);

    // BEGIN render the output and close it so that it is returned to the user
    out.println(xmlDocument.print());
    out.close();
    // END render the output and close it so that it is returned to the user
  }

}
