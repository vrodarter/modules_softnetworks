/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * Mallikarjun M
 ************************************************************************************
 */
package org.openbravo.localization.us.checkprinting.ad_forms;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;

import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.JRFormatFactory;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.localization.us.checkprinting.dp.usloccpchecks;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.utils.FileUtility;
import org.openbravo.utils.Replace;
import org.openbravo.xmlEngine.XmlDocument;


public class CheckPrintingCategory extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    String strPDFFileName=null;
    String strPDFFile = null;

    if (vars.commandIn("DEFAULT", "FIND")) {
    	log4j.info("DEFAULT............");
        String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
            "CheckPrintingCategory|C_BPartner_ID");
        String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
            "CheckPrintingCategory|Ad_Org_ID");
        if (strAD_Org_ID == null || strAD_Org_ID == "")
          strAD_Org_ID = "0";
        String strCategory_ID = vars.getRequestGlobalVariable("inpmProductCategoryId",
            "CheckPrintingCategory|inpmProductCategoryId");
        String strProductType = vars.getRequestGlobalVariable("inpmProductType",
            "CheckPrintingCategory|inpmProductType");
       String strSesICheck=(String) request.getSession().getAttribute("INVOICE_ON_CHECKS");

       String strPurchasedSold = vars.getStringParameter("inpPurchasedSold", "N");
       if(strSesICheck!=null && !strSesICheck.equals(""))
    	   strPurchasedSold =strSesICheck;

        String strClient = vars.getRequestGlobalVariable("inpmClientId",
        "CheckPrintingCategory|inpmClientId");
        String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
        "CheckPrintingCategory|inpmInvoiceNo");
        log4j.info("strInvoiceNo.."+strInvoiceNo);
        String strBankStmtName = vars.getRequestGlobalVariable("inpmBankstmtName",
        "CheckPrintingCategory|inpmBankstmtName");
        String strCashJourName = vars.getRequestGlobalVariable("inpmCashJorName",
        "CheckPrintingCategory|inpmCashJorName");
        String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
        "CheckPrintingCategory|inpmBankAcc");
        String strCashBook = vars.getRequestGlobalVariable("inpmCashBook",
        "CheckPrintingCategory|inpmCashBook");

        String strIntCheckNo =  vars.getRequestGlobalVariable("inpmCheckNumber",
        "CheckPrintingCategory|inpmCheckNumber");

        String strInvoiceCheck = vars.getStringParameter("inpInvoiceCheck");

        String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
        "CheckPrintingCategory|inpmOrderNo");
        log4j.info("strOrderNo.."+strOrderNo);
        String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
        "CheckPrintingCategory|inpmPaymentNo");
        log4j.info("strPaymentNo.."+strPaymentNo);

        String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
        "CheckPrintingCategory|inpmProposalNo");
        log4j.info("strProposalNo.."+strProposalNo);

        String ORG=(String) request.getParameter("ORG");

    	String DOCNO=(String) request.getParameter("DOCNO");
    	String PAYDOCNO=(String) request.getParameter("PAYDOCNO");
    	String PAY_PROPOSAL_ID=(String) request.getParameter("PAY_PROPOSAL_ID");
    	String STRC_INVOICE_ID=(String) request.getParameter("STRC_INVOICE_ID");
    	String strPaymtId=(String) request.getParameter("PAYMENT_ID");
    	log4j.info("(String) request.getParameter.........."+(String) request.getParameter("PAYMENT_ID"));
    	String strPaymentId=null;
    	
    	if(ORG!=null && !ORG.equals("")){
    		strAD_Org_ID=ORG;
    	}
    	if(DOCNO!=null && !DOCNO.equals("")){
    		strPaymentNo=DOCNO;
    	}
    	if(PAYDOCNO!=null && !PAYDOCNO.equals("")){
    		strProposalNo=PAYDOCNO;
    		log4j.info("check printing ..PAYDOCNO."+PAYDOCNO);
    		vars.setSessionValue("PAY_PROPOSAL_ID", PAY_PROPOSAL_ID);
    	}

		if(STRC_INVOICE_ID!=null && !STRC_INVOICE_ID.equals("")){
    		strInvoiceNo=STRC_INVOICE_ID;
    	}
		log4j.info("strPaymtId.."+strPaymtId);
		log4j.info("strPaymtId.getAttribute..."+request.getSession().getAttribute("PAYMENT_ID"));
		if(strPaymtId!=null && !strPaymtId.equals("")){
			strPaymentId=strPaymtId;
    	}else{
    		strPaymentId=(String)request.getSession().getAttribute("PAYMENT_ID");
    	}
		request.getSession().setAttribute("PAYMENT_ID", null);
		 log4j.info("strPaymentId.."+strPaymentId);
        printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strCategory_ID,
          strProductType, strPurchasedSold,strClient,strInvoiceNo,strCashJourName,
          strBankStmtName,strBankAcc,strCashBook,strIntCheckNo,strInvoiceCheck,strOrderNo,strPaymentNo,strProposalNo,strPaymentId);



    } else if (vars.commandIn("PRINT")) {
    		log4j.info("PRINT....");
		  String strPrintInvOnChecks=vars.getStringParameter("inpPurchasedSold", "N");
		  request.getSession().setAttribute("INVOICE_ON_CHECKS", strPrintInvOnChecks);
        String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
            "CheckPrintingCategory|C_BPartner_ID");
        String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
            "CheckPrintingCategory|Ad_Org_ID");
        if (strAD_Org_ID == null || strAD_Org_ID == "")
          strAD_Org_ID = "0";
        String strCategory_ID = vars.getRequestGlobalVariable("inpmProductCategoryId",
            "CheckPrintingCategory|inpmProductCategoryId");
        String strProductType = vars.getRequestGlobalVariable("inpmProductType",
            "CheckPrintingCategory|inpmProductType");
        String strPurchasedSold = vars.getStringParameter("inpPurchasedSold", "A");

        String strClient = vars.getRequestGlobalVariable("inpmClientId",
        "CheckPrintingCategory|inpmClientId");
        String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
        "CheckPrintingCategory|inpmInvoiceNo");
        String strBankStmtName = vars.getRequestGlobalVariable("inpmBankstmtName",
        "CheckPrintingCategory|inpmBankstmtName");
        String strCashJourName = vars.getRequestGlobalVariable("inpmCashJorName",
        "CheckPrintingCategory|inpmCashJorName");
        String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
        "CheckPrintingCategory|inpmBankAcc");
        String strCashBook = vars.getRequestGlobalVariable("inpmCashBook",
        "CheckPrintingCategory|inpmCashBook");

    	  String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);

    	  String strIntCheckNo = vars.getRequestGlobalVariable("inpmCheckNumber",
          "CheckPrintingCategory|inpmCheckNumber");

    	  String strInvoiceCheck = vars.getStringParameter("inpInvoiceCheck");
    	  String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
          "CheckPrintingCategory|inpmOrderNo");
          log4j.info("strOrderNo.."+strOrderNo);
          String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
          "CheckPrintingCategory|inpmPaymentNo");
          log4j.info("strPaymentNo.."+strPaymentNo);
          log4j.info("strPaymentNo...."+strPaymentNo);

          String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
          "CheckPrintingCategory|inpmProposalNo");
          log4j.info("strProposalNo.."+strProposalNo);
    	  //print pdf for selected items
    	  //and also insert the new check number and selected item in CP_CHECKS table

    	  if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
    		  //check if given check number exist....
    		  log4j.info("CheckPrintingCategory.java PRINT Call if block");
    		  boolean bCheckNoExist=checkNumberExist(this,strIntCheckNo);
    		  	String strPaymentId=strSelectedId;
    		  	strPaymentId=strPaymentId.replaceAll("\\('", "");
    		  	strPaymentId=strPaymentId.replaceAll("'\\)", "");
    		// boolean paymentExist=CheckPrintingCategoryData.getCheckExist(this,strSelectedId);

    		  if(bCheckNoExist){
	    			String strMessage="Check number already exist";
	      			String strTitle="Error";
	      		  	String strType="ERROR";
	      		  	vars.setMessage("CheckPrintingCategory", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
	      		  	printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strCategory_ID,
	          	    strProductType, strPurchasedSold,strClient,strInvoiceNo,strCashJourName,
	          	    strBankStmtName,strBankAcc,strCashBook,strIntCheckNo,strInvoiceCheck,strOrderNo,strPaymentNo,strProposalNo,null);

    		  }

    		  else{
    			 
	    			strPDFFileName=printChecks(vars,request,response,strSelectedId,strIntCheckNo,strClient,strAD_Org_ID,strC_BPartner_ID);
	    			String strMessage="To Re-Print or Void any check(s), click 'Check Management'. "
	    					 +"To Print new check(s), click 'Check Printing'.";
	    			  	String strTitle="Check(s) Printed Successfully.";
	    			  	String strType="SUCCESS";	      		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
	      		  	printedPageDataSheet(response,vars,strSelectedId,strPDFFileName);
	      			request.getSession().setAttribute("PrintedRecords", strSelectedId);
	      			request.getSession().setAttribute("strPDFFileName", strPDFFileName);

    		  }

    	  }else{
    		  log4j.info("CheckPrintingCategory.java PRINT Call else block");
	    		  String strMessage="Please select atleast one check to print";
	    		  String strTitle="Error";
	    		  String strType="ERROR";
	    		  vars.setMessage("CheckPrintingCategory", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));

	        	  printPageDataSheet(response, vars, strC_BPartner_ID, strAD_Org_ID, strCategory_ID,
        	          strProductType, strPurchasedSold,strClient,strInvoiceNo,strCashJourName,
        	          strBankStmtName,strBankAcc,strCashBook,strIntCheckNo,strInvoiceCheck,strOrderNo,strPaymentNo,strProposalNo,null);

    	  }

   }  else if (vars.commandIn("PRINTED")) {
	   
	   log4j.info("PRINTED............");
	   String strPrintInvOnChecks=vars.getStringParameter("inpPurchasedSold", "N");
		  request.getSession().setAttribute("INVOICE_ON_CHECKS", strPrintInvOnChecks);
     String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
         "CheckPrintingCategory|C_BPartner_ID");
     String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
         "CheckPrintingCategory|Ad_Org_ID");
     if (strAD_Org_ID == null || strAD_Org_ID == "")
       strAD_Org_ID = "0";
     String strCategory_ID = vars.getRequestGlobalVariable("inpmProductCategoryId",
         "CheckPrintingCategory|inpmProductCategoryId");
     String strProductType = vars.getRequestGlobalVariable("inpmProductType",
         "CheckPrintingCategory|inpmProductType");
     String strPurchasedSold = vars.getStringParameter("inpPurchasedSold", "A");

     String strClient = vars.getRequestGlobalVariable("inpmClientId",
     "CheckPrintingCategory|inpmClientId");
     String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
     "CheckPrintingCategory|inpmInvoiceNo");
     String strBankStmtName = vars.getRequestGlobalVariable("inpmBankstmtName",
     "CheckPrintingCategory|inpmBankstmtName");
     String strCashJourName = vars.getRequestGlobalVariable("inpmCashJorName",
     "CheckPrintingCategory|inpmCashJorName");
     String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
     "CheckPrintingCategory|inpmBankAcc");
     String strCashBook = vars.getRequestGlobalVariable("inpmCashBook",
     "CheckPrintingCategory|inpmCashBook");
 	  String strIntCheckNo = vars.getRequestGlobalVariable("inpmCheckNumber",
       "CheckPrintingCategory|inpmCheckNumber");
 	  //print pdf for selected items

	   String strSelectedId = vars.getInStringParameter("inpProduct", IsIDFilter.instance);
	   if(!strSelectedId.equals(null) && !strSelectedId.equals("")){
		   log4j.info("CheckPrintingCategory.java PRINTED Call if block");
		    strPDFFile=printedChecks(vars,request,response,strSelectedId);
 		  	String strMessage="If you want to reprint any check, select and click ok";
		  	String strTitle="Success";
		  	String strType="SUCCESS";
		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars,strMessage,strTitle,strType));
		  	reprintedChecks(this,strSelectedId);
		  	printedPageDataSheet(response,vars,strSelectedId,strPDFFile);
 	    	request.getSession().setAttribute("PrintedRecords", strSelectedId);
 	    	
 	  }else{
 		 log4j.info("CheckPrintingCategory.java PRINTED Call else block");
 		  	String strAlreadySelIds=(String)request.getSession().getAttribute("PrintedRecords");
 		  	String strMessage="Please select atleast one check to print";
			String strTitle="Error";
		  	String strType="ERROR";
		  	vars.setMessage("CheckPrintedCategory", CustomSuccessMsg(response, vars, strMessage,strTitle,strType));
		  	request.getSession().setAttribute("PrintedRecords", strAlreadySelIds);
		  	printedPageDataSheet(response,vars,strAlreadySelIds,null);
 	  }

   } else if (vars.commandIn("CHKMGMT")) {
   	log4j.info("Inside default........");
	 String strCheckNoFromId = vars.getRequestGlobalVariable("inpmCheckNoFromId",
    "CheckManagement|inpmCheckNoFromId");

	 String strCheckNoToId = vars.getRequestGlobalVariable("inpmCheckNoToId",
    "CheckManagement|inpmCheckNoToId");

	 String strClient = vars.getRequestGlobalVariable("inpmClientId",
    "CheckManagement|inpmClientId");

	 String strAD_Org_ID = vars.getRequestGlobalVariable("inpadOrgId",
    "CheckManagement|Ad_Org_ID");

	 if(strAD_Org_ID==null || strAD_Org_ID.equals("")){
		strAD_Org_ID="0";
	}

	 String strCPFromDate = vars.getRequestGlobalVariable("inpmCPFromDate",
    "CheckManagement|inpmCPFromDate");

	 String strCPToDate = vars.getRequestGlobalVariable("inpmCPToDate",
    "CheckManagement|inpmCPToDate");

	 String strInvoiceNo = vars.getRequestGlobalVariable("inpmInvoiceNo",
    "CheckManagement|inpmInvoiceNo");

	 String strC_BPartner_ID = vars.getRequestGlobalVariable("inpcBpartnerId",
       "CheckManagement|C_BPartner_ID");

	 String strBankAcc = vars.getRequestGlobalVariable("inpmBankAcc",
   "CheckManagement|inpmBankAcc");

	 String strOrderNo = vars.getRequestGlobalVariable("inpmOrderNo",
   "CheckPrintingCategory|inpmOrderNo");

	 String strPaymentNo = vars.getRequestGlobalVariable("inpmPaymentNo",
   "CheckPrintingCategory|inpmPaymentNo");

	 String strProposalNo = vars.getRequestGlobalVariable("inpmProposalNo",
   "CheckPrintingCategory|inpmProposalNo");
	 
	 reprintChecks(response, vars, strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

   
   }else
      pageError(response);
  }

  private String  printedChecks(VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFName="";
	  try{
		  data = CheckPrintingCategoryData.getPrintPaymentDetails(this,strSelectedIds);
		  String strSelId="";
		  
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
		      
		    for(int i=0;i<strTemp.length;i++){
		  	   strSelId=strTemp[i];
		  	   strSelId=strSelId.substring(1, strSelId.length()-1);
		  	   break;
		    }
		 
		    final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,strSelId);
		    
		    String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		    String strTemplateName=finPayObj.getAccount().getUsloccpTempname();

		    log4j.info("strTemplateLocation...."+strTemplateLocation);
		    log4j.info("strTemplateName...."+strTemplateName);

		
		//  String strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/CheckPrinting.jrxml";
			
		    String strReportName=strTemplateLocation+"/"+strTemplateName;
		    String srtRDBMS=this.getRDBMS();

				/*if(srtRDBMS.equals("ORACLE")){
					strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/OracleCheckPrinting.jrxml";
				}else{
				    strReportName="@basedesign@/org/openbravo/localization/us/checkprinting/erpReports/PostGreCheckPrinting.jrxml";
				}*/
		    log4j.info("printedChecks strSelectedIds...."+strSelectedIds);
		
		  
		    for(int i=0;i<strTemp.length;i++){
		  	  	strSelId=strTemp[i];
		  	  strSelId="("+strSelId+")";
		  
			  HashMap<String, Object> designParameters = new HashMap<String, Object>();
			  designParameters.put("strSelectedIds", strSelId);
			  int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelId);
			  designParameters.put("paymentsCount", payCount);
			  log4j.info("paymentsCount...."+payCount);
			  
			  String renderFile=renderJRFile(this,variables,request, response,strReportName,"pdf", designParameters,data,null);
			  strPDFName=strPDFName+renderFile+",";
			  log4j.info("printedChecks strPDFName...."+strPDFName);
		    }
		    

	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFName;

  }


  /**
   * Reinier Van Ommeren					Issue SPI-1103 				30 January 2012
   *    Changes - Multiple check bug to where a PDF is created for each check.. This module looped through each selectedId and 
   *     ran a report for each one.  Now the entire list is passed onto the renderJRFile(). 
   */
  public String printChecks(VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds,String strIntCheckNo,String strClient,String strAD_Org_ID,String strC_BPartner_ID){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFFileName="";
	  try{
		  //update the fin payment table for selected ids set the document num to the check number..
		  String strSelId="";
		  String strPayID="";
		  int strCheckNo=0;
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
		  strCheckNo=Integer.valueOf(strIntCheckNo);
		  String strSesICheck=(String) request.getSession().getAttribute("INVOICE_ON_CHECKS");
		  for(int i=0;i<strTemp.length;i++){
			  strSelId=strTemp[i];
			  strPayID=strTemp[0];
			 // updataPayments(this,strSelId,strCheckNo);
			  insertCheckDetails(this,strSelId,strCheckNo, strClient, strAD_Org_ID, strC_BPartner_ID,strSesICheck);
			  strCheckNo++;
		}
		  
		  strPayID=strPayID.substring(1, strPayID.length()-1);
		  log4j.info("strPayID...."+strPayID);
		  final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,
				  strPayID);
		  log4j.info("finPayObj...."+finPayObj);
		  String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		  String strTemplateName=finPayObj.getAccount().getUsloccpTempname();
		  
		  log4j.info("strTemplateLocation...."+strTemplateLocation);
		  log4j.info("strTemplateName...."+strTemplateName);
		  
		  data = CheckPrintingCategoryData.getPrintPaymentDetails(this,strSelectedIds);
		  
			String strReportName=strTemplateLocation+"/"+strTemplateName;
			  
	        log4j.info("strSelectedIds...." + strSelectedIds);
	  	  	HashMap<String, Object> designParameters = new HashMap<String, Object>();
	  	  	designParameters.put("strSelectedIds", strSelectedIds);
		  	  	
	  	  	int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelectedIds);
	  	  	designParameters.put("paymentsCount", payCount);
            log4j.info("paymentsCount...."+payCount);
			  
            String renderFile=renderJRFile(this,variables,request, response,strReportName,"pdf", designParameters,data,null);
	  	    log4j.info("renderFile...."+renderFile);
		  	strPDFFileName=strPDFFileName+renderFile+",";
		  	log4j.info("strPDFFileName...."+strPDFFileName);
	
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFFileName;

  }


  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strC_BPartner_ID, String strAD_Org_ID, String strCategory_ID, String strProductType,
      String strPurchasedSold,String strClient,String strInvoiceNo,String strCashJourName,String strBankStmtName, String strBankAcc,String strCashBook, String strIntCheckNo,String strInvoiceCheck,String strOrderNo,String strPaymentNo,String strProposalNo,String strPaymentId ) throws IOException, ServletException {

	  log4j.info("printPageDataSheet............");
    log4j.debug("CheckPrintingCategory: Call to printPagedataSheet");
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();

    String discard[] = { };
    XmlDocument xmlDocument = null;
    CheckPrintingCategoryData[] data = null;

    if (strAD_Org_ID.equals("")) {
      xmlDocument = xmlEngine.readXmlTemplate(
          "org/openbravo/localization/us/checkprinting/ad_forms/CheckPrintingCategory", discard).createXmlDocument();
      data = CheckPrintingCategoryData.set();
    } else {
      xmlDocument = xmlEngine.readXmlTemplate(
          "org/openbravo/localization/us/checkprinting/ad_forms/CheckPrintingCategory").createXmlDocument();
      log4j.info("strPaymentId...."+strPaymentId);
	      if(strPaymentId!=null){
	    	  log4j.info("strPaymentId!=null............");
	    	  data= CheckPrintingCategoryData.getPrintPaymentDetails(this,"('"+strPaymentId+"')");  
	      }else{
	    	  log4j.info("inside custom select...."+strPaymentId);
	    	  log4j.info("inside custom select...."+strPaymentId);
	    	  // change in custome select by rohit.
	    	  data= CheckPrintingCategoryData.customSelectForPrintCheck(vars,this,vars.getLanguage(), Utility.getContext(this,
	              vars, "#User_Client", "CheckPrintingCategory"), Utility.getContext(this, vars,
	                      "#User_Org", "CheckPrintingCategory"), strC_BPartner_ID,strAD_Org_ID,strCategory_ID,strProductType,
	                      strPurchasedSold,strClient,strInvoiceNo,strCashJourName,strBankStmtName,strBankAcc,strCashBook,strOrderNo,strPaymentNo,strProposalNo);
	      }
		  CheckPrintingCategory.deleteCheckDetails(this);
		    
    }

    // BEGIN generate and set UI elements
  
      try {
        WindowTabs tabs = new WindowTabs(this, vars,
            "org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintingCategory");
        xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
        xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
        xmlDocument.setParameter("childTabContainer", tabs.childTabs());
        xmlDocument.setParameter("theme", vars.getTheme());
        NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
            "CheckPrintingCategory.html", classInfo.id, classInfo.type, strReplaceWith, tabs
                .breadcrumb());
        xmlDocument.setParameter("navigationBar", nav.toString());
        LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CheckPrintingCategory.html",
            strReplaceWith);
        xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
      xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
    // END generate and set UI elements

      OBError myMessage = vars.getMessage("CheckPrintingCategory");
      vars.removeMessage("CheckPrintingCategory");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
         try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
            "AD_Org Security validation", Utility.getContext(this, vars, "#User_Org",
                "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                "CheckPrintingCategory"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
        xmlDocument.setData("NAME_reportOrganizationDropdown", "liststructure", comboTableData
            .select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
    
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
            "M_Product_Category_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                "CheckPrintingCategory"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
        xmlDocument.setData("NAME_reportProductCategoryDropdown", "liststructure", comboTableData
            .select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
   
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
            "M_Product_Category_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                "CheckPrintingCategory"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
        xmlDocument.setData("NAME_reportProductCategoryToSetDropdown", "liststructure",
            comboTableData.select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
    
      try {
        ComboTableData comboTableData = new ComboTableData(vars, this, "LIST",
            "M_Product_ProductType", "M_Product_ProductType", "", Utility.getContext(this, vars,
                "#User_Org", "CheckPrintingCategory"), Utility.getContext(this, vars,
                "#User_Client", "CheckPrintingCategory"), 0);
        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
        xmlDocument.setData("NAME_reportProductType", "liststructure", comboTableData.select(false));
        comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }
   
      try {
          ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
              "ad_client_id", "", "", Utility.getContext(this, vars, "#User_Org",
                  "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                  "CheckPrintingCategory"), 0);
          Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
          xmlDocument.setData("NAME_reportClient", "liststructure",
              comboTableData.select(false));
          comboTableData = null;
        } catch (Exception ex) {
          throw new ServletException(ex);
        }


        try {
           	String strId = null;

        	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
        		strId = "7ACD326BEAE34F338343AD4FFE793F77";
        	}
        	else
        	{
        		strId = "8511593E88D24C00991B382D0CF49B9E";
        	}
            ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                "c_invoice_id", strId, "", Utility.getContext(this, vars, "#User_Org",
                    "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                    "CheckPrintingCategory"), 0);
            Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
            xmlDocument.setData("NAME_reportInvoiceNo", "liststructure",
                comboTableData.select(false));
            comboTableData = null;
          } catch (Exception ex) {
            throw new ServletException(ex);
          }


          try {
              ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                  "C_BANKSTATEMENT_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                      "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                      "CheckPrintingCategory"), 0);
              Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
              xmlDocument.setData("NAME_reportBankStmtName", "liststructure",
                  comboTableData.select(false));
              comboTableData = null;
            } catch (Exception ex) {
              throw new ServletException(ex);
            }


            try {
                ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                    "c_cash_id", "", "", Utility.getContext(this, vars, "#User_Org",
                        "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                        "CheckPrintingCategory"), 0);
                Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                xmlDocument.setData("NAME_reportCashJourName", "liststructure",
                    comboTableData.select(false));
                comboTableData = null;
              } catch (Exception ex) {
                throw new ServletException(ex);
              }

              try {
                  ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                      "FIN_FINANCIAL_ACCOUNT_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                          "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                          "CheckPrintingCategory"), 0);
                  Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                  xmlDocument.setData("NAME_reportBankAcc", "liststructure",
                      comboTableData.select(false));
                  comboTableData = null;
                } catch (Exception ex) {
                  throw new ServletException(ex);
                }

                try {
                    ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                        "C_CASHBOOK_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                            "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                            "CheckPrintingCategory"), 0);
                    Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                    xmlDocument.setData("NAME_reportCashBook", "liststructure",
                        comboTableData.select(false));
                    comboTableData = null;
                  } catch (Exception ex) {
                    throw new ServletException(ex);
                  }

                  //july 21st mallik begins
                  //this combo for order...
                  try {
                     	String strOrder_Id = null;

                    	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
                    		strOrder_Id = "FD620CDEFBF2438B932269EBC65D0825";
                    	}
                    	else
                    	{
                    		strOrder_Id = "2036A37E2A6C4BD2BCAACD3CCEB0F67C";
                    	}
                      ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                          "C_Order_ID", strOrder_Id, "", Utility.getContext(this, vars, "#User_Org",
                              "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                              "CheckPrintingCategory"), 0);
                      Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                      xmlDocument.setData("NAME_reportOrderNo", "liststructure",
                          comboTableData.select(false));
                      comboTableData = null;
                    } catch (Exception ex) {
                      throw new ServletException(ex);
                    }

                    try {
                     	String strPayment_Id = null;

                    	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
                    		strPayment_Id = "8A18129DBDF047AD9A93DC2C8AFFEFD4";
                    	}
                    	else
                    	{
                    		strPayment_Id = "503938BDB79F4B449C05AD88E3B2DFBA";
                    	}
                        ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                            "documentno", strPayment_Id, "", Utility.getContext(this, vars, "#User_Org",
                                "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                                "CheckPrintingCategory"), 0);
                        Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                        xmlDocument.setData("NAME_reportPaymentNo", "liststructure",
                            comboTableData.select(false));
                        comboTableData = null;
                      } catch (Exception ex) {
                        throw new ServletException(ex);
                      }

                      try {
                       	String strProposal_Id = null;

                    	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
                    		strProposal_Id = "00D5D95618EE4C70B9488BDA9696BECD";
                    	}
                    	else
                    	{
                    		strProposal_Id = "417E0A623BE04FBC95B73333655C5C3E";
                    	}
                          ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                              "documentno", strProposal_Id, "", Utility.getContext(this, vars, "#User_Org",
                                  "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                                  "CheckPrintingCategory"), 0);
                          Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                          xmlDocument.setData("NAME_reportProposalNo", "liststructure",
                              comboTableData.select(false));
                          comboTableData = null;
                        } catch (Exception ex) {
                          throw new ServletException(ex);
                        }



    // BEGIN put parameter values back into the form so that they remain entered on reload
      xmlDocument.setParameter("NAME_paramBPartnerId", strC_BPartner_ID);
      final BusinessPartner objBPartner = OBDal.getInstance().get(BusinessPartner.class,
    			strC_BPartner_ID);
        String bPartnerDescription="";
        if(objBPartner!=null){
      	  bPartnerDescription=objBPartner.getName();
        }
        xmlDocument.setParameter("NAME_paramBPartnerDescription",bPartnerDescription);
      xmlDocument.setParameter("NAME_paramOrganizationId", strAD_Org_ID);
      xmlDocument.setParameter("NAME_paramProductCategoryId", strCategory_ID);
      xmlDocument.setParameter("NAME_paramProductType", strProductType);
      xmlDocument.setParameter("NAME_paramPurchased", strPurchasedSold);
      xmlDocument.setParameter("NAME_paramSold", strPurchasedSold);
      xmlDocument.setParameter("NAME_paramAny", strPurchasedSold);

      xmlDocument.setParameter("NAME_paramClient", strClient);
	  xmlDocument.setParameter("NAME_paramInvoiceNo", strInvoiceNo);
	  xmlDocument.setParameter("NAME_paramCashJourName", strCashJourName);
	  xmlDocument.setParameter("NAME_paramBankStmtName", strBankStmtName);
	  xmlDocument.setParameter("NAME_paramBankAcc", strBankAcc);
	  xmlDocument.setParameter("NAME_paramCashBook", strCashBook);
	  xmlDocument.setParameter("NAME_paramOrderNo", strOrderNo);
	  xmlDocument.setParameter("NAME_paramPaymentNo", strPaymentNo);
	  String strPropId=(String)vars.getSessionValue("PAY_PROPOSAL_ID");
	  if(strPropId!=null && strPropId!=""){
		  xmlDocument.setParameter("NAME_paramProposalNo", strPropId);
		  vars.setSessionValue("PAY_PROPOSAL_ID", null);
	  }else{
		  xmlDocument.setParameter("NAME_paramProposalNo", strProposalNo);
	  }

	  xmlDocument.setParameter("inpInvoiceCheck", strInvoiceCheck);


	  String  strIntCheckCount = "";
		  strIntCheckCount = CheckPrintingCategoryData.CheckCount(this);
		  int iNxtCheckNo=Integer.valueOf(strIntCheckCount)+1;
		  String strNxtCheckNo=iNxtCheckNo+"";
		  xmlDocument.setParameter("NAME_inpmCheckNumber", strNxtCheckNo);
	
		  xmlDocument.setData("structure1", data);

    // BEGIN render the output and close it so that it is returned to the user
    out.println(xmlDocument.print());
    out.close();
    // END render the output and close it so that it is returned to the user
  }

  public String getServletInfo() {
    return "CheckPrintingCategory Servlet. This Servlet was made by   Mallikarjun M";
  } 


  private void printedPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
	      String strSelectedId,String strPDFFileName ) throws IOException, ServletException {


	    log4j.debug("CheckPrintedCategory: Call to printedPageDataSheet");
	    log4j.info("CheckPrintedCategory: Call to printedPageDataSheet");
	    // BEGIN initialize response
	      response.setContentType("text/html; charset=UTF-8");
	      PrintWriter out = response.getWriter();
	    // END initialize response

	    String discard[] = { };
	    XmlDocument xmlDocument = null;
	    CheckPrintingCategoryData[] data = null;

	      xmlDocument = xmlEngine.readXmlTemplate(
	          "org/openbravo/localization/us/checkprinting/ad_forms/CheckPrintedCategory").createXmlDocument();
	      data= CheckPrintingCategoryData.customPrintedSelect(this,strSelectedId);

	    // BEGIN generate and set UI elements
	      /*ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CheckPrintedCategory", false, "",
	          "", "", false, "ad_forms", strReplaceWith, false, true);
	      toolbar.prepareSimpleToolBarTemplate();
	      xmlDocument.setParameter("toolbar", toolbar.toString());*/
	      try {
	        WindowTabs tabs = new WindowTabs(this, vars,
	            "org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintedCategory");
	        xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
	        xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
	        xmlDocument.setParameter("childTabContainer", tabs.childTabs());
	        xmlDocument.setParameter("theme", vars.getTheme());
	        NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
	            "CheckPrintedCategory.html", classInfo.id, classInfo.type, strReplaceWith, tabs
	                .breadcrumb());
	        xmlDocument.setParameter("navigationBar", nav.toString());
	        LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CheckPrintedCategory.html",
	            strReplaceWith);
	        xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
	      } catch (Exception ex) {
	        throw new ServletException(ex);
	      }
	      xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
	      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
	      xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
	    // END generate and set UI elements

	    // BEGIN check for any pending message to be shown and clear it from the session
	      OBError myMessage = vars.getMessage("CheckPrintedCategory");
	      vars.removeMessage("CheckPrintedCategory");
	      if (myMessage != null) {
	        xmlDocument.setParameter("messageType", myMessage.getType());
	        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
	        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
	      }
	   	  
	      if (strPDFFileName!=null){
	    	  String href="";
	    	  String strTempFileNames[]=strPDFFileName.split(",");

			    for(int i=0;i<strTempFileNames.length;i++){
			    	href=href+strDireccion + "/utility/DownloadReport.html?report=" + strTempFileNames[i]+",";
			    }
			    
			    href=href.substring(0,href.length()-1);
			    log4j.info("pdf href..check printingcategory..."+href);
		
			    xmlDocument.setParameter("href", href);
	    } else
	    	  xmlDocument.setParameter("href", "");

	      xmlDocument.setData("structure1", data);
	    // BEGIN render the output and close it so that it is returned to the user
	    out.println(xmlDocument.print());
	    out.close();
	    // END render the output and close it so that it is returned to the user
	  }

  private OBError CustomSuccessMsg(HttpServletResponse response, VariablesSecureApp vars,String strMessage,String strTitle,String strType) {

	    OBError myMessage = new OBError();
	    myMessage.setTitle(strTitle);
	    myMessage.setType(strType);
	    myMessage.setMessage(strMessage);
	    return myMessage;
	  }


  protected String renderJRFile(ConnectionProvider connectionProvider,VariablesSecureApp variables,HttpServletRequest request, HttpServletResponse response,
  	      String strReportName, String strOutputType, HashMap<String, Object> designParameters,
  	      FieldProvider[] data, Map<JRExporterParameter, Object> exportParameters) throws ServletException {
  		  String strClassInfoID=variables.getSessionValue("classInfoId");
  		  log4j.info("strClassInfoID...."+strClassInfoID);
  	       String strSelectedIds = ((String)designParameters.get("strSelectedIds"));
  		  String strPDFFileName=null;
  	    if (strReportName == null || strReportName.equals(""))
  	      strReportName = CheckManagementData.getReportName(connectionProvider, strClassInfoID);

  	   String strAttach = "";
  	    if(globalParameters== null || globalParameters.equals("")){
  	    	strAttach=(String) request.getSession().getAttribute("STR_ATTACH")+ "/284-" + strClassInfoID;
  	    }else{
  	    	strAttach = globalParameters.strFTPDirectory + "/284-" + strClassInfoID;
  	    }

  	    final String strLanguage = variables.getLanguage();
  	    final Locale locLocale = new Locale(strLanguage.substring(0, 2), strLanguage.substring(3, 5));

  	    final String strBaseDesign = getBaseDesignPath(strLanguage);
  	    strReportName = Replace.replace(Replace.replace(strReportName, "@basedesign@", strBaseDesign),
  	        "@attach@", strAttach);
  	    final String strFileName = strReportName.substring(strReportName.lastIndexOf("/") + 1);
  	  log4j.info("strReportName......."+strReportName);
  	    log4j.info("strFileName......."+strFileName);
  	    ServletOutputStream os = null;
  	    UUID reportId = null;
  	    try {
  	      final JasperReport jasperReport = Utility.getTranslatedJasperReport(connectionProvider, strReportName,
  	          strLanguage, strBaseDesign);
  	      if (designParameters == null)
  	        designParameters = new HashMap<String, Object>();

  	      Boolean pagination = true;
  	      if (strOutputType.equals("pdf"))
  	        pagination = false;

  	      designParameters.put("IS_IGNORE_PAGINATION", pagination);
  	      designParameters.put("BASE_WEB", strReplaceWithFull);
  	      designParameters.put("BASE_DESIGN", strBaseDesign);
  	      designParameters.put("ATTACH", strAttach);
  	      designParameters.put("USER_CLIENT", Utility.getContext(connectionProvider, variables, "#User_Client", ""));
  	      designParameters.put("USER_ORG", Utility.getContext(connectionProvider, variables, "#User_Org", ""));
  	      designParameters.put("LANGUAGE", strLanguage);
  	      designParameters.put("LOCALE", locLocale);

  	    designParameters.put("REPORT_TITLE", "Check Printing");
  	      final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
  	      dfs.setDecimalSeparator(variables.getSessionValue("#AD_ReportDecimalSeparator").charAt(0));
  	      dfs.setGroupingSeparator(variables.getSessionValue("#AD_ReportGroupingSeparator").charAt(0));
  	      final DecimalFormat numberFormat = new DecimalFormat(variables
  	          .getSessionValue("#AD_ReportNumberFormat"), dfs);
  	      designParameters.put("NUMBERFORMAT", numberFormat);
  	      if (log4j.isDebugEnabled())
  	        log4j.debug("creating the format factory: " + variables.getJavaDateFormat());
  	      final JRFormatFactory jrFormatFactory = new JRFormatFactory();
  	      jrFormatFactory.setDatePattern(variables.getJavaDateFormat());
  	      designParameters.put(JRParameter.REPORT_FORMAT_FACTORY, jrFormatFactory);

  	      JasperPrint jasperPrint = new JasperPrint();
  	      Connection con = null;
  	      try {
  	        con = getTransactionConnection();
  	      //mallik new changes begin..
    	      designParameters.put("REPORT_CONNECTION", con);
    	      
    	      designParameters.put("SUBREPORT_DIR", strBaseDesign+"//org//openbravo//localization//us//checkprinting//erpReports//");

             
              
     		  String strSelId="";
    		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
    		  for(int i=0;i<strTemp.length;i++)
    		  {
    			  log4j.info("strSelectedIds new one by one ----"+strTemp[i]);
    		  }
    		  log4j.info("strSelectedIds length new ----"+strTemp.length);
    		  
    		  /* loop through all id's and print each one, then put them together in same jasperPrint object */
              for(int i=0;i<strTemp.length;i++)
              {
    			  strSelId=strTemp[i];
    			  strSelId="("+strSelId+")";
    			  designParameters.put("strSelectedIds", strSelId);

                  if (i == 0) //first time around create jasperPrint...
                  {
                      jasperPrint = JasperFillManager.fillReport(jasperReport, designParameters, con);
                  }
                  else //just append the results to jasperPrint obj...
                  {
                      JasperPrint jasperPrint2 = JasperFillManager.fillReport(jasperReport, designParameters, con);

                      /* loop through all pages and append them to the jasperPrint obj... */
                      for (int j=0; j < jasperPrint2.getPages().size(); j++)
                      {
                          jasperPrint.addPage((JRPrintPage)jasperPrint2.getPages().get(j));
                      }
    	          }
              } 
    	      
  	      } catch (final Exception e) {
  	        throw new ServletException(e.getMessage(), e);
  	      } finally {
  	        releaseRollbackConnection(con);
  	      }
  	   
  	      if (exportParameters == null)
  	        exportParameters = new HashMap<JRExporterParameter, Object>();
  	      if (strOutputType == null || strOutputType.equals(""))
  	        strOutputType = "html";
  	      if (strOutputType.equals("html")) {
  	    	  os = response.getOutputStream();
  	        if (log4j.isDebugEnabled())
  	          log4j.debug("JR: Print HTML");
  	        response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "."
  	            + strOutputType);
  	        final JRHtmlExporter exporter = new JRHtmlExporter();
  	        exportParameters.put(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
  	        exportParameters.put(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
  	        exportParameters.put(JRHtmlExporterParameter.SIZE_UNIT,
  	            JRHtmlExporterParameter.SIZE_UNIT_POINT);
  	        exportParameters.put(JRHtmlExporterParameter.OUTPUT_STREAM, os);
  	        exporter.setParameters(exportParameters);
  	        exporter.exportReport();
  	      os.close();
  	      } else if (strOutputType.equals("pdf") || strOutputType.equalsIgnoreCase("xls")) {
  	        reportId = UUID.randomUUID();
  	        strPDFFileName=strFileName + "-" + (reportId) + "."+ strOutputType;
  	        saveReport(request,variables, jasperPrint, exportParameters, strFileName + "-" + (reportId) + "."
  	            + strOutputType);
  	        response.setContentType("text/html;charset=UTF-8");
  	        response.setHeader("Content-disposition", "inline" + "; filename=" + strFileName + "-"
  	            + (reportId) + ".html");
  	      } else {
  	        throw new ServletException("Output format no supported");
  	      }
  	      
  	    //JasperViewer.viewReport(jasperPrint,false);
  	 // JasperPrintManager.printReport(jasperPrint, false);
  	    } catch (final JRException e) {
  	      log4j.error("JR: Error: ", e);
  	      throw new ServletException(e.getMessage(), e);
  	    } catch (IOException ioe) {
  	      try {
  	        FileUtility f = new FileUtility(globalParameters.strFTPDirectory, strFileName + "-"
  	            + (reportId) + "." + strOutputType, false, true);
  	        if (f.exists())
  	          f.deleteFile();
  	      } catch (IOException ioex) {
  	        log4j.error("Error trying to delete temporary report file " + strFileName + "-"
  	            + (reportId) + "." + strOutputType + " : " + ioex.getMessage());
  	      }
  	    } catch (final Exception e) {
  	      throw new ServletException(e.getMessage(), e);
  	    } finally {
  	      try {
  	       // os.close();
  	      } catch (final Exception e) {
  	      }
  	    }

  	    return strPDFFileName;
  	  }


  private void saveReport(HttpServletRequest request,VariablesSecureApp vars, JasperPrint jp,
  	      Map<JRExporterParameter, Object> exportParameters, String fileName) throws JRException {
	  String strAttach ="";
	  if(globalParameters== null || globalParameters.equals("")){
	    	strAttach=(String) request.getSession().getAttribute("STR_ATTACH");
	    }else{
	    	strAttach = globalParameters.strFTPDirectory;
	    }

	  final String outputFile = strAttach + "/" + fileName;
	 log4j.info("CheckPrintingCategory file savereport method..."+outputFile);
  	    final String reportType = fileName.substring(fileName.lastIndexOf(".") + 1);
  	    if (reportType.equalsIgnoreCase("pdf")) {
  	      JasperExportManager.exportReportToPdfFile(jp, outputFile);
  	    } else if (reportType.equalsIgnoreCase("xls")) {
  	      JExcelApiExporter exporter = new JExcelApiExporter();
  	      exportParameters.put(JExcelApiExporterParameter.JASPER_PRINT, jp);
  	      exportParameters.put(JExcelApiExporterParameter.OUTPUT_FILE_NAME, outputFile);
  	      exportParameters.put(JExcelApiExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
  	      exportParameters.put(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
  	          Boolean.TRUE);
  	      exportParameters.put(JExcelApiExporterParameter.IS_DETECT_CELL_TYPE, true);
  	      exporter.setParameters(exportParameters);
  	      exporter.exportReport();
  	    } else {
  	      throw new JRException("Report type not supported");
  	    }

  	  }

  public void reprintedChecks(ConnectionProvider connectionProvider,String selectedIds)  throws ServletException{
//abhay.begins
	  	OBContext.setAdminMode();
	  	try{
	  		usloccpchecks objTdsfdChecks = OBDal.getInstance().get(usloccpchecks.class,selectedIds);
	  		objTdsfdChecks.setReprinted(true);
	  		int reprintedidcount = Integer.parseInt(objTdsfdChecks.getReprintedID())+1;
	  		objTdsfdChecks.setReprintedID(Integer.toString(reprintedidcount));
	  		objTdsfdChecks.setUpdated(new java.util.Date());
	  		objTdsfdChecks.setDateLastReprint(new java.util.Date());
	  		OBDal.getInstance().save(objTdsfdChecks);
	  		OBDal.getInstance().flush();
	  		 OBDal.getInstance().commitAndClose();
	  		
	  	}catch (Exception e) {
	  		e.printStackTrace();
			// TODO: handle exception
		}finally{
			OBContext.restorePreviousMode();
		}
	    
//abhay.ends    

}

  public static void insertCheckDetails(ConnectionProvider connectionProvider,String selIds,Integer strCheckNo,
		  String strClient, String strAD_Org_ID, String strC_BPartner_ID,String strSesICheck)    throws ServletException {
	  OBContext.setAdminMode();
	  try {
		  usloccpchecks objTdsfdChecks = OBProvider.getInstance().get(usloccpchecks.class);
		  objTdsfdChecks.setNewOBObject(true);
		  objTdsfdChecks.setId(String.valueOf(strCheckNo));
		  //System.out.println("strCheckNo is TDSFD_Checks_ID -- "+strCheckNo);
		  
		Organization objOrganization = OBDal.getInstance().get(Organization.class,strAD_Org_ID);
			objTdsfdChecks.setOrganization(objOrganization);
			//System.out.println("strAD_Org_ID is Organization -- "+strAD_Org_ID);
			
			Client objClient = OBDal.getInstance().get(Client.class,strClient);			
			objTdsfdChecks.setClient(objClient);
			selIds=selIds.trim();					
			selIds=selIds.substring(1, selIds.length()-1);			
			FIN_Payment objFinPayment = OBDal.getInstance().get(FIN_Payment.class,selIds);	
			objTdsfdChecks.setFINPayment(objFinPayment);			
			objTdsfdChecks.setBpartner(strC_BPartner_ID);			
			objTdsfdChecks.setINVOnchk(Boolean.parseBoolean(strSesICheck));			
			objTdsfdChecks.setDISPLAYCHECKNO(Long.parseLong(String.valueOf(strCheckNo)));
			OBDal.getInstance().save(objTdsfdChecks);
			OBDal.getInstance().flush();
			OBDal.getInstance().commitAndClose();
	  
	    } catch(Exception e){
	    	e.printStackTrace();
	    	
	    } finally {
	    	OBContext.restorePreviousMode();
	    }
	  }

 public boolean checkNumberExist(ConnectionProvider connectionProvider,String strCheckNumber) {
	 boolean bFlag=false;
		OBContext.setAdminMode();
		try {
			  final usloccpchecks objCheck = OBDal.getInstance().get(usloccpchecks.class,
					  strCheckNumber);

			  if(objCheck!=null){
					bFlag=true;
				}
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			OBContext.restorePreviousMode();
		}
	return bFlag;
  }



 public String AddPaymentPrintChecks(ConnectionProvider connectionProvider,VariablesSecureApp variables,HttpServletRequest request,HttpServletResponse response,String strSelectedIds,String strIntCheckNo,String strClient,String strAD_Org_ID,String strC_BPartner_ID){
	  CheckPrintingCategoryData[] data = null;
	  String strPDFFileName="";
	  try{
		  //update the fin payment table for selected ids set the document num to the check number..
		  String strSelId="";
		  String strPayID="";
		  int strCheckNo=0;
		  String strTemp[]=strSelectedIds.substring(1, strSelectedIds.length()-1).split(",");
		  strCheckNo=Integer.valueOf(strIntCheckNo);
		  String strSesICheck=(String) request.getSession().getAttribute("INVOICE_ON_CHECKS");
		  if ((strSesICheck==null) || strSesICheck.equals(""))
		  {
			  strSesICheck = "N";
		  }
		  for(int i=0;i<strTemp.length;i++){
			  strSelId=strTemp[i];
			  strPayID=strTemp[0];
			//  updataPayments(connectionProvider,strSelId,strCheckNo);
			  insertCheckDetails(connectionProvider,strSelId,strCheckNo, strClient, strAD_Org_ID, strC_BPartner_ID,strSesICheck);
			  strCheckNo++;
		  }

		  data = CheckPrintingCategoryData.getPrintPaymentDetails(connectionProvider,strSelectedIds);
		  strPayID=strPayID.substring(1, strPayID.length()-1);
		  final FIN_Payment finPayObj = OBDal.getInstance().get(FIN_Payment.class,strPayID);
		  
		  String strTemplateLocation=finPayObj.getAccount().getUsloccpTemploc();
		  log4j.info("strTemplateLocation........"+strTemplateLocation);
		  log4j.info("getAccount........"+finPayObj.getAccount().getId());
		  String strTemplateName=finPayObj.getAccount().getUsloccpTempname();

		  log4j.info("strTemplateLocation...."+strTemplateLocation);
		  log4j.info("strTemplateName...."+strTemplateName);

		
		  String strReportName=strTemplateLocation+"/"+strTemplateName;
		  HashMap<String, Object> designParameters = new HashMap<String, Object>();
		  designParameters.put("strSelectedIds", strSelectedIds);
		  designParameters.put("strIntCheckNo", strIntCheckNo);
		  log4j.info("AddPaymentPrintChecks strSelectedIds...."+strSelectedIds);
		  for(int i=0;i<strTemp.length;i++){
		  	  	strSelId=strTemp[i];
		  	  strSelId="("+strSelId+")";
		  
			  designParameters.put("strSelectedIds", strSelId);
			  int payCount=CheckPrintingCategoryData.PaymentsCount(this,strSelId);
			  designParameters.put("paymentsCount", payCount);
			  log4j.info("paymentsCount...."+payCount);
			  
			  String renderFile=renderJRFile(connectionProvider,variables,request, response,strReportName,"pdf", designParameters,data,null);
			  strPDFFileName=strPDFFileName+renderFile+",";
			  log4j.info("AddPaymentPrintChecks strPDFFileName...."+strPDFFileName);
		    }
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	  return strPDFFileName;

 }


 
 
 public static void deleteCheckDetails(ConnectionProvider connectionProvider){
	 OBContext.setAdminMode();
	 try
	 {
		 
		 usloccpchecks objTdsChecks = OBDal.getInstance().get(usloccpchecks.class,"-1");
		 OBDal.getInstance().remove(objTdsChecks);
		 OBDal.getInstance().flush();
		 OBDal.getInstance().commitAndClose();
		 
	 }catch (Exception e) {
		// TODO: handle exception
	}finally{
		 OBContext.restorePreviousMode();
	}
	 
		
  }
 private void reprintChecks(HttpServletResponse response, VariablesSecureApp vars,
		  String strCheckNoFromId,String  strCheckNoToId,String strClient,String strAD_Org_ID,String strCPFromDate,
		  String strCPToDate,String strInvoiceNo,String strC_BPartner_ID,String strBankAcc,String strOrderNo,String strPaymentNo,String strProposalNo) throws IOException, ServletException {

	  response.setContentType("text/html; charset=UTF-8");
     PrintWriter out = response.getWriter();

	    String discard[] = { };
	    XmlDocument xmlDocument = null;
	    CheckManagementData[] data = null;

     xmlDocument = xmlEngine.readXmlTemplate(
         "org/openbravo/localization/us/checkprinting/ad_forms/CheckManagement").createXmlDocument();
     data= CheckManagementData.reprintChecks(this,strCheckNoFromId,strCheckNoToId,strClient,strAD_Org_ID,strCPFromDate,
   		  strCPToDate,strInvoiceNo,strC_BPartner_ID,strBankAcc,strOrderNo,strPaymentNo,strProposalNo);

     // BEGIN generate and set UI elements

     ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CheckManagement", false, "",
         "", "", false, "ad_forms", strReplaceWith, false, true);
     toolbar.prepareSimpleToolBarTemplate();
     xmlDocument.setParameter("toolbar", toolbar.toString());
     try {
       WindowTabs tabs = new WindowTabs(this, vars,
           "org.openbravo.localization.us.checkprinting.ad_forms.CheckManagement");
       xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
       xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
       xmlDocument.setParameter("childTabContainer", tabs.childTabs());
       xmlDocument.setParameter("theme", vars.getTheme());
       NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
           "CheckManagement.html", classInfo.id, classInfo.type, strReplaceWith, tabs
               .breadcrumb());
       xmlDocument.setParameter("navigationBar", nav.toString());
       LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CheckManagement.html",
           strReplaceWith);
       xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
     } catch (Exception ex) {
       throw new ServletException(ex);
     }
     xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
     xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
     xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");
   // END generate and set UI elements

   // BEGIN check for any pending message to be shown and clear it from the session
     OBError myMessage = vars.getMessage("CheckManagement");
     vars.removeMessage("CheckManagement");
     if (myMessage != null) {
       xmlDocument.setParameter("messageType", myMessage.getType());
       xmlDocument.setParameter("messageTitle", myMessage.getTitle());
       xmlDocument.setParameter("messageMessage", myMessage.getMessage());
     }
   // END check for any pending message to be shown and clear it from the session

   // BEGIN Organization dropdown - using tabledir reference
     try {
       ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
           "AD_Org Security validation", Utility.getContext(this, vars, "#User_Org",
               "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
               "CheckManagement"), 0);
       Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
       xmlDocument.setData("NAME_reportOrganizationDropdown", "liststructure", comboTableData
           .select(false));
       comboTableData = null;
     } catch (Exception ex) {
       throw new ServletException(ex);
     }


     try {

         ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
             "ad_client_id", "", "", Utility.getContext(this, vars, "#User_Org",
                 "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                 "CheckManagement"), 0);
         Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
         xmlDocument.setData("NAME_reportClient", "liststructure",
             comboTableData.select(false));
         comboTableData = null;
       } catch (Exception ex) {
         throw new ServletException(ex);
       }

       try {
       	String strId = null;

       	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
       		strId = "7ACD326BEAE34F338343AD4FFE793F77";
       	}
       	else
       	{
       		strId = "8511593E88D24C00991B382D0CF49B9E";
       	}
       		ComboTableData comboTableData = new ComboTableData(vars, this, "18",
               "c_invoice_id", strId, "", Utility.getContext(this, vars, "#User_Org",
                   "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                   "CheckManagement"), 0);
           Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
           xmlDocument.setData("NAME_reportInvoiceNo", "liststructure",
               comboTableData.select(false));
           comboTableData = null;
         } catch (Exception ex) {
           throw new ServletException(ex);
         }


             try {
                 ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR",
                     "FIN_FINANCIAL_ACCOUNT_ID", "", "", Utility.getContext(this, vars, "#User_Org",
                         "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                         "CheckManagement"), 0);
                 Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
                 xmlDocument.setData("NAME_reportBankAcc", "liststructure",
                     comboTableData.select(false));
                 comboTableData = null;
               } catch (Exception ex) {
                 throw new ServletException(ex);
               }


               //this combo for order...
               try {
               	String strOrder_Id = null;

               	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
               		strOrder_Id = "FD620CDEFBF2438B932269EBC65D0825";
               	}
               	else
               	{
               		strOrder_Id = "2036A37E2A6C4BD2BCAACD3CCEB0F67C";
               	}

                   ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                       "C_Order_ID", strOrder_Id, "", Utility.getContext(this, vars, "#User_Org",
                           "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                           "CheckManagement"), 0);
                   Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
                   xmlDocument.setData("NAME_reportOrderNo", "liststructure",
                       comboTableData.select(false));
                   comboTableData = null;
                 } catch (Exception ex) {
                   throw new ServletException(ex);
                 }

                 try {
                 	String strPayment_Id = null;

               	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
               		strPayment_Id = "8A18129DBDF047AD9A93DC2C8AFFEFD4";
               	}
               	else
               	{
               		strPayment_Id = "C584536268CA4CF683F4761B57EBC67E";
               	}
                     ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                         "documentno", strPayment_Id, "", Utility.getContext(this, vars, "#User_Org",
                             "CheckPrintingCategory"), Utility.getContext(this, vars, "#User_Client",
                             "CheckPrintingCategory"), 0);
                     Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckPrintingCategory", null);
                     xmlDocument.setData("NAME_reportPaymentNo", "liststructure",
                         comboTableData.select(false));
                     comboTableData = null;
                  


                   } catch (Exception ex) {
                     throw new ServletException(ex);
                   }

                   try {
                     	String strProposal_Id = null;

                   	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
                   		strProposal_Id = "00D5D95618EE4C70B9488BDA9696BECD";
                   	}
                   	else
                   	{
                   		strProposal_Id = "417E0A623BE04FBC95B73333655C5C3E";
                   	}
                       ComboTableData comboTableData = new ComboTableData(vars, this, "18",
                           "documentno", strProposal_Id, "", Utility.getContext(this, vars, "#User_Org",
                               "CheckManagement"), Utility.getContext(this, vars, "#User_Client",
                               "CheckManagement"), 0);
                       Utility.fillSQLParameters(this, vars, null, comboTableData, "CheckManagement", null);
                       xmlDocument.setData("NAME_reportProposalNo", "liststructure",
                           comboTableData.select(false));
                       comboTableData = null;
                     } catch (Exception ex) {
                       throw new ServletException(ex);
                     }



   // BEGIN put parameter values back into the form so that they remain entered on reload

	        xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
			xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
			xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
			xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));

			xmlDocument.setParameter("NAME_paramBPartnerId", strC_BPartner_ID);

		  final BusinessPartner objBPartner = OBDal.getInstance().get(BusinessPartner.class,
		  			strC_BPartner_ID);
		      String bPartnerDescription="";
		      if(objBPartner!=null){
		    	  bPartnerDescription=objBPartner.getName();
		      }
		      xmlDocument.setParameter("NAME_paramBPartnerDescription",bPartnerDescription);

		      xmlDocument.setParameter("NAME_paramOrganizationId", strAD_Org_ID);

			  xmlDocument.setParameter("NAME_paramClient", strClient);
			  xmlDocument.setParameter("NAME_paramInvoiceNo", strInvoiceNo);
			  xmlDocument.setParameter("NAME_paramBankAcc", strBankAcc);
			  xmlDocument.setParameter("NAME_paramCheckNoFrom", strCheckNoFromId);
			  xmlDocument.setParameter("NAME_paramCheckNoTo", strCheckNoToId);

			  xmlDocument.setParameter("NAME_paramCheckDateFrom", strCPFromDate);
			  xmlDocument.setParameter("NAME_paramCheckDateTo", strCPToDate);

			  xmlDocument.setParameter("NAME_paramOrderNo", strOrderNo);
			  xmlDocument.setParameter("NAME_paramPaymentNo", strPaymentNo);
			  xmlDocument.setParameter("NAME_paramProposalNo", strProposalNo);

			  xmlDocument.setData("structure1", data);

   // BEGIN render the output and close it so that it is returned to the user
   out.println(xmlDocument.print());
   out.close();
   // END render the output and close it so that it is returned to the user
 }
}
