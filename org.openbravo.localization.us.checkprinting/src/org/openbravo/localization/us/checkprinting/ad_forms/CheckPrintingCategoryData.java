/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * Mallikarjun M
 ************************************************************************************
 */
package org.openbravo.localization.us.checkprinting.ad_forms;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.core.SessionHandler;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;


public class CheckPrintingCategoryData implements FieldProvider {
static Logger log4j = Logger.getLogger(CheckPrintingCategoryData.class);
  private String InitRecordNumber="0";
  public String mproductid;
  public String adorgname;
  public String searchkey;
  public String name;
  public String categoryname;
  public String bpartnername;
  public String producttype;
  public String purchased;
  public String sold;
  public String rownum;
  public String amount;
  public String dateplanned;
  public String paymentid;
  public String partnerid;
  public int iCheckNo;
  public String checknumber;
  public String checkdate;
  public String payee;
  public String status;
  public String checkamount;
  public String orgname;
  public String clientname;


  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("MPRODUCTID"))
      return mproductid;
    else if (fieldName.equalsIgnoreCase("ADORGNAME"))
      return adorgname;
    else if (fieldName.equalsIgnoreCase("SEARCHKEY"))
      return searchkey;
    else if (fieldName.equalsIgnoreCase("NAME"))
      return name;
    else if (fieldName.equalsIgnoreCase("CATEGORYNAME"))
      return categoryname;
    else if (fieldName.equalsIgnoreCase("BPARTNERNAME"))
      return bpartnername;
    else if (fieldName.equalsIgnoreCase("PRODUCTTYPE"))
      return producttype;
    else if (fieldName.equalsIgnoreCase("PURCHASED"))
      return purchased;
    else if (fieldName.equalsIgnoreCase("SOLD"))
      return sold;
    else if (fieldName.equals("rownum"))
      return rownum;
    else if (fieldName.equals("amount"))
        return amount;
    else if (fieldName.equals("dateplanned"))
        return dateplanned;
    else if (fieldName.equals("paymentid"))
        return paymentid;
    else if (fieldName.equals("partnerid"))
        return partnerid;
    else if (fieldName.equals("iCheckNo"))
        return String.valueOf(iCheckNo);
    else if (fieldName.equals("checknumber"))
        return String.valueOf(checknumber);
    else if (fieldName.equals("checkdate"))
        return String.valueOf(checkdate);
    else if (fieldName.equals("payee"))
        return String.valueOf(payee);
    else if (fieldName.equals("status"))
        return String.valueOf(status);
    else if (fieldName.equals("checkamount"))
        return String.valueOf(checkamount);
    else if (fieldName.equals("orgname"))
        return String.valueOf(orgname);
    else if (fieldName.equals("clientname"))
        return String.valueOf(clientname);
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CheckPrintingCategoryData[] set()    throws ServletException {
    CheckPrintingCategoryData objectCheckPrintingCategoryData[] = new CheckPrintingCategoryData[1];
    objectCheckPrintingCategoryData[0] = new CheckPrintingCategoryData();
    objectCheckPrintingCategoryData[0].mproductid = "";
    objectCheckPrintingCategoryData[0].adorgname = "";
    objectCheckPrintingCategoryData[0].searchkey = "";
    objectCheckPrintingCategoryData[0].name = "";
    objectCheckPrintingCategoryData[0].categoryname = "";
    objectCheckPrintingCategoryData[0].bpartnername = "";
    objectCheckPrintingCategoryData[0].producttype = "";
    objectCheckPrintingCategoryData[0].purchased = "";
    objectCheckPrintingCategoryData[0].sold = "";
    return objectCheckPrintingCategoryData;
  }

  public static CheckPrintingCategoryData[] customSelect(VariablesSecureApp vars,ConnectionProvider connectionProvider, String language,String adUserClient, String adUserOrg,String strC_BPartner_ID,String strAD_Org_ID,String strCategory_ID,String strProductType,
		  String strPurchasedSold,String strClient,String strInvoiceNo,String strCashJourName,String strBankStmtName,String strBankAcc,String strCashBook,String strOrderNo,String strPaymentNo,String strProposalNo)  throws ServletException {
	    return customSelect(vars,connectionProvider, language,adUserClient, adUserOrg,strC_BPartner_ID,strAD_Org_ID,strCategory_ID,strProductType,
	  		  strPurchasedSold,strClient,strInvoiceNo,strCashJourName,strBankStmtName,strBankAcc,strCashBook, 0, 0, strOrderNo, strPaymentNo, strProposalNo);
	  }

  public static CheckPrintingCategoryData[] customSelect(VariablesSecureApp vars,ConnectionProvider connectionProvider, String language,String adUserClient, String adUserOrg,String strC_BPartner_ID,String strAD_Org_ID,String strCategory_ID,String strProductType,
		  String strPurchasedSold,String strClient,String strInvoiceNo,String strCashJourName,String strBankStmtName,String strBankAcc,String strCashBook,int firstRegister, int numberRegisters,String strOrderNo,String strPaymentNo,String strProposalNo)  throws ServletException {
	  String strSql = "";
	  strSql = strSql +
	  "  select distinct checknumber,id,checkdate, payee, status,checkamount,orgname,clientname" +
      "  from  usloccp_checksearch_v where 1=1 " ;
	  
   strSql = strSql + ((strClient==null || strClient.equals(""))?"":" AND AD_CLIENT_ID= ? ");
   strSql = strSql + ((strAD_Org_ID==null || strAD_Org_ID.equals(""))?"":" and ad_org_id= ? ");
   strSql = strSql + ((strInvoiceNo==null || strInvoiceNo.equals(""))?"":" AND C_INVOICE_ID= ? ");
   strSql = strSql + ((strC_BPartner_ID==null || strC_BPartner_ID.equals(""))?"":" AND C_BPARTNER_ID= ? ");
   strSql = strSql + ((strBankAcc==null || strBankAcc.equals(""))?"":"  and FIN_FINANCIAL_ACCOUNT_ID= ? ");

   strSql = strSql + ((strOrderNo==null || strOrderNo.equals(""))?"":" AND C_order_ID= ? ");
   strSql = strSql + ((strPaymentNo==null || strPaymentNo.equals(""))?"":" AND DOCUMENTNO= ? ");
   strSql = strSql + ((strProposalNo==null || strProposalNo.equals(""))?"":" AND fin_payment_proposal_id= ? ");

    log4j.info("The query for customSelect method is...."+strSql);
    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
    if (strClient != null && !(strClient.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strClient);
          log4j.info("strClient...."+strClient);
        }
    if (strAD_Org_ID != null && !(strAD_Org_ID.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strAD_Org_ID);
          log4j.info("strAD_Org_ID...."+strAD_Org_ID);
        }
      if (strInvoiceNo != null && !(strInvoiceNo.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strInvoiceNo);
          log4j.info("strInvoiceNo...."+strInvoiceNo);
        }
      if (strC_BPartner_ID != null && !(strC_BPartner_ID.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strC_BPartner_ID);
          log4j.info("strC_BPartner_ID...."+strC_BPartner_ID);
        }
      if (strBankAcc != null && !(strBankAcc.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strBankAcc);
          log4j.info("strBankAcc...."+strBankAcc);
        }

      if (strOrderNo != null && !(strOrderNo.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strOrderNo);
          log4j.info("strOrderNo...."+strOrderNo);
        }
      if (strPaymentNo != null && !(strPaymentNo.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strPaymentNo);
          log4j.info("strPaymentNo...."+strPaymentNo);
        }
      if (strProposalNo != null && !(strProposalNo.equals(""))) {

    	  String strPropId=(String)vars.getSessionValue("PAY_PROPOSAL_ID");
    	  if(strPropId!=null && strPropId!=""){
    		  iParameter++; UtilSql.setValue(st, iParameter, 12, null,strPropId);
    		  log4j.info("strPropId...."+strPropId);

    	  }else{
        	  iParameter++; UtilSql.setValue(st, iParameter, 12, null,strProposalNo);
        	  log4j.info("strProposalNo...."+strProposalNo);
    	  }

        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CheckPrintingCategoryData objectCheckPrintingCategoryData = new CheckPrintingCategoryData();
        objectCheckPrintingCategoryData.paymentid = UtilSql.getValue(result, "id");
        objectCheckPrintingCategoryData.checknumber = UtilSql.getValue(result, "checknumber");
        objectCheckPrintingCategoryData.payee = UtilSql.getValue(result, "payee");
        objectCheckPrintingCategoryData.checkdate = UtilSql.getDateValue(result, "checkdate", "MM/dd/yyyy");
        objectCheckPrintingCategoryData.status = UtilSql.getValue(result, "status");
        objectCheckPrintingCategoryData.checkamount = UtilSql.getValue(result, "checkamount");
        log4j.info("amount...."+UtilSql.getValue(result, "checkamount"));
        objectCheckPrintingCategoryData.orgname = UtilSql.getValue(result, "orgname");
        objectCheckPrintingCategoryData.clientname = UtilSql.getValue(result, "clientname");

        objectCheckPrintingCategoryData.rownum = Long.toString(countRecord);
        objectCheckPrintingCategoryData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCheckPrintingCategoryData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
	    } catch(SQLException e){
	      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    CheckPrintingCategoryData objectCheckPrintingCategoryData[] = new CheckPrintingCategoryData[vector.size()];
	    vector.copyInto(objectCheckPrintingCategoryData);
	    return(objectCheckPrintingCategoryData);
  }

  //rohit
  public static CheckPrintingCategoryData[] customSelectForPrintCheck(VariablesSecureApp vars,ConnectionProvider connectionProvider, String language,String adUserClient, String adUserOrg,String strC_BPartner_ID,String strAD_Org_ID,String strCategory_ID,String strProductType,
		  String strPurchasedSold,String strClient,String strInvoiceNo,String strCashJourName,String strBankStmtName,String strBankAcc,String strCashBook,String strOrderNo,String strPaymentNo,String strProposalNo)  throws ServletException {
	    return customSelectForPrintCheck(vars,connectionProvider, language,adUserClient, adUserOrg,strC_BPartner_ID,strAD_Org_ID,strCategory_ID,strProductType,
	  		  strPurchasedSold,strClient,strInvoiceNo,strCashJourName,strBankStmtName,strBankAcc,strCashBook, 0, 0, strOrderNo, strPaymentNo, strProposalNo);
	  }
  //rohit
  public static CheckPrintingCategoryData[] customSelectForPrintCheck(VariablesSecureApp vars,ConnectionProvider connectionProvider, String language,String adUserClient, String adUserOrg,String strC_BPartner_ID,String strAD_Org_ID,String strCategory_ID,String strProductType,
		  String strPurchasedSold,String strClient,String strInvoiceNo,String strCashJourName,String strBankStmtName,String strBankAcc,String strCashBook,int firstRegister, int numberRegisters,String strOrderNo,String strPaymentNo,String strProposalNo) 
  throws ServletException {
	  if(strAD_Org_ID != null && strAD_Org_ID.equals("0")){
    	  strAD_Org_ID=null;
      }
	  String strSql = "";
	  strSql = strSql +
	  "  select distinct id,checkdate, payee, status,checkamount,orgname,clientname" +
      "  from  usloccp_checkprintsrch_v where 1=1 and not exists (select fin_payment_id from usloccp_checks where fin_payment_id=usloccp_checkprintsrch_v.id) " +
      "  and checkamount!=0 and C_BPARTNER_ID is not null";
	  
   strSql = strSql + ((strClient==null || strClient.equals(""))?"":" AND AD_CLIENT_ID= ? ");
   strSql = strSql + ((strAD_Org_ID==null || strAD_Org_ID.equals("0"))?"":" and ad_org_id= ? ");
   strSql = strSql + ((strInvoiceNo==null || strInvoiceNo.equals(""))?"":" AND C_INVOICE_ID= ? ");
   strSql = strSql + ((strC_BPartner_ID==null || strC_BPartner_ID.equals(""))?"":" AND C_BPARTNER_ID= ? ");
   strSql = strSql + ((strBankAcc==null || strBankAcc.equals(""))?"":"  and FIN_FINANCIAL_ACCOUNT_ID= ? ");

   strSql = strSql + ((strOrderNo==null || strOrderNo.equals(""))?"":" AND C_order_ID= ? ");
   strSql = strSql + ((strPaymentNo==null || strPaymentNo.equals(""))?"":" AND paymentno= ? ");
   strSql = strSql + ((strProposalNo==null || strProposalNo.equals(""))?"":" AND fin_payment_proposal_id= ? ");

	strSql = strSql + (" order by payee,checkdate ");

    log4j.info("The query for customSelectForPrintCheck method is...."+strSql);
    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
    if (strClient != null && !(strClient.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strClient);
          log4j.info("strClient...."+strClient);
        }
    if (strAD_Org_ID != null && !(strAD_Org_ID.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strAD_Org_ID);
          log4j.info("strAD_Org_ID...."+strAD_Org_ID);
        }
      if (strInvoiceNo != null && !(strInvoiceNo.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strInvoiceNo);
          log4j.info("strInvoiceNo...."+strInvoiceNo);
        }
      if (strC_BPartner_ID != null && !(strC_BPartner_ID.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strC_BPartner_ID);
          log4j.info("strC_BPartner_ID...."+strC_BPartner_ID);
        }
      if (strBankAcc != null && !(strBankAcc.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strBankAcc);
          log4j.info("strBankAcc...."+strBankAcc);
        }

      if (strOrderNo != null && !(strOrderNo.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strOrderNo);
          log4j.info("strOrderNo...."+strOrderNo);
        }
      if (strPaymentNo != null && !(strPaymentNo.equals(""))) {
          iParameter++; UtilSql.setValue(st, iParameter, 12, null, strPaymentNo);
          log4j.info("strPaymentNo...."+strPaymentNo);
        }
      if (strProposalNo != null && !(strProposalNo.equals(""))) {

    	  String strPropId=(String)vars.getSessionValue("PAY_PROPOSAL_ID");
    	  if(strPropId!=null && strPropId!=""){
    		  iParameter++; UtilSql.setValue(st, iParameter, 12, null,strPropId);
    		  log4j.info("strPropId...."+strPropId);

    	  }else{
        	  iParameter++; UtilSql.setValue(st, iParameter, 12, null,strProposalNo);
        	  log4j.info("strProposalNo...."+strProposalNo);
    	  }

        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CheckPrintingCategoryData objectCheckPrintingCategoryData = new CheckPrintingCategoryData();
        objectCheckPrintingCategoryData.paymentid = UtilSql.getValue(result, "id");
        //objectCheckPrintingCategoryData.checknumber = UtilSql.getValue(result, "checknumber");
        objectCheckPrintingCategoryData.payee = UtilSql.getValue(result, "payee");
        objectCheckPrintingCategoryData.checkdate = UtilSql.getDateValue(result, "checkdate", "MM/dd/yyyy");
        objectCheckPrintingCategoryData.status = UtilSql.getValue(result, "status");
        objectCheckPrintingCategoryData.checkamount = UtilSql.getValue(result, "checkamount");
        log4j.info("amount...."+UtilSql.getValue(result, "checkamount"));
        objectCheckPrintingCategoryData.orgname = UtilSql.getValue(result, "orgname");
        objectCheckPrintingCategoryData.clientname = UtilSql.getValue(result, "clientname");

        objectCheckPrintingCategoryData.rownum = Long.toString(countRecord);
        objectCheckPrintingCategoryData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCheckPrintingCategoryData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
	    } catch(SQLException e){
	      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    CheckPrintingCategoryData objectCheckPrintingCategoryData[] = new CheckPrintingCategoryData[vector.size()];
	    vector.copyInto(objectCheckPrintingCategoryData);
	    return(objectCheckPrintingCategoryData);
  }
  
  public static String CheckCount(ConnectionProvider connectionProvider) throws ServletException {
		
	    Boolean recordsExixts=PreCheckCount(connectionProvider);	  
		if(!recordsExixts){			
			return("1");
		}	  
	  String strSql = "";
    
      strSql = strSql +" select max(DISPLAY_CHECKNO) as USLOCCP_CHECKS_ID from USLOCCP_CHECKS ";
	
	  ResultSet result;
      String strReturn = "1";
      Statement st = null;

      try {
    	  st = connectionProvider.getStatement();
    	  result = st.executeQuery(strSql);
	        if(result.next()) {
	          strReturn = UtilSql.getValue(result, "USLOCCP_CHECKS_ID");
	          if(strReturn==null || strReturn.equals("null")|| strReturn.equals(""))
	        	  strReturn = "1";
	        }
         result.close();
      } catch(SQLException e){
        log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
        throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
      } catch(Exception ex){
        log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
        throw new ServletException("@CODE=@" + ex.getMessage());
      } finally {
        try {
          connectionProvider.releaseStatement(st);
        } catch(Exception ignore){
          ignore.printStackTrace();
        }
      }

      return(strReturn);
    }
  
  public static Boolean PreCheckCount(ConnectionProvider connectionProvider)    throws ServletException {
	  
	  String strSql = "";
	  
    
      strSql = strSql +" select DISPLAY_CHECKNO as USLOCCP_CHECKS_ID from USLOCCP_CHECKS; ";
	
	  ResultSet result;
      Statement st = null;

      try {
    	  st = connectionProvider.getStatement();
    	  result = st.executeQuery(strSql);
	        if(result.next()) {
	          return true;
	        }
	        else{
	        	return false;
	        }
      } catch(SQLException e){
        log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
        throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
      } catch(Exception ex){
        log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
        throw new ServletException("@CODE=@" + ex.getMessage());
      } finally {
        try {
          connectionProvider.releaseStatement(st);
        } catch(Exception ignore){
          ignore.printStackTrace();
        }
      }
     
    }
//****************************************************************************************************************************************************************
    public static CheckPrintingCategoryData[] customPrintedSelect(ConnectionProvider connectionProvider, String strSelectedId)  throws ServletException {
  	    return customPrintedSelect(connectionProvider,strSelectedId,  0, 0);
  	  }

    public static CheckPrintingCategoryData[] customPrintedSelect(ConnectionProvider connectionProvider, String strSelectedId,int firstRegister, int numberRegisters)  throws ServletException {
  	  String strSql = "";
  	 strSql = strSql +
	 " select distinct id as id,checknumber as checknumber,checkdate as checkdate, payee as payee,status as status, grandtotal as checkamount,orgname as orgname, clientname as clientname " +
     " from  usloccp_checksearch_v where id in  "+strSelectedId;
  
  	    ResultSet result;
  	    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
  	    Statement st = null;

  	    try {
  	    	st = connectionProvider.getStatement();

  	      result = st.executeQuery(strSql);
  	      long countRecord = 0;
  	      long countRecordSkip = 1;
  	      boolean continueResult = true;
  	      while(countRecordSkip < firstRegister && continueResult) {
  	        continueResult = result.next();
  	        countRecordSkip++;
  	      }
  	      while(continueResult && result.next()) {
  	        countRecord++;
  	        CheckPrintingCategoryData objectCheckPrintingCategoryData = new CheckPrintingCategoryData();
	  	  	objectCheckPrintingCategoryData.paymentid = UtilSql.getValue(result, "id");
	        objectCheckPrintingCategoryData.checknumber = UtilSql.getValue(result, "checknumber");
	        objectCheckPrintingCategoryData.payee = UtilSql.getValue(result, "payee");
	        objectCheckPrintingCategoryData.checkdate = UtilSql.getDateValue(result, "checkdate", "MM/dd/yyyy");
	        objectCheckPrintingCategoryData.status = UtilSql.getValue(result, "status");
	        objectCheckPrintingCategoryData.checkamount = UtilSql.getValue(result, "checkamount");
	       /* int strOrgCount=CheckPrintingCategoryData.getOrgName(connectionProvider,UtilSql.getValue(result, "id"));
	        if(strOrgCount==0)
	      	  objectCheckPrintingCategoryData.orgname = UtilSql.getValue(result, "orgname");
	        else
	      	  objectCheckPrintingCategoryData.orgname = "";
	        */
	        objectCheckPrintingCategoryData.orgname = UtilSql.getValue(result, "orgname");
	        objectCheckPrintingCategoryData.clientname = UtilSql.getValue(result, "clientname");

  	        objectCheckPrintingCategoryData.rownum = Long.toString(countRecord);
  	        objectCheckPrintingCategoryData.InitRecordNumber = Integer.toString(firstRegister);
  	        vector.addElement(objectCheckPrintingCategoryData);
  	        if (countRecord >= numberRegisters && numberRegisters != 0) {
  	          continueResult = false;
  	        }
  	      }
  	      result.close();
  	    } catch(SQLException e){
  	      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
  	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
  	    } catch(Exception ex){
  	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
  	      throw new ServletException("@CODE=@" + ex.getMessage());
  	    } finally {
  	      try {
  	        connectionProvider.releaseStatement(st);
  	      } catch(Exception ignore){
  	        ignore.printStackTrace();
  	      }
  	    }
  	    CheckPrintingCategoryData objectCheckPrintingCategoryData[] = new CheckPrintingCategoryData[vector.size()];
  	    vector.copyInto(objectCheckPrintingCategoryData);
  	    return(objectCheckPrintingCategoryData);
    }


   
public static String getPaymentID(ConnectionProvider connectionProvider,String strOrgId,String strInvoiceId) throws ServletException {
	
	  String strPaymentID="";
	  String strSql = "";
  	  strSql = strSql +" select fin_payment_id as id from fin_payment_detail_v "+
  	  				" where invoiceno in (select documentno from c_invoice where " +
  	  				" c_invoice_id ='"+strInvoiceId+"')";
  	  try {
  	      log4j.info("getPaymentId......"+strSql);
  	    Query q= SessionHandler.getInstance().getSession()
	            .createSQLQuery(strSql);
			  for (int i = 0; i < q.list().size(); i++) {
					strPaymentID =strPaymentID+"'"+(String) q.list().get(i)+"',";
			  }
			  
  	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	 return(strPaymentID);
}

public static String getProposalPaymentID(ConnectionProvider connectionProvider,String strFinPaymentProposalId) throws ServletException {
	  String strPaymentID="";
	  String strSql = "";
	  strSql = strSql +" select fin_payment_id as id  from FIN_Payment_Prop_Detail_V " +
	  		" where fin_payment_proposal_id='"+strFinPaymentProposalId+"'";
	  try {
		  
		  Query q= SessionHandler.getInstance().getSession()
		            .createSQLQuery(strSql);
				  for (int i = 0; i < q.list().size(); i++) {
					  strPaymentID =(String) q.list().get(i);
				  }
				  
	    log4j.info("getProposalPaymentID......"+strSql);
	    	log4j.info("strPaymentID......"+strPaymentID);
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	 return(strPaymentID);
}

//*****************************************************************************************************************************
//SQL converted DAL 
/*public static boolean getCheckExist(ConnectionProvider connectionProvider,String strPaymentId) throws ServletException {
	 boolean bFlag=false;
	  String strSql = "";
	  strSql = strSql +
  "  SELECT fin_payment_id FROM USLOCCP_CHECKS " +
  " WHERE fin_payment_id in "+strPaymentId+" and isvoid='Y' ";
	  OBContext.setAdminMode();
	    ResultSet result;
	    Statement st = null;
    try {
	    	st = connectionProvider.getStatement();
	    	log4j.info("checkNumberExist..sql..."+strSql);
	    	result = st.executeQuery(strSql);
	    	while(result.next()) {
	    		bFlag=true;
	    		return bFlag;
	    	 }
	      result.close();
	    	 Query q = OBDal.getInstance().getSession().createQuery(strSql);
	 	    log4j.info("getCheckExist......"+strSql);
	 	    for (int i = 0; i < q.list().size(); i++) {
	 	    	bFlag=true;
	    		//return bFlag;
	   	      }
	    	
	    } catch(Exception e){
			 log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
		      e.printStackTrace();
		    }  finally {
		      OBContext.restorePreviousMode();
		    }
	  return bFlag;
}*/
public static boolean getCheckExist(ConnectionProvider connectionProvider,String strPaymentId) throws ServletException {
	 boolean bFlag=false;
	  String strSql = "";
	  strSql = strSql +
   "  SELECT fin_payment_id FROM USLOCCP_CHECKS " +
   " WHERE fin_payment_id in "+strPaymentId+" and isvoid='Y' ";
	    try {
	    	Query q= SessionHandler.getInstance().getSession()
		            .createSQLQuery(strSql);
			    	  if (q.list().size() > 0) 
			    		  bFlag=true;
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	   log4j.info("checkNumberExist..bFlag..."+bFlag);
	  return bFlag;
}
//***********************************************************************************************************************

//SQL converted DAL
/*public static boolean getCheckExistWithoutVoid(ConnectionProvider connectionProvider,String strPaymentId) throws ServletException {
	 boolean bFlag=false;
	  String strSql = "";
	  strSql = strSql +
	  "  SELECT fin_payment_id FROM USLOCCP_CHECKS " +
	     " WHERE fin_payment_id in ('"+strPaymentId+"')";

	    ResultSet result;
	    Statement st = null;
	    OBContext.setAdminMode();
	    try {
	    	st = connectionProvider.getStatement();
	    	log4j.info("checkNumberExist..sql..."+strSql);
	    	result = st.executeQuery(strSql);
	    	while(result.next()) {
	    		bFlag=true;
	    	 }
	      result.close();
	    	Query q = OBDal.getInstance().getSession().createQuery(strSql);
	 	    log4j.info("getCheckExistWithoutVoid......"+strSql);
	 	    for (int i = 0; i < q.list().size(); i++) {
	 	    	bFlag=true;
	    		//return bFlag;
	   	      }
	    }  catch(Exception e){
			 log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
		      e.printStackTrace();
		    }  finally {
		      OBContext.restorePreviousMode();
		    }
	   log4j.info("checkNumberExist..bFlag..."+bFlag);
	  return bFlag;
}*/

public static boolean getCheckExistWithoutVoid(ConnectionProvider connectionProvider,String strPaymentId) throws ServletException {
	 boolean bFlag=false;
	  String strSql = "";
	  strSql = strSql +
	  "  SELECT fin_payment_id FROM USLOCCP_CHECKS " +
	     " WHERE fin_payment_id in ('"+strPaymentId+"')";
	    try {
	    	Query q= SessionHandler.getInstance().getSession()
		            .createSQLQuery(strSql);
			    	  if (q.list().size() > 0) 
			    		  bFlag=true;
	    	log4j.info("checkNumberExist..sql..."+strSql);
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	   log4j.info("checkNumberExist..bFlag..."+bFlag);
	  return bFlag;
}
//************************************************************************************************************************
public static CheckPrintingCategoryData[] getPrintPaymentDetails(ConnectionProvider connectionProvider,String strPaymentId)  throws ServletException {
	String strSql = "";
	strSql = strSql +
	 " select distinct id as id,checknumber as checknumber,checkdate as checkdate, payee as payee,status as status, checkamount as checkamount,orgname as orgname, clientname as clientname " +
     " from  USLOCCP_checksearch_v where C_BPARTNER_ID is not null and id in  "+strPaymentId;
	  
  ResultSet result;
  Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
  PreparedStatement st = null;

  try {
	  log4j.info("getPrintPaymentDetails...."+strSql);
    st = connectionProvider.getPreparedStatement(strSql);
    result = st.executeQuery();
    long countRecord = 0;
   
   
    while(result.next()) {
      countRecord++;
      CheckPrintingCategoryData objectCheckPrintingCategoryData = new CheckPrintingCategoryData();
      objectCheckPrintingCategoryData.paymentid = UtilSql.getValue(result, "id");
      objectCheckPrintingCategoryData.checknumber = UtilSql.getValue(result, "checknumber");
      objectCheckPrintingCategoryData.payee = UtilSql.getValue(result, "payee");
      objectCheckPrintingCategoryData.checkdate = UtilSql.getDateValue(result, "checkdate", "MM/dd/yyyy");
      objectCheckPrintingCategoryData.status = UtilSql.getValue(result, "status");
      objectCheckPrintingCategoryData.checkamount = UtilSql.getValue(result, "checkamount");
     /* int strOrgCount=CheckPrintingCategoryData.getOrgName(connectionProvider,UtilSql.getValue(result, "id"));
      if(strOrgCount==0)
    	  objectCheckPrintingCategoryData.orgname = UtilSql.getValue(result, "orgname");
      else
    	  objectCheckPrintingCategoryData.orgname = "";
      */
      objectCheckPrintingCategoryData.orgname = UtilSql.getValue(result, "orgname");
      objectCheckPrintingCategoryData.clientname = UtilSql.getValue(result, "clientname");
      objectCheckPrintingCategoryData.rownum = Long.toString(countRecord);
      objectCheckPrintingCategoryData.InitRecordNumber = Long.toString(countRecord);
      vector.addElement(objectCheckPrintingCategoryData);
      
    }
    result.close();
	    } catch(SQLException e){
	      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
	      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    } finally {
	      try {
	        connectionProvider.releasePreparedStatement(st);
	      } catch(Exception ignore){
	        ignore.printStackTrace();
	      }
	    }
	    CheckPrintingCategoryData objectCheckPrintingCategoryData[] = new CheckPrintingCategoryData[vector.size()];
	    vector.copyInto(objectCheckPrintingCategoryData);
	    return(objectCheckPrintingCategoryData);
}


public static int getOrgName(ConnectionProvider connectionProvider,String strPaymentId)  throws ServletException {
	String strSql = "";
	strSql = strSql +
	" select count(*) as count from "+
	" (select distinct ord.ad_org_id from c_order ord ,FIN_Payment_Prop_Detail_V ppdv where "+
	" ord.documentno =ppdv.orderno and "+
	" ppdv.fin_payment_id='"+strPaymentId+"' union " +
	" select distinct inv.ad_org_id  from c_invoice inv ,FIN_Payment_Prop_Detail_V ppdv where  "+
	" inv.documentno =ppdv.invoiceno and "+
	" ppdv.fin_payment_id='"+strPaymentId+"') a ";
	  
  int count=0;

  try {
	  log4j.info("getOrgName...."+strSql);
	  Query q= SessionHandler.getInstance().getSession()
	            .createSQLQuery(strSql);
			  for (int i = 0; i < q.list().size(); i++) {
				  count =Integer.valueOf((String) q.list().get(0));
			  }
	    } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	   
	    return(count);
}


public static int PaymentsCount(ConnectionProvider connectionProvider,String strPayment)    throws ServletException {
	String strSql = "";
	strSql = strSql +
	" select distinct cpv.checknumber from USLOCCP_checksearch_v cpv where id in "+strPayment+" ";
  int countRecord = 0;
  try {
	  Query q= SessionHandler.getInstance().getSession()
	            .createSQLQuery(strSql);
	  for (int i = 0; i < q.list().size(); i++) {
		    		  countRecord++;
	  }
    log4j.info("PaymentsCount....strSql...."+strSql);
       } catch(Exception ex){
	      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
	      throw new ServletException("@CODE=@" + ex.getMessage());
	    }
	    return(countRecord);
}


}
