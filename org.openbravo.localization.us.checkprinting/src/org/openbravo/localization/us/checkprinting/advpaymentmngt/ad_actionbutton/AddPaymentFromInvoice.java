/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * Mallikarjun M
 ************************************************************************************
 */
package org.openbravo.localization.us.checkprinting.advpaymentmngt.ad_actionbutton;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintingCategory;
import org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintingCategoryData;
import org.openbravo.model.common.invoice.Invoice;

import org.openbravo.xmlEngine.XmlDocument;

public class AddPaymentFromInvoice extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      log4j.info("Inside AddPaymentFromInvoice file..used for purchase invoice window.");
      OBContext.setAdminMode();
      String strTabId = vars.getRequiredStringParameter("inpTabId");
      String strC_Invoice_ID = vars.getGlobalVariable("inpcInvoiceId", "");
      Invoice invoice = OBDal.getInstance().get(Invoice.class,strC_Invoice_ID);
      String strWindowPath = Utility.getTabURL(strTabId, "R", true);
      if (strWindowPath.equals(""))
  		strWindowPath = strDefaultServlet;
      String strOrg=null;
      String strPaymentIds="";
      String strDocNo=null;
      boolean bCheckExist=false;
      OBError message = null;
      String strPaymentMethodName=null;
      try {
    	  strOrg=invoice.getOrganization().getId();
    	  strPaymentMethodName=invoice.getPaymentMethod().getName();
    	  strPaymentIds=CheckPrintingCategoryData.getPaymentID(this, strOrg, strC_Invoice_ID);
    	  //if paymentid is null then give error to create payments..
    	
    	  log4j.info("strPaymentID......"+strPaymentIds);
    	  log4j.info("strPaymentMethodName.."+strPaymentMethodName);
    	  if(!strPaymentMethodName.equalsIgnoreCase("Check") && !strPaymentMethodName.equalsIgnoreCase("Cheque")){
    		  log4j.info("Payment method is not check so check will not be printed");
				message=new OBError();
				message.setType("Error");
				message.setMessage("Checks will be printed only for the payments whose payment method is check.");
				vars.setMessage(strTabId, message);
				printPageClosePopUp(response, vars);
				
  		  }else if("".equalsIgnoreCase(strPaymentIds)){
    		    log4j.info("No payments added to the invoice");
				message=new OBError();
				message.setType("Error");
				message.setMessage("Please add payments before printing the check.");
				vars.setMessage(strTabId, message);
				printPageClosePopUp(response, vars);
				
    	  }else{
    		  String strSelId="";
    		  strPaymentIds="("+strPaymentIds.substring(0, strPaymentIds.length()-1)+")";
    		  String strTemp[]=strPaymentIds.substring(1, strPaymentIds.length()-1).split(",");
    		  log4j.info("strTemp.length.."+strTemp.length);
    		  String strPaymentPrint="";
    		    for(int i=0;i<strTemp.length;i++){
    		  	  strSelId=strTemp[i];
    		  	  strSelId=strSelId.substring(1, strSelId.length()-1);
    		  	  log4j.info("strSelId.."+strSelId);
    		  	 bCheckExist = CheckPrintingCategoryData.getCheckExistWithoutVoid(this,strSelId);
    		  	 log4j.info("bCheckExist.."+bCheckExist);
    		  	 if(bCheckExist){
    		  		 
    		  	 }else{
    		  		strPaymentPrint=strPaymentPrint+"'"+strSelId+"',";
    		  	 }
    		  		 
    		    }
    		    if("".equalsIgnoreCase(strPaymentPrint)){
    		    	log4j.info("Check already printed.Use check management screen to reprint the check.");
    				message=new OBError();
    				message.setType("Error");
    				message.setMessage("Check already printed.Use check management screen to reprint the check.");
    				vars.setMessage(strTabId, message);
    				printPageClosePopUp(response, vars);
    		  }else{
            	  strPaymentPrint=strPaymentPrint.substring(0, strPaymentPrint.length()-1);
            	  log4j.info("strPaymentPrint.."+strPaymentPrint);
            	  CheckPrintingCategoryData[] datapaymnt = null;
            	  CheckPrintingCategory.deleteCheckDetails(this);
            	  String strTempPaymentPrint[]=strPaymentPrint.substring(1, strPaymentPrint.length()-1).split(",");
            	  for(int i=0;i<strTempPaymentPrint.length;i++){
        			  strSelId=strTempPaymentPrint[i];
        			  strSelId=strSelId.replaceAll("'","");
        			  log4j.info("strPaymentPrint.strSelId."+strSelId);
        			  CheckPrintingCategory.insertCheckDetails(this,"'"+strSelId+"'",-1, vars.getClient(), vars.getOrg(), "","N");
            	  }
            	 
            	 /* datapaymnt = CheckPrintingCategoryData.getPrintPaymentDetails(this,"('"+strSelId+"')");
            	  
            	  log4j.info("datapaymnt.length.."+datapaymnt.length);
            	  if(datapaymnt.length==0){
            		log4j.info("Some of the payments selected have a different payment method than the payment method set in the header.");
      				message=new OBError();
      				message.setType("Error");
      				message.setMessage("Some of the payments selected have a different payment method than the payment method set in the header.");
      				CheckPrintingCategory.deleteCheckDetails();
      				vars.setMessage(strTabId, message);
      				printPageClosePopUp(response, vars);
            	  }else{*/
            		 // CheckPrintingCategory.deleteCheckDetails();
            	  strPaymentPrint=strPaymentPrint.substring(1, strPaymentPrint.length()-1);
            	  log4j.info("strPaymentPrint.strPaymentPrint............"+strPaymentPrint);
            	  	request.getSession().setAttribute("PAYMENT_ID", strPaymentPrint);
            	    log4j.info("strPaymentPrint.getAttribute............"+request.getSession().getAttribute("PAYMENT_ID"));
            	      String strCheckPrintPath=strDireccion + "/org.openbravo.localization.us.checkprinting.ad_forms/CheckPrintingCategory.html" +
	            	  "?Command=DEFAULT&ORG="+strOrg+"&STRC_INVOICE_ID="+strC_Invoice_ID;
	                  log4j.info("Add Payment from Invoice....strCheckPrintPath...."+strCheckPrintPath);
	                  CustomPrintPageClosePopUp(request,response, vars, strCheckPrintPath);
            	 // }
              }
    	  }
    	
     
      } catch (Exception ex) {ex.printStackTrace();
      }finally {OBContext.restorePreviousMode();}
   }

  }
  protected void CustomPrintPageClosePopUp(HttpServletRequest request,HttpServletResponse response, VariablesSecureApp vars,
	      String path) throws IOException, ServletException {
	    if (log4j.isDebugEnabled())
	      log4j.info("Output: CustomPrintPageClosePopUp");
	    final XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
	        "org/openbravo/base/secureApp/PopUp_Response").createXmlDocument();
	    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	    xmlDocument.setParameter("href", path.equals("") ? "null" : "'" + path + "'");
	    response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();
	    out.println(xmlDocument.print());
	    out.close();
  }
  
  
  public String getServletInfo() {
	    return "AddPaymentFromInvoice Servlet. This Servlet was made by   Mallikarjun M";
  } 
}
