/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * Mallikarjun M
 ************************************************************************************
 */
package org.openbravo.localization.us.checkprinting.advpaymentmngt.ad_actionbutton;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintingCategory;
import org.openbravo.localization.us.checkprinting.ad_forms.CheckPrintingCategoryData;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentProposal;
import org.openbravo.xmlEngine.XmlDocument;

public class ProcessPaymentProposal extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT")) {
    	log4j.info("Inside ProcessPayment Proposal file.used for payment proposal window..");
		String strTabId = vars.getRequiredStringParameter("inpTabId");
		String strFinPaymentProposalId = vars.getRequiredStringParameter("inpfinPaymentProposalId");
		OBContext.setAdminMode();
		String strWindowPath = Utility.getTabURL(strTabId, "R", true);
	      if (strWindowPath.equals(""))
	        strWindowPath = strDefaultServlet;

		FIN_PaymentProposal fpp = null;
		String strDocNo = null,strOrg = null;
		boolean bCheckExist=false;
	    OBError message = null;
	    String strPaymentId=null;
	    String strPaymentMethodName=null;
	    fpp = OBDal.getInstance().get(FIN_PaymentProposal.class, strFinPaymentProposalId);
	    strDocNo=fpp.getDocumentNo();
        strOrg=fpp.getOrganization().getId();
       try {
    	   strPaymentMethodName=fpp.getPaymentMethod().getName();
    	   strPaymentId=CheckPrintingCategoryData.getProposalPaymentID(this,strFinPaymentProposalId);
    	   bCheckExist= CheckPrintingCategoryData.getCheckExistWithoutVoid(this,strPaymentId);
    	   log4j.info("strPaymentMethodName.."+strPaymentMethodName);
    	   if(!strPaymentMethodName.equalsIgnoreCase("Check") && !strPaymentMethodName.equalsIgnoreCase("Cheque")){
     		  log4j.info("Payment method is not check so check will not be printed");
 				message=new OBError();
 				message.setType("Error");
 				message.setMessage("Checks can be printed only for the Invoices whose payment method is Check.");
 				vars.setMessage(strTabId, message);
 				printPageClosePopUp(response, vars);
 				
 		  }else if(bCheckExist){
				log4j.info("Check already printed.Use check management screen to reprint the check.");
				message=new OBError();
				message.setType("Error");
				message.setMessage("Check already printed.Use check management screen to reprint the check.");
				vars.setMessage(strTabId, message);
				printPageClosePopUp(response, vars);
				
         }else{
        	  log4j.info("Inside PorcessPaymentProposal file.strPaymentId..."+strPaymentId);
        	  CheckPrintingCategoryData[] datapaymnt = null;
        	  CheckPrintingCategory.deleteCheckDetails(this);
        	  CheckPrintingCategory.insertCheckDetails(this,"'"+strPaymentId+"'",-1, vars.getClient(), vars.getOrg(), "","N");
        	  datapaymnt = CheckPrintingCategoryData.getPrintPaymentDetails(this,"('"+strPaymentId+"')");
        	  
        	  log4j.info("datapaymnt.length.."+datapaymnt.length);
        	  if(datapaymnt.length==0){
        		log4j.info("Some of the payments selected have a different payment method than the payment method set in the header.");
  				message=new OBError();
  				message.setType("Error");
  				message.setMessage("Some of the payments selected have a different payment method than the payment method set in the header.");
  				CheckPrintingCategory.deleteCheckDetails(this);
  				vars.setMessage(strTabId, message);
  				printPageClosePopUp(response, vars);
        	  }else{
        		//  CheckPrintingCategory.deleteCheckDetails();
        		String strCheckPrintPath=strDireccion + "/org.openbravo.localization.us.checkprinting.ad_forms/CheckPrintingCategory.html" +
                "?Command=DEFAULT&ORG="+strOrg+"&PAYDOCNO="+strDocNo+"&PAYMENT_ID="+strPaymentId;
                log4j.info("the checkprinting path is...."+strCheckPrintPath);
                CustomPrintPageClosePopUp(request,response, vars, strCheckPrintPath);
        	  }
        	
         }
           log4j.info("fpp..."+fpp);
          } catch (Exception ex) {ex.printStackTrace();
          }finally {OBContext.restorePreviousMode();}

    }
  }
  protected void CustomPrintPageClosePopUp(HttpServletRequest request,HttpServletResponse response, VariablesSecureApp vars,
	      String path) throws IOException, ServletException {
	    if (log4j.isDebugEnabled())
	        log4j.debug("Output: PopUp Response");
	      final XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
	          "org/openbravo/base/secureApp/PopUp_Response").createXmlDocument();
	      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	      xmlDocument.setParameter("href", path.equals("") ? "null" : "'" + path + "'");
	      response.setContentType("text/html; charset=UTF-8");
	      final PrintWriter out = response.getWriter();
	      out.println(xmlDocument.print());
	      out.close();
  }
  
  public String getServletInfo() {
	    return "ProcessPaymentProposal Servlet. This Servlet was made by  Mallikarjun M";
} 
}
