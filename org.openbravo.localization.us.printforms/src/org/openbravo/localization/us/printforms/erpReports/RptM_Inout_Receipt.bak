<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="RptM_Inout_Receipt"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="612"
		 pageHeight="792"
		 columnWidth="540"
		 columnSpacing="0"
		 leftMargin="36"
		 rightMargin="36"
		 topMargin="28"
		 bottomMargin="28"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<style 
		name="default"
		isDefault="false"
		vAlign="Middle"
		fontName="Bitstream Vera Sans"
		fontSize="10"
	/>
	<style 
		name="Report_Title"
		isDefault="false"
		leftPadding="5"
		fontName="Bitstream Vera Sans"
		fontSize="18"
	/>
	<style 
		name="Report_Subtitle"
		isDefault="false"
		forecolor="#555555"
		leftPadding="5"
		fontName="Bitstream Vera Sans"
		fontSize="14"
	/>
	<style 
		name="Total_Field"
		isDefault="false"
		mode="Opaque"
		forecolor="#000000"
		backcolor="#CCCCCC"
		vAlign="Middle"
		leftPadding="5"
	/>
	<style 
		name="Detail_Header"
		isDefault="false"
		mode="Opaque"
		forecolor="#FFFFFF"
		backcolor="#555555"
		vAlign="Middle"
		leftBorder="1Point"
		leftBorderColor="#FFFFFF"
		leftPadding="5"
		rightBorder="1Point"
		rightBorderColor="#FFFFFF"
	/>
	<style 
		name="GroupHeader_DarkGray"
		isDefault="false"
		mode="Opaque"
		forecolor="#FFFFFF"
		backcolor="#555555"
		vAlign="Middle"
		leftPadding="5"
		fontName="Bitstream Vera Sans"
		fontSize="14"
	/>
	<style 
		name="GroupHeader_Gray"
		isDefault="false"
		mode="Opaque"
		backcolor="#999999"
	/>
	<style 
		name="Detail_Line"
		isDefault="false"
	>

		<conditionalStyle>
			<conditionExpression><![CDATA[new Boolean($V{REPORT_COUNT}.intValue()%2==1)]]></conditionExpression>
			<style 
				name="Detail_Line"
				isDefault="false"
				mode="Opaque"
				backcolor="#CCCCCC"
			/>
		</conditionalStyle>
	</style>
	<style 
		name="Total_Gray"
		isDefault="false"
		mode="Opaque"
		forecolor="#000000"
		backcolor="#CCCCCC"
	/>

	<parameter name="ATTACH" isForPrompting="false" class="java.lang.String"/>
	<parameter name="BASE_WEB" isForPrompting="false" class="java.lang.String"/>
	<parameter name="BASE_DESIGN" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["/opt2/openbravo/src"]]></defaultValueExpression>
	</parameter>
	<parameter name="LANGUAGE" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["en_US"]]></defaultValueExpression>
	</parameter>
	<parameter name="USER_CLIENT" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["(1000000)"]]></defaultValueExpression>
	</parameter>
	<parameter name="USER_ORG" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["(0)"]]></defaultValueExpression>
	</parameter>
	<parameter name="REPORT_TITLE" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA["REPORT TITLE"]]></defaultValueExpression>
	</parameter>
	<parameter name="REPORT_SUBTITLE" isForPrompting="false" class="java.lang.String"/>
	<parameter name="SUBREPORT_DIR" isForPrompting="false" class="java.lang.String">
		<defaultValueExpression ><![CDATA[$P{BASE_DESIGN} + "/src/org/openbravo/erpReports/"]]></defaultValueExpression>
	</parameter>
	<parameter name="DOCUMENT_ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="SUBREP_RptM_InoutLine_Receipt" isForPrompting="false" class="net.sf.jasperreports.engine.JasperReport"/>
	<queryString><![CDATA[SELECT 
    tdsiov.ad_client_id, 
    tdsiov.ad_org_id, 
    c_order_id, 
    m_inout_id, 
    issotrx, 
    so_number, 
    ship_number, 
    printstatus, 
    docstatus, 
    c_doctype_id, 
    tdsiov.c_bpartner_id, 
    bpvalue, 
    org_location_id, 
    documenttype, 
    documenttypenote, 
    salesrep_id, 
    salesrep_name, 
    rep_email, 
    dateordered, 
    datepromised, 
    shipdate, 
    trackingno, 
    movementdate, 
    movementtype, 
    tdsiov.name, 
    tdsiov.name2, 
    contactname, 
    tdsiov.c_location_id, 
    referenceno, 
    tdsiov.description, 
    poreference, 
    currency, 
    paymentterm, 
    paymenttermnote, 
    c_charge_id, 
    chargeamt, 
    totallines, 
    grandtotal, 
    m_pricelist_id, 
    istaxincluded, 
    c_campaign_id, 
    c_project_id, 
    c_activity_id, 
    shipper, 
    deliveryrule, 
    priorityrule, 
    invoicerule, 
    sold_to_address, 
    sold_to_city_state, 
    sold_to_country, 
    ship_to_address, 
    ship_to_city_state, 
    ship_to_country, 
    isprinted, 
    dateprinted, 
    org.value, 
    org.name                  as orgname, 
    coalesce(loc.address1,'') as address1, 
    coalesce(loc.address2,'') as address2, 
    CASE 
        WHEN loc.address2 is null     and loc.address1 is not null 
        THEN coalesce(loc.address1,'') 
        WHEN loc.address2 is not null and loc.address1 is null 
        THEN Coalesce(loc.address2,'') 
        WHEN loc.address2 is not null and loc.address1 is not null 
        THEN Coalesce(loc.address1,'') || ', ' || loc.address2 
        ELSE '' 
    END                               as addressLine, 
    coalesce(loc.city,city.name)      as city, 
    coalesce(loc.regionname,reg.name) as region, 
    CASE 
        WHEN Coalesce(loc.regionname,reg.name) is null and Coalesce(loc.city,city.name) is not null 
        THEN coalesce(Coalesce(loc.city,city.name),'') 
        WHEN Coalesce(loc.regionname,reg.name) is not null and Coalesce(loc.city,city.name) is null 
        THEN Coalesce(Coalesce(loc.regionname,reg.name),'') 
        WHEN Coalesce(loc.regionname,reg.name) is not null and Coalesce(loc.city,city.name) is not null 
        THEN Coalesce(Coalesce(loc.city,city.name),'') || ', ' || Coalesce(loc.regionname,reg.name) 
        ELSE '' 
    END as CityRegionLine, 
    loc.postal, 
    loc.postal_add, 
    ctry.name as country  
FROM 
    TFRM_M_INOUT_VIEW tdsiov 
INNER JOIN 
    ad_org org 
    ON 
    ( 
        org.ad_org_id = tdsiov.ad_org_id 
    ) 
INNER JOIN 
    ad_orginfo orginfo 
    ON 
    ( 
        orginfo.ad_org_id = tdsiov.ad_org_id 
    ) 
LEFT JOIN 
    c_location loc 
    ON 
    ( 
        loc.c_location_id = orginfo.c_location_id 
    ) 
LEFT JOIN 
    c_region reg 
    ON 
    ( 
        reg.c_region_id = loc.c_region_id 
    ) 
LEFT JOIN 
    c_country ctry 
    ON 
    ( 
        ctry.c_country_id = loc.c_country_id 
    ) 
LEFT JOIN 
    c_city city 
    ON 
    ( 
        city.c_city_id = loc.c_city_id 
    ) 
WHERE 
    tdsiov.M_INOUT_ID = $P{DOCUMENT_ID}]]></queryString>

	<field name="ad_client_id" class="java.lang.String"/>
	<field name="ad_org_id" class="java.lang.String"/>
	<field name="c_order_id" class="java.lang.String"/>
	<field name="m_inout_id" class="java.lang.String"/>
	<field name="issotrx" class="java.lang.String"/>
	<field name="so_number" class="java.lang.String"/>
	<field name="ship_number" class="java.lang.String"/>
	<field name="printstatus" class="java.lang.String"/>
	<field name="docstatus" class="java.lang.String"/>
	<field name="c_doctype_id" class="java.lang.String"/>
	<field name="c_bpartner_id" class="java.lang.String"/>
	<field name="bpvalue" class="java.lang.String"/>
	<field name="org_location_id" class="java.lang.String"/>
	<field name="documenttype" class="java.lang.String"/>
	<field name="documenttypenote" class="java.lang.String"/>
	<field name="salesrep_id" class="java.lang.String"/>
	<field name="salesrep_name" class="java.lang.String"/>
	<field name="rep_email" class="java.lang.String"/>
	<field name="dateordered" class="java.sql.Timestamp"/>
	<field name="datepromised" class="java.sql.Timestamp"/>
	<field name="shipdate" class="java.sql.Timestamp"/>
	<field name="trackingno" class="java.lang.String"/>
	<field name="movementdate" class="java.sql.Timestamp"/>
	<field name="movementtype" class="java.lang.String"/>
	<field name="name" class="java.lang.String"/>
	<field name="name2" class="java.lang.String"/>
	<field name="contactname" class="java.lang.String"/>
	<field name="c_location_id" class="java.lang.String"/>
	<field name="referenceno" class="java.lang.String"/>
	<field name="description" class="java.lang.String"/>
	<field name="poreference" class="java.lang.String"/>
	<field name="currency" class="java.lang.String"/>
	<field name="paymentterm" class="java.lang.String"/>
	<field name="paymenttermnote" class="java.lang.String"/>
	<field name="c_charge_id" class="java.lang.String"/>
	<field name="chargeamt" class="java.math.BigDecimal"/>
	<field name="totallines" class="java.math.BigDecimal"/>
	<field name="grandtotal" class="java.math.BigDecimal"/>
	<field name="m_pricelist_id" class="java.lang.String"/>
	<field name="istaxincluded" class="java.lang.String"/>
	<field name="c_campaign_id" class="java.lang.String"/>
	<field name="c_project_id" class="java.lang.String"/>
	<field name="c_activity_id" class="java.lang.String"/>
	<field name="shipper" class="java.lang.String"/>
	<field name="deliveryrule" class="java.lang.String"/>
	<field name="priorityrule" class="java.lang.String"/>
	<field name="invoicerule" class="java.lang.String"/>
	<field name="sold_to_address" class="java.lang.String"/>
	<field name="sold_to_city_state" class="java.lang.String"/>
	<field name="sold_to_country" class="java.lang.String"/>
	<field name="ship_to_address" class="java.lang.String"/>
	<field name="ship_to_city_state" class="java.lang.String"/>
	<field name="ship_to_country" class="java.lang.String"/>
	<field name="isprinted" class="java.lang.String"/>
	<field name="dateprinted" class="java.sql.Timestamp"/>
	<field name="value" class="java.lang.String"/>
	<field name="orgname" class="java.lang.String"/>
	<field name="address1" class="java.lang.String"/>
	<field name="address2" class="java.lang.String"/>
	<field name="addressline" class="java.lang.String"/>
	<field name="city" class="java.lang.String"/>
	<field name="region" class="java.lang.String"/>
	<field name="cityregionline" class="java.lang.String"/>
	<field name="postal" class="java.lang.String"/>
	<field name="postal_add" class="java.lang.String"/>
	<field name="country" class="java.lang.String"/>


		<group  name="C_ORDER_ID" isStartNewColumn="true" isStartNewPage="true" isResetPageNumber="true" >
			<groupExpression><![CDATA[]]></groupExpression>
			<groupHeader>
			<band height="137"  isSplitAllowed="true" >
				<line direction="TopDown">
					<reportElement
						x="10"
						y="136"
						width="516"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="22"
						y="9"
						width="72"
						height="14"
						key="staticText-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Vender Pay To :
]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="10"
						y="3"
						width="517"
						height="1"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="22"
						y="89"
						width="72"
						height="14"
						key="staticText-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Order Date :
]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="277"
						y="8"
						width="91"
						height="14"
						key="staticText-17"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Vendor Ship From :
]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="22"
						y="24"
						width="220"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{name}==null)?"":$F{name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="22"
						y="39"
						width="220"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{sold_to_address}==null)?"":$F{sold_to_address}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="22"
						y="55"
						width="220"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{sold_to_city_state}==null)?"":$F{sold_to_city_state}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="22"
						y="119"
						width="72"
						height="14"
						key="staticText-23"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Bill of Lading	:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="277"
						y="23"
						width="245"
						height="14"
						key="textField-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{name}==null)?"":$F{name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="277"
						y="39"
						width="245"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{ship_to_address}==null)?"":$F{ship_to_address}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="277"
						y="54"
						width="245"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{ship_to_city_state}==null)?"":$F{ship_to_city_state}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="22"
						y="104"
						width="72"
						height="14"
						key="staticText-32"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Ship Method : ]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="10"
						y="85"
						width="516"
						height="0"
						key="line-4"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="247"
						y="3"
						width="1"
						height="83"
						key="line-5"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="527"
						y="4"
						width="0"
						height="132"
						key="line-6"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="10"
						y="4"
						width="0"
						height="132"
						key="line-7"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="94"
						y="89"
						width="100"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{dateordered}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="94"
						y="104"
						width="100"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{shipper}==null)?"":$F{shipper}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="94"
						y="119"
						width="100"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{trackingno}==null)?"":$F{trackingno}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="231"
						y="89"
						width="56"
						height="14"
						key="staticText-38">
							<printWhenExpression><![CDATA[new java.lang.Boolean(0==1)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Pallets :
]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="231"
						y="104"
						width="56"
						height="14"
						key="staticText-40">
							<printWhenExpression><![CDATA[new java.lang.Boolean(0==1)]]></printWhenExpression>
						</reportElement>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Cartons : ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="22"
						y="71"
						width="220"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{sold_to_country}==null)?"":$F{sold_to_country}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="277"
						y="70"
						width="245"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{ship_to_country}==null)?"":$F{ship_to_country}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="89"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="444"
						y="69"
						width="34"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-9"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="422"
						y="69"
						width="19"
						height="14"
						key="textField-10"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="266"
						y="39"
						width="153"
						height="14"
						key="staticText-12"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Purchase Order Number :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="274"
						y="69"
						width="145"
						height="14"
						key="staticText-18"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Page Number :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="266"
						y="24"
						width="153"
						height="14"
						key="staticText-36"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Receipt Number :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="266"
						y="54"
						width="153"
						height="14"
						key="staticText-37"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Receipt Date :]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="422"
						y="39"
						width="100"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{so_number}==null)?"":$F{so_number}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="422"
						y="24"
						width="100"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{ship_number}==null)?"":$F{ship_number}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="422"
						y="54"
						width="100"
						height="14"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{movementdate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="266"
						y="5"
						width="256"
						height="19"
						key="staticText-45"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Material Receipt]]></text>
				</staticText>
				<image  evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="45"
						y="5"
						width="111"
						height="35"
						key="image-1"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA[$P{BASE_DESIGN}.replace("src-loc/design","WEB-INF") + "/images/logo.png"]]></imageExpression>
				</image>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="22"
						y="42"
						width="220"
						height="11"
						key="textField-13"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{orgname}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="22"
						y="53"
						width="220"
						height="11"
						key="textField-14"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{addressline}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="22"
						y="64"
						width="220"
						height="11"
						key="textField-15"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{cityregionline}+ (($F{postal}==null)?"":" "+ $F{postal}) + (($F{postal_add}==null)?"":"-" + $F{postal_add})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="22"
						y="75"
						width="220"
						height="11"
						key="textField-16"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{country}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="43"  isSplitAllowed="true" >
				<subreport  isUsingCache="true">
					<reportElement
						x="1"
						y="3"
						width="539"
						height="38"
						key="subreport-1"/>
					<subreportParameter  name="M_INOUT_ID">
						<subreportParameterExpression><![CDATA[$P{DOCUMENT_ID}]]></subreportParameterExpression>
					</subreportParameter>
					<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					<subreportExpression  class="net.sf.jasperreports.engine.JasperReport"><![CDATA[$P{SUBREP_RptM_InoutLine_Receipt}]]></subreportExpression>
				</subreport>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="91"  isSplitAllowed="true" >
				<rectangle>
					<reportElement
						x="5"
						y="1"
						width="520"
						height="73"
						key="rectangle-1"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="242"
						y="75"
						width="59"
						height="14"
						key="textField-11"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="186"
						y="75"
						width="55"
						height="14"
						key="staticText-9"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" rightPadding="4" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8"/>
					</textElement>
				<text><![CDATA[printed on]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="89"
						width="540"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="71"
						y="4"
						width="451"
						height="35"
						key="textField"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement>
						<font pdfFontName="Helvetica" size="8" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{description}==null)?"":$F{description}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="5"
						y="4"
						width="67"
						height="35"
						key="staticText-27"/>
					<box topBorder="None" topBorderColor="#000000" leftBorder="None" leftBorderColor="#000000" rightBorder="None" rightBorderColor="#000000" bottomBorder="None" bottomBorderColor="#000000"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[ Special 
 Instructions :]]></text>
				</staticText>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
