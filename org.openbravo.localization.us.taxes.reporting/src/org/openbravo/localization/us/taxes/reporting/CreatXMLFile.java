/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html

 ************************************************************************************
 */
package org.openbravo.localization.us.taxes.reporting;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.reference.PInstanceProcessData;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessContext;
import org.openbravo.service.db.DalBaseProcess;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.openbravo.data.UtilSql;

public class CreatXMLFile extends DalBaseProcess {

  private ArrayList<String> Values = new ArrayList<String>();
  private HashMap<Integer, ArrayList<String>> hp = new HashMap<Integer, ArrayList<String>>();
  private List<String> myData;
  private ProcessContext context = null;
  private ConnectionProvider connectionProvider = null;
  private Connection connection = null;
  private static Logger log4j = Logger.getLogger(CreatXMLFile.class);
  public String elementname;
  public String parentelementname;
  public String seqno;

  private Document dom = null;
  CreatXMLFile[] CreatXMLFileData=null;
  public void doExecute(ProcessBundle pb) throws Exception {
    try {

      // Get the Connection Provider
      connectionProvider = pb.getConnection();
      context = pb.getContext();

      // Get Connection
      connection = connectionProvider.getConnection();
      createXml();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void createXml() throws Exception {
    try {
    	 CreatXMLFileData  = getValues();
      // Values = getValues();

      // myData = new ArrayList<String>();

      // initialize the list
      // loadData();

      // Get a DOM object
      createDocument();
      createDOMTree();
      File f = printToFile();
      System.out.println("Generated file successfully....." + f.getName());
    } catch (Exception e) {
      log4j.info("createXml Exception......" + e.getMessage());
      // TODO: handle exception
    }
  }

  public CreatXMLFile[] getValues() throws Exception {

    ArrayList<String> values = new ArrayList<String>();
    HashMap<Integer, ArrayList<String>> map = new HashMap<Integer, ArrayList<String>>();
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    try {
      String clientID = context.getClient();
      String strSql = "select ele.name as elementname, pele.name as parentelementname, node.seqno as seqno"
          + " from"
          + " ad_accountingrpt_element ele, ad_treenode node, ad_accountingrpt_element pele"
          + " where node.ad_client_id='"
          + clientID
          + "' and"
          + " ele.ad_accountingrpt_element_id = node.node_id and"
          + " ele.ad_client_id = node.ad_client_id and"
          + " pele.ad_accountingrpt_element_id = node.parent_id "
          + " union"
          + " select ele.name as elementname, '0' as parentelementname, node.seqno"
          + " from"
          + " ad_accountingrpt_element ele, ad_treenode node"
          + " where "
          + " ele.ad_accountingrpt_element_id = node.node_id and"
          + " ele.ad_client_id = node.ad_client_id and"
          + " node.parent_id='0'"
          + " order by elementname,seqno";

      log4j.info("strSql......" + strSql);

      ResultSet result;

      PreparedStatement st = null;

      int iParameter = 0;

      st = connectionProvider.getPreparedStatement(strSql);
      iParameter++;
      // UtilSql.setValue(st, iParameter, 12, null, internalPONumber);

      result = st.executeQuery();
      while (result.next()) {
    	  CreatXMLFile objectCreatXMLFile = new CreatXMLFile();
    	  objectCreatXMLFile.parentelementname=UtilSql.getValue(result, "parentelementname");
    	  objectCreatXMLFile.elementname=UtilSql.getValue(result, "elementname");
    	  objectCreatXMLFile.seqno=UtilSql.getValue(result, "seqno");
    	  vector.addElement(objectCreatXMLFile);

      }
      result.close();
    } catch (SQLException e) {
      // log4j.error("SQL error in query: " + strSql + "Exception:" + e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@"
          + e.getMessage());
    } catch (Exception ex) {
      // log4j.error("Exception in query: " + strSql + "Exception:" + ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    }
    CreatXMLFile objectCreatXMLFile[] = new CreatXMLFile[vector.size()];
    vector.copyInto(objectCreatXMLFile);
    return(objectCreatXMLFile);
  }

  public void loadData() {
    try {
      System.out.println("loadData.......");
      myData.add(Values.get(0));
      myData.add(Values.get(1));
      myData.add(Values.get(2));
    } catch (Exception e) {
      log4j.info("loadData Exception......" + e.getMessage());// TODO: handle exception
    }
  }

  /**
   * Using JAXP in implementation independent manner create a document object using which we create
   * a xml tree in memory
   */
  public void createDocument() {

    // get an instance of factory
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    try {
      // get an instance of builder
      DocumentBuilder db = dbf.newDocumentBuilder();

      // create an instance of DOM
      dom = db.newDocument();

    } catch (Exception e) {
      log4j.info("createDocument Exception......" + e.getMessage());// TODO: handle exception
    }

  }

  /**
   * The real workhorse which creates the XML structure
   */
  public void createDOMTree() {
    try {

      // create the root element <wyoming>
      Element rootEle = dom.createElement("Openbravo");
      dom.appendChild(rootEle);

      // No enhanced for
      for (int i = 0; i < CreatXMLFileData.length; i++) {
        Element bookEle = createElement(CreatXMLFileData[i], "TreeNode");
        rootEle.appendChild(bookEle);

      }
    } catch (Exception e) {
      log4j.info("createDOMTree Exception......" + e.getMessage());// TODO: handle exception
    }
  }

  @SuppressWarnings("unchecked")
  public Element createElement(CreatXMLFile CreatXMLFileData, String tagType) {

    Element bookEle = dom.createElement(tagType);
    try {

      // create Source element and Source text node and attach it to bookElement
      Element SourceEle = dom.createElement("ElementName");
      Text SourceText = dom.createTextNode(CreatXMLFileData.elementname);
      SourceEle.appendChild(SourceText);
      bookEle.appendChild(SourceEle);

      // create CustomerPO element and CustomerPO text node and attach it to bookElement
      Element CustomerPOEle = dom.createElement("ParentElementName");
      Text CustomerPOText = dom.createTextNode(CreatXMLFileData.parentelementname);
      CustomerPOEle.appendChild(CustomerPOText);
      bookEle.appendChild(CustomerPOEle);

      // create BuyerName element and BuyerName text node and attach it to bookElement
      Element BuyerNameEle = dom.createElement("Sqno");
      Text BuyerNameText = dom.createTextNode(CreatXMLFileData.seqno);
      BuyerNameEle.appendChild(BuyerNameText);
      bookEle.appendChild(BuyerNameEle);

    } catch (Exception e) {
      log4j.info("createElement Exception......" + e.getMessage());// TODO: handle exception
    }
    return bookEle;

  }

  /**
   * @param strOrginal
   * @param strOld
   * @param strNew
   * @return
   */
  private String replaceString(String strOrginal, String strOld, String strNew) {
    if (strOrginal == null || strOld == null || strNew == null || strOrginal.equals("")
        || strOld.equals("") || strNew.equals(""))
      return strOrginal;

    int intstartIndex;
    int intlen;
    int intlen2;
    int intlastPosition = 0;
    char[] charArray;
    StringBuffer buff = new StringBuffer(strOrginal);
    while (intlastPosition > -1) {
      intstartIndex = strOrginal.indexOf(strOld, intlastPosition);
      if (intstartIndex == -1)
        break;

      intlen = strOld.length();
      buff.delete(intstartIndex, intstartIndex + intlen);
      charArray = strNew.toCharArray();
      buff.insert(intstartIndex, charArray);
      strOrginal = buff.toString();
      intlen2 = intstartIndex + strNew.length();
      intlastPosition = strOrginal.indexOf(strOld, intlen2);
    }

    return buff.toString();
  }

  @SuppressWarnings("deprecation")
  public File printToFile() {
    URL url = getClass().getResource("/");
    String path = url.getPath();
    String packageName = getClass().getPackage().toString();
    packageName = replaceString(packageName, "package ", " ");
    String newLink = replaceString(packageName, ".", "/");

    path = path + newLink.trim() + "/referencedata/standard/";

    File file = new File(path + "ParentNode.xml");
    log4j.info("path ......" + path);
    try {
      // print
      OutputFormat format = new OutputFormat(dom);
      format.setIndenting(true);

      XMLSerializer serializer = new XMLSerializer(new FileOutputStream(file), format);

      serializer.serialize(dom);

    } catch (IOException e) {
      log4j.info("printToFile Exception......" + e.getMessage());// TODO: handle exception
    }
    return file;
  }

}