/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html

 ************************************************************************************
 */
package org.openbravo.localization.us.taxes.reporting;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessContext;
import org.openbravo.service.db.DalBaseProcess;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TaxSummaryReadXml extends DalBaseProcess {

  private ProcessContext context = null;
  private ConnectionProvider connectionProvider = null;
  private Connection connection = null;
  int i = 0;
  private static Logger log4j = Logger.getLogger(TaxSummaryReadXml.class);

  /**
   * The code inside this function will be executed when the process is scheduled through the
   * Process Scheduler
   *
   * @param bundle
   *          the process' parameters, security and contextual information
   * @throws Exception
   *           if an error occurs executing the process
   */
  @SuppressWarnings("null")
  public void doExecute(ProcessBundle pb) throws Exception {
    connectionProvider = pb.getConnection();
    context = pb.getContext();

    // Get Connection
    connection = connectionProvider.getConnection();

    // TODO Auto-generated method stub
    try {
      URL url = getClass().getResource("/");
      String path = url.getPath();
      String packageName = getClass().getPackage().toString();
      packageName = replaceString(packageName, "package ", " ");
      String newLink = replaceString(packageName, ".", "/");
      log4j.info("newLink......" + newLink);
      path = path + newLink.trim() + "/referencedata/standard/TaxSummaryReport.xml";
      log4j.info("path......" + path);
      File file = new File(path);
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      Document doc = db.parse(file);
      doc.getDocumentElement().normalize();
      NodeList nodeLst = doc.getElementsByTagName("FinancialMgmtAccountingRptElement");
      String strAccGrptElemtid = "", strIsActive = "Y";
      String strName = "", strDesp = "", strIsSummary = "Y", strShown = "Y", strAccId = null, strAccSchemaId = "", strReport = "N";
      String strReportType = "", strFilterByOrg = "", strIntBalance = "Y", strReportingInterval = null;
      String acctIdentifier = null;

      String clientID = context.getClient();
      // log4j.info("clientID in TaxSummaryReadXml....>" + clientID);
      String orgID = context.getOrganization();
      // log4j.info("orgID in TaxSummaryReadXml....>" + orgID);
      String userID = context.getUser();
      // log4j.info("userID in TaxSummaryReadXml....>" + userID);

      //deleteOld(clientID);

      for (int s = 0; s < nodeLst.getLength(); s++) {

        Node fstNode = nodeLst.item(s);

        if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

          Element fstElmnt = (Element) fstNode;
          NodeList idNmElmntLst = fstElmnt.getElementsByTagName("id");
          Element idNmElmnt = (Element) idNmElmntLst.item(0);
          NodeList idNm = idNmElmnt.getChildNodes();
          // log4j.info("id: " + ((Node) idNm.item(0)).getNodeValue());
          // strAccGrptElemtid = ((Node) idNm.item(0)).getNodeValue();
          strAccGrptElemtid = SequenceIdData.getUUID();

          NodeList activeNmElmntLst = fstElmnt.getElementsByTagName("active");
          Element activeNmElmnt = (Element) activeNmElmntLst.item(0);
          NodeList activeNm = activeNmElmnt.getChildNodes();
          // log4j.info("active: " + ((Node) activeNm.item(0)).getNodeValue());
          strIsActive = "" + ((Node) activeNm.item(0)).getNodeValue();
          if ("false".equalsIgnoreCase(strIsActive)) {
            strIsActive = "N";
          } else {
            strIsActive = "Y";
          }
          NodeList nameNmElmntLst = fstElmnt.getElementsByTagName("name");
          Element nameNmElmnt = (Element) nameNmElmntLst.item(0);
          NodeList nameNm = nameNmElmnt.getChildNodes();
          // log4j.info("name: " + ((Node) nameNm.item(0)).getNodeValue());
          strName = ((Node) nameNm.item(0)).getNodeValue();
          NodeList descriptionNmElmntLst = fstElmnt.getElementsByTagName("description");
          Element descriptionNmElmnt = (Element) descriptionNmElmntLst.item(0);
          descriptionNmElmnt.getAttribute("xsi:nil");
          // log4j.info("getAttribute : " + descriptionNmElmnt.getAttribute("xsi:nil"));
          NodeList descriptionNm = descriptionNmElmnt.getChildNodes();

          strDesp = "" + (Node) descriptionNm.item(0);
          if (!strDesp.equals("null")) {
            strDesp = "" + ((Node) descriptionNm.item(0)).getNodeValue();
          } else {
            strDesp = "";
          }
          // log4j.info("description : " + ((Node) descriptionNm.item(0)));

          NodeList summaryLevelNmElmntLst = fstElmnt.getElementsByTagName("summaryLevel");
          Element summaryLevelNmElmnt = (Element) summaryLevelNmElmntLst.item(0);
          NodeList summaryLevelNm = summaryLevelNmElmnt.getChildNodes();
          // log4j.info("summaryLevel: " + ((Node) summaryLevelNm.item(0)).getNodeValue());
          strIsSummary = "" + ((Node) summaryLevelNm.item(0)).getNodeValue();
          if ("false".equalsIgnoreCase(strIsSummary)) {
            strIsSummary = "N";
          } else {
            strIsSummary = "Y";
          }

          NodeList shownNmElmntLst = fstElmnt.getElementsByTagName("shown");
          Element shownNmElmnt = (Element) shownNmElmntLst.item(0);
          NodeList shownNm = shownNmElmnt.getChildNodes();
          // log4j.info("shown : " + ((Node) shownNm.item(0)).getNodeValue());
          strShown = "" + ((Node) shownNm.item(0)).getNodeValue();

          if ("false".equalsIgnoreCase(strShown)) {
            strShown = "N";
          } else {
            strShown = "Y";
          }
          NodeList filteredByOrganizationNmElmntLst = fstElmnt
              .getElementsByTagName("filteredByOrganization");
          Element filteredByOrganizationNmElmnt = (Element) filteredByOrganizationNmElmntLst
              .item(0);
          NodeList filteredByOrganizationNm = filteredByOrganizationNmElmnt.getChildNodes();
          // log4j.info("filteredByOrganization: " + ((Node)
          // filteredByOrganizationNm.item(0)).getNodeValue());
          strFilterByOrg = "" + ((Node) filteredByOrganizationNm.item(0)).getNodeValue();

          if ("false".equalsIgnoreCase(strFilterByOrg)) {
            strFilterByOrg = "N";
          } else {
            strFilterByOrg = "Y";
          }

          NodeList reportingIntervalNmElmntLst = fstElmnt.getElementsByTagName("reportingInterval");
          Element reportingIntervalNmElmnt = (Element) reportingIntervalNmElmntLst.item(0);
          NodeList reportingIntervalNm = reportingIntervalNmElmnt.getChildNodes();
          // log4j.info("reportingInterval : " +
          strReportingInterval = ((Node) reportingIntervalNm.item(0)).getNodeValue();
          System.out
              .println("strReportingInterval ........................" + strReportingInterval);
          NodeList accountNmElmntLst = fstElmnt.getElementsByTagName("account");
          Element accountNmElmnt = (Element) accountNmElmntLst.item(0);
          NodeList accountNm = accountNmElmnt.getChildNodes();
          // log4j.info("account id : " + accountNmElmnt.getAttribute("id"));
          // log4j.info("account entity-name : " +
          // accountNmElmnt.getAttribute("entity-name"));
          // log4j.info("account identifier : " +
          // accountNmElmnt.getAttribute("identifier"));
          strAccId = accountNmElmnt.getAttribute("id");
          log4j.info("strAccId test: " + strAccId);
          NodeList accountingSchemaNmElmntLst = fstElmnt.getElementsByTagName("accountingSchema");
          Element accountingSchemaNmElmnt = (Element) accountingSchemaNmElmntLst.item(0);
          NodeList accountingSchemaNm = accountingSchemaNmElmnt.getChildNodes();
          acctIdentifier = accountNmElmnt.getAttribute("identifier");
          log4j.info("accountNmElmnt identifier: " + acctIdentifier);

          if (strAccId != null && !strAccId.equals("")) {
            acctIdentifier = acctIdentifier.substring(7, acctIdentifier.length());
            log4j.info("new identifier after substring: " + acctIdentifier);

          }

          strAccSchemaId = accountingSchemaNmElmnt.getAttribute("id");
          NodeList reportNmElmntLst = fstElmnt.getElementsByTagName("report");
          Element reportNmElmnt = (Element) reportNmElmntLst.item(0);
          NodeList reportNm = reportNmElmnt.getChildNodes();
          // log4j.info("report : " + ((Node) reportNm.item(0)).getNodeValue());
          // log4j.info("accountingSchema id: " +
          // accountingSchemaNmElmnt.getAttribute("id"));
          // log4j.info("accountingSchema entity-name: " +
          // accountingSchemaNmElmnt.getAttribute("entity-name"));
          strReport = "" + ((Node) reportNm.item(0)).getNodeValue();

          if ("false".equalsIgnoreCase(strReport)) {
            strReport = "N";
          } else {
            strReport = "Y";
          }
          NodeList reportTypeNmElmntLst = fstElmnt.getElementsByTagName("reportType");
          Element reportTypeNmElmnt = (Element) reportTypeNmElmntLst.item(0);
          NodeList reportTypeNm = reportTypeNmElmnt.getChildNodes();
          // log4j.info("reportType : " + ((Node) reportTypeNm.item(0)).getNodeValue());
          strReportType = ((Node) reportTypeNm.item(0)).getNodeValue();
          NodeList yearInitialBalanceNmElmntLst = fstElmnt
              .getElementsByTagName("yearInitialBalance");
          Element yearInitialBalanceNmElmnt = (Element) yearInitialBalanceNmElmntLst.item(0);
          NodeList yearInitialBalanceNm = yearInitialBalanceNmElmnt.getChildNodes();
          // log4j.info("yearInitialBalance : " + ((Node)
          // yearInitialBalanceNm.item(0)).getNodeValue());
          strIntBalance = "" + ((Node) yearInitialBalanceNm.item(0)).getNodeValue();

          if ("false".equalsIgnoreCase(strIntBalance)) {
            strIntBalance = "N";
          } else {
            strIntBalance = "Y";
          }

          if (strIsSummary.equalsIgnoreCase("Y")) {
            strAccId = null;
          } else {
            strAccId = getAcctId(clientID, acctIdentifier);

          }

          String schemaId = getSchemaId(clientID, orgID);

          StringBuffer strBuf = new StringBuffer(1000);
          strBuf
              .append("insert into AD_ACCOUNTINGRPT_ELEMENT(AD_CLIENT_ID,CREATEDBY,UPDATEDBY,AD_ORG_ID,TEMPORARYFILTERTYPE,AD_ACCOUNTINGRPT_ELEMENT_ID,ISACTIVE,NAME,DESCRIPTION,ISSUMMARY,ISSHOWN,ACCOUNT_ID,C_ACCTSCHEMA_ID,ISREPORT,REPORT_TYPE,FILTEREDBYORGANIZATION,ISINITIALBALANCE) values(");
          strBuf.append("'" + clientID);
          strBuf.append("','" + userID);
          strBuf.append("','" + userID);
          strBuf.append("','" + orgID);
          strBuf.append("','" + strReportingInterval);
          strBuf.append("','" + strAccGrptElemtid);
          strBuf.append("','" + strIsActive);
          strBuf.append("','" + strName);
          strBuf.append("','" + strDesp);
          strBuf.append("','" + strIsSummary);
          strBuf.append("','" + strShown);
          if (strAccId == null) {
            strBuf.append("'," + strAccId);
            strBuf.append(",'" + schemaId);
          } else {
            strBuf.append("','" + strAccId);
            strBuf.append("','" + schemaId);
          }

          strBuf.append("','" + strReport);
          strBuf.append("','" + strReportType);
          strBuf.append("','" + strFilterByOrg);
          strBuf.append("','" + strIntBalance + "')");
          log4j.info("the final query is ....." + strBuf.toString());
          //System.out.println("the final query is ....." + strBuf.toString());
          deleteOld(clientID,strName );
          insert(strBuf.toString());

          // String treeID = getTreeID(clientID);
          // log4j.info("TreeID " + treeID);
          // insertTreeNode(strAccGrptElemtid, treeID, clientID, orgID, userID);

        }

      }
      System.out
          .println("No of records inserted successfully into AD_ACCOUNTINGRPT_ELEMENT table is "
              + i);

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  private void updateParentID() throws ParserConfigurationException, SAXException, IOException {
    URL url = getClass().getResource("/");
    String path = url.getPath();
    String packageName = getClass().getPackage().toString();
    packageName = replaceString(packageName, "package ", " ");
    String newLink = replaceString(packageName, ".", "/");
    log4j.info("newLink......" + newLink);
    path = path + newLink.trim() + "/referencedata/standard/ParentNode.xml";
    log4j.info("path......" + path);
    File file = new File(path);
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db = dbf.newDocumentBuilder();
    Document doc = db.parse(file);
    doc.getDocumentElement().normalize();
    NodeList nodeLst = doc.getElementsByTagName("TreeNode");
    String elementName = null, ParentElementName = null, Sqno = null;

    try {
      for (int s = 0; s < nodeLst.getLength(); s++) {

        Node fstNode = nodeLst.item(s);

        if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

          Element fstElmnt = (Element) fstNode;

          NodeList elmntNameLst = fstElmnt.getElementsByTagName("ElementName");
          Element elmntNmElmnt = (Element) elmntNameLst.item(0);
          NodeList eleNl = elmntNmElmnt.getChildNodes();
          // log4j.info("id: " + ((Node) idNm.item(0)).getNodeValue());
          elementName = ((Node) eleNl.item(0)).getNodeValue();

          NodeList pNameNmElmntLst = fstElmnt.getElementsByTagName("ParentElementName");
          Element PnameNmElmnt = (Element) pNameNmElmntLst.item(0);
          NodeList pNameNl = PnameNmElmnt.getChildNodes();
          // log4j.info("name: " + ((Node) nameNm.item(0)).getNodeValue());
          ParentElementName = ((Node) pNameNl.item(0)).getNodeValue();

          NodeList sqNoLst = fstElmnt.getElementsByTagName("Sqno");
          Element sqNoElmnt = (Element) sqNoLst.item(0);
          NodeList sqNoNl = sqNoElmnt.getChildNodes();
          // log4j.info("reportingInterval : " +
          Sqno = ((Node) sqNoNl.item(0)).getNodeValue();

          log4j.info("elementName...." + elementName);
          log4j.info("ParentElementName...." + ParentElementName);
          log4j.info("Sqno...." + Sqno);
          updateStmt(elementName, ParentElementName, Sqno);

        }

      }
    } catch (Exception e) {

    }
  }

  private void updateStmt(String elementName, String ParentElementName, String Sqno) {
    try {
      String sql = null;
      Statement stmt;
      String client = context.getClient();
      if (!ParentElementName.equalsIgnoreCase("0")) {
        sql = "update ad_treenode set"
            + " parent_id = (select ad_accountingrpt_element_id from ad_accountingrpt_element where name = '"
            + ParentElementName
            + "' and ad_client_id='"
            + client
            + "'),"
            + " seqno ='"
            + Sqno
            + "'"
            + " where ad_treenode.node_id = (select ad_accountingrpt_element_id from ad_accountingrpt_element where name ='"
            + elementName + "' and ad_client_id='" + client + "' )";

      } else {
        sql = " update ad_treenode set"
            + " parent_id = '0'"
            + "seqno='"
            + Sqno
            + "'"
            + "where ad_treenode.node_id = (select ad_accountingrpt_element_id from ad_accountingrpt_element where name = '"
            + elementName + "' and ad_client_id='" + client + "')";
      }
      log4j.info("parent id update sql...." + sql);
      stmt = connection.createStatement();
      stmt.executeUpdate(sql);
      stmt.close();
    } catch (Exception e) {
      log4j.info("error in updating the parent node.");
    }
  }

  private void insert(String query) throws ParserConfigurationException, SAXException, IOException {
    Statement stmt;
    try {
      stmt = connection.createStatement();
      stmt.executeUpdate(query);
      i++;
      stmt.close();
      updateParentID();
    } catch (SQLException e) {
      log4j.info("This record already exists in database.");
      e.printStackTrace();
    }

  }

  private void deleteOld(String Client,String Name) throws ParserConfigurationException, SAXException,
      IOException {
    Statement stmt;
    try {
      stmt = connection.createStatement();
      String strSQL1 = "delete from AD_TREENODE where ad_tree_id=(SELECT AD_TREE_ID FROM AD_TREE WHERE treetype='AR' AND AD_Client_ID='"
          + Client + "')and node_id =(select AD_ACCOUNTINGRPT_ELEMENT_id from AD_ACCOUNTINGRPT_ELEMENT where ad_client_id='"+Client+"' and name ='"+Name+"')";

      String strSQL2 = "delete from AD_ACCOUNTINGRPT_ELEMENT where ad_client_id='" + Client + "' and name ='"+Name+"'";
      
      stmt.executeUpdate(strSQL1);

      stmt.executeUpdate(strSQL2);
      stmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

  private void insertTreeNode(String strAccGrptElemtid, String treeID, String clientID,
      String orgID, String userID) throws Exception {

    try {
      String treeNodeID = SequenceIdData.getUUID();
      Statement stmt = null;
      String query = "insert into ad_treenode(AD_TREENODE_ID,AD_TREE_ID,NODE_ID,AD_CLIENT_ID,AD_ORG_ID,ISACTIVE,CREATEDBY,UPDATEDBY,PARENT_ID,SEQNO) "
          + "values ('"
          + treeNodeID
          + "','"
          + treeID
          + "','"
          + strAccGrptElemtid
          + "','"
          + clientID + "','" + orgID + "','Y','" + userID + "','" + userID + "','0','999')";
      stmt = connection.createStatement();

      log4j.info("treenode insert...." + query);
      stmt.executeUpdate(query);
      stmt.close();
    } catch (SQLException e) {
      log4j.info("This record already exists in database.");
      e.printStackTrace();
    }

  }

  public String getTreeID(String clientID) throws Exception {
    String treeID = "";
    try {

      Statement stmt = connection.createStatement();
      // String strSQL = "SELECT EM_SC_PPPO_NUMBER FROM c_order WHERE EM_SC_PPPO_NUMBER="
      // + strOrderPONumber + "";
      String strSQL = " select ad_tree_id from ad_tree where name like '%Accounting report%' and ad_client_id='"
          + clientID + "'";

      log4j.info("getting the account id..." + strSQL);

      ResultSet rs = stmt.executeQuery(strSQL);

      while (rs.next()) {
        treeID = rs.getString(1);
        log4j.info("account id..." + treeID);
      }
    } catch (Exception e) {
      // log4j.info("EXCEPTION#701");
      // e.printStackTrace();
      // log4j.info("Error: " + e.getMessage());
    }

    return treeID;
  }

  public String getAcctId(String clientID, String acctIdentifier) throws Exception {
    String acctID = null;
    try {

      Statement stmt = connection.createStatement();
      // String strSQL = "SELECT EM_SC_PPPO_NUMBER FROM c_order WHERE EM_SC_PPPO_NUMBER="
      // + strOrderPONumber + "";
      String strSQL = " select c_elementvalue_id from c_elementvalue where ad_client_id='"
          + clientID + "' and name='" + acctIdentifier + "'";

      log4j.info("getting the account id..." + strSQL);

      ResultSet rs = stmt.executeQuery(strSQL);

      while (rs.next()) {
        acctID = rs.getString(1);
        log4j.info("account id..." + acctID);
      }
    } catch (Exception e) {
      // log4j.info("EXCEPTION#701");
      // e.printStackTrace();
      // log4j.info("Error: " + e.getMessage());
    }

    return acctID;
  }

  public String getSchemaId(String clientID, String orgId) throws Exception {
    String schemaId = "";
    try {

      Statement stmt = connection.createStatement();
      // String strSQL = "SELECT EM_SC_PPPO_NUMBER FROM c_order WHERE EM_SC_PPPO_NUMBER="
      // + strOrderPONumber + "";

      String strSQL = "SELECT C_ACCTSCHEMA_ID FROM C_ACCTSCHEMA WHERE ad_client_id ='" + clientID
          + "' and AD_ORG_ID='" + orgId + "' order by created desc";

      log4j.info("getting the schema id...");

      ResultSet rs = stmt.executeQuery(strSQL);

      while (rs.next()) {
        schemaId = rs.getString(1);
        log4j.info("schema id..." + schemaId);
      }
    } catch (Exception e) {
      // log4j.info("EXCEPTION#701");
      // e.printStackTrace();
      // log4j.info("Error: " + e.getMessage());
    }

    return schemaId;
  }

  /**
   * @param strOrginal
   * @param strOld
   * @param strNew
   * @return
   */
  private String replaceString(String strOrginal, String strOld, String strNew) {
    if (strOrginal == null || strOld == null || strNew == null || strOrginal.equals("")
        || strOld.equals("") || strNew.equals(""))
      return strOrginal;

    int intstartIndex;
    int intlen;
    int intlen2;
    int intlastPosition = 0;
    char[] charArray;
    StringBuffer buff = new StringBuffer(strOrginal);
    while (intlastPosition > -1) {
      intstartIndex = strOrginal.indexOf(strOld, intlastPosition);
      if (intstartIndex == -1)
        break;

      intlen = strOld.length();
      buff.delete(intstartIndex, intstartIndex + intlen);
      charArray = strNew.toCharArray();
      buff.insert(intstartIndex, charArray);
      strOrginal = buff.toString();
      intlen2 = intstartIndex + strNew.length();
      intlastPosition = strOrginal.indexOf(strOld, intlen2);
    }

    return buff.toString();
  }
}
