/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html

 ************************************************************************************
 */
package org.openbravo.localization.us.taxes.reporting.erpCommon.ad_process;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Vector;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_actionButton.ActionButtonDefaultData;
import org.openbravo.erpCommon.businessUtility.AccountingSchemaMiscData;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.DateTimeData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;

import org.openbravo.xmlEngine.XmlDocument;

public class CreateTaxSummaryReport extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  private static String strTreeOrg = "";
  private static Logger log4j =null;
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
    log4j=Logger.getLogger(CreateTaxSummaryReport.class);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    String process = CreateTaxSummaryReportData.processId(this, "UserDefinedAccountingReport");
    // TODO: Remove it if we are not using it
    /*
     * String strTabId = vars.getGlobalVariable("inpTabId", "CreateTaxSummaryReport|tabId"); String
     * strWindowId = vars.getGlobalVariable("inpwindowId", "CreateTaxSummaryReport|windowId");
     */
    // String strDeleteOld = vars.getStringParameter("inpDeleteOld", "Y");
    // String strCElementId = vars.getStringParameter("inpElementId", "");
    // String strUpdateDefault = vars.getStringParameter("inpUpdateDefault",
    // "Y");
    // String strCreateNewCombination =
    // vars.getStringParameter("inpCreateNewCombination", "Y");
    if (vars.commandIn("DEFAULT")) {
      // printPage(response, vars, process, strWindowId, strTabId,
      // strDeleteOld, strCElementId, strUpdateDefault,
      // strCreateNewCombination);
    	 Calendar ca1 = Calendar.getInstance();
         int iYear = ca1.get(Calendar.YEAR);
         int iPreYear = iYear - 1;
         log4j.info("iYear..."+iYear);
         log4j.info("iPreYear..."+iPreYear);
      String strcAcctSchemaId = vars.getGlobalVariable("inpcAcctSchemaId",
          "CreateTaxSummaryReport|cAcctSchemaId", "");
      String strAccountingReportId = vars.getGlobalVariable("inpAccountingReportId",
          "CreateTaxSummaryReport|accountingReport", "");
      String strOrg = vars.getGlobalVariable("inpadOrgId", "CreateTaxSummaryReport|orgId", "0");
      String strPeriod = vars.getGlobalVariable("inpPeriodId", "CreateTaxSummaryReport|period", "");
      String strYear = vars.getGlobalVariable("inpYearId", "CreateTaxSummaryReport|year", "");
      String strDateFrom = vars.getGlobalVariable("inpDateFrom", "CreateTaxSummaryReport|dateFrom",
      "");
  String strDateTo = vars.getGlobalVariable("inpDateTo", "CreateTaxSummaryReport|dateTo", "");
  if (strDateFrom.equals(""))
    strDateFrom = "01/01/" + iPreYear;
  if (strDateTo.equals(""))
    strDateTo = "12/31/" + iPreYear;
  if(strPeriod==""){
	  strPeriod=String.valueOf(iPreYear);
  }
  log4j.info("strPeriod..."+strPeriod);
      printPage(response, vars, strcAcctSchemaId, strAccountingReportId, strOrg, strPeriod,
          strYear, process,strDateTo,strDateFrom);
    } else if (vars.commandIn("FIND")) {
      String strcAcctSchemaId = vars.getRequestGlobalVariable("inpcAcctSchemaId",
          "CreateTaxSummaryReport|cAcctSchemaId");
      String strAccountingReportId = vars.getRequestGlobalVariable("inpAccountingReportId",
          "CreateTaxSummaryReport|accountingReport");
      String strOrg = vars.getGlobalVariable("inpadOrgId", "CreateTaxSummaryReport|orgId", "0");
      String strPeriod = vars.getRequestGlobalVariable("inpPeriodId",
          "CreateTaxSummaryReport|period");
      String strYear = vars.getRequestGlobalVariable("inpYearId", "CreateTaxSummaryReport|year");

      String strDateFrom = vars.getGlobalVariable("inpDateFrom", "CreateTaxSummaryReport|dateFrom",
      "");
  String strDateTo = vars.getGlobalVariable("inpDateTo", "CreateTaxSummaryReport|dateTo", "");

      printPagePopUp(response, vars, strcAcctSchemaId, strAccountingReportId, strOrg, strPeriod,
          strYear,strDateTo,strDateFrom);
      // printPageClosePopUp(response, vars, strWindowPath);
    } else
      pageErrorPopUp(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strcAcctSchemaId, String strAccountingReportId, String strOrg, String strPeriod,
      String strYear, String strProcessId,String strDateTo,String strDateFrom) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: process CreateTaxSummaryReport");

    ActionButtonDefaultData[] data = null;
    String strHelp = "", strDescription = "";
    if (vars.getLanguage().equals("en_US"))
      data = ActionButtonDefaultData.select(this, strProcessId);
    else
      data = ActionButtonDefaultData.selectLanguage(this, vars.getLanguage(), strProcessId);
    if (data != null && data.length != 0) {
      strDescription = data[0].description;
      strHelp = data[0].help;
    }

    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/localization/us/taxes/reporting/erpCommon/ad_process/CreateTaxSummaryReport").createXmlDocument();

    String strArray = arrayEntry(vars, strcAcctSchemaId);

    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("description", strDescription);
    xmlDocument.setParameter("help", strHelp);
    xmlDocument.setParameter("accounting", strAccountingReportId);
    xmlDocument.setParameter("org", strOrg);
    log4j.info("strPeriod inside page print method....?" + strPeriod);
    xmlDocument.setParameter("period", strPeriod);
    xmlDocument.setParameter("year", strYear);
    xmlDocument.setParameter("array", strArray);
    xmlDocument.setParameter("cAcctschemaId", strcAcctSchemaId);
    xmlDocument.setParameter("dateFrom", strDateFrom);
    xmlDocument.setParameter("dateTo", strDateTo);
    xmlDocument.setParameter("dateFromdisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateFromsaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTodisplayFormat", vars.getSessionValue("#AD_SqlDateFormat"));
    xmlDocument.setParameter("dateTosaveFormat", vars.getSessionValue("#AD_SqlDateFormat"));

    try {
      ComboTableData comboTableData = new ComboTableData(vars, this, "TABLEDIR", "AD_Org_ID", "",
          "", Utility.getContext(this, vars, "#User_Org", "CreateTaxSummaryReport"), Utility
              .getContext(this, vars, "#User_Client", "CreateTaxSummaryReport"), 0);
      Utility.fillSQLParameters(this, vars, null, comboTableData, "CreateTaxSummaryReport", "");
      xmlDocument.setData("reportAD_ORG", "liststructure", comboTableData.select(false));
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    xmlDocument.setData("reportAD_ACCOUNTINGRPT_ELEMENT", "liststructure",
        CreateTaxSummaryReportData.selectAD_Accountingrpt_Element_ID(this, Utility.getContext(this,
            vars, "#User_Org", "CreateTaxSummaryReport"), Utility.getContext(this, vars,
            "#User_Client", "CreateTaxSummaryReport"), strcAcctSchemaId, ""));
    xmlDocument.setData("reportC_ACCTSCHEMA_ID", "liststructure", AccountingSchemaMiscData
        .selectC_ACCTSCHEMA_ID(this, Utility.getContext(this, vars, "#AccessibleOrgTree",
            "CreateTaxSummaryReport"), Utility.getContext(this, vars, "#User_Client",
            "CreateTaxSummaryReport"), strcAcctSchemaId));

    xmlDocument.setParameter("accountArray", Utility.arrayDobleEntrada("arrAccount",
        CreateTaxSummaryReportData.selectAD_Accountingrpt_Element_Double_ID(this, Utility
            .getContext(this, vars, "#User_Org", "CreateTaxSummaryReport"), Utility.getContext(
            this, vars, "#User_Client", "CreateTaxSummaryReport"), "")));
//
//    // xmlDocument.setData("reportPeriod", "liststructure",
//    // CreateTaxSummaryReportData.selectPeriod(this, vars.getLanguage(),
//    // Utility.getContext(this, vars, "#User_Org",
//    // "CreateTaxSummaryReport"), Utility.getContext(this, vars,
//    // "#User_Client", "CreateTaxSummaryReport"), "800074"));
//    xmlDocument.setData("reportPeriod", "liststructure", CreateTaxSummaryReportData.selectCombo(
//        this, Utility.getContext(this, vars, "#User_Org", "CreateTaxSummaryReport"), Utility
//            .getContext(this, vars, "#User_Client", "CreateTaxSummaryReport"), vars.getLanguage()));
//

    //mallik--years begins
    try {
    		String strReference_Id = null;

                	if(this.getRDBMS().equalsIgnoreCase("ORACLE")){
                		strReference_Id = "6A9E000F31404F4F8F9A460580A22A61";
                	}
                	else
                	{
                		strReference_Id = "AE2C937D8680489FAECA986490780D4C";
                	}
             ComboTableData comboTableData = new ComboTableData(vars, this, "LIST",
            		 strReference_Id, strReference_Id, "", Utility.getContext(this, vars, "#User_Org",
                              "CreateTaxSummaryReport"), Utility.getContext(this, vars, "#User_Client",
                              "CreateTaxSummaryReport"), 0);
                      Utility.fillSQLParameters(this, vars, null, comboTableData, "CreateTaxSummaryReport", null);
                      xmlDocument.setData("reportPeriod", "liststructure",
                          comboTableData.select(false));
                      comboTableData = null;
      } catch (Exception ex) {
        throw new ServletException(ex);
      }

    //mallik ends

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "CreateTaxSummaryReport", false, "",
        "", "", false, "ad_process", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());
    //xmlDocument.setParameter("period", strPeriod);
    // New interface paramenters
    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "org.openbravo.localization.us.taxes.reporting.erpCommon.ad_process.CreateTaxSummaryReport");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(),
          "CreateTaxSummaryReport.html", classInfo.id, classInfo.type, strReplaceWith, tabs
              .breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "CreateTaxSummaryReport.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage("CreateTaxSummaryReport");
      vars.removeMessage("CreateTaxSummaryReport");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    // //----

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPagePopUp(HttpServletResponse response, VariablesSecureApp vars,
      String strcAcctSchemaId, String strAccountingReportId, String strOrg, String strPeriod,
      String strYear,String strDateTo,String strDateFrom) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: pop up CreateTaxSummaryReport");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/localization/us/taxes/reporting/erpCommon/ad_process/CreateTaxSummaryReportPopUp").createXmlDocument();
    String strPeriodFrom = "";
    int level = 0;
    String strPeriodTo = "";
    // String strYear = DateTimeData.sysdateYear(this);
    // log4j.debug("****************************strAccountingReportId: "+strAccountingReportId);
    String strAccountingType = CreateTaxSummaryReportData.selectType(this, strAccountingReportId);
    // log4j.debug("****************************strAccountingType: "+strAccountingType);
    if (strAccountingType.equals("Q")) {
      String strAux = CreateTaxSummaryReportData.selectMax(this, strPeriod);
      // log4j.debug("*************************strAux: "+strAux);
      strPeriodFrom = "01/" + CreateTaxSummaryReportData.selectMin(this, strPeriod) + "/" + strYear;
      // log4j.debug("*************************strPeriodFrom: "+strPeriodFrom);
      strPeriodTo = CreateTaxSummaryReportData.lastDay(this, "01/" + strAux + "/" + strYear, vars
          .getSqlDateFormat());
      strPeriodTo = DateTimeData.nDaysAfter(this, strPeriodTo, "1");
      // log4j.debug("*************************strPeriodTo: "+strPeriodTo);
    } else if (strAccountingType.equals("M")) {
      strPeriodFrom = "01/" + strPeriod + "/" + strYear;
      // log4j.debug("*************************strPeriodFrom1: "+strPeriodFrom);
      strPeriodTo = CreateTaxSummaryReportData
          .lastDay(this, strPeriodFrom, vars.getSqlDateFormat());
      strPeriodTo = DateTimeData.nDaysAfter(this, strPeriodTo, "1");
      // log4j.debug("*************************strPeriodTo1: "+strPeriodTo);
    } else {
      strPeriodFrom = "01/01/" + strPeriod;
      // log4j.debug("*************************strPeriodFrom2: "+strPeriodFrom);
      // TODO: Does this work with MM/DD/YY o YY/MM/DD locales?
      strPeriodTo = DateTimeData.nDaysAfter(this, "31/12/" + strPeriod, "1");
    }
    strPeriodFrom = CreateTaxSummaryReportData.selectFormat(this, strPeriodFrom, vars
        .getSqlDateFormat());
    strPeriodTo = CreateTaxSummaryReportData.selectFormat(this, strPeriodTo, vars
        .getSqlDateFormat());
    strTreeOrg = strOrg;
    treeOrg(vars, strOrg);

    Vector<Object> vectorArray = new Vector<Object>();
    log4j.info("exsisting from and to dates from ..strPeriodFrom...."+strPeriodFrom);
    log4j.info("exsisting from and to dates from .strPeriodTo..."+strPeriodTo);


    log4j.info("new from and to dates from ..strDateFrom...."+strDateFrom);
    log4j.info("new from and to dates from .strDateTo..."+strDateTo);
    childData(vars, vectorArray, strcAcctSchemaId, strAccountingReportId, strDateFrom,
    		strDateTo, strTreeOrg, level, "0", strPeriod);

    CreateTaxSummaryReportData[] dataTree = convertVector(vectorArray);
    dataTree = filterData(dataTree);
    strTreeOrg = "";

    //mallik
    String strYear1=strDateTo.substring(strDateTo.lastIndexOf("/")+1,strDateTo.length());
    SimpleDateFormat formatterTime = new SimpleDateFormat("hh:mm a");
	SimpleDateFormat formatterDate = new SimpleDateFormat("MM/dd/yyyy");
      Date tzId = new Date();
      String strTime=formatterTime.format(tzId);
      String strDate=formatterDate.format(tzId);
      log4j.info("strOrg......."+strOrg);
      String strCompany=getCompanyName(this,vars,strOrg);
    xmlDocument.setParameter("time", strTime);
    xmlDocument.setParameter("date",strDate);
    xmlDocument.setParameter("year", strYear1);
    xmlDocument.setParameter("compname",strCompany);

    //mallik ends
    xmlDocument.setParameter("title", dataTree[0].name);
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setData("structure", dataTree);
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private String arrayEntry(VariablesSecureApp vars, String strcAcctSchemaId)
      throws ServletException {
    String result = "";
    CreateTaxSummaryReportData[] data = CreateTaxSummaryReportData
        .selectAD_Accountingrpt_Element_ID(this, Utility.getContext(this, vars, "#User_Org",
            "CreateTaxSummaryReport"), Utility.getContext(this, vars, "#User_Client",
            "CreateTaxSummaryReport"), strcAcctSchemaId, "");
    if (data == null || data.length == 0) {
      result = "var array = null;";
    } else {
      result = "var array = new Array(\n";
      for (int i = 0; i < data.length; i++) {
        result += "new Array(\"" + data[i].id + "\",\"" + data[i].filteredbyorganization + "\",\""
            + data[i].temporaryfiltertype + "\")";
        if (i < data.length - 1)
          result += ",\n";
      }
      result += ");";
      CreateTaxSummaryReportData[] dataPeriod = CreateTaxSummaryReportData
          .selectCombo(this, Utility.getContext(this, vars, "#User_Org", "CreateTaxSummaryReport"),
              Utility.getContext(this, vars, "#User_Client", "CreateTaxSummaryReport"), vars
                  .getLanguage());
      if (dataPeriod == null || dataPeriod.length == 0) {
        result += "\nvar combo = null;";
      } else {
        result += "\nvar combo = new Array(\n";
        for (int j = 0; j < dataPeriod.length; j++) {
          result += "new Array(\"" + dataPeriod[j].value + "\", \"" + dataPeriod[j].id + "\", \""
              + dataPeriod[j].name + "\")";
          if (j < dataPeriod.length - 1)
            result += ",\n";
        }
        result += ");";
      }

    }
    return result;
  }

  private void treeOrg(VariablesSecureApp vars, String strOrg) throws ServletException {
    CreateTaxSummaryReportData[] dataOrg = CreateTaxSummaryReportData.selectOrg(this, strOrg, vars
        .getClient());
    for (int i = 0; i < dataOrg.length; i++) {
      strTreeOrg += "," + dataOrg[i].id;
      if (dataOrg[i].issummary.equals("Y"))
        treeOrg(vars, dataOrg[i].id);
    }
    return;
  }

  private void childData(VariablesSecureApp vars, Vector<Object> vectorArray,
      String strcAcctSchemaId, String strAccountingReportId, String strPeriodFrom,
      String strPeriodTo, String strOrg, int level, String strParent, String strPeriod)
      throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("**********************strAccountingReportId: " + strAccountingReportId);
    if (log4j.isDebugEnabled())
      log4j.debug("**********************strPeriodFrom: " + strPeriodFrom);
    if (log4j.isDebugEnabled())
      log4j.debug("**********************strPeriodTo: " + strPeriodTo);
    if (log4j.isDebugEnabled())
      log4j.debug("**********************strOrg: " + strOrg);
    if (log4j.isDebugEnabled())
      log4j.debug("**********************level: " + String.valueOf(level));
    if (log4j.isDebugEnabled())
      log4j.debug("**********************User_Client: "
          + Utility.getContext(this, vars, "#User_Client", "CreateTaxSummaryReport"));
    if (log4j.isDebugEnabled())
      log4j.debug("**********************#User_Org: "
          + Utility.getContext(this, vars, "#User_Org", "CreateTaxSummaryReport"));
    if (log4j.isDebugEnabled())
      log4j.debug("Ouput: child tree data");
    String strAccountId = CreateTaxSummaryReportData.selectAccounting(this, strAccountingReportId);
    if (log4j.isDebugEnabled())
      log4j.debug("**********************strAccountId: " + strAccountId);

    String initialBalance = CreateTaxSummaryReportData
        .isInitialBalance(this, strAccountingReportId);
    if (initialBalance.equals(""))
      initialBalance = "N";
    String dateInitialYear = CreateTaxSummaryReportData.dateInitialYear(this, Utility.getContext(
        this, vars, "#User_Org", "CreateTaxSummaryReport"), Utility.getContext(this, vars,
        "#User_Client", "CreateTaxSummaryReport"), strPeriod);
    dateInitialYear = CreateTaxSummaryReportData.selectFormat(this, dateInitialYear, vars
        .getSqlDateFormat());
    CreateTaxSummaryReportData[] data;
    if (initialBalance.equals("Y")) {
      data = CreateTaxSummaryReportData.selectInitial(this, strParent, String.valueOf(level),
          Utility.getContext(this, vars, "#User_Client", "CreateTaxSummaryReport"), strOrg,
          dateInitialYear, DateTimeData.nDaysAfter(this, dateInitialYear, "1"), strAccountId,
          strcAcctSchemaId, strAccountingReportId);
    } else {
      data = CreateTaxSummaryReportData.select(this, strParent, String.valueOf(level), Utility
          .getContext(this, vars, "#User_Client", "CreateTaxSummaryReport"), Utility
          .stringList(strOrg), strPeriodFrom, strPeriodTo, strAccountId, strcAcctSchemaId,
          strAccountingReportId);
    }
    if (data == null || data.length == 0)
      data = CreateTaxSummaryReportData.set();
    vectorArray.addElement(data[0]);
    if (log4j.isDebugEnabled())
      log4j.debug("**********************data[0]*********************: " + data[0].name + "  "
          + data[0].total);
    CreateTaxSummaryReportData[] dataAux = CreateTaxSummaryReportData.selectChild(this, Utility
        .getContext(this, vars, "#User_Client", "CreateTaxSummaryReport"), Utility.getContext(this,
        vars, "#User_Org", "CreateTaxSummaryReport"), data[0].id, strcAcctSchemaId);
    for (int i = 0; i < dataAux.length; i++) {
      childData(vars, vectorArray, strcAcctSchemaId, dataAux[i].id, strPeriodFrom, strPeriodTo,
          strOrg, level + 1, data[0].id, strPeriod);
    }
  }

  private CreateTaxSummaryReportData[] convertVector(Vector<Object> vectorArray)
      throws ServletException {
    CreateTaxSummaryReportData[] data = new CreateTaxSummaryReportData[vectorArray.size()];
    BigDecimal count = BigDecimal.ZERO;
    for (int i = 0; i < vectorArray.size(); i++) {
      data[i] = (CreateTaxSummaryReportData) vectorArray.elementAt(i);
    }
    for (int i = data.length - 1; i >= 0; i--) {
      if (data[i].issummary.equals("Y")) {
        for (int j = i + 1; j < data.length; j++) {
          if (Integer.valueOf(data[j].levelAccount).intValue() > Integer.valueOf(
              data[i].levelAccount).intValue()
              && data[j].parent.equals(data[i].id)) {
            String total = data[j].total;
            count = count.add(new BigDecimal(total));
          }
        }
        data[i].total = String.valueOf(count);
        count = BigDecimal.ZERO;
      }
    }
    return data;
  }

  private CreateTaxSummaryReportData[] filterData(CreateTaxSummaryReportData[] data)
      throws ServletException {
    ArrayList<Object> new_a = new ArrayList<Object>();
    for (int i = 0; i < data.length; i++) {
      if (data[i].isshown.equals("Y"))
        new_a.add(data[i]);
    }
    CreateTaxSummaryReportData[] newData = new CreateTaxSummaryReportData[new_a.size()];
    new_a.toArray(newData);
    return newData;
  }

  public String getServletInfo() {
    return "Servlet CreateTaxSummaryReport";
  } // end of getServletInfo() method

  public String getCompanyName(ConnectionProvider connectionProvider,VariablesSecureApp vars,String strOrg) throws ServletException {
	  	String strSql="";
	  	ResultSet result;
	    String strReturn = null;
	    PreparedStatement st = null;
	    int iParameter = 0;
	  if("*".equals(strOrg)){
		  String currentClient = vars.getClient();
		  System.out.println("currentClient................."+currentClient);
		  strSql="SELECT NAME as NAME FROM AD_CLIENT WHERE AD_CLIENT_ID=?";
		  try {

			    st = connectionProvider.getPreparedStatement(strSql);
			      iParameter++; UtilSql.setValue(st, iParameter, 12, null, currentClient);

			      result = st.executeQuery();
			      if(result.next()) {
			        strReturn = UtilSql.getValue(result, "NAME");
			      }
			      result.close();
			    } catch(SQLException e){
			      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
			      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
			    } catch(Exception ex){
			      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
			      throw new ServletException("@CODE=@" + ex.getMessage());
			    } finally {
			      try {
			        connectionProvider.releasePreparedStatement(st);
			      } catch(Exception ignore){
			        ignore.printStackTrace();
			      }
			    }
	  }else{
		  strSql="select bp.name as NAME from ad_orginfo ao,c_bpartner bp where ao.c_bpartner_id=bp.c_bpartner_id and ao.ad_org_id=?";
		  try {
			    st = connectionProvider.getPreparedStatement(strSql);
			      iParameter++; UtilSql.setValue(st, iParameter, 12, null, strOrg);

			      result = st.executeQuery();
			      if(result.next()) {
			        strReturn = UtilSql.getValue(result, "NAME");
			      }
			      result.close();
			    } catch(SQLException e){
			      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
			      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
			    } catch(Exception ex){
			      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
			      throw new ServletException("@CODE=@" + ex.getMessage());
			    } finally {
			      try {
			        connectionProvider.releasePreparedStatement(st);
			      } catch(Exception ignore){
			        ignore.printStackTrace();
			      }
			    }

			    if(strReturn==null){

					  String currentClient = vars.getClient();
					  System.out.println("currentClient................."+currentClient);
					  strSql="SELECT NAME as NAME FROM AD_CLIENT WHERE AD_CLIENT_ID=?";
					  try {
						  int iParameter1 = 0;
						    st = connectionProvider.getPreparedStatement(strSql);
						      iParameter1++; UtilSql.setValue(st, iParameter1, 12, null, currentClient);

						      result = st.executeQuery();
						      if(result.next()) {
						        strReturn = UtilSql.getValue(result, "NAME");
						      }
						      result.close();
						    } catch(SQLException e){
						      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
						      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
						    } catch(Exception ex){
						      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
						      throw new ServletException("@CODE=@" + ex.getMessage());
						    } finally {
						      try {
						        connectionProvider.releasePreparedStatement(st);
						      } catch(Exception ignore){
						        ignore.printStackTrace();
						      }
						    }

			    }

	  }
	  return strReturn;

}

}
