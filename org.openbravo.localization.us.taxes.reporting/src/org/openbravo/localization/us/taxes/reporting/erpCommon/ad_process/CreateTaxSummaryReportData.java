/*
 ************************************************************************************
 * Copyright (C) 2010 Transitional Data Services Inc.

 * http://www.transitionaldata.com

 *
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html

 ************************************************************************************
 */
package org.openbravo.localization.us.taxes.reporting.erpCommon.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class CreateTaxSummaryReportData implements FieldProvider {
static Logger log4j = Logger.getLogger(CreateTaxSummaryReportData.class);
  private String InitRecordNumber="0";
  public String parent;
  public String id;
  public String name;
  public String description;
  public String issummary;
  public String isshown;
  public String nodeId;
  public String total;
  public String filteredbyorganization;
  public String temporaryfiltertype;
  public String value;
  public String levelAccount;
  public String classAccount;
  public String classStyle;
  public String padre;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("parent"))
      return parent;
    else if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("issummary"))
      return issummary;
    else if (fieldName.equalsIgnoreCase("isshown"))
      return isshown;
    else if (fieldName.equalsIgnoreCase("node_id") || fieldName.equals("nodeId"))
      return nodeId;
    else if (fieldName.equalsIgnoreCase("total"))
      return total;
    else if (fieldName.equalsIgnoreCase("filteredbyorganization"))
      return filteredbyorganization;
    else if (fieldName.equalsIgnoreCase("temporaryfiltertype"))
      return temporaryfiltertype;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("level_account") || fieldName.equals("levelAccount"))
      return levelAccount;
    else if (fieldName.equalsIgnoreCase("class_account") || fieldName.equals("classAccount"))
      return classAccount;
    else if (fieldName.equalsIgnoreCase("class_style") || fieldName.equals("classStyle"))
      return classStyle;
    else if (fieldName.equalsIgnoreCase("padre"))
      return padre;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CreateTaxSummaryReportData[] select(ConnectionProvider connectionProvider, String parent, String level, String adClientId, String adOrgId, String periodFrom, String periodTo, String accountId, String acctschema, String adAccountingElementId)    throws ServletException {
    return select(connectionProvider, parent, level, adClientId, adOrgId, periodFrom, periodTo, accountId, acctschema, adAccountingElementId, 0, 0);
  }

  public static CreateTaxSummaryReportData[] select(ConnectionProvider connectionProvider, String parent, String level, String adClientId, String adOrgId, String periodFrom, String periodTo, String accountId, String acctschema, String adAccountingElementId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "          SELECT ? AS PARENT, AR.AD_ACCOUNTINGRPT_ELEMENT_ID AS ID, AR.NAME, AR.DESCRIPTION, AR.ISSUMMARY, AR.ISSHOWN," +
      "          AD_TREENODE.NODE_ID, COALESCE((CASE" +
      "          AR.ISSUMMARY WHEN 'N' THEN      (CASE REPORT_TYPE WHEN 'cc' THEN SUM(A.AMTACCTCR) WHEN 'dd'" +
      "          THEN SUM(A.AMTACCTDR) WHEN 'dc' THEN       SUM(A.AMTACCTDR - A.AMTACCTCR)" +
      "          WHEN 'cd' THEN SUM(A.AMTACCTCR - A.AMTACCTDR) END) ELSE 0 END),0) AS TOTAL, AR.FILTEREDBYORGANIZATION," +
      "          AR.TEMPORARYFILTERTYPE, '' AS VALUE, ? AS LEVEL_ACCOUNT, (CASE TO_NUMBER(?) WHEN 0 THEN" +
      "          '' ELSE '' END) AS CLASS_ACCOUNT, 'TEXT-INDENT: '||TO_CHAR(TO_NUMBER(?)*10)||'pt' AS CLASS_STYLE," +
      "          '' AS PADRE" +
      "          FROM AD_ACCOUNTINGRPT_ELEMENT AR left join (SELECT AMTACCTDR, AMTACCTCR, C_ACCTSCHEMA_ID" +
      "          FROM FACT_ACCT" +
      "          WHERE FACT_ACCT.AD_CLIENT_ID IN (";
    strSql = strSql + ((adClientId==null || adClientId.equals(""))?"":adClientId);
    strSql = strSql +
      ")" +
      "          AND FACT_ACCT.AD_ORG_ID IN (";
    strSql = strSql + ((adOrgId==null || adOrgId.equals(""))?"":adOrgId);
    strSql = strSql +
      ")" +
      "          AND 1=1";
    strSql = strSql + ((periodFrom==null || periodFrom.equals(""))?"":"  AND FACT_ACCT.DATEACCT >= TO_DATE(?) ");
    strSql = strSql + ((periodTo==null || periodTo.equals(""))?"":"  AND FACT_ACCT.DATEACCT < TO_DATE(?) ");
    strSql = strSql + ((accountId==null || accountId.equals(""))?"":"  AND FACT_ACCT.ACCOUNT_ID = ? ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND FACT_ACCT.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql +
      ") A on AR.C_ACCTSCHEMA_ID = A.C_ACCTSCHEMA_ID," +
      "          AD_TREE, AD_TREENODE" +
      "          WHERE AD_TREE.AD_TREE_ID = AD_TREENODE.AD_TREE_ID" +
      "          AND AD_TREENODE.NODE_ID = AR.AD_ACCOUNTINGRPT_ELEMENT_ID" +
      "          AND AR.AD_ACCOUNTINGRPT_ELEMENT_ID = ?" +
      "          AND AD_TREE.TREETYPE = 'AR'" +
      "          GROUP BY REPORT_TYPE, AR.AD_ACCOUNTINGRPT_ELEMENT_ID, AR.NAME, AR.DESCRIPTION, AR.ISSUMMARY, AR.ISSHOWN," +
      "          AD_TREENODE.NODE_ID," +
      "          AR.FILTEREDBYORGANIZATION, AR.TEMPORARYFILTERTYPE";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, parent);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, level);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, level);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, level);
      if (adClientId != null && !(adClientId.equals(""))) {
        }
      if (adOrgId != null && !(adOrgId.equals(""))) {
        }
      if (periodFrom != null && !(periodFrom.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodFrom);
      }
      if (periodTo != null && !(periodTo.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodTo);
      }
      if (accountId != null && !(accountId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, accountId);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingElementId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CreateTaxSummaryReportData objectCreateTaxSummaryReportData = new CreateTaxSummaryReportData();
        objectCreateTaxSummaryReportData.parent = UtilSql.getValue(result, "parent");
        objectCreateTaxSummaryReportData.id = UtilSql.getValue(result, "id");
        objectCreateTaxSummaryReportData.name = UtilSql.getValue(result, "name");
        objectCreateTaxSummaryReportData.description = UtilSql.getValue(result, "description");
        objectCreateTaxSummaryReportData.issummary = UtilSql.getValue(result, "issummary");
        objectCreateTaxSummaryReportData.isshown = UtilSql.getValue(result, "isshown");
        objectCreateTaxSummaryReportData.nodeId = UtilSql.getValue(result, "node_id");
        objectCreateTaxSummaryReportData.total = UtilSql.getValue(result, "total");
        objectCreateTaxSummaryReportData.filteredbyorganization = UtilSql.getValue(result, "filteredbyorganization");
        objectCreateTaxSummaryReportData.temporaryfiltertype = UtilSql.getValue(result, "temporaryfiltertype");
        objectCreateTaxSummaryReportData.value = UtilSql.getValue(result, "value");
        objectCreateTaxSummaryReportData.levelAccount = UtilSql.getValue(result, "level_account");
        objectCreateTaxSummaryReportData.classAccount = UtilSql.getValue(result, "class_account");
        objectCreateTaxSummaryReportData.classStyle = UtilSql.getValue(result, "class_style");
        objectCreateTaxSummaryReportData.padre = UtilSql.getValue(result, "padre");
        objectCreateTaxSummaryReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCreateTaxSummaryReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[vector.size()];
    vector.copyInto(objectCreateTaxSummaryReportData);
    return(objectCreateTaxSummaryReportData);
  }

  public static CreateTaxSummaryReportData[] selectInitial(ConnectionProvider connectionProvider, String parent, String level, String adClientId, String adOrgId, String periodFrom, String periodTo, String accountId, String acctschema, String adAccountingElementId)    throws ServletException {
    return selectInitial(connectionProvider, parent, level, adClientId, adOrgId, periodFrom, periodTo, accountId, acctschema, adAccountingElementId, 0, 0);
  }

  public static CreateTaxSummaryReportData[] selectInitial(ConnectionProvider connectionProvider, String parent, String level, String adClientId, String adOrgId, String periodFrom, String periodTo, String accountId, String acctschema, String adAccountingElementId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT ? AS PARENT, AR.AD_ACCOUNTINGRPT_ELEMENT_ID AS ID, AR.NAME, AR.DESCRIPTION, AR.ISSUMMARY, AR.ISSHOWN," +
      "        AD_TREENODE.NODE_ID, COALESCE((CASE" +
      "        AR.ISSUMMARY WHEN 'N' THEN (CASE REPORT_TYPE WHEN 'cc' THEN SUM(A.AMTACCTCR) WHEN 'dd'" +
      "        THEN SUM(A.AMTACCTDR) WHEN 'dc' THEN SUM(A.AMTACCTDR - A.AMTACCTCR)" +
      "        WHEN 'cd' THEN SUM(A.AMTACCTCR - A.AMTACCTDR) END) ELSE 0 END),0) AS TOTAL, AR.FILTEREDBYORGANIZATION," +
      "        AR.TEMPORARYFILTERTYPE, '' AS VALUE, ? AS LEVEL_ACCOUNT, (CASE TO_NUMBER(?) WHEN 0 THEN" +
      "        '' ELSE '' END) AS CLASS_ACCOUNT, 'TEXT-INDENT: '||TO_CHAR(TO_NUMBER(?)*10)||'pt' AS CLASS_STYLE," +
      "        '' AS PADRE" +
      "      FROM AD_ACCOUNTINGRPT_ELEMENT AR left join (SELECT AMTACCTDR, AMTACCTCR, C_ACCTSCHEMA_ID" +
      "        FROM FACT_ACCT" +
      "        WHERE FACT_ACCT.AD_CLIENT_ID IN (";
    strSql = strSql + ((adClientId==null || adClientId.equals(""))?"":adClientId);
    strSql = strSql +
      ")" +
      "          AND FACT_ACCT.AD_ORG_ID IN (";
    strSql = strSql + ((adOrgId==null || adOrgId.equals(""))?"":adOrgId);
    strSql = strSql +
      ")" +
      "          AND 1=1";
    strSql = strSql + ((periodFrom==null || periodFrom.equals(""))?"":"  AND FACT_ACCT.DATEACCT >= TO_DATE(?) ");
    strSql = strSql + ((periodTo==null || periodTo.equals(""))?"":"  AND FACT_ACCT.DATEACCT < TO_DATE(?) ");
    strSql = strSql + ((accountId==null || accountId.equals(""))?"":"  AND FACT_ACCT.ACCOUNT_ID = ? ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND FACT_ACCT.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql +
      "          AND FACT_ACCT.FACTACCTTYPE = 'O') A on AR.C_ACCTSCHEMA_ID = A.C_ACCTSCHEMA_ID," +
      "        AD_TREE, AD_TREENODE" +
      "      WHERE AD_TREE.AD_TREE_ID = AD_TREENODE.AD_TREE_ID" +
      "        AND AD_TREENODE.NODE_ID = AR.AD_ACCOUNTINGRPT_ELEMENT_ID" +
      "        AND AR.AD_ACCOUNTINGRPT_ELEMENT_ID = ?" +
      "        AND AD_TREE.TREETYPE = 'AR'" +
      "      GROUP BY REPORT_TYPE, AR.AD_ACCOUNTINGRPT_ELEMENT_ID, AR.NAME, AR.DESCRIPTION, AR.ISSUMMARY, AR.ISSHOWN," +
      "        AD_TREENODE.NODE_ID," +
      "        AR.FILTEREDBYORGANIZATION, AR.TEMPORARYFILTERTYPE";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, parent);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, level);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, level);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, level);
      if (adClientId != null && !(adClientId.equals(""))) {
        }
      if (adOrgId != null && !(adOrgId.equals(""))) {
        }
      if (periodFrom != null && !(periodFrom.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodFrom);
      }
      if (periodTo != null && !(periodTo.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodTo);
      }
      if (accountId != null && !(accountId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, accountId);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingElementId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CreateTaxSummaryReportData objectCreateTaxSummaryReportData = new CreateTaxSummaryReportData();
        objectCreateTaxSummaryReportData.parent = UtilSql.getValue(result, "parent");
        objectCreateTaxSummaryReportData.id = UtilSql.getValue(result, "id");
        objectCreateTaxSummaryReportData.name = UtilSql.getValue(result, "name");
        objectCreateTaxSummaryReportData.description = UtilSql.getValue(result, "description");
        objectCreateTaxSummaryReportData.issummary = UtilSql.getValue(result, "issummary");
        objectCreateTaxSummaryReportData.isshown = UtilSql.getValue(result, "isshown");
        objectCreateTaxSummaryReportData.nodeId = UtilSql.getValue(result, "node_id");
        objectCreateTaxSummaryReportData.total = UtilSql.getValue(result, "total");
        objectCreateTaxSummaryReportData.filteredbyorganization = UtilSql.getValue(result, "filteredbyorganization");
        objectCreateTaxSummaryReportData.temporaryfiltertype = UtilSql.getValue(result, "temporaryfiltertype");
        objectCreateTaxSummaryReportData.value = UtilSql.getValue(result, "value");
        objectCreateTaxSummaryReportData.levelAccount = UtilSql.getValue(result, "level_account");
        objectCreateTaxSummaryReportData.classAccount = UtilSql.getValue(result, "class_account");
        objectCreateTaxSummaryReportData.classStyle = UtilSql.getValue(result, "class_style");
        objectCreateTaxSummaryReportData.padre = UtilSql.getValue(result, "padre");
        objectCreateTaxSummaryReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCreateTaxSummaryReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[vector.size()];
    vector.copyInto(objectCreateTaxSummaryReportData);
    return(objectCreateTaxSummaryReportData);
  }

  public static CreateTaxSummaryReportData[] set()    throws ServletException {
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[1];
    objectCreateTaxSummaryReportData[0] = new CreateTaxSummaryReportData();
    objectCreateTaxSummaryReportData[0].parent = "";
    objectCreateTaxSummaryReportData[0].id = "";
    objectCreateTaxSummaryReportData[0].name = "";
    objectCreateTaxSummaryReportData[0].description = "";
    objectCreateTaxSummaryReportData[0].issummary = "";
    objectCreateTaxSummaryReportData[0].isshown = "";
    objectCreateTaxSummaryReportData[0].nodeId = "";
    objectCreateTaxSummaryReportData[0].total = "";
    objectCreateTaxSummaryReportData[0].filteredbyorganization = "";
    objectCreateTaxSummaryReportData[0].temporaryfiltertype = "";
    objectCreateTaxSummaryReportData[0].value = "";
    objectCreateTaxSummaryReportData[0].levelAccount = "";
    objectCreateTaxSummaryReportData[0].classAccount = "";
    objectCreateTaxSummaryReportData[0].classStyle = "";
    objectCreateTaxSummaryReportData[0].padre = "";
    return objectCreateTaxSummaryReportData;
  }

  public static CreateTaxSummaryReportData[] selectChild(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String adAccountingElementId, String acctschema)    throws ServletException {
    return selectChild(connectionProvider, adClientId, adOrgId, adAccountingElementId, acctschema, 0, 0);
  }

  public static CreateTaxSummaryReportData[] selectChild(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String adAccountingElementId, String acctschema, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT AR.AD_ACCOUNTINGRPT_ELEMENT_ID AS ID, AR.NAME, AR.DESCRIPTION, AR.ISSUMMARY, AR.ISSHOWN, AD_TREENODE.NODE_ID," +
      "      AR.FILTEREDBYORGANIZATION, AR.TEMPORARYFILTERTYPE" +
      "      FROM AD_TREE, AD_TREENODE, AD_ACCOUNTINGRPT_ELEMENT AR" +
      "      WHERE AD_TREE.AD_TREE_ID = AD_TREENODE.AD_TREE_ID" +
      "      AND AD_TREENODE.NODE_ID = AR.AD_ACCOUNTINGRPT_ELEMENT_ID" +
      "      AND AR.AD_CLIENT_ID IN (";
    strSql = strSql + ((adClientId==null || adClientId.equals(""))?"":adClientId);
    strSql = strSql +
      ")" +
      "      AND AR.AD_ORG_ID IN (";
    strSql = strSql + ((adOrgId==null || adOrgId.equals(""))?"":adOrgId);
    strSql = strSql +
      ")" +
      "      AND AD_TREENODE.PARENT_ID = ?" +
      "      AND AD_TREE.TREETYPE = 'AR'" +
      "      AND 1=1";
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND AR.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql +
      "      GROUP BY AR.AD_ACCOUNTINGRPT_ELEMENT_ID, AR.NAME, AR.DESCRIPTION, AR.ISSUMMARY, AR.ISSHOWN, AD_TREENODE.NODE_ID," +
      "      AR.FILTEREDBYORGANIZATION, AR.TEMPORARYFILTERTYPE, AD_TREENODE.SEQNO" +
      "      ORDER BY AD_TREENODE.SEQNO";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (adClientId != null && !(adClientId.equals(""))) {
        }
      if (adOrgId != null && !(adOrgId.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingElementId);
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CreateTaxSummaryReportData objectCreateTaxSummaryReportData = new CreateTaxSummaryReportData();
        objectCreateTaxSummaryReportData.id = UtilSql.getValue(result, "id");
        objectCreateTaxSummaryReportData.name = UtilSql.getValue(result, "name");
        objectCreateTaxSummaryReportData.description = UtilSql.getValue(result, "description");
        objectCreateTaxSummaryReportData.issummary = UtilSql.getValue(result, "issummary");
        objectCreateTaxSummaryReportData.isshown = UtilSql.getValue(result, "isshown");
        objectCreateTaxSummaryReportData.nodeId = UtilSql.getValue(result, "node_id");
        objectCreateTaxSummaryReportData.filteredbyorganization = UtilSql.getValue(result, "filteredbyorganization");
        objectCreateTaxSummaryReportData.temporaryfiltertype = UtilSql.getValue(result, "temporaryfiltertype");
        objectCreateTaxSummaryReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCreateTaxSummaryReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[vector.size()];
    vector.copyInto(objectCreateTaxSummaryReportData);
    return(objectCreateTaxSummaryReportData);
  }

  public static CreateTaxSummaryReportData[] selectOrg(ConnectionProvider connectionProvider, String adOrgId, String adClientId)    throws ServletException {
    return selectOrg(connectionProvider, adOrgId, adClientId, 0, 0);
  }

  public static CreateTaxSummaryReportData[] selectOrg(ConnectionProvider connectionProvider, String adOrgId, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT AD_ORG.AD_ORG_ID AS ID, AD_ORG.NAME, AD_ORG.ISSUMMARY, AD_TREENODE.NODE_ID" +
      "      FROM AD_CLIENTINFO, AD_TREENODE, AD_ORG" +
      "      WHERE AD_CLIENTINFO.AD_TREE_ORG_ID = AD_TREENODE.AD_TREE_ID" +
      "      AND AD_TREENODE.NODE_ID = AD_ORG.AD_ORG_ID" +
      "      AND AD_TREENODE.PARENT_ID = ?" +
      "      AND AD_CLIENTINFO.AD_CLIENT_ID = ?" +
      "      ORDER BY ISSUMMARY DESC";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CreateTaxSummaryReportData objectCreateTaxSummaryReportData = new CreateTaxSummaryReportData();
        objectCreateTaxSummaryReportData.id = UtilSql.getValue(result, "id");
        objectCreateTaxSummaryReportData.name = UtilSql.getValue(result, "name");
        objectCreateTaxSummaryReportData.issummary = UtilSql.getValue(result, "issummary");
        objectCreateTaxSummaryReportData.nodeId = UtilSql.getValue(result, "node_id");
        objectCreateTaxSummaryReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCreateTaxSummaryReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[vector.size()];
    vector.copyInto(objectCreateTaxSummaryReportData);
    return(objectCreateTaxSummaryReportData);
  }

  public static String processId(ConnectionProvider connectionProvider, String processId)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT AD_PROCESS.AD_PROCESS_ID AS ID" +
      "      FROM AD_PROCESS" +
      "      WHERE VALUE = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String selectMin(ConnectionProvider connectionProvider, String quarter)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT MIN(VALUE) AS VALUE" +
      "      FROM AD_MONTH" +
      "      WHERE QUARTER = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quarter);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "value");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String selectMax(ConnectionProvider connectionProvider, String quarter)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT MAX(VALUE) AS VALUE" +
      "      FROM AD_MONTH" +
      "      WHERE QUARTER = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quarter);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "value");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String lastDay(ConnectionProvider connectionProvider, String quarter, String dateFormat)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      select TO_CHAR(last_day(TO_DATE(?)), to_char(?)) from dual";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quarter);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFormat);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "to_char");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String selectFormat(ConnectionProvider connectionProvider, String quarter, String dateFormat)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      select TO_CHAR(TO_DATE(?), TO_CHAR(?)) from dual";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quarter);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFormat);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "to_char");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String selectAccounting(ConnectionProvider connectionProvider, String adAccountingrptElementId)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "        SELECT ACCOUNT_ID AS ID" +
      "        FROM AD_ACCOUNTINGRPT_ELEMENT" +
      "        WHERE AD_ACCOUNTINGRPT_ELEMENT_ID = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingrptElementId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static CreateTaxSummaryReportData[] selectAD_Accountingrpt_Element_ID(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String acctschema, String adAccountingrptElementId)    throws ServletException {
    return selectAD_Accountingrpt_Element_ID(connectionProvider, adOrgClient, adUserClient, acctschema, adAccountingrptElementId, 0, 0);
  }

  public static CreateTaxSummaryReportData[] selectAD_Accountingrpt_Element_ID(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String acctschema, String adAccountingrptElementId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT AD_ACCOUNTINGRPT_ELEMENT_ID AS ID, NAME, FILTEREDBYORGANIZATION, TEMPORARYFILTERTYPE" +
      "      FROM AD_ACCOUNTINGRPT_ELEMENT" +
      "      WHERE AD_ORG_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql +
      ")" +
      "      AND AD_CLIENT_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql +
      ")" +
      "      AND ISREPORT='Y'" +
      "      AND 1=1";
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND AD_ACCOUNTINGRPT_ELEMENT.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql +
      "      AND (isActive = 'Y' OR AD_ACCOUNTINGRPT_ELEMENT_ID = ? )";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingrptElementId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CreateTaxSummaryReportData objectCreateTaxSummaryReportData = new CreateTaxSummaryReportData();
        objectCreateTaxSummaryReportData.id = UtilSql.getValue(result, "id");
        objectCreateTaxSummaryReportData.name = UtilSql.getValue(result, "name");
        objectCreateTaxSummaryReportData.filteredbyorganization = UtilSql.getValue(result, "filteredbyorganization");
        objectCreateTaxSummaryReportData.temporaryfiltertype = UtilSql.getValue(result, "temporaryfiltertype");
        objectCreateTaxSummaryReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCreateTaxSummaryReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[vector.size()];
    vector.copyInto(objectCreateTaxSummaryReportData);
    return(objectCreateTaxSummaryReportData);
  }

  public static CreateTaxSummaryReportData[] selectAD_Accountingrpt_Element_Double_ID(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String adAccountingrptElementId)    throws ServletException {
    return selectAD_Accountingrpt_Element_Double_ID(connectionProvider, adOrgClient, adUserClient, adAccountingrptElementId, 0, 0);
  }

  public static CreateTaxSummaryReportData[] selectAD_Accountingrpt_Element_Double_ID(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String adAccountingrptElementId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT AD_ACCOUNTINGRPT_ELEMENT.C_ACCTSCHEMA_ID AS PADRE, AD_ACCOUNTINGRPT_ELEMENT_ID AS ID, NAME, FILTEREDBYORGANIZATION, TEMPORARYFILTERTYPE" +
      "      FROM AD_ACCOUNTINGRPT_ELEMENT" +
      "      WHERE AD_ORG_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql +
      ")" +
      "      AND AD_CLIENT_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql +
      ")" +
      "      AND ISREPORT='Y'" +
      "      AND 1=1" +
      "      AND (isActive = 'Y' OR AD_ACCOUNTINGRPT_ELEMENT_ID = ? )";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingrptElementId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CreateTaxSummaryReportData objectCreateTaxSummaryReportData = new CreateTaxSummaryReportData();
        objectCreateTaxSummaryReportData.padre = UtilSql.getValue(result, "padre");
        objectCreateTaxSummaryReportData.id = UtilSql.getValue(result, "id");
        objectCreateTaxSummaryReportData.name = UtilSql.getValue(result, "name");
        objectCreateTaxSummaryReportData.filteredbyorganization = UtilSql.getValue(result, "filteredbyorganization");
        objectCreateTaxSummaryReportData.temporaryfiltertype = UtilSql.getValue(result, "temporaryfiltertype");
        objectCreateTaxSummaryReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCreateTaxSummaryReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[vector.size()];
    vector.copyInto(objectCreateTaxSummaryReportData);
    return(objectCreateTaxSummaryReportData);
  }

  public static String selectType(ConnectionProvider connectionProvider, String adAccountingrptElementId)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT TEMPORARYFILTERTYPE" +
      "      FROM AD_ACCOUNTINGRPT_ELEMENT" +
      "      WHERE AD_ACCOUNTINGRPT_ELEMENT_ID = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingrptElementId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "temporaryfiltertype");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static CreateTaxSummaryReportData[] selectPeriod(ConnectionProvider connectionProvider, String adLanguage, String adOrgClient, String adUserClient, String adAccountingrptElementId)    throws ServletException {
    return selectPeriod(connectionProvider, adLanguage, adOrgClient, adUserClient, adAccountingrptElementId, 0, 0);
  }

  public static CreateTaxSummaryReportData[] selectPeriod(ConnectionProvider connectionProvider, String adLanguage, String adOrgClient, String adUserClient, String adAccountingrptElementId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT AD_REF_LIST.AD_REF_LIST_ID AS ID, CASE ? WHEN 'en_US' THEN AD_REF_LIST.NAME ELSE AD_REF_LIST_TRL.NAME END AS NAME" +
      "	   FROM AD_REF_LIST, AD_REF_LIST_TRL" +
      "	   WHERE AD_REF_LIST.AD_REF_LIST_ID = AD_REF_LIST_TRL.AD_REF_LIST_ID" +
      "	   AND AD_REF_LIST.AD_CLIENT_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql +
      ")" +
      "	   AND AD_REF_LIST.AD_ORG_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql +
      ")" +
      "	   AND AD_REF_LIST.AD_REFERENCE_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adLanguage);
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingrptElementId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CreateTaxSummaryReportData objectCreateTaxSummaryReportData = new CreateTaxSummaryReportData();
        objectCreateTaxSummaryReportData.id = UtilSql.getValue(result, "id");
        objectCreateTaxSummaryReportData.name = UtilSql.getValue(result, "name");
        objectCreateTaxSummaryReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCreateTaxSummaryReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[vector.size()];
    vector.copyInto(objectCreateTaxSummaryReportData);
    return(objectCreateTaxSummaryReportData);
  }

  public static CreateTaxSummaryReportData[] selectCombo(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String adLanguage)    throws ServletException {
    return selectCombo(connectionProvider, adOrgClient, adUserClient, adLanguage, 0, 0);
  }

  public static CreateTaxSummaryReportData[] selectCombo(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String adLanguage, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT 'M' AS VALUE, TO_CHAR(VALUE) AS ID, TO_CHAR(NAME) AS NAME" +
      "      FROM AD_MONTH" +
      "      WHERE AD_CLIENT_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql +
      ")" +
      "      AND AD_ORG_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql +
      ")" +
      "      UNION SELECT 'Q' AS VALUE, TO_CHAR(A.VALUE) AS ID, TO_CHAR(A.NAME) AS NAME" +
      "      FROM AD_MONTH, AD_REF_LIST_V A" +
      "      WHERE AD_CLIENT_ID  IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql +
      ")" +
      "      AND AD_ORG_ID  IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql +
      ")" +
      "      AND AD_MONTH.QUARTER = A.VALUE" +
      "      AND AD_REFERENCE_ID = '800027'" +
      "      AND AD_LANGUAGE = ?" +
      "      UNION SELECT 'A' AS VALUE, TO_CHAR(YEAR) AS ID, TO_CHAR(YEAR) AS NAME" +
      "      FROM C_YEAR";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adLanguage);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CreateTaxSummaryReportData objectCreateTaxSummaryReportData = new CreateTaxSummaryReportData();
        objectCreateTaxSummaryReportData.value = UtilSql.getValue(result, "value");
        objectCreateTaxSummaryReportData.id = UtilSql.getValue(result, "id");
        objectCreateTaxSummaryReportData.name = UtilSql.getValue(result, "name");
        objectCreateTaxSummaryReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCreateTaxSummaryReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CreateTaxSummaryReportData objectCreateTaxSummaryReportData[] = new CreateTaxSummaryReportData[vector.size()];
    vector.copyInto(objectCreateTaxSummaryReportData);
    return(objectCreateTaxSummaryReportData);
  }

  public static String dateInitialYear(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String Year)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "      SELECT MIN(STARTDATE)" +
      "      FROM C_PERIOD" +
      "      WHERE C_YEAR_ID = (" +
      "        SELECT C_YEAR_ID" +
      "        FROM C_YEAR" +
      "        WHERE AD_ORG_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql +
      ")" +
      "          AND AD_CLIENT_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql +
      ")" +
      "          AND YEAR = ?)";

    ResultSet result;
    String dateReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Year);

      result = st.executeQuery();
      if(result.next()) {
        dateReturn = UtilSql.getDateValue(result, "min", "MM/dd/yyyy");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(dateReturn);
  }

  public static String isInitialBalance(ConnectionProvider connectionProvider, String adAccountingrptElementId)    throws ServletException {
    String strSql = "";
    strSql = strSql +
      "        SELECT ISINITIALBALANCE" +
      "        FROM AD_ACCOUNTINGRPT_ELEMENT" +
      "        WHERE AD_ACCOUNTINGRPT_ELEMENT_ID = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adAccountingrptElementId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "isinitialbalance");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
