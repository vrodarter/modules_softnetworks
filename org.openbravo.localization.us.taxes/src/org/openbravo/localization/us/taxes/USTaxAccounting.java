package org.openbravo.localization.us.taxes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.data.UtilSql;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.database.RDBMSIndependent;
import org.openbravo.erpCommon.utility.SequenceIdData;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.exception.PoolNotFoundException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessContext;
import org.openbravo.service.db.DalBaseProcess;
import org.openbravo.erpCommon.reference.PInstanceProcessData;


// the background process needs to extend DalBaseProcess since
// we will be using DAL objects to perform database operations
public class USTaxAccounting extends DalBaseProcess {

private static Logger log4j = Logger.getLogger(USTaxAccounting.class);
private Connection conn = null;
private ProcessContext context = null;
private ConnectionProvider connectionProvider = null;

public void doExecute(ProcessBundle bundle) throws Exception {

try {

	connectionProvider = bundle.getConnection();
	context = bundle.getContext();
	conn = connectionProvider.getConnection();

      String userId = context.getUser();
      String roleId = context.getRole();
      String clientId = context.getClient();
      String orgId = context.getOrganization();



	String adPinstanceId = SequenceIdData.getUUID();
	log4j.info("pinstance id in USTaxAccounting....>" + adPinstanceId);
	log4j.info("clientId id in USTaxAccounting....>" + clientId);

	PInstanceProcessData.insertPInstance(connectionProvider, adPinstanceId, "D9C119089F58409B9F6E57809B3246D4", "0", "N", userId, clientId, orgId);
	PInstanceProcessData.insertPInstanceParam(connectionProvider, adPinstanceId, "10", "AD_Org_ID", orgId, clientId, orgId, userId);
	PInstanceProcessData.insertPInstanceParam(connectionProvider, adPinstanceId, "20", "AD_Client_ID", clientId, clientId, orgId, userId);


	String strSql = "";
	strSql = strSql +
	" CALL USLOCTX_ACCOUNTING_TAB(?)";

	CallableStatement st = null;
	if (connectionProvider.getRDBMS().equalsIgnoreCase("ORACLE")) {

	int iParameter = 0;
try {
	st = connectionProvider.getCallableStatement(conn, strSql);
	iParameter++; UtilSql.setValue(st, iParameter, 12, null, adPinstanceId);

	st.execute();
	} catch(SQLException e){
		log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
		throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
	} catch(Exception ex){
		log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
		throw new ServletException("@CODE=@" + ex.getMessage());
	} finally {
	try {
	} catch(Exception ignore){
		ignore.printStackTrace();
	}
}
}
else {
	Vector<String> parametersData = new Vector<String>();
	Vector<String> parametersTypes = new Vector<String>();
	parametersData.addElement(adPinstanceId);
	parametersTypes.addElement("in");
try {
	log4j.info("Executing POSGRES function");
	RDBMSIndependent.getCallableResult(conn, connectionProvider, strSql, parametersData, parametersTypes, 0);
	log4j.info("Executed usloctx_accounting_tab");
} catch(SQLException e){
	log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
} catch(NoConnectionAvailableException ec){
	log4j.error("Connection error in query: " + strSql + "Exception:"+ ec);
throw new ServletException("@CODE=NoConnectionAvailable");
} catch(PoolNotFoundException ep){
	log4j.error("Pool error in query: " + strSql + "Exception:"+ ep);
throw new ServletException("@CODE=NoConnectionAvailable");
} catch(Exception ex){
	log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
throw new ServletException("@CODE=@" + ex.getMessage());
}
}

} catch (Exception e) {
	log4j.info("Exception in us accounting....>");
	//log4j.info("Exception in us accounting....>"+ e.printStackTrace());
	e.printStackTrace();
}
}

}

