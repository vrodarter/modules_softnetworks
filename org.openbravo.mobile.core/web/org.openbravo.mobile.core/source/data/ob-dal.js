/*
 ************************************************************************************
 * Copyright (C) 2012-2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _, console, Backbone, enyo, Uint8Array */

OB.Dal = OB.Dal || {};

(function () {
  OB.Dal.EQ = '=';
  OB.Dal.NEQ = '!=';
  OB.Dal.CONTAINS = 'contains';
  OB.Dal.STARTSWITH = 'startsWith';
  OB.Dal.ENDSWITH = 'endsWith';
  OB.Dal.stackSize = 0;

  function executeSqlErrorHandler(logLevel, header, objectInvolved, txError, e) {
    // arguments check
    OB.UTIL.Debug.execute(function () {
      if (!logLevel) {
        throw "executeSqlErrorHandler: missing logLevel";
      }
      if (!header) {
        throw "executeSqlErrorHandler: missing header";
      }
      if (!objectInvolved) {
        throw "executeSqlErrorHandler: missing objectInvolved";
      }
      if (!e) {
        throw "executeSqlErrorHandler: missing e";
      }
    });
    // show an error
    var link;
    try {
      var errorobj = new Error();
      link = OB.UTIL.getStackLink(3);
    } catch (ex) {
      console.error("executeSqlErrorHandler.getStackLink: " + ex);
    }
    // LogClient can easily spam the console
    if (objectInvolved === 'obmobc_logclient') {
      return;
    }
    var header2 = header + " '" + objectInvolved + "'";
    // log depending on the logLevel
    if (logLevel === 'warn') {
      console.warn(OB.UTIL.argumentsToStringifyed(header2, e, link));
      return;
    }
    console.error(OB.UTIL.argumentsToStringifyed(header2, e, link));
  }

  function silentFunction(f) {
    return function () {
      if (_.isFunction(f)) {
        try {
          f(arguments);
        } catch (e) {
          OB.error('OB.Dal: a success callback threw an exception', e.message, f, e.stack.split('\n')[2].trimLeft());
        }
      }
    };
  }

  /*
   * initialize the WebSQL dababase
   */
  OB.Dal.openWebSQL = function () {
    if (!window.openDatabase) {
      OB.UTIL.Debug.execute(function (e) {
        throw "window.openDatabase not available";
      });
      return;
    }

    // do not initialize the db if it was already initialized
    if (OB.Data.localDB && OB.Data.localDB.version) {
      return OB.Data.localDB;
    }

    var dbInfo = OB.MobileApp.model.get('localDB');

    // arguments check
    if (!dbInfo) {
      OB.UTIL.Debug.execute(function () {
        throw "The database version information must be available before 'OB.Dal.openWebSQL' is called";
      });
      return;
    }

    console.info("OB.Dal.openWebSQL: initializing WebSQL");

    var undef;
    var wsql = window.openDatabase !== undef;
    var db;

    if (wsql === false) {
      // Support should get this error. Show it in productionn
      console.error("WebSQL error: Unable find the database engine");
    }

    try {
      db = (wsql && window.openDatabase(dbInfo.name, '', dbInfo.displayName, dbInfo.size));
    } catch (e) {
      // if the database could not be created, logging with OB.error is not available
      // Support should get this error. Show it in production
      console.error("Web SQL error: " + e);
      OB.UTIL.Debug.execute(function (e) {
        throw "Web SQL error: " + e;
      });
      return;
    }
    if (!db) {
      // if the database could not be created, logging with OB.error is not available
      // Support should get this error. Show it in production
      console.error("Web SQL error");
      OB.UTIL.Debug.execute(function (e) {
        throw "Web SQL error";
      });
      return;
    }

    OB.Data.localDB = db;

    return db;
  };

  /**
   * TODO: localStorage
   * This is a function to centralize TODO Dal code related to localstorage
   * As of changes in the issue 27166, this flow is now executed
   */
  OB.Dal.missingLocalStorageLogic = function () {
    OB.UTIL.Debug.execute(function () {
      throw "Not Implemented";
    });
  };

  OB.Dal.get_uuid = function () {
    var array;
    var uuid = "",
        i, digit = "";
    if (window.crypto && window.crypto.getRandomValues) {
      array = new Uint8Array(16);
      window.crypto.getRandomValues(array);

      for (i = 0; i < array.length; i++) {
        digit = array[i].toString(16).toUpperCase();
        if (digit.length === 1) {
          digit = "0" + digit;
        }
        uuid += digit;
      }

      return uuid;
    }

    function S4() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1).toUpperCase();
    }
    return (S4() + S4() + S4() + S4() + S4() + S4() + S4() + S4());
  };

  OB.Dal.transform = function (model, obj) {
    var tmp = {},
        modelProto = model.prototype,
        val, properties;
    properties = model.getProperties ? model.getProperties() : modelProto.properties;
    _.each(properties, function (property) {
      var prop;
      if (_.isString(property)) {
        prop = property;
        val = obj[modelProto.propertyMap[property]];

      } else {
        prop = property.name;
        val = obj[property.column];
      }
      if (val === 'false') {
        tmp[prop] = false;
      } else if (val === 'true') {
        tmp[prop] = true;
      } else {
        tmp[prop] = val;
      }
    });
    return new model(tmp);
  };

  OB.Dal.getWhereClause = function (criteria, propertyMap) {
    var appendWhere = true,
        firstParam = true,
        sql = '',
        params = [],
        res = {},
        orAnd = ' AND ';
    if (criteria && !_.isEmpty(criteria)) {
      if (criteria.obdalcriteriaType) {
        orAnd = ' ' + criteria.obdalcriteriaType + ' ';
        delete criteria.obdalcriteriaType;
      }
      _.each(_.keys(criteria), function (k) {

        var undef, colName, val = criteria[k],
            operator = (val !== undef && val !== null && val.operator !== undef) ? val.operator : '=',
            value = (val !== undef && val !== null && val.value !== undef) ? val.value : val;
        if (k !== '_orderByClause' && k !== '_limit') {
          if (appendWhere) {
            sql = sql + ' WHERE ';
            params = [];
            appendWhere = false;
          }

          if (_.isArray(propertyMap)) {
            if (k === '_filter') {
              colName = '_filter';
            } else {
              colName = _.find(propertyMap, function (p) {
                return k === p.name;
              }).column;
            }
          } else {
            colName = propertyMap[k];
          }

          sql = sql + (firstParam ? '' : orAnd) + ' ' + colName + ' ';

          if (value === null) {
            sql = sql + ' IS null ';
          } else {

            if (operator === OB.Dal.EQ) {
              sql = sql + ' = ? ';
            } else if (operator === OB.Dal.NEQ) {
              sql = sql + ' != ? ';
            } else {
              sql = sql + ' like ? ';
            }

            if (operator === OB.Dal.CONTAINS) {
              value = '%' + value + '%';
            } else if (operator === OB.Dal.STARTSWITH) {
              value = value + '%';
            } else if (operator === OB.Dal.ENDSWITH) {
              value = value + '%';
            }
            params.push(value);
          }

          if (firstParam) {
            firstParam = false;
          }

        }

      });
    }
    res.sql = sql;
    res.params = params;
    return res;
  };

  OB.Dal.getTableName = function (model) {
    if (model) {
      if (model.getTableName) {
        return model.getTableName();
      }
      if (model.prototype && model.prototype.tableName) {
        return model.prototype.tableName;
      }
    }
    OB.UTIL.Debug.execute(function () {
      throw "OB.Dal.getTableName: the model has not been initialized";
    });
    return null;
  };

  OB.Dal.getPropertyMap = function (model) {
    if (model) {
      if (model.getProperties) {
        return model.getProperties();
      }
      if (model.prototype && model.prototype.propertyMap) {
        return model.prototype.propertyMap;
      }
    }
    OB.UTIL.Debug.execute(function () {
      throw "OB.Dal.getPropertyMap: the model has not been initialized";
    });
    return null;
  };

  OB.Dal.findUsingCache = function (cacheName, model, whereClause, success, error, args) {
    if (OB.Cache.hasItem(cacheName, whereClause)) {
      OB.Dal.stackSize++;
      if (OB.Dal.stackSize % 1000 === 0) {
        setTimeout(function () {
          success(OB.Cache.getItem(cacheName, whereClause));
        }, 0);
      } else {
        success(OB.Cache.getItem(cacheName, whereClause));
      }
    } else {
      OB.Dal.find(model, whereClause, function (models) {
        OB.Cache.putItem(cacheName, whereClause, models);
        success(models);
      }, error, args);
    }

  };

  OB.Dal.find = function (model, whereClause, success, error, args) {
    var params = null,
        appendWhere = true,
        firstParam = true,
        k, v, undef, colType, xhr, i, criteria, j, params_where, orderBy, limit;

    if (model.prototype.online) {
      colType = OB && OB.Collection && OB.Collection[model.prototype.modelName + 'List'];
      if (undef === colType) {
        console.warn("OB.Dal.find: there is no collection defined at: OB.Data.Collection." + model.prototype.modelName + "List");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.find: there is no collection defined at: OB.Data.Collection." + model.prototype.modelName + "List";
        });
      }

      xhr = new enyo.Ajax({
        url: model.prototype.source,
        method: 'POST'
      });

      xhr.response(function (inSender, inResponse) {
        //FIXME: implement error handling
        success(new colType(inResponse.response.data));
      });

      params = enyo.clone(whereClause);

      if (whereClause && _.isNumber(whereClause._limit)) {
        limit = whereClause._limit;
      } else {
        limit = 100;
        OB.trace('OB.Dal.find used without specific limit. Automatically set to 100.', model, whereClause);
      }
      params._noCount = true;
      params._operationType = 'fetch';
      params._startRow = (whereClause && whereClause._offset ? whereClause._offset : 0);
      params._endRow = params._startRow + limit;
      params._sortBy = (whereClause && whereClause._sortBy ? whereClause._sortBy : '');
      params.isc_dataFormat = 'json';
      params.isc_metaDataPrefix = '_';

      if (whereClause && whereClause._constructor) {
        for (i in whereClause) {
          if (whereClause.hasOwnProperty(i)) {
            if (i === 'criteria') {
              params.criteria = [];
              criteria = whereClause[i];
              for (j = 0; j < criteria.length; j++) {
                params.criteria.push(JSON.stringify(criteria[j]));
              }
            } else {
              params[i] = whereClause[i];
            }
          }
        }
      } else {
        params_where = (whereClause && whereClause._where ? whereClause._where : '');
      }

      xhr.go(params);
    } else if (OB.Data.localDB) {
      var tableName = OB.Dal.getTableName(model),
          propertyMap = OB.Dal.getPropertyMap(model),
          sql = 'SELECT * FROM ' + tableName;
      // websql
      // arguments check
      if (tableName === null) {
        console.warn("OB.Dal.find: tableName not found");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.find: tableName not found";
        });
      }
      if (propertyMap === null) {
        console.warn("OB.Dal.find: propertyMap not found");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.find: propertyMap not found";
        });
      }

      if (whereClause && whereClause._orderByClause) {
        orderBy = whereClause._orderByClause;
      }
      if (whereClause && whereClause._limit) {
        limit = whereClause._limit;
      } else {
        limit = model.prototype.dataLimit;
      }
      if (whereClause && whereClause._whereClause) {
        whereClause.sql = ' ' + whereClause._whereClause;
      } else {
        whereClause = OB.Dal.getWhereClause(whereClause, propertyMap);
      }
      sql = sql + whereClause.sql;
      params = whereClause.params;

      if (orderBy) {
        sql = sql + ' ORDER BY ' + orderBy + ' ';
      } else if (model.propertyList || model.prototype.propertyMap._idx) {
        sql = sql + ' ORDER BY _idx ';
      }

      if (limit) {
        sql = sql + ' LIMIT ' + limit;
      }

      OB.Data.localDB.readTransaction(function (tx) {
        var synchId;
        if (model.prototype.modelName !== 'LogClient') {
          synchId = OB.UTIL.SynchronizationHelper.busyUntilFinishes('find ' + model.prototype.modelName);
        }
        tx.executeSql(sql, params, function (tr, result) {
          if (synchId) {
            OB.UTIL.SynchronizationHelper.finished(synchId, 'find');
          }
          var i, collectionType = OB.Collection[model.prototype.modelName + 'List'] || Backbone.Collection,
              collection = new collectionType(),
              len = result.rows.length;
          if (len === 0) {
            success(collection, args);
          } else {
            for (i = 0; i < len; i++) {
              collection.add(OB.Dal.transform(model, result.rows.item(i)));
            }
            success(collection, args);
          }
        }, function (txError, e) {
          if (synchId) {
            OB.UTIL.SynchronizationHelper.finished(synchId, 'find');
          }
          executeSqlErrorHandler('warn', "OB.Dal.find: table", tableName, txError, e);
          if (error) {
            error();
          }
        });
      });
    } else {
      this.missingLocalStorageLogic();
    }
  };

  OB.Dal.query = function (model, sql, params, success, error, args) {
    if (OB.Data.localDB) {
      if (model.prototype.dataLimit) {
        sql = sql + ' LIMIT ' + model.prototype.dataLimit;
      }
      OB.Data.localDB.readTransaction(function (tx) {
        tx.executeSql(sql, params, function (tr, result) {
          var i, collectionType = OB.Collection[model.prototype.modelName + 'List'] || Backbone.Collection,
              collection = new collectionType(),
              len = result.rows.length;
          if (len === 0) {
            success(collection, args);
          } else {
            for (i = 0; i < len; i++) {
              collection.add(OB.Dal.transform(model, result.rows.item(i)));
            }
            success(collection, args);
          }
        }, function (txError, e) {
          executeSqlErrorHandler('error', "OB.Dal.query: table", model.prototype.modelName, txError, e);
          if (_.isFunction(error)) {
            error();
          }
        });
      });
    } else {
      this.missingLocalStorageLogic();
    }
  };

  OB.Dal.save = function (model, success, error, forceInsert) {
    var modelProto = model.constructor.prototype,
        xhr, data = {};

    forceInsert = forceInsert || false;

    // TODO: properly check model type
    if (modelProto && modelProto.online) {
      if (!model) {
        console.warn("OB.Dal.save: you need to pass a Model instance to save");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.save: you need to pass a Model instance to save";
        });
      }

      xhr = new enyo.Ajax({
        url: modelProto.source,
        method: 'PUT'
      });

      xhr.response(function (inSender, inResponse) {
        success(inResponse);
      });

      data.operationType = 'update';
      data.data = model.toJSON();

      xhr.go(JSON.stringify(data));
    } else if (OB.Data.localDB) {
      var modelDefinition = OB.Model[modelProto.modelName],
          tableName = OB.Dal.getTableName(modelDefinition),
          primaryKey, primaryKeyProperty = 'id',
          primaryKeyColumn, sql = '',
          params = null,
          firstParam = true,
          uuid, propertyName, filterVal;
      // websql
      // argument checks
      if (!tableName) {
        console.warn("OB.Dal.save: tableName not found");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.save: tableName not found";
        });
      }

      if (modelDefinition.getPrimaryKey) {
        primaryKey = modelDefinition.getPrimaryKey();
        primaryKeyProperty = primaryKey.name;
        primaryKeyColumn = primaryKey.column;
      } else {
        primaryKeyColumn = modelProto.propertyMap[primaryKeyProperty];
      }

      if (model.get(primaryKeyProperty) && forceInsert === false) {
        if (modelDefinition.getUpdateStatement) {
          sql = modelDefinition.getUpdateStatement();
          params = [];
          _.each(modelDefinition.getPropertiesForUpdate(), function (property) {
            //filter doen't have name and always is the last one
            if (property.name) {
              params.push(model.get(property.name));
            }
          });
          //filter param
          if (modelDefinition.hasFilter()) {
            filterVal = '';
            _.each(modelDefinition.getFilterProperties(), function (filterProperty) {
              filterVal = filterVal + (model.get(filterProperty) ? (model.get(filterProperty) + '###') : '');
            });
            params.push(filterVal);
          }
          //Where param
          params.push(model.get(primaryKeyProperty));
        } else {
          // UPDATE
          sql = 'UPDATE ' + tableName + ' SET ';

          _.each(_.keys(modelProto.properties), function (attr) {
            propertyName = modelProto.properties[attr];
            if (attr === 'id') {
              return;
            }

            if (firstParam) {
              firstParam = false;
              params = [];
            } else {
              sql = sql + ', ';
            }

            sql = sql + modelProto.propertyMap[propertyName] + ' = ? ';
            params.push(model.get(propertyName));
          });

          if (modelProto.propertiesFilter) {
            filterVal = '';
            _.each(modelProto.propertiesFilter, function (prop) {
              filterVal = filterVal + (model.get(prop) ? (model.get(prop) + '###') : '');
            });
            sql = sql + ', _filter = ? ';
            params.push(filterVal);
          }
          sql = sql + ' WHERE ' + tableName + '_id = ?';
          params.push(model.get('id'));
        }
      } else {
        params = [];
        // INSERT
        sql = modelDefinition.getInsertStatement ? modelDefinition.getInsertStatement() : modelProto.insertStatement;
        if (forceInsert === false) {
          uuid = OB.Dal.get_uuid();
          params.push(uuid);
          if (model.getPrimaryKey) {
            primaryKey = model.getPrimaryKey();
            model.set(primaryKey.name, uuid);
          } else {
            model.set('id', uuid);
          }
        }
        //Set params
        if (modelDefinition.getProperties) {
          _.each(modelDefinition.getProperties(), function (property) {
            if (forceInsert === false) {
              if (property.primaryKey) {
                return;
              }
            }
            //_filter property doesn't have name.
            //don't set the filter column. We will do it in the next step
            if (property.name) {
              params.push(model.get(property.name) === undefined ? null : model.get(property.name));
            }
          });
        } else {
          _.each(modelProto.properties, function (property) {
            if (forceInsert === false) {
              if ('id' === property) {
                return;
              }
            }
            params.push(model.get(property) === undefined ? null : model.get(property));
          });
        }
        //set filter column
        if (modelDefinition.hasFilter) {
          if (modelDefinition.hasFilter()) {
            filterVal = '';
            _.each(modelDefinition.getFilterProperties(), function (filterProp) {
              filterVal = filterVal + (model.get(filterProp) ? (model.get(filterProp) + '###') : '');
            });
            //Include in the last position but before _idx
            params.splice(params.length - 1, 0, filterVal);
          }
        } else {
          if (modelProto.propertiesFilter) {
            filterVal = '';
            _.each(modelProto.propertiesFilter, function (prop) {
              filterVal = filterVal + (model.get(prop) ? (model.get(prop) + '###') : '');
            });
            //Include in the last position but before _idx
            params.splice(params.length - 1, 0, filterVal);
          }
        }
      }
      OB.Data.localDB.transaction(function (tx) {
        try {
          tx.executeSql(sql, params, silentFunction(success), function (txError, e) {
            executeSqlErrorHandler('error', "OB.Dal.save: table", tableName, txError, e);
            if (_.isFunction(error)) {
              error();
            }
          });
        } catch (e) {
          executeSqlErrorHandler('error', "OB.Dal.save: table", tableName, null, e);
          // OB.Data.localDB = undefined;
          // OB.Dal.openWebSQL();
        }
      });
    } else {
      this.missingLocalStorageLogic();
    }
  };

  OB.Dal.remove = function (model, success, error) {
    if (OB.Data.localDB) {
      var modelDefinition = OB.Model[model.constructor.prototype.modelName],
          modelProto = model.constructor.prototype,
          tableName = OB.Dal.getTableName(modelDefinition),
          pk, pkProperty = 'id',
          pkColumn, sql = '',
          params = [];

      // websql
      if (!tableName) {
        console.warn("OB.Dal.remove: tableName not found");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.remove: tableName not found";
        });
      }
      if (modelDefinition.getPrimaryKey) {
        pk = modelDefinition.getPrimaryKey() ? modelDefinition.getPrimaryKey() : null;
        if (pk) {
          pkProperty = pk.name;
          pkColumn = pk.column;
        } else {
          pkColumn = modelDefinition.propertyMap[pkProperty];
        }
      }
      if (model.get(pkProperty)) {
        if (modelDefinition.getDeleteByIdStatement) {
          sql = modelDefinition.getDeleteByIdStatement();
        } else {
          sql = 'DELETE FROM ' + tableName + ' WHERE ' + modelProto.propertyMap[pkProperty] + ' = ? ';
        }
        // UPDATE
        params.push(model.get(pkProperty));
      } else {
        console.warn("OB.Dal.remove: an object without primary key cannot be deleted");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.remove: an object without primary key cannot be deleted";
        });
      }

      //OB.info(sql);
      //OB.info(params);
      OB.Data.localDB.transaction(function (tx) {
        tx.executeSql(sql, params, silentFunction(success), function (txError, e) {
          executeSqlErrorHandler('error', "OB.Dal.remove: table", tableName, txError, e);
          if (_.isFunction(error)) {
            error();
          }
        });
      });
    } else {
      this.missingLocalStorageLogic();
    }
  };

  OB.Dal.removeAll = function (model, criteria, success, error) {
    if (OB.Data.localDB) {
      var tableName = OB.Dal.getTableName(model),
          propertyMap = OB.Dal.getPropertyMap(model),
          sql, params, whereClause;
      // websql
      if (!tableName) {
        console.warn("OB.Dal.removeAll: tableName not found");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.removeAll: tableName not found";
        });
      }

      sql = 'DELETE FROM ' + tableName;
      whereClause = OB.Dal.getWhereClause(criteria, propertyMap);
      sql = sql + whereClause.sql;
      params = whereClause.params;
      OB.Data.localDB.transaction(function (tx) {
        tx.executeSql(sql, params, silentFunction(success), function (txError, e) {
          executeSqlErrorHandler('error', "OB.Dal.removeAll: table", tableName, txError, e);
          if (_.isFunction(error)) {
            error();
          }
        });
      });
    } else {
      this.missingLocalStorageLogic();
    }
  };

  OB.Dal.get = function (model, id, success, error, empty) {
    if (OB.Data.localDB) {
      var tableName = OB.Dal.getTableName(model),
          sql = 'SELECT * FROM ' + tableName + ' WHERE ' + tableName + '_id = ?';

      // websql
      OB.Data.localDB.readTransaction(function (tx) {
        tx.executeSql(sql, [id], function (tr, result) {
          if (result.rows.length === 0) {
            if (empty) {
              empty();
            } else {
              return null;
            }
          } else {
            success(OB.Dal.transform(model, result.rows.item(0)));
          }
        }, function (txError, e) {
          executeSqlErrorHandler('error', "OB.Dal.get: table", tableName, txError, e);
          if (_.isFunction(error)) {
            error();
          }
        });
      });
    } else {
      this.missingLocalStorageLogic();
    }
  };

  OB.Dal.initCache = function (model, initialData, success, error, incremental) {
    if (OB.Data.localDB) {
      // error must be defined, if not it fails in some android versions
      error = error ||
      function () {};

      if (!model.propertyList && (!model.prototype.createStatement || !model.prototype.dropStatement)) {
        console.warn("OB.Dal.initCache: model requires a create and drop statement");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.initCache: model requires a create and drop statement";
        });
      }

      if (!initialData) {
        console.warn("OB.Dal.initCache: initialData must be passed as parameter");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.initCache: initialData must be passed as parameter";
        });
      }

      if (!model.prototype.local && !incremental) {
        OB.Data.localDB.transaction(function (tx) {
          var st = model.getDropStatement ? model.getDropStatement() : model.prototype.dropStatement;
          tx.executeSql(st, [], null, function (txError, e) {
            executeSqlErrorHandler('error', "OB.Dal.initCache: table", model.prototype.modelName, txError, e);
            if (_.isFunction(error)) {
              error();
            }
          });
        }, error);
      }

      OB.Data.localDB.transaction(function (tx) {
        var createStatement = model.getCreateStatement ? model.getCreateStatement() : model.prototype.createStatement;
        var createIndexStatement;
        tx.executeSql(createStatement, null, function () {
          //Create Index
          if (model.hasIndex && model.hasIndex()) {
            _.each(model.getIndexes(), function (indexDefinition) {
              createIndexStatement = model.getCreateIndexStatement(indexDefinition);
              tx.executeSql(createIndexStatement, null, null, function (txError, e) {
                executeSqlErrorHandler('error', "OB.Dal.initCache: index for table", model.prototype.modelName, txError, e);
              });
            });
          }
        }, function (txError, e) {
          executeSqlErrorHandler('error', "OB.Dal.initCache: table", model.prototype.modelName, txError, e);
        });
      }, error);

      if (_.isArray(initialData)) {
        OB.Data.localDB.transaction(function (tx) {
          var props = model.getProperties ? model.getProperties() : model.prototype.properties,
              filterVal, values, _idx = 0,
              updateRecord = function (tx, model, values, active) {
              var deleteStatement;
              deleteStatement = model.getDeleteByIdStatement ? model.getDeleteByIdStatement() : "DELETE FROM " + model.prototype.tableName + " WHERE " + model.prototype.propertyMap.id + "=?";
              tx.executeSql(deleteStatement, [values[0]], function () {
                if (_.isUndefined(active) || active) {
                  var insertSatement;
                  insertSatement = model.getInsertStatement ? model.getInsertStatement() : model.prototype.insertStatement;
                  tx.executeSql(insertSatement, values, null, _.isFunction(error) ? error : null);
                }
              }, function (txError, e) {
                executeSqlErrorHandler("OB.Dal.initCache: model insert", model.prototype.modelName, txError, e);
                if (_.isFunction(error)) {
                  error();
                }
              });
              };

          _.each(initialData, function (item) {
            var filterProps, insertStatement;
            values = [];

            _.each(props, function (prop) {
              var propName = typeof prop === 'string' ? prop : prop.name;
              if (!propName || '_idx' === propName) {
                return;
              }
              values.push(item[propName]);
            });

            if ((model.hasFilter && model.hasFilter()) || model.prototype.propertiesFilter) {
              filterVal = '';
              filterProps = model.getFilterProperties ? model.getFilterProperties() : model.prototype.propertiesFilter;
              _.each(filterProps, function (prop) {
                filterVal = filterVal + (item[prop] ? (item[prop] + '###') : '');
              });
              values.push(filterVal);
            }

            values.push(_idx);
            if (incremental) {
              updateRecord(tx, model, values, item.active);
            } else if (_.isUndefined(item.active) || (item.active && item.active === true)) {
              insertStatement = model.getInsertStatement ? model.getInsertStatement() : model.prototype.insertStatement;
              tx.executeSql(insertStatement, values, null, function (txError, e) {
                executeSqlErrorHandler('error', "OB.Dal.initCache: model insert", model.prototype.modelName, txError, e);
                if (_.isFunction(error)) {
                  error();
                }
              });
            }
            _idx++;
          });
        }, error, function () {
          // transaction success, execute callback
          if (_.isFunction(success)) {
            success();
          }
        });
      } else { // no initial data
        console.warn("OB.Dal.initCache: initialData must be an Array");
        OB.UTIL.Debug.execute(function () {
          throw "OB.Dal.initCache: initialData must be an Array";
        });
      }
    } else {
      this.missingLocalStorageLogic();
    }

  };

  /**
   * Loads a set of models
   *
   *
   */
  OB.Dal.loadModels = function (online, models, data, incremental) {
    function triggerReady(models) {
      if (models._LoadOnline && OB.UTIL.queueStatus(models._LoadQueue || {})) {
        // this is only triggered when all models (online and offline) are loaded.
        // offline models are loaded first but don't trigger this, it is not till
        // windowModel is going to be rendered when online models are loaded and this
        // is triggered.
        if (!OB.MobileApp.model.get('datasourceLoadFailed')) {
          models.trigger('ready');
        }
      }
    }

    var somethigToLoad = false,
        timestamp = 0;

    models._LoadOnline = online;

    if (models.length === 0) {
      triggerReady(models);
      return;
    }

    _.each(models, function (item) {
      var ds, load;

      if (item && item.generatedModel) {
        item = OB.Model[item.modelName];
      }

      load = item && ((online && item.prototype.online) || (!online && !item.prototype.online));
      //TODO: check permissions
      if (load) {
        if (item.prototype.local) {
          OB.Dal.initCache(item, [], function () {
            // OB.info('init success: ' + item.prototype.modelName);
          }, function () {
            OB.error('init error', arguments);
          });
        } else {
          // OB.info('[sdrefresh] load model ' + item.prototype.modelName + ' ' + (incremental ? 'incrementally' : 'full'));
          if (incremental && window.localStorage.getItem('lastUpdatedTimestamp' + item.prototype.modelName)) {
            timestamp = window.localStorage.getItem('lastUpdatedTimestamp' + item.prototype.modelName);
          }
          ds = new OB.DS.DataSource(new OB.DS.Request(item, timestamp));
          somethigToLoad = true;
          models._LoadQueue = models._LoadQueue || {};
          models._LoadQueue[item.prototype.modelName] = false;
          ds.on('ready', function () {
            OB.info('[sdreresh] Loading data for ' + item.prototype.modelName);
            if (data) {
              data[item.prototype.modelName] = new Backbone.Collection(ds.cache);
            }
            models._LoadQueue[item.prototype.modelName] = true;
            if (incremental) {
              window.localStorage.setItem('POSLastIncRefresh', new Date().getTime());
            } else {
              window.localStorage.setItem('POSLastTotalRefresh', new Date().getTime());
            }
            triggerReady(models);
          });

          if (item.prototype.includeTerminalDate) {
            var currentDate = new Date();
            item.params = item.params || {};
            item.params.terminalTime = currentDate;
            item.params.terminalTimeOffset = currentDate.getTimezoneOffset();
          }
          ds.load(item.params, incremental);
        }
      }
    });

    if (!somethigToLoad) {
      triggerReady(models);
    }
  };

}());