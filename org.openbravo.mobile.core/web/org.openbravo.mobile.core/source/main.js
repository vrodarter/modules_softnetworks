/*
 ************************************************************************************
 * Copyright (C) 2013-2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 *
 * Author RAL
 *
 */

/*global OB, console*/

(function () {

  /**
   * catches application wide uncaught exceptions
   * this method should be overriden by client applications
   * do not change this method, instead create the new assignment at the client application starting point
   */
  window.onerror = function (e, url, line) {
    if (typeof (e) === 'string') {
      var errorMessage = "core.main.js: " + e + "; line: " + url + ":" + line;
      // try to log the error in the backend
      if (OB.error) {
        OB.error(errorMessage);
        if (OB.UTIL && OB.UTIL.showError) {
          OB.UTIL.showError(errorMessage);
        }
      } else {
        console.error(errorMessage);
      }
    }
  };

  /**
   * Global versions for mobile.core
   */
  // Add mobileCore version
  // TODO: pick this information from the backend
  OB.UTIL.VersionManagement.current.mobileCore = {
    year: 15,
    major: 1,
    minor: 0
  };
  // Add WebSQL database version for mobile.core
  OB.UTIL.VersionManagement.current.mobileCore.WebSQLDatabase = {
    name: 'OBMOBCDB',
    size: 4 * 1024 * 1024,
    displayName: 'Mobile DB',
    dbVersion: OB.UTIL.VersionManagement.current.mobileCore.year + "." + OB.UTIL.VersionManagement.current.mobileCore.major + OB.UTIL.VersionManagement.current.mobileCore.minor
  };

  /**
   * Register active deprecations.
   * Deprecations must be registered before calling the 'deprecated' method
   */

  OB.UTIL.VersionManagement.registerDeprecation(27332, {
    year: '14',
    major: '4',
    minor: '0'
  }, "'OB.UTILS' namespace has been removed");

  OB.UTIL.VersionManagement.registerDeprecation(27349, {
    year: '14',
    major: '4',
    minor: '0'
  }, "'OB.MobileApp.model.hookManager' is a child of the model of the terminal. Please use 'OB.UTIL.HookManager' instead. More info: http://wiki.openbravo.com/wiki/How_to_Add_Hooks");

  /**
   * Global deprecations have to be executed somewhere, this is a good place
   */

  // 27332
  OB.UTILS = OB.UTILS || {};


}());
