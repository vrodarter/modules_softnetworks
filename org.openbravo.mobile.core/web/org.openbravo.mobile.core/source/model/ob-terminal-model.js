/*
 ************************************************************************************
 * Copyright (C) 2012-2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _, enyo, Backbone, CryptoJS, console, alert, Promise */

OB.Model = window.OB.Model || {};
OB.MobileApp = OB.MobileApp || {};
OB.Data = OB.Data || {};

OB.MobileApp.windowRegistry = new(Backbone.Model.extend({
  registeredWindows: [],

  registerWindow: function (window) {
    this.registeredWindows.push(window);
  }
}))();

OB.Model.Collection = Backbone.Collection.extend({
  constructor: function (data) {
    this.ds = data.ds;
    Backbone.Collection.prototype.constructor.call(this);
  },
  inithandler: function (init) {
    if (init) {
      init.call(this);
    }
  },
  exec: function (filter) {
    var me = this;
    if (this.ds) {
      this.ds.exec(filter, function (data, info) {
        var i;
        me.reset();
        me.trigger('info', info);
        if (data.exception) {
          OB.UTIL.showError(data.exception.message);
        } else {
          for (i in data) {
            if (data.hasOwnProperty(i)) {
              me.add(data[i]);
            }
          }
        }
      });
    }
  }
});

// Terminal model.
OB.Model.Terminal = Backbone.Model.extend({

  defaults: {
    terminal: null,
    context: null,
    permissions: null,
    businesspartner: null,
    location: null,
    pricelist: null,
    pricelistversion: null,
    currency: null,
    connectedToERP: null,
    loginUtilsUrl: '../../org.openbravo.mobile.core.loginutils',
    loginUtilsParams: {},
    supportsOffline: false,
    loginHandlerUrl: '../../org.openbravo.mobile.core/LoginHandler',
    applicationFormatUrl: '../../org.openbravo.client.kernel/OBCLKER_Kernel/Application',
    logConfiguration: {
      deviceIdentifier: '',
      logPropertiesExtension: []
    },
    windows: null,
    appName: 'OBMOBC',
    appDisplayName: 'Openbravo Mobile',
    useBarcode: false,
    profileOptions: {
      showOrganization: true,
      showWarehouse: true,
      defaultProperties: {
        role: null,
        organization: null,
        warehouse: null,
        languagage: null
      }
    },
    localDB: {
      size: OB.UTIL.VersionManagement.current.mobileCore.WebSQLDatabase.size,
      name: OB.UTIL.VersionManagement.current.mobileCore.WebSQLDatabase.name,
      displayName: OB.UTIL.VersionManagement.current.mobileCore.WebSQLDatabase.displayName,
      version: OB.UTIL.VersionManagement.current.mobileCore.WebSQLDatabase.dbversion
    },
    propertiesLoaders: [],
    dataSyncModels: []
  },

  initialize: function () {
    // expose terminal globally
    window.OB.MobileApp = OB.MobileApp || {};
    window.OB.MobileApp.model = this;

    OB.Dal.openWebSQL();

    // attach objects to model
    this.router = new OB.Model.RouterHelper(this);
    this.windowRegistry = OB.MobileApp.windowRegistry;
    OB.UTIL.VersionManagement.deprecated(27349, function () {
      this.hookManager = OB.UTIL.HookManager;
    }, function (deprecationMessage) {
      this.hookManager = {
        registerHook: function (qualifier, func) {
          OB.warn(deprecationMessage + ". Hook: '" + qualifier + "'");
          OB.UTIL.HookManager.registerHook(qualifier, func);
        },
        executeHooks: function (qualifier, args, callback) {
          OB.warn(deprecationMessage + ". Hook: '" + qualifier + "'");
          OB.UTIL.HookManager.executeHooks(qualifier, args, callback);
        },
        callbackExecutor: function (args, callback) {
          OB.warn(deprecationMessage);
          OB.UTIL.HookManager.callbackExecutor(args, callback);
        }
      };
    }, this);

    // DEVELOPER: activate this to see all the events that are being fired by the backbone components of the terminal
    // this.on('all', function(eventName, object, third) {
    //   if (eventName.indexOf('change') >= 0) {
    //     return;
    //   }
    //   var enyoName = object ? object.name : '';
    //   OB.info('event fired: ' + eventName + ', ' + enyoName + ', ' + third);
    // });
    // model events
    this.on('window:ready', this.renderContainerWindow); // When the active window is loaded, the window is rendered with the renderContainerWindow function
    this.on('terminalInfoLoaded', function () {
      OB.debug("next process: processPropertyLoaders");
      this.processPropertyLoaders();
    });
    this.on('propertiesLoadersReady', function () {
      OB.debug("next process: loadRegisteredWindows, renderMain, postLoginActions");
      this.loadRegisteredWindows();
      this.renderMain();
      this.postLoginActions();
    }, this);

    // when the page has finished loading the components, load the root webpage
    window.addEventListener('load', function (e) {
      // explicitly setting pushState to false tries to ensure...
      // that future default modes will not activate the pushState...
      // breaking the POS navigation
      // setTimeout to move the execution until all 'load' listeners have finished
      setTimeout(function () {
        Backbone.history.start({
          root: '',
          pushState: false
        });
      }, 0);
    }, false);
  },

  /**
   * Method to be called when there is an error while loading the application
   * @param  {String}  caller   a string that identifies the process that failed
   */
  loadingErrorsActions: function (caller) {
    // arguments check
    OB.UTIL.Debug.execute(function () {
      if (!caller) {
        throw "missing parameter: caller";
      }
    });

    var link = 'NA';
    try {
      var errorobj = new Error();
      link = OB.UTIL.getStackLink(3);
    } catch (ex) {
      console.error("executeSqlErrorHandler.getStackLink: " + ex);
    }
    console.error("loadingErrorsActions: '" + caller + "' failed while loading the application. line: " + link);
  },

  cleanTerminalData: function () {
    _.each(this.get('propertiesLoaders'), function (curPropertiesToLoadProcess) {
      _.each(curPropertiesToLoadProcess.properties, function (curProperty) {
        this.set(curProperty, null);
      }, this);
    }, this);
  },

  returnToOnline: function () {
    //DEVELOPERS: overwrite this function to perform actions when it happens
    OB.info('The system has come back to online');
  },

  //DEVELOPER: This function allow us to execute some code to add properties to to terminal model. Each position of the array
  // must identify the properties which are set and needs to define a loadFunction which will be executed to get the data.
  // when the data is ready terminalModel.propertiesReady(properties) should be executed.
  addPropertiesLoader: function (obj) {
    this.get('propertiesLoaders').push(obj);
    this.syncAllPropertiesLoaded = _.after(this.get('propertiesLoaders').length, this.allPropertiesLoaded);
  },

  propertiesReady: function (properties) {
    OB.info("[sdrefresh] '" + properties + "' property loaded");
    this.syncAllPropertiesLoaded();
  },

  processPropertyLoaders: function () {
    OB.debug("next process: allPropertiesLoaded");
    var termInfo;
    if (this.get('loggedOffline') || this.get('loggedUsingCache')) {
      //Load from termInfo
      if (this.usermodel.get('terminalinfo')) {
        termInfo = JSON.parse(this.usermodel.get('terminalinfo'));
        //TODO load all the recovered properties not only the array ones
        _.each(this.get('propertiesLoaders'), function (curPropertiesToLoadProcess) {
          _.each(curPropertiesToLoadProcess.properties, function (curProperty) {
            this.set(curProperty, termInfo[curProperty]);
          }, this);
        }, this);

        this.set('useBarcode', termInfo.useBarcode);
        //Not included into array
        this.set('permissions', termInfo.permissions);
        this.set('orgUserId', termInfo.orgUserId);

        this.allPropertiesLoaded();
      } else {
        // not enough data in cache
        OB.MobileApp.model.loadingErrorsActions('processPropertyLoaders: not enough data in cache');
        window.localStorage.setItem('cacheAvailableForUser:' + this.user, false);
        // OB.MobileApp.model.logout();
        // window.location.reload();
      }
      return;
    }
    this.set('loggedOffline', false);
    //Loading the properties of the array
    OB.info("[terminal] Starting to load properties based on properties loaders, count: " + this.get('propertiesLoaders').length);
    if (this.get('propertiesLoaders') && this.get('propertiesLoaders').length > 0) {
      _.each(this.get('propertiesLoaders'), function (curProperty) {
        //each loadFunction will call to propertiesReady function. This function will trigger
        //allPropertiesLoaded when all of the loadFunctions are done.
        curProperty.loadFunction(this);
      }, this);
    } else {
      this.allPropertiesLoaded();
    }
  },

  //DEVELOPER: this function will be automatically called when all the properties defined in
  //this.get('propertiesLoaders') are loaded. To indicate that a property is loaded
  //me.propertiesReady(properties) should be executed by loadFunction of each property
  allPropertiesLoaded: function () {
    OB.debug("next process: propertiesLoadersReady");
    OB.info('[terminal] Properties have been successfully loaded');
    // activate this line to see all the attributes (all the key data from the server)
    // OB.debug('[terminal] Properties have been successfully loaded', this.attributes);
    var undef;
    this.loggingIn = false;
    //core
    if (!this.get('loggedOffline') && !this.get('loggedUsingCache')) {
      //In online mode, we save the terminal information in the local db
      this.usermodel.set('terminalinfo', JSON.stringify(this));
      OB.Dal.save(this.usermodel, function () {}, function () {
        OB.error("allPropertiesLoaded", arguments);
      });
    }
    //renderMain
    if (!OB.MobileApp.model.get('datasourceLoadFailed')) {
      this.trigger('propertiesLoadersReady');
    }
  },

  navigate: function (route) {
    OB.debug("Navigating to '" + route + "'");
    this.router.navigate(route, {
      trigger: true,
      replace: true
    });
  },

  /**
   * Loads registered windows and calls renderMain method implemented by each app
   */
  renderTerminalMain: function () {
    OB.UTIL.Debug.execute(function () {
      if (!this.renderMain) {
        throw "There is no renderMain method in Terminal Model";
      }
    }, this);
    this.loadTerminalInfo();
  },

  /**
   * Loads all needed stuff for the terminal such as permissions, Application...
   */
  loadTerminalInfo: function () {
    OB.debug("next process: terminalInfoLoaded");
    var me = this,
        loadTerminalInfoQueue = {};

    function triggerLoadTerminalInfoReady() {
      OB.debug("triggerLoadTerminalInfoReady: " + (OB.UTIL.queueStatus(loadTerminalInfoQueue) === true));
      if (OB.UTIL.queueStatus(loadTerminalInfoQueue)) {
        me.setUserModelOnline(function () {
          me.trigger('terminalInfoLoaded');
        });
      }
    }

    if (window.openDatabase) {
      this.initLocalDB(loadTerminalInfoQueue, triggerLoadTerminalInfoReady);
    }

    if (OB.MobileApp.model.get('loggedOffline') || OB.MobileApp.model.get('loggedUsingCache')) {
      triggerLoadTerminalInfoReady();
      OB.Format = JSON.parse(window.localStorage.getItem('AppFormat'));
      OB.I18N.labels = JSON.parse(window.localStorage.getItem('I18NLabels_' + window.localStorage.getItem('languageForUser_' + me.usermodel.get('id'))));
      return;
    }

    loadTerminalInfoQueue.preferences = false;
    new OB.DS.Request('org.openbravo.mobile.core.login.RolePermissions').exec({
      appModuleId: this.get('appModuleId')
    }, function (data) {
      var i, max, separator, permissions = {};
      if (data) {
        for (i = 0, max = data.length; i < max; i++) {
          permissions[data[i].key] = data[i].value; // Add the permission value.
          separator = data[i].key.indexOf('_');
          if (separator >= 0) {
            permissions[data[i].key.substring(separator + 1)] = data[i].value; // if key has a DB prefix, add also the permission value without this prefix
          }
        }
        me.set('permissions', permissions);
        // TODO: offline + usermodel
        loadTerminalInfoQueue.preferences = true;
        triggerLoadTerminalInfoReady();
      }
    });

    loadTerminalInfoQueue.application = false;
    new enyo.Ajax({
      url: this.get('applicationFormatUrl'),
      method: 'GET',
      handleAs: 'text'
    }).response(function (inSender, inResponse) {
      eval(inResponse); //...we have an OB variable local to this function
      // we want to 'export' some properties to global OB
      window.OB.Application = OB.Application;
      window.OB.Format = OB.Format;
      window.localStorage.setItem('AppFormat', JSON.stringify(OB.Format));
      window.OB.Constants = OB.Constants;
      loadTerminalInfoQueue.application = true;
      triggerLoadTerminalInfoReady();
      window.localStorage.setItem('cacheAvailableForUser:' + me.user, true);
    }).go();

  },

  isSafeToResetDatabase: function (callbackIsSafe, callbackIsNotSafe) {
    callbackIsSafe();
  },

  databaseCannotBeResetAction: function () {
    //Will never happen in standard mobile core. Should be overwritten in applications if isSafeToResetDatabase is implemented
  },

  initLocalDB: function (loadTerminalInfoQueue, triggerLoadTerminalInfoReady) {
    // arguments check
    OB.UTIL.Debug.execute(function () {
      if (!loadTerminalInfoQueue || !triggerLoadTerminalInfoReady) {
        throw "The database is no longer opened here. Call this function only from loadTerminalInfo. Use OB.Dal.openWebSQL() to open the database";
      }
    });

    OB.Dal.openWebSQL();
    var db = OB.Data.localDB;

    function dropTable(db, sql) {
      return new Promise(function (resolve, reject) {
        db.transaction(function (tx) {
          tx.executeSql(sql, {}, function () {
            resolve();
            OB.debug('succesfully dropped table: ' + sql);
          }, function () {
            console.error(OB.UTIL.argumentsToStringifyed("dropTable", arguments));
            reject();
          });
        });
      });
    }

    function dropTables(callback) {
      var synchId = OB.UTIL.SynchronizationHelper.busyUntilFinishes('dropTables');
      loadTerminalInfoQueue.dropTables = false;
      var model;
      var modelObj;
      OB.info('Updating database model. Tables will be dropped:');
      var tablesToBeDropped = [];
      for (model in OB.Model) {
        if (OB.Model.hasOwnProperty(model)) {
          modelObj = OB.Model[model];
          if ((modelObj.prototype && modelObj.prototype.dropStatement) || modelObj.getDropStatement) {
            //There is a dropStatement, executing it
            tablesToBeDropped.push(dropTable(db, modelObj.getDropStatement ? modelObj.getDropStatement() : modelObj.prototype.dropStatement));
          }
        }
      }
      Promise.all(true, tablesToBeDropped).then(function () {
        OB.UTIL.SynchronizationHelper.finished(synchId, 'dropTables');
        loadTerminalInfoQueue.dropTables = true;
        callback();
      }, function () {
        OB.UTIL.SynchronizationHelper.finished(synchId, 'dropTables');
        loadTerminalInfoQueue.dropTables = true;
        callback();
      });
    }

    // check if terminal id has changed
    if (window.localStorage.getItem('loggedTerminalName') && (window.localStorage.getItem('loggedTerminalName') !== OB.MobileApp.model.get('terminalName'))) {
      console.warn(OB.UTIL.argumentsToStringifyed("terminal changed (" + window.localStorage.getItem('loggedTerminalName') + " -> " + OB.MobileApp.model.get('terminalName') + ")"));
      OB.MobileApp.model.isSafeToResetDatabase(function () {
        OB.info('Terminal has been changed. Resetting database and local storage information.');
        window.localStorage.clear();
        window.localStorage.setItem('terminalName', OB.MobileApp.model.get('terminalName'));
        dropTables(function () {
          OB.MobileApp.model.logout();
        });
      }, function () {
        OB.MobileApp.model.databaseCannotBeResetAction();
      });
      return;
    }

    // check if the database version has changed
    var dbInfo = this.get('localDB');
    if (db.version !== '' && db.version !== dbInfo.version) {
      console.warn("database version changed (" + db.version + " -> " + dbInfo.version + ")");
      db.changeVersion(db.version, dbInfo.version, function (t) {
        OB.MobileApp.model.isSafeToResetDatabase(function () {
          // version of the database changed, we need to drop the tables so they can be created again
          dropTables(triggerLoadTerminalInfoReady);
        }, OB.MobileApp.model.databaseCannotBeResetAction);
      }, function (e) {
        console.error("WebSQL error: changeVersion (" + db.version + " -> " + dbInfo.version + ") failed, message: " + (e.message ? e.message : e));
        OB.Data.localDB = undefined;
        OB.Dal.openWebSQL();
        triggerLoadTerminalInfoReady();
      });
      return;
    }

  },

  /**
   * initActions actions is executed before initializeCommonComponents.
   *
   * Override this in case you app needs to do something special
   * Do not forget to execute callback when overriding the method
   */
  initActions: function (callback) {
    callback();
  },

  /**
   * PreLoadContext actions is executed before loading the context.
   *
   * Override this in case you app needs to do something special
   * Do not forget to execute callback when overriding the method
   */
  preLoadContext: function (callback) {
    callback();
  },

  /** Data Synchronized Flag.
   *  It is false till all data have been synchronized. By default is true because no all mobile applications sync models.
   **/
  dataSynchronized: true,

  /** Recursively syncs models.
   *  Should not be called directly. syncAllModels should be used instead.
   **/
  syncModel: function (index, successCallback, errorCallback) {
    var me = this,
        modelObj, model, criteria = {
        hasBeenProcessed: 'Y'
        };
    if (index === this.get('dataSyncModels').length) {
      me.dataSynchronized = true;
      if (successCallback) {
        successCallback();
      }
      return;
    }
    modelObj = this.get('dataSyncModels')[index];
    if (modelObj.criteria) {
      criteria = modelObj.criteria;
    }
    criteria._limit = -1;
    model = modelObj.model;
    OB.Dal.find(model, criteria, function (dataToSync) {
      var className = modelObj.className;
      var timeout = modelObj.timeout || 20000;
      var timePerRecord = modelObj.timePerRecord || 1000;
      var proc;
      var dataToSend = [];
      var modelIndex = 0;
      var modelNotFullySynced = false;
      var newDataToSync = new Backbone.Collection();

      if (dataToSync.length === 0) {
        return me.syncModel(index + 1, successCallback, errorCallback);
      }

      if (model === OB.Model.ChangedBusinessPartners && OB.UTIL.processCustomerClass) {
        className = OB.UTIL.processCustomerClass;
      } else if (model === OB.Model.Order && OB.UTIL.processOrderClass) {
        className = OB.UTIL.processOrderClass;
      }

      proc = new OB.DS.Process(className);

      while (modelIndex < dataToSync.length) {
        newDataToSync.add(dataToSync.at(modelIndex));
        modelIndex++;
        if (modelIndex >= 100 && modelIndex < dataToSync.length) {
          modelNotFullySynced = true;
          break;
        }
      }

      newDataToSync.each(function (record) {
        if (record.get('json')) {
          dataToSend.push(JSON.parse(record.get('json')));
        } else if (record.get('objToSend')) {
          dataToSend.push(JSON.parse(record.get('objToSend')));
        } else {
          dataToSend.push(record);
        }
      });
      proc.exec({
        data: dataToSend
      }, function (data, message) {
        var removeSyncedElemsCallback = function () {

            newDataToSync.each(function (record) {
              if (modelObj.isPersistent) {
                // Persistent model. Do not delete, just mark it as processed.
                record.set('isbeingprocessed', 'Y');
                OB.Dal.save(record, null, function (tx, err) {
                  OB.UTIL.showError(err);
                });
              } else {
                // No persistent model (Default).
                OB.Dal.remove(record, null, function (tx, err) {
                  OB.UTIL.showError(err);
                });
              }
            });
            if (modelNotFullySynced) {
              OB.info(newDataToSync.length + " records of the model '" + OB.Dal.getTableName(model) + "' have been successfully synchronized with the server. " + (dataToSync.length - newDataToSync.length) + " records remaining to be synchronized.");
              me.syncModel(index, successCallback, errorCallback);
            } else {
              OB.info("The model '" + OB.Dal.getTableName(model) + "' has been fully synchronized with the server");
              me.syncModel(index + 1, successCallback, errorCallback);
            }
            };
        if (data && data.exception) {
          //Error
          OB.warn("The model '" + OB.Dal.getTableName(model) + "'' has not been synchronized with the server");
          if (data.exception.invalidPermission && !me.get('displayedInvalidPermission')) {
            // invalid permission message only will be displayed once time
            me.set('displayedInvalidPermission', true);
            OB.UTIL.showConfirmation.display('Info', OB.I18N.getLabel('OBMOBC_NoPermissionToSyncModel', [OB.Dal.getTableName(model), OB.Dal.getTableName(model)]), [{
              label: OB.I18N.getLabel('OBMOBC_LblOk'),
              isConfirmButton: true,
              action: function () {}
            }]);
          }
          me.dataSynchronized = true;
          if (errorCallback) {
            errorCallback();
          }
        } else {
          //Success. Elements can be now deleted from the database
          if (modelObj.postProcessingFunction) {
            modelObj.postProcessingFunction(newDataToSync, removeSyncedElemsCallback);
          } else {
            removeSyncedElemsCallback();
          }
        }
      }, null, null, timeout + timePerRecord * newDataToSync.length);
    }, function () {
      OB.warn("Error while synchronizing model '" + OB.Dal.getTableName(model) + "'");
      me.dataSynchronized = true;
      if (errorCallback) {
        errorCallback();
      }
    });
  },

  /** Automatically synchronizes all dataSyncModels
   *
   **/
  syncAllModels: function (successCallback, errorCallback) {
    if (!this.get('dataSyncModels') || this.get('dataSyncModels').length === 0) {
      return;
    }
    this.dataSynchronized = false;
    this.syncModel(0, successCallback, errorCallback);
  },

  /**
   * If any module needs to perform operations with properties coming from the server
   * this function must be overwritten
   */
  isUserCacheAvailable: function () {
    return false;
  },

  /**
   * Invoked from initComponents in terminal view
   */
  initializeCommonComponents: function () {
    var me = this,
        params = {};
    window.localStorage.setItem('LOGINTIMER', new Date().getTime());

    params = this.get('loginUtilsParams') || {};
    params.command = 'preRenderActions';
    // initialize labels and other common stuff
    var ajaxRequest = new enyo.Ajax({
      url: this.get('loginUtilsUrl'),
      cacheBust: false,
      timeout: 20000,
      method: 'GET',
      handleAs: 'json',
      contentType: 'application/json;charset=utf-8',
      data: params,
      success: function (inSender, inResponse) {
        OB.appCaption = inResponse.appCaption;
        if (_.isEmpty(inResponse.labels) || _.isNull(inResponse.labels) || _.isUndefined(inResponse.labels)) {
          OB.I18N.labels = JSON.parse(window.localStorage.getItem('I18NLabels'));
        } else {
          OB.I18N.labels = inResponse.labels;
          if (inResponse.activeSession) {
            // The labels will only be saved in the localStorage if they come from an active session
            // We do this to prevent the labels loaded in the login page from being stored here and overwritting
            // the labels in the correct language (see issue 23613)
            window.localStorage.setItem('languageForUser_' + inResponse.labels.userId, inResponse.labels.languageId);
            window.localStorage.setItem('I18NLabels', JSON.stringify(OB.I18N.labels));
            window.localStorage.setItem('I18NLabels_' + inResponse.labels.languageId, JSON.stringify(OB.I18N.labels));
          }
        }
        if (_.isEmpty(inResponse.dateFormats) || _.isNull(inResponse.dateFormats) || _.isUndefined(inResponse.dateFormats)) {
          OB.Format = JSON.parse(window.localStorage.getItem('AppFormat'));
        } else {
          OB.Format = inResponse.dateFormats;
        }

        me.trigger('initializedCommonComponents');
        me.preLoadContext(function () {
          new OB.DS.Request('org.openbravo.mobile.core.login.Context').exec({
            ignoreForConnectionStatus: true
          }, function (inResponse) {
            OB.MobileApp.model.triggerOnLine();
            if (inResponse && !inResponse.exception) {
              OB.MobileApp.model.user = inResponse[0].user.username;
              if (me.isUserCacheAvailable() && window.localStorage.getItem('cacheAvailableForUser:' + OB.MobileApp.model.user)) {
                me.loginUsingCache();
              } else {
                OB.MobileApp.model.set('orgUserId', inResponse[0].user.id);
                OB.UTIL.showLoading(true);
                me.set('context', inResponse[0]);
                me.renderTerminalMain();
              }
            } else {
              OB.MobileApp.model.navigate('login');
            }
          }, function () {
            OB.MobileApp.model.navigate('login');
          });
        });
      },
      fail: function (inSender, inResponse) {
        if (this.alreadyProcessed) {
          return;
        }
        this.alreadyProcessed = true;
        // we are likely offline. Attempt to navigate to the login page
        OB.I18N.labels = JSON.parse(window.localStorage.getItem('I18NLabels'));
        OB.Format = JSON.parse(window.localStorage.getItem('AppFormat'));
        me.trigger('initializedCommonComponents');
        if (!OB.I18N.labels || !OB.Format) {
          // These 2 objects are needed for the application to show i18n messages and date and number formats
          OB.UTIL.showLoading(false);
          var errorMsg = "The server is not available and the cache has not enough data. Connect to the server and try again.";
          OB.error("initializeCommonComponents", errorMsg);
          OB.UTIL.showConfirmation.display('Error', errorMsg, [{
            label: 'Retry',
            isConfirmButton: true,
            action: function () {
              // Retry. If the server is not available, the message will open again
              me.initializeCommonComponents();
            }
          }], {
            onHideFunction: function () {
              // Retry. If the server is not available, the message will open again
              me.initializeCommonComponents();
            }
          });
          return;
        }
        OB.MobileApp.model.navigate('login');
      }
    });

    ajaxRequest.go(ajaxRequest.data).response('success').error('fail');
  },

  renderLogin: function () {
    if (!OB.MobileApp.view) {
      // the terminal view has yet to be created
      OB.UTIL.Debug.execute(function () {
        throw "OB.MobileApp.view must be defined";
      });
      OB.MobileApp.model.navigate('');
      return;
    }
    if (!OB.MobileApp.view.$.containerWindow) {
      // this can happen if the webpage is reloaded
      // OB.UTIL.Debug.execute(function () {
      //   console.error("OB.MobileApp.view.$.containerWindow must be defined");
      // });
      OB.MobileApp.model.navigate('');
      return;
    }

    if (OB.Data.localDB) {
      OB.Dal.initCache(OB.Model.LogClient, [], null, null);
    }

    OB.MobileApp.view.$.containerWindow.destroyComponents();
    OB.MobileApp.view.$.containerWindow.createComponent({
      kind: OB.OBPOSLogin.UI.Login
    }).render();
    OB.UTIL.showLoading(false);
  },

  loadModels: function (windowv, incremental) {
    var windows, i, windowName, windowClass, datasources, timestamp = 0,
        w, c, path;

    if (OB.MobileApp.model.get('loggedOffline')) {
      return;
    }

    // If full data refresh is being done, the login process should continue normally,
    // not using the cache. The reason is that in the case of a full refresh, we
    // should wait until the data is loaded to start rendering the application.
    // In case of incremental refresh however, the application can be rendered and the
    // data can be loaded in parallel
    if (!incremental) {
      OB.MobileApp.model.set('loggedUsingCache', false);
    }

    if (windowv) {
      windows = [windowv];
    } else {
      windows = [];
      _.each(this.windowRegistry.registeredWindows, function (windowp) {
        windows.push(windowp);
      });
    }

    OB.info('[sdrefresh] Load models ' + (incremental ? 'incrementally' : 'full') + '.');

    for (i = 0; i < windows.length; i++) {
      windowClass = windows[i].windowClass;
      windowName = windows[i].route;
      if (OB && OB.DATA && OB.DATA[windowName]) {
        // old way of defining datasources...
        datasources = OB.DATA[windowName];
      } else if (windowClass.prototype && windowClass.prototype.windowmodel && windowClass.prototype.windowmodel.prototype && windowClass.prototype.windowmodel.prototype.models) {
        datasources = windowClass.prototype.windowmodel.prototype.models;
      } else if (typeof windowClass === 'string') {
        w = window; // global window
        path = windowClass.split('.');
        for (c = 0; c < path.length; c++) {
          w = w[path[c]];
        }

        if (w.prototype && w.prototype.windowmodel && w.prototype.windowmodel.prototype && w.prototype.windowmodel.prototype.models) {
          datasources = w.prototype.windowmodel.prototype.models;
        }
      }

      _.extend(datasources, Backbone.Events);

      OB.debug('[sdrefresh] window: ' + windowName);

      OB.Dal.loadModels(false, datasources, null, incremental);
    }
  },

  cleanWindows: function () {
    this.windows = new(Backbone.Collection.extend({
      comparator: function (window) {
        // sorts by menu position, 0 if not defined
        var position = window.get('menuPosition');
        return position ? position : 0;
      }
    }))();
  },

  registerWindow: function (window) {
    this.windowRegistry.registerWindow(window);
  },

  /**
   * Iterates over all registered windows loading their models
   */
  loadRegisteredWindows: function () {
    OB.debug("next process: renderMain");

    this.cleanWindows();

    setInterval(OB.UTIL.processLogClientAll, 30000);

    var countOfLoadedWindows = 0;
    _.each(this.windowRegistry.registeredWindows, function (windowp) {
      var datasources = [],
          windowClass, windowName = windowp.route,
          minTotalRefresh, minIncRefresh, lastTotalRefresh, lastIncRefresh, intervalTotal, intervalInc, now, terminalType;
      this.windows.add(windowp);
      //    if (!OB.POS.windowObjs) {
      //      OB.POS.windowObjs = [];
      //    }
      //    OB.POS.windowObjs.push(windowp);
      //    this.loadModels(windowp, false);
      //    if (false) {
      terminalType = OB.MobileApp.model.get('terminal') && OB.MobileApp.model.get('terminal').terminalType;
      if (terminalType) {
        minTotalRefresh = OB.MobileApp.model.get('terminal').terminalType.minutestorefreshdatatotal * 60 * 1000;
        minIncRefresh = OB.MobileApp.model.get('terminal').terminalType.minutestorefreshdatainc * 60 * 1000;
        lastTotalRefresh = window.localStorage.getItem('POSLastTotalRefresh');
        lastIncRefresh = window.localStorage.getItem('POSLastIncRefresh');
      }
      if ((!minTotalRefresh && !minIncRefresh) || (!lastTotalRefresh && !lastIncRefresh)) {
        // If no configuration of the masterdata loading has been done,
        // or an initial load has not been done, then always do
        // a total refresh during the login
        this.loadModels(windowp, false);
      } else {
        now = new Date().getTime();
        intervalTotal = lastTotalRefresh ? (now - lastTotalRefresh - minTotalRefresh) : 0;
        intervalInc = lastIncRefresh ? (now - lastIncRefresh - minIncRefresh) : 0;
        if (intervalTotal > 0) {
          this.set('FullRefreshWasDone', true);

          //It's time to do a full refresh
          this.loadModels(windowp, false);
        }
      }
      //}
      this.router.route(windowName, windowName, function () {
        this.renderGenericWindow(windowName);
      });

      this.router.route(windowName + "/:params", windowName, function (params) {
        this.renderGenericWindow(windowName, params);
      });
      countOfLoadedWindows += 1;
    }, this);
    // checks if the windows loaded are more than the windows registered, which never should happen
    var countOfRegisteredWindows = this.windowRegistry.registeredWindows.length;
    console.assert(countOfRegisteredWindows === countOfLoadedWindows, 'DEVELOPER: There are ' + countOfRegisteredWindows + ' registered windows but ' + countOfLoadedWindows + ' are being loaded');
  },

  isWindowOnline: function (route) {
    var i, windows;
    windows = this.windows.toArray();
    for (i = 0; i < windows.length; i++) {
      if (windows[i].get('route') === route) {
        return windows[i].get('online');
      }
    }
    return false;
  },

  renderGenericWindow: function (windowName, params) {
    if (!OB.MobileApp.view.$.containerWindow) {
      OB.UTIL.Debug.execute(function () {
        console.error("OB.MobileApp.view.$.containerWindow must be defined");
      });
      OB.MobileApp.model.loadingErrorsActions();
      return;
    }

    OB.UTIL.showLoading(true);
    var terminal = OB.MobileApp.model.get('terminal'),
        windowClass;

    windowClass = this.windows.where({
      route: windowName
    })[0].get('windowClass');

    OB.MobileApp.view.$.containerWindow.destroyComponents();
    OB.MobileApp.view.$.containerWindow.createComponent({
      kind: windowClass,
      params: params
    });
  },

  renderContainerWindow: function () {
    OB.MobileApp.view.$.containerWindow.render();
    OB.UTIL.showLoading(false);
    OB.MobileApp.view.$.containerWindow.resized();
    //enyo.dispatcher.capture(OB.MobileApp.view, false);
  },

  updateSession: function (user) {
    OB.Dal.find(OB.Model.Session, {
      'user': user.get('id')
    }, function (sessions) {
      var session;
      if (sessions.models.length === 0) {
        session = new OB.Model.Session();
        session.set('user', user.get('id'));
        session.set('terminal', OB.MobileApp.model.get('terminalName'));
        session.set('active', 'Y');
        OB.Dal.save(session, function () {}, function () {
          OB.error("updateSession", arguments);
        });
      } else {
        session = sessions.models[0];
        session.set('active', 'Y');
        OB.Dal.save(session, function () {}, function () {
          OB.error("updateSession", arguments);
        });
      }
      OB.MobileApp.model.set('session', session.get('id'));
    }, function () {
      OB.error("updateSession", arguments);
    });
  },

  /**
   * This method is invoked when closing session, it is in charge
   * of dealing with all stuff needed to close session. After it is
   * done it MUST do triggerLogout
   */
  postCloseSession: function (session) {
    OB.MobileApp.model.triggerLogout();
  },

  closeSession: function () {
    var sessionId = OB.MobileApp.model.get('session'),
        me = this;
    if (!this.get('supportsOffline') || !OB.Model.Session) {
      OB.MobileApp.model.triggerLogout();
      return;
    }

    OB.Dal.get(OB.Model.Session, sessionId, function (session) {
      session.set('active', 'N');
      OB.Dal.save(session, function () {
        me.postCloseSession(session);
      }, function () {
        OB.error("closeSession", arguments);
        OB.MobileApp.model.triggerLogout();
      });
    }, function () {
      OB.error("closeSession", arguments);
      OB.MobileApp.model.triggerLogout();
    }, function () {
      OB.error("closeSession", arguments);
      OB.MobileApp.model.triggerLogout();
    });
  },

  generate_sha1: function (theString) {
    return CryptoJS.enc.Hex.stringify(CryptoJS.SHA1(theString));
  },

  attemptToLoginOffline: function () {
    var me = this;
    OB.MobileApp.model.set('windowRegistered', undefined);
    OB.MobileApp.model.set('loggedOffline', true);
    OB.Dal.find(OB.Model.User, {
      'name': me.user
    }, function (users) {
      var user;
      if (users.models.length === 0) {
        alert(OB.I18N.getLabel('OBMOBC_OfflinePasswordNotCorrect'));
        window.location.reload();
      } else {
        if (users.models[0].get('password') === me.generate_sha1(me.password + users.models[0].get('created'))) {
          me.usermodel = users.models[0];
          me.set('orgUserId', users.models[0].id);
          me.updateSession(me.usermodel);
          me.renderTerminalMain();
        } else {
          alert(OB.I18N.getLabel('OBMOBC_OfflinePasswordNotCorrect'));
          window.location.reload();
        }
      }
    }, function () {});
  },

  postLoginActions: function () {
    OB.debug("next process: none");
  },

  loginUsingCache: function () {
    var me = this;
    OB.Dal.find(OB.Model.User, {
      'name': me.user
    }, function (users) {
      if (!users.models[0]) {
        // the data for the user is not valid
        OB.MobileApp.model.loadingErrorsActions('loginUsingCache I');
        return;
      }
      me.usermodel = users.models[0];
      me.set('orgUserId', users.models[0].id);
      OB.MobileApp.model.set('windowRegistered', undefined);
      OB.MobileApp.model.set('loggedUsingCache', true);
      me.updateSession(me.usermodel);
      me.renderTerminalMain();
    }, function () {
      // if the cache is corrupted
      OB.MobileApp.model.loadingErrorsActions('loginUsingCache II');
    });
  },

  /**
   * Prelogin actions is executed before login, it should take care of
   * removing all session values specific by application.
   *
   * Override this in case you app needs to do something special
   */
  preLoginActions: function () {
    OB.debug("next process: none. set your application start point here");
  },

  login: function (user, password, mode) {
    var params;
    OB.UTIL.showLoading(true);
    var me = this;
    me.user = user;
    me.password = password;

    // invoking app specific actions
    this.preLoginActions();
    this.set('isLoggingIn', true);

    params = this.get('loginUtilsParams') || {};
    params.user = user;
    params.password = password;
    params.Command = 'DEFAULT';
    params.IsAjaxCall = 1;
    params.appName = this.get('appName');

    var ajaxRequest = new enyo.Ajax({
      url: this.get('loginHandlerUrl'),
      cacheBust: false,
      method: 'POST',
      timeout: 5000,
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: params,
      success: function (inSender, inResponse) {
        var pos, baseUrl;
        if (this.alreadyProcessed) {
          return;
        }
        this.alreadyProcessed = true;
        if (inResponse && inResponse.sourceVersion) {
          OB.UTIL.checkSourceVersion(inResponse.sourceVersion, true);
        }
        if (OB.MobileApp.model.get('timeoutWhileLogin')) {
          OB.MobileApp.model.set('timeoutWhileLogin', false);
          return;
        }
        if (inResponse && inResponse.showMessage) {
          me.triggerLoginFail(401, mode, inResponse);
          return;
        }
        if (OB.MobileApp.model.get('terminalName') && !window.localStorage.getItem('loggedTerminalName')) {
          window.localStorage.setItem('loggedTerminalName', OB.MobileApp.model.get('terminalName'));
        }
        OB.MobileApp.model.set('orgUserId', inResponse.userId);
        me.set('loggedOffline', false);
        if (me.get('supportsOffline')) {
          me.setUserModelOnline();
        }
        if (me.isUserCacheAvailable() && window.localStorage.getItem('cacheAvailableForUser:' + user)) {
          me.loginUsingCache();
        } else {
          me.initializeCommonComponents();
        }
      },
      fail: function (inSender, inResponse) {
        if (this.alreadyProcessed) {
          return;
        }
        this.alreadyProcessed = true;
        me.attemptToLoginOffline();
      }
    });
    ajaxRequest.go(ajaxRequest.data).response('success').error('fail');
  },

  setUserModelOnline: function (callback) {
    OB.debug("next process:", callback);
    var me = this;
    OB.Dal.initCache(OB.Model.LogClient, [], function () {
      OB.Dal.initCache(OB.Model.Session, [], function () {
        OB.Dal.initCache(OB.Model.User, [], function () {
          OB.Dal.find(OB.Model.User, {
            'name': me.user
          }, function (users) {
            var user, session, date, savedPass;
            if (users.models.length === 0) {
              date = new Date().toString();
              user = new OB.Model.User();
              user.set('name', me.user);
              user.set('id', OB.MobileApp.model.get('orgUserId'));
              savedPass = me.generate_sha1(me.password + date);
              user.set('password', savedPass);
              user.set('created', date);
              user.set('formatInfo', JSON.stringify(OB.Format));
              me.usermodel = user;
              OB.Dal.save(user, function () {
                if (callback) {
                  callback();
                }
              }, function () {
                OB.MobileApp.model.loadingErrorsActions('setUserModelOnline I');
                // TODO: an error is shown when the transaction fail because a constraints error that shouldn't appear and must be analyzed
                // error shown: Table 'ad_user' save error: (6) could not execute statement due to a constaint failure (19 constraint failed)
                // QUICK FIX: trying again fixes the problem
                // me.setUserModelOnline(callback);
                return;
              }, true);
            } else {
              user = users.models[0];
              me.usermodel = user;
              if (me.password) {
                //The password will only be recomputed in case it was properly entered
                //(that is, if the call comes from the login page directly)
                savedPass = me.generate_sha1(me.password + user.get('created'));
                user.set('password', savedPass);
              }
              user.set('formatInfo', JSON.stringify(OB.Format));
              OB.Dal.save(user, function () {
                me.updateSession(user);
                if (callback) {
                  callback();
                }
              }, function () {
                OB.MobileApp.model.loadingErrorsActions('setUserModelOnline II');
                return;
              });
            }
          }, function () {
            OB.error("setUserModelOnline", arguments);
          });
        }, null);
      }, null);
    }, null);
  },

  /**
   * Set of app specific actions executed before loging out.
   *
   *  Override this method if your app needs to do something special
   */
  preLogoutActions: function () {

  },

  logout: function () {
    var me = this;

    this.preLogoutActions();

    var ajaxRequest = new enyo.Ajax({
      url: '../../org.openbravo.mobile.core.logout',
      cacheBust: false,
      method: 'GET',
      handleAs: 'json',
      timeout: 20000,
      contentType: 'application/json;charset=utf-8',
      success: function (inSender, inResponse) {
        me.closeSession();
      },
      fail: function (inSender, inResponse) {
        me.closeSession();
      }
    });
    ajaxRequest.go().response('success').error('fail');
  },

  lock: function () {
    var me = this;

    this.preLogoutActions();

    var ajaxRequest = new enyo.Ajax({
      url: '../../org.openbravo.mobile.core.logout',
      cacheBust: false,
      method: 'GET',
      handleAs: 'json',
      timeout: 20000,
      contentType: 'application/json;charset=utf-8',
      success: function (inSender, inResponse) {
        me.triggerLogout();
      },
      fail: function (inSender, inResponse) {
        me.triggerLogout();
      }
    });
    ajaxRequest.go().response('success').error('fail');
  },

  triggerLogout: function () {
    // deactivate the loginfail trigger
    this.off('loginfail');

    // trigger for custom applications
    this.trigger('logout');

    window.location.reload();
  },

  triggerLoginSuccess: function () {
    this.trigger('loginsuccess');
  },

  triggerOnLine: function () {
    if (!OB.MobileApp.model.loggingIn) {
      this.set('connectedToERP', true);
      if (this.pingId) {
        clearInterval(this.pingId);
        this.pingId = null;
      }
    }
  },

  triggerOffLine: function () {
    if (!OB.MobileApp.model.loggingIn) {
      this.set('connectedToERP', false);
      OB.UTIL.checkConnectivityStatus();
      if (!this.pingId) {
        this.pingId = setInterval(OB.UTIL.checkConnectivityStatus, 60000);
      }
    }
  },

  triggerLoginFail: function (e, mode, data) {
    OB.UTIL.showLoading(false);
    if (mode === 'userImgPress') {
      this.trigger('loginUserImgPressfail', e);
    } else {
      this.trigger('loginfail', e, data);
    }
  },

  hasPermission: function (p, checkForAutomaticRoles) {
    return (!checkForAutomaticRoles && !this.get('context').role.manual) || this.get('permissions')[p];
  },

  supportLogClient: function () {
    var supported = true;
    if ((OB.MobileApp && OB.MobileApp.model && OB.MobileApp.model.get('supportsOffline')) === false) {
      supported = false;
    }
    return supported;
  },

  saveTerminalInfo: function (callback) {
    OB.info('saving terminal info:', this.attributes);
    //In online mode, we save the terminal information in the local db
    this.usermodel.set('terminalinfo', JSON.stringify(this));
    OB.Dal.save(this.usermodel, function () {
      if (callback) {
        callback();
      }
    }, function () {
      OB.error("saveTerminalInfo", arguments);
    });
  }
});