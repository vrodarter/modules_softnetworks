package org.openbravo.mobile.warehouse.physicalinventory;

import java.math.BigDecimal;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.openbravo.base.weld.WeldUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.materialmgmt.InventoryCountProcess;
import org.openbravo.mobile.core.process.SimpleQueryBuilder;
import org.openbravo.mobile.warehouse.WarehouseJSONProcess;
import org.openbravo.model.materialmgmt.transaction.InventoryCount;
import org.openbravo.model.materialmgmt.transaction.InventoryCountLine;
import org.openbravo.service.db.DalConnectionProvider;

public class ProcessPhysicalInventory extends WarehouseJSONProcess {

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    // TODO Auto-generated method stub

    OBContext.setAdminMode(false);

    try {
      final String action = jsonsent.getString("action");

      String inventoryId = jsonsent.getString("inventoryId");
      JSONObject result = new JSONObject();
      JSONObject data = new JSONObject();
      if ("updateCounted".equals(action)) {
        String lineid = jsonsent.getString("lineid");
        String countQty = jsonsent.getString("countQty");
        updateCounted(lineid, countQty);
      } else if ("process".equals(action)) {
        InventoryCount inventory = OBDal.getInstance().get(InventoryCount.class, inventoryId);
        InventoryCountProcess inv = (InventoryCountProcess) WeldUtils
            .getInstanceFromStaticBeanManager(InventoryCountProcess.class);
        inv.processInventory(inventory);// new InventoryCountProcess().processInventory(inventory);
      } else if ("paginationScan".equals(action)) {
        String productid = jsonsent.getString("productid");
        PaginationScanData row = pageScan(inventoryId, productid);
        if (row == null) {
          result.put("status", 1);
          return result;
        } else {
          data.put("lineno", row.getField("num").toString());
          data.put("lineid", row.getField("lineid"));
          // data.put("productid", productid);
        }
      } else if ("resetLine".equals(action)) {
        resetLine(jsonsent);
      }

      OBDal.getInstance().flush();
      OBDal.getInstance().commitAndClose();

      result.put("status", 0);
      result.put("message", OBMessageUtils.messageBD("success"));
      data.put("inventoryId", inventoryId);
      result.put("data", data);

      return result;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void updateCounted(String lineid, String countQty) throws JSONException {
    InventoryCountLine physInventoryLine = OBDal.getInstance()
        .get(InventoryCountLine.class, lineid);
    physInventoryLine.setOBMWHPICounted(!physInventoryLine.isOBMWHPICounted());
    physInventoryLine.setQuantityCount(new BigDecimal(countQty));
    OBDal.getInstance().save(physInventoryLine);
  }

  private PaginationScanData pageScan(String inventoryId, String productid) throws ServletException {
    DalConnectionProvider cp = new DalConnectionProvider();
    PaginationScanData[] rows = PaginationScanData.getRow(cp, inventoryId, productid);
    if (rows.length == 0) {
      return null;
    } else {
      return rows[0];
    }
  }

  private void resetLine(JSONObject jsonsent) throws JSONException {
    final String lineId = jsonsent.getString("lineId");

    String hqlQuery = "UPDATE MaterialMgmtInventoryCountLine l "
        + "SET l.oBMWHPICounted = false, l.quantityCount = l.bookQuantity " + "WHERE l.id = '"
        + lineId + "'";

    SimpleQueryBuilder querybuilder = new SimpleQueryBuilder(hqlQuery, OBContext.getOBContext()
        .getCurrentClient().getId(), OBContext.getOBContext().getCurrentOrganization().getId(),
        null);

    final Session session = OBDal.getInstance().getSession();
    final Query query = session.createQuery(querybuilder.getHQLQuery());

    query.executeUpdate();
  }
}
