/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.mobile.warehouse.physicalinventory;

public class WHPhysicalInventoryConstants {
  public static final String COMPONENT_TYPE = "OBMWHPI_Main";
  public static final String MODULE_JAVAPACKAGE = "org.openbravo.mobile.warehouse.physicalinventory";
  public static final String MODULE_ID = "019C86A8D5D143E9897F74BDB615A174";

  public static final String PHYSICALINV_PROPERTY = "OBMWHPI_physicalinventory";
}
