/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.mobile.warehouse.physicalinventory;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.mobile.core.MobileCoreComponentProvider;
import org.openbravo.mobile.warehouse.WarehouseConstants;

@ApplicationScoped
@ComponentProvider.Qualifier(WHPhysicalInventoryConstants.COMPONENT_TYPE)
public class WarehousePhysicalInventoryComponentProvider extends MobileCoreComponentProvider {

  @Inject
  @Any
  private Instance<ComponentProvider> componentProviders;

  private static final String PREFIX = "web/" + WHPhysicalInventoryConstants.MODULE_JAVAPACKAGE;

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> resources = new ArrayList<ComponentResource>();

    final String[] deps = { "physicalinventory/pi-model.js", "physicalinventory/pi-view.js",
        "physicalinventory/pi-inventory.js", "physicalinventory/pi-line.js",
        "physicalinventory/pi-search.js", "physicalinventory/pi-process-modals.js", "pi-styles.css" };

    for (final String dep : deps) {
      if (dep.endsWith(".js")) {
        resources.add(createComponentResource(ComponentResourceType.Static, PREFIX + "/source/"
            + dep, WarehouseConstants.APP_IDENTIFIER));
      } else if (dep.endsWith(".css")) {
        resources.add(createComponentResource(ComponentResourceType.Stylesheet, PREFIX + "/assets/"
            + dep, WarehouseConstants.APP_IDENTIFIER));
      }
    }

    return resources;
  }

}
