package org.openbravo.mobile.warehouse.physicalinventory.eventhandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.materialmgmt.transaction.InventoryCount;

public class MobilePhysicalInventoryEventHandler extends EntityPersistenceEventObserver {
  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      InventoryCount.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    if (((User) event.getCurrentState(getProperty(InventoryCount.PROPERTY_OBMWHPIUSER))) != null) {
      event.setCurrentState(getProperty(InventoryCount.PROPERTY_OBMWHPISTATUS), "A");
    }
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    if (!isValidEvent(event)) {
      return;
    }

    boolean previouslyAssigned = ((User) event
        .getPreviousState(getProperty(InventoryCount.PROPERTY_OBMWHPIUSER))) != null;
    boolean currentAssigned = ((User) event
        .getCurrentState(getProperty(InventoryCount.PROPERTY_OBMWHPIUSER))) != null;
    String status = (String) event
        .getCurrentState(getProperty(InventoryCount.PROPERTY_OBMWHPISTATUS));
    if (!"P".equals(status) && !previouslyAssigned && currentAssigned) {
      event.setCurrentState(getProperty(InventoryCount.PROPERTY_OBMWHPISTATUS), "A");
    } else if (!"P".equals(status) && previouslyAssigned && !currentAssigned) {
      event.setCurrentState(getProperty(InventoryCount.PROPERTY_OBMWHPISTATUS), "D");
    }

    boolean previouslyProcessed = ((Boolean) event
        .getPreviousState(getProperty(InventoryCount.PROPERTY_PROCESSED)));
    boolean currentProcessed = ((Boolean) event
        .getCurrentState(getProperty(InventoryCount.PROPERTY_PROCESSED)));
    if (!previouslyProcessed && currentProcessed) {
      event.setCurrentState(getProperty(InventoryCount.PROPERTY_OBMWHPISTATUS), "P");
    }
  }

  private Property getProperty(String PROPERTY) {
    return entities[0].getProperty(PROPERTY);
  }
}
