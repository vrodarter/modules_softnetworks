package org.openbravo.mobile.warehouse.physicalinventory.hook;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.materialmgmt.hook.InventoryCountCheckHook;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.materialmgmt.transaction.InventoryCount;
import org.openbravo.model.materialmgmt.transaction.InventoryCountLine;
import org.openbravo.service.db.DalConnectionProvider;

@ApplicationScoped
public class InventoryCountCheckHookExtension implements InventoryCountCheckHook {

  @Override
  public void exec(InventoryCount inventory) throws Exception {
    User user = OBContext.getOBContext().getUser();

    if ("A".equals(inventory.getObmwhpiStatus())
        && (inventory.getOBMWHPIUser() == null || !user.getId().equals(
            inventory.getOBMWHPIUser().getId()))) {
      OBException obException = new OBException(Utility.messageBD(new DalConnectionProvider(false),
          "OBMWHPI_NotAssignedInv", OBContext.getOBContext().getLanguage().getLanguage()));
      throw obException;
    }

    DalConnectionProvider cp = new DalConnectionProvider(false);
    for (InventoryCountLine inventoryLine : inventory.getMaterialMgmtInventoryCountLineList()) {
      if (!inventoryLine.isOBMWHPICounted()) {
        InventoryLineData.updateCountQty(cp, inventoryLine.getId());
        // inventoryLine.setQuantityCount(inventoryLine.getBookQuantity());
        // OBDal.getInstance().save(inventoryLine);
      }
    }
  }
}
