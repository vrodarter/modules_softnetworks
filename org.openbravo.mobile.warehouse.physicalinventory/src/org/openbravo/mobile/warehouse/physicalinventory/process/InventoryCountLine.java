package org.openbravo.mobile.warehouse.physicalinventory.process;

import java.util.Arrays;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.mobile.warehouse.WarehouseProcessHQLQuery;

public class InventoryCountLine extends WarehouseProcessHQLQuery {

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    final String inventoryId = jsonsent.getString("inventoryId");
    final boolean showCounted;
    if (jsonsent.has("showCounted")) {
      showCounted = jsonsent.getBoolean("showCounted");
    } else {
      showCounted = true;
    }

    if (showCounted) {
      return Arrays
          .asList(new String[] { "SELECT il.id as id, il.product.id as product, il.product.name as productName, "
              + " loc.id as storageBin, loc.searchKey as storageBinName, att.id as attributeSetValue, "
              + " att.description as attributeSetValueName, il.uOM.name as uom, il.bookQuantity as bookQuantity, "
              + " il.quantityCount as quantityCount, il.oBMWHPICounted as oBMWHPICounted "
              + " FROM MaterialMgmtInventoryCountLine AS il "
              + "        left outer join il.storageBin AS loc"
              + "        left outer join il.product AS prod"
              + "        left outer join il.attributeSetValue AS att"
              + " WHERE il.physInventory.id = '"
              + inventoryId
              + "' ORDER BY loc.searchKey, prod.name,  att.description" });
    } else {
      return Arrays
          .asList(new String[] { "SELECT il.id as id, il.product.id as product, il.product.name as productName, "
              + " loc.id as storageBin, loc.searchKey as storageBinName, att.id as attributeSetValue, "
              + " att.description as attributeSetValueName, il.uOM.name as uom, il.bookQuantity as bookQuantity, "
              + " il.quantityCount as quantityCount, il.oBMWHPICounted as oBMWHPICounted "
              + " FROM MaterialMgmtInventoryCountLine AS il "
              + "        left outer join il.storageBin AS loc"
              + "        left outer join il.product AS prod"
              + "        left outer join il.attributeSetValue AS att"
              + " WHERE il.physInventory.id = '"
              + inventoryId
              + "' AND il.oBMWHPICounted = false"
              + " ORDER BY loc.searchKey, prod.name,  att.description" });
    }
  }
}
