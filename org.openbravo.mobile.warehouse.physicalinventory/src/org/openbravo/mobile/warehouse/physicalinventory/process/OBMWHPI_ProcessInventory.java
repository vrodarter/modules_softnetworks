package org.openbravo.mobile.warehouse.physicalinventory.process;

import org.openbravo.base.weld.WeldUtils;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.materialmgmt.InventoryCountProcess;
import org.openbravo.model.materialmgmt.transaction.InventoryCount;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

public class OBMWHPI_ProcessInventory implements Process {

  @Override
  public void execute(ProcessBundle bundle) throws Exception {
    OBError obError;

    final String recordID = (String) bundle.getParams().get("M_Inventory_ID");
    InventoryCount inventory = OBDal.getInstance().get(InventoryCount.class, recordID);
    InventoryCountProcess inv = (InventoryCountProcess) WeldUtils
        .getInstanceFromStaticBeanManager(InventoryCountProcess.class);
    obError = inv.processInventory(inventory);

    bundle.setResult(obError);
  }
}
