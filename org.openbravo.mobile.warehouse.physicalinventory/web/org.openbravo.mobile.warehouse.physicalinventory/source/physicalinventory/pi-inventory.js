/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global OBWH, enyo, Backbone, _*/

enyo.kind({
  name: 'OBWH.PhysicalInventory.LeftToolbar',
  kind: 'OB.UI.MultiColumn.Toolbar',
  showMenu: true,
  menuEntries: [{
    kind: 'OBWH.UI.MenuAction',
    i18nLabel: 'OBMOBC_LblBack',
    eventName: 'onBackMenuAction',
    name: 'properties'
  }],
  buttons: [{
    kind: 'OBWH.PhysicalInventory.ButtonSelect',
    name: 'btnSelect',
    span: 6
  }, {
    kind: 'OBWH.PhysicalInventory.ButtonDone',
    name: 'btnDone',
    span: 6
  }]
});

enyo.kind({
  name: 'OBWH.UI.MenuAction',
  kind: 'OB.UI.MenuAction',

  initComponents: function () {
    this.sourceTap = this.tap;
    this.inherited(arguments);

    this.tap = function () {
      var currentLine = this.model.get('inventory').get('windowModel').get('currentLine');

      if (currentLine && currentLine.timeout) {
        clearTimeout(currentLine.timeout);
        if (!currentLine.get('isCounted')) {
          currentLine.toggleCounted();
        }
      }
      this.sourceTap();
    };
  },
  init: function (model) {
    var me = this;

    me.model = model;
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.ButtonSelect',
  kind: 'OB.UI.ToolbarButton',
  icon: 'btn-icon btn-icon-new',

  events: {
    onShowPopup: ''
  },
  tap: function () {
    var currentLine = this.model.get('inventory').get('windowModel').get('currentLine');

    if (currentLine && currentLine.timeout) {
      clearTimeout(currentLine.timeout);
      if (!currentLine.get('isCounted')) {
        currentLine.toggleCounted();
      }
    }
    this.doShowPopup({
      popup: 'modalPhysicalInventorySearch'
    });
  },
  init: function (model) {
    var me = this;

    me.model = model;
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.ButtonDone',
  kind: 'OB.UI.ToolbarButton',
  events: {
    onProcessInventory: '',
    onShowPopup: ''
  },
  handlers: {
    onDisableButton: 'disableButton',
    onEnableButton: 'enableButton'
  },

  disableButton: function (inSender, inEvent) {
    this.setDisabled(inEvent.buttons.indexOf('done') !== -1);
    return true;
  },
  enableButton: function () {
    this.setDisabled(false);
  },
  tap: function () {
    var currentLine = this.model.get('inventory').get('windowModel').get('currentLine');

    if (currentLine && currentLine.timeout) {
      clearTimeout(currentLine.timeout);
      if (!currentLine.get('isCounted')) {
        currentLine.toggleCounted();
      }
    }

    if (this.disabled) {
      return;
    } else {
      this.setDisabled(true);
    }
    if (this.model.get('inventory').get('totalLines') !== this.model.get('inventory').get('doneLines')) {
      this.doShowPopup({
        popup: 'modalSureToProcess'
      });
      this.setDisabled(false);
    } else {
      this.doProcessInventory();
    }
  },

  init: function (model) {
    var me = this,
        inventory = model.get('inventory');

    me.model = model;
    inventory.on('change:ready', function (model) {
      //enable button
      me.setDisabled(!model.get('ready'));
    });
  },

  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBWH_BtnDone'));
    this.setDisabled(true);
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.InventoryView',
  handlers: {
    onLoadTotalPage: 'loadTotalPage'
  },
  components: [{
    style: 'overflow:auto; margin: 1px 5px 5px 5px; position: relative; background-color: #ffffff; color: black; padding: 5px;',
    classes: 'row-fluid inventoryViewiPadFix',
    components: [{
      name: 'inventory',
      showing: false,
      components: [{
        kind: 'OBWH.PhysicalInventory.Header',
        style: 'position: relative; height: 40px;',
        name: 'header'
      }, {
        kind: 'OBWH.PhysicalInventory.PageSelector',
        style: 'position: relative; height: 40px;',
        name: 'pageSelector'
      }, {
        kind: 'OBWH.PhysicalInventory.Details',
        name: 'details'
      }]
    }, {
      // Default kind to show when window is opened and no picking is still selected.
      name: 'noInventory',
      kind: 'OBWH.PhysicalInventory.BaseInventoryList'
    }]
  }],
  loadTotalPage: function () {
    this.waterfall('totalPageLoaded');
  },
  init: function (model) {
    var me = this;
    model.get('inventory').on('change:inventory.id', function () {
      me.$.noInventory.hide();
      me.$.inventory.show();
    });
    model.on('change:activeTab', function () {
      if (model.get('activeTab') === 'scan') {
        me.waterfall('onLineToScanMode');
      } else {
        me.waterfall('onLineToEditMode');
      }
    });
    model.on('change:currentLine', function () {
      me.waterfall('onUnselectGroups');
      model.set('unselectLocked', false);
    });
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.BaseInventoryList',
  events: {
    onSelectInventory: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'invList',
          kind: 'OB.UI.ScrollableTable',
          scrollAreaMaxHeight: '600px',
          renderLine: 'OBWH.PhysicalInventory.InvRecord',
          renderEmpty: 'OB.UI.RenderEmpty'
        }]
      }]
    }]
  }],

  init: function () {
    this.invList = new Backbone.Collection();
    this.$.invList.setCollection(this.invList);
    this.invList.on('click', function (model) {
      this.doSelectInventory({
        inventory: model
      });
    }, this);

    OB.Dal.find(OB.Model.MaterialMgmtInventoryCount, OBWH.PhysicalInventory.Model.getInventoryWhereClause(), enyo.bind(this, function (invs) {
      if (invs && invs.length > 0) {
        this.invList.reset(invs.models);
      } else {
        this.invList.reset();
      }
    }));
  }
});
enyo.kind({
  name: 'OBWH.PhysicalInventory.Header',
  kind: 'OB.UI.ScrollableTableHeader',

  components: [{
    name: 'line',
    style: 'line-height: 40px; font-size: 20px; text-align: center; width: 50%; position: absolute; left: 0;'
  }, {
    name: 'warehouse',
    style: 'line-height: 40px; font-size: 20px; text-align: center; background-color: #CCCCCC; width: 50%; position: absolute; right: 0; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;'
  }],
  init: function (model) {
    var me = this,
        inventory = model.get('inventory');
    inventory.on('change:inventory.id', function () {
      //me.$.line.setContent(inventory.get('inventory').get(OB.Constants.IDENTIFIER));
      me.$.line.setContent(inventory.get('inventory').get('_identifier'));
      //me.$.warehouse.setContent(inventory.get('inventory').get('warehouse$' + OB.Constants.IDENTIFIER));
      me.$.warehouse.setContent(inventory.get('inventory').get('warehouse$' + '_identifier'));
    });
  }

});

enyo.kind({
  name: 'OBWH.PhysicalInventory.PageSelector',
  kind: 'OB.UI.ScrollableTableHeader',
  handlers: {
    totalPageLoaded: 'totalLoaded'
  },
  components: [{
    kind: 'OBWH.PhysicalInventory.PageBefore',
    name: 'pageBefore'
  }, {
    kind: 'OBWH.PhysicalInventory.PaginationContainer',
    name: 'paginationContainer'
  }, {
    kind: 'OBWH.PhysicalInventory.PageAfter',
    name: 'pageAfter'
  }],
  totalLoaded: function () {
    this.model.set('currentLine', null);

    this.$.paginationContainer.$.pageLabel.setContent(OB.I18N.getLabel('OBMWHPI_PageAdviser'));
    //this.$.pageNumber.setContent(OB.I18N.getLabel('OBMWHPI_PageAdviser', [this.model.get('inventory').get('currentPage'), this.model.get('inventory').get('totalPage')]));
    this.$.paginationContainer.$.pageTotal.setContent('/' + this.model.get('inventory').get('totalPage'));
    this.$.paginationContainer.$.pageNumber.setValue(this.model.get('inventory').get('currentPage'));

    // If this is the first page, disable pageBefore button  
    if (this.model.get('inventory').get('currentPage') === 1) {
      this.$.pageBefore.setDisabled(true);
    } else {
      this.$.pageBefore.setDisabled(false);
    }

    // If there are more than one page, enable pageAfter button  
    if (this.model.get('inventory').get('totalPage') !== 1 && this.model.get('inventory').get('totalPage') !== this.model.get('inventory').get('currentPage')) {
      this.$.pageAfter.setDisabled(false);
    } else {
      this.$.pageAfter.setDisabled(true);
    }
  },
  init: function (model) {
    var me = this,
        inventory = model.get('inventory');
    this.model = model;
    inventory.on('change:inventory.id', function () {

      // Disable buttons on load
      me.$.pageBefore.setDisabled(true);
      me.$.pageAfter.setDisabled(true);
    });
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.PaginationContainer',
  style: 'display: inline-block; width:50%!important; line-height: 40px; ',
  components: [{
    name: 'pageLabel',
    style: 'line-height: 40px; font-size: 20px; text-align: right; width: 40%!important; display: inline-block; position:relative; '
  }, {
    style: 'line-height: 30px; font-size: 20px; text-align: right; width: 20%!important; display: inline-block; position:relative; padding:0px!important; ',
    kind: 'OB.UI.SearchInput',
    name: 'pageNumber',
    handlers: {
      onchange: 'inputChanged',
      onblur: 'blur'
    },
    inputChanged: function () {
      var pageSelector = this.parent.parent,
          inventory = this.parent.parent.model.get('inventory'),
          inputValue = Number(this.getValue()),
          callback;

      if (isNaN(inputValue)) {
        OB.UTIL.showError(OB.I18N.getLabel('OBMWHPI_NotANumber'));
        this.setValue(inventory.get('currentPage'));
        return;
      } else if (inputValue > Number(inventory.get('totalPage'))) {
        OB.UTIL.showError(OB.I18N.getLabel('OBMWHPI_InputGreaterTotalPages'));
        this.setValue(inventory.get('currentPage'));
        return;
      }

      pageSelector.$.pageBefore.setDisabled(true);
      pageSelector.$.pageAfter.setDisabled(true);

      inventory.set('currentPage', Number(inputValue));

      callback = function () {
        if (inventory.get('currentPage') !== 1) {
          pageSelector.$.pageBefore.setDisabled(false);
        }
        if (inventory.get('currentPage') !== inventory.get('totalPage')) {
          pageSelector.$.pageAfter.setDisabled(false);
        }
      };
      this.parent.parent.model.get('inventory').loadLines();
      OB.MobileApp.view.scanningFocus(true);
    },
    tap: function () {
      OB.MobileApp.view.scanningFocus(false);
    },
    blur: function () {
      OB.MobileApp.view.scanningFocus(true);
    }
  }, {
    name: 'pageTotal',
    style: 'line-height: 40px; font-size: 20px; text-align: left; width: 39%!important; display: inline-block; position:relative; '
  }]
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.PageBefore',
  kind: 'OB.UI.Button',
  classes: 'enyo-tool-decorator btnlink btnlink-small btnlink-orange pageSelector',
  events: {
    onChangePage: ''
  },
  tap: function () {
    var callback, me = this,
        currentLine = this.model.get('inventory').get('windowModel').get('currentLine');

    if (currentLine && currentLine.timeout) {
      clearTimeout(currentLine.timeout);
      if (!currentLine.get('isCounted')) {
        currentLine.toggleCounted();
      }
    }

    // Fix for mobile
    if (this.disabled) {
      this.owner.$.pageAfter.focus();
      return;
    }

    this.focus();
    this.setDisabled(true);
    this.owner.$.pageAfter.setDisabled(true);
    callback = function () {
      if (me.model.get('inventory').get('currentPage') !== 1) {
        me.setDisabled(false);
      }
      me.owner.$.pageAfter.setDisabled(false);
    };

    // Set the new current page and print the number on the label
    this.model.get('inventory').set('currentPage', this.model.get('inventory').get('currentPage') - 1);
    this.owner.$.paginationContainer.$.pageNumber.setContent(this.model.get('inventory').get('currentPage'));

    this.model.get('inventory').loadLines(callback);
    this.doChangePage();
  },
  init: function (model) {
    this.model = model;
    this.setContent('<');
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.PageAfter',
  kind: 'OB.UI.Button',
  classes: 'enyo-tool-decorator btnlink btnlink-small btnlink-orange pageSelector positionFixIPad',
  events: {
    onChangePage: ''
  },
  tap: function () {
    var callback, me = this,
        currentLine = this.model.get('inventory').get('windowModel').get('currentLine');

    if (currentLine && currentLine.timeout) {
      clearTimeout(currentLine.timeout);
      if (!currentLine.get('isCounted')) {
        currentLine.toggleCounted();
      }
    }

    // Fix for mobile
    if (this.disabled) {
      this.owner.$.pageBefore.focus();
      return;
    }

    this.focus();
    this.setDisabled(true);
    this.owner.$.pageBefore.setDisabled(true);
    callback = function () {
      if (me.model.get('inventory').get('currentPage') !== me.model.get('inventory').get('totalPage')) {
        me.setDisabled(false);
      }
      me.owner.$.pageBefore.setDisabled(false);
    };

    // Set the new current page and print the number on the label
    this.model.get('inventory').set('currentPage', this.model.get('inventory').get('currentPage') + 1);
    this.owner.$.paginationContainer.$.pageNumber.setContent(this.model.get('inventory').get('currentPage'));

    this.model.get('inventory').loadLines(callback);
    this.doChangePage();

  },
  init: function (model) {
    this.model = model;
    this.setContent('>');
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.Details',
  kind: 'OB.UI.ScrollableTable',
  scrollAreaMaxHeight: '522px',
  renderLine: 'OBWH.PhysicalInventory.Locator',
  renderEmpty: 'OB.UI.RenderEmpty',
  listStyle: 'edit',
  handlers: {
    onUnselectFocus: 'unselectFocus'
  },
  init: function (model) {
    var locators = model.get('inventory').get('locators');
    this.setCollection(locators);
    this.model = model;
  },
  unselectFocus: function () {
    this.waterfall('onUnselect');
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.Locator',
  components: [{
    name: 'locatorHeader',
    kind: 'OBWH.PhysicalInventory.LocatorHeader',
    style: 'font-size: 24px; line-height: 32px;'
  }, {
    name: 'lines',
    kind: 'OBWH.PhysicalInventory.Lines'
  }],
  events: {
    onLoadTotalPage: ''
  },
  handlers: {
    onUnselectGroups: 'unselectGroups',
    onNewPage: 'newPage'
  },
  initComponents: function () {
    this.inherited(arguments);

    // Set page number label
    this.doLoadTotalPage();
    this.newPage();
    OB.MobileApp.view.scanningFocus(true);
  },
  unselectGroups: function () {
    this.waterfall('onUnselectNonCurrentLine');
    this.model.get('inventory').get('windowModel').set('unselectLocked', false);
  },
  newPage: function () {
    this.$.locatorHeader.$.header.setContent(this.model.get('name'));
    this.$.lines.setCollection(this.model.get('lines'));
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.LocatorHeader',
  kind: 'OB.UI.ScrollableTableHeader',
  components: [{
    style: ' display: inline-block; width: 50.5%;',
    name: 'header'
  }, {
    name: 'tQty',
    style: 'display: inline-block; width: 15%; font-size: 20px; position: relative; left: -5px; line-height: 32px; text-align: right;'
  }, {
    name: 'cQty',
    style: 'display: inline-block; width: 15%; font-size: 20px; position: relative; left: -5px; line-height: 32px; text-align: right;'
  }],
  initComponents: function () {
    this.inherited(arguments);
    if (OB.MobileApp.model.get('permissions').OBMWHPI_ShowBookedQty) {
      this.$.tQty.setContent('T');
    }
    this.$.cQty.setContent('C');
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.Lines',
  kind: 'OB.UI.ScrollableTable',
  scrollAreaMaxHeight: '40000px',
  renderLine: 'OBWH.PhysicalInventory.Line',
  renderEmpty: 'OB.UI.RenderEmpty',
  listStyle: 'edit'
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.Line',
  kind: 'OB.UI.SelectButton',
  classes: 'btnselect-orderline',
  style: 'padding: 1% !important;',
  handlers: {
    onUnselect: 'unselect',
    onLineToScanMode: 'lineToScanMode',
    onLineToEditMode: 'lineToEditMode',
    onUnselectNonCurrentLine: 'unselectNonCurrentLine'
  },
  components: [{
    style: 'float: left; width: 50%;',
    components: [{
      name: 'product',
      style: 'font-size: 16px; line-height: 32px;'
    }, {
      name: 'attribute'
    }]
  }, {
    style: 'display: inline-block; width: 15%; font-size: 20px; line-height: 32px; text-align: right;',
    name: 'bookQty'
  }, {
    style: 'display: inline-block; width: 15%; font-size: 20px; line-height: 32px; text-align: right;',
    name: 'countQty',
    tap: function () {
      this.parent.model.trigger('selected', this.parent.model);
      this.parent.model.setCountQty(Number(1), true);
    }
  }, {
    style: 'float: right; width: 3%;'
  }, {
    style: 'float: right; width: 7%;',
    kind: 'OBWH.PhysicalInventory.CheckLine',
    name: 'countedFlag'
  }],

  initComponents: function () {
    var me = this,
        line = this.model;
    this.inherited(arguments);
    this.$.product.setContent(line.get('productName'));
    this.$.attribute.setContent(line.get('attributeSetValueName'));
    if (OB.MobileApp.model.get('permissions').OBMWHPI_ShowBookedQty) {
      this.$.bookQty.setContent(line.get('bookQty'));
    }
    this.$.countQty.setContent(line.get('countQty'));
    if (line.get('inventory').get('windowModel').get('activeTab') === 'edit') {
      me.addClass('btnselect-orderline-edit');
    }
    line.on('selected', function () {
      me.bubble('onUnselectFocus');
      me.addClass('btnselect-orderline-edit');
    });
    if (line.get('inventory').get('windowModel').get('currentLine') !== line) {
      me.model.trigger('unselected', line);
    }
  },

  lineToScanMode: function () {
    if (this.model === this.model.get('inventory').get('windowModel').get('currentLine')) {
      this.removeClass('btnselect-orderline-edit');
    }
  },

  lineToEditMode: function () {
    if (this.model === this.model.get('inventory').get('windowModel').get('currentLine')) {
      this.addClass('btnselect-orderline-edit');
    }
  },

  unselectNonCurrentLine: function () {
    if (!this.model.get('inventory').get('windowModel').get('unselectLocked')) {
      this.model.trigger('unselected', this.model);
      this.model.get('inventory').get('windowModel').set('unselectLocked', true);
    }
    if (this.model === this.model.get('inventory').get('windowModel').get('currentLine')) {
      this.model.trigger('selected', this.model);
    }
  }

});

enyo.kind({
  name: 'OBWH.PhysicalInventory.CheckLine',
  //kind: 'OB.UI.CheckboxButton',
  tag: 'div',

  tap: function () {},

  initComponents: function () {
    var status = this.parent.model.get('isCounted');
    this.inherited(arguments);
    if (status) {
      this.setClassAttribute('obwh-btn-check active');
    } else {
      this.setClassAttribute('obwh-btn-check');
    }
  }
});