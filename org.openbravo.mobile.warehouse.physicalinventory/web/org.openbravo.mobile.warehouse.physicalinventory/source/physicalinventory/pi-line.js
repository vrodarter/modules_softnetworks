/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global OBWH, enyo, Backbone, _*/

enyo.kind({
  name: 'OBWH.PhysicalInventory.RightToolbar',
  kind: 'OB.UI.MultiColumn.Toolbar',

  showMenu: false,

  buttons: [{
    kind: 'OBWH.PhysicalInventory.ToolbarButton',
    name: 'scan',
    span: '6'
  }, {
    kind: 'OBWH.PhysicalInventory.ToolbarButton',
    name: 'edit',
    span: '6'
  }]
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.ToolbarButton',
  kind: 'OB.UI.ToolbarButtonTab',

  events: {
    onActivateTab: ''
  },
  tap: function () {
    this.doActivateTab({
      tab: this.name
    });
  },
  setTabActive: function (tab) {
    this.parent.parent.addRemoveClass('active', this.name === tab);
    this.setDisabled(tab === this.name);
  },
  initComponents: function () {
    this.inherited(arguments);
    if (this.name === 'scan') {
      this.setContent(OB.I18N.getLabel('OBWH_BtnScan'));
    } else {
      this.setContent(OB.I18N.getLabel('OBWH_BtnEdit'));
    }
  },
  init: function (model) {
    var me = this;
    this.setTabActive(model.get('activeTab'));
    model.on('change:activeTab', function () {
      me.setTabActive(model.get('activeTab'));
    });
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.LineView',
  components: [{
    name: 'base',
    style: 'position: relative; display: block; background-color: #7da7d9; background-size: cover; color: white; height: 200px; margin: 5px; padding: 5px',

    components: [{
      classes: 'span8',
      style: 'position: absolute; top: 20px; left: 15px; font-size: 32px;',
      name: 'product'
    }, {
      classes: 'span8',
      style: 'position: absolute; bottom: 20px; left: 15px; font-size: 32px;',
      name: 'bookQty'
    }, {
      style: 'position: absolute; top: 20px; right: 15px; font-size: 42px; line-height: 42px; color: black;',
      name: 'countQty'
    }, {
      style: 'position: absolute; bottom: 0; right: 0; margin: 5px; padding: 5px;',
      kind: 'OB.UI.Clock'
    }]
  }, {
    kind: 'OBWH.PhysicalInventory.Keyboard',
    name: 'mykeyboard'
  }],
  setTabActive: function (scanMode) {
    this.$.mykeyboard.show();
    this.$.mykeyboard.isEnabled = true;
    this.$.mykeyboard.defaultcommand = 'code';
  },
  init: function (model) {
    var me = this,
        currentLine = model.get('currentLine');
    me.model = model;
    model.on('change:currentLine', function (model) {
      var currentLine = model.get('currentLine');
      if (currentLine) {
        me.$.product.setContent(currentLine.get('productName'));
        if (OB.MobileApp.model.get('permissions').OBMWHPI_ShowBookedQty) {
          me.$.bookQty.setContent(currentLine.get('bookQty') + ' ' + currentLine.get('uom'));
        }
        me.$.countQty.setContent(currentLine.get('countQty'));
        currentLine.on('change:countQty', function (currentLine) {
          me.$.countQty.setContent(currentLine.get('countQty'));
        });
      } else {
        me.$.product.setContent();
        if (OB.MobileApp.model.get('permissions').OBMWHPI_ShowBookedQty) {
          me.$.bookQty.setContent();
        }
        me.$.countQty.setContent();
      }
    });
    me.setTabActive(model.get('activeTab') === 'scan');
    model.on('change:activeTab', function () {
      me.setTabActive(model.get('activeTab') === 'scan');
    });

    if (!currentLine) {
      return;
    }

    this.$.product.setContent(currentLine.get('productName'));
    this.$.quantity.setContent(currentLine.get('neededQuantity'));
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.Keyboard',
  kind: 'OB.UI.Keyboard',
  sideBarEnabled: true,
  showCounted: true,
  events: {
    onSetQuantityCount: '',
    onShowHideCounted: '',
    onResetQuantity: '',
    onScan: ''
  },
  keyMatcher: /^([0-9]|\.|,| |%|[a-z]|[A-Z])$/,

  buttonsDef: {
    sideBar: {
      qtyI18nLbl: '',
      priceI18nLbl: '',
      discountI18nLbl: ''
    }
  },

  initComponents: function () {
    var toolbar, me = this;

    this.addCommand('add', {
      action: function (keyboard, qty) {
        if (me.showing) {
          var value = OB.I18N.parseNumber(qty);
          if (value || value === 0) {
            me.doSetQuantityCount({
              quantity: value,
              incremental: false
            });
          }
        }
      }
    });

    this.addCommand('line:qty', {
      action: function (keyboard, qty) {
        var value = OB.I18N.parseNumber(qty);
        if (value || value === 0) {
          me.doSetQuantityCount({
            quantity: value
          });
        }
      }
    });

    this.addCommand('-', {
      stateless: true,
      action: function (keyboard, qty) {
        //remove
        if (me.showing) {
          var value = OB.I18N.parseNumber(qty);
          if (!value) {
            value = Number(1);
          }
          value = value * -1;
          me.doSetQuantityCount({
            quantity: value,
            incremental: true
          });
        }
      }
    });

    this.addCommand('+', {
      stateless: true,
      action: function (keyboard, qty) {
        //remove
        if (me.showing) {
          var value = OB.I18N.parseNumber(qty);
          if (!value) {
            value = Number(1);
          }
          me.doSetQuantityCount({
            quantity: value,
            incremental: true
          });
        }
      }
    });

    this.addCommand('code', new OBWH.PhysicalInventory.BarcodeHandler({
      keyboard: this
    }));

    // Add the Show / Hide button command
    this.addCommand('showHide', {
      stateless: true,
      action: function () {
        if (me.owner.model.get('inventory').get('showCounted')) {
          me.buttons.showHide.setContent(OB.I18N.getLabel('OBMWHPI_KbShowCounted'));
        } else {
          me.buttons.showHide.setContent(OB.I18N.getLabel('OBMWHPI_KbHideCounted'));
        }
        me.owner.model.get('inventory').set('showCounted', !me.owner.model.get('inventory').get('showCounted'));
        me.doShowHideCounted();
      }
    });

    // Add the Reset Quantity command
    this.addCommand('resetQuantity', {
      stateless: true,
      action: function () {
        me.doResetQuantity();
      }
    });

    // calling super after setting keyboard properties
    this.inherited(arguments);

    this.addKeypad('OBWH.PhysicalInventory.MyKeypad');

    toolbar = {
      name: 'toolbarscan',
      buttons: [{
        command: 'code',
        label: OB.I18N.getLabel('OBMOBC_KbCode'),
        classButtonActive: 'btnactive-blue'
      }, {
        name: 'showHide',
        command: 'showHide',
        label: OB.I18N.getLabel('OBMWHPI_KbHideCounted'),
        classButtonActive: 'btnactive-blue'
      }, {
        name: 'resetQuantity',
        command: 'resetQuantity',
        label: OB.I18N.getLabel('OBMWHPI_KbResetQuantity'),
        classButtonActive: 'btnactive-blue'
      }],
      shown: function () {
        var keyboard = this.owner.owner;
        keyboard.showKeypad('obmwhp-keypad');
        keyboard.showSidepad('sideenabled');
        keyboard.defaultcommand = 'code';
      }
    };

    this.$.btnQty.$.button.setContent(OB.I18N.getLabel('OBMOBC_KbQuantity'));

    this.addToolbar(toolbar);
    this.showToolbar('toolbarscan');
    this.showKeypad('obmwhp-keypad');

    this.defaultcommand = 'code';
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.BarcodeHandler',
  kind: 'OB.UI.AbstractBarcodeActionHandler',
  events: {
    onScan: ''
  },
  findProductByBarcode: function (code, callback, keyboard) {
    keyboard.doScan({
      code: code
    });
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.MyKeypad',
  kind: 'OB.UI.KeypadBasic',
  padName: 'obmwhp-keypad',
  toolbarButtons: [{
    label: ' ',
    command: ''
  }, {
    label: ' ',
    command: ''
  }, {
    label: ' ',
    command: 'Add:'
  }],
  initComponents: function () {
    this.inherited(arguments);
    //    this.$.toolbarBtn1.disableButton(this.$.toolbarBtn1, {
    //      disabled: true
    //    });
  }
});