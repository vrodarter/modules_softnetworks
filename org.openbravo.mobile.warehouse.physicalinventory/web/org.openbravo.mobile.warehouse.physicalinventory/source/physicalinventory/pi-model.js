/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global OBWH, enyo, Backbone, _*/

OBWH.PhysicalInventory = OBWH.PhysicalInventory || {};

OBWH.PhysicalInventory.Model = OB.Model.WindowModel.extend({
  models: [],

  updateCountedFlag: function (line) {
    //TODO call the handler to update the counted flag in the backend
    var proc = new OB.DS.Process(OBWH.PhysicalInventory.Model.processPhysicalInventoryClass),
        lineid, countQty;
    lineid = line.attributes.id;
    countQty = line.attributes.countQty;

    proc.exec({
      action: 'updateCounted',
      lineid: lineid,
      countQty: countQty,
      inventoryId: this.get('inventory').get('inventory.id')
    }, enyo.bind(this, function (response, message, inventoryId) {
      var criteria = {};
      if (response && response.exception) {
        OB.UTIL.showError(response.exception.message);
      } else {
        OB.UTIL.showSuccess('success');
      }
      OB.UTIL.showLoading(false);

      criteria._where = 'e.physInventory.id = \'' + response.inventoryId + '\' and e.oBMWHPICounted <> \'Y\'';
      OB.Dal.find(OB.Model.MaterialMgmtInventoryCountLine, criteria, enyo.bind(this, function (values) {
        if (values.length !== 0) {
          this.get('inventory').set('allCounted', false);
        } else {
          this.get('inventory').set('allCounted', true);
        }
      }));

    }), function () {
      OB.UTIL.showLoading(false);
    });
  },

  processInventory: function (inventory) {
    var proc = new OB.DS.Process(OBWH.PhysicalInventory.Model.processPhysicalInventoryClass),
        inventoryId;

    inventoryId = inventory.get('inventory.id');

    proc.exec({
      action: 'process',
      inventoryId: inventoryId
    }, function (response, message) {
      if (response && response.exception) {
        OB.UTIL.showError(response.exception.message);
        this.app.waterfall('onEnableButton');
      } else {
        OB.UTIL.showSuccess('success');
      }
      OB.UTIL.showLoading(false);
    }, function () {
      this.app.waterfall('onEnableButton');
      OB.UTIL.showLoading(false);
    });
  },

  scan: function (code) {
    var me = this,
        inventory = this.get('inventory');
    var proc = new OB.DS.Process(OBWH.Movement.Model.scanHandlerClass);
    proc.exec({
      code: code,
      line: {},
      eventName: OBWH.Movement.Model.scanEvent
    }, enyo.bind(this, function (response, message) {
      var lines, i;
      if (response.exception) {
        OB.UTIL.showError(response.exception.message);
        return;
      }

      if (response.product) {

        // If it is not on this page go to its page
        var paginationScan = new OB.DS.Process(OBWH.PhysicalInventory.Model.processPhysicalInventoryClass);
        paginationScan.exec({
          inventoryId: inventory.get('inventory.id'),
          productid: response.product.id,
          action: 'paginationScan'
        }, enyo.bind(this, function (response, message) {
          var row, limit, callback, lineid;
          if (response.exception) {
            OB.UTIL.showError(OB.I18N.getLabel('OBMWHPI_NoItemFound'));
            return;
          }

          if (response.lineno) {
            row = response.lineno;
            lineid = response.lineid;
            limit = OBWH.PhysicalInventory.Model.limit();
            callback = enyo.bind(this, function () {
              var lines, currentLine = null;
              lines = inventory.get('lines');

              for (i = 0; i < lines.models.length; i++) {
                var line = lines.models[i];
                if (line.get('id') === lineid) {

                  me.set('currentLine', line);

                  line.setCountQty(Number(1), true);
                  OB.UTIL.showSuccess(OB.I18N.getLabel('OBWH_SacannedProduct', [line.get('productName')]));
                  break;
                }
              }
            });
            if (Math.ceil(row / limit) !== this.get('inventory').get('currentPage')) {
              this.get('inventory').set('currentPage', Math.ceil(row / limit));
              this.get('inventory').loadLines(callback);
            } else {
              callback();
            }
          }
        }), function () {
          window.console.error('error');
        });
      }
    }), function () {
      window.console.error('error');
    });
  },

  init: function () {
    var inventory, lines, locators;
    OB.Data.Registry.registerModel('MaterialMgmtInventoryCount');
    OB.Data.Registry.registerModel('MaterialMgmtInventoryCountLine');

    lines = new OBWH.PhysicalInventory.Model.Lines();
    locators = new OBWH.PhysicalInventory.Model.Locators();
    inventory = new OBWH.PhysicalInventory.Model.Inventory({
      windowModel: this,
      lines: lines,
      locators: locators
    });
    this.set('inventory', inventory);
    this.set('activeTab', 'scan');
    if (OB.MobileApp.model.get('permissions').OBMWHPI_BttnDone_CheckLinesNeeded && !this.get('allCounted')) {
      this.set('ready', false);
    } else {
      this.set('ready', true);
    }

    lines.on('selected', function (line) {
      if (this.get('currentLine') && this.get('currentLine').timeout && this.get('currentLine') !== line) {
        clearTimeout(this.get('currentLine').timeout);
        if (!this.get('currentLine').get('isCounted')) {
          this.get('currentLine').toggleCounted();
        }
      }
      this.set('prevCurrentLine', this.get('currentLine'));
      this.set('currentLine', line);
      this.set('activeTab', 'edit');
    }, this);
    lines.on('change:isCounted', function (line) {
      this.updateCountedFlag(line);
      inventory.set('doneLines', (line.get('isCounted')) ? inventory.get('doneLines') + 1 : inventory.get('doneLines') - 1);
    }, this);
    inventory.on('change:allCounted', function () {
      if (OB.MobileApp.model.get('permissions').OBMWHPI_BttnDone_CheckLinesNeeded && !this.get('allCounted')) {
        this.set('ready', false);
      } else {
        this.set('ready', true);
      }
    }, inventory);
    inventory.on('change:totalLines', function () {
      // Needed for the load of the lines.
      if (OB.MobileApp.model.get('permissions').OBMWHPI_BttnDone_CheckLinesNeeded && !this.get('allCounted')) {
        this.set('ready', false);
      } else {
        this.set('ready', true);
      }
    }, inventory);
    inventory.on('change:currentPage', function () {
      locators.reset();
    }, inventory);
    this.set('unselectLocked', false);
  }
});

OBWH.PhysicalInventory.Model.limit = function () {
  var limit = OB.MobileApp.model.get('permissions').OBMWHPI_NumberOfRecords;

  if (limit && !isNaN(limit) && parseInt(limit, 10) < 100) {
    limit = parseInt(limit, 10);
  } else {
    // Default value
    limit = 100;
  }
  return limit;
};

OBWH.PhysicalInventory.Model.Inventory = Backbone.Model.extend({
  reset: function (dalinventory) {
    var me = this,
        proc, limit = OBWH.PhysicalInventory.Model.limit(),
        criteria = {};

    dalinventory = dalinventory || me.get('inventory');
    me.set('inventory', dalinventory);
    me.set('inventory.id', dalinventory.get('id'));
    me.set('inventory.name', dalinventory.get('name'));
    if (OB.MobileApp.model.get('permissions').OBMWHPI_BttnDone_CheckLinesNeeded && !this.get('allCounted')) {
      me.set('ready', false);
    } else {
      me.set('ready', true);
    }
    me.get('lines').reset();
    me.set('totalLines', Number(0));
    me.set('doneLines', Number(0));
    me.set('currentPage', Number(1));

    proc = new OB.DS.Process(OBWH.PhysicalInventory.Model.processPhysicalInventoryLineNumber);

    if (typeof this.get('showCounted') === "undefined") {
      this.set('showCounted', true);
    }

    proc.exec({
      inventoryId: me.get('inventory.id'),
      showCounted: this.get('showCounted')
    }, enyo.bind(me, function (response, message) {
      enyo.forEach(response, function (model) {
        me.set('totalPage', Math.ceil(model[0] / Number(limit)));
      });
    }), function () {
      window.console.error('error');
    });

    criteria._where = 'e.physInventory.id = \'' + this.get('inventory.id') + '\' and e.oBMWHPICounted <> \'Y\'';
    OB.Dal.find(OB.Model.MaterialMgmtInventoryCountLine, criteria, enyo.bind(this, function (values) {
      if (values.length !== 0) {
        this.set('allCounted', false);
      } else {
        this.set('allCounted', true);
      }
    }));

    me.loadLines();
  },
  loadLines: function (callback) {
    var me = this,
        lines = this.get('lines'),
        criteria = {},
        proc, offset, limit = OBWH.PhysicalInventory.Model.limit();
    lines.reset();

    proc = new OB.DS.Process(OBWH.PhysicalInventory.Model.processPhysicalInventoryLine);

    offset = (Number(me.get('currentPage')) - Number(1)) * Number(limit).toString();

    if (typeof this.get('showCounted') === "undefined") {
      this.set('showCounted', true);
    }
    proc.exec({
      inventoryId: this.get('inventory.id'),
      showCounted: this.get('showCounted'),
      _limit: limit,
      _offset: offset
    }, enyo.bind(this, function (response, message) {
      enyo.forEach(response, function (model) {
        var dalLine = new Backbone.Model(),
            undf;
        _.each(_.keys(model), function (key) {
          if (model[key] !== undf) {
            if (model[key] === null) {
              dalLine.set(key, null);
            } else {
              dalLine.set(key, model[key]);
            }
          }
        });
        me.addLine(dalLine);
      });
      if (callback) {
        callback();
      }
    }), function () {
      window.console.error('error');
    });
  },
  addLine: function (dalLine) {
    var locator, line;
    line = new OBWH.PhysicalInventory.Model.Line({
      inventory: this,
      id: dalLine.get('id'),
      isCounted: dalLine.get('oBMWHPICounted')
    });
    locator = this.get('locators').get(dalLine.get('storageBin'));
    if (!locator) {
      locator = new OBWH.PhysicalInventory.Model.Locator({
        inventory: this,
        name: dalLine.get('storageBinName'),
        id: dalLine.get('storageBin')
      });
      this.get('locators').add(locator);
    }
    locator.get('lines').add(line);
    this.get('lines').add(line);

    line.setLine(dalLine);
    this.set('totalLines', this.get('totalLines') + 1);
    if (dalLine.get('oBMWHPICounted')) {
      this.set('doneLines', this.get('doneLines') + 1);
    }
  },
  resetQuantity: function (dalLine) {
    var me = this,
        proc = new OB.DS.Process(OBWH.PhysicalInventory.Model.processPhysicalInventoryClass);
    proc.exec({
      action: 'resetLine',
      inventoryId: this.get('inventory.id'),
      lineId: dalLine.id
    }, function (response, message) {
      if (response && response.exception) {
        OB.UTIL.showError(response.exception.message);
      } else {
        OB.UTIL.showSuccess('success');
      }
      OB.UTIL.showLoading(false);
    }, function () {
      window.console.error('error');
      OB.UTIL.showLoading(false);
    });
  }
});

OBWH.PhysicalInventory.Model.Line = Backbone.Model.extend({
  setLine: function (dalLine) {
    this.set('product', dalLine.get('product'));
    this.set('productName', dalLine.get('productName'));
    this.set('attributeSetValue', dalLine.get('attributeSetValue'));
    this.set('attributeSetValueName', dalLine.get('attributeSetValueName'));
    this.set('uom', dalLine.get('uom'));
    if (OB.MobileApp.model.get('permissions').OBMWHPI_ShowBookedQty) {
      this.set('bookQty', dalLine.get('bookQuantity'));
    }
    if (dalLine.get('oBMWHPICounted')) {
      this.set('countQty', dalLine.get('quantityCount'));
    } else {
      this.set('countQty', '_');
    }

  },
  toggleCounted: function () {
	if (this.get('countQty') !== '_') {
	  this.set('isCounted', !this.get('isCounted'));
	}
  },
  setCountQty: function (qty, incremental) {
    var currentQty = this.get('countQty'),
        auxQty;
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }

    if (this.get('isCounted')) {
      this.set('isCounted', false);
    }
    // TODO: Perhaps is better to use BigDecimal to add the quantity.
    if (incremental) {
      auxQty = Number(qty) + ((currentQty !== '_') ? currentQty : Number(0));
      this.set('countQty', ((auxQty >= 0) ? auxQty : Number(0)));
    } else {
      if (qty === '_') {
        this.set('countQty', '_');
      } else {
        this.set('countQty', ((Number(qty) > 0) ? Number(qty) : Number(0)));
      }

    }

    var timeout = OB.MobileApp.model.get('permissions').OBMWHPI_SavingTimeout || 4000;
    this.timeout = setTimeout(enyo.bind(this, this.toggleCounted), timeout);
  }
});

OBWH.PhysicalInventory.Model.Lines = Backbone.Collection.extend({
  model: OBWH.PhysicalInventory.Model.Line
});

OBWH.PhysicalInventory.Model.Locator = Backbone.Model.extend({
  initialize: function () {
    this.set('lines', new OBWH.PhysicalInventory.Model.Lines());
  }
});

OBWH.PhysicalInventory.Model.Locators = Backbone.Collection.extend({
  model: OBWH.PhysicalInventory.Model.Locator
});

OBWH.PhysicalInventory.Model.processPhysicalInventoryClass = 'org.openbravo.mobile.warehouse.physicalinventory.ProcessPhysicalInventory';
OBWH.PhysicalInventory.Model.processPhysicalInventoryLine = 'org.openbravo.mobile.warehouse.physicalinventory.process.InventoryCountLine';
OBWH.PhysicalInventory.Model.processPhysicalInventoryLineNumber = 'org.openbravo.mobile.warehouse.physicalinventory.process.InventoryCountLineNumber';
OBWH.PhysicalInventory.Model.getInventoryWhereClause = function (qry) {
  var criteria = {
    _where: 'e.processed = false AND e.oBMWHPIUser.id = \'' + OB.MobileApp.model.usermodel.id + '\''
  };

  if (qry) {
    criteria = _.extend(criteria, {
      _OrExpression: true,
      operator: 'or',
      _constructor: 'AdvancedCriteria',
      criteria: [{
        'fieldName': 'name',
        'operator': 'iContains',
        'value': qry
      }]
    });
  }
  criteria._sortBy = 'movementDate';
  return criteria;
};