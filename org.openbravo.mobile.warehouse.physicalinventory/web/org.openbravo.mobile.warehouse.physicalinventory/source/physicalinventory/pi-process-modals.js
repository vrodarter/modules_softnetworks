/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, $ */


enyo.kind({
  kind: 'OB.UI.ModalAction',
  name: 'OB.OBMWHPI.UI.sureToProcess',
  i18nHeader: 'OBMWHPI_SureToProcess',
  bodyContent: {
    i18nContent: 'OBMWHPI_MsgSureToProcess'
  },
  bodyButtons: {
    components: [{
      //OK button
      kind: 'OB.OBMWHPI.UI.sureToProcess_OkButton'
    }, {
      //Cancel button
      kind: 'OB.OBMWHPI.UI.sureToProcess_CancelButton'
    }]
  }
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.OBMWHPI.UI.sureToProcess_OkButton',
  i18nContent: 'OBMOBC_LblOk',
  isDefaultAction: true,
  events: {
    onProcessInventory: ''
  },
  tap: function () {
    this.doProcessInventory();
    this.doHideThisPopup();
  }
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.OBMWHPI.UI.sureToProcess_CancelButton',
  i18nContent: 'OBMOBC_LblCancel',
  tap: function () {
    this.doHideThisPopup();
    //OB.POS.navigate('retail.pointofsale');
  }
});