/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global enyo, OBWH, Backbone, _*/

enyo.kind({
  name: 'OBWH.PhysicalInventory.InventorySearch',
  kind: 'OB.UI.Modal',
  topPosition: '125px',
  i18nHeader: 'OBMWHPI_InventoryChooser',
  body: {
    kind: 'OBWH.PhysicalInventory.InventorySearchList',
    name: 'list'
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.InventorySearchList',
  clases: 'row-fluid',

  handlers: {
    onSearchAction: 'searchAction',
    onClearAction: 'clearAction'
  },
  events: {
    onSelectInventory: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'invList',
          kind: 'OB.UI.ScrollableTable',
          scrollAreaMaxHeight: '400px',
          renderHeader: 'OBWH.PhysicalInventory.PhysicalInventoryHeader',
          renderLine: 'OBWH.PhysicalInventory.InvRecord',
          renderEmpty: 'OB.UI.RenderEmpty'
        }]
      }]
    }]
  }],

  clearAction: function (inSender, inEvent) {
    this.inventoryList.reset();
    return true;
  },

  searchAction: function (inSender, inEvent) {
    var query = inEvent ? inEvent.query : null;
    OB.Dal.find(OB.Model.MaterialMgmtInventoryCount, OBWH.PhysicalInventory.Model.getInventoryWhereClause(query), enyo.bind(this, function (prodCol) {
      if (prodCol && prodCol.length > 0) {
        this.inventoryList.reset(prodCol.models);
      } else {
        this.inventoryList.reset();
      }
    }));

    return true;
  },

  init: function () {
    this.inventoryList = new Backbone.Collection();
    this.$.invList.setCollection(this.inventoryList);
    this.inventoryList.on('click', function (model) {
      this.doSelectInventory({
        inventory: model
      });
    }, this);
    this.searchAction();
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.PhysicalInventoryHeader',
  kind: 'OB.UI.ScrollableTableHeader',
  events: {
    onSearchAction: '',
    onClearAction: ''
  },
  components: [{
    style: 'display: table;',
    components: [{
      style: 'display: table-cell; width: 100%;',
      components: [{
        kind: 'OB.UI.SearchInputAutoFilter',
        name: 'filterText',
        style: 'width: 100%'
      }]
    }, {
      style: 'display: table-cell;',
      components: [{
        kind: 'OB.UI.SmallButton',
        classes: 'btnlink-gray btn-icon-small btn-icon-clear',
        style: 'width: 100px; margin: 0px 5px 8px 19px;',
        ontap: 'clearAction'
      }]
    }, {
      style: 'display: table-cell;',
      components: [{
        kind: 'OB.UI.SmallButton',
        classes: 'btnlink-yellow btn-icon-small btn-icon-search',
        style: 'width: 100px; margin: 0px 0px 8px 5px;',
        ontap: 'searchAction'
      }]
    }]
  }],
  clearAction: function () {
    this.$.filterText.setValue('');
    this.doClearAction();
  },
  searchAction: function () {
    this.doSearchAction({
      query: this.$.filterText.getValue()
    });
    return true;
  }
});

enyo.kind({
  name: 'OBWH.PhysicalInventory.InvRecord',
  kind: 'OB.UI.SelectButton',
  components: [{
    name: 'line',
    style: 'line-height: 23px;',
    components: [{
      name: 'identifier'
    }, {
      style: 'clear: both;'
    }]
  }],
  events: {
    onHideThisPopup: ''
  },
  tap: function () {
    this.doHideThisPopup();
    this.inherited(arguments);
  },
  create: function () {
    this.inherited(arguments);
    //this.$.identifier.setContent(this.model.get(OB.Constants.IDENTIFIER));
    this.$.identifier.setContent(this.model.get('_identifier'));
    //    this.$.status.setContent(OBWH.PhysicalInventory.Model.PickingStatus[this.model.get('pickliststatus')]);
  }
});