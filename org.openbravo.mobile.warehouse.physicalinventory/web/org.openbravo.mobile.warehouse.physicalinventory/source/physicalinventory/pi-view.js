/*
 ************************************************************************************
 * Copyright (C) 2014 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global enyo, OBWH*/

enyo.kind({
  name: 'OBWH.PhysicalInventory.View',
  kind: 'OB.UI.WindowView',
  windowmodel: OBWH.PhysicalInventory.Model,
  handlers: {
    onSelectInventory: 'selectInventory',
    onProcessInventory: 'processInventory',
    onActivateTab: 'activateTabHandler',
    onSetQuantityCount: 'setQtyCountHandler',
    onBackMenuAction: 'backMenuHandler',
    onScan: 'scanHandler',
    onChangePage: 'changePage',
    onShowHideCounted: 'showHideCountedHandler',
    onResetQuantity: 'resetQuantityHandler'
  },
  components: [{
    kind: 'OB.UI.MultiColumn',
    name: 'multiColumn',
    leftToolbar: {
      kind: 'OBWH.PhysicalInventory.LeftToolbar'
    },
    leftPanel: {
      kind: 'OBWH.PhysicalInventory.InventoryView',
      name: 'inventoryPanel'
    },
    rightToolbar: {
      kind: 'OBWH.PhysicalInventory.RightToolbar'
    },
    rightPanel: {
      kind: 'OBWH.PhysicalInventory.LineView',
      name: 'rightPanel'
    }
  }, {
    kind: 'OBWH.PhysicalInventory.InventorySearch',
    name: 'modalPhysicalInventorySearch'
  }, {
    kind: 'OB.OBMWHPI.UI.sureToProcess',
    name: 'modalSureToProcess'
  }],

  changePage: function () {
    this.waterfall('onNewPage');
  },
  selectInventory: function (inSender, inEvent) {
    // Reset all previous locators
    this.model.get('inventory').get('locators').reset();
    this.model.get('inventory').reset(inEvent.inventory);
  },
  processInventory: function (inSender, inEvent) {
    this.model.processInventory(this.model.get('inventory'));
  },
  activateTabHandler: function (inSender, inEvent) {
    if (!this.model.get('currentLine')) {
      inEvent.tab = 'scan';
    }
    this.model.set('activeTab', inEvent.tab);
  },
  backMenuHandler: function () {
    OB.MobileApp.model.navigate('wh');
  },
  setQtyCountHandler: function (inSender, inEvent) {
    if (this.model.get('currentLine')) {
      this.model.get('currentLine').setCountQty(inEvent.quantity, inEvent.incremental);
    }
  },
  // show / hide counted handler
  showHideCountedHandler: function (inSender, inEvent) {
    this.model.get('inventory').get('locators').reset();
    this.model.get('inventory').reset();
    this.changePage();
  },

  // reset quantity handler
  resetQuantityHandler: function (inSender, inEvent) {
    this.model.get('inventory').resetQuantity(this.model.get('currentLine'));
    this.model.get('currentLine').setCountQty('_', false);
  },

  scanHandler: function (inSender, inEvent) {
    this.model.scan(inEvent.code);
  },

  init: function () {
    this.inherited(arguments);

    OB.MobileApp.view.scanningFocus(true);
  }
});

OB.MobileApp.windowRegistry.registerWindow({
  windowClass: 'OBWH.PhysicalInventory.View',
  route: 'OBMWHPI_physicalinventory',
  menuPosition: null
});