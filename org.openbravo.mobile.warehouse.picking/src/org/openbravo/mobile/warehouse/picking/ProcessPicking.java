/*
 ************************************************************************************
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package org.openbravo.mobile.warehouse.picking;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.mobile.warehouse.WarehouseJSONProcess;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.warehouse.pickinglist.MovementLineHandler;
import org.openbravo.warehouse.pickinglist.actionhandler.RaiseIncidenceHandler;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBCriteria;
import org.hibernate.criterion.Restrictions;

public class ProcessPicking extends WarehouseJSONProcess {
  private static final Logger log = Logger.getLogger(ProcessPicking.class);

  @Override
  protected String getProperty() {
    return WHPickingConstants.PICKING_PROPERTY;
  }

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    long t0 = System.currentTimeMillis();

     System.out.println("****************************************************");//EZL


    OBContext.setAdminMode(true);

    try {
      final String action = jsonsent.getString("action");
      JSONArray items = jsonsent.getJSONArray("items");
      if ("incidence".equals(action)) {
        raiseIncidence(items);
      } else if ("resetincidence".equals(action)) {
        resetIncidence(items);
      } else if ("process".equals(action)) {
        processItems(items);
      }

      OBDal.getInstance().flush();

      long t1 = System.currentTimeMillis();
      log.debug("time to create the document: " + (t1 - t0));

      JSONObject result = new JSONObject();
      result.put("status", 0);
      result.put("message", OBMessageUtils.messageBD("success"));

      long t2 = System.currentTimeMillis();
      log.debug("time to process: " + (t2 - t1));
      log.debug(" TOTAL time: " + (t2 - t0));

      return result;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private void raiseIncidence(JSONArray items) throws JSONException {
    for (int i = 0; i < items.length(); i++) {
      JSONObject item = items.getJSONObject(i);
      InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
          item.getString("id"));
      RaiseIncidenceHandler.raiseIncidence(mvmtLine, "");
      OBDal.getInstance().save(mvmtLine);
    }
  }

  private void resetIncidence(JSONArray items) throws JSONException {
    for (int i = 0; i < items.length(); i++) {
      JSONObject item = items.getJSONObject(i);
      InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
          item.getString("id"));
      RaiseIncidenceHandler.resetIncidence(mvmtLine);
      OBDal.getInstance().save(mvmtLine);
    }
  }

  private void processItems(JSONArray items) throws JSONException {
    for (int i = 0; i < items.length(); i++) {
      JSONObject item = items.getJSONObject(i);

      try{

        System.out.println("SN:"+item.getString("serialNumber"));

      InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
          item.getString("id"));


      AttributeSetInstance attributeSetInstance = null;

  
      // Get Attribute Set for Serial Number
      OBCriteria<AttributeSet> criteriaAttributeSet = OBDal.getInstance().createCriteria(
          AttributeSet.class);
      criteriaAttributeSet.add(Restrictions.eq("serialNo", true));
      criteriaAttributeSet.add(Restrictions.eq("client", mvmtLine.getClient()));
      List<AttributeSet> attributeSetList = criteriaAttributeSet.list();
      AttributeSet attributeSet = null;
      if (attributeSetList.size() > 0) {
        attributeSet = attributeSetList.get(0);
      }

      if (attributeSet != null) {
        // Insert Attribute Set Instance
        attributeSetInstance = OBProvider.getInstance().get(AttributeSetInstance.class);
        attributeSetInstance.setOrganization(mvmtLine.getOrganization());
        attributeSetInstance.setAttributeSet(attributeSet);
        attributeSetInstance.setSerialNo(item.getString("serialNumber"));
        attributeSetInstance.setDescription("");
        attributeSetInstance.setLocked(false);
        OBDal.getInstance().save(attributeSetInstance);
      mvmtLine.setAttributeSetValue(attributeSetInstance);
      }


      OBDal.getInstance().save(mvmtLine);

      }catch(Exception e){
        e.printStackTrace();
      }

      final String strMvmtLineId = item.getString("id");
      try {
        MovementLineHandler.processMovementLine(strMvmtLineId, "CF");
      } catch (PropertyException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }
}

