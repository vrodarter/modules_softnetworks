/*
 ************************************************************************************
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global enyo, OBWH, Backbone*/

enyo.kind({
  name: 'OBWH.Picking.LeftToolbar',
  kind: 'OB.UI.MultiColumn.Toolbar',
  handlers: {
    onDisableButton: 'disableButton'
  },

  showMenu: true,
  menuEntries: [{
    kind: 'OB.UI.MenuAction',
    i18nLabel: 'OBMOBC_LblBack',
    eventName: 'onBackMenuAction',
    name: 'properties'
  }],
  buttons: [{
    kind: 'OBWH.Picking.ButtonSelect',
    name: 'btnSelect',
    span: 6
  }, {
    kind: 'OBWH.Picking.ButtonDone',
    name: 'btnDone',
    span: 6
  }],

  disableButton: function (inSender, inEvent) {
    this.waterfall('onDisableMenuEntry', {
      entryName: 'properties',
      disable: inEvent.buttons.indexOf('new') === -1
    });
  },

  initComponents: function () {
    this.inherited(arguments);
    this.waterfall('onDisableMenuEntry', {
      entryName: 'properties',
      disable: true
    });
  }
});

enyo.kind({
  name: 'OBWH.Picking.ButtonSelect',
  kind: 'OB.UI.ToolbarButton',
  icon: 'btn-icon btn-icon-new',
  handlers: {
    onDisableButton: 'disableButton'
  },
  events: {
    onShowPopup: ''
  },
  tap: function () {
    //this.setDisabled(true);
    this.doShowPopup({
      popup: 'modalPickingSearch'
    });
  },
  disableButton: function (inSender, inEvent) {
    this.setDisabled(inEvent.buttons.indexOf('select') !== -1);
    //return true;
  }
});

enyo.kind({
  name: 'OBWH.Picking.ButtonDone',
  kind: 'OB.UI.ToolbarButton',
  events: {
    onProcessPicking: ''
  },
  handlers: {
    onDisableButton: 'disableButton'
  },

  disableButton: function (inSender, inEvent) {
    this.setDisabled(inEvent.buttons.indexOf('done') !== -1);
    return true;
  },

  tap: function () {
    this.doProcessPicking();
  },

  init: function (model) {
    var me = this,
        picking = model.get('picking');
    picking.on('change:ready', function (model) {
      //enable button
      me.setDisabled(!model.get('ready'));
    });
  },

  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBWH_BtnDone'));
    this.setDisabled(true);
  }
});

enyo.kind({
  name: 'OBWH.Picking.PickingView',
  classes: 'span12',

  components: [{
    style: 'overflow:auto; margin: 5px; position: relative; background-color: #ffffff; color: black; padding: 5px;',
    classes: 'row-fluid',
    components: [{
      name: 'picking',
      showing: false,
      components: [{
        kind: 'OBWH.Picking.PickingHeader',
        style: 'position: relative; height: 40px;',
        name: 'header'
      }, {
        kind: 'OBWH.Picking.PickingDetails',
        name: 'details'
      }]
    }, {
      name: 'noPicking',
      kind: 'OBWH.Picking.BasePickingList'
    }]
  }],

  init: function (model) {
    var me = this;
    model.get('picking').on('change:picking.id', function () {
      me.$.noPicking.hide();
      me.$.picking.show();
    });
  }
});

enyo.kind({
  name: 'OBWH.Picking.BasePickingList',
  classes: 'row-fluid',
  published: {
    pickingList: null
  },
  events: {
    onSelectPicking: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'pickList',
          kind: 'OB.UI.ScrollableTable',
          scrollAreaMaxHeight: '600px',
          renderLine: 'OBWH.Picking.PickingLine',
          renderEmpty: 'OB.UI.RenderEmpty'
        }]
      }]
    }]
  }],

  init: function () {
    this.pickingList = new Backbone.Collection();
    this.$.pickList.setCollection(this.pickingList);
    this.pickingList.on('click', function (model) {
      this.doSelectPicking({
        picking: model
      });
    }, this);
    OB.Dal.find(OB.Model.OBWPL_pickinglist, OBWH.Picking.Model.getPickingWhereClause(), enyo.bind(this, function (prodCol) {
      if (prodCol && prodCol.length > 0) {
        this.pickingList.reset(prodCol.models);
      } else {
        this.pickingList.reset();
      }
    }));
  }
});

enyo.kind({
  name: 'OBWH.Picking.PickingHeader',
  kind: 'OB.UI.ScrollableTableHeader',

  components: [{
    name: 'line',
    style: 'line-height: 40px; font-size: 20px; text-align: center; width: 60%; position: absolute; left: 0;'
  }, {
    name: 'outbound',
    style: 'line-height: 40px; font-size: 20px; text-align: center; background-color: #F8941D; width: 40%; position: absolute; right: 0;'
  }],
  init: function (model) {
    var me = this,
        picking = model.get('picking');
    picking.get('items').on('add remove', function () {
      me.$.line.setContent(picking.get('picking').get(OB.Constants.IDENTIFIER));
      me.$.outbound.setContent(picking.get('picking').get('outboundStorageBin$' + OB.Constants.IDENTIFIER));
    });
  }
});

enyo.kind({
  name: 'OBWH.Picking.PickingDetails',
  kind: 'OB.UI.ScrollableTable',
  scrollAreaMaxHeight: '437px',
  renderLine: 'OBWH.Picking.Locator',
  renderEmpty: 'OB.UI.RenderEmpty',
  listStyle: 'edit',
  init: function (model) {
    var locators = model.get('picking').get('locators');
    this.setCollection(locators);
  }
});

enyo.kind({
  name: 'OBWH.Picking.Locator',
  components: [{
    name: 'header',
    kind: 'OB.UI.ScrollableTableHeader',
    style: 'font-size: 24px; line-height: 32px;'
  }, {
    name: 'items',
    kind: 'OBWH.Picking.Items'
  }],
  initComponents: function () {
    this.inherited(arguments);
    var locator = this.model;
    this.$.header.setContent(locator.get('name'));
    this.$.items.setCollection(locator.get('items'));
  }
});

enyo.kind({
  name: 'OBWH.Picking.Items',
  kind: 'OB.UI.ScrollableTable',
  scrollAreaMaxHeight: '437px',
  renderLine: 'OBWH.Picking.Item',
  renderEmpty: 'OB.UI.RenderEmpty',
  //listStyle: 'edit',
  listStyle: 'nonselectablelist',
  init: function (model) {
    var items = model.get('items');
    this.setCollection(items);
  }
});

enyo.kind({
  name: 'OBWH.Picking.CheckItem',
  kind: 'OB.UI.CheckboxButton',
  tag: 'div',
  events: {
    onPickQtyTap: ''
  },
  tap: function () {
    if (!this.disabled) {
      this.inherited(arguments);
      this.doPickQtyTap();
    }
  },

  initComponents: function () {
    var status = this.parent.model.get('status');
    this.inherited(arguments);
    if (status === 'RE') {
      this.setClassAttribute('btn-check active');
    } else if (status === 'IN') {
      this.setClassAttribute('btn-incidence');
      this.setDisabled(true);
    } else if (status === 'CO' || status === 'CF') {
      this.setClassAttribute('btn-done');
      this.setDisabled(true);
    } else {
      this.setClassAttribute('btn-check');
    }
  }
});

enyo.kind({
  name: 'OBWH.Picking.PickQty',
  style: 'position:relative;',
  published: {
    disabled: false
  },
  events: {
    onPickQtyTap: ''
  },
  components: [{
    style: 'float: right; line-height: 32px;',
    name: 'pickedqty'
  }],
  tap: function () {
    this.inherited(arguments);
    if (!this.getDisabled()) {
      this.doPickQtyTap({
        qty: Number(1)
      });
    }
  },
  initComponents: function () {
    var item = this.parent.model,
        status = item.get('status');
    this.inherited(arguments);
    this.$.pickedqty.setContent(item.get('pickedQty'));
    if (status === 'CO' || status === 'CF' || status === 'IN') {
      this.setDisabled(true);
      this.$.pickedqty.addStyles('font-size: 16px;');
      this.$.pickedqty.setContent(OBWH.Picking.Model.ItemStatus[status]);
    } else {
      this.$.pickedqty.addStyles('font-size: 24px;');
    }
  }
});

enyo.kind({
  name: 'OBWH.Picking.Item',
  kind: 'OB.UI.SelectButton',
  classes: 'btnselect-orderline',
  events: {
    onStatusChange: ''
  },
  handlers: {
    onPickQtyTap: 'pickQtyTapHandler'
  },
  components: [{
    style: 'float: left; width: 10%;',
    kind: 'OBWH.Picking.CheckItem',
    name: 'setQty'
  }, {
    style: 'float: left; width: 60%;',
    components: [{
      name: 'product',
      style: 'font-size: 16px;'
    }, {
      name: 'attribute'
    }]
  }, {
    style: 'float: left; width: 15%; font-size: 24px; line-height: 32px;',
    name: 'quantity'
  }, {
    style: 'float: left; width: 15%; font-size: 24px; line-height: 32px;',
    kind: 'OBWH.Picking.PickQty',
    name: 'pickQty'
  }],
  pickQtyTapHandler: function (inSender, inEvent) {
    var qty;

    if (inEvent.qty) {
      qty = inEvent.qty + this.model.get('pickedQty');
    } else {
      qty = this.model.get('neededQty');
    }
    this.model.setPickedQty(qty);
    this.model.trigger('selected', this.model);
  },

  initComponents: function () {
    var item = this.model,
        me = this,
        status = item.get('status');
    this.inherited(arguments);
    this.$.product.setContent(item.get('productName'));
    this.$.attribute.setContent(item.get('attributeSetValueName'));
    this.$.quantity.setContent(item.get('neededQty'));
    if (status === 'IN') {
      this.addStyles('background-color: #FFE1C0;');
      this.setDisabled(true);
    } else if (status === 'CO' || status === 'CF') {
      this.addStyles('background-color: #BBFFBB;');
      this.setDisabled(true);
      this.tap = null;
    }
  }
});