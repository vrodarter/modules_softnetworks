/*
 ************************************************************************************
 * Copyright (C) 2013 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global enyo, OBWH*/

enyo.kind({
  name: 'OBWH.Picking.View',
  kind: 'OB.UI.WindowView',
  windowmodel: OBWH.Picking.Model,
  handlers: {
    onSelectPicking: 'selectPicking',
    onProcessPicking: 'processPicking',
    onSetQuantity: 'setPickedQtyHandler',
    onActivateTab: 'activateTabHandler',
    onRaiseIncidence: 'raiseIncidence',
    onResetIncidence: 'resetIncidence',
    onScan: 'scanHandler',
    onBackMenuAction: 'backMenuHandler'
  },
  events: {
    onShowPopup: ''
  },
  components: [{
    kind: 'OB.UI.MultiColumn',
    name: 'multiColumn',
    leftToolbar: {
      kind: 'OBWH.Picking.LeftToolbar'
    },
    leftPanel: {
      kind: 'OBWH.Picking.PickingView',
      name: 'pickingPanel'
    },
    rightToolbar: {
      kind: 'OBWH.Picking.RightToolbar'
    },
    rightPanel: {
      kind: 'OBWH.Picking.ItemView',
      name: 'rightPanel'
    }
  }, {
    kind: 'OBWH.Picking.PickingSearch',
    name: 'modalPickingSearch'
  }],

  init: function () {
    var p, me = this;

    this.inherited(arguments);

    if (this.params) {
      p = JSON.parse(decodeURI(this.params));
      this.model.set('parameters', p);
    }

    OB.MobileApp.view.scanningFocus(true);
  },

  selectPicking: function (inSender, inEvent) {
    this.model.get('picking').reset(inEvent.picking);
  },

  setPickedQtyHandler: function (inSender, inEvent) {
    if (this.model.get('currentItem').get('status') === 'IN') {
      return;
    }
    this.model.get('currentItem').setPickedQty(inEvent.quantity, inEvent.incremental);
  },

  activateTabHandler: function (inSender, inEvent) {
    if (!this.model.get('currentItem')) {
      inEvent.tab = 'scan';
    }
    this.model.set('activeTab', inEvent.tab);
  },

  scanHandler: function (inSender, inEvent) {
    this.model.scan(inEvent.code);
  },

  processPicking: function () {
    var me = this;
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMWHP_ProcessTitle'), OB.I18N.getLabel('OBMWHP_ProcessText'), [{
      label: OB.I18N.getLabel('OBMOBC_LblOk'),
      action: function () {
        OB.UTIL.showLoading(true);
        me.model.processPicking();
      }
    }, {
        label: OB.I18N.getLabel('OBMOBC_LblCancel')
    }]);
  },

  raiseIncidence: function () {
    var me = this;
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMWHP_RaiseIncidenceTitle'), OB.I18N.getLabel('OBMWHP_RaiseIncidenceText'), [{
      label: OB.I18N.getLabel('OBMWHP_Incidence'),
      action: function () {
        OB.UTIL.showLoading(true);
        me.model.raiseIncidence();
      }
    }, {
        label: OB.I18N.getLabel('OBMOBC_LblCancel')
    }]);
  },

  resetIncidence: function () {
    var me = this;
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMWHP_UndoIncidence'), OB.I18N.getLabel('OBMWHP_ResetIncidenceText'), [{
      label: OB.I18N.getLabel('OBMOBC_LblUndo'),
      action: function () {
        OB.UTIL.showLoading(true);
        me.model.resetIncidence();
      }
    }, {
        label: OB.I18N.getLabel('OBMOBC_LblCancel')
    }]);
  },

  backMenuHandler: function () {
    OB.MobileApp.model.navigate('wh');
  }
});

OB.MobileApp.windowRegistry.registerWindow({
  windowClass: 'OBWH.Picking.View',
  route: 'OBMWHP_Picking',
  menuPosition: null
});