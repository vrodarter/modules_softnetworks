/************************************************************************************ 
 * Copyright (C) 2010 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package org.openbravo.module.invoiceTaxReportEnhanced30.ad_reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;

import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.security.OrganizationStructureProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord;
import org.openbravo.module.invoiceTaxReportEnhanced30.comparators.OrderByTaxNameAndBP;
import org.openbravo.module.invoiceTaxReportEnhanced30.utility.Utility;

/**
 * This class contains the specific code used Invoice Tax Report to access the database using DAL
 * (Hibernate)
 * 
 * @author openbravo
 * 
 */
public class OBMTR30InvoiceTaxReportDao {

  private final boolean isWithHolding;

  private final Date dateFrom;
  private final Date dateTo;
  private final String businessPartners;
  private final String cTaxId;
  private final String withHoldingAsTaxId;
  private final Organization org;
  private final String cWithHoldingId;
  private final VariablesSecureApp vars;
  private final String lang;
  private final String userCurrency;
  private final String dateFormat;

  public OBMTR30InvoiceTaxReportDao(final HashMap<String, Object> params) {
    this.isWithHolding = Utility.convertToBoolean((String) params.get("isWithHolding"));
    this.dateFrom = (Date) params.get("dateFrom");
    this.dateTo = (Date) params.get("dateTo");
    this.businessPartners = (String) params.get("businessPartners");
    this.cTaxId = (String) params.get("cTaxId");
    this.withHoldingAsTaxId = (String) params.get("WithHoldingAsTaxId");
    this.org = (Organization) params.get("organization");
    this.cWithHoldingId = (String) params.get("cWithHoldingId");
    this.vars = (VariablesSecureApp) params.get("vars");
    this.lang = vars.getLanguage();
    this.userCurrency = (String) params.get("PARAM_CURRENCY");
    this.dateFormat = (String) params.get("strDateFormat");
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getCashPayment(final Date fromDate, final Date toDate,
      final String cWithHoldingID, final boolean isSales) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(cl.cashJournal.name");
      sql_e.append(" ,'-1' as businessPartner ");
      sql_e.append(" ,'-1' as partnerAddress ");
      sql_e.append(" ,cl.cashJournal.transactionDate ");
      sql_e.append(" ,cl.cashJournal.accountingDate ");
      sql_e.append(" ,cl.gLItem.withholding.name ");
      sql_e.append(" ,cl.gLItem.withholding.rate ");
      if (isSales) {
        sql_e.append(" ,cl.amount as taxAmount ");
        sql_e.append(" ,c.id ");
        sql_e.append(" ,'C' as documentType ");
        sql_e
            .append(" ,cl.amount/((case cl.gLItem.withholding.rate when 0 then 1 else cl.gLItem.withholding.rate end)/100) as taxableAmount ");
        sql_e.append(" ,cl.cashJournal.endingBalance as totalDoc ");
      } else {
        sql_e.append(" ,coalesce(cl.amount*(-1),0) as taxAmount ");
        sql_e.append(" ,c.id ");
        sql_e.append(" ,'C' as documentType ");
        sql_e
            .append(" ,coalesce(cl.amount*(-1),0)/((case cl.gLItem.withholding.rate when 0 then 1 else cl.gLItem.withholding.rate end)/100) as taxableAmount ");
        sql_e.append(" ,cl.cashJournal.endingBalance as totalDoc ");
      }
      sql_e.append(" ,cl.gLItem.withholding.id ");
      sql_e.append(" ,'-1' as taxCategory ");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e.append(" from FinancialMgmtJournalLine cl, Currency c ");
      whereClause.append(" where cl.cashJournal.active ='Y' ");
      whereClause.append(" and cl.cashJournal.posted = 'Y' ");
      whereClause.append(" and cl.cashJournal.processed='Y' ");
      whereClause.append(" and cl.cashJournal.accountingDate < ? ");
      whereClause.append(" and cl.cashJournal.accountingDate >= ? ");
      whereClause.append(" and cl.currency.id = c.id");
      if (isSales) {
        whereClause.append(" and cl.amount > 0 ");
      } else {
        whereClause.append(" and cl.amount < 0 ");
      }
      if (!"".equals(cWithHoldingID)) {
        whereClause.append(" and cl.gLItem.withholding.id = '" + cWithHoldingID + "'");
      }

      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and cl.cashJournal.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return query_e.list();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getCashTrx(final Date fromDate, final Date toDate,
      final String cTaxWithHoldingId, final String cTaxID, final boolean isWithholding,
      final boolean isSales) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(cl.cashJournal.name");
      sql_e.append(" ,'-1' as businessPartner ");
      sql_e.append(" ,'-1' as partnerAddress ");
      sql_e.append(" ,cl.cashJournal.transactionDate ");
      sql_e.append(" ,cl.cashJournal.accountingDate ");
      sql_e.append(" ,cl.gLItem.tax.name ");
      sql_e.append(" ,cl.gLItem.tax.rate ");
      if (isSales) {
        sql_e.append(" ,cl.amount as taxAmount ");
        sql_e.append(" ,c.id ");
        sql_e.append(" ,'C' as documentType ");
        sql_e
            .append(" ,cl.amount/((case cl.gLItem.tax.rate when 0 then 1 else cl.gLItem.tax.rate end)/100) as taxableAmount ");
        sql_e.append(" ,cl.cashJournal.endingBalance as totalDoc ");
      } else {
        sql_e.append(" ,coalesce(cl.amount*(-1),0) as taxAmount ");
        sql_e.append(" ,c.id ");
        sql_e.append(" ,'C' as documentType ");
        sql_e
            .append(" ,coalesce(cl.amount*(-1),0)/((case cl.gLItem.tax.rate when 0 then 1 else cl.gLItem.tax.rate end)/100) as taxableAmount ");
        sql_e.append(" ,cl.cashJournal.endingBalance as totalDoc ");
      }
      sql_e.append(" ,cl.gLItem.tax.id ");
      sql_e.append(" ,cl.gLItem.tax.taxCategory.id ");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e.append(" from FinancialMgmtJournalLine cl, Currency c ");
      whereClause.append(" where cl.cashJournal.active ='Y' ");
      whereClause.append(" and cl.cashJournal.posted = 'Y' ");
      whereClause.append(" and cl.cashJournal.processed='Y' ");
      whereClause.append(" and cl.cashJournal.accountingDate < ? ");
      whereClause.append(" and cl.cashJournal.accountingDate >= ? ");
      whereClause.append(" and cl.currency.id = c.id");
      if (isSales) {
        whereClause.append(" and cl.amount > 0 ");
      } else {
        whereClause.append(" and cl.amount < 0 ");
      }
      if (isWithholding) {
        whereClause.append(" and cl.gLItem.tax.withholdingTax = true ");
      } else {
        whereClause.append(" and cl.gLItem.tax.withholdingTax = false ");
      }
      if (!"".equals(cTaxWithHoldingId)) {
        whereClause.append(" and cl.gLItem.tax.id = '" + cTaxWithHoldingId + "'");
      }
      if (!"".equals(cTaxID)) {
        whereClause.append(" and cl.gLItem.tax.id = '" + cTaxID + "'");
      }
      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and cl.cashJournal.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return scrollResults(query_e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getBankStatementsTrx(final Date fromDate, final Date toDate,
      final String cTaxWithHoldingId, final String cTaxID, final boolean isWithholding,
      final boolean isSales) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(bsl.bankStatement.name");
      sql_e.append(" ,'-1' as businessPartner ");
      sql_e.append(" ,'-1' as partnerAddress ");
      sql_e.append(" ,bsl.effectiveDate ");
      sql_e.append(" ,bsl.accountingDate ");
      sql_e.append(" ,bsl.gLItem.tax.name ");
      sql_e.append(" ,bsl.gLItem.tax.rate ");
      if (isSales) {
        sql_e.append(" ,bsl.statementAmount as taxAmount ");
        sql_e.append(" ,c.id ");
        sql_e.append(" ,'B' as documentType ");
        sql_e
            .append(" ,bsl.statementAmount/((case bsl.gLItem.tax.rate when 0 then 1 else bsl.gLItem.tax.rate end)/100) as taxableAmount ");
        sql_e.append(" ,bsl.bankStatement.endingBalance as totalDoc ");
      } else {
        sql_e.append(" ,coalesce(bsl.statementAmount*(-1),0) as taxAmount ");
        sql_e.append(" ,c.id ");
        sql_e.append(" ,'B' as documentType ");
        sql_e
            .append(" ,coalesce(bsl.statementAmount*(-1),0)/((case bsl.gLItem.tax.rate when 0 then 1 else bsl.gLItem.tax.rate end)/100) as taxableAmount ");
        sql_e.append(" ,bsl.bankStatement.endingBalance as totalDoc ");
      }
      sql_e.append(" ,bsl.gLItem.tax.id ");
      sql_e.append(" ,bsl.gLItem.tax.taxCategory.id ");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e.append(" from FinancialMgmtBankStatementLine bsl, Currency as c ");
      whereClause.append(" where bsl.bankStatement.active ='Y' ");
      whereClause.append(" and bsl.bankStatement.posted = 'Y' ");
      whereClause.append(" and bsl.bankStatement.processed='Y' ");
      whereClause.append(" and bsl.accountingDate < ? ");
      whereClause.append(" and bsl.accountingDate >= ? ");
      whereClause.append(" and bsl.currency.id = c.id");
      if (isSales) {
        whereClause.append(" and bsl.statementAmount > 0 ");
      } else {
        whereClause.append(" and bsl.statementAmount < 0 ");
      }
      if (isWithholding) {
        whereClause.append(" and bsl.gLItem.tax.withholdingTax = true ");
      } else {
        whereClause.append(" and bsl.gLItem.tax.withholdingTax = false ");
      }
      if (!"".equals(cTaxWithHoldingId)) {
        whereClause.append(" and bsl.gLItem.tax.id =  '" + cTaxWithHoldingId + "'");
      }
      if (!"".equals(cTaxID)) {
        whereClause.append(" and bsl.gLItem.tax.id =  '" + cTaxID + "'");
      }
      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and bsl.bankStatement.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return scrollResults(query_e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getBankStatementsPayment(final Date fromDate, final Date toDate,
      final String cWithHoldingID, final boolean isSales) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(bsl.bankStatement.name");
      sql_e.append(" ,'-1' as businessPartner ");
      sql_e.append(" ,'-1' as partnerAddress ");
      sql_e.append(" ,bsl.effectiveDate ");
      sql_e.append(" ,bsl.accountingDate ");
      sql_e.append(" ,bsl.gLItem.withholding.name ");
      sql_e.append(" ,bsl.gLItem.withholding.rate ");
      if (isSales) {
        sql_e.append(" ,bsl.statementAmount as taxAmount ");
        sql_e.append(" ,c.id ");
        sql_e.append(" ,'B' as documentType ");
        sql_e
            .append(" ,bsl.statementAmount/((case bsl.gLItem.withholding.rate when 0 then 1 else bsl.gLItem.withholding.rate end)/100) as taxableAmount ");
        sql_e.append(" ,bsl.bankStatement.endingBalance as totalDoc ");
      } else {
        sql_e.append(" ,coalesce(bsl.statementAmount*(-1),0) as taxAmount ");
        sql_e.append(" ,c.id ");
        sql_e.append(" ,'B' as documentType ");
        sql_e
            .append(" ,coalesce(bsl.statementAmount*(-1),0)/((case bsl.gLItem.withholding.rate when 0 then 1 else bsl.gLItem.withholding.rate end)/100) as taxableAmount ");
        sql_e.append(" ,bsl.bankStatement.endingBalance as totalDoc ");
      }
      sql_e.append(" ,bsl.gLItem.withholding.id ");
      sql_e.append(" ,'-1' as taxCategory ");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e.append(" from FinancialMgmtBankStatementLine bsl, Currency as c ");
      whereClause.append(" where bsl.bankStatement.active ='Y' ");
      whereClause.append(" and bsl.bankStatement.posted = 'Y' ");
      whereClause.append(" and bsl.bankStatement.processed='Y' ");
      whereClause.append(" and bsl.accountingDate < ? ");
      whereClause.append(" and bsl.accountingDate >= ? ");
      whereClause.append(" and bsl.currency.id = c.id");
      if (isSales) {
        whereClause.append(" and bsl.statementAmount > 0 ");
      } else {
        whereClause.append(" and bsl.statementAmount < 0 ");
      }
      if (!"".equals(cWithHoldingID)) {
        whereClause.append(" and bsl.gLItem.withholding.id = '" + cWithHoldingID + "'");
      }

      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and bsl.bankStatement.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return query_e.list();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getGLJournalsPayment(final Date fromDate, final Date toDate,
      final String cWithHoldingID) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(jl.journalEntry.documentNo, '-1' as businessPartner, '-1' as partnerAddress, jl.journalEntry.documentDate, jl.journalEntry.accountingDate, ");
      sql_e.append(" jl.withholding.name,  jl.withholding.rate ");
      sql_e
          .append(" ,(case jl.foreignCurrencyDebit when 0 then coalesce(jl.foreignCurrencyCredit*(-1),0) else jl.foreignCurrencyDebit end) as taxAmount ");
      sql_e.append(" ,c.id , jl.journalEntry.documentType.id ");
      sql_e
          .append(" ,(case jl.foreignCurrencyCredit when 0 then coalesce(jl.foreignCurrencyDebit*(-1),0) else jl.foreignCurrencyCredit end)/((case jl.withholding.rate when 0 then 1 else jl.withholding.rate end)/100) as taxableAmount ");
      sql_e.append(" ,jl.journalEntry.totalCreditAmount as totalDoc ");
      sql_e.append(" , jl.withholding.id");
      sql_e.append(" , '-1' as taxCategory");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e.append(" from FinancialMgmtGLJournalLine as jl, Currency as c ");
      whereClause.append(" where jl.journalEntry.processed = 'Y' ");
      whereClause.append(" and jl.journalEntry.active = 'Y'");
      whereClause.append(" and jl.journalEntry.posted ='Y' ");
      whereClause.append(" and jl.currency.id = c.id");
      whereClause.append(" and jl.journalEntry.accountingDate < ? ");
      whereClause.append(" and jl.journalEntry.accountingDate >= ? ");
      whereClause.append(" and ( jl.foreignCurrencyCredit > 0 or  jl.foreignCurrencyDebit < 0) ");

      if (!"".equals(cWithHoldingID)) {
        whereClause.append(" and jl.withholding.id = '" + cWithHoldingID + "'");
      }
      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and jl.journalEntry.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return query_e.list();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getGLJournalsTransaction(final boolean isSales,
      final boolean isWithHoldingTax, final Date fromDate, final Date toDate, final String strBps,
      final String cTaxID, final String cTaxWithHoldingId) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append("select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(jl.journalEntry.documentNo, '-1' as businessPartner, '-1' as partnerAddress, jl.journalEntry.documentDate, jl.journalEntry.accountingDate, ");
      sql_e.append(" jl.tax.name,  jl.tax.rate ");

      whereClause.append(" where jl.journalEntry.processed = 'Y' ");
      whereClause.append(" and jl.journalEntry.active = 'Y'");
      whereClause.append(" and jl.journalEntry.posted ='Y' ");
      // whereClause.append(" and bpl.businessPartner.id = jl.accountingCombination.businessPartner.id");
      // whereClause.append(" and bpl.payFromAddress = true ");
      whereClause.append(" and jl.currency.id = c.id");
      whereClause.append(" and jl.journalEntry.accountingDate < ? ");
      whereClause.append(" and jl.journalEntry.accountingDate >= ? ");

      // VAT Purchase or WH Sales
      if ((!isSales && !isWithHoldingTax) || (isSales && isWithHoldingTax)) {
        sql_e
            .append(" ,(case jl.foreignCurrencyCredit when 0 then jl.foreignCurrencyDebit else coalesce(jl.foreignCurrencyCredit*(-1),0) end) as taxAmount ");
        sql_e.append(" ,c.id , jl.journalEntry.documentType.id ");
        sql_e
            .append(" ,(case jl.foreignCurrencyCredit when 0 then jl.foreignCurrencyDebit else coalesce(jl.foreignCurrencyCredit*(-1),0) end)/((case jl.tax.rate when 0 then 1 else jl.tax.rate end)/100) as taxableAmount ");
        whereClause.append(" and ( jl.foreignCurrencyCredit < 0 or  jl.foreignCurrencyDebit > 0) ");
      }
      // VAT Sales or WH Purchase
      if ((isSales && !isWithHoldingTax) || (!isSales && isWithHoldingTax)) {
        sql_e
            .append(" ,(case jl.foreignCurrencyCredit when 0 then coalesce(jl.foreignCurrencyDebit*(-1),0) else jl.foreignCurrencyCredit end) as taxAmount ");
        sql_e.append(" ,c.id , jl.journalEntry.documentType.id ");
        sql_e
            .append(" ,(case jl.foreignCurrencyCredit when 0 then coalesce(jl.foreignCurrencyDebit*(-1),0) else jl.foreignCurrencyCredit end)/((case jl.tax.rate when 0 then 1 else jl.tax.rate end)/100) as taxableAmount ");
        whereClause.append(" and ( jl.foreignCurrencyCredit > 0 or  jl.foreignCurrencyDebit < 0) ");
      }

      sql_e.append(" ,jl.journalEntry.totalCreditAmount as totalDoc ");
      sql_e.append(" , jl.tax.id");
      sql_e.append(" , jl.tax.taxCategory.id");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e.append(" from FinancialMgmtGLJournalLine as jl, Currency as c ");
      if (isWithHoldingTax) {
        whereClause.append(" and jl.tax.withholdingTax = true ");
        if (!"".equals(cTaxWithHoldingId)) {
          whereClause.append(" and jl.tax.id = '" + cTaxWithHoldingId + "'");
        }

      } else {
        whereClause.append(" and jl.tax.withholdingTax = false ");
        if (!"".equals(cTaxID)) {
          whereClause.append(" and jl.tax.id = '" + cTaxID + "'");
        }
      }
      // FIXME: IN CLAUSE with more than 1000 values
      if (!"".equals(strBps)) {
        whereClause.append(" and jl.accountingCombination.businessPartner.id in " + strBps);
      }
      // whereClause.append(" and bpl.payFromAddress = true");
      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and jl.journalEntry.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return scrollResults(query_e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getMSettlementsPaymentDirectPosting(final Date fromDate,
      final Date toDate, final String strBps, final String cWithHoldingID) {
    try {
      OBContext.setAdminMode(true);

      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(paym.settlementGenerate.documentNo, bp.id, (select min(bpl.id) from BusinessPartnerLocation bpl where bpl.businessPartner.id=bp.id),paym.settlementGenerate.transactionDate");
      sql_e
          .append(" ,paym.settlementGenerate.accountingDate,dpb.gLItem.withholding.name,dpb.gLItem.withholding.rate");
      sql_e
          .append(" ,(case dpb.creditAmount when 0 then dpb.debitAmount else coalesce(dpb.creditAmount*(-1),0) end) as taxAmount");
      sql_e.append(" ,c.id ,paym.settlementGenerate.documentType.id");
      sql_e
          .append(" ,(case dpb.debitAmount when 0 then dpb.creditAmount else coalesce(dpb.debitAmount*(-1),0) end)/((case dpb.gLItem.withholding.rate when 0 then 1 else dpb.gLItem.withholding.rate end)*(-1)/100) as taxableAmount");
      sql_e.append(" ,paym.amount,dpb.gLItem.withholding.id,'-1' as taxCategory");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e
          .append(" from FinancialMgmtDebtPaymentBalancing dpb join dpb.payment paym join paym.businessPartner bp, Currency c");
      whereClause
          .append(" where dpb.payment.settlementGenerate.posted='Y'  and dpb.payment.settlementGenerate.active ='Y'  and dpb.payment.settlementGenerate.processed = 'Y' ");
      // whereClause.append(" and dpb.payment.paymentComplete= true");
      whereClause
          .append(" and c.id = paym.currency.id and (dpb.creditAmount > 0 or dpb.debitAmount < 0)");
      whereClause.append(" and paym.settlementGenerate.accountingDate < ? ");
      whereClause.append(" and paym.settlementGenerate.accountingDate >= ? ");
      whereClause.append(" and paym.directPosting = true");
      whereClause.append("  ");
      // FIXME: IN CLAUSE with more than 1000 values
      if (!"".equals(strBps)) {
        whereClause.append(" and bp.id in " + strBps);
      }
      if (!"".equals(cWithHoldingID)) {
        whereClause.append(" and dpb.gLItem.withholding.id = '" + cWithHoldingID + "'");
      }
      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and paym.settlementGenerate.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return query_e.list();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getMSettlementsPayment(final Date fromDate, final Date toDate,
      final String strBps, final String cWithHoldingID) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(paym.settlementGenerate.documentNo, bp.id, (select min(bpl.id) from BusinessPartnerLocation bpl where bpl.businessPartner.id=bp.id), paym.settlementGenerate.transactionDate");
      sql_e
          .append(" , paym.settlementCancelled.accountingDate, dpb.gLItem.withholding.name, dpb.gLItem.withholding.rate");
      sql_e
          .append(" ,(case dpb.creditAmount when 0 then dpb.debitAmount else coalesce(dpb.creditAmount*(-1),0) end) as taxAmount");
      sql_e.append(" ,c.id , paym.settlementGenerate.documentType.id");
      sql_e
          .append(" ,(case dpb.creditAmount when 0 then dpb.debitAmount else coalesce(dpb.creditAmount*(-1),0) end)/((case dpb.gLItem.withholding.rate when 0 then 1 else dpb.gLItem.withholding.rate end)*(-1)/100) as taxableAmount");
      sql_e.append(" , paym.amount,dpb.gLItem.withholding.id,'-1' as taxCategory");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e
          .append(" from FinancialMgmtDebtPaymentBalancing dpb join dpb.payment paym join paym.businessPartner bp,  Currency c");
      whereClause
          .append(" where paym.settlementCancelled.posted='Y'  and paym.settlementCancelled.active ='Y'  and paym.settlementCancelled.processed = 'Y' ");
      whereClause.append(" and paym.paymentComplete= true");
      whereClause
          .append(" and c.id = paym.currency.id and (dpb.creditAmount > 0 or dpb.debitAmount < 0)");
      whereClause.append(" and paym.settlementCancelled.accountingDate < ? ");
      whereClause.append(" and paym.settlementCancelled.accountingDate >= ? ");
      whereClause.append(" and paym.directPosting = false");
      // FIXME: IN CLAUSE with more than 1000 values
      if (!"".equals(strBps)) {
        whereClause.append(" and bp.id in " + strBps);
      }
      if (!"".equals(cWithHoldingID)) {
        whereClause.append(" and dpb.gLItem.withholding.id = '" + cWithHoldingID + "'");
      }
      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and paym.settlementCancelled.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return query_e.list();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getInvoicesPayment(final Date fromDate, final Date toDate,
      final String strBps, final String cWithHoldingID) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();

      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(dp.invoice.documentNo, dp.invoice.businessPartner.id, dp.invoice.partnerAddress.id ,dp.invoice.invoiceDate ");
      sql_e
          .append(" ,dp.settlementCancelled.accountingDate ,dp.withholding.name ,dp.withholding.rate ,coalesce(dp.withholdingamount*(-1), 0),c.id");
      sql_e
          .append(" ,dp.invoice.documentType.id, (case dp.invoice.documentType.documentCategory when 'APC' then coalesce(dp.invoice.summedLineAmount*(-1),0) else dp.invoice.summedLineAmount end) ");
      sql_e
          .append(" ,(case dp.invoice.documentType.documentCategory when 'APC' then coalesce((dp.invoice.grandTotalAmount - dp.invoice.withholdingamount) * (-1),0) else (dp.invoice.grandTotalAmount - dp.invoice.withholdingamount) end)");
      sql_e.append(" ,dp.withholding.id, '-1' as taxCategory");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e.append(" from FinancialMgmtDebtPayment dp, Currency c ");
      whereClause.append(" where dp.settlementCancelled.posted='Y' ");
      whereClause.append(" and dp.settlementCancelled.active ='Y' ");
      whereClause.append(" and dp.settlementCancelled.processed = 'Y' ");
      whereClause.append(" and dp.paymentComplete= true ");
      whereClause.append(" and c.id = dp.currency.id ");
      whereClause.append(" and dp.invoice.salesTransaction = false ");
      whereClause.append(" and dp.settlementCancelled.accountingDate < ? ");
      whereClause.append(" and dp.settlementCancelled.accountingDate >= ? ");
      // FIXME: IN CLAUSE with more than 1000 values
      if (!"".equals(strBps)) {
        whereClause.append(" and dp.businessPartner.id in " + strBps);
      }
      if (!"".equals(cWithHoldingID)) {
        whereClause.append(" and dp.withholding.id = '" + cWithHoldingID + "'");
      }
      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and dp.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);
      return query_e.list();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getSettlementsTransaction(final boolean isSales,
      final boolean isWithHoldingTax, final Date fromDate, final Date toDate, final String strBps,
      final String cTaxID, final String cTaxWithHoldingId, final boolean isDirectPosting) {
    try {
      OBContext.setAdminMode(true);
      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();
      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(s.documentNo, dp.businessPartner.id, (select min(bpl.id) from BusinessPartnerLocation bpl where bpl.businessPartner.id=dp.businessPartner.id), s.transactionDate, s.accountingDate, ");
      sql_e.append(" gli.tax.name,  gli.tax.rate, ");

      if (isDirectPosting) {
        whereClause.append(" where s.id = dp.settlementGenerate ");
        // If we uncomment the following line, the Manual Settlement does not appear when it goes to
        // a posted bank statement line
        // whereClause.append(" and dp.settlementCancelled is null ");
      } else {
        whereClause.append(" where s.id = dp.settlementCancelled ");
      }

      whereClause.append(" and dp.id = dpb.payment ");
      whereClause.append(" and gli.id = dpb.gLItem");
      whereClause.append(" and s.active = 'Y' ");
      whereClause.append(" and s.processed = 'Y' ");
      whereClause.append(" and s.posted = 'Y' ");
      whereClause.append(" and s.accountingDate < ? ");
      whereClause.append(" and s.accountingDate >= ? ");

      // VAT Sales or WH Purchase
      if ((isSales && !isWithHoldingTax) || (!isSales && isWithHoldingTax)) {
        whereClause.append(" and (dpb.creditAmount > 0 or dpb.debitAmount < 0) ");
      }
      // VAT Purchase or WH Sales
      else if ((!isSales && !isWithHoldingTax) || (isSales && isWithHoldingTax)) {
        whereClause.append(" and (dpb.creditAmount < 0 or dpb.debitAmount > 0) ");
      }

      final String firstCase = "(case dpb.creditAmount when 0 then dpb.debitAmount else coalesce(dpb.creditAmount*(-1),0) end)";
      sql_e.append(" (case dp.receipt when 'Y' then coalesce((-1)*" + firstCase + ", 0) else "
          + firstCase + " end) as taxAmount, ");
      sql_e.append(" dp.currency.id, s.documentType.id, ");

      final String secondCase = firstCase
          + "/((case gli.tax.rate when 0 then 1 else gli.tax.rate end)/100)";
      sql_e.append(" (case dp.receipt when 'Y' then coalesce((-1)*(" + secondCase + "), 0) else ("
          + secondCase + ") end) as taxableAmount, ");

      whereClause.append(" and dp.directPosting = " + isDirectPosting);
      sql_e.append(" dp.amount as totalDoc ");
      sql_e.append(" , gli.tax.id");
      sql_e.append(" , gli.tax.taxCategory.id");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("') ");
      sql_e
          .append(" from FinancialMgmtSettlement as s, FinancialMgmtDebtPayment as dp, FinancialMgmtDebtPaymentBalancing dpb, ");
      sql_e.append(" FinancialMgmtGLItem gli");
      if (isWithHoldingTax) {
        whereClause.append(" and gli.tax.withholdingTax = true ");
        if (!"".equals(cTaxWithHoldingId)) {
          whereClause.append(" and gli.tax.id = '" + cTaxWithHoldingId + "'");
        }

      } else {
        whereClause.append(" and gli.tax.withholdingTax = false ");
        if (!"".equals(cTaxID)) {
          whereClause.append(" and gli.tax.id = '" + cTaxID + "'");
        }
      }
      // FIXME: IN CLAUSE with more than 1000 values
      if (!"".equals(strBps)) {
        whereClause.append(" and dp.businessPartner.id in " + strBps);
      }

      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and s.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);

      return scrollResults(query_e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  @SuppressWarnings("unchecked")
  public Collection<MTRRecord> getInvoices(final boolean isSales, final boolean isWithHoldingTax,
      final Date fromDate, final Date toDate, final String strBps, final String cTaxID,
      final String cTaxWithHoldingId) {
    try {
      OBContext.setAdminMode(true);

      final StringBuilder sql_e = new StringBuilder();
      final StringBuilder whereClause = new StringBuilder();
      sql_e
          .append(" select new org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord(inv.documentNo as DocNo, ");
      sql_e
          .append(
              " inv.businessPartner.id, ad_column_identifier('C_BPartner', coalesce(inv.businessPartner.id, '-1'), '")
          .append(lang).append("') as bpIdentifier  ");
      sql_e
          .append(
              " , inv.businessPartner.name2, inv.businessPartner.taxID, coalesce(obmtr30_getcountryname(inv.partnerAddress.id, '")
          .append(lang).append("'), '-1') ");
      sql_e.append(" , reg.name as region ");
      sql_e.append(", inv.invoiceDate, inv.accountingDate, ");
      sql_e.append(" it.tax.name,  it.tax.rate ");

      final String docType;
      if (isSales)
        docType = "'ARC'";
      else
        docType = "'APC'";

      sql_e.append(" ,(case inv.documentType.documentCategory when " + docType
          + " then coalesce(it.taxAmount*(-1),0) else it.taxAmount end) as taxAmount ");
      sql_e.append(" , inv.currency.id, inv.currency.symbol, inv.documentType.id ");
      sql_e.append(" ,(case inv.documentType.documentCategory when " + docType
          + " then coalesce(it.taxableAmount*(-1),0) else it.taxableAmount end) as taxableAmount ");
      sql_e
          .append(" ,(case inv.documentType.documentCategory when "
              + docType
              + " then coalesce(inv.grandTotalAmount*(-1) + coalesce(inv.withholdingamount,0), 0) else coalesce((inv.grandTotalAmount) - coalesce(inv.withholdingamount, 0), 0)  end) as totaldoc ");

      sql_e.append(" , it.tax.id");
      sql_e.append(" , coalesce(it.tax.taxCategory.id, '-1') ");
      sql_e.append(" , it.tax.taxCategory.name ");
      sql_e.append(", '").append(userCurrency).append("' ");
      sql_e.append(", '").append(lang).append("' ");
      sql_e.append(", '").append(dateFormat).append("' ");
      sql_e.append(" , crd.rate) ");
      sql_e.append(" from InvoiceTax as it");
      sql_e.append(" left join it.invoice as inv ");
      sql_e.append(" left join inv.currencyConversionRateDocList as crd ");
      sql_e.append(" left join inv.partnerAddress.locationAddress.region as reg ");
      whereClause.append(" where inv.salesTransaction = " + isSales);
      whereClause.append(" and inv.accountingDate < ? ");
      whereClause.append(" and inv.accountingDate >= ? ");
      whereClause.append(" and it.taxableAmount <> 0 ");
      whereClause.append(" and inv.active = 'Y' ");
      whereClause.append(" and inv.processed = 'Y' ");
      whereClause.append(" and inv.posted = 'Y' ");
      whereClause.append(" and coalesce(crd.currency.id, inv.currency.id) = inv.currency.id ");
      whereClause.append(" and (coalesce(crd.toCurrency.id, '").append(userCurrency)
          .append("') = '").append(userCurrency).append("' or inv.currency.id = '")
          .append(userCurrency).append("') ");
      // FIXME: IN CLAUSE with more than 1000 values
      if (!"".equals(strBps)) {
        whereClause.append(" and inv.businessPartner.id in " + strBps);
      }

      if (isWithHoldingTax) {
        whereClause.append(" and it.tax.withholdingTax = true ");
        if (!"".equals(cTaxWithHoldingId)) {
          whereClause.append(" and it.tax.id = '" + cTaxWithHoldingId + "'");
        }

      } else {
        whereClause.append(" and it.tax.withholdingTax = false ");
        if (!"".equals(cTaxID)) {
          whereClause.append(" and it.tax.id = '" + cTaxID + "'");
        }
      }
      final OrganizationStructureProvider osp = OBContext.getOBContext()
          .getOrganizationStructureProvider(org.getClient().getId());
      whereClause.append(" and inv.organization in ("
          + this.createINClauseForOrgs(osp.getChildTree(org.getId(), true)) + ") ");

      final Session session_e = OBDal.getInstance().getSession();
      final Query query_e = session_e.createQuery(sql_e.toString() + whereClause.toString());
      query_e.setParameter(0, toDate);
      query_e.setParameter(1, fromDate);

      return scrollResults(query_e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Return all the Purchase data ordered by Tax name and Business Partner
   * 
   * @return
   * @throws ServletException
   */
  public Collection<MTRRecord> mergeDataDetailPurchase() throws ServletException {
    try {
      OBContext.setAdminMode(true);

      final List<MTRRecord> allData = new ArrayList<MTRRecord>();

      final boolean isSales = false;

      if (this.isWithHolding) {
        // Search on payment and transactions
        if ("".equals(this.withHoldingAsTaxId) && "".equals(this.cWithHoldingId)) {
          // Transactions
          final Collection<MTRRecord> transactions = this.getTransactions(isSales);

          // Payments
          final Collection<MTRRecord> payments = this.getPayments(isSales);

          allData.addAll(transactions);
          allData.addAll(payments);
        } else {
          // Search on transactions
          if (!"".equals(withHoldingAsTaxId) && "".equals(cWithHoldingId)) {
            final Collection<MTRRecord> transactions = this.getTransactions(isSales);
            allData.addAll(transactions);
          }
          // Search on payments
          if ("".equals(withHoldingAsTaxId) && !"".equals(cWithHoldingId)) {
            final Collection<MTRRecord> payments = this.getPayments(isSales);
            allData.addAll(payments);
          }

        }
      } else {
        // Search on transaction because is VAT tax
        final Collection<MTRRecord> transactions = this.getTransactions(isSales);
        allData.addAll(transactions);
      }

      // sort results by TaxName
      Collections.sort(allData, new OrderByTaxNameAndBP());
      return allData;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Return all the Sales data ordered by Tax name and Business Partner
   * 
   * @return
   * @throws ServletException
   */
  public Collection<MTRRecord> mergeDataDetailSales() throws ServletException {

    try {
      OBContext.setAdminMode(true);
      final boolean isSales = true;
      final List<MTRRecord> allData = new ArrayList<MTRRecord>(this.getTransactions(isSales));

      // sort results
      Collections.sort(allData, new OrderByTaxNameAndBP());
      return allData;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private Collection<MTRRecord> getTransactions(final boolean isSales) {
    final Collection<MTRRecord> allData = new ArrayList<MTRRecord>();

    final Collection<MTRRecord> invoices = getInvoices(isSales, isWithHolding, dateFrom, dateTo,
        businessPartners, cTaxId, withHoldingAsTaxId);
    final Collection<MTRRecord> mSettlementsTrx_NoDirectPost = getSettlementsTransaction(isSales,
        isWithHolding, dateFrom, dateTo, businessPartners, cTaxId, withHoldingAsTaxId, false);
    final Collection<MTRRecord> mSettlementsTrx_DirectPost = getSettlementsTransaction(isSales,
        isWithHolding, dateFrom, dateTo, businessPartners, cTaxId, withHoldingAsTaxId, true);
    final Collection<MTRRecord> glJournalsTrx = getGLJournalsTransaction(isSales, isWithHolding,
        dateFrom, dateTo, businessPartners, cTaxId, withHoldingAsTaxId);
    final Collection<MTRRecord> bankStatementsTrx = getBankStatementsTrx(dateFrom, dateTo,
        withHoldingAsTaxId, cTaxId, isWithHolding, isSales);
    final Collection<MTRRecord> cashJournalsTrx = getCashTrx(dateFrom, dateTo, withHoldingAsTaxId,
        cTaxId, isWithHolding, isSales);

    allData.addAll(invoices);
    allData.addAll(mSettlementsTrx_NoDirectPost);
    allData.addAll(mSettlementsTrx_DirectPost);
    allData.addAll(glJournalsTrx);
    allData.addAll(bankStatementsTrx);
    allData.addAll(cashJournalsTrx);

    return allData;
  }

  private Collection<MTRRecord> getPayments(final boolean isSales) {
    final Collection<MTRRecord> allData = new ArrayList<MTRRecord>();

    final Collection<MTRRecord> invoicesPayment = getInvoicesPayment(dateFrom, dateTo,
        businessPartners, cWithHoldingId);
    final Collection<MTRRecord> mSettlementsPayment = getMSettlementsPayment(dateFrom, dateTo,
        businessPartners, cWithHoldingId);
    final Collection<MTRRecord> glJournalsPayment = getGLJournalsPayment(dateFrom, dateTo,
        cWithHoldingId);
    final Collection<MTRRecord> mSettlementsPaymentDP = getMSettlementsPaymentDirectPosting(
        dateFrom, dateTo, businessPartners, cWithHoldingId);
    final Collection<MTRRecord> bankStatementsPayment = getBankStatementsPayment(dateFrom, dateTo,
        cWithHoldingId, isSales);
    final Collection<MTRRecord> cashJournalsPayment = getCashPayment(dateFrom, dateTo,
        cWithHoldingId, isSales);

    allData.addAll(invoicesPayment);
    allData.addAll(mSettlementsPayment);
    allData.addAll(glJournalsPayment);
    allData.addAll(mSettlementsPaymentDP);
    allData.addAll(bankStatementsPayment);
    allData.addAll(cashJournalsPayment);

    return allData;
  }

  /**
   * Creates the expected output for Hibernate IN clause used for Identifiers (id) for the given
   * Collection of Organization's IDs
   * 
   * @param organizations
   * @return
   */
  private String createINClauseForOrgs(final Collection<String> organizations) {
    final StringBuffer sb = new StringBuffer();
    for (final String s : organizations) {
      sb.append("'" + s + "',");
    }
    if (sb == null || sb.length() == 0)
      return "''";
    else
      return sb.substring(0, sb.length() - 1).toString();
  }

  private ArrayList<MTRRecord> scrollResults(Query query_e) {
    final ScrollableResults scroller = query_e.scroll(ScrollMode.FORWARD_ONLY);
    ArrayList<MTRRecord> resultsCollection = new ArrayList<MTRRecord>();
    int i = 0;
    try {
      while (scroller.next()) {
        MTRRecord mTRRecord = (MTRRecord) scroller.get()[0];
        resultsCollection.add(mTRRecord);
        if ((i % 25) == 0) {
          OBDal.getInstance().getSession().clear();
        }
        i++;
      }
    } finally {
      scroller.close();
    }
    return resultsCollection;
  }
}