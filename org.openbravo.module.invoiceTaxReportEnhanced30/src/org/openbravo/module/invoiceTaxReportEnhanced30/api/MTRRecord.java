/************************************************************************************ 
 * Copyright (C) 2010 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 ************************************************************************************/
package org.openbravo.module.invoiceTaxReportEnhanced30.api;

import java.math.BigDecimal;
import java.util.Date;

import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.financialmgmt.tax.TaxCategory;
import org.openbravo.module.invoiceTaxReportEnhanced30.utility.Utility;
import org.openbravo.service.db.DalConnectionProvider;

/**
 * Class that represents a record (line) of the MultiDimensional Tax Report. Each line can
 * correspond to different documents, like invoices, settlements, etc. But, independently of its
 * origin, they must have some attributes in common which are defined here.
 * 
 * This class is FieldProvider compatible, so it defines public methods which return String for all
 * its attributes. This is a requirement if we want to pass instances of this class to the Jasper
 * Report.
 * 
 * @author openbravo
 * 
 */
public class MTRRecord {
  private boolean calculatedAmts = false;

  private final String documentNo;
  private final String businessPartner;
  private final String businessPartner2;
  private final String bpCountry;
  private final String bpRegion;
  private final String taxName;
  private final BigDecimal rate;
  private final String docType;
  private final Date docDate;
  private final Date acctDate;
  private final String symbol;
  private final String taxId;
  private final String taxCategoryId;
  private final String taxCategoryName;
  private final String businessPartnerId;
  private final String businessPartnerTaxId;
  private final BigDecimal taxBaseAmt;
  private final BigDecimal totalAmt;
  private final BigDecimal taxAmt;

  private final String userCurrency;
  private final String lang;
  private final String userDateFormat;
  private final MTRRecordDao dao;

  private final BigDecimal convRate;

  public MTRRecord(final String documentNo, final String businessPartnerID, final String location,
      final Date docDate, final Date acctDate, final String taxName, final BigDecimal taxRate,
      final BigDecimal taxAmt, final String currencyId, final String docTypeId,
      final BigDecimal taxableAmt, final BigDecimal totalDocAmt, final String taxOrWithholdingId,
      final String taxCategory, final String userCurrency, final String lang,
      final String dateFormat) {
    this(documentNo, businessPartnerID, location, docDate, acctDate, taxName, taxRate, taxAmt,
        currencyId, docTypeId, taxableAmt, totalDocAmt, taxOrWithholdingId, taxCategory,
        userCurrency, lang, dateFormat, new BigDecimal(-1));
  }

  /**
   * Creates a Multidimensional Tax Report record (line) with the following parameters
   * 
   * @param documentNo
   *          Number of Document
   * @param businessPartnerID
   *          ID of the document's Business Partner
   * @param businessPartnerTaxID
   *          Tax ID of the Business Partner
   * @param location
   *          Business Partner location
   * @param docDate
   *          Document Date
   * @param acctDate
   *          Accounting Date
   * @param taxName
   *          Tax Name
   * @param taxRate
   *          Tax Rate (%)
   * @param taxAmt
   *          Tax Amount
   * @param currencyId
   *          ID of the Currency
   * @param docTypeId
   *          ID of the Document Type
   * @param taxableAmt
   *          Tax Base Amount
   * @param totalDocAmt
   *          Total Document Amount
   * @param taxOrWithholdingId
   *          Tax/Witholding ID
   * @param taxCategory
   *          Tax Category
   * @param userCurrency
   *          User defined Currency
   * @param lang
   *          Language to display results (useful for getting messages from the database in the
   *          right language)
   * @param dateFormat
   *          Defines the date format to be used in the report
   */
  public MTRRecord(final String documentNo, final String businessPartnerID, final String location,
      final Date docDate, final Date acctDate, final String taxName, final BigDecimal taxRate,
      final BigDecimal taxAmt, final String currencyId, final String docTypeId,
      final BigDecimal taxableAmt, final BigDecimal totalDocAmt, final String taxOrWithholdingId,
      final String taxCategory, final String userCurrency, final String lang,
      final String dateFormat, final BigDecimal convRate) {
    dao = new MTRRecordDao();
    this.documentNo = documentNo;
    this.taxName = taxName;
    this.rate = taxRate;
    this.docDate = docDate;
    this.acctDate = acctDate;
    this.taxId = taxOrWithholdingId;

    final Currency cur = dao.getCurrency(currencyId);
    this.symbol = cur.getSymbol();

    if (currencyId.equalsIgnoreCase(userCurrency)) {
      this.taxBaseAmt = taxableAmt;
      this.totalAmt = totalDocAmt;
      this.taxAmt = taxAmt;
    } else {
      this.taxBaseAmt = Utility.convertCurrency(taxableAmt, cur.getId(), userCurrency, acctDate,
          convRate);
      this.totalAmt = Utility.convertCurrency(totalDocAmt, cur.getId(), userCurrency, acctDate,
          convRate);
      this.taxAmt = Utility.convertCurrency(taxAmt, cur.getId(), userCurrency, acctDate, convRate);
    }

    this.userCurrency = userCurrency;
    this.lang = lang;
    this.userDateFormat = dateFormat;

    this.convRate = convRate;

    if (businessPartnerID.equals("-1")) {
      businessPartner = "#";
      businessPartner2 = "#";
      businessPartnerId = "#";
      businessPartnerTaxId = "#";
    } else {
      final BusinessPartner bp = dao.getBusinessPartner(businessPartnerID);
      businessPartner = bp.getIdentifier();
      businessPartner2 = !"".equals(bp.getName2()) ? bp.getName2() : "";
      businessPartnerId = businessPartnerID;
      businessPartnerTaxId = bp.getTaxID();
    }

    if (location.equals("-1")) {
      bpCountry = " ";
      bpRegion = " ";
    } else {
      bpCountry = dao.getCountryName(location, this.lang);
      bpRegion = dao.getRegionName(location);
    }

    if (taxCategory.equals("-1")) {
      taxCategoryId = "#";
      taxCategoryName = "Withholding";
    } else {
      final TaxCategory tc = dao.getTaxCategory(taxCategory);
      taxCategoryId = taxCategory;
      taxCategoryName = tc.getIdentifier();
    }

    if (docTypeId.equals("B")) {
      final StringBuffer docTypeSb = new StringBuffer("* ");
      docTypeSb.append(org.openbravo.erpCommon.utility.Utility.messageBD(
          new DalConnectionProvider(), "Bank Statement", lang));
      docType = docTypeSb.toString();
      calculatedAmts = true;
    } else if (docTypeId.equals("C")) {
      final StringBuffer docTypeSb = new StringBuffer("* ");
      docTypeSb.append(org.openbravo.erpCommon.utility.Utility.messageBD(
          new DalConnectionProvider(), "Cash Journal", lang));
      docType = docTypeSb.toString();
      calculatedAmts = true;
    } else {
      final DocumentType dt = dao.getDocumentType(docTypeId);
      final String strDocumentType = dao.getDocumentTypeName(docTypeId, lang);
      if (dt.getDocumentCategory().equals("GLJ") || dt.getDocumentCategory().equals("STM")) {
        docType = "* " + strDocumentType;
        calculatedAmts = true;
      } else {
        docType = strDocumentType;
      }
    }
  }

  public MTRRecord(final String documentNo, final String businessPartnerID,
      final String bpIdentifier, final String businessPartnerName2,
      final String businessPartnerTaxID, final String location, final String region,
      final Date docDate, final Date acctDate, final String taxName, final BigDecimal taxRate,
      final BigDecimal taxAmt, final String currencyId, final String curSymbol,
      final String docTypeId, final BigDecimal taxableAmt, final BigDecimal totalDocAmt,
      final String taxOrWithholdingId, final String taxCategory, final String taxCatName,
      final String userCurrency, final String lang, final String dateFormat,
      final BigDecimal convRate) {
    dao = new MTRRecordDao();
    this.documentNo = documentNo;
    this.taxName = taxName;
    this.rate = taxRate;
    this.docDate = docDate;
    this.acctDate = acctDate;
    this.taxId = taxOrWithholdingId;

    this.symbol = curSymbol;

    if (currencyId.equalsIgnoreCase(userCurrency)) {
      this.taxBaseAmt = taxableAmt;
      this.totalAmt = totalDocAmt;
      this.taxAmt = taxAmt;
    } else {
      this.taxBaseAmt = Utility.convertCurrency(taxableAmt, currencyId, userCurrency, acctDate,
          convRate);
      this.totalAmt = Utility.convertCurrency(totalDocAmt, currencyId, userCurrency, acctDate,
          convRate);
      this.taxAmt = Utility.convertCurrency(taxAmt, currencyId, userCurrency, acctDate, convRate);
    }

    this.userCurrency = userCurrency;
    this.lang = lang;
    this.userDateFormat = dateFormat;

    this.convRate = convRate;

    if (businessPartnerID.equals("-1")) {
      businessPartner = "#";
      businessPartner2 = "#";
      businessPartnerId = "#";
      businessPartnerTaxId = "#";
    } else {
      businessPartner = bpIdentifier;
      businessPartner2 = businessPartnerName2;
      businessPartnerId = businessPartnerID;
      businessPartnerTaxId = businessPartnerTaxID;
    }

    if (location.equals("-1")) {
      bpCountry = " ";
      bpRegion = " ";
    } else {
      bpCountry = location;
      bpRegion = region;
    }

    if (taxCategory.equals("-1")) {
      taxCategoryId = "#";
      taxCategoryName = "Withholding";
    } else {
      taxCategoryId = taxCategory;
      taxCategoryName = taxCatName;
    }

    if (docTypeId.equals("B")) {
      final StringBuffer docTypeSb = new StringBuffer("* ");
      docTypeSb.append(org.openbravo.erpCommon.utility.Utility.messageBD(
          new DalConnectionProvider(), "Bank Statement", lang));
      docType = docTypeSb.toString();
      calculatedAmts = true;
    } else if (docTypeId.equals("C")) {
      final StringBuffer docTypeSb = new StringBuffer("* ");
      docTypeSb.append(org.openbravo.erpCommon.utility.Utility.messageBD(
          new DalConnectionProvider(), "Cash Journal", lang));
      docType = docTypeSb.toString();
      calculatedAmts = true;
    } else {
      final DocumentType dt = dao.getDocumentType(docTypeId);
      final String strDocumentType = dao.getDocumentTypeName(docTypeId, lang);
      if (dt.getDocumentCategory().equals("GLJ") || dt.getDocumentCategory().equals("STM")) {
        docType = "* " + strDocumentType;
        calculatedAmts = true;
      } else {
        docType = strDocumentType;
      }
    }
  }

  public Boolean hasCalculatedAmts() {
    return calculatedAmts;
  }

  public String getCalculatedAmts() {
    return Utility.convertToString(calculatedAmts);
  }

  /**
   * @return the documentNo
   */
  public String getDocumentNo() {
    return documentNo;
  }

  /**
   * @return the businessPartner
   */
  public String getBusinessPartner() {
    return businessPartner;
  }

  /**
   * @return the businessPartner
   */
  public String getBusinessPartner2() {
    return businessPartner2;
  }

  /**
   * @return the businessPartnerTaxId
   */
  public String getBusinessPartnerTaxId() {
    return businessPartnerTaxId;
  }

  /**
   * @return the bpCountry
   */
  public String getBpCountry() {
    return bpCountry;
  }

  /**
   * @return the bpRegion
   */
  public String getBpRegion() {
    return bpRegion;
  }

  /**
   * @return the taxName
   */
  public String getTaxName() {
    return taxName;
  }

  /**
   * @return the rate
   */
  public String getRate() {
    return rate.setScale(2).toPlainString();
  }

  /**
   * @return the docType
   */
  public String getDocType() {
    return docType;
  }

  /**
   * @return the docDate
   */
  public String getDocDate() {
    return org.openbravo.erpCommon.utility.Utility.formatDate(docDate, userDateFormat);
  }

  /**
   * @return the acctDate
   */
  public String getAcctDate() {
    return org.openbravo.erpCommon.utility.Utility.formatDate(acctDate, userDateFormat);
  }

  /**
   * @return the symbol
   */
  public String getSymbol() {
    return symbol;
  }

  /**
   * @return the taxId
   */
  public String getTaxId() {
    return taxId;
  }

  /**
   * @return the taxCategoryId
   */
  public String getTaxCategoryId() {
    return taxCategoryId;
  }

  /**
   * @return the taxCategoryName
   */
  public String getTaxCategoryName() {
    return taxCategoryName;
  }

  /**
   * @return the businessPartnerId
   */
  public String getBusinessPartnerId() {
    return businessPartnerId;
  }

  /**
   * @return the taxBaseAmt
   */
  public String getTaxBaseAmt() {
    return taxBaseAmt.toPlainString();
  }

  /**
   * @return the totalAmt
   */
  public String getTotalAmt() {
    return totalAmt.toPlainString();
  }

  /**
   * @return the taxAmt
   */
  public String getTaxAmt() {
    return taxAmt.toPlainString();
  }

  /**
   * @return the userCurrency
   */
  public String getUserCurrency() {
    return userCurrency;
  }

  /**
   * @return the lang
   */
  public String getLang() {
    return lang;
  }

  /**
   * @return the convRate
   */
  public BigDecimal getConvRate() {
    return convRate;
  }

}
