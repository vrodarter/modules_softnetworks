package org.openbravo.module.invoiceTaxReportEnhanced30.api;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.DocumentTypeTrl;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.geography.CountryTrl;
import org.openbravo.model.common.geography.Region;
import org.openbravo.model.financialmgmt.tax.TaxCategory;

public class MTRRecordDao {

  /**
   * Returns country name in the given language
   * 
   * @param locationId
   * @param lang
   * @return
   */
  public String getCountryName(final String locationId, String lang) {
    if (lang == null || lang.equals(""))
      lang = "en_US";

    try {
      OBContext.setAdminMode(true);
      final Location loc = this.getLocation(locationId);
      final Country country = loc.getLocationAddress().getCountry();

      if (!"en_US".equals(lang)) {
        for (final CountryTrl c : country.getCountryTrlList()) {
          if (lang.equals(c.getLanguage().getLanguage()))
            return c.getName();
        }
      }
      return country.getName();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the Region Name for the given location
   * 
   * @param locationId
   * @return
   */
  public String getRegionName(final String locationId) {
    try {
      OBContext.setAdminMode(true);
      Region r = getLocation(locationId).getLocationAddress().getRegion();
      if (r == null)
        return "";
      else
        return r.getName();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the Location with the given location ID
   * 
   * @param locationId
   * @return
   */
  public Location getLocation(final String locationId) {
    try {
      OBContext.setAdminMode(true);
      return OBDal.getInstance().get(Location.class, locationId);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the TaxCategory with the given taxCategory ID
   * 
   * @param taxCategoryId
   * @return
   */
  public TaxCategory getTaxCategory(final String taxCategoryId) {
    try {
      OBContext.setAdminMode(true);
      return OBDal.getInstance().get(TaxCategory.class, taxCategoryId);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the Currency with the given currency ID
   * 
   * @param currencyId
   * @return
   */
  public Currency getCurrency(final String currencyId) {
    try {
      OBContext.setAdminMode(true);
      return OBDal.getInstance().get(Currency.class, currencyId);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the Business Partner with the given currency ID
   * 
   * @param businessPartnerId
   * @return
   */
  public BusinessPartner getBusinessPartner(final String businessPartnerId) {
    try {
      OBContext.setAdminMode(true);
      return OBDal.getInstance().get(BusinessPartner.class, businessPartnerId);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the DocumentType with the given documentType ID
   * 
   * @param docTypeId
   * @param lang
   * @return
   */
  public String getDocumentTypeName(final String docTypeId, String lang) {
    if (lang == null || lang.equals(""))
      lang = "en_US";

    try {
      OBContext.setAdminMode(true);
      final DocumentType docType = this.getDocumentType(docTypeId);
      if (!"en_US".equals(lang)) {
        for (final DocumentTypeTrl d : docType.getDocumentTypeTrlList()) {
          if (lang.equals(d.getLanguage().getLanguage()))
            return d.getIdentifier();
        }
      }
      return docType.getIdentifier();
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * Returns the DocumentType with the given documentType ID
   * 
   * @param docTypeId
   * @return
   */
  public DocumentType getDocumentType(final String docTypeId) {
    try {
      OBContext.setAdminMode(true);
      return OBDal.getInstance().get(DocumentType.class, docTypeId);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

}
