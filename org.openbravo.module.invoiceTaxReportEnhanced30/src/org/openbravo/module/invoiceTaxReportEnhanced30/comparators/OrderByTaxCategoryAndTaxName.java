/************************************************************************************ 
 * Copyright (C) 2010 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 * or in the legal folder of this module distribution.
 ************************************************************************************/

package org.openbravo.module.invoiceTaxReportEnhanced30.comparators;

import java.util.Comparator;

import org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord;

/**
 * Order by Tax Category and Tax Name
 * 
 * @author openbravo
 * 
 */
public class OrderByTaxCategoryAndTaxName implements Comparator<MTRRecord> {
  @Override
  public int compare(MTRRecord o1, MTRRecord o2) {
    final String taxCategory1 = o1.getTaxCategoryName();
    final String taxCategory2 = o2.getTaxCategoryName();

    final String taxName1 = o1.getTaxName();
    final String taxName2 = o2.getTaxName();

    if (taxCategory1.compareTo(taxCategory2) == 0) {
      if (taxName1.compareTo(taxName2) == 0) {
        return 0;
      } else {
        return taxName1.compareTo(taxName2);
      }
    } else {
      return taxCategory1.compareTo(taxCategory2);
    }
  }
}
