/************************************************************************************ 
 * Copyright (C) 2010 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package org.openbravo.module.invoiceTaxReportEnhanced30.comparators;

import java.util.Comparator;

import org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord;

/**
 * Order by Tax Name and Business Partner
 * 
 * @author openbravo
 * 
 */
public class OrderByTaxNameAndBP implements Comparator<MTRRecord> {

  @Override
  public int compare(MTRRecord o1, MTRRecord o2) {
    final String tax1 = o1.getTaxName();
    final String tax2 = o2.getTaxName();

    final String bp1 = o1.getBusinessPartner();
    final String bp2 = o2.getBusinessPartner();

    if (tax1.compareTo(tax2) == 0) {
      if (bp1.compareTo(bp2) == 0) {
        return 0;
      } else {
        return bp1.compareTo(bp2);
      }
    } else {
      return tax1.compareTo(tax2);
    }
  }
}
