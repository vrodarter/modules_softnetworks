/************************************************************************************ 
 * Copyright (C) 2010 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 * or in the legal folder of this module distribution.
 ************************************************************************************/

package org.openbravo.module.invoiceTaxReportEnhanced30.comparators;

import java.math.BigDecimal;
import java.util.Comparator;

import org.openbravo.module.invoiceTaxReportEnhanced30.api.MTRRecord;

/**
 * Order by Tax rate Percentage and Business Partner
 * 
 * @author openbravo
 * 
 */
public class OrderByTaxPercentageAndBP implements Comparator<MTRRecord> {

  @Override
  public int compare(MTRRecord o1, MTRRecord o2) {
    final BigDecimal tax1 = new BigDecimal(o1.getRate());
    final BigDecimal tax2 = new BigDecimal(o2.getRate());

    final String bp1 = o1.getBusinessPartner();
    final String bp2 = o2.getBusinessPartner();

    if (tax1.compareTo(tax2) == 0) {
      if (bp1.compareTo(bp2) == 0) {
        return 0;
      } else {
        return bp1.compareTo(bp2);
      }
    } else {
      return tax1.compareTo(tax2);
    }
  }

}
