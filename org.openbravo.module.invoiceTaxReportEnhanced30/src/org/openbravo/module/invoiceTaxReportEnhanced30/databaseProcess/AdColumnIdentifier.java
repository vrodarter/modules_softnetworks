package org.openbravo.module.invoiceTaxReportEnhanced30.databaseProcess;

import javax.enterprise.context.ApplicationScoped;

import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;
import org.openbravo.client.kernel.ApplicationInitializer;
import org.openbravo.dal.service.OBDal;

@ApplicationScoped
public class AdColumnIdentifier implements ApplicationInitializer {

  public void initialize() {
    OBDal.getInstance().registerSQLFunction("ad_column_identifier",
        new StandardSQLFunction("ad_column_identifier", StandardBasicTypes.STRING));
  }
}