/************************************************************************************ 
 * Copyright (C) 2010-2012 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 ************************************************************************************/
package org.openbravo.module.invoiceTaxReportEnhanced30.utility;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;

public class Utility {

  public static BigDecimal convertCurrency(final BigDecimal origAmount, final String fromCurrency,
      final String toCurrency, final Date date) {
    return convertCurrency(origAmount, fromCurrency, toCurrency, date, null);
  }

  /**
   * Convert currency amounts
   * 
   * @param origAmount
   *          amount to convert
   * @param fromCurrency
   *          original currency
   * @param toCurrency
   *          final currency
   * @param date
   *          conversion date
   * @return a BigDecimal with the converted amount
   */
  public static BigDecimal convertCurrency(final BigDecimal origAmount, final String fromCurrency,
      final String toCurrency, final Date date, BigDecimal convRate) {

    // if same currencies don't do anything
    if (fromCurrency.equals(toCurrency)) {
      return origAmount;
    }

    // call the Stored Function through Dal connection
    PreparedStatement ps = null;
    try {
      // first get a connection
      final Connection connection = OBDal.getInstance().getConnection(false);

      if (convRate == null || convRate.compareTo(BigDecimal.valueOf(-1)) == 0) {
        ps = connection
            .prepareStatement("SELECT C_CURRENCY_CONVERT(?, ?, ?, ?, NULL, ?) FROM DUAL");
      } else {
        ps = connection
            .prepareStatement("SELECT C_CURRENCY_CONVERT_RATE(?, ?, ?, ?, NULL, ?, '0', ?) FROM DUAL");
      }

      // Set Parameters
      ps.setBigDecimal(1, origAmount);
      ps.setString(2, fromCurrency);
      ps.setString(3, toCurrency);
      ps.setTimestamp(4, (Timestamp) date);
      ps.setString(5, OBContext.getOBContext().getCurrentClient().getId());
      if (convRate != null) {
        ps.setBigDecimal(6, convRate);
      }
      final ResultSet rs = ps.executeQuery();

      BigDecimal convertedAmt = BigDecimal.ZERO;
      if (rs.next()) {
        convertedAmt = rs.getBigDecimal(1);
      }

      return convertedAmt;
    } catch (final Exception e) {
      throw new IllegalStateException(e);
    } finally {
      try {
        ps.close();
      } catch (SQLException se) {
        // won't happen
      }
    }
  }

  /**
   * If value is "Y" returns true, else returns false
   * 
   * @param value
   * @return
   */
  public static boolean convertToBoolean(final String value) {
    if ("Y".equals(value))
      return true;
    else
      return false;
  }

  /**
   * If value is true returns "Y", else returns "N"
   * 
   * @param value
   * @return
   */
  public static String convertToString(final boolean value) {
    if (value)
      return "Y";
    else
      return "N";
  }

}
