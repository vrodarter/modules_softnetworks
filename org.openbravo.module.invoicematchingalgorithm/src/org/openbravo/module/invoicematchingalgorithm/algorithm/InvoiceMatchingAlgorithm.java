/*
 ************************************************************************************ 
 * Copyright (C) 2011-2013 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package org.openbravo.module.invoicematchingalgorithm.algorithm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.dao.AdvPaymentMngtDao;
import org.openbravo.advpaymentmngt.dao.MatchTransactionDao;
import org.openbravo.advpaymentmngt.dao.TransactionsDao;
import org.openbravo.advpaymentmngt.process.FIN_AddPayment;
import org.openbravo.advpaymentmngt.process.FIN_TransactionProcess;
import org.openbravo.advpaymentmngt.utility.FIN_MatchedTransaction;
import org.openbravo.advpaymentmngt.utility.FIN_MatchingAlgorithm;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.advpaymentmngt.utility.Value;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.ad_forms.AcctServer;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.ad.datamodel.Table;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.accounting.AccountingFact;
import org.openbravo.model.financialmgmt.payment.FIN_BankStatementLine;
import org.openbravo.model.financialmgmt.payment.FIN_FinaccTransaction;
import org.openbravo.model.financialmgmt.payment.FIN_FinancialAccount;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.model.financialmgmt.payment.FinAccPaymentMethod;
import org.openbravo.model.financialmgmt.payment.MatchingAlgorithm;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalConnectionProvider;

public class InvoiceMatchingAlgorithm implements FIN_MatchingAlgorithm {

  List<FIN_PaymentMethod> allowedPaymentMethods = new ArrayList<FIN_PaymentMethod>();

  public FIN_MatchedTransaction match(FIN_BankStatementLine line,
      List<FIN_FinaccTransaction> excluded) throws ServletException {
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    boolean isReceipt = amount.signum() > 0;
    allowedPaymentMethods = getAllowedPaymentMethods(line.getBankStatement().getAccount(),
        isReceipt);
    MatchingAlgorithm matchingAlgorithm = getMatchingAlgorithm();
    // long init = System.currentTimeMillis();
    MatchingDocument matchingDocument = findStrongMatch(line, matchingAlgorithm, excluded);
    // System.out.println("Finding Strong Match took: " + (System.currentTimeMillis() - init));
    if (matchingDocument == null) {
      // init = System.currentTimeMillis();
      matchingDocument = findWeakMatch(line, matchingAlgorithm, excluded);
      // System.out.println("Finding Weak Match took: " + (System.currentTimeMillis() - init));
    }
    if (matchingDocument != null) {
      FIN_FinaccTransaction myTrx = getTransaction(matchingDocument, line);
      if (myTrx != null) {
        return new FIN_MatchedTransaction(myTrx, matchingDocument.matchType);
      }
    }
    if (line.getGLItem() != null) {
      // No transaction found so create one
      FIN_FinaccTransaction transaction = matchGLItem(line);
      if (transaction != null) {
        return new FIN_MatchedTransaction(transaction, FIN_MatchedTransaction.STRONG);
      }
    }
    if (matchingAlgorithm.isOBAMAMatchCredit()) {
      // If there is no match against existing orders then create credit for the customer/vendor
      FIN_FinaccTransaction transactionCreatedAsCredit = createCredit(line);
      if (transactionCreatedAsCredit != null) {
        return new FIN_MatchedTransaction(transactionCreatedAsCredit, FIN_MatchedTransaction.WEAK);
      }
    }
    return new FIN_MatchedTransaction(null, FIN_MatchedTransaction.NOMATCH);
  }

  MatchingDocument findStrongMatch(FIN_BankStatementLine line, MatchingAlgorithm matchingAlgorithm,
      List<FIN_FinaccTransaction> excluded) {
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    // If GL Item is present in BSL then transaction is created using it
    if (line.getGLItem() != null) {
      // First search for an existing transaction
      List<FIN_FinaccTransaction> glItemTransactions = MatchTransactionDao
          .getMatchingGLItemTransaction(line.getBankStatement().getAccount().getId(),
              line.getGLItem(), line.getTransactionDate(), amount, excluded);
      if (glItemTransactions.size() > 0) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_TRANSACTION, glItemTransactions
            .get(0).getId(), FIN_MatchedTransaction.STRONG);
      }
    } else {
      List<FIN_FinaccTransaction> transactions = MatchTransactionDao
          .getMatchingFinancialTransaction(line.getBankStatement().getAccount().getId(),
              line.getReferenceNo(), amount, line.getBpartnername(), excluded);
      if (!transactions.isEmpty()) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_TRANSACTION, transactions.get(0)
            .getId(), FIN_MatchedTransaction.STRONG);
      }
    }
    if (matchingAlgorithm.isOBAMAMatchPayment()) {
      // If there is no match against existing transactions then try with existing payments
      // long init = System.currentTimeMillis();
      String payment = findPayment(line, matchingAlgorithm.isOBAMAMatchPaymentBP(),
          matchingAlgorithm.isOBAMAMatchPaymentPM(), matchingAlgorithm.isOBAMAMatchPaymentFA(),
          matchingAlgorithm.isOBAMAMatchPaymentDate());
      // System.out.println("Find Payment Took: " + (System.currentTimeMillis() - init));
      if (payment != null) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_PAYMENT, payment,
            FIN_MatchedTransaction.STRONG);
      }
    }
    if (matchingAlgorithm.isOBAMAMatchInvoice()) {
      // If there is no match against existing payments then try with existing invoices
      // First using BPartner and Payment Method
      FIN_PaymentSchedule paymentScheduleFromInvoice = findInvoice(line,
          matchingAlgorithm.isOBAMAMatchInvoiceBP(), matchingAlgorithm.isOBAMAMatchInvoicePM(),
          matchingAlgorithm.isOBAMAMatchinvoiceDate());
      if (paymentScheduleFromInvoice != null) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_INVOICE,
            paymentScheduleFromInvoice.getId(), FIN_MatchedTransaction.STRONG);
      }
    }
    if (matchingAlgorithm.isOBAMAMatchOrder()) {
      // If there is no match against existing invoices then try with existing orders
      FIN_PaymentSchedule paymentScheduleFromOrder = findOrder(line,
          matchingAlgorithm.isOBAMAMatchOrderPM(), matchingAlgorithm.isOBAMAMatchOrderBP(),
          matchingAlgorithm.isOBAMAMatchOrderDate());
      if (paymentScheduleFromOrder != null) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_ORDER,
            paymentScheduleFromOrder.getId(), FIN_MatchedTransaction.STRONG);
      }
    }
    return null;
  }

  MatchingDocument findWeakMatch(FIN_BankStatementLine line, MatchingAlgorithm matchingAlgorithm,
      List<FIN_FinaccTransaction> excluded) {
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    // If GL Item is present in BSL then transaction is created using it
    if (line.getGLItem() != null) {
      // First search for an existing transaction
      List<FIN_FinaccTransaction> glItemTransactions = MatchTransactionDao
          .getMatchingGLItemTransaction(line.getBankStatement().getAccount().getId(),
              line.getGLItem(), null, amount, excluded);
      if (glItemTransactions.size() > 0) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_TRANSACTION, glItemTransactions
            .get(0).getId(), FIN_MatchedTransaction.WEAK);
      }
    } else {
      List<FIN_FinaccTransaction> transactions = MatchTransactionDao
          .getMatchingFinancialTransaction(line.getBankStatement().getAccount().getId(), amount,
              excluded);
      if (!transactions.isEmpty()) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_TRANSACTION, transactions.get(0)
            .getId(), FIN_MatchedTransaction.WEAK);
      }
    }
    if (matchingAlgorithm.isOBAMAMatchPayment()) {
      // If there is no match against existing transactions then try with existing payments
      String payment = null;
      if (matchingAlgorithm.isOBAMAMatchPaymentBP()) {
        payment = findPayment(line, false, matchingAlgorithm.isOBAMAMatchPaymentPM(),
            matchingAlgorithm.isOBAMAMatchPaymentFA(), matchingAlgorithm.isOBAMAMatchPaymentDate());
      }
      if (matchingAlgorithm.isOBAMAMatchPaymentPM()) {
        payment = findPayment(line, false, false, matchingAlgorithm.isOBAMAMatchPaymentFA(),
            matchingAlgorithm.isOBAMAMatchPaymentDate());
      }
      if (payment == null && matchingAlgorithm.isOBAMAMatchPaymentDate()) {
        payment = findPayment(line, false, false, matchingAlgorithm.isOBAMAMatchPaymentFA(), false);
      }
      if (payment == null && matchingAlgorithm.isOBAMAMatchPaymentFA()) {
        payment = findPayment(line, false, false, false,
            matchingAlgorithm.isOBAMAMatchPaymentDate());
      }
      if (payment == null && matchingAlgorithm.isOBAMAMatchPaymentDate()
          && matchingAlgorithm.isOBAMAMatchPaymentFA()) {
        payment = findPayment(line, false, false, false, false);
      }
      if (payment != null) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_PAYMENT, payment,
            FIN_MatchedTransaction.WEAK);
      }
    }
    if (matchingAlgorithm.isOBAMAMatchInvoice()) {
      // If there is no match against existing payments then try with existing invoices
      // First using BPartner and Payment Method
      FIN_PaymentSchedule paymentScheduleFromInvoice = null;
      if (matchingAlgorithm.isOBAMAMatchinvoiceDate()) {
        paymentScheduleFromInvoice = findInvoice(line, matchingAlgorithm.isOBAMAMatchInvoiceBP(),
            matchingAlgorithm.isOBAMAMatchInvoicePM(), false);
      }
      if (paymentScheduleFromInvoice == null && matchingAlgorithm.isOBAMAMatchInvoicePM()) {
        paymentScheduleFromInvoice = findInvoice(line, matchingAlgorithm.isOBAMAMatchInvoiceBP(),
            false, false);
      }
      if (paymentScheduleFromInvoice == null && matchingAlgorithm.isOBAMAMatchInvoiceBP()) {
        paymentScheduleFromInvoice = findInvoice(line, false, false, false);
      }
      if (paymentScheduleFromInvoice != null) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_INVOICE,
            paymentScheduleFromInvoice.getId(), FIN_MatchedTransaction.WEAK);
      }
    }
    if (matchingAlgorithm.isOBAMAMatchOrder()) {
      // If there is no match against existing invoices then try with existing orders
      FIN_PaymentSchedule paymentScheduleFromOrder = null;
      if (matchingAlgorithm.isOBAMAMatchOrderDate()) {
        paymentScheduleFromOrder = findOrder(line, matchingAlgorithm.isOBAMAMatchOrderBP(),
            matchingAlgorithm.isOBAMAMatchOrderPM(), false);
      }
      if (paymentScheduleFromOrder == null && matchingAlgorithm.isOBAMAMatchOrderPM()) {
        paymentScheduleFromOrder = findOrder(line, matchingAlgorithm.isOBAMAMatchOrderBP(), false,
            false);
      }
      if (paymentScheduleFromOrder == null && matchingAlgorithm.isOBAMAMatchOrderBP()) {
        paymentScheduleFromOrder = findOrder(line, false, false, false);
      }
      if (paymentScheduleFromOrder != null) {
        return new MatchingDocument(MatchingDocument.DOCUMENT_ORDER,
            paymentScheduleFromOrder.getId(), FIN_MatchedTransaction.WEAK);
      }
    }
    return null;
  }

  FIN_FinaccTransaction matchGLItem(FIN_BankStatementLine line) {
    AdvPaymentMngtDao dao = new AdvPaymentMngtDao();
    FIN_FinaccTransaction transaction = null;
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    try {
      VariablesSecureApp vars = new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
          OBContext.getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
              .getCurrentOrganization().getId(), OBContext.getOBContext().getRole().getId());
      transaction = dao.getNewFinancialTransaction(line.getBankStatement().getAccount()
          .getOrganization(), line.getBankStatement().getAccount(), TransactionsDao
          .getTransactionMaxLineNo(line.getBankStatement().getAccount()), null,
          Utility.messageBD(new DalConnectionProvider(), "APRM_GLItem", vars.getLanguage()) + ": "
              + line.getGLItem().getName() + " " + line.getDescription(),
          line.getTransactionDate(), line.getGLItem(), amount.signum() > 0 ? "RDNC" : "PWNC", line
              .getCramount(), line.getDramount(), null, null, null, amount.signum() > 0 ? "BPD"
              : "BPW", line.getTransactionDate(), line.getBankStatement().getAccount()
              .getCurrency(), BigDecimal.ONE, amount, line.getBusinessPartner(), null, null);
      transaction.setCreatedByAlgorithm(true);
      OBDal.getInstance().save(transaction);
      OBDal.getInstance().flush();
      ConnectionProvider conn = new DalConnectionProvider();
      processTransaction(vars, conn, "P", transaction);
    } catch (Exception e) {
      return null;
    }
    return transaction;
  }

  FIN_FinaccTransaction matchInvoice(FIN_BankStatementLine line,
      FIN_PaymentSchedule invoicePaymentSchedule) {
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    boolean isReceipt = amount.signum() > 0;
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {
      if (invoicePaymentSchedule != null) {
        AdvPaymentMngtDao dao = new AdvPaymentMngtDao();
        DocumentType docType = FIN_Utility.getDocumentType(
            invoicePaymentSchedule.getOrganization(), isReceipt ? "ARR" : "APP");
        // get DocumentNo
        HashMap<String, BigDecimal> hm = new HashMap<String, BigDecimal>();
        BigDecimal totalAmount = BigDecimal.ZERO;
        // Just schedule details for first payment schedule are selected
        ArrayList<FIN_PaymentScheduleDetail> toBePaidPSDs = new ArrayList<FIN_PaymentScheduleDetail>();
        for (FIN_PaymentScheduleDetail psd : invoicePaymentSchedule
            .getFINPaymentScheduleDetailInvoicePaymentScheduleList()) {
          if (psd.getPaymentDetails() == null) {
            hm.put(psd.getId(), psd.getAmount());
            totalAmount = totalAmount.add(psd.getAmount());
            toBePaidPSDs.add(psd);
          }
        }
        String strPaymentDocumentNo = FIN_Utility.getDocumentNo(docType,
            docType.getTable() != null ? docType.getTable().getDBTableName() : "");
        FIN_PaymentMethod paymentMethod = invoicePaymentSchedule.getFinPaymentmethod();
        if (!allowedPaymentMethods.contains(paymentMethod)) {
          String defaultPaymentMethodId = dao.getDefaultPaymentMethodId(line.getBankStatement()
              .getAccount(), isReceipt);
          if ("".equals(defaultPaymentMethodId)) {
            paymentMethod = allowedPaymentMethods.get(0);
          } else {
            paymentMethod = OBDal.getInstance()
                .get(FIN_PaymentMethod.class, defaultPaymentMethodId);
          }
        }
        FIN_Payment payment = FIN_AddPayment.savePayment(null, isReceipt, docType,
            strPaymentDocumentNo, invoicePaymentSchedule.getInvoice().getBusinessPartner(),
            paymentMethod, line.getBankStatement().getAccount(), totalAmount.toString(),
            line.getTransactionDate(), invoicePaymentSchedule.getOrganization(),
            line.getReferenceNo(), toBePaidPSDs, hm, false, false);
        // Flag payment as created by algorithm
        payment.setCreatedByAlgorithm(true);
        OBDal.getInstance().save(payment);
        OBDal.getInstance().flush();
        try {
          ConnectionProvider conn = new DalConnectionProvider();
          OBError myError = FIN_AddPayment.processPayment(new VariablesSecureApp(OBContext
              .getOBContext().getUser().getId(), OBContext.getOBContext().getCurrentClient()
              .getId(), OBContext.getOBContext().getCurrentOrganization().getId(), OBContext
              .getOBContext().getRole().getId()), conn, "P", payment);
          if (!"Success".equals(myError.getType())) {
            return null;
          }
          FIN_FinaccTransaction transaction = dao.getFinancialTransaction(payment);
          // Flag transaction as created by algorithm
          transaction.setCreatedByAlgorithm(true);
          OBDal.getInstance().save(transaction);
          OBDal.getInstance().flush();
          if (!transaction.isProcessed()) {
            myError = processTransaction(new VariablesSecureApp(OBContext.getOBContext().getUser()
                .getId(), OBContext.getOBContext().getCurrentClient().getId(), OBContext
                .getOBContext().getCurrentOrganization().getId(), OBContext.getOBContext()
                .getRole().getId()), conn, "P", transaction);
            if (!"Success".equals(myError.getType())) {
              return null;
            }
          }
          return transaction;
        } catch (Exception e) {
          return null;
        }
      }
      return null;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * 
   * @param line
   *          : Bank Statement Line
   * @param sameBP
   *          : Match BP
   * @param samePM
   *          : Match PM
   * @param sameDate
   *          : Match Date
   * @return matching invoice payment schedule or null if there is no match
   */
  FIN_PaymentSchedule findInvoice(FIN_BankStatementLine line, boolean sameBP, boolean samePM,
      boolean sameDate) {
    final StringBuilder whereClause = new StringBuilder();
    final List<Object> parameters = new ArrayList<Object>();
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    boolean isReceipt = amount.signum() > 0;
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {

      whereClause.append(" as psd "); // pending scheduled payments //
      whereClause.append(" left outer join psd.invoicePaymentSchedule as psdi");
      whereClause.append(" left outer join psdi.invoice ");
      whereClause.append(" where psd.");
      whereClause.append(FIN_PaymentScheduleDetail.PROPERTY_PAYMENTDETAILS);
      whereClause.append(" is null");
      whereClause.append(" and psd.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_ORGANIZATION);
      whereClause.append(".id in (");
      whereClause.append(parse(OBContext.getOBContext().getOrganizationStructureProvider()
          .getChildTree(line.getOrganization().getId(), true)));
      whereClause.append(")");

      // Transaction type filter
      whereClause.append(" and psdi.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_INVOICE);
      whereClause.append(" is not null");
      // Just allowed payment methods: no execution process and enabled for that financial account
      if (samePM && allowedPaymentMethods.size() > 0) {
        whereClause.append(" and psdi.");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_FINPAYMENTMETHOD);
        whereClause.append(" in (");
        whereClause.append(FIN_Utility.getInStrList(allowedPaymentMethods));
        whereClause.append(")");
      }
      if (sameBP && line.getBusinessPartner() != null) {
        whereClause.append(" and psdi.");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_INVOICE);
        whereClause.append(".");
        whereClause.append(Invoice.PROPERTY_BUSINESSPARTNER);
        whereClause.append(".id = '");
        whereClause.append(line.getBusinessPartner().getId());
        whereClause.append("'");
      }
      whereClause.append(" and psdi.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_INVOICE);
      whereClause.append(".");
      whereClause.append(Invoice.PROPERTY_SALESTRANSACTION);
      whereClause.append(" = ");
      whereClause.append(isReceipt);
      whereClause.append(" and psdi.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_INVOICE);
      whereClause.append(".");
      whereClause.append(Invoice.PROPERTY_CURRENCY);
      whereClause.append(".id = '");
      whereClause.append(line.getBankStatement().getAccount().getCurrency().getId());
      whereClause.append("'");
      // amount
      whereClause.append(" and psdi.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_OUTSTANDINGAMOUNT);
      whereClause.append(" = ?");
      parameters.add(amount.abs());
      // dateTo
      // TODO Review this date. i guess someone can pay a bill prior to due date
      if (sameDate) {
        whereClause.append(" and psd.");
        whereClause.append(FIN_PaymentScheduleDetail.PROPERTY_INVOICEPAYMENTSCHEDULE);
        whereClause.append(".");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
        whereClause.append(" = ?");
        parameters.add(line.getTransactionDate());
      }
      // TODO: Add order to show first scheduled payments from invoices and later scheduled payments
      // from not invoiced orders.
      whereClause.append(" order by");
      whereClause.append(" psdi.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_FINPAYMENTPRIORITY);
      whereClause.append(", psdi.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
      whereClause.append(", psdi.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_INVOICE);
      whereClause.append(".");
      whereClause.append(Invoice.PROPERTY_DOCUMENTNO);
      final OBQuery<FIN_PaymentScheduleDetail> obqPSD = OBDal.getInstance().createQuery(
          FIN_PaymentScheduleDetail.class, whereClause.toString());
      obqPSD.setFilterOnReadableOrganization(false);
      obqPSD.setParameters(parameters);
      obqPSD.setMaxResult(1);
      FIN_PaymentScheduleDetail paymentScheduleDetail = obqPSD.uniqueResult();
      if (paymentScheduleDetail != null) {
        return paymentScheduleDetail.getInvoicePaymentSchedule();
      } else {
        return null;
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * @param line
   *          : Bank statement Line
   * @param sameBPartner
   *          : Same Business Partner when matching
   * @param samePM
   *          : Same Payment Method when matching
   * @param sameAccount
   *          : Same Financial Account when matching
   * @param sameDate
   *          : Same Date when matching
   * @return matching payment or null when no one matches
   */
  String findPayment(FIN_BankStatementLine line, boolean sameBPartner, boolean samePM,
      boolean sameAccount, boolean sameDate) {
    final StringBuilder whereClause = new StringBuilder();
    final List<Object> parameters = new ArrayList<Object>();
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    boolean isReceipt = amount.signum() > 0;
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {

      whereClause.append("select p.id from FIN_Payment as p "); // pending payments //
      whereClause.append(" where not exists ");
      whereClause
          .append(" (select 1 from FIN_Finacc_Transaction as t where t.finPayment.id = p.id and t.processed = true) ");

      if (sameBPartner && line.getBusinessPartner() != null) {
        whereClause.append(" and p.");
        whereClause.append(FIN_Payment.PROPERTY_BUSINESSPARTNER);
        whereClause.append(".id = '");
        whereClause.append(line.getBusinessPartner().getId());
        whereClause.append("'");
      }
      if (samePM && allowedPaymentMethods.size() > 0) {
        // Just allowed payment methods: no execution process and enabled for that financial account
        whereClause.append(" and p.");
        whereClause.append(FIN_Payment.PROPERTY_PAYMENTMETHOD);
        whereClause.append(" in (");
        whereClause.append(FIN_Utility.getInStrList(allowedPaymentMethods));
        whereClause.append(")");
      }
      if (sameDate) {
        whereClause.append(" and p.");
        whereClause.append(FIN_Payment.PROPERTY_PAYMENTDATE);
        whereClause.append(" = ? ");
        parameters.add(line.getTransactionDate());
      }
      whereClause.append(" and p.");
      whereClause.append(FIN_Payment.PROPERTY_PROCESSED);
      whereClause.append(" = true ");
      whereClause.append(" and p.");
      whereClause.append(FIN_Payment.PROPERTY_ORGANIZATION);
      whereClause.append(".id in (");
      whereClause.append(parse(OBContext.getOBContext().getOrganizationStructureProvider()
          .getNaturalTree(line.getOrganization().getId())));
      whereClause.append(")");
      whereClause.append(" and p.");
      whereClause.append(FIN_Payment.PROPERTY_STATUS);
      whereClause.append(" not in ("
          + Utility.getInStrSet(new HashSet<String>(FIN_Utility.getListPaymentNotConfirmed()))
          + ") ");
      whereClause.append(" and p.");
      whereClause.append(FIN_Payment.PROPERTY_RECEIPT);
      whereClause.append(" = ");
      whereClause.append(isReceipt);
      whereClause.append(" and p.");
      whereClause.append(FIN_Payment.PROPERTY_CURRENCY);
      whereClause.append(".id = '");
      whereClause.append(line.getBankStatement().getAccount().getCurrency().getId());
      whereClause.append("'");
      if (sameAccount) {
        whereClause.append(" and p.");
        whereClause.append(FIN_Payment.PROPERTY_ACCOUNT);
        whereClause.append(".id = '");
        whereClause.append(line.getBankStatement().getAccount().getId());
        whereClause.append("'");
      }
      // amount
      whereClause.append(" and p.");
      whereClause.append(FIN_Payment.PROPERTY_AMOUNT);
      whereClause.append(" = ?");
      parameters.add(amount.abs());
      whereClause.append(" order by");
      whereClause.append(" p.");
      whereClause.append(FIN_Payment.PROPERTY_PAYMENTDATE);
      whereClause.append(", p.");
      whereClause.append(FIN_Payment.PROPERTY_DOCUMENTNO);

      final Session session = OBDal.getInstance().getSession();
      final Query obqPayment = session.createQuery(whereClause.toString());
      for (int i = 0; i < parameters.size(); i++) {
        obqPayment.setParameter(i, parameters.get(i));
      }
      Object resultObject = obqPayment.uniqueResult();
      if (resultObject != null && resultObject instanceof String) {
        return (String) resultObject;
      }
      return null;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  FIN_FinaccTransaction matchPayment(FIN_BankStatementLine line, FIN_Payment payment) {
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {
      if (payment != null) {
        AdvPaymentMngtDao dao = new AdvPaymentMngtDao();
        FIN_FinaccTransaction transaction = dao.getFinancialTransaction(payment);
        // Flag transaction as created by algorithm
        transaction.setDateAcct(line.getTransactionDate());
        transaction.setTransactionDate(line.getTransactionDate());
        transaction.setAccount(line.getBankStatement().getAccount());
        transaction.setCreatedByAlgorithm(true);
        OBDal.getInstance().save(transaction);
        OBDal.getInstance().flush();
        if (!transaction.isProcessed()) {
          try {
            ConnectionProvider conn = new DalConnectionProvider();
            OBError myError = processTransaction(new VariablesSecureApp(OBContext.getOBContext()
                .getUser().getId(), OBContext.getOBContext().getCurrentClient().getId(), OBContext
                .getOBContext().getCurrentOrganization().getId(), OBContext.getOBContext()
                .getRole().getId()), conn, "P", transaction);
            if (!"Success".equals(myError.getType())) {
              return null;
            }
          } catch (Exception e) {
            return null;
          }
        }
        return transaction;
      }
      return null;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  FIN_PaymentSchedule findOrder(FIN_BankStatementLine line, boolean PMCheck, boolean BPCheck,
      boolean DateCheck) {
    final StringBuilder whereClause = new StringBuilder();
    final List<Object> parameters = new ArrayList<Object>();
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    boolean isReceipt = amount.signum() > 0;
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {

      whereClause.append(" as psd "); // pending scheduled payments //
      whereClause.append(" left outer join psd.orderPaymentSchedule psdo");
      whereClause.append(" left outer join psdo.order ");
      whereClause.append(" where psd.");
      whereClause.append(FIN_PaymentScheduleDetail.PROPERTY_PAYMENTDETAILS);
      whereClause.append(" is null");
      whereClause.append(" and psd.");
      whereClause.append(FIN_PaymentScheduleDetail.PROPERTY_INVOICEPAYMENTSCHEDULE);
      whereClause.append(" is null");
      whereClause.append(" and psd.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_ORGANIZATION);
      whereClause.append(".id in (");
      whereClause.append(parse(OBContext.getOBContext().getOrganizationStructureProvider()
          .getNaturalTree(line.getOrganization().getId())));
      whereClause.append(")");

      // Transaction type filter
      whereClause.append(" and psdo.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_ORDER);
      whereClause.append(" is not null");
      // Just allowed payment methods: no execution process and enabled for that financial account
      if (PMCheck && allowedPaymentMethods.size() > 0) {
        whereClause.append(" and psdo.");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_FINPAYMENTMETHOD);
        whereClause.append(" in (");
        whereClause.append(FIN_Utility.getInStrList(allowedPaymentMethods));
        whereClause.append(")");
      }
      if (BPCheck && line.getBusinessPartner() != null) {
        whereClause.append(" and psdo.");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_ORDER);
        whereClause.append(".");
        whereClause.append(Order.PROPERTY_BUSINESSPARTNER);
        whereClause.append(".id = '");
        whereClause.append(line.getBusinessPartner().getId());
        whereClause.append("'");
      }
      whereClause.append(" and psdo.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_ORDER);
      whereClause.append(".");
      whereClause.append(Order.PROPERTY_SALESTRANSACTION);
      whereClause.append(" = ");
      whereClause.append(isReceipt);
      whereClause.append(" and psdo.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_ORDER);
      whereClause.append(".");
      whereClause.append(Order.PROPERTY_CURRENCY);
      whereClause.append(".id = '");
      whereClause.append(line.getBankStatement().getAccount().getCurrency().getId());
      whereClause.append("'");
      // amount
      whereClause.append(" and psdo.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_OUTSTANDINGAMOUNT);
      whereClause.append(" = ?");
      parameters.add(amount.abs());
      // dateTo
      if (DateCheck) {
        whereClause.append(" and psd.");
        whereClause.append(FIN_PaymentScheduleDetail.PROPERTY_ORDERPAYMENTSCHEDULE);
        whereClause.append(".");
        whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
        whereClause.append(" = ?");
        parameters.add(line.getTransactionDate());
      }
      // TODO: Add order to show first scheduled payments from invoices and later scheduled payments
      // from not invoiced orders.
      whereClause.append(" order by");
      whereClause.append(" psdo.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_FINPAYMENTPRIORITY);
      whereClause.append(", psdo.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_EXPECTEDDATE);
      whereClause.append(", psdo.");
      whereClause.append(FIN_PaymentSchedule.PROPERTY_ORDER);
      whereClause.append(".");
      whereClause.append(Order.PROPERTY_DOCUMENTNO);
      final OBQuery<FIN_PaymentScheduleDetail> obqPSD = OBDal.getInstance().createQuery(
          FIN_PaymentScheduleDetail.class, whereClause.toString());

      obqPSD.setParameters(parameters);
      obqPSD.setMaxResult(1);
      FIN_PaymentScheduleDetail paymentScheduleDetail = obqPSD.uniqueResult();
      if (paymentScheduleDetail != null) {
        return paymentScheduleDetail.getOrderPaymentSchedule();
      } else {
        return null;
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  FIN_FinaccTransaction matchOrder(FIN_BankStatementLine line,
      FIN_PaymentSchedule orderPaymentSchedule) {
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    boolean isReceipt = amount.signum() > 0;
    // FIXME: added to access the FIN_PaymentSchedule and FIN_PaymentScheduleDetail tables to be
    // removed when new security implementation is done
    OBContext.setAdminMode();
    try {
      if (orderPaymentSchedule != null) {
        AdvPaymentMngtDao dao = new AdvPaymentMngtDao();
        DocumentType docType = FIN_Utility.getDocumentType(orderPaymentSchedule.getOrganization(),
            isReceipt ? "ARR" : "APP");
        // get DocumentNo
        HashMap<String, BigDecimal> hm = new HashMap<String, BigDecimal>();
        ArrayList<FIN_PaymentScheduleDetail> toBePaidPSD = new ArrayList<FIN_PaymentScheduleDetail>();
        BigDecimal toBePaid = BigDecimal.ZERO;
        for (FIN_PaymentScheduleDetail scheduleDetail : orderPaymentSchedule
            .getFINPaymentScheduleDetailOrderPaymentScheduleList()) {
          if (scheduleDetail.getPaymentDetails() == null) {
            hm.put(scheduleDetail.getId(), scheduleDetail.getAmount());
            toBePaidPSD.add(scheduleDetail);
            toBePaid = toBePaid.add(scheduleDetail.getAmount());
          }
        }
        String strPaymentDocumentNo = FIN_Utility.getDocumentNo(docType,
            docType.getTable() != null ? docType.getTable().getDBTableName() : "");
        FIN_PaymentMethod paymentMethod = orderPaymentSchedule.getFinPaymentmethod();
        if (!allowedPaymentMethods.contains(paymentMethod)) {
          String defaultPaymentMethodId = dao.getDefaultPaymentMethodId(line.getBankStatement()
              .getAccount(), isReceipt);
          if ("".equals(defaultPaymentMethodId)) {
            paymentMethod = allowedPaymentMethods.get(0);
          } else {
            paymentMethod = OBDal.getInstance()
                .get(FIN_PaymentMethod.class, defaultPaymentMethodId);
          }
        }
        FIN_Payment payment = FIN_AddPayment.savePayment(null, isReceipt, docType,
            strPaymentDocumentNo, orderPaymentSchedule.getOrder().getBusinessPartner(),
            paymentMethod, line.getBankStatement().getAccount(), toBePaid.toString(),
            line.getTransactionDate(), orderPaymentSchedule.getOrganization(),
            line.getReferenceNo(), toBePaidPSD, hm, false, false);
        // Flag payment as created by algorithm
        payment.setCreatedByAlgorithm(true);
        OBDal.getInstance().save(payment);
        OBDal.getInstance().flush();
        try {
          ConnectionProvider conn = new DalConnectionProvider();
          OBError myError = FIN_AddPayment.processPayment(new VariablesSecureApp(OBContext
              .getOBContext().getUser().getId(), OBContext.getOBContext().getCurrentClient()
              .getId(), OBContext.getOBContext().getCurrentOrganization().getId(), OBContext
              .getOBContext().getRole().getId()), conn, "P", payment);
          if (!"Success".equals(myError.getType())) {
            return null;
          }
          FIN_FinaccTransaction transaction = dao.getFinancialTransaction(payment);
          // Flag transaction as created by algorithm
          transaction.setCreatedByAlgorithm(true);
          OBDal.getInstance().save(transaction);
          OBDal.getInstance().flush();
          if (!transaction.isProcessed()) {
            myError = processTransaction(new VariablesSecureApp(OBContext.getOBContext().getUser()
                .getId(), OBContext.getOBContext().getCurrentClient().getId(), OBContext
                .getOBContext().getCurrentOrganization().getId(), OBContext.getOBContext()
                .getRole().getId()), conn, "P", transaction);
            if (!"Success".equals(myError.getType())) {
              return null;
            }
          }
          return transaction;
        } catch (Exception e) {
          return null;
        }
      }
      return null;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  FIN_FinaccTransaction createCredit(FIN_BankStatementLine line) {
    BusinessPartner bp = line.getBusinessPartner();
    BigDecimal amount = line.getCramount().subtract(line.getDramount());
    boolean isReceipt = amount.signum() > 0;
    if (bp == null) {
      return null;
    }
    FIN_PaymentMethod pm = isReceipt ? bp.getPaymentMethod() : bp.getPOPaymentMethod();
    if (pm == null) {
      return null;
    }
    if (!getAllowedPaymentMethods(line.getBankStatement().getAccount(), isReceipt).contains(pm)) {
      return null;
    }
    // PriceList is required to determine the currency of the BP. Credit will be calculated using
    // that currency
    PriceList priceList = isReceipt ? bp.getPriceList() : bp.getPurchasePricelist();
    if (priceList == null) {
      return null;
    }
    DocumentType docType = FIN_Utility.getDocumentType(line.getOrganization(), isReceipt ? "ARR"
        : "APP");
    // get DocumentNo
    String strPaymentDocumentNo = FIN_Utility.getDocumentNo(docType,
        docType.getTable() != null ? docType.getTable().getDBTableName() : "");
    FIN_Payment payment = FIN_AddPayment.savePayment(null, isReceipt, docType,
        strPaymentDocumentNo, bp, pm, line.getBankStatement().getAccount(),
        amount.abs().toString(), line.getTransactionDate(), line.getOrganization(),
        line.getReferenceNo(), new ArrayList<FIN_PaymentScheduleDetail>(),
        new HashMap<String, BigDecimal>(), false, false);
    // Flag payment as created by algorithm
    payment.setCreatedByAlgorithm(true);
    OBDal.getInstance().save(payment);
    OBDal.getInstance().flush();
    try {
      ConnectionProvider conn = new DalConnectionProvider();
      OBError myError = FIN_AddPayment.processPayment(new VariablesSecureApp(OBContext
          .getOBContext().getUser().getId(), OBContext.getOBContext().getCurrentClient().getId(),
          OBContext.getOBContext().getCurrentOrganization().getId(), OBContext.getOBContext()
              .getRole().getId()), conn, "P", payment);
      if ("Error".equalsIgnoreCase(myError.getType())) {
        return null;
      }
      AdvPaymentMngtDao dao = new AdvPaymentMngtDao();
      FIN_FinaccTransaction transaction = dao.getFinancialTransaction(payment);
      // Flag transaction as created by algorithm
      transaction.setCreatedByAlgorithm(true);
      OBDal.getInstance().save(transaction);
      OBDal.getInstance().flush();
      if (!transaction.isProcessed()) {
        myError = processTransaction(new VariablesSecureApp(OBContext.getOBContext().getUser()
            .getId(), OBContext.getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
            .getCurrentOrganization().getId(), OBContext.getOBContext().getRole().getId()), conn,
            "P", transaction);
        if ("Error".equalsIgnoreCase(myError.getType())) {
          return null;
        }
      }
      return transaction;
    } catch (Exception e) {
      return null;
    }
  }

  public void unmatch(FIN_FinaccTransaction transaction) throws ServletException {
    OBContext.setAdminMode();
    try {
      if (transaction == null)
        return;
      FIN_Payment payment = transaction.getFinPayment();
      VariablesSecureApp vars = new VariablesSecureApp(OBContext.getOBContext().getUser().getId(),
          OBContext.getOBContext().getCurrentClient().getId(), OBContext.getOBContext()
              .getCurrentOrganization().getId(), OBContext.getOBContext().getRole().getId());
      ConnectionProvider conn = new DalConnectionProvider();
      if (transaction.isCreatedByAlgorithm()) {
        removeTransaction(vars, conn, transaction);
      } else
        return;
      if (payment != null && payment.isCreatedByAlgorithm()) {
        removePayment(vars, conn, payment);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return;
  }

  void removeTransaction(VariablesSecureApp vars, ConnectionProvider conn,
      FIN_FinaccTransaction transaction) {
    final String FIN_FINACC_TRANSACTION_TABLE = "4D8C3B3C31D1410DA046140C9F024D17";
    OBContext.setAdminMode();
    try {
      if ("Y".equals(transaction.getPosted())) {
        boolean orgLegalWithAccounting = FIN_Utility.periodControlOpened(
            FIN_FinaccTransaction.TABLE_NAME, transaction.getId(), FIN_FinaccTransaction.TABLE_NAME
                + "_ID", "LE");
        if (!FIN_Utility.isPeriodOpen(transaction.getClient().getId(),
            AcctServer.DOCTYPE_FinAccTransaction, transaction.getOrganization().getId(),
            OBDateUtils.formatDate(transaction.getDateAcct()))
            && orgLegalWithAccounting) {
          throw new OBException(Utility.parseTranslation(conn, vars, vars.getLanguage(),
              "@PeriodNotAvailable@"));
        }
        List<AccountingFact> accountingEntries = FIN_Utility.getAllInstances(
            AccountingFact.class,
            false,
            false,
            new Value(AccountingFact.PROPERTY_TABLE, OBDal.getInstance().get(Table.class,
                FIN_FINACC_TRANSACTION_TABLE)), new Value(AccountingFact.PROPERTY_RECORDID,
                transaction.getId()));
        for (AccountingFact accountingEntry : accountingEntries) {
          OBDal.getInstance().remove(accountingEntry);
          OBDal.getInstance().flush();
        }
        transaction.setPosted("N");
        OBDal.getInstance().save(transaction);
        OBDal.getInstance().flush();
      }
      OBError msg = processTransaction(vars, conn, "R", transaction);
      if ("Success".equals(msg.getType())) {
        OBContext.setAdminMode();
        try {
          OBDal.getInstance().remove(transaction);
          OBDal.getInstance().flush();
        } finally {
          OBContext.restorePreviousMode();
        }
      }
    } catch (Exception e) {
      throw new OBException("Process failed deleting the financial account Transaction", e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  void removePayment(VariablesSecureApp vars, ConnectionProvider conn, FIN_Payment payment) {
    final String FIN_PAYMENT_TABLE = "D1A97202E832470285C9B1EB026D54E2";
    OBContext.setAdminMode();
    try {
      if ("Y".equals(payment.getPosted())) {
        boolean orgLegalWithAccounting = FIN_Utility.periodControlOpened(FIN_Payment.TABLE_NAME,
            payment.getId(), FIN_Payment.TABLE_NAME + "_ID", "LE");
        if (!FIN_Utility.isPeriodOpen(payment.getClient().getId(), payment.getDocumentType()
            .getDocumentCategory(), payment.getOrganization().getId(), OBDateUtils
            .formatDate(payment.getPaymentDate()))
            && orgLegalWithAccounting) {
          throw new OBException(Utility.parseTranslation(conn, vars, vars.getLanguage(),
              "@PeriodNotAvailable@"));
        }
        List<AccountingFact> accountingEntries = FIN_Utility.getAllInstances(
            AccountingFact.class,
            false,
            false,
            new Value(AccountingFact.PROPERTY_TABLE, OBDal.getInstance().get(Table.class,
                FIN_PAYMENT_TABLE)), new Value(AccountingFact.PROPERTY_RECORDID, payment.getId()));
        for (AccountingFact accountingEntry : accountingEntries) {
          OBDal.getInstance().remove(accountingEntry);
          OBDal.getInstance().flush();
        }
        payment.setPosted("N");
        OBDal.getInstance().save(payment);
        OBDal.getInstance().flush();
      }
      OBError msg = FIN_AddPayment.processPayment(vars, conn, "R", payment);
      if ("Success".equals(msg.getType())) {
        OBContext.setAdminMode();
        try {
          OBDal.getInstance().remove(payment);
          OBDal.getInstance().flush();
        } finally {
          OBContext.restorePreviousMode();
        }
      }
    } catch (Exception e) {
      throw new OBException("Process failed deleting payment", e);
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  /**
   * It calls the Transaction Process for the given transaction and action.
   * 
   * @param vars
   *          VariablesSecureApp with the session data.
   * @param conn
   *          ConnectionProvider with the connection being used.
   * @param strAction
   *          String with the action of the process. {P, D, R}
   * @param transaction
   *          FIN_FinaccTransaction that needs to be processed.
   * @return a OBError with the result message of the process.
   * @throws Exception
   */
  public static OBError processTransaction(VariablesSecureApp vars, ConnectionProvider conn,
      String strAction, FIN_FinaccTransaction transaction) throws Exception {
    ProcessBundle pb = new ProcessBundle("F68F2890E96D4D85A1DEF0274D105BCE", vars).init(conn);
    HashMap<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("action", strAction);
    parameters.put("Fin_FinAcc_Transaction_ID", transaction.getId());
    pb.setParams(parameters);
    OBError myMessage = null;
    new FIN_TransactionProcess().execute(pb);
    myMessage = (OBError) pb.getResult();
    return myMessage;
  }

  String parse(Set<String> stringSet) {
    String result = "";
    Iterator<String> i = stringSet.iterator();
    while (i.hasNext()) {
      result += "'";
      result += i.next();
      result += "', ";
    }
    if (result.length() > 0)
      result = result.substring(0, result.length() - 2);
    return result;
  }

  private List<FIN_PaymentMethod> getAllowedPaymentMethods(FIN_FinancialAccount account,
      boolean isReceipt) {
    List<FIN_PaymentMethod> allowedPaymentMethods = new ArrayList<FIN_PaymentMethod>();
    OBContext.setAdminMode();
    try {
      OBCriteria<FinAccPaymentMethod> obc = OBDal.getInstance().createCriteria(
          FinAccPaymentMethod.class);
      obc.createAlias(FinAccPaymentMethod.PROPERTY_PAYMENTMETHOD, "pm");
      obc.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_ACCOUNT, account));
      if (isReceipt) {
        obc.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYINALLOW, true));
        obc.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYINEXECUTIONTYPE, "M"));
      } else {
        obc.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYOUTALLOW, true));
        obc.add(Restrictions.eq(FinAccPaymentMethod.PROPERTY_PAYOUTEXECUTIONTYPE, "M"));
      }
      obc.addOrder(org.hibernate.criterion.Order.asc("pm." + FIN_PaymentMethod.PROPERTY_NAME));
      for (FinAccPaymentMethod pm : obc.list()) {
        allowedPaymentMethods.add(pm.getPaymentMethod());
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    return allowedPaymentMethods;
  }

  private MatchingAlgorithm getMatchingAlgorithm() {
    List<MatchingAlgorithm> matchingAlgorithm = new ArrayList<MatchingAlgorithm>();
    OBContext.setAdminMode();
    final String JAVACLASSNAME = "org.openbravo.module.invoicematchingalgorithm.algorithm.InvoiceMatchingAlgorithm";
    try {
      OBCriteria<MatchingAlgorithm> obc = OBDal.getInstance().createCriteria(
          MatchingAlgorithm.class);
      obc.add(Restrictions.eq(MatchingAlgorithm.PROPERTY_JAVACLASSNAME, JAVACLASSNAME));
      obc.setMaxResults(1);
      matchingAlgorithm = obc.list();
    } finally {
      OBContext.restorePreviousMode();
    }
    return matchingAlgorithm.size() > 0 ? matchingAlgorithm.get(0) : null;
  }

  FIN_FinaccTransaction getTransaction(MatchingDocument matchingDocument, FIN_BankStatementLine line) {
    if (MatchingDocument.DOCUMENT_TRANSACTION.equals(matchingDocument.documentType)) {
      return OBDal.getInstance().get(FIN_FinaccTransaction.class, matchingDocument.id);
    } else if (MatchingDocument.DOCUMENT_PAYMENT.equals(matchingDocument.documentType)) {
      return matchPayment(line, OBDal.getInstance().get(FIN_Payment.class, matchingDocument.id));
    } else if (MatchingDocument.DOCUMENT_INVOICE.equals(matchingDocument.documentType)) {
      return matchInvoice(line,
          OBDal.getInstance().get(FIN_PaymentSchedule.class, matchingDocument.id));
    } else if (MatchingDocument.DOCUMENT_ORDER.equals(matchingDocument.documentType)) {
      return matchOrder(line,
          OBDal.getInstance().get(FIN_PaymentSchedule.class, matchingDocument.id));
    }
    return null;
  }

  public class MatchingDocument {
    final static String DOCUMENT_TRANSACTION = "TRX";
    final static String DOCUMENT_PAYMENT = "PAY";
    final static String DOCUMENT_INVOICE = "INV";
    final static String DOCUMENT_ORDER = "ORD";
    final String documentType;
    final String id;
    final String matchType;

    public MatchingDocument(String myDocumentType, String myId, String myMatchType) {
      if (!DOCUMENT_TRANSACTION.equals(myDocumentType) && !DOCUMENT_PAYMENT.equals(myDocumentType)
          && !DOCUMENT_INVOICE.equals(myDocumentType) && !DOCUMENT_ORDER.equals(myDocumentType)) {
        throw new OBException("IlegalMatchingDocument");
      }
      documentType = myDocumentType;
      id = myId;
      matchType = myMatchType;
    }
  }
}
