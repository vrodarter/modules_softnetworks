/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2013 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.utility.draganddropselector3listsimplementation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.service.datasource.ReadOnlyDataSourceService;

public class DragAndDropThreeListDataSource extends ReadOnlyDataSourceService {
  private static final Logger log4j = Logger.getLogger(DragAndDropThreeListDataSource.class);

  @Override
  protected int getCount(Map<String, String> parameters) {

    String adReferenceID = parameters.get("_adReferenceID");

    OBContext.setAdminMode(true);
    OBCriteria<org.openbravo.model.ad.domain.List> obc = OBDal.getInstance().createCriteria(
        org.openbravo.model.ad.domain.List.class);
    obc.add(Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_REFERENCE + ".id",
        adReferenceID));
    obc.addOrderBy(org.openbravo.model.ad.domain.List.PROPERTY_SEQUENCENUMBER, true);
    obc.addOrderBy(org.openbravo.model.ad.domain.List.PROPERTY_SEARCHKEY, false);

    return obc.list().size();
  }

  @Override
  protected List<Map<String, Object>> getData(Map<String, String> parameters, int startRow,
      int endRow) {

    List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

    String adReferenceID = parameters.get("_adReferenceID");

    try {
      OBContext.setAdminMode(true);
      // Retrieve the List that belongs to the ID recovered as a Parameter
      OBCriteria<org.openbravo.model.ad.domain.List> obc = OBDal.getInstance().createCriteria(
          org.openbravo.model.ad.domain.List.class);
      obc.add(Restrictions.eq(org.openbravo.model.ad.domain.List.PROPERTY_REFERENCE + ".id",
          adReferenceID));
      obc.addOrderBy(org.openbravo.model.ad.domain.List.PROPERTY_SEQUENCENUMBER, true);
      obc.addOrderBy(org.openbravo.model.ad.domain.List.PROPERTY_SEARCHKEY, false);
      List<org.openbravo.model.ad.domain.List> referenceList = obc.list();

      if (obc.list().size() > 100) {
        // This reference is intended to be used with short lists. If the list is bigger than
        // expected, this log would help to notice it.
        log4j.error("DragAndDropDataSource.java retrieving a list of more than 100 records");
      }

      for (org.openbravo.model.ad.domain.List adList : referenceList) {
        Map<String, Object> row = new LinkedHashMap<String, Object>();
        row.put("id", adList.getId());
        row.put("value", adList.getSearchKey());
        row.put("name", adList.getIdentifier());
        result.add(row);
      }

    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }
}
