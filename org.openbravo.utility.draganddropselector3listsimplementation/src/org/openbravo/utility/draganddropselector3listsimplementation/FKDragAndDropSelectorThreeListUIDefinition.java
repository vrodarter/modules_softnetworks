/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2013 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.utility.draganddropselector3listsimplementation;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.application.Parameter;
import org.openbravo.client.kernel.reference.TextUIDefinition;
import org.openbravo.model.ad.domain.Reference;

/**
 * Implementation of the foreign key ui definition which uses a multi selector for its input/filter
 * types.
 * 
 * @author dmiguelez
 */
public class FKDragAndDropSelectorThreeListUIDefinition extends TextUIDefinition {
  private static final Logger log4j = Logger
      .getLogger(FKDragAndDropSelectorThreeListUIDefinition.class);

  @Override
  public String getFormEditorType() {
    return "OBDragAndDropSelectorItem";
  }

  @Override
  public String getParameterProperties(Parameter parameter) {

    if (parameter == null) {
      return super.getParameterProperties(parameter);
    }

    final String superJsonStr = super.getParameterProperties(parameter);
    Reference adReference = parameter.getReferenceSearchKey();
    JSONObject json = new JSONObject();
    if (adReference == null) {
      return superJsonStr;
    }
    try {
      if (superJsonStr.trim().startsWith("{")) {
        return "\"_adReferenceListID\":'" + adReference.getId() + "',"
            + superJsonStr.trim().substring(1, superJsonStr.trim().length() - 1);
      } else {
        if ("".equals(superJsonStr)) {
          return "\"_adReferenceListID\":'" + adReference.getId() + "'";
        } else {
          return "\"_adReferenceListID\":'" + adReference.getId() + "'," + superJsonStr;
        }
      }
    } catch (Exception e) {
      log4j.error("FKDragAndDropSelectorUIDefinition.getParameterProperties()", e);
    }
    return json.toString();

  }
}