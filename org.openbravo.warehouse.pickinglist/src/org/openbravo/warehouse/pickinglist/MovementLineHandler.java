/************************************************************************************
 * Copyright (C) 2013-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package org.openbravo.warehouse.pickinglist;

import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.model.materialmgmt.transaction.InternalMovementLine;
import org.openbravo.service.db.DbUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MovementLineHandler extends BaseActionHandler {
  final private static Logger log = LoggerFactory.getLogger(MovementLineHandler.class);

  @Override
  protected JSONObject execute(Map<String, Object> parameters, String content) {
    JSONObject jsonRequest = null;

    try {
      jsonRequest = new JSONObject(content);
      final String strAction = jsonRequest.getString("action");
      if ("confirm".equals(strAction)) {
        final JSONArray movLineIds = jsonRequest.getJSONArray("movementlines");
        // Get movementlines
        for (int i = 0; i < movLineIds.length(); i++) {
          if (i == movLineIds.length() - 1) {
            processMovementLine(movLineIds.getString(i), "CF");
          } else {
            processMovementLine(movLineIds.getString(i), "CF", false);
          }
        }
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "success");
        errorMessage.put("text", OBMessageUtils.messageBD("Success", false));
        jsonRequest.put("message", errorMessage);
      } else if ("reject".equals(strAction)) {
        final JSONArray movLineIds = jsonRequest.getJSONArray("movementlines");
        // Get movementlines
        for (int i = 0; i < movLineIds.length(); i++) {
          processMovementLine(movLineIds.getString(i), "PE");
        }
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "success");
        errorMessage.put("text", OBMessageUtils.messageBD("Success"));
        jsonRequest.put("message", errorMessage);
      }
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      try {
        jsonRequest = new JSONObject();
        Throwable ex = DbUtility.getUnderlyingSQLException(e);
        String message = OBMessageUtils.translateError(ex.getMessage()).getMessage();

        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", message);
        jsonRequest.put("message", errorMessage);
      } catch (Exception e2) {
        log.error(e.getMessage(), e2);
      }
    }
    return jsonRequest;

  }

  public static void processMovementLine(String strMoveLineId, String strItemStatus,
      boolean checkStatus) throws PropertyException {
    InternalMovementLine mvmtLine = OBDal.getInstance().get(InternalMovementLine.class,
        strMoveLineId);
    mvmtLine.setOBWPLItemStatus(strItemStatus);
    if (mvmtLine.getOBWPLGroupPickinglist() != null) {
      PickingList groupPL = mvmtLine.getOBWPLGroupPickinglist();
      String strStatus = OutboundPickingListProcess.checkStatus(
          groupPL.getMaterialMgmtInternalMovementLineEMOBWPLGroupPickinglistList(), false);
      groupPL.setPickliststatus(strStatus);
    }
    if (checkStatus) {
      String strStatus = OutboundPickingListProcess.checkStatus(mvmtLine
          .getOBWPLWarehousePickingList()
          .getMaterialMgmtInternalMovementLineEMOBWPLWarehousePickingListList(), true);
      mvmtLine.getOBWPLWarehousePickingList().setPickliststatus(strStatus);
      String prefValue = Preferences.getPreferenceValue("OBWPL_AutoClose", true, OBContext
          .getOBContext().getCurrentClient(), OBContext.getOBContext().getCurrentOrganization(),
          OBContext.getOBContext().getUser(), OBContext.getOBContext().getRole(), null);
      PickingList picking = (PickingList) mvmtLine.getOBWPLWarehousePickingList();
      if (prefValue.equals("Y") && strStatus.equals("CO")) {
        OutboundPickingListProcess.close(picking);
      }
    }
  }

  public static void processMovementLine(String strMoveLineId, String strItemStatus)
      throws PropertyException {
    processMovementLine(strMoveLineId, strItemStatus, true);
  }
}
