/************************************************************************************
 * Copyright (C) 2012-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package org.openbravo.warehouse.pickinglist;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;

@ApplicationScoped
@ComponentProvider.Qualifier(OBWPLCompontentProvider.PICKINGLIST_COMPONENT_TYPE)
public class OBWPLCompontentProvider extends BaseComponentProvider {
  public static final String PICKINGLIST_COMPONENT_TYPE = "OBWPL_ComponentType";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    return null;
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> resources = new ArrayList<ComponentResource>();
    resources.add(createStaticResource(
        "web/org.openbravo.warehouse.pickinglist/js/OBWPL_Process.js", false));
    resources.add(createStaticResource(
        "web/org.openbravo.warehouse.pickinglist/js/obwpl-assign.js", false));
    resources.add(createStaticResource(
        "web/org.openbravo.warehouse.pickinglist/js/obwpl-createfromorder.js", false));
    resources.add(createStaticResource(
        "web/org.openbravo.warehouse.pickinglist/js/obwpl-movementline.js", false));
    resources.add(createStaticResource(
        "web/org.openbravo.warehouse.pickinglist/js/OBWPL_ValidateComponent.js", false));
    resources.add(createStaticResource(
        "web/org.openbravo.warehouse.pickinglist/js/OBWPL_PickEditValidations.js", false));
    resources.add(createStaticResource(
        "web/org.openbravo.warehouse.pickinglist/js/obwpl-pickingdelete.js", false));

    resources.add(createStaticResource(
        "web/org.openbravo.userinterface.smartclient/openbravo/skins/Default/"
            + "org.openbravo.warehouse.pickinglist/ob-pick-validate-process.js", false));

    resources.add(createStyleSheetResource(
        "web/org.openbravo.userinterface.smartclient/openbravo/skins/Default/"
            + "org.openbravo.warehouse.pickinglist/ob-pick-validate-process.css", false));
    return resources;
  }

}
