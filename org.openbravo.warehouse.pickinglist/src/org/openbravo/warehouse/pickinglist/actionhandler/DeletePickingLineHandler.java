/************************************************************************************
 * Copyright (C) 2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************/
package org.openbravo.warehouse.pickinglist.actionhandler;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.materialmgmt.ReservationUtils;
import org.openbravo.model.common.enterprise.Locator;
import org.openbravo.model.common.plm.AttributeSetInstance;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.onhandquantity.Reservation;
import org.openbravo.model.materialmgmt.onhandquantity.ReservationStock;
import org.openbravo.model.materialmgmt.onhandquantity.StorageDetail;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.warehouse.pickinglist.PickingList;

public class DeletePickingLineHandler extends BaseProcessActionHandler {
  private static Logger log = Logger.getLogger(DeletePickingLineHandler.class);

  @Override
  protected JSONObject doExecute(Map<String, Object> parameters, String content) {
    JSONObject jsonRequest = null;
    JSONObject jsonResponse = new JSONObject();
    try {
      jsonRequest = new JSONObject(content);
      log.debug(jsonRequest);
      JSONObject errorMessage = new JSONObject();

      String strKey = jsonRequest.getString("M_InOutLine_ID");
      ShipmentInOutLine inoutLine = OBDal.getInstance().get(ShipmentInOutLine.class, strKey);
      PickingList picking = inoutLine.getObwplPickinglist();

      StorageDetail sd = getStorageDetail(inoutLine.getProduct(), inoutLine.getAttributeSetValue(),
          inoutLine.getStorageBin());

      Reservation reservation = ReservationUtils.getReservationFromOrder(inoutLine
          .getSalesOrderLine());
      ReservationStock reservationStock = ReservationUtils.reserveStockManual(reservation, sd,
          inoutLine.getMovementQuantity().subtract(reservation.getReleased()).negate(), "Y");
      if (reservationStock.getQuantity().compareTo(BigDecimal.ZERO) == 0) {
        OBDal.getInstance().remove(reservationStock);
      }

      OBDal.getInstance().flush();

      reservation = OBDal.getInstance().get(Reservation.class, reservation.getId());
      OBDal.getInstance().refresh(reservation);

      if (reservation.getReservedQty().equals(BigDecimal.ZERO)
          && reservation.getMaterialMgmtReservationStockList().isEmpty()) {
        if (reservation.getRESStatus().equals("CO")) {
          // Unprocess reservation
          ReservationUtils.processReserve(reservation, "RE");
          OBDal.getInstance().remove(reservation);
          OBDal.getInstance().flush();
        }
      }
      picking.getOBWPLPickinglistManualPickEditList().remove(inoutLine);
      picking.getMaterialMgmtShipmentInOutLineEMObwplPickinglistIDList().remove(inoutLine);

      if (inoutLine.getShipmentReceipt().getMaterialMgmtShipmentInOutLineList().size() == 1) {
        inoutLine.getSalesOrderLine().getSalesOrder().setObwplIsinpickinglist(false);
        OBDal.getInstance().save(inoutLine.getSalesOrderLine().getSalesOrder());
        OBDal.getInstance().remove(inoutLine);
        OBDal.getInstance().remove(inoutLine.getShipmentReceipt());
      } else {
        inoutLine.getShipmentReceipt().getMaterialMgmtShipmentInOutLineList().remove(inoutLine);
        OBDal.getInstance().remove(inoutLine);
      }

      OBDal.getInstance().flush();

      JSONArray responseActions = new JSONArray();
      JSONObject refreshGrid = new JSONObject();
      refreshGrid.put("refreshGrid", new JSONObject());
      responseActions.put(refreshGrid);
      errorMessage.put("severity", "success");
      errorMessage.put("title", OBMessageUtils.messageBD("Success"));
      jsonResponse.put("message", errorMessage);
      jsonResponse.put("responseActions", responseActions);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      try {
        jsonResponse = new JSONObject();
        String message = OBMessageUtils.translateError("OBWPL_ErrorDeletingLine").getMessage();
        JSONObject errorMessage = new JSONObject();
        errorMessage.put("severity", "error");
        errorMessage.put("text", message);
        jsonRequest.put("message", errorMessage);
      } catch (Exception e2) {
        log.error(e.getMessage(), e2);
        // do nothing, give up
      }
    }
    return jsonResponse;
  }

  private StorageDetail getStorageDetail(Product product, AttributeSetInstance attributeSetInst,
      Locator storageBin) {
    OBCriteria<StorageDetail> sdCriteria = OBDal.getInstance().createCriteria(StorageDetail.class);
    sdCriteria.add(Restrictions.eq(StorageDetail.PROPERTY_PRODUCT, product));
    sdCriteria.add(Restrictions.eq(StorageDetail.PROPERTY_ATTRIBUTESETVALUE, attributeSetInst));
    sdCriteria.add(Restrictions.eq(StorageDetail.PROPERTY_STORAGEBIN, storageBin));
    sdCriteria.add(Restrictions.isNull(StorageDetail.PROPERTY_ORDERUOM));
    sdCriteria.setMaxResults(1);
    return (StorageDetail) sdCriteria.uniqueResult();
  }
}
